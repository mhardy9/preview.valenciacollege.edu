<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/mathequipmentcheckout.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Math Lab Equipment Check out for Students &amp; Faculty</h2>
                        
                        <p>Here you will find an inventory of all of the equipment that the Math Department has
                           to offer from a Graphing Calculator for student check out<br>
                           to a Laptop Cart for classroom use.  
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <h2>Students<br>
                                          
                                       </h2>
                                       
                                    </div>
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <h2>Faculty/Staff</h2>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       <strong>Students can check out calculators in the Interactive Math Lab 1-144/142, The Depot
                                          4-121 and the Library 4-202.</strong><br>
                                       <em>Students must have a Valencia ID. Check out is for 4 hours, anything after 4 hours
                                          your account is charged $1.50 an hour. </em>
                                       
                                    </div>
                                 </div>
                                 
                                 
                                 <div><strong>Faculty/Staff resources are available for checking out by emailing<a href="mailto:aberry@valenciacollege.edu"> Andi Berry</a>. Iinclude equipment,  room, date and time needed. </strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>TI-84 Plus Graphing Calculators<br>
                                          <img alt="Graphine Calculator" height="147" src="Calculator.jpg" width="111">      
                                       </p>
                                       
                                       <p>TI-83 Graphing Calculators</p>
                                       
                                       <p>TI-84 Plus TALKING Graphing Calculators<br>
                                          <em>(Students with disabilities only)</em>        <br>
                                          <img alt="Talking Graphing Calculators" height="210" src="talkingCalcjpg.jpg" width="158">        <br>
                                          
                                       </p>
                                       
                                       <p>1 Tactile Math drawing board<br>
                                          <em>(Students with disabilities only) <br>
                                             <img alt="Tactile Board" height="147" src="Tactileboard_001.jpg" width="197">        </em> 
                                       </p>
                                       
                                       
                                       
                                    </div>
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <p>2 Classroom sets of Graphing Calculators (25 each) <br>
                                          <br>
                                          <img alt="Classroom set of calculators" height="184" src="CalculatorSet.jpg" width="139">        <br>
                                          
                                       </p>
                                       
                                       <p>5 Sets of Turning Point Clickers ( 1 set of 50 &amp; 4 sets of 25) <br>
                                          <img alt="Turning Point Clickers" height="129" src="clickers.jpg" width="172">      
                                       </p>
                                       
                                       <p>2 IPad Carts for classrooms (24 IPads each) <br>
                                          <img alt="IPad Cart" height="161" src="IPadCart_001.jpg" width="121">      
                                       </p>
                                       
                                       <p>1 Laptop cart for classrooms (25 Laptops) <br>
                                          <img alt="Laptop Cart" height="141" src="LaptopCart_000.jpg" width="189">      
                                       </p>
                                       
                                       <p>1 Tactile Math drawing board<br>
                                          <em>(Students with disabilities only) <br>
                                             <img alt="Tactile board" height="141" src="Tactileboard_000.jpg" width="189">        </em></p>
                                       
                                       <p><br>
                                          
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <p><a href="MathEquipmentCheckOut.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/mathequipmentcheckout.pcf">©</a>
      </div>
   </body>
</html>