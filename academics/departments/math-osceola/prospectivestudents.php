<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/math-osceola/prospectivestudents.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/math-osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Osceola Campus Math Department</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/math-osceola/">Math Osceola</a></li>
               <li>Osceola Campus Math Department</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Prospective Students </h2>
                        
                        <p><a href="documents/Rampupflyer_000.pdf"><img alt="Ramp Logo" height="327" src="rampupforaxistv_000.jpg" width="582"><br>
                              Click on banner for more information</a> 
                        </p>
                        
                        <p><strong>The PERT is a placement test to assess English, reading and math skills. </strong><strong>The PERT score will be used </strong><strong>for placement in the correct math class.</strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong>PERT</strong><br>
                                          (<em>Postsecondary Education 
                                             Readiness Test</em>)
                                       </p>
                                       
                                       <p> The PERT is Florida’s placement test for all students entering the Florida College
                                          System. 
                                       </p>
                                       
                                       <p>For more information please click <a href="../../assessments/pert/taking-the-exam.html" target="_blank">here</a></p>
                                       
                                       <p><strong><a href="documents/MathPathwaysChartFinal_000.pdf">Math Path </a></strong><br>
                                          <span>This document will show the order in which math classes must be taken.<br>
                                             </span></p>
                                       
                                       <p><strong><a href="http://net5.valenciacollege.edu/schedule//" target="_blank">Credit Class Schedule</a><br>
                                             </strong><span>For a schedule of classes on all campuses. </span></p>
                                       
                                       <p><strong><u>Advising</u></strong><br>
                                          <span>If you have any questions about the classes or the direction you would like to take,
                                             please see an advisor.</span></p>
                                       
                                       <p>Students seeking A.S. degrees can email
                                          
                                          
                                          <a href="mailto:czequeira@valenciacollege.edu">Claudia Zequeira</a> to make an appointment.
                                       </p>
                                       
                                       <p>Students seeking A.A. degrees can get their questions answered at the <a href="../../students/answer-center/index.html" target="_blank">Answer Center</a>
                                          Building 2, Room 150, phone 407-582-4139.
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><img alt="Johnie Forsythe" height="123" src="johnie.jpg" width="123"></p>
                                       
                                       <p><a href="mailto:jforsythe3@valenciacollege.edu">Johnie Forsythe</a> <br>
                                          <span><strong>Math Coordinator</strong></span></p>
                                       
                                       <p>Building 4, room 231B <br>
                                          407-582-4178<br>
                                          407-697-4178
                                       </p>
                                       
                                       <p>I AM HERE FOR YOU!!</p>
                                       
                                       <p>Not sure what math classes you should be taking?</p>
                                       
                                       <p>Math anxiety?</p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><a href="Prospectivestudents/index.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/math-osceola/prospectivestudents.pcf">©</a>
      </div>
   </body>
</html>