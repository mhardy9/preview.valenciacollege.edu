<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Behavioral &amp; Social Sciences | West Campus | Valencia College</title>
      <meta name="Description" content="Behavioral &amp; Social Sciences | West Campus">
      <meta name="Keywords" content="college, school, educational, behavioral, social, sciences, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Behavioral Social Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12"><a id="content" name="content"></a>
                        				
                        <div class="col-md-6">
                           					
                           <h2>Division of Behavioral &amp; Social Sciences</h2>
                           					
                           <p>General education and specialty course work in the <strong>Division of Behavioral &amp; Social Sciences</strong> is associated with a wide range of academic disciplines including anthropology, criminal
                              justice, economics, education, history, physical education, political science, psychology,
                              sociology. These components of the educational program are designed to establish and
                              enhance awareness and understanding of human behavior, cultural diversity, global
                              economy and historical development. Interaction with this curriculum will provide
                              students with a knowledge base and skill orientation essential for intellectual growth,
                              academic progress and employment opportunity. Course work is designed to:
                           </p>
                           					
                           <ul>
                              						
                              <li>establish important connections with a heritage of human understanding by examining
                                 various approaches to issues of universal human concern (financial, moral, intellectual,
                                 technological, political, economic, scientific, familial, sexual, etc.);
                              </li>
                              						
                              <li>develop an awareness and comprehension of cultural, social, historical, geographical
                                 and environmental contexts essential for personal insight and productivity, social
                                 cooperation and life management;
                              </li>
                              						
                              <li>provide experiential learning opportunities which integrate academic and career pursuits;</li>
                              						
                              <li>accommodate a variety of student skill orientations including thoughtful and precise
                                 writing, critical reading, oral discourse and independent thinking;
                              </li>
                              						
                              <li>increase awareness and empathy in relation to historical events, personal and economic
                                 development, cultural diversity and a wide range of complex societal issues.
                              </li>
                              					
                           </ul>
                           					
                           <div>
                              						
                              <div>
                                 <hr>
                                 							
                                 <h3>Location</h3>
                                 							
                                 <div>
                                    								
                                    <div>Bldg 11, Rm 103</div>
                                    							
                                 </div>
                                 							
                                 <div>
                                    								
                                    <div>Tel: 407-582-1203 Fax: 407-582-1675</div>
                                    							
                                 </div>
                                 							
                                 <div>
                                    								
                                    <div>Monday - Friday 8am - 5pm</div>
                                    							
                                 </div>
                                 						
                              </div>
                              						
                              <p><strong>Campus Map:<span>&nbsp;</span></strong><a href="/academics/map/west.html">West Campus</a></p>
                              					
                           </div>
                           					
                           <hr>
                           					
                           <h3>Administration</h3>
                           					
                           <p><strong>Dean:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=mmcintire1.html">Molly McIntire</a><br><strong>Admin Assistant:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=jmorgan.html">Jonnel Morgan<span>&nbsp;</span></a><br><strong>Admin Assistant:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=ebeal.html">Elizabeth Beal</a><strong>Division Coordinator:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a></p>
                           					
                           <h3>Discipline Chairs</h3>
                           					<strong>Anthropology, History, &amp; Sociology</strong><span>:<span>&nbsp;</span></span><a href="/academics/includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a><br><strong>Criminal Justice Technology</strong><span>:<span>&nbsp;</span></span><a href="/academics/includes/UserInfo.cfm-username=lsykes3.html">Lauren Sykes</a><br><strong>Economics:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=tharris65.html">Tarteashia Harris</a><span><span>&nbsp;</span></span><br><strong>Education:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=ratkinson3.html">Rhonda Atkinson</a><span><span>&nbsp;</span></span><br><strong>Political Science:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=scrosby.html">Scott Crosby</a><span><span>&nbsp;</span></span><br><strong>Psychology:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=emodel.html">Eric Model</a><span></span>
                           					
                           <h3>Career Program Advisors</h3>
                           					
                           <p><strong>Criminal Justice Technology:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=ghall13.html">Genevieve Hall</a><br><strong>Education:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=ddeitrick.html">Donna Deitrick</a></p>
                           					
                           <p><strong>A.A. Coordinator:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=lrendle.html">Ellen Rendle</a></p>
                           					
                           <h3>Support Staff</h3>
                           					
                           <p><strong>Instructional Assistant:<span>&nbsp;</span></strong><a href="/academics/includes/UserInfo.cfm-username=rowens12.html">Ron Owens</a>, Physical Education
                           </p>
                           				
                        </div>
                        				
                        <div class="col-md-6">
                           					
                           <div class="aspect-ratio" style="height: 500px;"><iframe style="border: none;" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FValenciaBusiness&amp;tabs=timeline&amp;width=500&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=203940866824764" width="300" height="150" frameborder="0" scrolling="no"></iframe></div>
                           				
                        </div>	
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/index.pcf">©</a>
      </div>
   </body>
</html>