<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Handbook | Behavioral &amp; Social Sciences | West Campus | Valencia College</title>
      <meta name="Description" content="Faculty Handbook | Behavioral &amp; Social Sciences | West Campus">
      <meta name="Keywords" content="college, school, educational, behavioral, social, sciences, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/faculty-handbook.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Faculty Handbook</h2>
                        
                        <p>It is indeed a pleasure to have you as part of our faculty. Your expertise and your
                           love for teaching are essential to our everyday operations.&nbsp; To assist you during
                           this process, we have identified several links which will provide you with excellent
                           information. (See below.) Please take the time to review each one and remember also
                           to visit our website and become familiar with it.&nbsp; &nbsp;If you need additional information,
                           please contact us.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/generalcounsel/policy/default.cfm?policyID=67&amp;volumeID_1=14&amp;pcdure=0&amp;navst=0">Absence of a Professor from a Class Policy</a></li>
                           
                           <li><a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-11-Academic-Dishonesty.pdf">Academic Dishonesty Policy</a></li>
                           
                           <li><a class="icon_for_pdf" href="http://valenciacollege.edu/atlas/documents/FacultyQuickReference.pdf">Atlas Faculty Quick Reference Guide</a></li>
                           
                           <li><a href="http://valenciacollege.edu/calendar/">Calendars</a></li>
                           
                           <li><a href="http://valenciacollege.edu/generalcounsel/policy/default.cfm?policyID=66&amp;volumeID_1=4&amp;pcdure=0&amp;navst=0">Course Competencies and Student Outcomes Policy</a></li>
                           
                           <li><a href="http://valenciacollege.edu/schedule/">Credit Class Schedule</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty.cfm">Faculty and Staff Directory</a></li>
                           
                           <li><a href="http://valenciacollege.edu/calendar/">Important Dates</a></li>
                           
                           <li><a href="http://valenciacollege.edu/contact/directory.cfm">Phone Directory</a></li>
                           
                           <li><a href="http://valenciacollege.edu/generalcounsel/">Policy Manual</a></li>
                           
                           <li><a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct Policy</a></li>
                           
                           <li><a href="http://valenciacollege.edu/catalog/">Valencia Catalog</a></li>
                           
                        </ul>
                        
                        <h3 class="heading_divider_gray">Adjunct Issues</h3>
                        
                        <ul>
                           
                           <li><a href="http://net4.valenciacollege.edu/cp/adjunct_faculty_public/list.cfm">Adjunct Faculty Digital Dossier</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty/resources/">Faculty Resources</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty/resources/west/">Faculty Resources - West</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty/resources/checklist.cfm">Adjunct Faculty Toolbox</a></li>
                           
                        </ul>
                        
                        <h3 class="heading_divider_gray">Support Services</h3>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/atlas/documents/FacultyQuickReference.pdf">Atlas Learning Support System</a></li>
                           
                           <li><a href="http://valenciacollege.edu/facilities/plantops/workrequest.cfm">Maintenance Work Requests</a></li>
                           
                           <li><a href="http://valenciacollege.edu/learning-support/testing/rules.cfm">Testing Center Information and Rules</a></li>
                           
                           <li><a href="http://valenciacollege.edu/wordprocessing/">Word Processing Center Requests</a></li>
                           
                        </ul>
                        
                        <h3 class="heading_divider_gray">Academics</h3>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/departments/">Academic Departments</a></li>
                           
                           <li><a href="http://valenciacollege.edu/aadegrees/courserequirement.cfm">Associate in Arts Degree General Education Requirements</a></li>
                           
                           <li><a href="http://valenciacollege.edu/courses/">Course Descriptions</a></li>
                           
                           <li><a href="http://valenciacollege.edu/calendar/FinalExam.cfm">Final Exam Schedule</a></li>
                           
                           <li><a class="icon_for_pdf" href="http://valenciacollege.edu/atlas/documents/InputtingFinalGrades.pdf">Inputting Final Grades</a></li>
                           
                           <li><a href="http://valenciacollege.edu/faculty/development/CoursesResources/">Learning Centered Reference Guide</a></li>
                           
                           <li><a href="http://valenciacollege.edu/ferpa/">Privacy Rights of Students</a></li>
                           
                           <li><a href="http://www.sacscoc.org/principles.asp" target="_blank">Southern Association of College and Schools</a></li>
                           
                           <li><a href="http://valenciacollege.edu/programs/">Valencia Degree and Career Programs</a></li>
                           
                           <li><a href="http://valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Valencia's Strategic Planning 2008-2013</a></li>
                           
                           <li><a href="http://valenciacollege.edu/competencies/">Valencia Student Core Competencies</a></li>
                           
                           <li><a class="icon_for_pdf" href="http://valenciacollege.edu/atlas/documents/Withdrawing_a_Student.pdf">Withdrawing a Student</a></li>
                           
                        </ul>
                        
                        <h3 class="heading_divider_gray">Links</h3>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/oit/networking/telecomm/yellowpages.cfm">Yellow Pages</a></li>
                           
                           <li><a href="http://valenciacollege.edu/map/">Campus Locations</a></li>
                           
                           <li><a href="http://valenciacollege.edu/oit/networking/telecomm/emergencynumbers.cfm">Emergency Telephone Numbers</a></li>
                           
                           <li><a href="http://valenciacollege.edu/security/">Parking Permits</a></li>
                           
                        </ul>
                        
                        <h3 class="heading_divider_gray">Special Documents</h3>
                        
                        <ul>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/AbsenceandClassCancellationProcedures.doc">Absence and Class Cancellation Procedures</a></li>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/AdjunctMailboxes.doc">Adjunct Mailboxes</a></li>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/ApproveYourContract.doc">Approve Your Contract</a></li>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/ClassAttendanceandFinancialAid-FacultyMemoFall_2008.doc">Class Attendance and Financial Aid - Faculty Memo Fall_ 2008</a></li>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/GeneralInformation.doc">General Information</a></li>
                           
                           <li><a class="icon_for_doc" href="http://valenciacollege.edu/west/behavioral-and-social-sciences/documents/ResourceCenter.doc">Resource Center</a></li>
                           
                        </ul>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/faculty-handbook.pcf">©</a>
      </div>
   </body>
</html>