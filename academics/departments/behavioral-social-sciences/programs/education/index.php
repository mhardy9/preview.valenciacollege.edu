<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Education | Behavioral &amp; Social Sciences | Valencia College</title>
      <meta name="Description" content="History | Behavioral &amp; Social Sciences">
      <meta name="Keywords" content="college, school, educational, history, behavioral, social, science">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/programs/education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/programs/education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/">Programs</a></li>
               <li>Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Elementary Education, Pre-Major</h2>
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <p>This pre-major is designed for the student who plans to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor’s degree in Elementary Education
                           at the University of Central Florida.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/elementaryeducation/#text%23text">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/elementaryeducation/#programrequirementstext%23programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/educationgeneralpreparation/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>&nbsp;Early Childhood Education, Pre-Major</h3>
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <p>This pre-major is designed for the student who plans to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor’s degree in Early Childhood
                           Education.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/earlychildhoodeducation/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/earlychildhoodeducation/#programrequirementstext%23programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/eec/">Course Description</a>&nbsp;
                           </li>
                           
                        </ul>
                        
                        <h3>Links</h3>
                        
                        <hr>
                        
                        <h2>Alternative Certification &amp; Teacher Re-certification</h2>
                        
                        <p>The Educator Preparation Institute (EPI) provides a competency-based program that
                           offers an individual with a Bachelor’s degree in a discipline other than education
                           the preparation to become a classroom teacher in Florida. Participants who demonstrate
                           mastery of the 6 Florida Educator Accomplished Practices and present passing scores
                           on all sections of the Florida Teacher Certification Exams will be awarded a Certificate
                           of Completion. The program requirements designated by an EPI prefix provide institutional
                           credit, are not transferable to an upper-division institution, and do not count toward
                           any degree. Program acceptance is required for participation in the EPI.
                        </p>
                        
                        <p>Most of Valencia’s courses meet the requirements of teachers seeking recertification.
                           These courses are offered in a variety of delivery modes (face-to-face, online, hybrid,
                           etc.) on each Valencia campus.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/epi/index.html">Educator Preparation Institute</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/teacherpreparationrecertification/#Teacher_Recertification%23Teacher_Recertification">T</a><a href="http://catalog.valenciacollege.edu/teacherpreparationrecertification/#Teacher_Recertification%23Teacher_Recertification">eacher Re-certification</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h2>Faculty &amp; Staff</h2>
                        
                        <h3 class="heading_divider_gray">Faculty</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ratkinson3">Rhonda Atkinson</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?ratkinson3" target="_blank">http://frontdoor.valenciacollege.edu?ratkinson3</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3 class="heading_divider_gray">Adjunct</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=rbarnett5">Rebecca Barnett</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?rbarnett5" target="_blank">http://frontdoor.valenciacollege.edu?rbarnett5</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=cbarnhill">Cassandra Barnhill</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?cbarnhill" target="_blank">http://frontdoor.valenciacollege.edu/?cbarnhill</a><a href="http://frontdoor.valenciacollege.edu/?cbarnhill" target="_blank">http://frontdoor.valenciacollege.edu?cbarnhill</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=lnordmann">Lisa Bugden</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?lnordmann" target="_blank">http://frontdoor.valenciacollege.edu?lnordmann</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ngautier">Nirsa Gautier</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?ngautier" target="_blank">http://frontdoor.valenciacollege.edu?ngautier</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dgullett">Diane Gullett</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?dgullett" target="_blank">http://frontdoor.valenciacollege.edu?dgullett</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=dmullins4">Deborah Mullins</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?dmullins4" target="_blank">http://frontdoor.valenciacollege.edu?dmullins4</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=roehlrich">Rhonda Oehlrich</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?roehlrich" target="_blank">http://frontdoor.valenciacollege.edu?roehlrich</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jstandberry">JaNise Standberry</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?jstandberry" target="_blank">http://frontdoor.valenciacollege.edu?jstandberry</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=kunger3">Kathryn Unger</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/" target="_blank">http://frontdoor.valenciacollege.edu/</a><a href="http://frontdoor.valenciacollege.edu/?kunger3" target="_blank">http://frontdoor.valenciacollege.edu?kunger3</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3 class="heading_divider_gray">Staff</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="220px">Position</th>
                                 
                                 <th width="80px">Phone</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg3643136412496028296.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mbosley">Mike Bosley</a></td>
                                 
                                 <td>Executive Dean, LNC</td>
                                 
                                 <td>(407) 582-7007</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg1258889194279341567.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=mmcintire1">Molly McIntire</a></td>
                                 
                                 <td>Dean, Behav/Social Science, W</td>
                                 
                                 <td>(407) 582-5588</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/CFFileServlet/_cf_image/_cfimg-5804049821584979538.PNG" alt="Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=jmurdock3">Josh Murdock</a></td>
                                 
                                 <td>IT Mgr, Inst Design Svcs</td>
                                 
                                 <td>(407) 582-5423</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/programs/education/index.pcf">©</a>
      </div>
   </body>
</html>