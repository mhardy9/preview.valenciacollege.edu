<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Behavioral &amp; Social Sciences | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/programs/political-science-government/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/programs/political-science-government/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/">Programs</a></li>
               <li>Political Science Government</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        <h2>Political Science &amp; Government, Transfer Plan</h2>
                        
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <p>This transfer plan is designed to help you prepare to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor's degree in Political Science
                           and Government. 
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/politicalsciencegovernment/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/politicalsciencegovernment/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/politicalsciencegovernment/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        
                        
                        <h3>Student Clubs and Organizations</h3>
                        
                        
                        <ul>
                           
                           <li><a href="../../../../studentdev/clubs3.cfm-club=136.html">Model United Nations (M.U.N)</a></li>
                           
                           
                           <li><a href="../../../../studentdev/clubs3.cfm-club=164.html">Human Empathy and Rights Organization (HERO)</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../index.html">
                                          West Campus Behavioral and Social Sciences 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 11, Rm 103</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Tel: 407-582-1203 Fax: 407-582-1675</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Friday 8am - 5pm</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <p>
                              <strong>Campus Map: </strong><a href="../../../../map/west.html">West Campus</a>
                              
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/likebox.php?id=107641529281272&amp;width=250&amp;connections=0&amp;stream=false&amp;header=false&amp;height=100"></iframe>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h3>Administration</h3>
                        
                        <p>
                           <strong>Dean: </strong><a href="../../../../includes/UserInfo.cfm-username=mmcintire1.html">Molly McIntire</a><br>
                           <strong>Admin Assistant: </strong><a href="../../../../includes/UserInfo.cfm-username=jmorgan.html">Jonnel Morgan </a><br>
                           <strong>Admin Assistant: </strong><a href="../../../../includes/UserInfo.cfm-username=ebeal.html">Elizabeth Beal</a>
                           <strong>Division Coordinator: </strong><a href="../../../../includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a>
                           
                        </p>
                        
                        
                        <h3>Discipline Chairs</h3>
                        
                        
                        
                        <div>
                           <strong>Anthropology</strong>: <a href="../../../../includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a><br>
                           
                        </div>
                        
                        <div>
                           <strong>Criminal Justice Technology</strong>: <a href="../../../../includes/UserInfo.cfm-username=lsykes3.html"> Lauren Sykes</a><br>
                           
                        </div>
                        
                        <div>
                           <strong>Economics: </strong><a href="../../../../includes/UserInfo.cfm-username=tharris65.html">Tarteashia Harris</a> <br>
                           
                        </div>
                        
                        <div>
                           <strong>Education: </strong><a href="../../../../includes/UserInfo.cfm-username=ratkinson3.html">Rhonda Atkinson</a>  <br>
                           
                        </div>
                        
                        <div>
                           <strong>History</strong>: <a href="../../../../includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a><br>
                           
                        </div>
                        
                        <div>
                           <strong>Political Science: </strong><a href="../../../../includes/UserInfo.cfm-username=scrosby.html">Scott Crosby</a> <br>
                           
                        </div>
                        
                        <div>
                           <strong>Psychology: </strong><a href="../../../../includes/UserInfo.cfm-username=emodel.html">Eric Model</a> <br>
                           
                        </div>
                        
                        <div>
                           <strong>Sociology</strong>: <a href="../../../../includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a><br>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <h3>Support Staff </h3>
                        
                        <p>
                           <strong>Instructional Assistant: </strong><a href="../../../../includes/UserInfo.cfm-username=rowens12.html">Ron Owens</a>, Physical Education
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/programs/political-science-government/index.pcf">©</a>
      </div>
   </body>
</html>