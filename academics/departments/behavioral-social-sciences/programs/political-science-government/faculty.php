<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Behavioral &amp; Social Sciences | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/programs/political-science-government/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/programs/political-science-government/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/">Programs</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/political-science-government/">Political Science Government</a></li>
               <li>Behavioral &amp; Social Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        <h2>Faculty &amp; Staff - Political Science &amp; Government</h2>
                        
                        
                        
                        
                        
                        
                        
                        <link href="../../../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        <h3>Faculty</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>Name</div>
                                 
                                 <div>Website</div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/_cfimg4284775311316223337.PNG.html" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=tbranz.html">Tyler Branz</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?tbranz" target="_blank">http://frontdoor.valenciacollege.edu?tbranz</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=scrosby.html">Scott Crosby</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://faculty.valenciacollege.edu/scrosby" target="_blank">http://faculty.valenciacollege.edu/scrosby</a> <br><a href="http://frontdoor.valenciacollege.edu?scrosby" target="_blank">http://frontdoor.valenciacollege.edu?scrosby</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=dduncan.html">Desmond Duncan</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?dduncan" target="_blank">http://frontdoor.valenciacollege.edu?dduncan</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/_cfimg-4744717111458703248.PNG.html" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=amathews1.html">Adrienne Mathews</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?amathews1" target="_blank">http://frontdoor.valenciacollege.edu?amathews1</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=srampersaud1.html">Subhas Rampersaud</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?srampersaud1" target="_blank">http://frontdoor.valenciacollege.edu?srampersaud1</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3>Adjunct</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>Name</div>
                                 
                                 <div>Website</div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=nboujaberdiederichs.html">Nicolle Boujaber-Diederichs</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?nboujaberdiederichs" target="_blank">http://frontdoor.valenciacollege.edu?nboujaberdiederichs</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=bfarcau.html">Bruce Farcau</a></div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=wgeeslin.html">William Geeslin</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?wgeeslin" target="_blank">http://frontdoor.valenciacollege.edu?wgeeslin</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jgranger.html">David Granger</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jgranger" target="_blank">http://frontdoor.valenciacollege.edu?jgranger</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=smelnick.html">Stan Melnick</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?smelnick" target="_blank">http://frontdoor.valenciacollege.edu?smelnick</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="../../../../programs/behavioral-and-social-sciences/programs/political-science-government/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=ftua.html">Phil Tua</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu/?ftua" target="_blank">http://frontdoor.valenciacollege.edu/?ftua</a> <a href="http://frontdoor.valenciacollege.edu?ftua" target="_blank">http://frontdoor.valenciacollege.edu?ftua</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/programs/political-science-government/faculty.pcf">©</a>
      </div>
   </body>
</html>