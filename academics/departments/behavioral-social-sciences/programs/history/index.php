<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>History | Behavioral &amp; Social Sciences | Valencia College</title>
      <meta name="Description" content="History | Behavioral &amp; Social Sciences">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/programs/history/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/programs/history/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/">Programs</a></li>
               <li>History</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>History, Transfer Plan</h2>
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <p>This transfer plan is designed to help you prepare to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor's degree in History.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/history/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/history/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/history/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3>Faculty</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=msmith01">Mark Smith</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?msmith01" target="_blank">http://frontdoor.valenciacollege.edu?msmith01</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Adjunct Faculty</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th width="50px">&nbsp;</th>
                                 
                                 <th width="200px">Name</th>
                                 
                                 <th width="300px">Website</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=ikennedy">Ian Kennedy</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?ikennedy" target="_blank">http://frontdoor.valenciacollege.edu?ikennedy</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><img src="http://valenciacollege.edu/oit/about/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" align="absmiddle" border="0"></td>
                                 
                                 <td><a class="colorbox cboxElement" href="http://valenciacollege.edu/includes/UserInfo.cfm?username=colittle">Cass O'Little</a></td>
                                 
                                 <td><a href="http://frontdoor.valenciacollege.edu/?colittle" target="_blank">http://frontdoor.valenciacollege.edu?colittle</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/programs/history/index.pcf">©</a>
      </div>
   </body>
</html>