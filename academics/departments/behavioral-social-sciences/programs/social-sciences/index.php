<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Social Sciences | Behavioral &amp; Social Sciences | Valencia College</title>
      <meta name="Description" content="Social Sciences | Behavioral &amp; Social Sciences">
      <meta name="Keywords" content="college, school, educational, social, sciences, behavioral">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/behavioral-social-sciences/programs/social-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/behavioral-social-sciences/programs/social-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Behavioral &amp; Social Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/">Behavioral Social Sciences</a></li>
               <li><a href="/academics/departments/behavioral-social-sciences/programs/">Programs</a></li>
               <li>Social Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        <h2>Social Sciences, Transfer Plan</h2>
                        
                        
                        <h3>Associate in Arts Degree</h3>
                        
                        <p>This transfer plan is designed to help you prepare to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor's degree in Social Sciences.
                           
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/socialsciences/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/socialsciences/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/socialsciences/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/behavioral-social-sciences/programs/social-sciences/index.pcf">©</a>
      </div>
   </body>
</html>