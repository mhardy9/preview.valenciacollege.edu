<ul>
	<li><a href="index.php">Science</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<div class="menu-wrapper c3">
			<div class="col-md-4">
				<ul>
					<li><a href="astronomy.php">Astronomy</a></li>
					<li><a href="biology.php">Biology</a></li>
					<li><a href="chemistry.php">Chemistry</a></li>
					<li><a href="earth_science.php">Earth Science</a></li>
					<li><a href="landscape_horticulture_technology.php">Landscape &amp; Horticulture Technology</a></li>
					<li><a href="physics.php">Physics</a></li>
					<li><a href="faculty.php">Science Faculty</a></li>
					<li><a href="adjunct.php">Teaching Opportunities</a></li>
				</ul>
			</div>
		</div>
	</li>
</ul>
