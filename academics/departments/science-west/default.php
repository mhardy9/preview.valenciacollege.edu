<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img src="../west/science/arrow.gif"> 
                                 <img src="../west/science/arrow.gif"> 
                                 <img src="../west/science/arrow.gif">
                                 <img src="../west/science/arrow.gif"> 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div> 
                                       
                                       <div> 
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>Navigate</div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div> 
                                                   
                                                   
                                                   <div> 
                                                      
                                                      <link href="../west/science/science-global.css.html" media="screen" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      <a name="content" id="content"></a>
                                                      
                                                      <br>
                                                      <img alt="" border="0" height="100" name="scienceheader" src="../west/science/science-header.jpg" usemap="#m_scienceheader" width="582" id="scienceheader"><map name="m_scienceheader" id="m_scienceheader">
                                                         
                                                         <area alt="West Campus Science Department" coords="0,0,582,78" href="default.html" shape="rect" target="_self" title="West Campus Science Department">
                                                         
                                                         <area alt="Physics" coords="497,81,550,81,550,100,497,100,497,81" href="physics.html" shape="poly" title="Physics">
                                                         
                                                         <area alt="Landscape &amp; Horticulture Technology" coords="297,81,491,81,491,100,297,100,297,81" href="landscape_horticulture_technology.html" shape="poly" title="Landscape &amp; Horticulture Technology">
                                                         
                                                         <area alt="Earth Science" coords="205,81,288,81,288,100,205,100,205,81" href="earth_science.html" shape="poly" title="Earth Science">
                                                         
                                                         <area alt="Chemistry" coords="139,81,201,81,201,100,139,100,139,81" href="chemistry.html" shape="poly" title="Chemistry">
                                                         
                                                         <area alt="Biology" coords="84,81,136,81,136,100,84,100,84,81" href="biology.html" shape="poly" title="Biology">
                                                         
                                                         <area alt="Astronomy" coords="11,81,78,100" href="astronomy.html" shape="rect" title="Astronomy">
                                                         </map>    
                                                      
                                                      <h2>Division Information</h2>
                                                      
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div> 
                                                               
                                                               
                                                               <div><strong>Dean </strong></div>
                                                               
                                                               
                                                               <div>Robert Gessner, D.V.M.</div>
                                                               
                                                            </div>
                                                            
                                                            <div> 
                                                               
                                                               <div><strong>Administrative Assistant </strong></div>
                                                               
                                                               
                                                               <div><a href="mailto:ktate3@valenciacollege.edu">Kerri Tate </a></div>
                                                               
                                                            </div>
                                                            
                                                            <div> 
                                                               
                                                               <div><strong>Office Location</strong></div>
                                                               
                                                               <div>West Campus, AHS-231 </div>
                                                               
                                                            </div>
                                                            
                                                            <div> 
                                                               
                                                               
                                                               <div><strong> Phone</strong></div>
                                                               
                                                               <div>407-582-1407</div>
                                                               
                                                            </div>
                                                            
                                                            <div> 
                                                               
                                                               <div><strong>Mailing Address</strong></div>
                                                               
                                                               <div>1800 South Kirkman Rd. 4-3<br>
                                                                  Orlando, FL 32811
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>          
                                                   <div><img alt="" height="1" src="../west/science/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../west/science/spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div> 
                                       
                                       <div><img alt="" height="1" src="../west/science/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../west/science/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>  
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/default.pcf">©</a>
      </div>
   </body>
</html>