<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Chemistry | Division of Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/chemistry.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Chemistry </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <h2>Chemistry</h2>
                        					
                        <p>CHM 1020 - Chemistry in Everyday Life</p>
                        
                        					
                        <p>Meets general education requirement for non-science major. Study of some basic concepts
                           in inorganic chemistry, organic chemistry and biochemistry. Students apply principles
                           as they analyze, discuss and make decisions on chemically related problems that affect
                           everyday life. (Not prerequisite for any other science course.)
                        </p>
                        
                        					
                        <p>CHM 1025C - Introduction to General Chemistry</p>
                        
                        					
                        <p>Prerequisite: One year of high school algebra or MAT 0025C. Prepares students without
                           high school chemistry or with inadequate background for CHM 1045C. Modern chemical
                           theories used to develop understanding of fundamentals of inorganic chemistry and
                           its applications. Emphasis on quantitative relationships, using dimensional analysis
                           to solve problems. Laboratory experiences integral part of course. May not be taken
                           for credit subsequent to earning C or better in CHM 1045C. (Lab fee)
                        </p>
                        
                        					
                        <p>CHM 1045C - General Chemistry with Qualitative Analysis I</p>
                        
                        					
                        <p>Prerequisite: CHM 1025C or one year of high school chemistry with C or better and
                           MAC 1102 or MAT 1033 or two years of high school algebra. Study of basic principles
                           of chemistry, emphasizing formation of unifying model from collection of observations
                           and measurements. Laboratory illustrates principles discussed in classroom. (Lab fee)CHM1045H
                           GENERAL CHEMISTRY WITH QUALITATIVE ANALYSIS I - HONORS is the same as CHM 1045C with
                           honors content. Honors program permission required. (Lab fee)
                        </p>
                        
                        					
                        <p>CHM 1046C - General Chemistry with Qualitative Analysis II</p>
                        
                        					
                        <p>Prerequisite: CHM 1045C with C or better. Continuation of CHM 1045C dealing mainly
                           with equilibrium theory, thermodynamics, chemical kinetics and electrochemistry. Laboratory
                           illustrates principles of ionic equilibria within framework of qualitative analysis.
                           (Lab fee)CHM1046H GENERAL CHEMISTRY WITH QUALITATIVE ANALYSIS II - HONORS is the same
                           as CHM 1046C with honors content. Honors program permission required. (Lab fee)
                        </p>
                        
                        					
                        <p>CHM 2210C - Organic Chemistry I</p>
                        
                        					
                        <p>Prerequisite: CHM 1046C or comparable college-level general chemistry course. Deals
                           with aliphatic and aromatic compounds, their properties, reactions and synthesis,
                           emphasizing dependence of properties and reaction mechanisms upon structure. Laboratory
                           illustrates techniques of separation, identification and purification. (Lab fee)
                        </p>
                        
                        					
                        <p>CHM 2211C - Organic Chemistry II</p>
                        
                        					
                        <p>Prerequisite: Satisfactory completion of CHM 2210C. Continuation of CHM 2210C with
                           laboratory devoted to multistep synthesis. (Lab fee)
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/chemistry.pcf">©</a>
      </div>
   </body>
</html>