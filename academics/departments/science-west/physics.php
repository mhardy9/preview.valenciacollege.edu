<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Physics | Division of Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/physics.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Physics </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Physics</h2>
                        
                        
                        
                        <p>PHY 1007C - Physics</p>
                        
                        
                        <p>Prerequisite: MAC 1102 or MAC 1105 or two years of high school algebra.  One-semester
                           course for health-related majors.  Survey of topics in physics related to health field.
                           Applications of physics to principles of mechanics, heat, light, sound, electricity
                           and magnetism, and radioactivity as they apply to health field.  May not be taken
                           for credit subsequent to receiving grade of C or better in any higher physics course.
                           (Lab fee)
                        </p>
                        
                        
                        <p>PHY 1053C - Introductory Physics I</p>
                        
                        
                        <p>Prerequisite: High school trigonometry with a minimum grade of C or MAC 1114 or MAC
                           1147.  Fundamental principles of mechanics, heat, and sound.  For students whose requirements
                           for baccalaureate degree include basic course in physics. (Lab fee)
                        </p>
                        
                        
                        <p>PHY 1054C - Introductory Physic II</p>
                        
                        
                        <p>Prerequisite: PHY 1053C.  Fundamental principles of electricity, magnetism, optics,
                           and elements of modern physics.  For students whose requirements for baccalaureate
                           degree include basic course in physics (Lab fee)
                        </p>
                        
                        
                        <p>PHY 2048C - General Physics with Calculus I</p>
                        
                        
                        <p>Prerequisite: MAC 2311.  Fundamental principles of mechanics, heat, and sound.  For
                           physics, mathematics, chemistry, and pre-engineering majors (Lab fee)
                        </p>
                        
                        
                        <p>PHY 2049C - General Physics with Calculus II</p>
                        
                        
                        <p>Prerequisite: PHY 2048C.  Fundamental principles of electricity, magnetism, and optics.
                           For physics, mathematics, chemistry, and pre-engineering majors. (Lab fee)
                        </p>
                        
                        
                        <p>PHY 2101 - Modern Physics</p>
                        
                        
                        <p>Prerequisite: PHY 2049C.  Special relativity, optical and X-ray spectra, interaction
                           and duality of particles and radiation, basic concepts of quantum mechanics, atomic
                           and molecular structures, introductory solid state and elementary nuclear and particle
                           physics.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/physics.pcf">©</a>
      </div>
   </body>
</html>