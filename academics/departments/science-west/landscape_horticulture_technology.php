<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Landscape &amp; Horticultural Technology | Division of Science  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/landscape_horticulture_technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Landscape &amp; Horticultural Technology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Landscape &amp; Horticulture Technology</h2>
                        
                        <div>
                           
                           <!-- <a href="http://www.agrowknow.org/" target="_blank"><img border="0" src="../west/science/AgrowKnowledge.gif" /></a>
                <br />
                <br /> -->
                           <a href="http://www.agrowknow.org">Valencia is a partner of Agrowknowledge</a>
                           
                        </div>
                        
                        <p><strong>HOS 1010 - Introduction to Horticultural Science</strong></p>
                        
                        <p>Study of scientific basis of horticulture for beginning students. Part I introduces
                           biology of horticulture; Part II deals with techniques of horticulture; Part III surveys
                           industry, emphasizing distinguishing characteristics and special problem. Includes
                           aesthetic aspects of horticulture. Lab periods cover such practices as pruning, propagation,
                           plant classification. (Lab fee)
                        </p>
                        
                        <p><strong>IPM 1301C - Pesticides </strong></p>
                        
                        <p>Involves classification, mode of action, toxicity, mixing, registration and safe application
                           techniques of pesticides used in pest control industry. 
                        </p>
                        
                        <p><strong>ORH 1510C - Ornamental Plant Materials I </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Detailed study of temperate zone
                           plants used in Florida landscaping. Includes identification, growth characteristics,
                           propagation, culture and uses of plants. (Lab fee) 
                        </p>
                        
                        <p><strong>ORH 1511C - Ornamental Plant Materials II </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Identification, growth characteristics,
                           propagation, culture and use of tropical and sub-tropical plants in Florida plantscapes.
                           (Lab fee) 
                        </p>
                        
                        <p><strong>ORH 1873C - Interiorscaping </strong></p>
                        
                        <p>Prerequisite: ORH 1511 or departmental approval. Comprehensive study of design elements
                           and principles as applied to interior situations and conditions. Emphasis on preparation,
                           evaluation and implementation of functionally designed areas. Maintenance procedures
                           and practices discussed in detail. (Lab fee) 
                        </p>
                        
                        <p><strong>ORH 2220C - Turf Grass Culture </strong></p>
                        
                        <p>Prerequisites: HOS 1010 and SOS 2102 or departmental approval. Detailed study of turf
                           grass varieties and their establishment, maintenance and renovation. Emphasis on physiology,
                           soil-water relationships, nutrition, pests and their control and sod production. (Lab
                           fee) 
                        </p>
                        
                        <p><strong>ORH 2251C - Nursery Operation-Management </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Study of management and cultural
                           practices. Includes laboratory work in time-motion studies, production scheduling,
                           marketing surveying, nursery design, nursery visitations, nursery personnel, cost
                           analysis, cultural approaches, growing structures and equipment use. (Lab fee) 
                        </p>
                        
                        <p><strong>ORH 2262C - Flori-Crop Production and Use </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Basic concepts of commercial greenhouse
                           construction, maintenance and environmental control techniques. Majoremphasis on production,
                           management and marketing of major floral crops. (Lab fee) 
                        </p>
                        
                        <p><strong>ORH 2840C - Landscape Construction </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Comprehensive study of methods and
                           practices in landscape construction industry today. Topics include mechanics of landscaping
                           construction, materials selection and preparation, personnel hiring, training and
                           retraining, work schedules, planning, labor analysis, pricing, bidding, and estimating.
                           
                        </p>
                        
                        <p><strong>PLP 2001C - Plant Pathology </strong></p>
                        
                        <p>Prerequisite: HOS 1010 or departmental approval. Deals with more common and important
                           diseases of horticultural plants. Discusses symptoms, life history of casual organisms
                           and treatments of diseases. Analysis of biological, physical and chemical approaches.
                           (Lab fee) 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/landscape_horticulture_technology.pcf">©</a>
      </div>
   </body>
</html>