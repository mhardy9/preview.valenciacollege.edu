<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Biology | Division of Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/biology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Biology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <h2>Biology</h2>
                        
                        					
                        <p>BSC 1005. BIOLOGICAL SCIENCE.</p>
                        
                        					
                        <p>An introduction to essential principles of biological science. Topics include, but
                           are not limited to, the nature of science and the scientific method, chemistry for
                           biology, cell structure, metabolism, reproduction and genetics, organisms and ecology.
                           This is a general education course for non-biology majors. It is also recommended
                           for students who need preparation before enrolling in a biology course for Science
                           majors.
                        </p>
                        
                        					
                        <p>BSC 1005C. BIOLOGICAL SCIENCE COMBINED.</p>
                        
                        					
                        <p>Same as <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005');" title="BSCÂ&nbsp;1005">BSC 1005</a> and <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005L');" title="BSCÂ&nbsp;1005L">BSC 1005L</a> with class and lab combined. (Special Fee: $62.00).
                        </p>
                        
                        					
                        <p>BSC 1005H. BIOLOGICAL SCIENCE - HONORS.</p>
                        
                        					
                        <p>Prerequisites: Honors Program permission Same as <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005');" title="BSCÂ&nbsp;1005">BSC 1005</a> with honors content. Honors program permission required. (Special Fee: $62.00).
                        </p>
                        
                        					
                        <p>BSC 1005L. LAB IN APPLIED BIOLOGY.</p>
                        
                        					
                        <p>Biology laboratory course that will satisfy the General Education Requirement for
                           a laboratory science at many universities and may be taken concurrently with, or independently
                           of, <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005');" title="BSCÂ&nbsp;1005">BSC 1005</a>. (Special Fee: $62.00).
                        </p>
                        
                        					
                        <p>BSC 1010C. FUNDAMENTALS OF BIOLOGY I.</p>
                        
                        					
                        <p>Prerequisite: Satisfactory completion of all mandated courses in reading, mathematics,
                           English, and English for Academic Purposes. Introduction to fundamental biological
                           principles emphasizing common attributes of all living organisms. Unifying concepts
                           include chemical structure of living matter, structure and function of the cell, specialized
                           cells, major metabolic functions, control systems, reproduction, genetics, evolution
                           and ecology. Prerequisite for advanced biology courses. (Special Fee: $62.00).
                        </p>
                        
                        					
                        <p>BSC 1010H. FUNDAMENTALS OF BIOLOGY - HONORS.</p>
                        
                        					
                        <p>Same as <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1010C');" title="BSCÂ&nbsp;1010C">BSC 1010C</a> with honors content. Honors program permission required. (Special Fee: $62.00).
                        </p>
                        
                        					
                        <p>BSC 1011C. FUNDAMENTALS OF BIOLOGY II.</p>
                        
                        					
                        <p>Prerequisite: Minimum grade of C in <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1010C');" title="BSCÂ&nbsp;1010C">BSC 1010C</a> or <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1010H');" title="BSCÂ&nbsp;1010H">BSC 1010H</a> A continuation of BSC1010C. Includes an analysis of biological systems at the organismal
                           and supraorganismal levels: Unity and diversity of life, organismal structure and
                           function. Will examine such topics as: Darwinism, origin of life, diversity and origin
                           of Eukaryotes, evolution and diversity of the five kingdoms; animal and plant morphology,
                           reproduction, development of animal behavior, population biology and ecology. (Special
                           Fee: $62.00).
                        </p>
                        					
                        <p>BSC 1011H. FUNDAMENTALS OF BIOLOGY II - HONORS.</p>
                        
                        					
                        <p>Same as <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1011C');" title="BSCÂ&nbsp;1011C">BSC 1011C</a> with honors content. Honors program permission required. (Special Fee: $62.00).
                        </p>
                        					
                        <p>BSC 1020. HUMAN BIOLOGY.</p>
                        
                        					
                        <p>A general education non-laboratory course for students not majoring in biology. Includes
                           study of the human body with an emphasis on major organ systems and processes and
                           their links to biological concepts underlying major societal and bioethical issues.
                        </p>
                        					
                        <p>BSC 1020C. HUMAN BIOLOGY COMBINED.</p>
                        
                        					
                        <p>A general education course for students not majoring in biology that is fully integrated
                           with a laboratory that emphasizes active learning strategies. Includes study of scientific
                           method and study of human biology with an emphasis on major organ systems and processes,
                           human development, genetics, diseases, biochemical processes and their relation to
                           the human body, and biological concepts underlying major societal and bioethical issues.
                           (Special Fee: $62.00).
                        </p>
                        					
                        <p>BSC 1026. BIOLOGY OF HUMAN SEXUALITY.</p>
                        
                        					
                        <p>General non-laboratory course designed to introduce students to various biological
                           aspects of human sexuality, including sexual genetics and sex determination, sexual
                           anatomy, physiology and development, processes of fertilization, pregnancy, birth
                           control, sexually transmitted diseases, menstruation, menopause and aging. Other appropriate
                           areas may be discussed when time permits.
                        </p>
                        					
                        <p>BSC 1026H. BIOLOGY OF HUMAN SEXUALITY - HONORS.</p>
                        
                        					
                        <p>Prerequisites: Honors Program permission Same as <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1026');" title="BSCÂ&nbsp;1026">BSC 1026</a> with honors content. Honors program permission required.
                        </p>
                        					
                        <p>BSC 1050. ENVIRONMENTAL SCIENCE.</p>
                        
                        					
                        <p>General education non-laboratory course provides understanding of our interdependence
                           with and responsibility for environment. Investigates such aspects of environment
                           as pollution, urbanization, population trends and changes in lifestyles. Treats present
                           and projected solutions to problems.
                        </p>
                        					
                        <p>BSC 1050H. ENVIRONMENTAL SCIENCE - HONORS.</p>
                        
                        					
                        <p>Prerequisite: Honors Program permission Same as BSC with honors content. Honors program
                           permission required.
                        </p>
                        					
                        <p>BSC 1061. CENTRAL FLORIDA HABITATS.</p>
                        
                        					
                        <p>This active learning elective science course teaches students to recognize the common
                           habitats of Central Florida through identification of typical plants, plant species
                           composition, and soil and topography. Supporting topics are natural fire cycles, fire-mediated
                           succession and prescribed burning, nature of watersheds and causes and effects of
                           flooding, invasive exotic plants and animals, and conservation land acquisition and
                           management. A 3-hour canoe trip allows study of the floodplain swamp habitat. Field
                           work will vary with weather conditions, with some indoor class work included. This
                           course will not be considered for Science General Education credit.
                        </p>
                        					
                        <p>BSC 1084. ESSENTIALS OF HUMAN STRUCTURE AND FUNCTION.</p>
                        
                        					
                        <p>Prerequisites: EMT program completion and Emergency Medical Services (EMS) department
                           approval Comprehensive course presenting basic information on the structure and function
                           of the human body. Applies principles of anatomy and physiology to show interaction
                           of body systems as they maintain homeostasis. Each body system is presented with emphasis
                           on the cardiovascular, respiratory and nervous systems. Course must be completed with
                           a C or better to continue in the Paramedic Program.
                        </p>
                        					
                        <p>BSC 1777. BIOLOGY LINC'D COURSES.</p>
                        
                        					
                        <p>Course created for internal purposes ony. Course is being used to create a section
                           for two LINC'd courses - <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005');" title="BSCÂ&nbsp;1005">BSC 1005</a> and <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1005L');" title="BSCÂ&nbsp;1005L">BSC 1005L</a>. (Special Fee: $35.00)..
                        </p>
                        					
                        <p>BSC 2062. EVERLGLADES ECOLOGY.</p>
                        
                        					
                        <p>A non- lab science course designed to introduce students to the history, biology,
                           climatology, ecology, geology, hydrology, biodiversity, evolution and restoration
                           of the Everglades. Topics include: the Greater Everglades watershed, natural communities,
                           flora and fauna interactions, ecological niches, hydro-pattern modification and its
                           impact on the Everglades, exotic and invasive species, endangered species, nutrient
                           loads and cycles, habitat alteration, protected areas, and the Comprehensive Everglades
                           Restoration Plan. This course will not be considered for Science General Education
                           credit.
                        </p>
                        					
                        <p>BSC 2093C. HUMAN ANATOMY AND PHYSIOLOGY I.</p>
                        
                        					
                        <p>Prerequisite: Satisfactory completion of all mandated courses in reading, mathematics,
                           English, and English for Academic Purposes and a minimum grade of C in: Honors high
                           school biology or AP biology and Honors high school chemistry or AP chemistry; or
                           <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1010C');" title="BSCÂ&nbsp;1010C">BSC 1010C</a>. Tissues, structure and function of integumentary, skeletal, muscular, endocrine
                           and nervous systems, and organs of special sense. Lab exercises emphasize anatomic
                           and physiologic principles associated with classroom work. (Special Fee: $62.00).
                        </p>
                        					
                        <p>BSC 2094C. HUMAN ANATOMY AND PHYSIOLOGY II.</p>
                        
                        					
                        <p>Prerequisite: Minimum grade of C in BSC2093C or department approval Continuation of
                           <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 2093C');" title="BSCÂ&nbsp;2093C">BSC 2093C</a>, including circulatory, cardiovascular, lymphatic, respiratory, digestive, urinary
                           and reproductive systems with considerable emphasis on biochemistry of metabolic processes
                           and body fluids. (Special Fee: $62.00).
                        </p>
                        					
                        <p>BSC 2366. NEOTROPICAL ECOLOGY.</p>
                        
                        					
                        <p>Prerequisite or Corequisite: <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'PCB 2350');" title="PCBÂ&nbsp;2350">PCB 2350</a> This course emphasizes observations of ecological relationships of plants and animals
                           of the New World Tropics and associated habitats. Onsite studies in neotropical locations
                           (Costa Rica, Belize, Amazonia, etc.) will emphasize basic ecological concepts, such
                           as the niche concept, niche portioning, symbiosis, competition, trophic structure,
                           evolutionary relationships, co-evolution, and predation. In addition, this course
                           will emphasize identification of representative forms of life of various tropical
                           ecosystems in the study country located within the Neotropics. Both natural and artificial
                           biological comunities will be visited. Special travel fees apply. A valid passport
                           is required prior to travel. This course will not be considered for Science General
                           Education credit.
                        </p>
                        					
                        <p>BSC 2933. SELECTED TOPICS IN BIOLOGY.</p>
                        
                        					
                        <p>Prerequisite: Departmental approval Selected topics in biological sciences based on
                           historical, traditional or contemporary approach as background and interest of students
                           and professor dictate. Multiple credit course. May be repeated for credit, but grade
                           forgiveness cannot be applied.
                        </p>
                        					
                        <p>BSC 2941. INTERNSHIP EXPLORATION IN BIOLOGY.</p>
                        
                        					
                        <p>Prerequisites: Satisfactory completion of all mandated courses in Reading, Mathematics,
                           English, and English for Academic Purposes; a minimum 2.0 institutional or overall
                           GPA; and 12 credits, including <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/bsc/#" onclick="return showCourse(this, 'BSC 1010C');" title="BSCÂ&nbsp;1010C">BSC 1010C</a>. The Program Director/Program Chair/Program Coordinator or Internship Placement Office
                           has the discretion to provide override approval as it relates to the waiver of required
                           program/discipline-related courses. This course is a planned work-based experience
                           that provides students with supervised career exploration activities and/ or practical
                           experiences. Each earned credit of internship requires a minimum of 80 clock hours
                           of work. Multiple credit course. May be repeated for credit, but grade forgiveness
                           cannot be applied. (Internship Fee: $10.00).
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/biology.pcf">©</a>
      </div>
   </body>
</html>