<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Science Faculty | Division of Science  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science-west/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Division of Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science-west/">Science on West</a></li>
               <li>Science Faculty </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>Faculty Contact Information</h2>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              <tr> 
                                 							
                                 <td><b>NAME</b></td>
                                 							
                                 <td> 
                                    <p><b>EXTENSION</b> 
                                       								<br>
                                       								407-299-5000
                                    </p>
                                 </td>
                                 							
                                 <td> 
                                    <p><b>OFFICE</b> 
                                    </p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr> 
                                 								
                                 <td>Mary Beck (Earth Science)</td>
                                 								
                                 <td> 
                                    <div>1882</div>
                                 </td>
                                 								
                                 <td>
                                    <div>1-242C</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Betsy Brantley (Biology) </td>
                                 								
                                 <td>
                                    <div>5750</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-138 </div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Lois Crichlow (Biology) </td>
                                 								
                                 <td>
                                    <div>1204</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-323</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr> 
                                 								
                                 <td>Dr. Eric Crumpler (Chemistry) </td>
                                 								
                                 <td>
                                    <div>5610 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-205 </div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>David Curtis (Biology) </td>
                                 								
                                 <td>
                                    <div>5733</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-138A</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Bryan Derrickson (Biology)</td>
                                 								
                                 <td>
                                    <div>1196  </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-234 </div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Diego Diaz (Chemistry)</td>
                                 								
                                 <td>
                                    <div>1718</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-138C</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Javier Garces (Horticulture)</td>
                                 								
                                 <td>
                                    <div>1820</div>
                                 </td>
                                 								
                                 <td>
                                    <div>1-136</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Robert Gessner (Biology)</td>
                                 								
                                 <td>
                                    <div>1321 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-231B</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Barbara Gutierrez (Biology) </td>
                                 								
                                 <td>
                                    <div>1941</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-122</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Richard Jerousek (Astronomy) </td>
                                 								
                                 <td>
                                    <div>5666</div>
                                 </td>
                                 								
                                 <td>
                                    <div>2-231</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Jeffrey Kaesberg (Biology) </td>
                                 								
                                 <td>
                                    <div>1586</div>
                                 </td>
                                 								
                                 <td>
                                    <div>1-137</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Kamal Kamal (Biology)</td>
                                 								
                                 <td> 
                                    <div>1257</div>
                                 </td>
                                 								
                                 <td> 
                                    <div>HSB-121</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Shahnaz Kanani (Biology) </td>
                                 								
                                 <td>
                                    <div>5895</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-138D</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Wafaa Khattou (Physics)</td>
                                 								
                                 <td>
                                    <div>1950</div>
                                 </td>
                                 								
                                 <td>
                                    <div>2-233</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Kimberly Lewis (Biology) </td>
                                 								
                                 <td>
                                    <div>1809</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-324</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. A. Graeme Lindbeck (Biology)</td>
                                 								
                                 <td>
                                    <div>1256 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-217</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Jackie Lindbeck (Biology)</td>
                                 								
                                 <td>
                                    <div>1961 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>1-223 </div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Marc LoCascio (Biology)</td>
                                 								
                                 <td>
                                    <div>5516 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-216</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Susan Matthews (Biology)</td>
                                 								
                                 <td>
                                    <div>1384</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-215</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Anthony Mellone (Chemistry)</td>
                                 								
                                 <td>
                                    <div>1438</div>
                                 </td>
                                 								
                                 <td>
                                    <div>3-219</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Kelly Moore (Biology)</td>
                                 								
                                 <td>
                                    <div>1197</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-119</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Gustavo Morales (Earth Science)</td>
                                 								
                                 <td>
                                    <div>1428</div>
                                 </td>
                                 								
                                 <td>
                                    <div>1-221</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Eileen Perez (Chemistry)</td>
                                 								
                                 <td>
                                    <div>1236</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-207</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Brenda Schumpert (Biology)</td>
                                 								
                                 <td>
                                    <div>1232</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-219</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Laura Sessions (Chemistry) </td>
                                 								
                                 <td>
                                    <div>5631 </div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-211</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Irina Struganova (Physics) </td>
                                 								
                                 <td>
                                    <div>1947</div>
                                 </td>
                                 								
                                 <td>
                                    <div>2-230</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Andrea Tice (Chemistry)</td>
                                 								
                                 <td>
                                    <div>1877</div>
                                 </td>
                                 								
                                 <td>
                                    <div>HSB-138B</div>
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                        </table>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              <tr>
                                 							
                                 <td colspan="3">
                                    <h3><strong>LAB MANAGERS</strong></h3>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr> 
                                 								
                                 <td><b>NAME</b></td>
                                 								
                                 <td> 
                                    <p><b>EXTENSION</b> 
                                       									
                                    </p>
                                 </td>
                                 								
                                 <td> 
                                    <p><b>OFFICE</b> 
                                    </p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Wanda Davila Aponte (Chemistry)</td>
                                 								
                                 <td>
                                    <div>1435</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-212</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Shivani Persaud (Biology)</td>
                                 								
                                 <td>
                                    <div>1927</div>
                                 </td>
                                 								
                                 <td>
                                    <div>AHS-325</div>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Bill Stillwell (Physics)</td>
                                 								
                                 <td>
                                    <div>1767</div>
                                 </td>
                                 								
                                 <td>
                                    <div>2-209</div>
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                        </table>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              <tr>
                                 							
                                 <td><strong>PART TIME FACULTY</strong></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Mahreen Ahmed (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Adele Balmer (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Stephanie Bledsoe (Horticulture)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Robert Borders (Chemistry)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Robert Bowden (Horticulture) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Eric Brown (Horticulture)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Tandace Burkhart (Biology) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Carmen CardenasVasquez (Biology) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>John Centko (Horticulture)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Roy Coleman (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Stephen Cramer (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Mohammed Daoudi (Chemistry)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Angela Dean (Biology) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Marcien Dentey (Physics) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Jose Diaz (Biology) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Gale Fair (Physics)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>David Feller (Meterology, Oceanography)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Barton Galloway (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Kassu Gebreyes (Chemistry)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Marlene Gillies (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Russell Gminder (Astronomy)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Norman Greer (Horticulture)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Ronald Keiper (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Ted Klenk (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. David Lamb (Physics)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Kim Mancas (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Edward Meyers (Earth Science)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Jennifer Odom (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Nalini Odapalli (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Ivan Padron (Physics)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>John Perrone (Horticulture)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Thomas Ravenscroft (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Juan Rivera (Astronomy)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Daniel Rubin (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Yasser Saad (Biology) </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Casey Schwarz (Physics)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Uma Singh (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Anatoly Sobolveskiy (Chemistry)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Vikas Sudesh (Physics)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Robert Weigle (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Lynda Wilson (Biology/EMT)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Dr. Richard Wright (Biology)</td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Nader Zikra (Chemistry) </td>
                                 							
                              </tr>
                              						
                           </tbody>
                        </table>
                        
                        
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science-west/faculty.pcf">©</a>
      </div>
   </body>
</html>