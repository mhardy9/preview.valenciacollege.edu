<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office Administration | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/office/articulation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Office Administration</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li><a href="/academics/departments/business-east/office/">Office</a></li>
               <li>Office Administration</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Articulation Agreements</h2>
                        
                        <p>      <span>Get college credit for programs taken at a Tech Center!</span><br>
                           Valencia College, Orange County Technical Centers, and
                           the Technical Education Center of Osceola (TECO) have teamed up to offer
                           you college credit for the programs you complete at any Orange County
                           Tech Center (Mid-Florida Tech, Orlando Tech, Winter Park Tech, or Westside
                           Tech) or TECO. 
                        </p>
                        
                        <p><strong>Save Time and Money</strong><br>
                           Save time and money by starting your career at a Tech Center and
                           then successfully completing your courses at Valencia. You’ll be
                           on the fast track to gaining the technical skills you need to land a great
                           job with great pay.
                        </p>
                        
                        <p><strong>Here’s How it Works</strong></p>
                        
                        <ol>
                           
                           <li>Complete one of the Tech Center programs identified below </li>
                           
                           <li>Enroll at
                              Valencia in the linked program identified below 
                           </li>
                           
                           <li>Earn college
                              credits for the Tech Center program completed and save money    
                           </li>
                           
                           <li>Earn a Valencia Technical Certificate or A.S. degree in a shorter period
                              of time 
                           </li>
                           
                           <li>Get a great-paying job</li>
                           
                        </ol>
                        
                        <p>Articulation Agreements with Orange Technical Education Centers
                           (Mid Florida Tech, Orlando Tech, Westside Tech, and Winter
                           Park Tech)
                        </p>
                        <p> In a continuing effort to provide career ladder opportunities for
                           students in technical education programs, Valencia Community
                           College agrees to extend at no cost to the eligible students (other than
                           the application for admission fee) full college credit who have
                           completed the following tech center programs. 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Articulation Agreements </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><a href="../../../asdegrees/credit_octc.html">Credits From Orange Technical Education Centers and Technical Education Center of
                                       Osceola - Valencia College</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><a href="../../../asdegrees/credit_alternative.html">Articultation Agreements - Industry Certifcations</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                                  
                        
                        <blockquote>
                           
                           
                        </blockquote>
                        <p>  <a href="articulation.html#top">TOP</a>
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/office/articulation.pcf">©</a>
      </div>
   </body>
</html>