<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office Administration | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/office/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Office Administration</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Office</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2> Office &amp; Medical Office Administration</h2>
                        
                        <p>Thank you for your interest in Valencia College’s Associate
                           in Science Degree in
                           
                           
                           
                           Office and Medical Office Administration. 
                        </p>
                        
                        <p> Valencia offers day and evening classes for each of the two Associate
                           in Science Degrees. 
                        </p>
                        
                        <p> If you need assistance in creating an educational plan or program advising, please
                           contact your campus specific Career Program Advisor.
                        </p>
                        
                        <p>To locate the Advisor for your Career Program visit <a href="../../../associate-in-science-students/index.html">valenciacollege.edu/helpas</a>. <br>
                           
                        </p>
                        
                        <p>If you are not a current Valencia student visit the <a href="http://preview.valenciacollege.edu/future-students/">Future Students</a> menu above to request more information and/or schedule a campus visit. 
                        </p>            
                        
                        <p>Again, thank you for your interest in Valencia’s Medical and Office Administration
                           Program. The best to you in your preparation for a career in this area
                        </p>
                        
                        <ul>
                           
                           <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/office-administration/">Office Administration A.S. Degree</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/business/moa.cfm">Medical 
                                 Office Administration A.S. Degree</a></li>
                           
                           <li>                <a href="../waiverandcreditbyexaminformation.html">Waiver &amp; Credit by Exam Information</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Organizations</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.ahdionline.org/">Association for Healthcare Documentation </a></li>
                           
                           <li><a href="http://www.ahdi-fl.org">Association for Healthcare Documentation Integrity - Florida </a></li>
                           
                        </ul>
                        
                        
                        <h3>Program Chair-East Campus</h3>
                        
                        <p><strong>Office Administration </strong> 
                        </p>
                        
                        <p><a href="mailto:bwanielista@valenciacollege.edu">Betty Wanielista</a><br>
                           407-582-2347<br>
                           Building 8 Room 159
                        </p>
                        
                        <p><br>
                           <br>
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/office/index.pcf">©</a>
      </div>
   </body>
</html>