<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Paralegal Studies | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/paralegal/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/paralegal/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Paralegal Studies</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Paralegal</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        <h2>East Campus</h2>
                        
                        <p>Thank you for your interest in Valencia College's 
                           Associate in Science Degree in Paralegal Studies. Our program is 
                           approved by the American Bar Association. 
                        </p>
                        
                        <p>Valencia College 's Paralegal Studies Program is one 
                           of only nine paralegal studies programs in Florida approved by the 
                           American Bar Association. It offers an Associate in Science degree 
                           designed to prepare students to work in the legal field for law 
                           firms, banks, corporations, and government agencies in the performance 
                           of delegated, substantive legal services under the supervision of 
                           a licensed attorney. The program stresses practical applications 
                           in areas such as civil litigation, real property, legal research 
                           and writing, business organizations, and legal technology. Students 
                           will understand the ethical framework within which they will work 
                           and be able to effectively analyze and communicate in these areas. 
                           
                        </p>
                        
                        <p>The services paralegals provide include assisting in trial preparation, 
                           drafting court pleadings and related documents, performing legal 
                           research and informal investigations, preparing real estate documents 
                           and assisting in real estate closings, drafting contracts, and maintaining 
                           corporate books. Paralegals cannot give legal advice, accept a case, 
                           set a fee, or present a case in court, and may not provide legal 
                           services directly to the public, except as permitted by law. 
                        </p>
                        
                        <p><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/paralegal-studies/">Program  Details, Career Outlook, FAQs and Request More Information</a></p>
                        
                        <p>If you would like to register, please contact our Paralegal Studies 
                           Academic Advisor, <br>
                           information is located at <a href="../../../associate-in-science-students/index.html">valenciacollege.edu/helpas</a>. Please check the <a href="../../../students/catalog/index.html">catalog</a>  to ensure you register in time for next term.
                        </p>
                        
                        <p>If you are not a current Valencia student visit the "Future Students" menu on <a href="../../../index.html">valenciacollege.edu</a> to request more information and/or schedule a campus visit. 
                        </p>
                        
                        <p>Again, thank you for your interest in Valencia's Paralegal 
                           Studies Program. Good luck as you prepare to make some very important 
                           decisions regarding your career.<br>
                           
                        </p>
                        
                        <h3>Faculty </h3>
                        
                        <p><a href="mailto:wtoscano@valenciacollege.edu">Wendy Toscano</a><br>
                           407-582-2529<br>
                           Building 6 Room 228
                        </p>
                        
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/paralegal/index.pcf">©</a>
      </div>
   </body>
</html>