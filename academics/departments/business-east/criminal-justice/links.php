<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/criminal-justice/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/criminal-justice/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li><a href="/academics/departments/business-east/criminal-justice/">Criminal Justice</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>    
                        <h2>Criminal
                           Justice Links
                        </h2>
                        
                        <p>Police</p>
                        
                        <p>Local Florida Police Agencies<br>
                           <a href="http://www.fpca.com/stagencies.htm" target="_blank">http://www.fpca.com/stagencies.htm</a></p>
                        
                        <p>FDLE <br>
                           <a href="http://www.fdle.state.fl.us" target="_blank">http://www.fdle.state.fl.us</a></p>
                        
                        <p>ATF<br>
                           <a href="http://www.atf.gov" target="_blank">http://www.atf.gov</a></p>
                        
                        <p>FBI<br>
                           <a href="http://www.fbi.gov" target="_blank">http://www.fbi.gov</a></p>
                        
                        <p>DEA<br>
                           <a href="http://www.dea.gov" target="_blank">http://www.dea.gov</a></p>
                        
                        <p>Courts</p>
                        
                        <p>Florida Court System<br>
                           <a href="http://www.flcourts.org" target="_blank">http://www.flcourts.org</a></p>
                        
                        <p>U.S. Federal Courts<br>
                           <a href="http://www.uscourts.gov/links.html" target="_blank">http://www.uscourts.gov/links.html</a></p>
                        
                        <p>U.S. Supreme Court<br>
                           <a href="http://www.supremecourtus.gov" target="_blank">http://www.supremecourtus.gov</a></p>
                        
                        <p>Corrections</p>
                        
                        <p>Orange County Corrections<br>
                           <a href="http://www.orangecountyfl.net/dept/correct" target="_blank">http://www.orangecountyfl.net/dept/correct</a></p>
                        
                        <p>Florida Department of Corrections<br>
                           <a href="http://www.dc.state.fl.us" target="_blank">http://www.dc.state.fl.us</a></p>
                        
                        <p>Federal Bureau of Prisons<br>
                           <a href="http://www.bop.gov" target="_blank">http://www.bop.gov</a><br>
                           
                        </p>
                        
                        <div>
                           <a href="links.html#top">TOP</a> 
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/criminal-justice/links.pcf">©</a>
      </div>
   </body>
</html>