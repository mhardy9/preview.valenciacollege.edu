<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/criminal-justice/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/criminal-justice/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <p>The safety, order and freedom of our democratic society depend 
                           on our criminal justice professionals. These men and women spend 
                           a majority of their time providing a wide range of services to their 
                           communities. In today’s complex and diverse society, communities 
                           demand criminal justice professionals that are broadly educated 
                           and highly trained. Valencia’s Criminal Justice 
                           A. S. degree program provides students with a high quality education 
                           preparing them for challenging careers in law enforcement, corrections, 
                           private and industrial security and other areas in the criminal 
                           justice field. 
                        </p>
                        
                        <p><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/criminal-justice/">Program  Details, Career Outlook, FAQs and Request More Information</a></p>
                        
                        <p><a href="../../../public-safety/criminal-justice-institute/index.html"> Criminal Justice Programs</a></p>
                        
                        <p>Click here to view some of our <a href="studentrecognitionpage.html">"Students' Educational Experiences in Criminal Justice"</a></p>
                        
                        <h3>Program Chair - East Campus</h3>
                        
                        <p><strong> Criminal Justice</strong></p>
                        
                        <p>Brian Murphy <br>
                           <a href="mailto:jmurphy60@valenciacollege.edu">JMurphy60@valenciacollege.edu</a> <br>
                           407-582-2381            
                        </p>
                        
                        
                        <blockquote>&nbsp;</blockquote>
                        
                        <blockquote>
                           
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/criminal-justice/index.pcf">©</a>
      </div>
   </body>
</html>