<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/criminal-justice/studentrecognitionpage.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/criminal-justice/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Criminal Justice</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li><a href="/academics/departments/business-east/criminal-justice/">Criminal Justice</a></li>
               <li>Criminal Justice</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2><em>Students' Educational Experiences in Criminal Justice at Valencia College</em></h2>
                        
                        <p><em><strong><font color="#336600">Joe Nguyen</font></strong></em><span><em><img alt="CJT student Joe N." height="132" hspace="4" src="../../east/business/criminal-justice/JoeN_B.jpg" vspace="6" width="152"></em></span></p>
                        
                        <p>Valencia has been a great learning institution for me! Not only 
                           has the Criminal Justice Program provided me with an 
                           opportunity to learn about how the criminal justice system operates, 
                           I also have been able to explore what my career path in criminal 
                           justice will be like. By being provided with a wide range of professors 
                           that possess practical experience in the "system", I have been able 
                           to gain personal insight into what my future holds as a future criminal 
                           justice practitioner. Soon I'll be graduating with a 3.5 GPA and 
                           just want to say THANKS Valencia , I am now ready to pursue an exciting 
                           career in criminal justice. 
                        </p>
                        
                        <p><em><strong><font color="#336600">C<font color="#333300">r</font>ystal 
                                    Algea</font></strong></em><span><em><strong><font color="#336600"> 
                                       </font></strong></em><img alt="CJT student Crystal" height="159" hspace="4" src="../../east/business/criminal-justice/CrystalB.jpg" vspace="4" width="176"><br>
                              <br>
                              </span>Hi! My name is Crystal and I have attended Valencia Community 
                           College for the past two years and will be graduating soon. I had 
                           somewhat of a rough start at Valencia but with the help, guidance 
                           and dedication of faculty and staff I will be graduating with a 
                           3.66 GPA and have managed to make the Dean's Honor List two semesters 
                           in row. Upon completing my A.S. Degree in Criminal Justice, I 
                           plan on attending the Henry Lee College of Criminal Justice and 
                           Forensic Science at the University of New Haven, Connecticut. Overall, 
                           Valencia has been a great experience for me. The professors have 
                           been wonderful and have challenged me to think outside of the box. 
                           I really think coming to Valencia was one the best decisions I made 
                           in my lifetime and look forward to the challenges I face ahead. 
                           THANKS VALENCIA ! 
                        </p>
                        
                        <p><span><em><strong><font color="#336600">Jonathan Curzon, 
                                       selected for the Program Chairs "Most Promising CJT Student" 
                                       award for Fall 2007. </font></strong></em><img alt="CJT student Johathon" height="148" hspace="4" src="../../east/business/criminal-justice/JonathonB.jpg" vspace="6" width="143"><br>
                              <br>
                              </span>My name is Jonathon Curzon and I am a transfer student who 
                           was attracted to the Articulated A.S. to B.S. Degree in Criminal 
                           Justice. My experience in the Criminal Justice Program 
                           has been very positive and I have been very impressed with the real 
                           life experience and expertise the professors bring to the classroom. 
                           In addition to offering an excellent blend of theory and practical 
                           experience in the classroom, faculty at Valencia have been very 
                           accessible and take time to provide excellent learning opportunities 
                           for students. Soon, I'll be graduating with my A.S. degree and will 
                           finish with a 3.8 GPA. My goal is to work as a federal law enforcement 
                           officer and Valencia has helped me reach my first milestone towards 
                           my career goal. Thanks Valencia for providing an interesting and 
                           challenging education. 
                        </p>
                        
                        <p><a href="studentrecognitionpage.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/criminal-justice/studentrecognitionpage.pcf">©</a>
      </div>
   </body>
</html>