<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business, IT &amp; Public Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/waiverandcreditbyexaminformation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business, IT &amp; Public Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Business, IT &amp; Public Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Credit by Exam Information </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Office &amp; Medical Office Administration Technology </h3>
                                    
                                    <h6><strong>Courses assessed via Credit-by-Examination:</strong></h6>
                                    
                                    <ul>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1257C</a> - Medical Terminology for the Office, 3 credits
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1467C</a> - Introduction to Body Systems for OST, 3 credits 
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1100C</a> - Keyboarding &amp; Document Processing I, 3 credits 
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1110C</a> - Keyboarding &amp; Document Processing II, 3 credits 
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1141C</a> - Computer Keyboarding, 1 credit
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ost/">OST1611C</a> - Medical Transcription, 3 credits
                                          </h6>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    
                                    <h6>A different campus offers the Credit-by-Examination each term: </h6>
                                    
                                    
                                    
                                    <h6>East Campus in Fall in November</h6>
                                    
                                    <p>Contact: Betty Wanielista<br>
                                       407-582-2347<br>
                                       <a href="mailto:bwanielista@valenciacollege.edu">bwanielista@valenciacollege.edu</a></p>
                                    
                                    <h6>West Campus in Spring in March</h6>
                                    
                                    <p> Contact: Marie Howard<br>
                                       407-582-1423<br>
                                       <a href="mailto:mhoward@valenciacollege.edu">mhoward@valenciacollege.edu</a> 
                                    </p>
                                    
                                    <h6>Osceola Campus in Summer in June&nbsp;</h6>
                                    
                                    <p> Contact: Coleen Jones<br>
                                       407-582-4851<br>
                                       <a href="mailto:cjones3@valenciacollege.edu">cjones3@valenciacollege.edu</a> 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>                                  
                                          
                                          <h6>
                                             <a href="documents/oststudyguide_3-16-16.pdf" target="_blank">OST Study Guide</a> 
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <h6>
                                             <a href="documents/ostcreditbyexamapplication_b.pdf" target="_blank">Credit by Exam OST Application</a>                                  
                                          </h6>
                                          
                                       </li>
                                       
                                       <li>                                  
                                          
                                          <h6>
                                             <a href="documents/credit-by-examination-cbe-east-nov-2017.pdf" target="_blank">Credit by Exam Dates FALL 2017 </a> 
                                          </h6>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <div>
                                       <strong>There is a fee for this exam.</strong>
                                       
                                    </div>                  
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="waiverandcreditbyexaminformation.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/waiverandcreditbyexaminformation.pcf">©</a>
      </div>
   </body>
</html>