<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business, IT &amp; Public Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/it/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/it/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business, IT &amp; Public Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>It</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        <h2>East Campus</h2>
                        
                        <p>Information Technology, or IT, is the language of today's high-tech world.
                           At one time, only very large companies needed an IT department or expert.
                           But in today's information-driven economy, all companies need someone who
                           can keep their computer systems running smoothly and their technology working
                           efficiently. Today's IT workers are working in virtually every industry,
                           including banking, telecommunications, education, retail, law enforcement
                           and healthcare. 
                        </p>
                        
                        <p>Valencia College offers three A. S. Degrees and seven 
                           Technical Certificate programs designed to give our students the 
                           latest IT skills. For the most current degree and certificate information, 
                           visit <a href="../../../asdegrees/index.html">valenciacollege.edu/asdegrees.</a></p>
                        
                        <p> <font color="#333333">Click on the program name below to view the Program 
                              Planning  and Suggested Course Sequence Guides. If you have further questions about
                              course content or program outcomes, please contact the program 
                              director or lead instructor.</font></p>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>
                                    <div> 
                                       
                                       <h2><strong><font color="#cc0000">Program Guides and Contacts</font></strong></h2>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p><strong><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/computer-programming-and-analysis/">Computer Information Technology -  AS Degree &amp; Certificate Programs</a><br>
                                          <br>
                                          </strong>Dave Brunick, Program Chair <br>
                                       East Campus<br>
                                       407-582- 2360 <br>
                                       <a href="mailto:DBrunick@valenciacollege.edu%20">DBrunick@valenciacollege.edu</a><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><font color="#333333"><strong><a href="http://preview.valenciacollege.edu/asdegrees/information-technology/cpa.cfm">Computer Programming &amp; Analysis -AS Degree &amp; Certificate Programs</a><br>
                                             <br>
                                             </strong></font>Dennis Hunchuck, Lead Instructor<br>
                                       407-582-2695<br>
                                       <a href="mailto:DHunchuck@valenciacollege.edu">DHunchuck@valenciacollege.edu</a><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><u><strong><font color="#333333"><a href="http://preview.valenciacollege.edu/asdegrees/engineering/ddt.cfm">*Technical  Certificate</a></font></strong></u></p>
                                    
                                    <ul>
                                       
                                       <li><strong>Drafting - AutoCAD</strong></li>
                                       
                                       <li><strong>Rapid Prototyping Specialist</strong></li>
                                       
                                    </ul>                    
                                    
                                    <p>Irma Berner, Lead Instructor East Campus<br>
                                       407-582-2627 (East Campus) <br>
                                       <a href="mailto:Iberner@valenicacc.edu">IBerner@valenciacollege.edu</a></p>
                                    
                                    <p>Andrew Ray, Program Chair<br>
                                       407-582-1847 (West Campus)<br>
                                       <a href="mailto:ARay@valenciacollege.edu">ARay@valenciacollege.edu</a> <br>
                                       <a href="../../../west/engineering/index.html">Engineering Technology - Valencia College</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><em>*The associated degree, AS-Drafting &amp; Design Technology is based on West Campus, most
                                       of the techncial courses must be completed on that campus. See<a href="http://preview.valenciacollege.edu/schedule/"> Courses Offered</a> to find classes offered on other campus.</em></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    <h4>For program advising and degree completion, please contact the Career Program Adviors
                                       on your Campus: 
                                    </h4>
                                    
                                    <p>To locate the Advisor for your Career Program visit <a href="../../../associate-in-science-students/index.html">valenciacollege.edu/helpas</a>. <br>
                                       
                                    </p>
                                    
                                    <p>If you are not a current Valencia student visit the "Future Students" menu to request
                                       more information and/or schedule a campus visit. 
                                    </p>                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Department Information</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div><strong> Dean: </strong></div>
                                 
                                 <div>
                                    
                                    <p><strong>Carin Gordon </strong><br>
                                       407-582-2807<br>
                                       <a href="mailto:CGordon15@valenciacollege.edu">CGordon15@valenciacollege.edu</a>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p>Department 
                                       Office: 8-105<br>
                                       Mail Code 3-25<br>
                                       <br>
                                       701 N. Econlockhatchee Trail<br>
                                       Orlando, FL 32825
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <div><strong>Assistant to the 
                                          Dean:</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    <strong>Stephanie Hines</strong> <br>
                                    407-582-2958<br>
                                    <a href="mailto:shines10@valenciacollege.edu">SHines10@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Assistant to the Dean:</strong></div>
                                 
                                 <div>
                                    <strong>Donna (Suzie) Gant</strong><br>
                                    407-582-2807 <br>
                                    <a href="mailto:dgant1@valenciacollege.edu">DGant1@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <ul>
                           
                           <li><a href="../../../map/east.html">Campus Map/Driving
                                 directions</a></li>
                           
                        </ul>
                        
                        <p><a href="index.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/it/index.pcf">©</a>
      </div>
   </body>
</html>