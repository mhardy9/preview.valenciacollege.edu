<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business, IT &amp; Public Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business, IT &amp; Public Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Business, IT &amp; Public Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>Full Time Faculty</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <h3>East Campus Faculty Contact Information </h3>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Accounting Technology</div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program  Chair</strong><br>
                                    Laurie Larson<br>
                                    (407) 582-2513<br>
                                    Office: 7-148<br>
                                    <a href="mailto:llarson@valenciacollege.edu">LLarson@valenciacollege.edu </a><br>
                                    
                                 </div>
                                 
                                 <div>Tom Baselice <br>
                                    (407) 582-2522<br>
                                    Office: 7-174<br>
                                    <a href="mailto:TBaselice@valenciacollege.edu">TBaselice@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>Cecil Battiste<br>
                                    (407) 582-2508<br>
                                    Office: 7-157 <br>
                                    <a href="mailto:cbattiste@valenciacollege.edu">CBattiste@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program  Chair</strong><br>
                                    Patti Lopez<br>
                                    (407) 582-2518<br>
                                    Office: 7-154<br><a href="mailto:plopez@valenciacollege.edu">PLopez@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>Walter Martin<br>
                                    (407) 582-2849<br>
                                    Office: 7-145<br>
                                    <a href="mailto:wmartin@valenciacollege.edu">WMartin@valenciacollege.edu </a>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    <div>Business Administration</div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program Chair</strong><br>
                                    Storm Russo<br>
                                    (407) 582-2348<br>
                                    Office: 8-256<br>
                                    <a href="mailto:srusso@valenciacollege.edu%20">SRusso@valenciacollege.edu 
                                       </a><br>
                                    <a href="http://faculty.valenciacollege.edu/srusso/">Web Site</a><br>
                                    
                                 </div>
                                 
                                 <div>Lee McCain <br>
                                    (407) 582- 2489 <br>
                                    Office: 8-138<br>
                                    <a href="mailto:lmccain@valenciacollege.edu">LMccain@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>Taiwan  Huggins-Jordan<br>
                                    407-482-2386<br>
                                    Office 8-139<br>
                                    <a href="mailto:thugginsjordan@valenciacollege.edu">thugginsjordan@valenciacollege.edu</a><br>                  
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Office Adminisration and Medical Office</div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program Chair</strong><br>
                                    Betty Wanielista<br>
                                    (407) 582-2347<br>
                                    Office: 8-159<br>
                                    <a href="mailto:bwanielista@valenciacollege.edu">BWanielista@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Criminal Justice </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program Chair</strong><br>
                                    Brian Murphy<br>
                                    407-582-2381<br><a href="mailto:jmurphy60@valenciacollege.edu">JMurphy60@valenciacollege.edu</a> <br>                  
                                 </div>
                                 
                                 <div>Terry Miller<br>
                                    (407) 582-2749<br>
                                    Office: 8-158<br><a href="mailto:tmiller@valenciacollege.edu%20">TMiller@valenciacollege.edu</a><br>                  
                                 </div>
                                 
                                 <div>Susan Yawn<br>
                                    (407) 582-2173<br>
                                    Office: 8-140<br>
                                    <a href="mailto:SYawn@valenciacollege.edu">SYawn@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Drafting</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Irma Berner<br>
                                    (407) 582-2807<br>
                                    Office: 6-229<br>
                                    <a href="mailto:iberner@valenciacollege.edu">IBerner@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Nutrition</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>April Engel<br>
                                    (407) 582- 2069<br>
                                    Office: 2-114<br>
                                    <a href="mailto:AEngel@valenciacollege.edu">AEngel@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Paralegal Studies </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p><strong>Program Chair<br>
                                          </strong>Wendy Toscano<br>
                                       (407) 582-2529<br>
                                       Office: 6-228<br>
                                       <a href="mailto:wtoscano@valenciacollege.edu">WToscano@valenciacollege.edu</a>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <br>
                                    Cathy Mestre<br>
                                    (407) 582-2514<br>
                                    Office: 6-223 <br>
                                    <a href="mailto:cmestre@valenciacollege.edu">CMestre@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Information Technology, Computer Programming &amp; Analysis</div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Program </strong><strong>Chair</strong><strong><br>
                                       </strong>Dave Brunick<br>
                                    (407) 582-2360<br>
                                    Office: 2-112<br>
                                    <a href="mailto:dbrunick@valenciacollege.edu">DBrunick@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>                  Heith Hennel<br>
                                    (407) 582-2629<br>
                                    Office: 8-254<br>
                                    <a href="mailto:hhennel@valenciacollege.edu">HHennel@valenciacollege.edu</a> 
                                 </div>
                                 
                                 <div>                  Gerald Hensel<br>
                                    (407) 582-2509<br>
                                    Office: 7-147<br>
                                    <a href="mailto:jhensel@valenciacollege.edu">GHensel@valenciacollege.edu</a> 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>Dennis Hunchuck <br>
                                    (407) 582-2695<br>
                                    Office: 1-3611<br><a href="mailto:DHunchuck@valenciacollege.edu">DHunchuck@valenciacollege.edu</a>                  
                                 </div>
                                 
                                 <div>Sanika Paranjape<br>
                                    (407) 582- 2351<br>
                                    Office: 2-309 <br>
                                    <a href="mailto:SParanjape@valenciacollege.edu">SParanjape@valenciacollege.edu</a> 
                                 </div>
                                 
                                 <div>Reina Reynolds<br>
                                    (407) 582-2104<br>
                                    Office: 2-308<br>
                                    <a href="mailto:rreynolds@valenciacollege.edu">RReynolds@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Reneva Walker<br>
                                    (407) 582-2344<br>
                                    Office: 4-231<br>
                                    <a href="mailto:rwalker@valenciacollege.edu">RWalker@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>Deymond Hoyte (Network Engineering Technology) <br>
                                    (407) 582- 2118<br>
                                    Office: 8-253<br>
                                    <a href="mailto:dhoyte@valenciacollege.edu">DHoyte@valenciacollege.edu</a>
                                    
                                 </div> 
                                 
                                 <div>
                                    
                                    <p>Irma Berner (Drafting &amp; Design Techology) <br>
                                       (407) 582-2807<br>
                                       Office: 6-229<br>
                                       <a href="mailto:iberner@valenciacollege.edu">IBerner@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="faculty.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/faculty.pcf">©</a>
      </div>
   </body>
</html>