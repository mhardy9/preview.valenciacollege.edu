<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business, IT &amp; Public Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-east/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business, IT &amp; Public Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-east/">Business East</a></li>
               <li>Business, IT &amp; Public Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a> 
                        
                        
                        
                        
                        <h2>Mission Statement</h2>
                        
                        <div>
                           <iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/likebox.php?id=119607908069841&amp;width=250&amp;connections=0&amp;stream=false&amp;header=false&amp;height=100"></iframe>  
                        </div>
                        
                        <p>The mission of the East Campus Business, Information Technology, and Public Services
                           Division is to provide for the quality workforce education of a diverse student body
                           in a learning-centered environment. Strong technical, analytical, communication, workplace
                           and &nbsp;critical thinking skills will be developed to prepare students for careers meeting
                           the needs of today’s workforce, and environment, through the use of the state of the
                           art models of instructional delivery, up to date equipment, and the resources of skilled
                           and innovative faculty..
                        </p>
                        
                        <h4>Click here for <strong><a href="../../faculty/resources/index.html">Adjunct Faculty Resources</a></strong>
                           
                        </h4>
                        
                        <p><strong>Welcome!</strong><br>
                           The East Campus Business, IT &amp; Public Services department offers 
                           A.S. degrees and Technical Certificates in Accounting, 
                           Business Administration, Criminal Justice, Office Administration, 
                           Paralegal Studies, Computer Information Technology, and Computer Programming 
                           &amp; Analysis. We appreciate your visit 
                           to our web page. 
                        </p>
                        
                        <h2>Program Information</h2>
                        
                        <p>Click on the academic area for information on courses. If you have 
                           further questions, please call the program director at the contact 
                           number below.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <h2>                    East Campus Program Chairs/Lead Faculty</h2>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Department information for other campus located on: <a href="../index.html">http://preview.valenciacollege.edu/departments/</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    
                                    <p><strong>Accounting Technology</strong><br>
                                       <strong>Program Chairs<br>
                                          </strong>Located on East Campus 
                                    </p>
                                    
                                    <p>Patti Lopez, 407-582-2518<br>
                                       <a href="mailto:PLopez@valenciacollege.edu">PLopez@valenciacollege.edu</a></p>
                                    
                                    <p><a href="mailto:PLopez@valenciacollege.edu">                      </a>Laurie Larson , 407-582-2513<br>
                                       <a href="mailto:LLarson@valenciacollege.edu">LLarson@valenciacollege.edu<br>
                                          </a><a href="mailto:LLarson@valenciacollege.edu"><br>
                                          </a><a href="accounting/default.html">Accounting Technology Web Site</a><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Business Administration</strong><br>
                                       <strong>Program Chairs</strong><br>
                                       Located on East Campus <br>
                                       <br>
                                       Storm Russo, 407-582-2348<br>
                                       <a href="mailto:srusso@valenciacollege.edu%20">SRusso@valenciacollege.edu </a></p>
                                    
                                    <p>Lee McCain, 407-582-2489 <br>
                                       <a href="mailto:LMccain@valenciacollege.edu">LMccain@valenciacollege.edu</a>
                                       <br>
                                       <br>
                                       <a href="businessadmin/index.html">Business Administration Web Site</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p><strong>Criminal Justice<br>
                                          Program Chair</strong><br>
                                       Located on East Campus                  
                                    </p>
                                    
                                    <p><strong>                    </strong>Brian Murphy , 407-582-2381<br>
                                       <a href="mailto:jmurphy60@valenciacollege.edu">JMurphy60@valenciacollege.edu</a> <br>
                                       <br>
                                       <a href="criminal-justice/index.html">Criminal Justice Web Site</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Information Technology Computer  Programming &amp; Analysis<br>
                                          Program Chair</strong><br>
                                       Located on East Campus
                                    </p>
                                    
                                    <p><strong>                      </strong>Dave Brunick , 407-582-2360<br>
                                       <a href="mailto:dbrunick@valenciacollege.edu">DBrunick@valenciacollege.edu<br>
                                          <br>
                                          </a><a href="it/index.html">Information Technology Web Site</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p><strong>Office and Medical Office Administration<br>
                                          </strong><strong>
                                          Program Chair </strong><br>
                                       Located on East Campus
                                    </p>
                                    
                                    <p>                      Betty Wanielista, 407-582-2347 <br>
                                       <a href="mailto:bwanielista@valenciacollege.edu">BWanielista@valenciacollege.edu</a><br>
                                       <br>
                                       <a href="office/index.html">Office Administration Web Site</a></p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p><strong>Paralegal Studies</strong><br>
                                       <strong>Program Chair </strong><br>
                                       Located on East Campus<br>
                                       <br>
                                       Wendy Toscano, 407-582-2529 <br>
                                       <a href="mailto:wtoscano@valenciacollege.edu">WToscano@valenciacollege.edu</a><br>
                                       <br>
                                       <a href="paralegal/index.html">Paralegal Web 
                                          Site</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Nutrition</strong><br>
                                       April Engel MSH, RDN <br>
                                       Nutrition Professor and Discipline Coordinator<br>
                                       <a href="mailto:aengel@valenciacollege.edu">aengel@valenciacollege.edu</a> <br>
                                       407-582-2069
                                    </p>
                                    
                                    <p><a href="nutrition/index.html">Nutrition Web 
                                          Site</a></p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <h3>Career 
                                       Programs in the Business, IT and Public Services Division East Campus
                                    </h3>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/business/at.cfm">Accounting Technology (AS/TC) </a></strong></li>
                                       
                                       <li>
                                          <strong><a href="http://preview.valenciacollege.edu/asdegrees/business/ba.cfm">Business</a></strong> <a href="http://preview.valenciacollege.edu/asdegrees/business/ba.cfm"><strong>Administration (AS/TC)</strong></a><strong> </strong>
                                          
                                       </li>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/information-technology/cit.cfm">Computer Information Technology (AS/TC) </a></strong></li>
                                       
                                       <li><strong> <a href="http://preview.valenciacollege.edu/asdegrees/information-technology/cpa.cfm">Computer Programming &amp; Analysis (AS/TC) </a></strong></li>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/criminal-justice/">Criminal Justice (AS/TC) </a></strong></li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/engineering/ddt.cfm">Drafting (TC-Auto CADD only) </a></strong></li>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/business/imt.cfm">Industrial Management (AS) </a></strong></li>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/business/moa.cfm">Medical Office Administration (AS/TC) </a></strong></li>
                                       
                                       <li>
                                          <strong> </strong><a href="http://preview.valenciacollege.edu/asdegrees/business/oa.cfm"><strong>Office Administration (AS/TC) </strong></a>
                                          
                                       </li>
                                       
                                       <li><strong><a href="http://preview.valenciacollege.edu/asdegrees/criminal-justice/ps.cfm">Paralegal Studies (AS) </a></strong></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p>Detailed Program requirements:<a href="../../academics/programs/index.html"> http://preview.valenciacollege.edu/programs/</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/future-students/financial-aid/">Information on Cost of Attending Valencia &amp; Financial Aid</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/future-students/admissions/">Admissions and Applying to Valencia College</a></p>
                                    
                                    <p><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Additional Information, Campus Tours, and Information Sessions</a><br>
                                       <br>
                                       AS-Degree Advisor for all Career Program: <a href="../../associate-in-science-students/index.html">valenciacollege.edu/helpas</a>. <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> Dean: </div>
                                 
                                 <div>
                                    
                                    <p><strong>Carin Gordon<br>
                                          </strong> Dean<strong><br>
                                          </strong><a href="mailto:cgordon15@valenciacollege.edu">CGordon15@valenciacollege.edu</a>                <br>
                                       407-582-2807<br>                  
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p>Office Location: 
                                       8-105<br>
                                       Office Fax: 407-582-8901
                                    </p>
                                    
                                    <p>Mail Code 3-25<br>
                                       701 N. Econlockhatchee Trail<br>
                                       Orlando, FL 32825
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div> 
                                    <div>Assistants to the Dean:</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Donna (Suzie) Gant</strong><br>
                                       407-582-2807 <br>
                                       <a href="mailto:dgant1@valenciacollege.edu">DGant1@valenciacollege.edu</a></p>
                                    
                                    <p> <strong>Stephanie Hines</strong> <br>
                                       407-582-2958<br>
                                       <a href="mailto:shines10@valenciacollege.edu">SHines10@valenciacollege.edu</a></p>
                                    
                                    <p><strong>Mary Franke </strong><br>
                                       Staff Assistant
                                       <br>
                                       407-582-2551 <br>
                                       <a href="mailto:%20mfranke1@valenciacollege.edu">MFranke1@valenciacollege.edu</a>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Fall and Spring Office</strong> <strong>Hours:</strong><br>
                           Monday - Friday: 8AM - 5PM
                        </p>
                        
                        <p><strong>Summer Office</strong> <strong>Hours:</strong><br>
                           Monday - Thursday: 8AM - 5PM<br>
                           Friday: 8AM - Noon
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../map/east.html">Campus Map/Driving
                                 directions</a></li>
                           
                        </ul>
                        
                        <p><a href="default.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-east/default.pcf">©</a>
      </div>
   </body>
</html>