<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Article about Plagiarism</title>
<cfinclude template="/includes/headTags.ssi">
<link rel="stylesheet" href="/includes/contribute.css" type="text/css" />
<link rel="stylesheet" href="/includes/headeralert.css" type="text/css" />
<cfinclude template="/includes/google.ssi">
</head>
<body>
<a href="../documents/AboutPlagiarism2014.pdf">Article about Plagiarism</a>
<div id="containerHeaderAlert"><cfinclude template="/includes/incl_headeralert.ssi"></div>
<div id="containerTop"><cfinclude template="/includes/header.ssi"></div>
<div id="containerMain">


<div id="containerContent" role="main">

<div id="wrapper">
<div id="crumb"><cfinclude template="../../crumb.ssi"></div>

<table id="tableMain" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
      <div id="contentNav"><cfinclude template="/west/arts-and-humanities/nav.ssi"></div> 
      
    </td>
    <td valign="top">
      <a name="content" id="content"></a>
      <table id="contentTable" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top">&nbsp;</td>
          <td valign="top"><img src="/west/arts-and-humanities/programs/humanities/images/humanities1.jpg" width="770" height="260" alt="" />
          <cfinclude template="/west/arts-and-humanities/headMain.ssi">
<!---***************************Page Content goes Here**********************************--->          
          
            <h2>Plagiarism</h2>
            <p>
            
            <ul>
              <li>All  work submitted for credit in any class must be the product of the  individual student's own original thoughts supported and informed by  appropriately documented and credited sources.</li>
              <li>Plagiarism is  the use of someone else's words, ideas, pictures, design, and/or intellectual  property without the  correct documentation and punctuation.</li>
              <li>Plagiarism  takes many forms: for example, turning in the same  essay  for two  different courses  is considered self-plagiarism and  will result in a zero for the paper. You may legitimately wonder how anyone  would ever know; this is one of the purposes of the <a href="https://webmail.valenciacollege.edu/owa/redir.aspx?SURL=O7W_rkz8B5I0kFS97xuI-IZgpYdFjlWgHncnKQfbOjThY9-mZjnSCGgAdAB0AHAAOgAvAC8AawBiAC4AYgBsAGEAYwBrAGIAbwBhAHIAZAAuAGMAbwBtAC8AZABpAHMAcABsAGEAeQAvAFMAQQBGAEUALwBTAHUAYgBtAGkAdAArAGEAKwBQAGEAcABlAHIAKwB0AG8AKwBhACsAUwBhAGYAZQBBAHMAcwBpAGcAbgBtAGUAbgB0AA..&URL=http://kb.blackboard.com/display/SAFE/Submit%2ba%2bPaper%2bto%2ba%2bSafeAssignment">SafeAssign</a> plagiarism  scan.</li>
            </ul>
            
            <p>Plagiarism is morally indefensible.  Any assignment showing signs of plagiarism, either the:</p>
            <ol>
              <li>Deliberate  cut-and-paste  of online or print sources</li>
              <li>Recycling  of  essays from previous classes </li>
              <li>Essays  written  on behalf of the student by family members or friends, or third  parties, such as professional essay writing services</li>
              <li>The result  of inattention and incompetence </li>
              <li>Paraphrasing large sections based on the ideas of another source...even if you put it into your own words <strong>will be graded zero.</strong>  A second offense will result in an appointment with the dean of the Arts and Humanities Department.</li>			
            </ol>			
			
			<h2>How to Avoid Plagiarism</h2>

            <ul>
            <li>Here is a good rule: It is better to be safe than sorry in academic writing. Always cite your sources. A person’s ideas are their intellectual property. You wouldn’t go into another student’s backpack and steal their property. Plagiarism is stealing the ideas of another person and passing them off as your own without giving them credit.</li> 
            <ul type="-">
              <li>This includes paraphrasing or summarizing, as well as cut and paste.</li>
              <li>Cutting and pasting large sections of text, even when you cite your sources, does not represent college level, original writing.</li>
            </ul>
          <li>If you have to look something up for your paper, cite your sources in text, and in your works cited page.</li>
          <li>Even if you summarize, give the author of the ideas credit.</li>
          </ul>
          
        <h2>Plagiarism Consequences</h2>
            <p> <a href="http://valenciacollege.edu/oit/learning-technology-services/student-resources/academic-integrity/plagiarism.cfm">Valencia resources on plagiarism</a></p>
            </p>
			<!---****************************End Page Content***************************************--->          
          </td>
        </tr>
        <tr>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="15" height="1" alt="" /></td>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="770" height="1" alt="" /></td>
        </tr>
      </table>
      
      
    </td>
  </tr>
  <tr>
    <td><img class="no_print" src="/images/spacer.gif" width="162" height="1" alt="" /></td>
    <td><img class="no_print" src="/images/spacer.gif" width="798" height="1" alt="" /></td>
  </tr>
</table>

</div>
</div>
</div> 
<div id="containerBtm"><cfinclude template="/includes/footer.ssi"></div>
</body>
</html>
