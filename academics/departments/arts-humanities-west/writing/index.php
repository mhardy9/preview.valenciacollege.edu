<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Level Writing | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="College Level Writing | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, writing, arts, humanities, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/writing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/writing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li>Writing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>College Level Writing</h2>
                        
                        <p>State Board of Education Rule 6A-10.030 requires that students complete with grades
                           of C or better 12 credit hours in designated courses in which the student is required
                           to demonstrate college-level writing skills through multiple assignments. These courses
                           must be completed successfully (grades of C or better) prior to the receipt of an
                           A.A. Degree and prior to entry into the upper division of a Florida public university.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h2>What does college level writing mean for the Humanities?</h2>
                        <span>For a student to meet the criteria for college level writing in Humanities, and achieve
                           a grade of C or better, they must be able to demonstrate the following skills through
                           multiple writing assignments:</span>
                        
                        <p>&nbsp;</p>
                        
                        <ol>
                           
                           <li>Evaluate appropriate primary sources (visual arts, architecture, performing arts,
                              literature, religious texts, philosophy, music) and make original observations about&nbsp;those
                              sources.
                           </li>
                           
                           <li>Demonstrate an awareness of the connection between the primary sources, the historical
                              period, and cultural movements occurring at the time the sources were produced.
                           </li>
                           
                           <li>Formulate an argument that uses primary sources as evidence to defend a central thesis
                              statement.
                           </li>
                           
                           <li>Employ grammar, spelling, punctuation, writing mechanics, and format appropriate for
                              a college student preparing for real world situations in a continuing academic or
                              professional environment.
                           </li>
                           
                        </ol>
                        
                        <p><strong>What are College Level Writing Assignments?</strong></p>
                        
                        <p>Although each professor’s assignments will vary…</p>
                        
                        <p>•Expect to write at least one formal essay in each course</p>
                        
                        <p>•Other assignments may include, but are not limited to, in-class essays, responses
                           to readings, observations on works of art, or responses to cultural events.
                        </p>
                        
                        <p>•Treat every writing assignment as an opportunity to practice college level writing
                           skills, and follow the four guidelines of college level writing for the Humanities.
                        </p>
                        
                        <p>•Students must take Comp I before they can take a Gordon Rule Humanities course, and
                           you will receive some general guidelines for college level academic writing.
                        </p>
                        
                        <p>•Your Humanities professor may also give you a writing guide, or explain their individual
                           expectations to you. Just as every work environment is different, every professor
                           is different. Part of practicing college level writing is to be flexible and adaptable,
                           follow directions, and take advantage of any guidance that is offered to you.
                        </p>
                        
                        <h2>Reconizing and Preventing&nbsp;Plagiarism&nbsp;</h2>
                        
                        <p class="container-fluid box_style_3 v-red main-title .h2"><strong><em>"Plagiarism is the act of presenting the words, ideas, images, sounds, </em></strong><br><strong><em>or the creative expression of others as your own. </em></strong><br><strong><em>Plagiarism is representing someone else’s work as your own."</em></strong><br>—<span>California College of the Arts,&nbsp;<em>Plagiarism Prevention and Awareness Guide</em></span></p>
                        
                        <p><span>Please refer to your course syllabus, Valencia College's policies on plagiarism. To
                              help better understand what plagiarism is and how to prevent it please refer to this
                              article: &nbsp;&nbsp;</span></p>
                        
                        <p><a href="https://www.affordablecollegesonline.org/college-resource-center/plagiarism-prevention-and-awareness/" target="_blank">Plagiarism On Campus: Resources to Help Detect, Prevent and Avoid Classroom Plagiarism
                              for Teachers and Students, by Shelly Errington Nicholson, University of Massachusetts
                              Amherst. &nbsp;</a></p>
                        <a id="content" name="content"></a></div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/writing/index.pcf">©</a>
      </div>
   </body>
</html>