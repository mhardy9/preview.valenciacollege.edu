<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/writing/standards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/writing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/writing/">Writing</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <img alt="" height="260" src="../../../programs/arts-and-humanities/writing/humanities1.jpg" width="770">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Valencia Gen-Ed Standards</h2>
                        
                        <p>The general education program at Valencia is an integral part of the A.A. Degree program
                           and is designed to contribute to the student’s educational growth by providing a basic
                           liberal arts education. A student who completes the general education program should
                           have achieved the following outcomes:
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>CULTURAL &amp; HISTORICAL UNDERSTANDING: </strong></div>
                                 
                                 <div>
                                    <strong></strong>
                                    
                                    <p>Demonstrate understanding of the diverse traditions of the world, and an individual's
                                       place in it.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>QUANTITATIVE AND SCIENTIFIC REASONING: </strong></div>
                                 
                                 <div>
                                    <p>Use processes, procedures, data, or evidence to solve problems and make effective
                                       decisions.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>COMMUNICATION SKILLS: </strong></div>
                                 
                                 <div>
                                    <p>Engage in effective interpersonal, oral, and written communication.</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>ETHICAL RESPONSIBILITY: </strong></div>
                                 
                                 <div>
                                    <p>Demonstrate awareness of personal responsibility in one's civic, social, and academic
                                       life.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>INFORMATION LITERACY:</strong></div>
                                 
                                 <div>
                                    <p>Locate, evaluate, and effectively use information from diverse sources.</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>CRITICAL THINKING: </strong></div>
                                 
                                 <div>
                                    <p>Effectively analyze, evaluate, synthesize, and apply information and ideas from diverse
                                       sources and disciplines.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/writing/standards.pcf">©</a>
      </div>
   </body>
</html>