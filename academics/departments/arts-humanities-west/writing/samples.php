<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Samples | College Level Writing | Writing | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Samples | College Level Writing | Writing | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, writing, samples, arts, humanities, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/writing/samples.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/writing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/writing/">Writing</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Writing Samples</h2>
                        
                        <p>
                           <strong><u>College Level</u></strong></p>
                        
                        <p>
                           
                           <a href="/academics/departments/arts-humanities-west/writing/documents/good-examples/During_1640.docx" target="_blank">Thomas Hobbes: As Influenced by the English Revolution</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/good-examples/example2essaymodern-intro_thesis.pdf" target="_blank">RED, White and Blue</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/good-examples/Hoeschen_Jessica_Sample_Essay_2-1.docx" target="_blank">Even Centuries Apart</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/good-examples/Hoeschen_Jessica_Sample_Essay_2.docx" target="_blank">Even Centuries Apart</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/good-examples/Major_Essay-Example_for_Website.docx" target="_blank">The Contrast of Hope and Despair</a><br>
                           
                           
                        </p>
                        
                        <strong><u>Not College Level</u></strong><p> 
                           
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/CharlenneGREEK.docx" target="_blank">Mythology in Greek and Roman Societies</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/essay_bad_example.pdf" target="_blank">Compare and Contrast of Lee Millers' Buchenwald and Pablo Picassos' Guernica</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/example1essayancient-intro_thesis.pdf" target="_blank">The Quintessential Search for the Meaning of Life: A Comparative Analysis of the Epic
                              of Gilgamesh and The Allegory of the Cave</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/Greece_VS_Rome_Gods.docx" target="_blank">Greece VS Rome</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/Hoeschen_Jessica_Paper_Prompt.docx" target="_blank">Comparative Expression Essay</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/Hum_paper.docx" target="_blank">Science Revolution</a><br>
                           <a href="/academics/departments/arts-humanities-west/writing/documents/bad-examples/Paper_1-1.docx" target="_blank">Effects of World War II on the Arts</a><br>
                           
                           
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/writing/samples.pcf">©</a>
      </div>
   </body>
</html>