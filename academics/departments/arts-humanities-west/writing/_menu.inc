<ul>
<li><a href="/academics/departments/arts-humanities-west/index.php">Home</a></li>

<li> <a class="show-submenu" href="/academics/departments/arts-humanities-west/writing/index.php"> College Level Writing <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/departments/arts-humanities-west/writing/standards.php"> Gen-Ed Standards </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/historical-cultural.php"> Historical and Cultural Context </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/primary-sources.php"> Primary Sources </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/plagiarism.php"> Plagiarism </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/writing-tips.php"> Pre Writing Tips </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/help.php"> Writing Help </a> </li>
<li> <a href="/academics/departments/arts-humanities-west/writing/samples.php"> Writing Samples </a> </li>
</ul>
</li>
	
<li><a href="http://net4.valenciacollege.edu/forms/west/arts-and-humanities/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
