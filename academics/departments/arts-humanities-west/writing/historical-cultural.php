<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/writing/historical-cultural.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/writing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/writing/">Writing</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <img alt="" height="260" src="../../../programs/arts-and-humanities/writing/humanities1.jpg" width="770">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Historical and Cultural Context</h2>
                        
                        <ul>
                           
                           <li>Every Humanities class expects students to be able to analyze works of visual art,
                              music, literature, philosophy, religion, or performing arts and develop the ability
                              to understand how it fits into its historical period.
                           </li>
                           
                           <li>This is a college wide standard</li>
                           
                           <li>We refer to this standard as Historical and Cultural Context.</li>
                           
                        </ul>
                        
                        
                        <h2>What is the Definition of Historical and Cultural Context?</h2>
                        
                        <p><strong>What  is historical and cultural context?</strong><br>
                           Context,  in analysis of the humanities, refers to factors that surround a work of
                           art or  literature but may not be stated explicitly. This background information 
                           informs our deeper understanding of the work in question and allows us to  analyze,
                           rather than summarize, what we are studying. It is important to be  aware of what
                           the artist was experiencing in their time and reacting to or reacting against.<br>
                           Possible  context questions to ask...
                        </p>
                        
                        <p>
                           <strong>What  key historical events occurred at the time the work was created?</strong><br>
                           Example:  How did the French Revolution influence the paintings of Jacques Louis David?<br>
                           Example:  How was Dadaism a reaction to World War I?<br>
                           Example:  How did the discovery of AIDS influence theatre and film in the 20th century?
                        </p>
                        
                        <p>
                           <strong>What  scientific discoveries or technological innovations may have influenced the
                              artist?</strong><br>
                           Example:  How did the invention of the printing press in Europe influence the spread
                           of  religion, ideas, and art?<br>
                           Example:  How did Freud’s theories on the  unconscious and dreams influence the paintings
                           of Salvador Dali?
                        </p>
                        
                        <p>
                           <strong>What are the other artistic influences on the work?</strong><br>
                           Example:  How is Picasso influenced by the paintings of Cezanne?<br>
                           Example:  What are the relationships between Ragtime, Jazz, Blues, Gospel, Country,
                           and  Rock and Roll music?
                        </p>
                        
                        <p>
                           <strong>What are the philosophical ideas of the time?</strong><br>
                           Example:  How is Baroque music a product of the Age of Reason?<br>
                           Example:  How does The  Matrix  reflect Post Modern Theory?
                        </p>
                        
                        <p>
                           <strong>What  are the cultural influences?</strong><br>
                           Example:  Does the artist live in a society that is individualistic or collectivistic?<br>
                           Example:  How are women regarded in the culture that produced the work?<br>
                           Who  is the intended audience?<br>
                           Example:  Is this work personal, for the masses, or for other artists?<br>
                           Example:  Is the work a tool of propaganda?
                        </p>
                        
                        <p>
                           <strong>Is the artist identified with a particular movement, school, or “-ism”?</strong><br>
                           Examples:  Transcendentalism, Romanticism, Existentialism, Neo-Classicism, Surrealism,
                           Anti-art, Rococo, High Renaissance, etc.?
                        </p>
                        
                        <p>
                           <strong>What  is the original purpose of the work?</strong><br>
                           Example:  Was the piece designed to inform, to educate, to entertain, to shock, or
                           to be  functional?
                        </p>
                        
                        <p>
                           <strong>What context is NOT:<br>
                              Lengthy  summary of plot details</strong><br>
                           While  a SMALL amount of plot summary may be useful to the purposes of writing a paper
                           it does not allow you to demonstrate critical thinking or analysis, and should  not
                           comprise the bulk of your writing. If you think of context as what is  UNSAID, think
                           of summary as what has OBVIOUSLY been said already, and does not  need to be repeated.
                        </p>
                        
                        <p>
                           <strong>Irrelevant  biographical or background information</strong><br>
                           Example:  When discussing the influence of Freud’s ideas on Dali’s art, it is not
                           necessary to include information  about Freud’s family, Dali’s childhood, the geography
                           of Europe, etc.
                        </p>
                        
                        <p>
                           <strong>Your  personal “context”</strong><br>
                           Sometimes  your professor may ask you to write a personal reflection that takes your
                           own  context into account. However, unless you are specifically asked to do  otherwise,
                           an academic paper is usually not the place to discuss your personal  experiences,
                           whether or not you “liked” the work, or difficulty you may have had writing the  paper.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/writing/historical-cultural.pcf">©</a>
      </div>
   </body>
</html>