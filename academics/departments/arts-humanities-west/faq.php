<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Frequently Asked Questions | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, frequently, asked, questions, arts, humanities, west, faq">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions (FAQs)</h2>
                        
                        <p>
                           <strong>Please review the frequently asked questions prior to contacting the Graphics Program
                              with questions.</strong>
                           
                           
                           
                        </p>
                        
                        <div>
                           
                           
                           <hr class="styled_2">
                           <h3>General</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>Why should I choose to major in Graphic &amp; Interactive Design?</strong> With the increase growth in technology there is a higher demand for graphic, interactive,
                                    and web designers. Those choosing to major in Graphic &amp; Interactive Design desire
                                    career satisfaction and possess qualities including creativity, capable of solving
                                    problems, good communication skills, ability to meet deadlines and manage projects,
                                    and willingness to work hard. Those that have made the decision to major in Graphic
                                    &amp; Interactive Design are often artistic and have experience with computers. 
                                 </p>
                                 
                                 <p>
                                    <strong>What type of job would I be able to work once graduating with a Graphic &amp; Interactive
                                       Designdegree?</strong> Graduates work in positions such as web designers, graphic designers, graphic artists,
                                    web production designers, layout designers, user interaction designer, user experience
                                    designer, interaction designer, publication designers, production artists, and digital
                                    media designers. 
                                 </p>
                                 
                                 <p>
                                    <strong>What type of salary should be expected once graduating?</strong> The estimated salary range for a Graphic &amp; Interactive Design major is $25,000 to
                                    $40,000 per year. This is fantastic considering our program only costs approximately
                                    $9,100 (including books/supplies). 
                                 </p>
                                 
                                 <p>
                                    <strong>What ways can I learn beyond the classroom?</strong> Students are encouraged to take advantage of the opportunity they have to go beyond
                                    the classroom and join organizations such as Visual Voices of Valencia, The Creatologists,
                                    the Orlando Chapter of AIGA (American Institute of Graphic Arts), Ad2 (an advertising
                                    group for young professionals) and OAF (Orlando Advertising Federation). Throughout
                                    the year all organizations hold seminars, workshops, and guest speakers which would
                                    benefit students majoring in Graphic, interactive, or web design. In addition, by
                                    joining these organizations students have the opportunity to network with other individual's
                                    in the graphic design industry. 
                                 </p>
                                 
                                 <p>
                                    <strong>I registered for a 3 credit hour course; why is the class nearly 4 hours long?</strong> There is usually one hour of lab time factored into most class times. During this
                                    time instructors are available to answer questions and provide assistance to students.
                                    Students may also be given this additional time to work on course projects. 
                                 </p>
                                 
                                 <p>
                                    <strong>I want to make sure I go to the lab when the lab is open, is there any way I can find
                                       out when the lab is open?</strong> We keep our East, West and Osceola lab hours posted on our website/blog here: <a href="http://valenciagraphicdesign.com/locationslabs/" target="_blank">http://valenciagraphicdesign.com/locationslabs/</a>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           <h3>Prerequisite</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>Why can't I take a class such as Electronic Imaging or Adobe Illustrator before taking
                                       Graphic Design Essentials?</strong> Graphic Design Essentials is required prior to taking most other Graphics courses
                                    so it is highly recommended that students enroll in this course early in the program.
                                    This course will overview software such as Photoshop, Illustrator, InDesign. An emphasis
                                    will also be placed on the use of typography, design, layout, color, pre-press production.
                                    Prior to completion of Graphic Design Essentials students will undergo a timed, hands-on
                                    competency exam. Graphic Design Essentials provides a fundamental understanding of
                                    programs such as photoshop and illustrator and therefore it is a prerequisite for
                                    classes such as Electronic Imaging and Adobe Illustrator.
                                    
                                 </p>
                                 
                                 <p>
                                    <strong>What if I wish to take a class but I haven't taken the prerequisite course required
                                       to for the course?</strong> It is highly recommended that students enroll in prerequisite courses prior to ensure
                                    they are adequately educated and prepared for each course. In some special circumstances,
                                    such as a student transfering from another school, students are able to take a course
                                    without the prerequisite course. Students desiring to take a course without taking
                                    a prerequisite course must demonstrate sufficient experience to take the course which
                                    requires a prerequisite. Students who feel they are capable of taking a course without
                                    prior completion of a prerequisite course may request an override which will be approved
                                    or disapproved by the program director based on the students competency for the required
                                    course demonstrated in their portfolio, by way of taking a competency exam and/or
                                    by way of transfer credits from another college-level institution. 
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Override</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>What is an override?</strong> An override may grant a student permission to take a course if they have not taken
                                    the required prerequisite course or if the course is filled at the time of registration.
                                    The program director is responsible for approving and disapproving overrides. 
                                 </p>
                                 
                                 <p>
                                    <strong>How is it decided who receives an override?</strong> Students closest to completing their degree are given higher priority when override
                                    decisions are made. Students requesting overrides due to lack of completion of prerequisite
                                    courses should demonstrate competency to take the course requiring an override via
                                    a portfolio review and proof of college-level instruction from another institution.
                                    
                                 </p>
                                 
                                 <p>
                                    <strong>When are override decisions made?</strong> Overrides decisions are made after the payment deadline purge date prior to the beginning
                                    of each semester. 
                                 </p>
                                 
                                 <p>
                                    <strong>The class I wish to take is full on West Campus but not on East Campus. Can I request
                                       an override to take the course on West Campus?</strong> No overrides requests will be accepted for courses if the same course is not full
                                    on another campus.                    
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Portfolio</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>When is the best time to take portfolio?</strong> Portfolio Review is to be scheduled during your graduating semester and you should
                                    not take any other classes that semester except for Internship.  
                                 </p>
                                 
                                 <p>
                                    <strong>What should I expect during portfolio review class?</strong> Although the course is only one credit hour, this is undoubtedly one of the most
                                    demanding and time consuming courses Graphic &amp; Interactive Design students face during
                                    their degree. During this course students prepare their past projects for their portfolio
                                    and often rework past projects. In addition students will design an identity/logo
                                    for themselves as well as a business card, letterhead, and self-promotional piece.
                                    Students will also refine their resume. All of this hard work is reflected during
                                    their portfolio review with professionals in the industry. Students should expect
                                    to also take a timed competency exam during the course, where they must score a minimum
                                    of a 70% 
                                 </p>
                                 
                                 <p>
                                    <strong>What is the competency exam and why do I have to take it to complete the portfolio
                                       review course?</strong> The competency exam is a timed examination where student will be evaluated for their
                                    ability to design and use necessary software to produce a printed publication or a
                                    website with files they are supplied with. The competency exam measures students capabilities
                                    and reflects the knowledge and experience they have gained in the program. After completing
                                    the competency exam students should feel confident that they are capable of working
                                    in an entry-level position as a Graphic, Interactive, or Web Designer.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Internship</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <strong>When is the best time to take the internship course?</strong> The best time to take the internship course is during your last semester while you
                                    are taking Portfolio Review. 
                                 </p>
                                 
                                 <p> 
                                    
                                    <strong>What do I need to do to register for the internship course?</strong> To get the best idea of how to register for internship and what to expect from the
                                    interning experience, students are required to view this orientation prior to being
                                    registered:  <a href="http://valenciagraphicdesign.com/2013/06/plan-to-internship-nextsemester/" target="_blank">http://valenciagraphicdesign.com/2013/06/plan-to-internship-nextsemester</a>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/faq.pcf">©</a>
      </div>
   </body>
</html>