<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Arts Humanities West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Welcome</h2>
                        
                        <p>The academic disciplines that constitute the Arts and Humanities arise from our need
                           to create; search for life's meaning, and to understand the nature of beauty, fellowship,
                           knowledge, creativity, and imagination. The Arts and Humanities Department of Valencia
                           College-West supports the College's mission by providing general education courses
                           and extracurricular opportunities focused on foreign languages, fine arts, humanities,
                           and interdisciplinary studies.
                        </p>
                        
                        <p>We, the Arts and Humanities Department, pledge to support and do our utmost to protect
                           members of our community who are discriminated against, unjustly treated, or otherwise
                           targeted because of race, religion, gender, sexuality, immigration status, and other
                           forms of difference. Recognizing that words and symbols are powerful and can be manipulated
                           into violence, we proclaim our commitment to direct the force of language toward large
                           and small acts of learning, alliance, imagination, and justice.
                        </p>
                        
                        <p>We acknowledge this statement from the Faculty of the English Department at Cornell
                           University.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>Upcoming Events</h3>
                        
                        <div data-id="hye_shin">
                           
                           <div>Sep<br> <span>21</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/hye_shin" target="_blank">Hye Shin - 'Embrace The Hope' at East Campus Anita S. Wooten Gallery</a><br> <span>8:30 AM</span></div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h3>Location</h3>
                        Arts &amp; Humanities, West Campus Division Office:&nbsp;<br>Bldg 5, Rm 146<br> 407-582-5730 or 407-582-1300 // Fax: 407-582-5436<br> Monday - Friday: 8:00am to 5pm
                        
                        <p>&nbsp;</p>
                        
                        <hr class="styled_2">
                        
                        <h3>Contacts</h3>
                        
                        <p><strong>Dean:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=acalderofigueroa.html">Ana J. Caldero</a> <br><br> <strong>Administrative Assistant to the Dean:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=asorrough1.html">Amy Sorrough</a> <br><br> <strong>Administrative Assistant to the Dean:</strong><br>Lindsay Stephan<br><br> <strong>Chair Pre-Major in Architecture: </strong><br> <a href="/academics/includes/UserInfo.cfm-username=dwatters.html">Professor Allen Watters</a> <br><br> <strong>Chair Foreign Languages:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=jmenig.html">Professor Joseph Menig</a><br><br> <strong>Chair Graphic &amp; Interactive Design:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=mcurtiss.html">Professor Meg Curtiss</a> <br><br> <strong>Chair Humanities:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=lsenecal.html">Professor Lisa Cole</a> <br><br> <strong>Chair Interdisciplinary Studies:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=trodgers6.html">Dr. Travis Rodgers</a> <br><br> <strong>Program Advisor AA Pre-Majors Architecture:</strong><br> <a href="/academics/includes/UserInfo.cfm-username=mcancarevic.html">Mia Cancarevic</a> 
                        </p>
                        
                        <p><strong>Career Program Advisor Graphic &amp; Interactive Design</strong><br> <a href="/academics/includes/UserInfo.cfm-username=ghall13.html">Genevieve Hall</a></p>
                        
                        <p><strong>Program Advisor Communication, Arts, and Humanities</strong><br> <a href="/academics/includes/UserInfo.cfm-username=mreid6.html">Michael Reid</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/index.pcf">©</a>
      </div>
   </body>
</html>