<ul>
<li><a href="/academics/departments/arts-humanities-west/index.php">Home</a></li>
<li><a class="show-submenu" href="/academics/programs/arts-humanities-west/programs/index.php"> Programs </a>
<ul>
<li><a href="/academics/departments/arts-humanities-west/programs/architecture/index.php">Articulated Pre-Major&nbsp;in&nbsp;Architecture</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/fine-arts/index.php">Fine Arts</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/foreign-language/index.php">Foreign Language</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/graphic-interactive-design/index.php">Graphic &amp; Interactive Design</a>
<ul>
<li><a href="/academics/departments/arts-humanities-west/faq.php">FAQs</a></li>
</ul>
</li>
<li><a href="/academics/departments/arts-humanities-west/programs/humanities/index.php">Humanities</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/interdisciplinary-studies/index.php">Interdisciplinary Studies</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/philosophy/index.php">Philosophy</a></li>
<li><a href="/academics/departments/arts-humanities-west/programs/Religion/index.php">Religion</a></li>
</ul>
</li>
<li><a class="show-submenu" href="javascript:void(0);"> Resources </a>
<ul>
<li><a href="/academics/departments/arts-humanities-west/writing/index.php">College Level Writing</a></li>
</ul>
</li>
<li><a href="http://net4.valenciacollege.edu/forms/west/arts-and-humanities/contact.cfm" target="_blank">Contact Us</a></li>
</ul>