<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Philosophy | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Philosophy | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, philosophy">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/philosophy/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/philosophy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li>Philosophy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Philosophy</h2>
                        
                        <p>Philosophy, the love of wisdom, is the process of formulating and answering fundamental
                           questions about ourselves and the world around us. It deals principally with critical
                           thinking; philosophy seeks to explicate the distinction between rational explanations
                           and pseudo-scientific nonsense and to identify dogmatism and prejudice in the public
                           debate surrounding important moral and political issues.
                        </p>
                        
                        <p>Students in philosophy classes will be introduced to central movements, figures, and
                           works in the field. They will explore what reality is, what we should believe, what
                           we ought to do, and how we ought to be. Students will be able to identify arguments,
                           critically evaluate the arguments, and defend positions through standardized arguments.
                        </p>
                        
                        <p> They will learn to define essential concepts, to apply theory to reality, and to
                           assume responsibility for their choices by considering the ethical impact of their
                           behaviors. Because it is a critical discipline, philosophy encourages multiple viewpoints
                           and approaches to problem solving and aims to foster dialogue among diverse peoples.
                        </p>
                        
                        <p>The study of philosophy is unmatched in developing the broad intellectual competence
                           invaluable for all disciplines. This is evident in the fact that philosophy majors
                           tend to do well on standardized tests:
                        </p>
                        
                        <p><strong></strong></p>
                        
                        <p><strong>GRE (For Grad School Generally)</strong></p>
                        
                        <p>Among students taking the GRE, those intending to major in Philosophy at the Graduate
                           School level scored very well, according to Educational Testing Services, Inc. (ETS).
                           This data is current through April 2014.
                        </p>
                        
                        <p>Verbal Reasoning: mean of 160, the highest of any intended major.</p>
                        
                        <p>Analytical Writing: mean of 4.4, the highest of any intended major.</p>
                        
                        <p>Quantitative Reasoning: mean of 153, lower than only Physical Sciences and Engineering.</p>
                        
                        <p>SOURCE:<a href="https://www.ets.org/s/gre/pdf/gre_guide_table4.pdf">https://www.ets.org/s/gre/pdf/gre_guide_table4.pdf</a></p>
                        
                        <p><strong></strong></p>
                        
                        <p><strong>GMAT (For Business School)</strong></p>
                        
                        <p>The Graduate Management Admission Council, owner of the GMAT, reports that Philosophy
                           Majors’ scores were the 4th highest by major at a mean of 588, behind Physics (608),
                           Mathematics (605), and Engineering (588). This data was current through 2009-10.
                        </p>
                        
                        <p>SOURCE:<a href="http://blog.prep4gmat.com/majors-with-the-highest-and-lowest-gmat-scores/">http://blog.prep4gmat.com/majors-with-the-highest-and-lowest-gmat-scores/</a></p>
                        
                        
                        <p><strong>LSAT (For Law School)</strong></p>
                        
                        <p>Philosophy majors have the sixth highest LSAT scores (156.8 average), after Classics
                           (159.8), Policy Studies (158.8), International Relations (157.3), Art History (157.1),
                           and Mathematics (157.0). This data is current through 2014.
                        </p>
                        
                        <p>SOURCE:<a href="http://excessofdemocracy.com/blog/2014/4/the-best-prospective-law-students-read-homer">http://excessofdemocracy.com/blog/2014/4/the-best-prospective-law-students-read-homer</a></p>
                        
                        <p><strong></strong></p>
                        
                        <p><strong>MCAT (For Medicine)</strong></p>
                        
                        <p>The AAMC groups subject broadly, but Humanities majors score an average of 9.9 on
                           the MCAT VR (highest of any group) and 29.5 for the Total MCAT. This number is significantly
                           above average (28.6) and only Math and Science and Physical Science majors score higher.
                           This data is current through 2014.
                        </p>
                        
                        <p>SOURCE:<a href="https://www.aamc.org/download/321496/data/factstable18.pdf">https://www.aamc.org/download/321496/data/factstable18.pdf</a></p>            
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/philosophy/index.pcf">©</a>
      </div>
   </body>
</html>