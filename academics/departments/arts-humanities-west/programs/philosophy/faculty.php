<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty | Philosophy | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Faculty | Philosophy | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, west, faculty, philosophy">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/philosophy/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/philosophy/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/philosophy/">Philosophy</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Faculty &amp; Staff</h2>
                        
                        
                        
                        
                        
                        
                        
                        <link href="../../../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        <h3>Faculty</h3>
                        
                        <div>
                           
                           <div>
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=ehernandezcolon.html">Ernesto Hernandez Colon</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?ehernandezcolon" target="_blank">http://frontdoor.valenciacollege.edu?ehernandezcolon</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3>Adjunct</h3>
                        
                        <div>
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=dbenjamin11.html">David Benjamin</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?dbenjamin11" target="_blank">http://frontdoor.valenciacollege.edu?dbenjamin11</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=dlucius1.html">Dennis Lucius</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?dlucius1" target="_blank">http://frontdoor.valenciacollege.edu?dlucius1</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/philosophy/faculty.pcf">©</a>
      </div>
   </body>
</html>