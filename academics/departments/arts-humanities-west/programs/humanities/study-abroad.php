<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Study Abroad | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Study Abroad | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, west, study, abroad">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/humanities/study-abroad.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/humanities/">Humanities</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Study Abroad</h2>
                        
                        
                        <h3>Why Choose to Study Abroad?</h3>
                        
                        
                        <p>You will grow culturally, linguistically, personally, intellectually, and spiritually.
                           You will have the chance to go beyond sightseeing, and become acquainted with another
                           country, another culture, and other fascinating people. Study abroad will give you
                           an experience that a classroom setting will never do. You will have the opportunity
                           to learn about the country and the people firsthand, while you are immersed in the
                           language and the culture. You will gain new friendships on a global scale, and it
                           will surely expand your world view.
                        </p>
                        
                        
                        <p>For more information, visit <a href="../../../../international/studyabroad/students/default.html">Study Abroad &amp; Global Experiences (SAGE)</a> for students.
                        </p>
                        
                        
                        <h3>Experiences</h3>
                        
                        <p>This section could link to individual pages with Faculty or Student Experiences. 
                           Could have different Faculty or Students write essays about their experience, add
                           pictures, etc &amp; give a blurb here to link to a full page with one person or group's
                           experience.
                        </p>
                        
                        
                        <h3>Pictures</h3>
                        
                        <p>This is an example of a Flickr Slideshow that could be used to share pictures.</p>
                        
                        <div><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" height="300" width="430"> 
                              <param name="flashvars" value="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2Fmyvalencia%2Fsets%2F72157642700153203%2Fshow&amp;set_id=72157642700153203"> 
                              <param name="allowFullScreen" value="true"> 
                              <param name="src" value="https://www.flickr.com/apps/slideshow/show.swf?v=71649"> 
                              <embed allowfullscreen="true" flashvars="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2Fmyvalencia%2Fsets%2F72157642700153203%2Fshow&amp;set_id=72157642700153203" height="300" src="https://www.flickr.com/apps/slideshow/show.swf?v=71649" type="application/x-shockwave-flash" width="430"> </object></div>
                        
                        
                        <h3>Have questions?</h3>
                        
                        <p>If you have questions about the Study Abroad program:</p>
                        
                        <ul>
                           
                           <li>Program Leader</li>
                           
                           <li>Faculty Member</li>
                           
                           <li>Program Advisor</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/humanities/study-abroad.pcf">©</a>
      </div>
   </body>
</html>