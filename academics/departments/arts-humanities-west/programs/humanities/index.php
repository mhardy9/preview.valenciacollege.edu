<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Humanities | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Humanities | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/humanities/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li>Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Humanities</h2>
                        
                        <p>The general education program at Valencia is designed to contribute to the student's
                           educational growth by providing a basic liberal arts education.   Humanities Courses
                           are required as part of the General Education Requirements for Valencia
                        </p>
                        
                        
                        <p><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/hum/">Humanities Courses Offered</a></p>
                        
                        <p>Students will confirm the ability to think critically through demonstrating interpretive
                           ability and cultural literacy.&nbsp; Students will acquire competence in reflecting critically
                           upon the human condition.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>CORE COURSES</strong> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">ARH 1000</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ART APPRECIATION 
                        </p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 1020</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INTRODUCTION TO HUMANITIES 
                        </p>
                        
                        <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 1020H</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INTRODUCTION TO HUMANITIES-Honors 
                        </p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">MUL 1010</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MUSIC APPRECIATION <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                        </p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">PHI 2010</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INTRODUCTION TO PHILOSOPHY 
                        </p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">THE 1000</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; THEATRE APPRECIATION 
                        </p>
                        
                        
                        
                        <p><strong>INSTITUTIONAL HOURS </strong></p>
                        
                        <p><strong></strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2220</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - GREEK AND ROMAN <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2220H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES- GREEK AND ROMAN- HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2223</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - LATE ROMAN AND MEDIEVAL <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2223H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - LATE ROMAN AND MEDIEVAL HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2232</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - RENAISSANCE AND BAROQUE <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2232H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - RENAISSANCE AND BAROQUE - HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2234</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - ENLIGHTMENT AND ROMANTICISM <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2234H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - ENLIGHTENMENT AND ROMANTICISM - HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2250</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - TWENTIETH CENTURY <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2250H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HUMANITIES - TWENTIETH CENTURY - HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR</a>)
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2310</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>MYTHOLOGY <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2310H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>MYTHOLOGY - HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2410</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>ASIAN HUMANITIES <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">HUM 2461</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>LATIN AMERICAN HUMANITIES <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">MUT 1111</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>MUSIC THEORY I</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">PHI 2600</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>ETHICS AND CRITICAL THINKING <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">REL 2300</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>WORLD RELIGIONS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>or <u>REL 2300H</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                    </p>
                                    
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">ARH 2051</a><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>WORLD RELIGIONS -HONORS (<u>GR)</u></p>
                                    
                                    <p>INTRODUCTION TO ART HISTORY II (GR)</p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>or <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">ARH 2051H</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>INTRODUCTION TO ART HISTORY II - HONORS <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">ARC 1701</a> 
                                    </p>
                                 </div>
                                 
                                 <div>
                                    <p>HISTORY OF ARCHITECTURE I <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">(GR)</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/humanities/index.pcf">©</a>
      </div>
   </body>
</html>