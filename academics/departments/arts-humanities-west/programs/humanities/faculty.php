<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty | Humanities | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Faculty | Humanities | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, west, faculty">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/humanities/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/humanities/">Humanities</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Faculty &amp; Staff</h2>
                        
                        
                        
                        
                        
                        
                        
                        <link href="../../../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        <h3>Faculty</h3>
                        
                        <div>
                           
                           <div>
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jbroitman.html">Jed Broitman</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jbroitman" target="_blank">http://frontdoor.valenciacollege.edu?jbroitman</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=lsenecal.html">Lisa Cole</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?lsenecal" target="_blank">http://frontdoor.valenciacollege.edu?lsenecal</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=mcurtiss.html">Meg Curtiss</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?mcurtiss" target="_blank">http://frontdoor.valenciacollege.edu?mcurtiss</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=eframe.html">Ed Frame</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?eframe" target="_blank">http://frontdoor.valenciacollege.edu?eframe</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jhoeschen.html">Jessica Hoeschen</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jhoeschen" target="_blank">http://frontdoor.valenciacollege.edu?jhoeschen</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=rjenne.html">Ralf Jenne</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?rjenne" target="_blank">http://frontdoor.valenciacollege.edu?rjenne</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=vkunu.html">Vishma Kunu</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?vkunu" target="_blank">http://frontdoor.valenciacollege.edu?vkunu</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=llippitt.html">Lisa Lippitt</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?llippitt" target="_blank">http://frontdoor.valenciacollege.edu?llippitt</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jsindler.html">Julie Montione</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jsindler" target="_blank">http://frontdoor.valenciacollege.edu?jsindler</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jmoring.html">Julianna Moring</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jmoring" target="_blank">http://frontdoor.valenciacollege.edu?jmoring</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jnazariogrzechowi.html">Joanna Nazario Grzechowiak</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jnazariogrzechowi" target="_blank">http://frontdoor.valenciacollege.edu?jnazariogrzechowi</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=trodgers6.html">Travis Rodgers</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?trodgers6" target="_blank">http://frontdoor.valenciacollege.edu?trodgers6</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=bwarren11.html">Bob Warren</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?bwarren11" target="_blank">http://frontdoor.valenciacollege.edu?bwarren11</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    <img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40">
                                    
                                 </div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=lwilson56.html">Lauren Wilson</a></div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?lwilson56" target="_blank">http://frontdoor.valenciacollege.edu?lwilson56</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3>Adjunct</h3>
                        
                        <div>
                           
                           <div>
                              
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=aalexander.html">Andrew Alexander</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?aalexander" target="_blank">http://frontdoor.valenciacollege.edu?aalexander</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=gcalabrese.html">Guy Calabrese</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?gcalabrese" target="_blank">http://frontdoor.valenciacollege.edu?gcalabrese</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=ccranston1.html">Chris Cranston</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?ccranston1" target="_blank">http://frontdoor.valenciacollege.edu?ccranston1</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=ccully.html">Caroline Cully</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?ccully" target="_blank">http://frontdoor.valenciacollege.edu?ccully</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=adesormier1.html">Tony DeSormier</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?adesormier1" target="_blank">http://frontdoor.valenciacollege.edu?adesormier1</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=adiaz54.html">Aprilis Diaz</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?adiaz54" target="_blank">http://frontdoor.valenciacollege.edu?adiaz54</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jgriffith7.html">Jeannette Griffith</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jgriffith7" target="_blank">http://frontdoor.valenciacollege.edu?jgriffith7</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=igrimes.html">Ilana Grimes</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?igrimes" target="_blank">http://frontdoor.valenciacollege.edu?igrimes</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=dharrell10.html">Don Harrell</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?dharrell10" target="_blank">http://frontdoor.valenciacollege.edu?dharrell10</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jortega22.html">Jeannine Ortega</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jortega22" target="_blank">http://frontdoor.valenciacollege.edu?jortega22</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=mosborn.html">Melanie Osborn</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?mosborn" target="_blank">http://frontdoor.valenciacollege.edu?mosborn</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=lrodriguezhernand.html">Luz Rodriguez Hernandez</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?lrodriguezhernand" target="_blank">http://frontdoor.valenciacollege.edu?lrodriguezhernand</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=krumbley.html">Karen Rumbley</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?krumbley" target="_blank">http://frontdoor.valenciacollege.edu?krumbley</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=jscolaro.html">John Scolaro</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?jscolaro" target="_blank">http://frontdoor.valenciacollege.edu?jscolaro</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div><img alt="Faculty Photo" border="0" height="40" src="/academics/departments/arts-humanities-west/images/default_profile.jpg" width="40"></div>
                                 
                                 <div><a href="../../../../includes/UserInfo.cfm-username=lstevens4.html">Laura Stevens</a></div>
                                 
                                 
                                 <div> 
                                    
                                    <a href="http://frontdoor.valenciacollege.edu?lstevens4" target="_blank">http://frontdoor.valenciacollege.edu?lstevens4</a>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/humanities/faculty.pcf">©</a>
      </div>
   </body>
</html>