<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Top 10 Reasons | Study Abroad | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Top 10 Reasons | Study Abroad | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, study, abroad, top, reasons">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/humanities/top-reasons.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/humanities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/humanities/">Humanities</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Top Ten Reasons to Study Humanities</h2>
                        
                        <ol>
                           
                           <li>To practice the analytical thinking skills you need to 
                              be a successful student and employee.
                           </li>
                           
                           <li>To improve your skill at oral and written communication.</li>
                           
                           <li>To see the interconnectedness of all areas of knowledge 
                              -- how it all fits together..
                           </li>
                           
                           <li>To develop a global perspective by studying cultures throughout 
                              the world.
                           </li>
                           
                           <li>To deepen your understanding and appreciation of other's 
                              cultures and other's points of view.
                           </li>
                           
                           <li>To support and strengthen your local arts community by 
                              learning to appreciate the importance of creativity.
                           </li>
                           
                           <li>To clarify your values by comparing and contrasting them 
                              to what others have thought.
                           </li>
                           
                           <li>To deepen your sources of wisdom by learning how others 
                              have dealt with failures, success, adversities, and triumphs.
                           </li>
                           
                           <li>To appreciate what is enduring and to be able to tell 
                              the difference between the meaningless and the meaningful.
                           </li>
                           
                           <li>To be inspired by some of the greatest minds and thoughts 
                              of the ages.
                           </li>
                           
                        </ol>
                        
                        <p>by Professor Roberta Vandermast</p>            
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/humanities/top-reasons.pcf">©</a>
      </div>
   </body>
</html>