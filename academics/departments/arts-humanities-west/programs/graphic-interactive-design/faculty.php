<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/graphic-interactive-design/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/graphic-interactive-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/graphic-interactive-design/">Graphic Interactive Design</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Graphic &amp; Interactive Design<br>Faculty &amp; Staff
                        </h2>
                        
                        <h3>Faculty</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div><img src="/academics/departments/arts-humanities-west/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" border="0"></div>
                              
                              <div><a href="/academics/includes/UserInfo.cfm-username=mcurtiss.html">Meg Curtiss, Professor &amp; Program Chair<br></a><a href="mailto:mcurtiss@valenciacollege.edu">mcurtiss@valenciacollege.edu</a></div>
                              
                              <div><a href="http://frontdoor.valenciacollege.edu/?mcurtiss">http://frontdoor.valenciacollege.edu?mcurtiss</a></div>
                              
                              <div>&nbsp;</div>
                              
                              <div>
                                 
                                 <div><img src="/academics/departments/arts-humanities-west/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" border="0"></div>
                                 
                                 <div><a href="/academics/includes/UserInfo.cfm-username=jsousa.html">John Sousa</a>, Professor
                                 </div>
                                 
                                 <div><a href="mailto:jsousa@valenciacollege.edu">jsousa@valenciacollege.edu</a></div>
                                 
                                 <div><a href="http://frontdoor.valenciacollege.edu/?jsousa">http://frontdoor.valenciacollege.edu?jsousa</a></div>
                                 
                                 <div>&nbsp;</div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div><img src="/academics/departments/arts-humanities-west/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" border="0"></div>
                              
                              <div><a href="/academics/includes/UserInfo.cfm-username=kmai3.html">Khanh Mai</a>, Professor (east)
                              </div>
                              
                              <div><a href="mailto:kmai3@valenciacollege.edu">kmai3@valenciacollege.edu</a></div>
                              
                              <div><a href="http://frontdoor.valenciacollege.edu/?kmai3">http://frontdoor.valenciacollege.edu?kmai3</a></div>
                              
                              <div>&nbsp;</div>
                              
                              <div>Jason Ellison, Professor (east)<br><a href="mailto:jellison3@valenciacollege.edu">jellison3@valenciacollege.edu</a></div>
                              
                              <div>&nbsp;</div>
                              
                           </div>
                           
                           <div>&nbsp;</div>
                           
                        </div>
                        
                        <h3>Adjunct</h3>
                        
                        <p>Barbara Peterson<br><a href="mailto:bpeterson@valenciacollege.edu&gt;">bpeterson@valenciacollege.edu</a></p>
                        
                        <div>
                           
                           <div><img src="/academics/departments/arts-humanities-west/images/default_profile.jpg" alt="Faculty Photo" width="40" height="40" border="0"></div>
                           
                           <div class="v-red"><a href="/academics/includes/UserInfo.cfm-username=whurst.html">Jason Hurst<br>whurst@valenciacollege.edu<br></a></div>
                           
                           <div><a href="http://frontdoor.valenciacollege.edu/?whurst">http://frontdoor.valenciacollege.edu?whurst</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Jason Jones<br><a href="mailto:jjones112@valenciacollege.edu">jjones112@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Kevin Boynton <br><a href="mailto:kboynton@valenciacollege.edu&gt;">kboynton@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Jessica Garrity <br><a href="mailto:jgarrity1@valenciacollege.edu">jgarrity1@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Sarah Siddiqui <br><a href="mailto:ssiddiqui11@valenciacollege.edu">ssiddiqui11@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Edward McBride <br><a href="mailto:emcbride@valenciacollege.edu">emcbride@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>Amanda Froelich <br><a href="mailto:abonilla6@valenciacollege.edu">abonilla6@valenciacollege.edu</a><a href="mailto:abonilla6@valenciacollege.edu&gt;"><br><br></a></div>
                           
                           <div>Andrew Porter <br><a href="mailto:aporter6@valenciacollege.edu">aporter6@valenciacollege.edu</a></div>
                           
                           <div>&nbsp;</div>
                           
                           <div>&nbsp;</div>
                           
                           <div>
                              
                              <h3>Staff</h3>
                              
                              <p>Andy Hamer <br><a href="mailto:rhamer@valenciacollege.edu&gt;">rhamer@valenciacollege.edu</a><br>Graphics Lab: West Campus, Building 3, Rooms 150-151
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <h3><br>Dean
                           </h3>
                           <a href="/academics/includes/UserInfo.cfm-username=acalderofigueroa.html">Ana J. Caldero</a><span> </span>&nbsp;
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/graphic-interactive-design/faculty.pcf">©</a>
      </div>
   </body>
</html>