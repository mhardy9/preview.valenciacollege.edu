<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/architecture/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/architecture/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li>Architecture</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Articulated Pre-Major: Architecture</h2>
                        
                        <p>Housed on Valencia's West Campus, the Architecture Department is a 2-Year Pre-Articulated
                           Major.  Students in the architecture department develop fundamental skills in drawing,
                           model making, spatial analysis and critical thinking.
                        </p>
                        
                        <a href="http://architecture.ucf.edu/" target="_blank">UCF Complimentary Bachelor Design Program </a>
                        
                        
                        <h3>Associate in Arts Degree</h3>
                        <strong> Articulated Pre-Major: Architecture at University of Central Florida</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofcentralflorida/">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofcentralflorida/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofcentralflorida/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <br>
                        <strong> Articulated Pre-Major: Architecture at University of Florida</strong>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofflorida/">Catalog Information</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofflorida/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofflorida/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        <br>
                        
                        <p>After completion of the Pre-Articulated Degree, our students have successfully gained
                           admittance to the following Undergraduate Architecture Programs:
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Auburn University</li>
                                       
                                       <li>Boston Architectural College</li>
                                       
                                       <li>California College for the Arts </li>
                                       
                                       <li><strong>Columbia University</strong></li>
                                       
                                       <li><strong>Cooper Union for the Advancement of Science and Art </strong></li>
                                       
                                       <li><strong>Cornell University</strong></li>
                                       
                                       <li>Delft Technological University- located in Holland, Netherlands</li>
                                       
                                       <li>Florida International University</li>
                                       
                                       <li>Florida A &amp; M University</li>
                                       
                                       <li>Florida Atlantic University</li>
                                       
                                       <li>New School of Architecture-San Diego</li>
                                       
                                       <li>New York Institute of Technology</li>
                                       
                                       <li><strong>Pratt University</strong></li>
                                       
                                       <li>Savannah College of Art and Design</li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li><strong>Southern California Institute of Architecture (SCI-ARC)</strong></li>
                                       
                                       <li><strong>University of Central Florida</strong></li>
                                       
                                       <li>University of Colorado. Denver </li>
                                       
                                       <li><strong>University of Florida</strong></li>
                                       
                                       <li>University of Houston</li>
                                       
                                       <li>University of Illinois @ Chicago</li>
                                       
                                       <li>University of Miami</li>
                                       
                                       <li><strong>University of Michigan @ Ann Arbor</strong></li>
                                       
                                       <li>University of Nevada, Las Vegas</li>
                                       
                                       <li>University of Pennsylvania</li>
                                       
                                       <li><strong>University of South Florida</strong></li>
                                       
                                       <li><strong>University of Texas @ Austin</strong></li>
                                       
                                       <li>Virgina Tech.</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/architecture/index.pcf">©</a>
      </div>
   </body>
</html>