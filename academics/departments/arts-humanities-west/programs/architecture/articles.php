<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/architecture/articles.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/architecture/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/architecture/">Architecture</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2 class="v-red">Architecture Resources</h2>
                        
                        <hr>
                        
                        <h2 style="text-align: left;">Student Design Gallery</h2>
                        
                        <h3>Design I</h3>
                        
                        <p>Design 1 is an introduction to the fundamentals of two- and three-dimensional design,
                           principles of architectonics, and basic architectural design. Model making, drawing
                           and rendering techniques are applied to projects that explore the conceptualization
                           and communication of design ideas.
                        </p>
                        
                        <p><a href="https://www.flickr.com/photos/dms_vc_architecture/sets/72157648404409283/" target="_blank"><img src="/academics/departments/arts-humanities-west/programs/architecture/images/design_1_link-000.png" alt="Design 1 Gallery" width="300" height="40" border="0" hspace="0" vspace="0"></a></p>
                        
                        <h3>Design II</h3>
                        
                        <p>Design 2 is a focused study of two-dimensional design, including the primary elements
                           of line, shape, color, value, balance, pattern, texture, and rhythm. Assigned projects
                           will explore the application of space, scale, and proportion in basic architectural
                           design.
                        </p>
                        
                        <p><a href="https://www.flickr.com/photos/dms_vc_architecture/sets/72157650721147131/" target="_blank"><img src="/academics/departments/arts-humanities-west/programs/architecture/images/design_2_link-000.png" alt="Design 2 Gallery" width="300" height="40" border="0" hspace="0" vspace="0"></a></p>
                        
                        <h3>Design III</h3>
                        
                        <p>Design 3 is a focused study of three-dimensional design, including the principles
                           of architectonics and communication of design ideas. Issues of human use in relationship
                           to program and function are considered in detail. Projects vary but often address
                           residential or institutional programs and involve the integration of form, massing,
                           structure, circulation, light and context.
                        </p>
                        
                        <p><a href="https://www.flickr.com/photos/dms_vc_architecture/sets/72157650733943835/" target="_blank"><img src="/academics/departments/arts-humanities-west/programs/architecture/images/design_3_link-000.png" alt="Design 3 Gallery" width="300" height="40" border="0" hspace="0" vspace="0"></a></p>
                        
                        <h3>Design IV</h3>
                        
                        <p>Design 4 addresses the issues of functional relationships, spatial composition, and
                           site constraints, as well as selection and detailing of building materials. A sequence
                           of design exercises and projects with a high degree of complexity explore the three-dimensional
                           nature of architectural forms and spaces.
                        </p>
                        
                        <p><a href="https://www.flickr.com/photos/dms_vc_architecture/sets/72157650861201412/" target="_blank"><img src="/academics/departments/arts-humanities-west/programs/architecture/images/design_4_link-000.png" alt="Design 4 Gallery" width="300" height="40" border="0" hspace="0" vspace="0"></a></p>
                        
                        <hr>
                        
                        <h3>Sample Course Materials</h3>
                        
                        <ul>
                           
                           <li><a title="Click to Open" href="/academics/departments/arts-humanities-west/programs/architecture/documents/designisyllabus.pdf" target="_blank">Sample Design I Syllabus</a></li>
                           
                           <li><a title="Click to Open" href="/academics/departments/arts-humanities-west/programs/architecture/documents/design_1_materials_list.pdf" target="_blank">Sample Materials List</a></li>
                           
                           <li><a title="Click to Open" href="/academics/departments/arts-humanities-west/programs/architecture/documents/thecubicconstruct.pdf" target="_blank">Sample Design I Assignment</a></li>
                           
                           <li><a title="Click to Open" href="/academics/departments/arts-humanities-west/programs/architecture/documents/design_1_vocabulary_list.pdf" target="_blank">Design I Vocabulary List</a></li>
                           
                           <li><a title="Click to Open" href="/academics/departments/arts-humanities-west/programs/architecture/documents/Assessment_of_the_Effectiveness_of_Valencia_College_Architecture_Website_Survey.docx" target="_blank">Assessments of Effectiveness of Web site Survey</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Architecture Articles</h3>
                        
                        <ul>
                           
                           <li><a href="http://archrecord.construction.com/" target="_blank">Architectural Record Web Site</a></li>
                           
                           <li><a href="http://www.ncarb.org/" target="_blank">NCARB Web Site</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Where to find Architecture Material</h3>
                        
                        <ul>
                           
                           <li><strong>Sam Flax</strong><br>1800 E Colonial Dr<br>Orlando, FL 32803<br><a href="http://www.samflaxorlando.com/">samflaxorlando.com</a><br>(407) 898-9785
                           </li>
                           
                           <li><strong>Colonial Photo and Hobby</strong><br>634 North Mills Avenue<br>Orlando, FL 32803<br><a href="http://www.colonialphotohobby.com/">colonialphotohobby.com</a><br>(407) 841-1485
                           </li>
                           
                           <li><strong>Art Systems</strong><br>1740 State Road 436<br>Winter Park, FL 32792-1932<br><a href="http://www.artsystemsfl.com/">artsystemsfl.com</a><br>(407) 679-4700
                           </li>
                           
                           <li><strong>Valencia College West Campus Bookstore</strong><br>1800 S. Kirkman Rd<br>Orlando, FL 32811 <br><a href="/academics/locations-store/index.html">valenciacollege.edu/locations-store</a><br>(407) 582-1471
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/architecture/articles.pcf">©</a>
      </div>
   </body>
</html>