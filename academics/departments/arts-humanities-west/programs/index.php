<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li>Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Programs</h2>
                        
                        <ul>
                           
                           <li><a href="../../arts-and-humanities/programs/architecture/index.html">Articulated Pre-Major: Architecture</a></li>
                           
                           <li><a href="../../arts-and-humanities/programs/fine-arts/index.html">Fine Arts</a></li>
                           
                           <li><a href="../../arts-and-humanities/programs/foreign-language/index.html">Foreign Language</a></li>
                           
                           <li>
                              <a href="../../arts-and-humanities/programs/graphic-interactive-design/index.html">Graphic &amp; Interactive Design</a>  
                           </li>
                           
                           <li><a href="../../arts-and-humanities/programs/humanities/index.html">Humanities</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../arts-and-humanities/index.html">
                                          
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 146</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-5730 or 407-582-1300 // Fax: 407-582-5436</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday -  Friday: 8:00am to 5pm</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/west/arts-and-humanities/contact.cfm" target="_blank" title="Contact Us">CONTACT US</a></div> 
                        
                        
                        
                        
                        <p><strong>Dean:</strong><br> <a href="../../../includes/UserInfo.cfm-username=acalderofigueroa.html">Ana J. Caldero</a> <br><br>
                           <strong>Administrative Assistant to the Dean:</strong><br> <a href="../../../includes/UserInfo.cfm-username=asorrough1.html">Amy Sorrough</a>  <br><br>
                           
                           <strong>Administrative Assistant to the Dean:</strong><br> <a href="../../../includes/UserInfo.cfm-username=nsutton1.html">Natalie Sutton</a>  <br><br>
                           
                           <strong>Chair Pre-Major in Architecture: </strong><br> <a href="../../../includes/UserInfo.cfm-username=dwatters.html">Professor Allen Watters</a> <br><br> 
                           
                           <strong>Chair Foreign Languages:</strong><br> <a href="../../../includes/UserInfo.cfm-username=jmenig.html">Professor Joseph Menig</a><br><br> 
                           
                           <strong>Chair Graphic &amp; Interactive Design:</strong><br> <a href="../../../includes/UserInfo.cfm-username=mcurtiss.html">Professor Meg Curtiss</a>  <br><br>
                           
                           <strong>Chair Humanities:</strong><br> <a href="../../../includes/UserInfo.cfm-username=lsenecal.html">Professor Lisa Cole</a>  <br><br> 
                           
                           <strong>Chair Interdisciplinary Studies:</strong><br> <a href="../../../includes/UserInfo.cfm-username=trodgers6.html">Dr. Travis Rodgers</a> <br><br> 
                           
                           <strong>Program Advisor AA Pre-Majors Architecture:</strong><br> <a href="../../../includes/UserInfo.cfm-username=mcancarevic.html">Mia Cancarevic</a> <br>
                           
                           
                        </p>
                        
                        <p><strong>Career Program Advisor Graphic &amp; Interactive Design</strong><br> <a href="../../../includes/UserInfo.cfm-username=ghall13.html">Genevieve Hall</a> 
                        </p>
                        
                        
                        <p><strong>Program Advisor Communication, Arts, and Humanities</strong><br> <a href="../../../includes/UserInfo.cfm-username=mreid6.html">Michael Reid</a> 
                        </p>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/index.pcf">©</a>
      </div>
   </body>
</html>