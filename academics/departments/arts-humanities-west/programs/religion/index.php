<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Religion | Arts and Humanities | West Campus | Valencia College</title>
      <meta name="Description" content="Religion | Arts and Humanities | West Campus">
      <meta name="Keywords" content="college, school, educational, arts, humanities, west, religion">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/religion/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/religion/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li>Religion</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Religion</h2>
                        
                        <p>The academic study of religion seeks to facilitate understanding of various aspects
                           of religious traditions across the world, including their histories, doctrines, practices,
                           literature, codes of conduct and material culture. <strong>Our World Religions course (REL 2300)  covers Hinduism, Buddhism, Chinese Religions,
                              Shinto, Indigenous religions, Judaism, Christianity and Islam, among others. </strong></p>
                        
                        <p><strong></strong></p>
                        
                        <p>Religious traditions can be thought of as a lens through which people understand themselves
                           and their place in the world. Through a comparative approach, students are able to
                           identify factors that unite us across cultures as they consider the role of religion
                           in the human condition. &nbsp;Accordingly, students of religion will develop greater sensitivity
                           to diversity, an invaluable skill in today's multicultural society and the global
                           economy.&nbsp; 
                        </p>
                        
                        
                        <p>REL 2300 is a college writing course. &nbsp;Students will be able to refine their written
                           communication skills through a variety of assignments. Students who produce exceptional
                           work will be encouraged to apply to participate in our Religion Symposiums Students
                           will also have the opportunity to visit religious sites in the Central Florida area
                           and engage in dialogue with clergy members. Please keep an eye on this website for
                           information regarding upcoming events. &nbsp;
                        </p>
                        
                        
                        <p>Here are some links to articles on the benefits of religious literacy in the 21st
                           century: &nbsp;
                        </p>
                        
                        <p><a href="http://bulletin.hds.harvard.edu/articles/summerautumn2012/why-study-religion-twenty-first-century"> http://bulletin.hds.harvard.edu/articles/summerautumn2012/why-study-religion-twenty-first-century</a> 
                        </p>
                        
                        <p><a href="http://americamagazine.org/issue/religion-and-diplomacy">http://americamagazine.org/issue/religion-and-diplomacy</a></p>
                        
                        
                        
                        
                        <p><img alt="REL 2300 class at Lakshmi Narayan Mandir" height="367" src="/academics/departments/arts-humanities-west/programs/religion/images/IMG_4430.jpg" width="490"></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/religion/index.pcf">©</a>
      </div>
   </body>
</html>