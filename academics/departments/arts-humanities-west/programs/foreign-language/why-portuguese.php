<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Humanities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/arts-humanities-west/programs/foreign-language/why-portuguese.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/arts-humanities-west/programs/foreign-language/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Humanities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/arts-humanities-west/">Arts Humanities West</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/">Programs</a></li>
               <li><a href="/academics/departments/arts-humanities-west/programs/foreign-language/">Foreign Language</a></li>
               <li>Arts and Humanities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Why Study Portuguese?</h2>
                        
                        <ul>
                           
                           <li>Worldwide there are more speakers of Portuguese than there are of French, German,
                              Italian or Japanese.
                           </li>
                           
                           <li>Brazil's economy now has the sixth largest in the world behind the U.S., China, Japan,
                              Germany &amp; France.
                           </li>
                           
                           <li>In addition to Brazil - the 5th largest country in the world - Portuguese is spoken
                              in nine other countries on four continents.
                           </li>
                           
                           <li>Brazil is the world's 6th largest economy according to the World Bank.</li>
                           
                           <li>Florida's number one trading partner is Brazil, generating billions of dollars for
                              businesses statewide
                           </li>
                           
                           <li>Over 1 Million Brazilians visited Florida last year, and they spent an estimated 1.9
                              Billion dollars in our state.
                           </li>
                           
                           <li>Valencia's Portuguese language curriculum has flourished for over 20 years, enabling
                              its students to further studies and skills at universities stateside and in Brazil.
                           </li>
                           
                           <li>You can fulfill the Foreign Language requirement by studying Portuguese at Valencia.</li>
                           
                        </ul>
                        
                        <p> <strong>Take a look at the Valencia Catalog for the <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/por/" target="_blank">Portuguese courses offered</a></strong></p>
                        
                        <p>For more information on registering for Portuguese at Valencia's West Campus, contact
                           <a href="../../../../includes/UserInfo.cfm-username=rsansone.html">Professor Richard Sansone</a>.
                        </p>
                        
                        
                        <hr class="styled_2">
                        	
                        	
                        <h3>Location</h3>
                        
                        	Bldg 5, Rm 146<br>
                        	407-582-5730 or 407-582-1300 // Fax: 407-582-5436<br>
                        	Monday -  Friday: 8:00am to 5pm
                        
                        	
                        <hr class="styled_2">
                        
                        	
                        <h3>Contact Us</h3>
                        	<strong>Dean:</strong><br> <a href="../../../../includes/UserInfo.cfm-username=acalderofigueroa.html">Ana J. Caldero</a> <br><br>
                        	<strong>Administrative Assistant to the Dean:</strong><br> <a href="../../../../includes/UserInfo.cfm-username=asorrough1.html">Amy Sorrough</a>  <br><br>
                        
                        	<strong>Administrative Assistant to the Dean:</strong><br> <a href="../../../../includes/UserInfo.cfm-username=nsutton1.html">Natalie Sutton</a>  <br>
                        
                        	<strong>Chair Foreign Languages:</strong><br> <a href="../../../../includes/UserInfo.cfm-username=jmenig.html">Professor Joseph Menig</a><br><br>
                        
                        	
                        	
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/arts-humanities-west/programs/foreign-language/why-portuguese.pcf">©</a>
      </div>
   </body>
</html>