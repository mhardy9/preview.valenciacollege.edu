
<ul>
<li><a href="../../index.php">Home</a></li>

<li> <a class="show-submenu" href="../../../arts-humanities-west/programs/foreign-language/index.php"> Foreign Language <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li><a href="../../../arts-humanities-west/programs/foreign-language/why-portuguese.php">Why Study Portuguese?</a></li>
<li><a href="../../../arts-humanities-west/programs/foreign-language/why-spanish.php">Why Study Spanish?</a></li>
</ul>
</li>
<li><a href="../../../arts-humanities-west/programs/foreign-language/faculty.php">Faculty &amp; Staff Listings</a></li>

<li><a href="http://net4.valenciacollege.edu/forms/west/arts-and-humanities/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
