<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Supervision and Management for Industry | Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Supervision and Management for Industry | Business and Hospitality">
      <meta name="Keywords" content="college, school, educational, supervision, management, industry, business, hospitality">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/supervision-and-management-for-industry/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/supervision-and-management-for-industry/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/">Programs</a></li>
               <li>Supervision And Management For Industry</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        
                        
                        
                        
                        					
                        <h1>Supervision and Management for Industry</h1>
                        
                        					
                        <p>Program Chair:  <a href="../../../../includes/UserInfo.cfm-username=bbunn.html">Barry Bunn</a></p>
                        
                        
                        					
                        <p><strong>Supervision and Management for Industry (A.S. Degree)</strong></p>
                        					
                        <p>The Supervision and Management for Industry program is an articulated program with
                           the Orange County Public Schools Tech Centers, TECO of Osceola District Schools, and
                           Orlando Utilities Commission. It provides the opportunity for students from the Orange
                           County Public Schools Tech Centers or TECO of Osceola District Schools who have completed
                           specific programs in the fields of Automotive Collision Repair and Automotive Service
                           Technology, Heavy Duty Truck and Bus Mechanics, Manufacturing, Air Conditioning, Carpentry,
                           Building Construction, Electricity and Electrician, Welding, Plumbing, Industrial
                           Pipefitter, Brick and Block Masonry, Fire Sprinkler Systems, Glazing, Heavy Equipment
                           Operations, Structural SteelWork, Sheet Metal Fabrication, Cosmetology, Sewing Technology,
                           Jewelry Making, Painting and Decorating, and Interior Décor and Interior Decorating
                           Services or employees of Orlando Utilities Commission who have completed the Line
                           Technician Program to pursue college-level course work that is appropriate for supervisory
                           and management roles and upward mobility in their respective fields.   If you have
                           completed one of the approved Tech Center programs, please contact your Career Program
                           Advisor to discuss the credit transfer process.  
                        </p>
                        					
                        <p>Career Program Advisor: <a href="../../../../includes/UserInfo.cfm-username=cteumer.html">Chris Teumer</a><br></p>
                        					
                        <ul>
                           						
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/industrialmanagementtechnology/">Catalog Overview</a></li>
                           						
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/industrialmanagementtechnology/#programrequirementstext">Program Requirements</a></li>
                           						
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/industrialmanagementtechnology/#coursedescriptionstext">Course Descriptions</a></li>
                           						
                           <!--                <li><a href="../../../../asdegrees/transfer-agreements.html">Alternative Award of Credit Agreements with Tech Centers </a></li> -->
                           					
                        </ul>
                        
                        
                        				
                     </div>
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/supervision-and-management-for-industry/index.pcf">©</a>
      </div>
   </body>
</html>