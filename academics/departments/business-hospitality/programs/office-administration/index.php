<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office and Medical Administration | Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Office and Medical Administration | Business and Hospitality">
      <meta name="Keywords" content="college, school, educational, office, medical, administration, business, hospitality">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/office-administration/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/office-administration/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/">Programs</a></li>
               <li>Office Administration</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Office and Medical Administration</h3>
                        
                        
                        <p>Program Chair: <a href="../../../../includes/UserInfo.cfm-username=mhoward.html">Marie Howard</a><br>
                           Career Program Advisor: <a href="../../../../includes/UserInfo.cfm-username=bjohnson.html">Beverly Moore-Johnson</a>
                           
                        </p>
                        
                        <p><strong>Office Administration</strong></p>
                        
                        
                        <p>Office Administration is designed to prepare students for office support positions
                           of receptionists, administrative assistants, records managers, office supervisors
                           and managers and other office related positions. The program prepares students for
                           immediate employment in a business office and provides training for both first-time
                           job seekers and experienced employees who wish to advance in their careers.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#programrequirementstext">Program Requirements</a></li>
                           
                           <li> <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Certificates</a>
                              
                           </li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://net1.valenciacollege.edu/future-students/degree-options/associates/office-administration/" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>Medical Office Administration</strong></p>
                        
                        <p>Medical Office Administration  is designed to prepare students for employment as office
                           support staff and medical transcriptionists in medical offices, hospitals and other
                           healthcare organizations. It provides training for both first-time job seekers and
                           experienced employees who wish to advance in their careers.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Certificates</a></li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#coursedescriptionstext">Course Descriptions</a>
                              
                           </li>
                           
                           <li><a href="http://net1.valenciacollege.edu/future-students/degree-options/associates/medical-office-administration/" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/office-administration/index.pcf">©</a>
      </div>
   </body>
</html>