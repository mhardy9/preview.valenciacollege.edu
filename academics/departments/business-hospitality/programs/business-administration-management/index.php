<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business Administration and Management | Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Business Administration and Management | Business and Hospitality">
      <meta name="Keywords" content="college, school, educational, business, administration, management, hospitality">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/business-administration-management/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/business-administration-management/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/">Programs</a></li>
               <li>Business Administration Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Business Administration and Management</h3>
                        
                        
                        <p>Program Chair:  <a href="../../../../includes/UserInfo.cfm-username=bbunn.html">Dr. Barry Bunn</a><br>
                           Faculty:  <a href="../../../../includes/UserInfo.cfm-username=lpowell.html">Lana Powell</a></p>
                        
                        
                        
                        <p><strong>Associate in Arts Business Transfer Plan (A.A. General Studies Degree)</strong></p>
                        
                        <p>This transfer plan is designed to help you prepare to transfer to a Florida public
                           university as a junior to complete a four-year Bachelor's degree.
                        </p>
                        
                        <p>Career Program Advisor: <a href="../../../../includes/UserInfo.cfm-username=acollins31.html">Ayanna Collins</a><br></p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/#coursedescriptionstext">Course Descriptions</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>Business Administration (A.S. Degree)</strong>
                           
                        </p>
                        
                        <p>This program is designed for students who seek immediate employment in the field of
                           business administration and/or who decide to utilize this Articulated A.S. to B.A./B.S.
                           Career Path to transfer to any Florida public university as a junior to complete a
                           four-year Bachelor's degree in General Business Administration.
                        </p>
                        
                        <p>Career Program Advisors:<br><br>
                           <a href="../../../../includes/UserInfo.cfm-username=cteumer.html">Chris Teumer</a> - A.S. Business Administration with specialization in Finance, Marketing, Management,
                           Human Resources, International Business, Small Business Management and Real Estate
                           Management.  Technical Certificates in Customer Service Management, Human Resources
                           Management and Real Estate Specialist.<br><br>
                           <a href="../../../../includes/UserInfo.cfm-username=ghall13.html">Genevieve Hall</a> - Articulated A.S. to B.S./B.A. Business Administration and Technical Certificates
                           in Business Management, Operations and Specialist
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Technical Certificates</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/business/ba.cfm">A.S. Program Overview</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/business-administration-management/index.pcf">©</a>
      </div>
   </body>
</html>