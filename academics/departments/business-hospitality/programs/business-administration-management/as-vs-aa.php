<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>AA vs AS | Business Administration Management | Business and Hospitality | Valencia College</title>
      <meta name="Description" content="AA vs AS | Business Administration Management | Business and Hospitality">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/business-administration-management/as-vs-aa.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/business-administration-management/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/">Programs</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/business-administration-management/">Business Administration Management</a></li>
               <li>AA vs AS</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					<a name="content" id="content"></a>
                        
                        					
                        <h3>A.S. vs. A.A., What's the Right Business Degree For Me?</h3>
                        
                        					
                        <ol>
                           						
                           <li>Students who plan to  transfer to a university within the <a href="http://www.flbog.edu/aboutsus/universities/">State University System of  Florida</a> to pursue a bachelor's degree in business (i.e. Finance, Marketing,  Management,
                              etc...) or accounting are advised to consider the Associate in Arts  Degree with Business
                              Transfer Plan.*
                           </li>
                           						
                           <li>Students who plan to  earn a specialized two year degree in their field and transition
                              directly to  the workforce are advised to consider any one of Valencia's A.S. Business
                              Degrees  in Finance, Management, Marketing, Human Resources, International Business,
                              Small Business Management or Real Estate.&nbsp; These A.S. degrees will also  give you
                              the option to transfer to the Bachelor of Applied Science Degree upon  completion.*&nbsp;
                              
                           </li>
                           						
                           <li>Students who plan to  earn a specialized two-year degree in their field and transition
                              directly to  the workforce, but also want the option to transfer to a university within
                              the <a href="http://www.flbog.edu/aboutsus/universities/">State University System of  Florida</a> to pursue a <a href="http://www.fldoe.org/core/fileparse.php/5421/urlt/0078406-astobaccalaureate_agreemnts.pdf">four-year  articulated degree in General Business or Business Administration and 
                                 Management</a> are advised to consider the Articulated A.S. to B.S. / B.A.  Business Administration
                              Degree.*&nbsp; 
                           </li>
                           					
                        </ol>
                        					
                        <p>*Please contact your transfer institution to ensure that  you identify all of the
                           transfer requirements for your specific university and  major.&nbsp; Private universities
                           and institutions outside of the <a href="http://www.flbog.edu/aboutsus/universities/">State University System of  Florida</a> may have differing requirements as well.&nbsp; Please see the  following link for agreements
                           between Valencia College and various private  universities <a href="../../../../asdegrees/credit_private.html">http://preview.valenciacollege.edu/asdegrees/credit_private.cfm</a>.  &nbsp;
                        </p> 
                        
                        					
                        <p>For more information on common prerequisites, check the statewide advising manual
                           at <a href="https://www.floridashines.org" target="_blank">floridashines.org</a>.
                        </p>         
                     </div>
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/business-administration-management/as-vs-aa.pcf">©</a>
      </div>
   </body>
</html>