<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li>Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Programs</h2>
                        
                        
                        <ul> 
                           
                           <li><a href="accounting/index.html">Accounting</a></li>
                           
                           <li><a href="business-administration-management/index.html">Business Administration &amp; Management</a>
                              
                           </li>
                           
                           <li><a href="hospitality-culinary/index.html">Hospitality &amp; Culinary</a></li>
                           
                           <li><a href="supervision-and-management-for-industry/index.html">Industrial Management Technology</a></li>
                           
                           <li><a href="office-administration/index.html">Office &amp; Medical Administration</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Division of Business and Hospitality 
                              
                           </div>
                           <br>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../index.html">
                                             West Campus Division of Business and Hospitality 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Building 7, Room 107</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-1069</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Friday: 8am to 5pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        <br>
                        
                        
                        <div><a href="../documents/valencia-business-and-hospitality-whats-my-degree.pdf" target="_blank" title="Which Business Degree is Right for Me?">Which Business Degree is Right for Me?</a></div>
                        <br>
                        
                        <div><a href="http://preview.valenciacollege.edu/west/business-and-hospitality/documents/AA_and_AS_Presentation.pptx" target="_blank" title="AA &amp; AS Degree Presentation">AA &amp; AS Degree Presentation</a></div>
                        
                        
                        
                        <p><strong>Open Computer Lab</strong><br>
                           West Campus Building 7, Room 144<br>
                           <a href="../../../includes/UserInfo.cfm-username=cchreptak.html">Charles Chreptak</a><br>
                           
                        </p>
                        
                        
                        <p><strong>Other West Campus Computer Labs</strong>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Building 7, Room 140 (OST)</li>
                           
                           <li>Building 7, Room 141 (OST, ACC)</li>
                           
                           <li>Building 7, Room 145 (ACC)</li>
                           
                           <li>Building 7, Room 148 (ACC)</li>
                           
                           <li>Building 9, Room 109 (HOSP&amp;CUL)</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/index.pcf">©</a>
      </div>
   </body>
</html>