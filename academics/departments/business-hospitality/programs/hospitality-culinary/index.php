<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hospitality and Culinary | Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Hospitality and Culinary | Business and Hospitality">
      <meta name="Keywords" content="college, school, educational, business, culinary, hospitality">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/programs/hospitality-culinary/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/programs/hospitality-culinary/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/business-hospitality/">Business Hospitality</a></li>
               <li><a href="/academics/departments/business-hospitality/programs/">Programs</a></li>
               <li>Hospitality Culinary</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3>Hospitality &amp; Culinary</h3>
                        
                        <p>From meeting planning to menu planning, the skills Valencia students gain during their
                           courses of study are transferable around the world, which means that, upon graduation,
                           your job could take you anywhere you want to go. Of course, starting out here in Central
                           Florida gives you quite a leg up. As one of the most fertile hospitality markets in
                           the world, Orlando is a hot place to get started because jobs are plentiful and opportunities
                           for experience are abundant. That's why we call it the world's largest learning laboratory.
                        </p>
                        
                        <p>Culinary Program Chair: <a href="../../../../includes/UserInfo.cfm-username=ppilloud.html">Chef Pierre Pilloud</a><br>
                           Hospitality Program Chair:  <a href="../../../../includes/UserInfo.cfm-username=jinglis.html">James Inglis</a><br>
                           Career Program Advisor:<a href="../../../../includes/UserInfo.cfm-username=dclay5.html"> D'Mya Clay</a><br>
                           Culinary Faculty:  <a href="../../../../includes/UserInfo.cfm-username=kbourgoin.html">Chef Kenneth Bourgoin</a> <br>
                           Culinary Faculty:  <a href="../../../../includes/UserInfo.cfm-username=srujak.html">Chef Steven Rujak</a><br>
                           Hospitality Faculty:  <a href="../../../../includes/UserInfo.cfm-username=crapp1.html">Craig Rapp</a></p>
                        
                        <p><strong><a name="HospTourism" id="HospTourism"></a>Hospitality and Tourism</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#certificatetext">Certificate</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/hospitalityandtourismmanagement/#coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/hospitality/htm.cfm" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        <p><strong><a name="Baking" id="Baking"></a>Baking and Pastry Management</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/bakingandpastrymanagement/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/bakingandpastrymanagement/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/bakingandpastrymanagement/#certificatetext">Certificate</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/bakingandpastrymanagement/#coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/hospitality/bpm.cfm" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        <p><strong><a name="Culinary" id="Culinary"></a>Culinary Management</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/culinarymanagement/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/culinarymanagement/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/culinarymanagement/#certificatetext">Certificate</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/culinarymanagement/#coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/hospitality/cm.cfm" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        <p><strong><a name="Restaurant" id="Restaurant"></a>Restaurant and Food Service Management</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/restaurantandfoodservicemanagement/">Catalog Overview</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/restaurantandfoodservicemanagement/#programrequirementstext">Program Requirements</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/restaurantandfoodservicemanagement/#coursedescriptionstext">Course Descriptions</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/asdegrees/hospitality/rfsm.cfm" target="_blank">A.S. Degree Program Overview &amp; Description</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/programs/hospitality-culinary/index.pcf">©</a>
      </div>
   </body>
</html>