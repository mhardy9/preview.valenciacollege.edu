
<ul>
<li><a href="/academics/departments/business-hospitality/index.php">Home</a></li>
<li><a href="/academics/departments/business-hospitality/faculty.php">Faculty &amp; Staff</a></li>
	
<li> <a class="show-submenu" href="/academics/departments/business-hospitality/programs/index.php"> Programs <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li><a href="/academics/departments/business-hospitality/programs/accounting/index.php">Accounting</a></li>
<li><a href="/academics/departments/business-hospitality/programs/business-administration-management/index.php">Business Administration &amp; Management</a></li>
<li><a href="/academics/departments/business-hospitality/programs/hospitality-culinary/index.php">Hospitality &amp; Culinary</a></li>
<li><a href="/academics/departments/business-hospitality/programs/supervision-and-management-for-industry/index.php">Supervision and Management for Industry</a></li>
<li><a href="/academics/departments/business-hospitality/programs/office-administration/index.php">Office &amp; Medical Administration</a></li>	
</ul>
</li>

<li><a href="/academics/departments/business-hospitality/programs/business-administration-management/as-vs-aa.php">A.S. vs. A.A.</a></li>

<li><a href="http://net4.valenciacollege.edu/forms/west/business-and-hospitality/contact.cfm" target="_blank">Contact Us</a></li>
</ul>

