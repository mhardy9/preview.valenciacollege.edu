<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business and Hospitality | Valencia College</title>
      <meta name="Description" content="Business and Hospitality">
      <meta name="Keywords" content="college, school, educational, business, hospitality">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/business-hospitality/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business-hospitality/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Business and Hospitality</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Business Hospitality</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Welcome</h2>
                        
                        <h3>Mission</h3>
                        The Business and Hospitality Division will emphasize, measure, improve and model the
                        qualities necessary for students to be successful in their fields.
                        
                        <h3>Vision</h3>
                        Our Business and Hospitality programs will produce competent graduates who are successful
                        in their fields.
                        
                        <h3>Values</h3>
                        
                        <ul>
                           
                           <li>We value excellent teaching and learning</li>
                           
                           <li>We value practical experiential learning and mentoring</li>
                           
                           <li>We value a positive, cooperative and safe learning community</li>
                           
                           <li>We value divisional and programmatic improvement for the instructors and students</li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>&nbsp;Location</h3>
                        
                        <div>
                           
                           <div>Building 7, Room 107</div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>407-582-1069</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monday - Friday: 8am to 5pm&nbsp;</div>
                                 
                              </div>
                              
                              <div>
                                 <hr>
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a title="Which Business Degree is Right for Me?" href="/academics/departments/business-and-hospitality/documents/Valencia-Business-and-hospitality-Whats-My-Degree.pdf" target="_blank">Which Business Degree is Right for Me?</a></p>
                        
                        <div><a title="AA &amp; AS Degree Presentation" href="http://preview.valenciacollege.edu/academics/departments//business-and-hospitality/documents/AA_and_AS_Presentation.pptx" target="_blank">AA &amp; AS Degree Presentation</a></div>
                        
                        <div>&nbsp;</div>
                        
                        <p><strong>Open Computer Lab</strong><br>West Campus Building 7, Room 144<br><a href="/academics/includes/UserInfo.cfm-username=cchreptak.html">Charles Chreptak</a></p>
                        
                        <p><strong>Other West Campus Computer Labs</strong></p>
                        
                        <ul>
                           
                           <li>Building 7, Room 140 (OST)</li>
                           
                           <li>Building 7, Room 141 (OST, ACC)</li>
                           
                           <li>Building 7, Room 145 (ACC)</li>
                           
                           <li>Building 7, Room 148 (ACC)</li>
                           
                           <li>Building 9, Room 109 (HOSP&amp;CUL)</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/business-hospitality/index.pcf">©</a>
      </div>
   </body>
</html>