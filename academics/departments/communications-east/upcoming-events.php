<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Communications | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-east/upcoming-events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-east/">Communications East</a></li>
               <li>East Campus Communications</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>Upcoming Events</h2>  
                        
                        <img height="298" src="../../../locations/east/communications/Visiting-Authors.jpg" title="Visting Authors" width="490">
                        
                        <div>
                           
                           <h3><a href="documents/visiting-authors.pdf" target="_blank">Download Fall 2017 Visiting Authors</a></h3>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Visiting Authors</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Ira Sukrungruang</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Sept. 21, 2017</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>1:00-3:00 PM</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>3-Atrium</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Cary Holliday</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Oct. 31, 2017</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>1:00-3:00 PM</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>8-101</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Tana and Thomas Welch</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Nov. 16, 2017</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>1:00-3:00 PM</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>3-Atrium</strong></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 7, Rm 163 // MC: 3-20</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2433 // 407-582-2370 // 407-582-2444</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:30am to 5:30pm<br>Friday: 8am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <br>
                        
                        
                        
                        <div>
                           
                           <div>Valencia College Creative Writers</div>
                           
                           <div>
                              <a href="https://www.facebook.com/pages/Valencia-College-Creative-Writers/149293765236967?fref=ts" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>
                           
                        </div> 
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-east/upcoming-events.pcf">©</a>
      </div>
   </body>
</html>