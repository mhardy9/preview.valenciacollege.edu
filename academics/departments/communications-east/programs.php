<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Communications | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-east/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-east/">Communications East</a></li>
               <li>East Campus Communications</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Academic Disciplines &amp; Faculty</h2>
                        
                        
                        <h3>EAP Students Film</h3>
                        
                        <a href="https://www.youtube.com/embed/b6QIgyewY0g" target="_blank"><img alt="EAP Video" height="400" src="../../../locations/east/communications/eap-video-thumb.jpg" title="EAP Video" width="490"></a><br><br>
                        
                        
                        
                        <div>
                           
                           <ul>
                              
                              <li data-id="#tab-1">EAP</li>
                              
                              <li data-id="#tab-2">English</li>
                              
                              <li data-id="#tab-4">Speech</li>
                              
                              <li data-id="#tab-5">Creative Writing</li>   
                              
                              <li data-id="#tab-6">Mass Communication</li>                   
                              
                           </ul>
                           
                           
                           <div>
                              
                              <h3>English for Academic Purposes Courses</h3>
                              
                              <ul>
                                 
                                 <li><a href="../../../locations/eap/courses.html" target="_blank">Reading (Levels 2, 3, 4, 5, 6)</a></li>
                                 
                                 <li><a href="../../../locations/eap/courses.html" target="_blank">Composition (Levels 2, 3, 4, 5, 6)</a></li>
                                 
                                 <li><a href="../../../locations/eap/courses.html" target="_blank">Speech (Levels 2, 3, 4, 5)</a></li>
                                 
                                 <li>
                                    <a href="../../../locations/eap/courses.html" target="_blank">Grammar (Levels 2, 3, 4, 5)</a> 
                                 </li>
                                 
                              </ul>
                              
                              
                              <h3>English for Academic Purposes Faculty</h3>
                              
                              <p><strong><a href="mailto:tbizon@valenciacollege.edu">Tatiana Bizon</a></strong> - <strong><em>Professor and Coordinator of EAP</em></strong><br>
                                 407-582-2406
                              </p>
                              
                              <p><strong><a href="mailto:idreilinger@valenciacollege.edu">Ian Dreilinger</a> - <em>Professor of EAP<br>
                                       </em></strong>407-582-2024
                              </p>
                              
                              <p><strong><a href="mailto:nhopkins3@valenciacollege.edu">Nissa Hopkins</a></strong> - <strong><em>Professor of EAP</em></strong><br>
                                 407-582-2501
                              </p>
                              
                              <p><strong><a href="mailto:jmay@valenciacollege.edu">James May</a></strong> - <strong><em>Professor of EAP</em></strong><br>
                                 407-582-2047
                              </p>
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>English Courses</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/" target="_blank">ENC: English Composition</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/eng/" target="_blank">ENG: English - General</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enl/" target="_blank">ENL: English Literature</a></li>
                                 
                              </ul>
                              
                              
                              <h3>English Faculty</h3>
                              
                              
                              <p><strong><a href="mailto:aboumarate@valenciacollege.edu">Abdallah Boumarate</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2011
                              </p>
                              
                              <p><strong><a href="mailto:dcharron@valenciacollege.edu">Dorothy Charron</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2113
                              </p>
                              
                              <p><strong><a href="mailto:sdauer@valenciacollege.edu">Susan Dauer</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2401
                              </p>
                              
                              <p><strong><a href="mailto:helliott3@valenciacollege.edu">Holly Elliott</a></strong><strong> - <em>Professor of English</em></strong><br>
                                 407-582-2215
                              </p>
                              
                              <p><strong><a href="mailto:lermel@valenciacollege.edu">Lauren Ermel</a></strong><strong> - <em>Professor of English</em></strong><br>
                                 407-582-2436
                              </p>
                              
                              <p><strong><a href="mailto:afaulkner4@valenciacollege.edu">Shea Faulkner</a> - <em>Professor of English</em></strong> <br>
                                 407-582-2094                    
                              </p>
                              
                              <p><strong><a href="mailto:rgordon7@valenciacollege.edu">Randy Gordon</a></strong> - <strong><em>Professor of English and Discipline Co-Coordinator</em></strong><br>
                                 407-582-2010
                              </p>
                              
                              <p><strong><a href="mailto:tgrajeda@valenciacollege.edu">Victoria Grajeda</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2276
                              </p>
                              
                              <p><strong><a href="mailto:cholliday@valenciacollege.edu">Clay Holliday</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2751
                              </p>
                              <strong><a href="mailto:kholt12@valenciacollege.edu">Kirsten Holt</a> - <em>Professor  of English </em></strong><br>
                              407-582-2248
                              
                              
                              <p><strong><a href="mailto:jhughes@valenciacollege.edu">John Hughes</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2210
                              </p>
                              
                              <p><strong><a href="mailto:zhyde@valenciacollege.edu">Zachary Hyde</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2647
                              </p>
                              
                              <p><strong><a href="mailto:skoopman@valenciacollege.edu">Shari Koopman</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2945
                              </p>
                              
                              <p><strong><a href="mailto:jleonard9@valenciacollege.edu">James Leonard</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2632
                              </p>
                              
                              <p><strong><a href="mailto:tmadison2@valenciacollege.edu">Tamara Madison</a></strong><strong> - <em>Professor of English</em></strong><br>
                                 407-582-2819
                              </p>
                              
                              <p><strong><a href="mailto:jmaguire1@valenciacollege.edu">Jane Maguire</a></strong><strong> - <em>Professor of English</em></strong><br>
                                 407-582-2228
                              </p>
                              
                              <p><strong><a href="mailto:mmahaffey1@valenciacollege.edu">Mandy Mahaffey</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2084
                              </p>
                              
                              <p><strong><a href="mailto:amiller95@valenciacollege.edu">Ashley Miller</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2475
                              </p>
                              
                              <p><strong><a href="mailto:cojeda4@valenciacollege.edu">Chiara Ojeda</a></strong><strong> - <em>Professor of English</em><em></em></strong><br>
                                 407-582-2521
                              </p>
                              
                              <p><strong><a href="mailto:rschachel@valenciacollege.edu">Robert Schachel</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2634
                              </p>
                              
                              <p><strong><a href="mailto:csmith134@valenciacollege.edu">Crystal Smith</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2072
                              </p>
                              
                              <p><a href="mailto:rthomas108@valenciacollege.edu"><strong>Richard Thomas</strong></a><strong> - <em>Professor of English</em></strong><br>
                                 407-582-2295
                              </p>
                              
                              <p><a href="mailto:strazzera@valenciacollege.edu"><strong>Summer Trazzera</strong></a><strong> - Professor  of English and Discipline Co-Coordinator</strong><strong></strong><br>
                                 407-582-2267
                              </p>
                              
                              <p><strong><a href="mailto:nvalentino@valenciacollege.edu">Nicole Valentino</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2420
                              </p>
                              
                              <p><strong><a href="mailto:mwycha@valenciacollege.edu">Marcelle Wycha</a></strong> - <strong><em>Professor of English</em></strong><br>
                                 407-582-2362                    
                              </p>
                              
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>Speech  Courses</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/" target="_blank">SPC 1017. INTERPERSONAL  COMMUNICATION</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/" target="_blank">SPC 1017H. INTERPERSONAL  COMMUNICATION - HONORS</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/" target="_blank">SPC 1608. FUNDAMENTALS OF SPEECH</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/" target="_blank">SPC 1608H. FUNDAMENTALS OF SPEECH - HONORS</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/" target="_blank">SPC 1700. CROSS CULTURAL  COMMUNICATION</a></li>
                                 
                                 
                              </ul>
                              
                              
                              
                              <h3>Speech Faculty</h3>
                              
                              <p><strong><a href="mailto:egaythwaite@valenciacollege.edu">Edie Gaythwaite</a></strong> - <strong><em>Professor of Speech</em></strong><br>
                                 407-582-2485
                              </p>
                              
                              <p><strong><a href="mailto:bgombash@valenciacollege.edu">William Gombash</a></strong> - <strong><em>Professor of Speech</em></strong><br>
                                 407-582-2356
                              </p>
                              
                              <p><strong><a href="mailto:mgonzalez39@valenciacollege.edu">Margaret Gonzalez</a> - <em>Professor of Speech</em></strong><br>
                                 407-582-2249
                              </p>
                              
                              <p><strong><a href="mailto:alafavor@valenciacollege.edu">Alycia LaFavor</a> - <em>Professor of Speech</em></strong><br>
                                 407-582-2315
                              </p>
                              
                              <p><strong><a href="mailto:clewis1@valenciacollege.edu">Courtney Lewis</a></strong> - <strong><em>Professor of Speech &amp; Discipline Coordinator</em></strong><br>
                                 407-582-2234
                              </p>
                              
                              <p><strong><a href="mailto:klong@valenciacollege.edu">Kim Long</a></strong> - <strong><em>Professor of Speech</em></strong><br>
                                 407-582-2777
                              </p>
                              
                              <p><strong><a href="mailto:emclaughlin2@valenciacollege.edu">Molly McLaughlin</a></strong> - <strong><em>Professor of Speech</em></strong><br>
                                 407-582-2266
                              </p>
                              
                              <p><strong><a href="mailto:rnewman@valenciacollege.edu">Rebecca Newman</a> - <em>Professor of Journalism</em></strong><br>
                                 407-582-2370
                              </p>
                              
                              <p><strong><a href="mailto:koneal12@valenciacollege.edu">Kathleen O'Neal</a> - <em>Professor of Speech</em></strong><br>
                                 407-582-2252
                              </p>
                              
                              <p><strong><a href="mailto:kperri@valenciacollege.edu">Kathleen Perri</a></strong> - <strong><em>Professor of Speech</em></strong><br>
                                 407-582-2806
                              </p>
                              
                              <p><strong><a href="mailto:krushing@valenciacollege.edu">Kevin Rushing</a></strong> - <strong><em>Professor of Speech</em></strong> <br>
                                 407-582-2653
                              </p>
                              
                              <p><strong><a href="mailto:lschellpfeffer@valenciacollege.edu">Liza Schellpfeffer</a></strong> - <strong><em>Professor of Speech</em></strong> <br>
                                 407-582-2731
                              </p>
                              
                              <p><strong><a href="mailto:astone28@valenciacollege.edu">Arri Stone</a> - <em>Professor of Speech</em></strong><br>
                                 407-582-3270
                              </p>
                              
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <h3>Creative Writing Courses</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2001. CREATIVE WRTING AND ADVANCED COMPOSITION </a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2100. INTRODUCTION TO FICTION </a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2102. FICTION II </a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2300. INTRODUCTION TO POETRY </a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2301. POETRY II </a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/crw/" target="_blank">CRW 2710. INTRODUCTION TO SCRIPT WRITING </a></li>
                                 
                                 
                              </ul>
                              
                              
                              <h3>Creative Writing Faculty Bios</h3>
                              
                              
                              <p><strong>Elliott, Holly </strong><br>
                                 Dr. Holly Elliott, a native&nbsp;of&nbsp;Palatka, Florida, earned a  B.A. in Literature and
                                 an M.A., and Ph.D. in Creative Writing from Florida  State University. &nbsp;She served
                                 as Associate Poetry Editor and then Poetry Editor  for&nbsp;<em>The</em><em>&nbsp;Southeast&nbsp;Review</em> while attending F.S.U. and was  awarded&nbsp;the&nbsp;George M. Harper Award in Graduate Essay
                                 Writing. She has previously taught composition, literature and creative writing  courses
                                 at Florida State University, and at the&nbsp;University&nbsp;of&nbsp;Central Florida, where she served
                                 as content  advisor for the first-year writing publication, <em>Stylus</em>.  &nbsp;Publications include works selected for&nbsp;<em>Dream Fantasy  International</em>, <em>Strange Horizons</em>, and <em>Barnyard Horror</em>. &nbsp;She has been teaching at Valencia College since 2007. 
                              </p>
                              
                              <p><strong>Holt, Kirsten</strong><br>
                                 Kirsten Holt&nbsp;received  her MFA in poetry from the University of Central Florida. Her
                                 chapbook  "Overwintered" was the winner of the 2010 Annual Chapbook Contest and is
                                 available from Yellowjacket Press. She has served as managing editor for&nbsp;<em>The  Florida Review</em>, and reads for&nbsp;<em>Sweet: A Literary Confection</em>.  Kirsten presented her paper, "Video Games as Legitimate Literature"  at the 2012
                                 AWP Conference in Boston, and continues to push for higher learning  in alternative
                                 art forms.&nbsp;Recent work  can be found in the&nbsp;<em>Louisville&nbsp;Review,  Portland Review</em>, and&nbsp;<em>Orion</em>.&nbsp;She has been teaching Valencia College since 2013. 
                              </p>
                              
                              <p><strong>Koopmann,  Shari</strong><br>
                                 Shari  Koopmann holds a bachelor’s degree in English from Bloomsburg University, a
                                 master’s degree in Literature from the University of New Hampshire, and a  doctoral
                                 degree in Education from the University of Central Florida. She has  published poetry
                                 in various small press journals in both the United States and  Canada, including Red
                                 Hand Press’s <em>Fulva Flava</em> and <em>Defect Cult</em> literary annuals,  Second-Hand Production’s <em>Falling Star  Magazine</em>, Day Break Press’s <em>Prism Quarterly</em><em>,</em> and <em>Ascent  Aspirations Magazine</em>’s anthologies <a href="http://www.ascentaspirations.ca/AguaTerra%20Winners.htm" target="_blank"><em>Aqua Terra</em></a> and <a href="http://www.ascentaspirations.ca/ascentspring2011.htm" target="_blank"><em>Close to Quitting Time</em>,</a> in which she was awarded first and second  prizes for her poems "Today’s Lesson:
                                 Staying Dry" and "Treading  Water" respectively. Bella Books published her first novel
                                 in the summer  of 2015 and her second in the fall of 2016. Shari has also published
                                 articles  of literary criticism in international peer-reviewed scholarly journals
                                 and  books, including the <em>Atlantic  Literary Review</em>, the<em> Atlantic  Critical Review Quarterly</em>, and <em>Studies in  Women Writers in English</em>.
                              </p>
                              
                              <p><strong>Madison, Tamara</strong><br>
                                 Tamara J. Madison is an author,  poet, performer, and instructor.&nbsp; Her critical and
                                 creative works have been  published in various journals, magazines and anthologies.
                                 She has performed and  recorded her work for stage, television and studio. Madison
                                 holds a BA from  Purdue University and a MFA from New England College. She is currently
                                 professor of English at Valencia College in Orlando, Florida. Her most recent  release
                                 is <a href="http://www.tamarajmadison.com/blog/kentucky_curdled_3_d_release" target="_blank"><em>Kentucky</em> <em>Curdled</em></a>, a collection of poetry and essay. For  more, visit <a href="http://www.tamarajmadison.com/" target="_blank">www.tamarajmadison.com</a>.
                              </p>
                              
                              <p><strong>Mahaffey,  Mandy</strong><br>
                                 Mandy Mahaffey received her M.A. in English (Literary, Cultural,  and Textual Studies)
                                 and Gender Studies at the University of Central Florida  and is active in the local
                                 and national writing community. Outside of the  academic classroom, she has studied
                                 under bestselling authors like Francesca  Lia Block (<em>The Weetzie Bat Books</em>) in writing workshops around the  nation, and maintains close professional contacts
                                 with prolific poets and  writers around the globe. She believes that to be a good
                                 writer, one must be a  good reader; with that in mind, she spends much of her time
                                 pouring over prose  and honing her craft.
                              </p>
                              
                              <p><strong>Hughes,  John Calvin</strong><br>
                                 John Calvin Hughes, originally from Mississippi, holds  a B.S. in Philosophy and an
                                 M.A. in Creative Writing from the University of  Southern Mississippi. He was awarded
                                 a doctorate from the University of South  Florida. His publications include <em>The Novels and Short Stories of Frederick  Barthelme</em>, a critical study; <em>The Shape of Our Luck</em>, a poetry  chapbook, and <em>Twilight of the Lesser Gods</em>, a novel. He has a novel  forthcoming from Second Wind Press. He has taught at Valencia
                                 College for 26  years. Â&nbsp;His publications include the  following:<br>
                                 "Our  Miz Brooks" in <em>Provo Canyon Review</em>, (2013)<br>
                                 "Comes  Good Friday" in <em>Provo Canyon Review,</em> 2013<br>
                                 "Peter  Quince Improvises on <em>Le Sacre du Printemps</em>" and "Logical Love Song" in <em>Clockwise Ca</em>t, 2010<br>
                                 "The  Cantina" in Slipstream.<br>
                                 "Recess"  and "I Forget You Every Day" in <em>Clockwise  Cat</em>, <em>2009</em><br>
                                 "Epiphany,"  "Bad Wednesday," and "Come Closer" in <em>Dead  Mule</em>, Summer 2009<br>
                                 "Brautigan  in Elisinore" in <em>Stirring</em>, March 2009<br>
                                 "Self-Help"  and "What’s All This Stuff?" in <em>Slow  Trains</em>, March 2009<br>
                                 "Hey  You!" in <em>Clockwise Cat</em>, December 2008<br>
                                 "The  Graceland Historical Site" in <em>DecomP</em>&nbsp; <br>
                                 "The Way  It Has To Be," "Confession at St. Augustine," "Subjunctive Blues," and 
                                 ""Departure" in <em>Prick of the Spindle</em><br>
                                 Novel - <em>Killing Rush</em><br>
                                 Poetry Collection - <em>Music from a Farther Room</em> 
                              </p>
                              
                              <p><strong>Ross,  Jesse</strong><br>
                                 Jesse Ross published poetry and fiction in collections including <em>Red Lion Square</em> and <em>McSweeney's</em>. He has worked with the Crealde School of Art, the  Center for Contemporary Dance,
                                 and the Orlando Museum of Art as a writer in the  community. Ross studied literature
                                 and writing at Emerson College and the  University of Edinburgh (UK) and earned his
                                 MFA at the University of Central  Florida, where his creative nonfiction story "Unload"
                                 was nominated for the  Iron Horse Literary Review’s Discovered Voices Award. Jesse
                                 Ross teaches at  Valencia College and in the Youthful Offender's Program with minors
                                 who have  been adjudicated as adults.                    
                              </p>
                              
                              <p><strong>Deyon Williams</strong><br>
                                 Deyon Williams is a writer, poet, playwright and performer currently  living in the
                                 Orlando Area. Deyon, is a  Rollins College graduate in English and a graduate from
                                 New England College MFA  Program (Poetry and Creative writing). He teaches Creative
                                 Writing and English Composition at Valencia College. He also lectures on various 
                                 topics in English at different universities, colleges, and high schools in the Orlando
                                 area. Deyon has performed for  stage and video as a solo and collaborative poet. He
                                 also facilitates creative expressions workshops for youths and adults and volunteers
                                 as a youth mentor and coach for local community organizations.<br><br>
                                 He is the author of <em>The Bible for the Descendant of Slaves</em>, a collection of poems, which won the Laura Van den Berg Fellowship. His current
                                 manuscript, <em>The Dream Chasers</em>, is a collection of poems and short stories that examines the  creative perception
                                 of teenagers growing up in the late ‘90s. He has also authored two plays and a novella,
                                 <em>The Anatomy of 53: the Romeo and Juliet Theory.</em></p>
                              
                              
                              
                           </div>                
                           
                           <div>
                              
                              <h3>Mass Communication</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/mmc/" target="_blank">MMC&nbsp;2004. MASS MEDIA</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/mmc/" target="_blank">MC&nbsp;2100. WRITING FOR MASS COMMUNICATION</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/mmc/" target="_blank">MMC&nbsp;2150. WRITING FOR SOCIAL MEDIA</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/jou/" target="_blank">JOU 1100 NEWS REPORTING</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/jou/" target="_blank">JOU 1400L MEDIA PRODUCTION 1</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/jou/" target="_blank">JOU 1401L &nbsp;MEDIA PRODUCTION 2</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/jou/" target="_blank">JOU 1402L MEDIA PRODUCTION 3</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/jou/" target="_blank">JOU 2200 EDITING AND DESIGN</a></li>
                                 
                                 <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/pur/" target="_blank">PUR&nbsp;2003 &nbsp;INTRODUCTION TO PUBLIC RELATIONS</a></li>
                                 
                              </ul>
                              
                              
                              <h3>Mass Communication Faculty</h3>
                              
                              <p><strong><a href="mailto:bgombash@valenciacollege.edu">William  Gombash</a></strong> - <em><strong>Professor  of Mass Communication</strong></em><br>
                                 407-582-2356
                              </p>
                              
                              <p><strong><a href="mailto:clewis1@valenciacollege.edu">Courtney  Lewis</a></strong> - <em><strong>Professor  of Mass Communication</strong></em><br>
                                 407-582-2234
                              </p>
                              
                              <p><strong><a href="mailto:rnewman@valenciacollege.edu">Rebecca  Newman</a></strong> - <em><strong>Professor  of Journalism</strong></em><br>
                                 407-582-2294
                              </p>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 7, Rm 163 // MC: 3-20</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2433 // 407-582-2370 // 407-582-2444</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:30am to 5:30pm<br>Friday: 8am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <br>
                        
                        
                        
                        <div>
                           
                           <div>Valencia College Creative Writers</div>
                           
                           <div>
                              <a href="https://www.facebook.com/pages/Valencia-College-Creative-Writers/149293765236967?fref=ts" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>
                           
                        </div> 
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-east/programs.pcf">©</a>
      </div>
   </body>
</html>