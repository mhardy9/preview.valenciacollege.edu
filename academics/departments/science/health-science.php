<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science/health-science.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science/">Science</a></li>
               <li>East Campus Science</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <blockquote>
                           
                           <h3>Health Science Adjunct Faculty</h3>
                           
                        </blockquote>            
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>
                                    <strong>Bivins, Tomasena</strong> 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> 
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:tbivins2@valenciacollege.edu">tbivins2@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Carmack, Bonnie </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> 
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:bcarmack@valenciacollege.edu">bcarmack@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Chaves, Rosa </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:rchaves@valenciacollege.edu">rchaves@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Sablad, Lorie </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:lsablad@valenciacollege.edu">lsablad@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Yates, Lorraine </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:lyates2@valenciacollege.edu">lyates2@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        <blockquote>
                           
                           <blockquote>&nbsp;</blockquote>
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science/health-science.pcf">©</a>
      </div>
   </body>
</html>