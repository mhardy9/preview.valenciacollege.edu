<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science/departmentresource.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science/">Science</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../../locations/east/science/arrow.gif" width="8">
                                 <img alt="" height="8" src="../../../locations/east/science/arrow.gif" width="8">  <img alt="" height="8" src="../../../locations/east/science/arrow.gif" width="8">  <img alt="" height="8" src="../../../locations/east/science/arrow.gif" width="8">
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div> 
                                       
                                       <div> 
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>Navigate</div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div> 
                                                   
                                                   
                                                   <div>            
                                                      
                                                      <h2>Division, Campus, and College Resources</h2>
                                                      
                                                      
                                                      <h3>Resources available to Faculty and Staff</h3>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Calendars</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="../../../locations/calendar/index.html">College Calendars</a></li>
                                                                     
                                                                     <li>
                                                                        <a href="../../../locations/calendar/index.html">Final Exam Schedule</a> (Flex &amp; Summer finals exams given at the last class.)
                                                                     </li>
                                                                     
                                                                     <li><a href="../../../locations/calendar/index.html">Withdrawal and No Show Deadlines</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>Human Resources</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="../../../locations/HR/benefits.html">Benefits</a></li>
                                                                     
                                                                     <li><a href="../../../locations/HR/compensation.html">Compensation</a></li>
                                                                     
                                                                     <li><a href="../../../locations/human-resources/index.html">Employment Opportunities</a></li>
                                                                     
                                                                     <li>
                                                                        <a href="../../../locations/human-resources/index.html">Human Resources Homepage</a>                      
                                                                     </li>
                                                                     
                                                                  </ul>                  
                                                                  <h3>Forms</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     
                                                                     <li><a href="../../../locations/employees/wordprocessing/default.html">Word Processing Forms</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>Biology Lab Forms </h3>                
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>
                                                                        
                                                                        <div><a href="documents/laboratorysafetyrulesandagreementrevised.pdf">Laboratory Safety Rules and Agreements</a></div>
                                                                        
                                                                     </li>
                                                                     
                                                                     <li><a href="documents/answersheet-labpractical.pdf">A-B Answer Sheet </a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>Chemistry Lab Forms</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="documents/laboratorysafetyrulesandagreementrevised.pdf">Laboratory Safety Rules and Agreements </a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>&nbsp; </h3>
                                                                  
                                                                  <h3>&nbsp;</h3>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Handbooks</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="../../../locations/faculty/resources/east/index.html">East Adjunct Faculty Handbook</a></li>
                                                                     
                                                                     <li><a href="http://preview.valenciacollege.edu/pdf/studenthandbook.pdf">Student Handbook</a></li>
                                                                     
                                                                     <li><a href="documents/valencia-study-abroad-student-handbook.pdf">Study Abroad Handbook</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>Library Resources </h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>
                                                                        <a href="http://www.linccweb.org/linccsearchdirect?setting_key=29x02">East Campus Library Catalog</a> 
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3><strong>Collegewide Information</strong></h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="../../../locations/catalog/index.html">College Catalog</a></li>
                                                                     
                                                                     <li>
                                                                        <a href="../../../locations/contact/directory.html">Phone Directory</a> 
                                                                     </li>
                                                                     
                                                                     <li><a href="../../../locations/generalcounsel/default.html">Policy Manual </a></li>
                                                                     
                                                                     <li><a href="../../../locations/refund/index.html">Refund Policy</a></li>
                                                                     
                                                                     <li><a href="http://www.valencia.org/">Valencia Foundation</a></li>
                                                                     
                                                                     <li><a href="../../../locations/students/disputes/index.html" target="_blank">Student Conflict Resolution</a></li>
                                                                     
                                                                     <li>
                                                                        <a href="documents/memostothedeans.pdf">Class Attendance and Financial Aid</a> 
                                                                     </li>
                                                                     
                                                                     <li><a href="../../../locations/students/student-services/index.html">Student Services</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3><strong>Division Action Plan </strong></h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="documents/sciencedeptunitplan2016-2017.pdf">2016-2017</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h3>Classroom Management Tips</h3>                    
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="../../../locations/about/ferpa/index.html">FERPA</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>            
                                                      
                                                      
                                                      <p><a href="departmentresource.html#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>          
                                                   <div><img alt="" height="1" src="../../../locations/east/science/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/east/science/spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>    
                                       <div><img alt="" height="1" src="../../../locations/east/science/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/east/science/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science/departmentresource.pcf">©</a>
      </div>
   </body>
</html>