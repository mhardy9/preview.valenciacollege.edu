<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science/chemistry.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science/">Science</a></li>
               <li>East Campus Science</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Chemistry Full-Time Faculty</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>
                                    <p><strong>Becker, Renee </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2893</div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-316</div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:pfowler1@atlas.valenciacollege.edu">rbecker2@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Sharma, Vasudha</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2651</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-315</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:vsharma@valenciacollege.edu">vsharma@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Tenery, Daeri </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2483</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>4-238</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:dtenery@valenciacollege.edu">dtenery@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Vagle, Angelica </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2794</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-314</div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:avagle2@valenciacollege.edu">avagle2@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>Chemistry Adjunct Faculty</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Ateeq, Humayun</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:hateeq@valenciacollege.edu">hateeq@valenciacollege.edu</a><br>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Carey, Donell </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:dcarey4@valenciacollege.edu">dcarey4@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <p><strong>Cuevas, Sonia </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:scuevas@valenciacollege.edu">scuevas@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Fowler, Patrick </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:pfowler1@atlas.valenciacollege.edu">pfowler1@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Jamison, Susanne (Su)</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:sjamison2@valenciacollege.edu">sjamison2@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Kuchma, Melissa </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:mkuchma@valenciacollege.edu">mkuchma@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Oztec, M. Tonguc</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:moztek@valenciacollege.edu">moztek@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Prasad-Permaul, Vanessa </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:vprasadpermaul@valenciacollege.edu">vprasadpermaul@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Rios, Orlando </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:orios2@valenciacollege.edu">orios2@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>&nbsp;</h2>
                        
                        <blockquote>
                           
                           <blockquote> 
                              
                              
                           </blockquote>
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science/chemistry.pcf">©</a>
      </div>
   </body>
</html>