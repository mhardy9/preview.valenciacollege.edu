<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Science</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Dean of Science </h3>
                        
                        <p><a href="../../../locations/contact/Search_Detail2.cfm-ID=jsnyder38.html">Jennifer Snyder            </a></p>
                        
                        <h3>Administrative Staff</h3>
                        
                        <p>Tonaysha Askew</p>
                        
                        <p>Kelsey Bryceson</p>
                        
                        <p>Zenobia Aviles</p>
                        
                        <h3>Lab Coordinators</h3>
                        
                        <p><strong>Heidi Haghighat<a href="mailto:corcutt@valenciacollege.edu"><br>
                                 </a> </strong>Anatomy/Physiology/Microbiology <strong>- </strong>407-582-2750
                        </p>
                        
                        <p><strong>Krista Verali <br>
                              </strong>Vannice Biology/Geology/Greenhouse <strong>- </strong>407-582-2729
                        </p>
                        
                        <p><strong>Ryan Hodges <br>
                              </strong>Chemistry/Physics<strong> - </strong>407-582-2339
                        </p>
                        
                        
                        <h3><strong>Academic 
                              Programs</strong></h3>
                        
                        <ul>
                           
                           <li>
                              <a href="biology.html">Biology</a> 
                           </li>
                           
                           <li>
                              <a href="chemistry.html">Chemistry</a> 
                           </li>
                           
                           <li>
                              <a href="earthscience.html">Earth Science (Geology &amp; Oceanography)</a> 
                           </li>
                           
                           <li>
                              <a href="physics.html">Physics (Physical Sciences)</a> 
                           </li>
                           
                        </ul>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <a href="index.html"> East Campus Science</a>
                              
                              
                           </div>
                           <br>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-2311</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:eacsciencedept@valenciacollege.edu">eacsciencedept@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday-Friday 8am-6:30pm<br>Summer hours<br>Monday-Thursday 8am-6:30pm<br>Friday 8am-12pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science/index.pcf">©</a>
      </div>
   </body>
</html>