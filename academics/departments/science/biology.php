<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus Science | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/science/biology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/science/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>East Campus Science</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/science/">Science</a></li>
               <li>East Campus Science</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <blockquote>
                           
                           <h3>Biology Full-Time Faculty</h3>
                           
                        </blockquote>            
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div><strong><font color="#000000"> Belcher,&nbsp;Jim</font></strong></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2108</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-125</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 <div><a href="http://eastlrc.valenciacollege.edu/faculty/kbm/jim.htm">http://eastlrc.valenciacollege.edu/faculty/kbm/jim.htm</a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:jbelcher@valenciacollege.edu">jbelcher@valenciacollege.edu 
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Cravaritis, Candace </strong></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2131</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-120</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:ccravaritis@valenciacollege.edu">ccravaritis@valenciacollege.edu 
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Chu, Francie </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2452</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room</strong></div>
                                 
                                 <div>8-217</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:fchu1@valenciacollege.edu">fchu1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Falconer, Regina </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2025</div> 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-129</div> 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:kmalmos@valenciacollege.edu">rfalconer@valenciacollege.edu </a></div> 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Ingram, Elizabeth </strong></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2771</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-124</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 <div><a href="http://eastlrc.valenciacollege.edu/faculty/eingram/">http://eastlrc.valenciacollege.edu/faculty/eingram/</a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:eingram@valenciacollege.edu">eingram@valenciacollege.edu 
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Luther, Rita </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2222</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-130</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:rluther1@valenciacollege.edu">rluther1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Malmos, Keith </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2805</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-131</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 <div><a href="http://eastlrc.valenciacollege.edu/faculty/kbm/keith.htm">http://eastlrc.valenciacollege.edu/faculty/kbm/keith.htm</a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:kmalmos@valenciacollege.edu">kmalmos@valenciacollege.edu 
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Myers, Steve </strong></div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2205</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-123</div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 <div><a href="http://faculty.valenciacollege.edu/smyers/default.htm">http://faculty.valenciacollege.edu/smyers/default.htm</a></div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:smyers@valenciacollege.edu">smyers@valenciacollege.edu 
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Schreiber, Melissa </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2246</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-128</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:mschreiber@valenciacollege.edu%20">mschreiber@valenciacollege.edu </a> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Tardif, Gilman</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2856</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-131</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:gtardif@valenciacollege.edu%20">gtardif@valenciacollege.edu</a> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Vazquez, Olga </strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Phone:</strong></div>
                                 
                                 <div>407-582-2202</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Building/Room:</strong></div>
                                 
                                 <div>1-143</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Homepage:</strong></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:ovazque1@valenciacollege.edu">ovazque1@valenciacollege.edu</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Biology Adjunct Faculty</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Bartoe, Colin </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:cbartoe@valenciacollege.edu">cbartoe@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Bawaney, Noorulain</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:nbawaney@valenciacollege.edu">nbawaney@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Beg, Sameena </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:sbeg1@valenciacollege.edu">sbeg1@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Berlin, Linda</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Email</strong>:
                                 </div>
                                 
                                 <div><a href="mailto:lberlin1@valenciacollege.edu">lberlin1@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Carey, Donell </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email: </strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><u><a href="mailto:dcarey4@valenciacollege.edu">dcarey4@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Chaves, Rosa </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:rchaves@valenciacollege.edu">rchaves@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Cohill, Paul </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:pcohill@valenciacollege.edu">pcohill@valenciacollge.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong> Febres, Fernando</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:ffebres@valenciacollege.edu">ffebres@valenciacollge.edu</a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Harris, Elizabeth </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Email</strong>:
                                 </div>
                                 
                                 <div><a href="mailto:eharris25@valenciacollege.edu">eharris25@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Hatfield, Meghan</strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div><a href="mailto:mhatfield8@valenciacollege.edu"><u>mhatfield8@valenciacollege.edu</u></a></div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Iglesias, Melissa </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:mvantrump2@atlas.valenciacollege.edu">mvantrump2@valenciacollege.edu </a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Mason, Elizabeth </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:emason1@valenciacollege.edu">emason1@.valenciacollege.edu</a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <p><strong>Miller, Maribeth </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:mmiller@valenciacollege.edu">mmiller@valenciacollege.edu </a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Murrell, Shar-Jani </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             <div><u><a href="mailto:smurrell@valenciacollege.edu">smurrell@valenciacollege.edu</a></u></div>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Pierre, Kathy </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div>
                                    <a href="mailto:kpierre13@valenciacollege.edu">kpierre13@avalenciacollege.edu </a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    <p><strong>Schultz, Edward </strong></p>
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div> <a href="mailto:eshultz@valenciacollege.edu">eschultz@valenciacollege.edu</a> 
                                 </div>
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Sekar (Venkataraman), Raji </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:rvenkataraman@valenciacollege.edu">rvenkataraman@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Torres Arroyo, Nelson </strong></div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Email:</strong></div>
                                 
                                 <div><a href="mailto:ntorresarroyo@valenciacollege.edu">ntorresarroyo@valenciacollege.edu</a></div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <blockquote>
                           
                           <blockquote>&nbsp;</blockquote>
                           
                        </blockquote>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/science/biology.pcf">©</a>
      </div>
   </body>
</html>