<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Communications | West Campus | Valencia College</title>
      <meta name="Description" content="West Communications | West Campus">
      <meta name="Keywords" content="college, school, educational, communications, west, campus">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-west/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-west/">Communications West</a></li>
               <li>Communications</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <h3>Contact Information</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>&nbsp;</div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 231</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1">407-582-1616</span><span>&nbsp;</span>// Fax:<span>&nbsp;</span><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1">407-582-1676</span></div>
                                    
                                    <div>
                                       <hr><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1"></span></div>
                                    
                                    <h3><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1">Faculty</span></h3>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;<em><strong>Full-Time Fauclty</strong></em></td>
                                    
                                    <td><strong>&nbsp;Discipline</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Affricano, Armand</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bailey, Ronda</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Baldridge-Hale, Kate</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Barlow, Mailin&nbsp;</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bartee, Patrick</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bentham, Claudine</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Blewitt, Angela</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Brand, Jayanti</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Brown, Nick</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Carruth, Debra</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Colwell, Donna</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Coombes, Esther</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Council, Tracey</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Cowden, Karen</td>
                                    
                                    <td>&nbsp;English/English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Creighton, John&nbsp;</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Dalle Molle, Gina&nbsp;</td>
                                    
                                    <td>&nbsp;English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Darden, Rudy</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>DiLiberto, Stacey</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Freeman, David</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Gibson, Lauren</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Holzer, Mayra</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Johnson, Melissa</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Lima, Michelle</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>McWhorter, Robert</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Murray, Karen</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Orsini, Diane</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Perrell, Beth</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Phillips, Neal</td>
                                    
                                    <td>Englsih</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Powell, Aaron</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Ramos-Cotto, Sabrina</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Sebacher, Jill</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Sebacher, Neil</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Sedik, Dawn</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Simmons, Robin</td>
                                    
                                    <td>Englsih</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Spottke, Nicole</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Tan, Tina</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Toole, Becca</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Vrhovac, James</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Wells, Kathryn</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Wish, Wendy</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Zaleckaite, Milena</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Zuromski, Jackie</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Part-Time Faculty</strong></td>
                                    
                                    <td><strong>Discipline</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Branciforte,Rosie</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Brophy, Jennifer</td>
                                    
                                    <td>&nbsp;Englsih</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Campbell, Brian</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Carpenter, Julia</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Chambers,Yasmyn</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Covert, Pam</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Douglas, Anne</td>
                                    
                                    <td>&nbsp;English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Fama, Christopher</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Fleming, James</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Foley, Stephanie</td>
                                    
                                    <td>&nbsp;English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Golphin, Vincent</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Goodwin, Bernard</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Hamilton, Regina</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Heller, Michael</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Jackson, Doreen</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Jones, Pam</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Kokoulin, Anna</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Korpov, Roz</td>
                                    
                                    <td>English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Krause, Michelle</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Looney, Erin</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Looney, Melissa</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Luse, Bill</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Moniz, Michael</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Nelson, Rhonda</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Phillips, Robert&nbsp;</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Rodriguez, DJ</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Rudden,Michele</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Sanchez, Louis</td>
                                    
                                    <td>&nbsp;English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Santiago, Yesenia&nbsp;</td>
                                    
                                    <td>&nbsp;English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Schulz, Deanne&nbsp;</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Schwab, Amy</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Selitto, Janet</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Smith, Andrew</td>
                                    
                                    <td>&nbsp;English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Spear, Jennifer</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;Spero, Anthony</td>
                                    
                                    <td>&nbsp;Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Strong-RobinsonSmith,Danielle</td>
                                    
                                    <td>English</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Vega, Jamie</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Wright, Geni</td>
                                    
                                    <td>Speech</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                           <h3>&nbsp;</h3>
                           
                           <div>&nbsp;</div>
                           
                        </div>
                        <a id="ccw" name="ccw"></a></div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-west/faculty.pcf">©</a>
      </div>
   </body>
</html>