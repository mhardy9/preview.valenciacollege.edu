<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Courses | Communications | West Campus | Valencia College  | Valencia College</title>
      <meta name="Description" content="Courses | Communications | West Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, west, communications, courses">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-west/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-west/">Communications West</a></li>
               <li>Courses </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <h2>Course Catalog</h2>
                           
                           <ul>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#ccw">English Composition and Creative Writing</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#esl">English as a Second Language for Academic Purposes</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#journalism">Journalism</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#litfilm">Literature and Film</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#reading">Reading</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#speech">Speech</a></li>
                              
                           </ul>
                           
                           <p align="left">&nbsp;</p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0017. DEVELOPMENTAL READING AND WRITING I.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>4</strong></td>
                                    
                                    <td align="center"><strong>4</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: Appropriate score on PERT or other approved assessment. An integrated
                                          reading and writing course designed for students scoring 83 or below on the reading
                                          portion of the PERT exam and scoring 89 or below on the writing portion of the PERT
                                          exam. The course will focus on reading comprehension, vocabulary skills, grammar,
                                          and paragraph development. In addition, the course will address the connection between
                                          reading and writing through reading response activities.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0025. DEVELOPMENTAL WRITING II.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: Minimum grade of C in ENC 0015C or appropriate score on PERT or other
                                          approved assessment. Prepares students to plan and write grammatically correct paragraphs
                                          and short essays. Upon successful completion, students have met the writing requirement
                                          for entry into<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>.&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%200025">ENC&nbsp;0025</a>&nbsp;credit does not apply towards any associate degree.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0027. DEVELOPMENTAL READING AND WRITING II.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>4</strong></td>
                                    
                                    <td align="center"><strong>4</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: Minimum grade of C in&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%200017">ENC&nbsp;0017</a>&nbsp;or appropriate score on PERT or other approved assessment. An integrated reading
                                          and writing course designed for students scoring 50-83 on PERT reading and a 90-98
                                          on PERT writing; or 84-103 on PERT reading and 50-89 on PERT writing; or 84-103 on
                                          PERT reading and 90-98 on PERT writing. The course will focus on reading comprehension,
                                          vocabulary skills, grammar, and essay writing. In addition, the course will address
                                          the connection between reading and writing through reading response activities.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0030. TECHNICAL COMMUNICATIONS.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>1.5</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Emphasis on clear, simple and precise English. Writing of business letters, office
                                          memos, technical reports, proposals and recommendations. Practice in collecting and
                                          organizing data and preparing report formats. Includes oral reports and interview
                                          techniques.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0055. DEVELOPMENTAL ENGLISH MODULE I.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>1</strong></td>
                                    
                                    <td align="center"><strong>1</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: Score of 90-98 on PERT and department approval. This course provides
                                          specialized instruction in developmental writing concepts to prepare students for&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>. Topics include the components necessary to write a unified, well-developed paragraph,
                                          such as topic sentences, use of major and minor development, coherence, unity and
                                          use of logical organizational patterns. Other topics include the conventions of standard
                                          American English as appropriate for academic writing. Upon completion of the PERT
                                          diagnostic or a Communications division diagnostic, students will be assigned one
                                          or two topic modules. Students must complete each module with a grade of C or higher.
                                          This course does not apply toward Communications requirements in general education
                                          or toward any associate degree.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;0056. DEVELOPMENTAL ENGLISH MODULE I.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>1</strong></td>
                                    
                                    <td align="center"><strong>1</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: Score of 90-98 on PERT and department approval. This course provides
                                          specialized instruction in developmental writing concepts to prepare students for&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>. Topics include the components necessary to write a unified, well-developed paragraph,
                                          such as topic sentences, use of major and minor development, coherence, unity and
                                          use of logical organizational patterns. Other topics include the conventions of standard
                                          American English as appropriate for academic writing. Upon completion of the PERT
                                          diagnostic or a Communications division diagnostic, students will be assigned one
                                          or two topic modules. Students must complete each module with a grade of C or higher.
                                          This course does not apply toward Communications requirements in general education
                                          or toward any associate degree.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;1101. FRESHMAN COMPOSITION I.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisites: Score of 103 on writing component of PERT or equivalent score on other
                                          state-approved entry test or minimum grade of C in ENC 0025C or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=EAP%201640C">EAP&nbsp;1640C</a>, and a score of 106 on reading component of PERT or equivalent score on other state-approved
                                          entry test or minimum grade of C in REA 0017C or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=EAP%201620C">EAP&nbsp;1620C</a>. Development of essay form, including documented essay; instruction and practice
                                          in expository writing. Emphasis on clarity of central and support ideas, adequate
                                          development, logical organization, coherence, appropriate citing of primary and/or
                                          secondary sources, and grammatical and mechanical accuracy. Gordon Rule course in
                                          which the student is required to demonstrate college-level writing skills through
                                          multiple assignments. Minimum grade of C is required if&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>&nbsp;is used to satisfy Gordon Rule and General Education Requirements.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;1101H. FRESHMAN COMPOSITION I - HONORS.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Same as&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>&nbsp;with honors content. Honors program permission required.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;1102. FRESHMAN COMPOSITION II.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite: ENC1101 or 1101H with minimum grade of C Application of skills learned
                                          in&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>. Emphasis on style; use of library; reading and evaluating available sources; planning,
                                          writing, and documenting short research paper. Gordon Rule course in which the student
                                          is required to demonstrate college-level writing skills through multiple assignments.
                                          Minimum grade of C required if&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201102">ENC&nbsp;1102</a>&nbsp;is used to satisfy Gordon Rule and general education requirements.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;1102H. FRESHMAN COMPOSITION II - HONORS.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Same as&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201102">ENC&nbsp;1102</a>&nbsp;with honors content. Honors program permission required.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;1210. TECHNICAL COMMUNICATION.</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite:&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>&nbsp;or 1101H. Emphasis on clear, simple and precise English. Writing of business letters,
                                          office memos, technical reports, proposals and recommendations. Practice in collecting
                                          and organizing data and preparing report formats. Includes oral reports and interview
                                          techniques.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p><strong>ENC&nbsp;2341. ADVANCED CREATIVE WRITING -LITERARY MAGAZINE..</strong></p>
                                       
                                    </td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>3</strong></td>
                                    
                                    <td align="center"><strong>0</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="4">
                                       
                                       <p>Prerequisite:&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC&nbsp;1101</a>&nbsp;or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101H">ENC&nbsp;1101H</a>&nbsp;or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=IDH%201110">IDH&nbsp;1110</a>&nbsp;or departmental approval. Writing in a genre of the student's choice. Literary criticism
                                          and production of College literary magazine.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h2>&nbsp;</h2>
                           
                           <p align="right"><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#top">TOP</a></p>
                           
                           <p align="left"><a name="journalism"></a></p>
                           
                           <h2>Journalism</h2>
                           
                           <p>&nbsp;</p>
                           
                           <h3>JOU 1100, News Reporting</h3>
                           
                           <blockquote>
                              
                              <p align="left">In-depth reporting with emphasis on modern news and feature stories. Stresses elements
                                 of news: sources, structure, style, and mechanics of production. Recommend students
                                 also enroll in College Newspaper.
                              </p>
                              
                           </blockquote>
                           
                           <h3>JOU 1400L, College Newspaper</h3>
                           
                           <blockquote>
                              
                              <p align="left">Laboratory course for production of College newspaper. Includes reporting, editing,
                                 business, makeup, and other phases of newspaper production.
                              </p>
                              
                           </blockquote>
                           
                           <h3>JOU 1404L, College Newspaper</h3>
                           
                           <blockquote>
                              
                              <p align="left">Laboratory Course for production of College newspaper. Includes reporting, editing,
                                 business, makeup, and other phases of newspaper production.
                              </p>
                              
                           </blockquote>
                           
                           <h3>JOU 2402L, College Newspaper</h3>
                           
                           <blockquote>
                              
                              <p align="left">Laboratory course for production of College newspaper. Includes reporting, editing,
                                 business, makeup, and other phases of newspaper production.
                              </p>
                              
                           </blockquote>
                           
                           <h3>JOU 2403L, College Newspaper</h3>
                           
                           <blockquote>
                              
                              <p align="left">Laboratory course for production of College newspaper. Includes reporting, editing,
                                 business, makeup, and other phases of newspaper production.
                              </p>
                              
                           </blockquote>
                           
                           <h3>MMC 1000, Survey of Mass Communications</h3>
                           
                           <blockquote>
                              
                              <p align="left">Introduction to history, development and current practices of media of mass communication.
                                 Presents functions of newspapers, magazines, radio, television, and advertising in
                                 light of responsibilities to public.
                              </p>
                              
                           </blockquote>
                           
                           <h3>MMC 2100, Writing for Mass Communications</h3>
                           
                           <blockquote>
                              
                              <p align="left">Introduction to history, development and current practices of media of mass communication.
                                 Presents functions of newspapers, magazines, radio, television, and advertising in
                                 light of responsibilities to public.
                              </p>
                              
                           </blockquote>
                           
                           <p align="right"><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#top">TOP</a></p>
                           
                           <p align="left"><a name="litfilm"></a></p>
                           
                           <h2>Literature and Film</h2>
                           
                           <p>&nbsp;</p>
                           
                           <h3>ENG 2100, Intro To Film</h3>
                           
                           <blockquote>
                              
                              <p align="left">Techniques of American, British, and foreign-language films. Emphasis on films of
                                 30's through contemporary cinema.
                              </p>
                              
                           </blockquote>
                           
                           <h3>LIT 2090, Contemporary Literature</h3>
                           
                           <blockquote>
                              
                              <p align="left">Prerequisite: ENC 1101 or 1101H or IDH 1110. Representative works of contemporary
                                 poets, novelists, short story writers, dramatists, and non-fiction writers especially
                                 significant in the last thirty years. Emphasis on issues and ideas related to present
                                 and future. Gordon Rule course which requires 6,000 words of writing. Minimum grade
                                 of C required if LIT 2090 is used to satisfy Gordon Rule and general education requirements.
                              </p>
                              
                           </blockquote>
                           
                           <h3>LIT 2110, Survey in World Literature:&nbsp; Beginning Through Renaissance</h3>
                           
                           <blockquote>
                              
                              <p align="left">Prerequisite: ENC 1101 or 1101H or IDH 1110. Major poetry, fiction, drama and essays.
                                 Gordon Rule course which requires 6,000 words of writing. Minimum grade of C required
                                 if LIT 2120 is used to satisfy Gordon Rule and general education requirements.
                              </p>
                              
                           </blockquote>
                           
                           <h3>LIT 2120, Survey in World Literature:&nbsp; Enlightenment to Present</h3>
                           
                           <blockquote>
                              
                              <p align="left">Prerequisite: ENC 1101 or 1101H. Major poetry, fiction, drama and essays. Gordon Rule
                                 course which requires 6,000 words of writing. Minimum grade of C required if LIT 2120
                                 is used to satisfy Gordon Rule and general education requirements.
                              </p>
                              
                           </blockquote>
                           
                           <h3>LIT 2120H, Survey in World Literature:&nbsp; Enlightenment to Present</h3>
                           
                           <blockquote>
                              
                              <p align="left">Same as LIT 2120 with honors content. Honors program permission required.</p>
                              
                           </blockquote>
                           
                           <p>&nbsp;</p>
                           
                           <h3>LIT 2174, Multimedia Literature and the Holocaust</h3>
                           
                           <blockquote>
                              
                              <p>Prerequisite: Minimum grade of C in ENC 1101 or ENC 1101H<br>or IDH 1110. This course explores literary characteristics inherent in various media
                                 including (but not limited to) Holocaust-related historical text,<br>documentary film, comics (graphic narrative), survivor narratives,<br>pre-and post-Nazi art and contemporary major motion pictures.<br>The examination includes critical analyses of textual, visual, syntactical,<br>mechanical and thematic conventional similarities found in traditional<br>textual“literature”and in the structure, syntax and language of visual<br>media. Gordon Rule course in which the student is required to<br>demonstrate college-level writing skills through multiple writing<br>assignments. Minimum grade of C required if LIT 2174 is used to<br>satisfy Gordon Rule and general education requirements.
                              </p>
                              
                           </blockquote>
                           
                           <p align="right"><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#top">TOP</a></p>
                           
                           <p align="left"><a name="reading"></a></p>
                           
                           <h2>Reading</h2>
                           
                           <p>&nbsp;</p>
                           
                           <h3>REA 0001, College Preparatory Reading I</h3>
                           
                           <blockquote>
                              
                              <p align="left">Prerequisite:&nbsp; Appropriate score on CPT or other approved assessment.&nbsp; Corequisite:&nbsp;
                                 REA 0001L.&nbsp; Study of literal and critical comprehension skills with emphasis on literal
                                 skills and organization patterns of information.&nbsp; Includes strategies for vocabulary
                                 development.&nbsp; Minimum grade of C required for successful completion.&nbsp; Upon successful
                                 completion, degree-seeking student must take REA 0002 and REA 0002L or REA 0002C.&nbsp;
                                 REA 0001 credit does not apply toward any associate degree.&nbsp; (Special Fee: $35.00)
                              </p>
                              
                           </blockquote>
                           
                           <h3>REA 0002, College Preparatory Reading II</h3>
                           
                           <blockquote>
                              
                              <p align="left">Prerequisite:&nbsp; Minimum grade of C in REA 0001 and REA 0001L or REA 0001C or appropriate
                                 score on CPT or other approved assessment.&nbsp; Corequisite:&nbsp; REA 0002L.&nbsp; Review and reinforcement
                                 of skills covered in REA 0001 and REA 0001L.&nbsp; Emphasis on additional critical comprehension
                                 skills.&nbsp; Strategies for vocabulary development.&nbsp; Minimum grade of C required for successful
                                 completion.&nbsp; Upon successful completion, student has met the reading requirement for
                                 entry into ENC 1101.&nbsp; Students who pass with scores equivalent to 83 - 85 on CPT are
                                 strongly encouraged to enroll in REA 1106.&nbsp; REA 0002 credit does not apply toward
                                 any associate degree.&nbsp; (Special Fee: $35.00)
                              </p>
                              
                           </blockquote>
                           
                           <p align="right"><a href="https://valenciacollege.edu/departments/west/communications/courses.cfm#top">TOP</a></p>
                           
                           <p align="left"><a name="speech"></a></p>
                           
                           <h2>Speech</h2>
                           
                           <p>&nbsp;</p>
                           
                           <h3>SPC 1016, Informal Communication</h3>
                           
                           <blockquote>Study and application of communication principles to remove verbal and non-verbal
                              barriers. Emphasis on role playing, simulated conflicts, and dynamics of group discussion.
                           </blockquote>
                           
                           <h3>SPC 1600, Fundamentals Of Speech</h3>
                           
                           <blockquote>Principles of oral communication common to speaking and listening. Emphasis on listening
                              techniques, preparation and delivery techniques for extemporaneous speaking.
                           </blockquote>
                           
                           <h3>SPC 1600H, Fundamentals of Speech Honors</h3>
                           
                           <blockquote>Same as SPC 1600 with honors content. Honors program permission required.</blockquote>
                           
                           <h3>SPC 1700, Cross Cultural Communication</h3>
                           
                           <blockquote>Study and application of awareness skills for communication with people of other cultures.
                              Upon completion, student uses problem analysis skills when communicating with people
                              of other cultures.
                           </blockquote>
                           &nbsp;
                        </div>
                        <a id="ccw" name="ccw"></a></div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-west/courses.pcf">©</a>
      </div>
   </body>
</html>