<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links | Communications | West Campus | Valencia College  | Valencia College</title>
      <meta name="Description" content="Links | Communications | West Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, west, communications, links">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-west/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-west/">Communications West</a></li>
               <li>Links </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           
                           <h2>Index of Links</h2>
                           
                           <ul>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/links.cfm#ccw">Composition and Creative Writing</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/links.cfm#esl">English as a Second Language for Academic Purposes</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/links.cfm#journalism">Journalism</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/links.cfm#litfilm">Literature and Film</a></li>
                              
                              <li><a href="https://valenciacollege.edu/departments/west/communications/links.cfm#speech">Speech</a></li>
                              
                           </ul>
                           <a id="ccw"></a>
                           
                           <h3>Composition and Creative Writing</h3>
                           
                           <ul>
                              
                              <li>Dictionaries and Thesauruses</li>
                              
                              <ul>
                                 
                                 <li><a href="http://dictionary.cambridge.org/">Cambridge Dictionaries Online</a></li>
                                 
                                 <li><a href="http://m-w.com/">Merriam-Webster Online</a></li>
                                 
                                 <li><a href="http://bartelby.com/59/">The New Dictionary of Cultural Literacy</a></li>
                                 
                                 <li><a href="http://www.visualthesaurus.com/">The Visual Thesaurus</a></li>
                                 
                                 <li><a href="http://wordcentral.com/">Word Central at Merriam-Webster</a></li>
                                 
                                 <li><a href="http://www.writeexpress.com/online2.html">WriteExpress Rhyming Dictionary for Poetry and Songwriting</a></li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Essays</li>
                              
                              <ul>
                                 
                                 <li><a href="http://owl.english.purdue.edu/handouts/general/index.html">Purdue's Online Writing Lab [General Writing Concerns]</a></li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Dictionaries and Thesauruses</li>
                              
                              <ul>
                                 
                                 <li><a href="http://aliscot.com/bigdog/">Big Dog's Grammar, A Bare Bones Guide to English</a></li>
                                 
                                 <li><a href="http://chompchomp.com/">Grammar Bytes! Interactive Grammar Review</a></li>
                                 
                                 <li><a href="http://www.grammarphobia.com/">Grammarphobia [especially this essay on grammar myths]</a></li>
                                 
                                 <li><a href="http://owl.english.purdue.edu/handouts/grammar/">Purdue's Online Writing Lab [Grammar, Punctuation, and Spelling Index]</a></li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Research</li>
                              
                              <ul>
                                 
                                 <li><a href="http://turnitin.com/research_site/e_home.html">Research Resources at turnitin.com</a></li>
                                 
                              </ul>
                              
                           </ul>
                           <a id="esl"></a>
                           
                           <h3>English as a Second Language for Academic Purposes</h3>
                           
                           <ul>
                              
                              <li><a href="http://www.eslcafe.com/">Dave's ESL Cafe</a></li>
                              
                           </ul>
                           <a id="journalism"></a>
                           
                           <h3>Journalism</h3>
                           
                           <ul>
                              
                              <li><a href="http://valenciavoice.com/">The Valencia Voice</a></li>
                              
                           </ul>
                           <a id="litfilm"></a>
                           
                           <h3>Literature</h3>
                           
                           <ul>
                              
                              <li><a href="http://www.ipl.org/">The Internet Public Library</a></li>
                              
                              <li><a href="https://valenciacollege.edu/phoenix/">Phoenix Magazine</a></li>
                              
                              <li><a href="http://www.gutenberg.org/wiki/Main_Page">Project Gutenberg</a></li>
                              
                              <li><a href="http://www.readprint.com/">Read Print</a></li>
                              
                           </ul>
                           <a id="speech"></a>
                           
                           <h3>Speech</h3>
                           
                           <ul>
                              
                              <li><a href="http://www.americanrhetoric.com/">American Rhetoric</a></li>
                              
                           </ul>
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-west/links.pcf">©</a>
      </div>
   </body>
</html>