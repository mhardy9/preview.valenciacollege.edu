<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources | Communications | West Campups | Valencia College  | Valencia College</title>
      <meta name="Description" content="Resources | Communications | West Campups | Valencia College">
      <meta name="Keywords" content="college, school, educational, west, communications, resources">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-west/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li><a href="/academics/departments/communications-west/">Communications West</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <h2>Know Where to Get Help</h2>
                           
                           <h3>People</h3>
                           
                           <ul>
                              
                              <li>Dr. Elizabeth Renn, Dean</li>
                              
                              <li>Sharon Sorrough</li>
                              
                              <li>MaryBeth Moore Zocco</li>
                              
                           </ul>
                           
                           <h3>Places</h3>
                           
                           <ul>
                              
                              <li><a href="https://valenciacollege.edu/west/communications/lab/">Communications Center</a></li>
                              
                              <li><a href="https://valenciacollege.edu/learning-support/communications/writing.cfm/">Writing Center</a></li>
                              
                              <li><a href="https://valenciacollege.edu/library/west/cal/">Computer Lab</a></li>
                              
                              <li><a href="https://valenciacollege.edu/library/">West Campus Library</a></li>
                              
                              <li><a href="/students/tutoring/">Tutoring Center</a></li>
                              
                           </ul>
                           
                           <h3>Handouts</h3>
                           
                           <ul>
                              
                              <li>Practice CLAST</li>
                              
                              <li>Practice FCBSET [Writing]</li>
                              
                              <li>Practice FCBSET [Reading]</li>
                              
                              <li>Sample Topics for FCBSET essay</li>
                              
                              <li>How to Write a 50-Minute Essay for FCBSET</li>
                              
                           </ul>
                           &nbsp;
                        </div>
                        <a id="ccw" name="ccw"></a></div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-west/resources.pcf">©</a>
      </div>
   </body>
</html>