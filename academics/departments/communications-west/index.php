<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Communications | West Campus | Valencia College</title>
      <meta name="Description" content="Communications | West Campus">
      <meta name="Keywords" content="college, school, educational, communications, west">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/departments/communications-west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/communications-west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Communications</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/departments/">Departments</a></li>
               <li>Communications West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               <div class="container margin-60" role="main">
                  		
                  <h2 style="margin: 0.83em 0px; color: #000000; text-transform: none; text-indent: 0px; letter-spacing: normal; font-family: Arial, Helvetica, sans-serif; font-size: 1.5em; font-style: normal; word-spacing: 0px; white-space: normal; orphans: 2; widows: 2; background-color: #ffffff; font-variant-ligatures: normal; font-variant-caps: normal; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">West Campus Communications Division</h2>
                  
                  <table class="table ">
                     
                     <tbody>
                        
                        <tr>
                           
                           <td><strong>Administration</strong></td>
                           
                           <td>&nbsp;</td>
                           
                           <td>&nbsp;</td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;Dean</td>
                           
                           <td><a href="mailto:erenn1@valenciacollege.edu">Dr. Elizabeth Renn</a></td>
                           
                           <td><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-528-1313</span>&nbsp;
                           </td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;Administrative Assistant to the Dean</td>
                           
                           <td><a href="mailto:mmoorezocco@valenciacollege.edu"> MaryBeth Moore Zocco</a></td>
                           
                           <td>&nbsp;<span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1616</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;Administrative Assistant to the Dean</td>
                           
                           <td><a href="mailto:ssorrough@valenciacollege.edu">Sharon Sorrough</a></td>
                           
                           <td>&nbsp;<span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1313</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;</td>
                           
                           <td>&nbsp;</td>
                           
                           <td>&nbsp;</td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;<em><strong>Discipline Coordinator</strong></em></td>
                           
                           <td>&nbsp;</td>
                           
                           <td>&nbsp;</td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;English</td>
                           
                           <td>&nbsp;<a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=dfreeman9&amp;CFID=7930163&amp;CFTOKEN=aa67ac46f2175752-F9B87BEC-ACAB-0F99-822608BFC686AA9C">David Freeman</a></td>
                           
                           <td>&nbsp;<span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1207</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;</td>
                           
                           <td><a href="http://ntdoor.valenciacollege.edu/faculty.cfm?uid=jzuromski1&amp;CFID=7930163&amp;CFTOKEN=aa67ac46f2175752-F9B87BEC-ACAB-0F99-822608BFC686AA9C">Jackie Zuromski</a></td>
                           
                           <td>&nbsp;<span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1074</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;English for Academic Purposes (EAP)</td>
                           
                           <td><a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=wwishbogue&amp;CFID=7930163&amp;CFTOKEN=aa67ac46f2175752-F9B87BEC-ACAB-0F99-822608BFC686AA9C">Wendy Wish</a></td>
                           
                           <td><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1338</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>Speech</td>
                           
                           <td><a href="http://frontdoor.valenciacollege.edu/faculty.cfm?uid=bperrell&amp;CFID=7930163&amp;CFTOKEN=aa67ac46f2175752-F9B87BEC-ACAB-0F99-822608BFC686AA9C">Beth Perrell</a></td>
                           
                           <td><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-5505</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>&nbsp;</td>
                           
                           <td>&nbsp;</td>
                           
                           <td><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">&nbsp;</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td><strong>Office Location</strong></td>
                           
                           <td colspan="2">Building 5 <span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">&nbsp;Room 231</span></td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>Mailing Address</td>
                           
                           <td colspan="2">
                              
                              <p>1800 South Kirkman Road</p>
                              
                              <p>Orlando, Florida 32811</p>
                              
                              <p>Mail Code: 4-11</p>
                              
                           </td>
                           
                        </tr>
                        
                        <tr>
                           
                           <td>Fax Number</td>
                           
                           <td colspan="2"><span class="baec5a81-e4d6-4674-97f3-e9220f0136c1" style="white-space: nowrap;">407-582-1676</span></td>
                           
                        </tr>
                        
                     </tbody>
                     
                  </table>
                  		
               </div>			
               			
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/departments/communications-west/index.pcf">©</a>
      </div>
   </body>
</html>