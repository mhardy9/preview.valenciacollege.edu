<?pcf-stylesheet path="/_resources/xsl/home.xsl" title="Home Page" extension="php"?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="keywords" content=", college, school, educational" />
		<meta name="description" content="Academics | Valencia College" />
		<meta name="author" content="Valencia College" />
		<title>Academics | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_staging/headcode.inc"); ?>
		<script>
			var page_url="https://preview.valenciacollege.edu/academics/static_index.php";
		</script>
		<script>
			FontAwesomeConfig = { searchPseudoElements: true };
		</script>
		<link rel="stylesheet" type="text/css" href="_resources/css/bootstrap/bootstrap.min.css" />
		<style>
			.check ul {
				list-style: none;
			}

			.check ul li:before {
				content: '✓\00a0\00a0';
			}
			.btn-val-gold {
				font-weight: bold;
				color: #000000;
				text-transform: uppercase;
				background-color: #FDB913 ;
				border-color: #FDB913 ;	
			}
			ul,li {
				margin: 0;
				padding: 0;
			}

			ul {
				counter-reset: foo;
				display: table;
			}

			li {
				list-style: none;
				counter-increment: foo;
				display: table-row;
			}

			li::before {
				content: counter(foo) ".";
				display: table-cell;
				text-align: right;
				padding-right: .3em;
			}
			body {
				font-size: 14px;
			}
			.center {text-align:center;}
			.card-footer-light {
				padding: 1rem 0 0 1.25rem;
				background-color: transparent;
				border-top: none;
			}
			.card-light {
				border: none;
			}
			@media screen and (min-width: 1024px){
				.card-deck {
					-webkit-box-orient: horizontal;
					-webkit-box-direction: normal;
					-ms-flex-flow: row wrap;
					flex-flow: row wrap;
					margin-right: -30px!important;
					margin-left: -30px!important;
				}
			}
			.btn-lower-r {
				position: absolute;
right:    2rem;
bottom:   1rem;
			}
			.btn-sm {
    padding: .3rem 1.25rem;
			}
		</style>
	</head>
	<body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
		<div id="preloader">
			<div class="pulse"></div>
		</div>
		<!-- Pulse Preloader --> <!-- Return to Top --><a id="return-to-top" href="/javascript:" aria-label="Return to top"> </a> <!-- <header class="valencia-alert"><iframe></header> --> 
		<!-- ChromeNav Header ================================================== -->
		<style>
			@media screen and (min-width: 960px){
				.menu-site{margin-top: 80px; position: absolute; z-index: 1;}
				.menu-site a, .menu-college a {color: white!important;}
				.menu-college {background-color:#BF311A;}
			}
		</style>
		<nav class="navbar navbar-expand-lg menu-college">
			<div class="container"> 
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<div class="menu-college">          
						<ul class="navbar-nav mr-auto">
							<a class="navbar-brand" href="#">LOGO HERE</a>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Students
								</a>
								<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
									<li class="nav-item"><a class="dropdown-item" href="#">Continuing Education</a></li>
									<li class="nav-item"><a class="dropdown-item" href="#">Current Students</a></li>
									<li class="nav-item"><a class="dropdown-item" href="#">Future Students</a></li>
									<a class="dropdown-item" href="#">Internation Students</a>
									<li class="nav-item"><a class="dropdown-item" href="#">Military &amp; Veterans</a></li>

								</ul>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Faculty</a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="#">Employees</a>
							</li>
							<li class="nav-item">
								<a class="nav-link disabled" href="#">Alumni &amp; Foundation</a>
							</li>          
							<li><form class="form-inline my-2 my-lg-0">
								<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
								<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
								</form></li>
						</ul>

					</div>
					<style>
						@media screen and (min-width: 960px){
							.menu-site{margin-top: 80px; position: absolute; z-index: 1;}
							.menu-site a {color: white!important;}
						}
					</style>
					<div class="menu-site">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link active" href="#">Academics</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Admissions</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Programs &amp; Degrees</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">About</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>

		<!-- SiteMenu Header ================================================== -->

		<div class="sub-header bg bg-academics">
			<div id="intro-txt">
				<h1>
					Academics
				</h1>
			</div>
		</div>
		<div class="container-fluid bg-grey">

			<div class="container margin-30">
				<section>
					<div class="container py-3">
						<div class="card card-light">
							<div class="row ">
								<div class="col-md-3">
									<img src="/_resources/img/prestige/art-academics-eng-tech.jpg" class="w-100">
								</div>
								<div class="col-md-5 px-3">
									<div class="card-block px-3 mt-4">
										<h4 class="card-title">Engineering and Technology</h4>
										<p class="card-text">From building houses to building computer networks, there are plenty of career options. </p>
									</div>
								</div>
								<div class="col-md-4 px-3">
									<div class="card-block px-3 mt-4 check">
										<ul class="card-text">
											<li>3 Associate in Arts Transfer Plan </li>
											<li>8 Associate in Science</li>
											<li>1 Bachelor of Science</li>
											<li>18 Certificates</li>
											<li>6 Associate in Arts Articulated<br />Pre-Major</li> </ul>

										<div class="card-footer card-footer-light btn-lower-r">
											<a href="#" class="btn btn-sm btn-val-gold">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container py-3">
						<div class="card card-light">
							<div class="row ">
								<div class="col-md-3">
									<img src="/_resources/img/prestige/art-academics-hosp-cul.jpg" class="w-100">
								</div>
								<div class="col-md-5 px-3">
									<div class="card-block px-3 mt-4">
										<h4 class="card-title">Hospitality and Culinary</h4>
										<p class="card-text">For fledging chefs, hoteliers, restaurant managers and tourism professionals. </p>
									</div>
								</div>
								<div class="col-md-4 px-3">
									<div class="card-block px-3 mt-4 check">
										<ul class="card-text">
											<li>4 Associate in Science</li>
											<li>8 Certificates</li> 
										</ul>
									</div>

<div class="card-footer card-footer-light btn-lower-r">
										<a href="#" class="btn btn-sm btn-val-gold">Learn More</a>
									</div>
								</div>									
							</div>
						</div>
					</div>
				</section>
				<section>
					<div class="container py-3">
						<div class="card">
							<div class="row ">
								<div class="col-md-3">
									<img src="/_resources/img/prestige/art-academics-pub-safe.jpg" class="w-100">
								</div>
								<div class="col-md-5 px-3">
									<div class="card-block px-3 mt-4 check">
										<h4 class="card-title">Public Safety and Paralegal Studies</h4>
										<p class="card-text">Valencia College offers degrees in Paralegal Studies, Criminal Justice, and Fire Science. </p>
									</div>
								</div>
								<div class="col-md-4 px-3">
									<div class="card-block px-3 mt-4">
										<ul class="card-text check">
											<li>3 Associate in Science</li>
											<li>15 Certificates</li>
											<li>23 Certifications</li>
										</ul>

										<a href="#" class="btn btn-val-gold btn-sm btn-lower-r">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section>
					<div class="col-md-12">
						<div class="card flex-md-row mb-4 box-shadow h-md-220">
							<img class="card-img-left flex-auto d-none d-md-block" src="/_resources/img/prestige/art-academics-generic.jpg"><div class="card-body d-flex flex-column align-items-start">
							<strong class="d-inline-block mb-2 text-success">Program</strong>
							<h3 class="mb-0">
								<a class="text-dark" href="#">Text title</a>
							</h3>
							<div class="mb-1 text-muted">Nov 11</div>
							<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
							</div>

							<div class="card-body d-flex flex-column align-items-start check">
								<ul class="card-text mb-auto">
									<li>3 Associate in Science</li>
									<li>15 Certificates</li>
									<li>23 Certifications</li>
								</ul>
								<a href="#" class="btn btn-sm btn-val-gold btn-lower-r">LEARN MORE</a>
							</div></div>
					</div></section>
			</div>
		</div>




		<?php/* include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/homepage-featured-stories.inc"); */?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_staging/footcode.inc"); ?> <?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
		<div id="hidden" style="display: none;"><a id="de" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/index.pcf" rel="nofollow">©</a></div><a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/static-index.php">©</a></body>
</html>
