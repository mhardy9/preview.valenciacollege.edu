<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Core Competencies and General Education Outcomes  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/competencies/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/competencies/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Student Core Competencies and General Education Outcomes</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/competencies/">Competencies</a></li>
               <li>Student Core Competencies and General Education Outcomes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a> <a href="index.html">
                           
                           </a>
                        
                        
                        <h2>Student Core Competencies</h2>
                        
                        <p>The faculty of Valencia 
                           College have established four Core Competencies that describe 
                           the learning outcomes for a Valencia graduate. They are: 
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="default.html#Think:">Think</a></li>
                           
                           <li><a href="default.html#Value:">Value</a></li>
                           
                           <li><a href="default.html#Act:">Act </a></li>
                           
                           <li><a href="default.html#Communicate:">Communicate</a></li>
                           
                        </ul>
                        
                        
                        <p>These general competencies can be applied 
                           in many contexts and must be developed over a lifetime. They specify 
                           how learning can be expressed and assessed in practice. They enable 
                           students and faculty to set learning goals and assess learning within 
                           and across the many disciplines of human inquiry. Use the descriptions 
                           and examples of academic work for each to measure your own learning 
                           outcomes. Samples of the academic work are great additions to your 
                           Learning Portfolio.&nbsp;
                        </p>
                        
                        
                        
                        
                        <h2>General Education Outcomes</h2>
                        
                        <p>In December 2007, the faculty at Valencia College developed General Education outcomes.
                           The <a href="../aadegrees/courserequirement.html">General Education Program</a> at Valencia is an integral part of the A.A. Degree Program and is designed to contribute
                           to the student's educational growth by providing a basic liberal arts education. A
                           student who completes the General Education Program should have achieved the following
                           outcomes:
                        </p>
                        
                        
                        <div>
                           
                           <p><strong>Cultural and Historical Understanding</strong><br>Demonstrate understanding of the diverse traditions of the world, and an individual's
                              place in it.
                           </p>
                           
                           <p><strong>Quantitative and Scientific Reasoning</strong><br>Use processes, procedures, data, or evidence to solve problems and make effective
                              decisions.
                           </p>
                           
                           <p><strong>Communication Skills</strong><br>Engage in effective interpersonal, oral, and written communication.
                           </p>
                           
                           <p><strong>Ethical Responsibility</strong><br>Demonstrate awareness of personal responsibility in one's civic, social, and academic
                              life.
                           </p>
                           
                           <p><strong>Information Literacy</strong><br>Locate, evaluate, and effectively use information from diverse sources.
                           </p>
                           
                           <p><strong>Critical  Thinking</strong><br>Effectively analyze, evaluate, synthesize, and apply information and ideas from diverse
                              sources and disciplines. 
                           </p>
                           
                        </div>
                        
                        
                        
                        <h2>Core Competencies Descriptions</h2>
                        
                        
                        <p><a name="Think:" id="Think:"></a></p>
                        
                        <h3>Think</h3>
                        
                        <h3>think clearly, critically, 
                           and creatively, analyze, synthesize, integrate and evaluate in many 
                           domains of human inquiry
                        </h3>
                        
                        
                        <p>To think, what must you do?</p>
                        
                        <ol type="a">
                           
                           <li> 
                              
                              <p>analyze data, ideas, 
                                 patterns, principles, perspectives
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>employ the facts, 
                                 formulas, procedures of the discipline
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>integrate ideas 
                                 and values from different disciplines
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>draw well-supported 
                                 conclusions
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>revise conclusions 
                                 consistent with new observations, interpretations, or reasons
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p>How and where must 
                           you think?
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>with curiosity 
                                 and consistency
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>individually and 
                                 in groups
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Samples of my work 
                           which demonstrate that I can:
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>identify data, 
                                 ideas, patterns, principles, perspectives
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>use facts, formulas, 
                                 procedures
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>draw well-supported 
                                 conclusions
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>integrate ideas 
                                 and values from different disciplines
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>revise my conclusions 
                                 in light of new observations, interpretations, or reasons
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><a name="Value:" id="Value:"></a></p>
                        
                        <h3>Value</h3>
                        
                        <h3> make reasoned judgments and responsible commitments</h3>
                        
                        
                        <p>To value, what must you do?</p> 
                        
                        <ol type="a">
                           
                           <li> 
                              
                              <p>recognize values 
                                 as expressed in attitudes, choices, and commitments
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>distinguish among 
                                 personal, ethical, aesthetic, cultural, and scientific values
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>employ values and 
                                 standards of judgment from different disciplines
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>evaluate your own 
                                 and others’ values from individual, cultural, and global perspectives 
                                 articulate a considered and self-determined set of values
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p>How and where must 
                           you value? 
                           
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>&nbsp;with empathy 
                                 and fair-mindedness
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>&nbsp;individually 
                                 and in groups
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Samples of my work 
                           which demonstrate that I can:
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>identify values 
                                 expressed in feelings, attitudes, beliefs, choices, and commitments
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>recognize my own 
                                 and others’ values
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>distinguish among 
                                 personal, ethical, aesthetic, cultural, and scientific values
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>employ values and 
                                 standards of judgment from different disciplines
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>evaluate my own 
                                 and others’ values from global or universal perspectives
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>commit to actions 
                                 consistent with a considered and self-determined set of values<br>
                                 
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        <p><a name="Act:" id="Act:"></a></p>
                        
                        <h3>Act</h3>
                        
                        <h3> act purposefully, effectively, 
                           and responsibly
                        </h3>
                        
                        
                        <p>To act, what must you do?</p>
                        
                        <ol type="a">
                           
                           <li> 
                              
                              <p>apply disciplinary 
                                 knowledge, skills, and values to educational and career goals
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>implement effective 
                                 problem-solving, decision-making, and goal-setting strategies
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>act effectively 
                                 and appropriately in various personal and professional settings
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>assess the effectiveness 
                                 of personal behavior and choices
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>respond appropriately 
                                 to changing circumstances
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p>How and where must 
                           you act?
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>with courage and 
                                 perseverance
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>individually and 
                                 in groups
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>in your personal, 
                                 professional, and community life
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Samples of my work 
                           which demonstrate that I can:
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>act effectively 
                                 and appropriately in different contexts and settings
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>implement problem-solving 
                                 and decision-making strategies
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>manage my time 
                                 and activities in daily life
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>apply disciplinary 
                                 knowledge, skills, values to my goals
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>plan for and implement 
                                 desirable change in response to circumstances
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><a name="Communicate:" id="Communicate:"></a></p>
                        
                        <h3>Communicate</h3>
                        
                        <h3> communicate with different 
                           audiences using varied means
                        </h3>
                        
                        
                        <p>To communicate, what must you do? 
                           
                        </p>
                        
                        <ol type="a">
                           
                           <li> 
                              
                              <p>identify your own 
                                 strengths and need for improvement as communicator
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>employ methods 
                                 of communication appropriate to your audience and purpose
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>evaluate the effectiveness 
                                 of your own and others’ communication
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p>How and where must 
                           you communicate? 
                           
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>by speaking, listening, 
                                 reading and writing
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>verbally, non-verbally, 
                                 and visually
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>with honesty and 
                                 civility
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>in different disciplines 
                                 and settings
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Samples of my work 
                           which demonstrate that I can: 
                           
                        </p>
                        
                        <ul>
                           
                           <li> 
                              
                              <p>identify my own 
                                 strengths and weaknesses as a communicator
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>analyze audience 
                                 to improve communication in various settings
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>communicate in 
                                 different contexts, settings, and disciplines
                              </p>
                              
                           </li>
                           
                           <li> 
                              
                              <p>evaluate effectiveness 
                                 of my own and others communication
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/competencies/default.pcf">©</a>
      </div>
   </body>
</html>