<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Degree Options  | Valencia College</title>
      <meta name="Description" content="Valencia College degree options &amp;ndash; transfer to a university or enter directly into a career.">
      <meta name="Keywords" content="programs, degrees, certificates, aa, associate in arts, as, associate in science, A.S., A.A., university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Degree Options</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <h2>Degree Options</h2>
                  
                  <p>Whether you’re planning to transfer to a university or enter directly into a career,
                     Valencia has a degree to get you there.
                  </p>
                  
                  <div class="col-md-9">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list"><a href="associate-in-arts/index.html"> <img src="/_resources/img/academics/programs/degree-options/aa-degree.png" alt="Associate in Arts"></a></div>
                              
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block"></div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Associate in Arts</h3>
                                 <br>
                                 
                                 <p>The Associate in Arts degree (A.A. degree) is a general studies degree. An A.A. Degree
                                    will prepare you to transfer to a Florida public university as a junior. As an A.A.
                                    student, there are ways to focus in on your intended major. You can follow a transfer
                                    plan or choose a pre-major which will help you meet most upper division pre-reqs.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div><a href="associate-in-arts/index.html" class="button-outline">View Degrees</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list"><a href="associate-in-science/index.html"> <img src="/_resources/img/academics/programs/degree-options/as-degree.png" alt="Associate in Science"></a></div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block"></div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Associate in Science</h3>
                                 
                                 <p>The Associate in Science degree (A.S. degree) prepares you to enter a specialized
                                    career field. It also transfers to the B.A.S. (Bachelor of Applied Science) program
                                    offered at some universities. If the program is “articulated,” it will also transfer
                                    to a B.A. or B.S. degree program at a designated university.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div><a href="associate-in-science/index.html" class="button-outline">View Degrees</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list"><a href="bachelor-of-science/index.html"> <img src="/_resources/img/academics/programs/degree-options/bs-degree.png" alt="Bachelor of Science"></a></div> 
                           </div>
                           
                           <div class="clearfix visible-xs-block"></div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Bachelor of Science</h3>
                                 
                                 <p>The B.S. degree prepares you to enter an advanced position within a specialized career
                                    field. At Valencia, students must first complete an associate degree before transferring
                                    into a baccalaureate program. Bachelor’s degrees require about four years of study
                                    (including time spent earning the associate degree).
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div><a href="bachelor-of-science/index.html" class="button-outline">View Degrees</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list"><a href="certificate-programs/index.html"> <img src="/_resources/img/academics/programs/degree-options/cert-programs.png" alt="Certificate Programs"></a></div> 
                           </div>
                           
                           <div class="clearfix visible-xs-block"></div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Certificate Programs</h3>
                                 
                                 <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                                    for job advancement. Credits earned can be applied toward a related A.S. degree program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div><a href="certificate-programs/index.html" class="button-outline">View Certificates</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div>
                        
                        <ul class="action-palette">
                           
                           <li><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=" class="button-action landing">Apply Now <i class="far fa-long-arrow-right"></i></a></li>
                           
                           <li><a href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm" class="button-action_outline landing">Request Information <i class="far fa-long-arrow-right"></i></a></li>
                           
                           <li class="row">
                              <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" class="button-action_outline landing split col-md-5">Visit Valencia <i class="far fa-long-arrow-right"></i></a> <span class="col-md-1 split">&nbsp;</span> 
                              
                              
                              <form id="chatForm">
                                 <input type="hidden" id="firstName" value="Valencia">
                                 <input type="hidden" id="lastName" value="Student">
                                 <button type="button" class="button-action_outline landing split col-md-5" onclick="startPopupChat()">Chat With Us <i class="far fa-long-arrow-right"></i></button>
                                 
                              </form>
                              
                              <div id="chatContainer"></div>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/index.pcf">©</a>
      </div>
   </body>
</html>