<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Requirements  | Valencia College</title>
      <meta name="Description" content="Associate in Arts Degree Course and Graduation Requirements at Valencia College.">
      <meta name="Keywords" content="requirements, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/course-requirements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li>Course Requirements </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Course Requirements</h2>
                        
                        <p>Associate in Arts Degree Course Requirements at Valencia College</p>
                        
                        
                        <p>The Associate in Arts Degree requires a minimum of 60 college-level credit hours including
                           36 hours in general education, 24 hours of acceptable electives, and satisfaction
                           of the foreign language proficiency
                           requirement.
                        </p> 
                        
                        <hr class="styled_2">
                        
                        
                        <h3>General Education Requirements</h3>
                        
                        <p>Requirements for the Associate in Arts Degree - 36 Credits</p>
                        
                        
                        <p> The general education program at Valencia is designed to contribute
                           to the student's educational growth by providing a basic liberal
                           arts education and it is an integral part of the A.A. Degree program.
                        </p>
                        
                        <p>  There are two approaches to general education at Valencia. The
                           first is 36 semester hours of academic credit which serve as the
                           core of the curriculum. The 36 hours are selected from 5 core areas
                           of academic courses offered at Valencia: Communications, Humanities,
                           Mathematics, Science and Social Sciences. The second approach is
                           the 24-semester-hour Interdisciplinary
                           Studies Honors Program and completion of SPC 1600, POS 2041,
                           and the six-semester-hour mathematics requirement outlined in Area
                           3. The description of the first approach follows, and the second
                           approach is described in the Honors section of this catalog. When
                           a number of courses may be used to satisfy a degree requirement,
                           a course attribute code identifies each course that may be used
                           to satisfy that specific requirement.
                        </p>
                        
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>AREA 1. COMMUNICATIONS9 Credits</h3>
                        
                        <p></p>
                        
                        
                        <p>Required Courses - There are a required 12 credits. The Freshman Composition courses
                           must be completed with a minimum grade of C to fulfill the Gordon Rule 
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <thead>
                                 
                                 <tr>
                                    
                                    <th> Course Number </th>
                                    
                                    
                                    <th> Course Description</th>
                                    
                                    
                                    <th>Credits </th>
                                    
                                 </tr>
                                 
                              </thead>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th> Freshman Composition I</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th></th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> <a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101">ENC 1101</a> 
                                    </td>
                                    
                                    <td> FRESHMAN COMPOSITION I +*~</td>
                                    
                                    <td> 3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101H">ENC 1101H</a>
                                       
                                    </td>
                                    
                                    <td> FRESHMAN COMPOSITION I - HONORS</td>
                                    
                                    
                                    <td> 3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th> Freshman Composition II</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th></th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ENC%201102">ENC 1102</a></td>
                                    
                                    <td>FRESHMAN COMPOSITION II +*~</td>
                                    
                                    <td>3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=ENC%201102H">ENC 1102H</a>
                                       
                                    </td>
                                    
                                    <td> FRESHMAN COMPOSITION II - HONORS</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th> Select one of the following Oral Communication courses:</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SPC%201608">SPC 1608</a></td>
                                    
                                    <td>FUNDAMENTALS OF SPEECH ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SPC%201608H">SPC 1608H</a></td>
                                    
                                    <td>FUNDAMENTALS OF SPEECH - HONORS ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SPC%201017">SPC 1017</a></td>
                                    
                                    <td>INTERPERSONAL COMMUNICATION ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SPC%201017H">SPC 1017H</a></td>
                                    
                                    <td>INTERPERSONAL COMMUNICATION HONORS ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 
                                 <th>New Student Experience</th>
                                 
                                 
                                 <th> </th>
                                 
                                 
                                 <th>3</th>
                                 
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SLS%201122">SLS 1122</a></td>
                                    
                                    <td>NEW STUDENT EXPERIENCE ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=SLS%201122H">SLS 1122H</a>
                                       
                                    </td>
                                    
                                    <td> New Student Experience-Honors</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Total Credit Hours</strong></td>
                                    
                                    <td></td>
                                    
                                    <td><strong>12</strong></td>
                                    
                                 </tr>
                                 
                                 
                                 
                              </tbody>
                              
                           </table>
                           
                           	  
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <h3>AREA 2. HUMANITIES6 Credits</h3>
                        
                        <p></p>
                        
                        
                        <p>Required Courses - There are 6 required credits. Students must take three credits
                           hours from the Core offerings and three credit hours from Institutional offerings.
                           One course must be a Gordon Rule <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a> course, and completed with a minimum grade of C to fulfill the Gordon Rule Requirement
                           
                        </p>
                        
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <thead>
                                 
                                 <tr>
                                    
                                    <th> Course Number </th>
                                    
                                    
                                    <th> Course Description</th>
                                    
                                    
                                    <th>Credits </th>
                                    
                                 </tr>
                                 
                              </thead>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Core</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th></th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ARH%201000">ARH 1000</a></td>
                                    
                                    <td>Art Appreciation ~</td>
                                    
                                    <td> </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%201020">HUM 1020</a></td>
                                    
                                    <td>INTRODUCTION TO HUMANITIES ~</td>
                                    
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%201020H">HUM 1020H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTION TO HUMANITIES - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=LIT%201000">LIT 1000</a></td>
                                    
                                    <td>INTRODUCTION TO LITERATURE ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHI%202010">PHI 2010</a></td>
                                    
                                    <td>PHILOSOPHY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=THE%201000">THE 1000</a></td>
                                    
                                    <td>Introduction To Theater ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <th>Institutional</th>
                                    
                                    
                                    <th> </th> 
                                    
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202220">HUM 2220</a></td>
                                    
                                    <td>HUMANITIES - GREEK AND ROMAN +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202220H">HUM 2220H</a>
                                       
                                    </td>
                                    
                                    <td> HUMANITIES- GREEK AND ROMAN- HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202223">HUM 2223</a></td>
                                    
                                    <td>HUMANITIES - LATE ROMAN AND MEDIEVAL +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202223H">HUM 2223H</a>
                                       
                                    </td>
                                    
                                    <td> HUMANITIES - LATE ROMAN AND MEDIEVAL HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202232">HUM 2232</a></td>
                                    
                                    <td>HUMANITIES - RENAISSANCE AND BAROQUE +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202232H">HUM 2232H</a>
                                       
                                    </td>
                                    
                                    <td> HUMANITIES - RENAISSANCE AND BAROQUE - HONORS</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202234">HUM 2234</a></td>
                                    
                                    <td>HUMANITIES - ENLIGHTENMENT AND ROMANTICISM +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202234H">HUM 2234H</a>
                                       
                                    </td>
                                    
                                    <td> HUMANITIES - ENLIGHTENMENT AND ROMANTICISM - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202250">HUM 2250</a></td>
                                    
                                    <td>HUMANITIES - TWENTIETH CENTURY +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202250H">HUM 2250H</a>
                                       
                                    </td>
                                    
                                    <td> HUMANITIES - TWENTIETH CENTURY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202310">HUM 2310</a></td>
                                    
                                    <td>MYTHOLOGY +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202310H">HUM 2310H</a>
                                       
                                    </td>
                                    
                                    <td> MYTHOLOGY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202410">HUM 2410</a></td>
                                    
                                    <td>ASIAN HUMANITIES +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=HUM%202461">HUM 2461</a></td>
                                    
                                    <td>LATIN AMERICAN HUMANITIES +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=HUM%202461H">HUM 2461H</a>
                                       
                                    </td>
                                    
                                    <td> LATIN AMERICAN HUMANITIES - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=REL%202300">REL 2300</a></td>
                                    
                                    <td>UNDERSTANDING RELIGIOUS TRADITIONS +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ARC%201701">ARC 1701</a></td>
                                    
                                    <td>HISTORY OF ARCHITECTURE I +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ARH%202051">ARH 2051</a></td>
                                    
                                    <td>INTRODUCTION TO ART HISTORY II +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=ARH%202051H">ARH 2051H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTION TO ART HISTORY II - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MUT%201111">MUT 1111</a></td>
                                    
                                    <td>MUSIC THEORY I ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHI%202600">PHI 2600</a></td>
                                    
                                    <td>ETHICS AND CRITICAL THINKING +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Total Credit Hours</strong></td>
                                    
                                    <td></td>
                                    
                                    <td><strong>6</strong></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           	  
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>AREA 3. MATHEMATICS6 Credits</h3>
                        
                        
                        <p>Required Courses - There are 6 required credits. Students must take three credit hours
                           from the Core offerings, and three credit hours from either the Core or Institutional
                           offerings. All courses must be completed with a minimum grade of C to meet the Gordon
                           Rule Requirement. Any student who successfully completes a mathematics course for
                           which one of the general education core course options in mathematics is an immediate
                           prerequisite shall be considered to have completed the mathematics core.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <thead>
                                 
                                 <tr>
                                    
                                    <th> Course Number </th>
                                    
                                    
                                    <th> Course Description</th>
                                    
                                    
                                    <th>Credits </th>
                                    
                                 </tr>
                                 
                              </thead>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Core ^must take a minimum of three credit hours</th>
                                    
                                    <th></th>
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC 1105</a></td>
                                    
                                    <td>COLLEGE ALGEBRA +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105H">MAC 1105H</a>
                                       
                                    </td>
                                    
                                    <td> COLLEGE ALGEBRA - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MGF%201106">MGF 1106</a></td>
                                    
                                    <td>COLLEGE MATHEMATICS +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MGF%201107">MGF 1107</a></td>
                                    
                                    <td>MATH FOR THE LIBERAL ARTS +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202311">MAC 2311</a></td>
                                    
                                    <td>CALCULUS WITH ANALYTIC GEOMETRY I +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=MAC%202311H">MAC 2311H</a>
                                       
                                    </td>
                                    
                                    <td> CALCULUS WITH ANALYTIC GEOMETRY I - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=STA%202023">STA 2023</a></td>
                                    
                                    <td>STATISTICAL METHODS +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=STA%202023H">STA 2023H</a>
                                       
                                    </td>
                                    
                                    <td> STATISTICAL METHODS - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Institutional ^students may take three or more credit hours</th>
                                    
                                    <th></th>
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201114">MAC 1114</a></td>
                                    
                                    <td>COLLEGE TRIGONOMETRY +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%201140">MAC 1140</a></td>
                                    
                                    <td>PRECALCULUS ALGEGRA +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202312">MAC 2312</a></td>
                                    
                                    <td>CALCULUS WITH ANALYTIC GEOMETRY II +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=MAC%202312H">MAC 2312H</a>
                                       
                                    </td>
                                    
                                    <td> CALCULUS WITH ANALYTIC GEOMETRY II - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MAC%202233">MAC 2233</a></td>
                                    
                                    <td>CALCULUS FOR BUSINESS AND SOCIAL SCIENCE +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=MAC%202233H">MAC 2233H</a>
                                       
                                    </td>
                                    
                                    <td> CALCULUS FOR BUSINESS AND SOCIAL SCIENCE</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Total Credit Hours</strong></td>
                                    
                                    <td></td>
                                    
                                    <td><strong>6</strong></td>
                                    
                                 </tr>
                                 
                                 
                                 
                              </tbody>
                              
                           </table>
                           	  
                        </div>
                        
                        
                        <h3>AREA 4. SCIENCE 6 Credits</h3>
                        
                        
                        <p>Required Courses -There are 6 required credits. Students must take at least three
                           credit hours from the Core offerings and three credit hours from either the Core or
                           Institutional offerings. Any student who successfully completes a natural science
                           course for which one of the general education core course options in natural science
                           is an immediate prerequisite shall be considered to have completed the natural science
                           core.
                        </p>
                        
                        <p>Valencia does not require a science course with a laboratory. Courses with a C designation
                           have a combined class and laboratory; however, university majors determine if the
                           student must take one or more laboratory science courses at Valencia. To select the
                           most appropriate science courses for your major or transfer plan, complete an education
                           plan through Atlas.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <thead>
                                 
                                 <tr>
                                    
                                    <th> Course Number </th>
                                    
                                    
                                    <th> Course Description</th>
                                    
                                    
                                    <th>Credits </th>
                                    
                                 </tr>
                                 
                              </thead>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th> Core ^minimum of three credit hours required</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=AST%201002">AST 1002</a></td>
                                    
                                    <td>ASTRONOMY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=AST%201002H">AST 1002H</a>
                                       
                                    </td>
                                    
                                    <td> ASTRONOMY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201005">BSC 1005</a></td>
                                    
                                    <td>BIOLOGICAL SCIENCE ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201005L">BSC 1005L</a></td>
                                    
                                    <td>LAB IN APPLIED BIOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201005C">BSC 1005C</a></td>
                                    
                                    <td>BIOLOGICAL SCIENCE COMBINED ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=BSC%201005H">BSC 1005H</a>
                                       
                                    </td>
                                    
                                    <td> BIOLOGICAL SCIENCE - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201010C">BSC 1010C</a></td>
                                    
                                    <td>GENERAL BIOLOGY I *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=BSC%201010H">BSC 1010H</a>
                                       
                                    </td>
                                    
                                    <td> GENERAL BIOLOGY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=CHM%201020">CHM 1020</a></td>
                                    
                                    <td>CHEMISTRY IN EVERYDAY LIFE ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=CHM%201045C">CHM 1045C</a></td>
                                    
                                    <td>GENERAL CHEMISTRY WITH QUALITATIVE ANALYSIS *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=CHM%201045H">CHM 1045H</a>
                                       
                                    </td>
                                    
                                    <td> GENERAL CHEMISTRY WITH QUALITATIVE ANALYSIS I -HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ESC%201000">ESC 1000</a></td>
                                    
                                    <td>EARTH SCIENCES ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=EVR%201001">EVR 1001</a></td>
                                    
                                    <td>INTRODUCTION TO ENVIRONMENTAL SCIENCE ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=EVR%201001H">EVR 1001H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTION TO ENVIRONMENTAL SCIENCE HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHY%201020">PHY 1020</a></td>
                                    
                                    <td>CONCEPTUAL PHYSICS ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHY%202048C">PHY 2048C</a></td>
                                    
                                    <td>GENERAL PHYSICS WITH CALCULUS I *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=PHY%202048H">PHY 2048H</a>
                                       
                                    </td>
                                    
                                    <td> GENERAL PHYSICS WITH CALCULUS I - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHY%202053C">PHY 2053C</a></td>
                                    
                                    <td>COLLEGE PHYSICS I WITH ALGEBRA AND TRIGONOMETRY *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Institutional ^students may take three or more credit hours</th>
                                    
                                    <th></th>
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BOT%202010C">BOT 2010C</a></td>
                                    
                                    <td>BOTANY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201011C">BSC 1011C</a></td>
                                    
                                    <td>GENERAL BIOLOGY II *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=BSC%201011H">BSC 1011H</a>
                                       
                                    </td>
                                    
                                    <td> GENERAL BIOLOGY II - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201020">BSC 1020</a></td>
                                    
                                    <td>HUMAN BIOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201020C">BSC 1020C</a></td>
                                    
                                    <td>HUMAN BIOLOGY COMBINED ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%201026">BSC 1026</a></td>
                                    
                                    <td>BIOLOGY OF HUMAN SEXUALITY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=BSC%201026H">BSC 1026H</a>
                                       
                                    </td>
                                    
                                    <td> BIOLOGY OF HUMAN SEXUALITY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%202093C">BSC 2093C</a></td>
                                    
                                    <td>HUMAN ANATOMY AND PHYSIOLOGY I *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=BSC%202094C">BSC 2094C</a></td>
                                    
                                    <td>HUMAN ANATOMY AND PHYSIOLOGY II *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=CHM%201025C">CHM 1025C</a></td>
                                    
                                    <td>INTRODUCTION TO GENERAL CHEMISTRY *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=CHM%201046C">CHM 1046C</a></td>
                                    
                                    <td>GENERAL CHEMISTRY WITH QUALITATIVE ANALYSIS II *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=GLY%202010C">GLY 2010C</a></td>
                                    
                                    <td>PHYSICAL GEOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=GLY%202160">GLY 2160</a></td>
                                    
                                    <td>GEOLOGY OF NATIONAL PARKS ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MET%201010">MET 1010</a></td>
                                    
                                    <td>INTRODUCTION TO METEORLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=OCB%201000">OCB 1000</a></td>
                                    
                                    <td>INTRODUCTION TO MARINE BIOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=OCE%201001">OCE 1001</a></td>
                                    
                                    <td>INTRODUCTION TO OCEANOGRAPHY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=OCE%201001H">OCE 1001H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTION TO OCEANOGRAPHY-HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHY%202049C">PHY 2049C</a></td>
                                    
                                    <td>GENERAL PHYSICS WITH CALCULUS II *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PHY%202054C">PHY 2054C</a></td>
                                    
                                    <td>COLLEGE PHYSICS II WITH ALGEBRA AND TRIGONOMETRY *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=MCB%202010C">MCB 2010C</a></td>
                                    
                                    <td>MICROBIOLOGY *~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Total Credit Hours</th>
                                    
                                    <th></th>
                                    
                                    <th>6</th>
                                    
                                 </tr>
                                 
                                 
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <h3>AREA 5. SOCIAL SCIENCES6 Credits</h3>
                        
                        <p></p>
                        
                        
                        <p>Required Courses - There are 6 required credits. Students are required to take three
                           credit hours from the Core offerings and three credit hours from Institutional offerings.
                           The three hours of Institutional credit will apply toward the Gordon Rule <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a> requirement and must be completed with a minimum grade of C to fulfill the Gordon
                           Rule Requirement. 
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <thead>
                                 
                                 <tr>
                                    
                                    <th>Course Number </th>
                                    
                                    
                                    <th>Course Description</th>
                                    
                                    
                                    <th>Credits </th>
                                    
                                 </tr>
                                 
                              </thead>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Core</th>
                                    
                                    
                                    <th> </th>
                                    
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=POS%202041">POS 2041</a></td>
                                    
                                    <td>U.S. GOVERNMENT. ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=POS%202041H">POS 2041H</a>
                                       
                                    </td>
                                    
                                    <td> U.S. GOVERNMENT - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=AMH%202020">AMH 2020</a></td>
                                    
                                    <td>U.S. HISTORY 1877 TO PRESENT ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=AMH%202020H">AMH 2020H</a>
                                       
                                    </td>
                                    
                                    <td> UNITED STATES HISTORY 1877 TO PRESENT - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ANT%202000">ANT 2000</a></td>
                                    
                                    <td>INTRODUCTORY ANTHROPOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=ANT%202000H">ANT 2000H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTORY ANTHROPOLOGY HON</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=ECO%202013">ECO 2013</a></td>
                                    
                                    <td>PRINCIPLES OF ECONOMICS-MACRO</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=ECO%202013H">ECO 2013H</a>
                                       
                                    </td>
                                    
                                    <td> PRINCIPLES OF ECONOMICS-MACRO-HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=PSY%202012">PSY 2012</a></td>
                                    
                                    <td>GENERAL PSYCHOLOGY ~</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=PSY%202012H">PSY 2012H</a>
                                       
                                    </td>
                                    
                                    <td> GENERAL PSYCHOLOGY - HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=SYG%202000">SYG 2000</a></td>
                                    
                                    <td>INTRODUCTORY SOCIOLOGY.</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=SYG%202000H">SYG 2000H</a>
                                       
                                    </td>
                                    
                                    <td> INTRODUCTORY SOCIOLOGY- HONORS.</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Institutional</th>
                                    
                                    <th></th>
                                    
                                    <th>3</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=EUH%202000">EUH 2000</a></td>
                                    
                                    <td>ANCIENT AND MEDIEVAL WESTERN CIVILIZATION +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=EUH%202001">EUH 2001</a></td>
                                    
                                    <td>MODERN WESTERN CIVILIZATION +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=EUH%202001H">EUH 2001H</a>
                                       
                                    </td>
                                    
                                    <td> Modern Western Civilization-Honors</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=INR%202002">INR 2002</a></td>
                                    
                                    <td>INTERNATIONAL POLITICS +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>or <a href="http://catalog.valenciacollege.edu/search/?P=INR%202002H">INR 2002H</a>
                                       
                                    </td>
                                    
                                    <td> INTERNATIONAL POLITICS HONORS</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><a href="http://catalog.valenciacollege.edu/search/?P=POS%202112">POS 2112</a></td>
                                    
                                    <td>STATE AND LOCAL GOVERNMENT +*~ <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a>
                                       
                                    </td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Total Credit Hours</th>
                                    
                                    <th></th>
                                    
                                    <th>6</th>
                                    
                                 </tr>
                                 
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <p>This course must be completed with a grade of C or better.<br>
                           This course has a prerequisite; check description in Valencia catalog<br>
                           This is a General Education course.<br>
                           Denotes a Gordon Rule course
                        </p>
                        
                        <p>Electives/Required Prerequisites for Major (24 Credits)</p>
                        
                        <p>Most college-level credit courses taught at Valencia will count
                           toward elective credit. Any credit hours in excess of the minimum
                           required in any of the five core areas listed above will count as
                           elective credits. University majors require certain prerequisite
                           courses before transferring; these are the courses that should be
                           taken as electives at Valencia. To select the most appropriate electives,
                           create an educational plan through Atlas for review with a student
                           services staff member.
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Foreign Language Proficiency Requirement</h3>
                        
                        <p></p>
                        
                        
                        <p> </p>
                        
                        <p>Valencia's college-level foreign language proficiency requirement can be satisfied
                           in one of the following ways:
                           
                        </p>
                        
                        <ul class="list-unstyled">
                           
                           <li>You may complete two credits (two years) of high school instruction in one language
                              other than English with a passing grade each year as documented on your official high
                              school transcript.
                           </li>
                           
                           <li>You may successfully complete one of the following sequences in a single foreign language
                              with a letter grade of “C” or better:
                              
                              
                              <p></p>
                              
                              <table class="table table table-striped cart-list add_bottom_30">
                                 
                                 <thead>
                                    
                                    <tr>
                                       
                                       <th> Course Number </th>
                                       
                                       
                                       <th> Course Description</th>
                                       
                                       
                                       <th>Credits </th>
                                       
                                    </tr>
                                    
                                 </thead>
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=ARA%201120">ARA 1120</a><br>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=ARA%201121">ARA 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY ARABIC I<br>
                                          and ELEMENTARY ARABIC II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=ASL%202140">ASL 2140</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=ASL%202150">ASL 2150</a>
                                          
                                       </td>
                                       
                                       <td>AMERICAN SIGN LANGUAGE I<br>
                                          and AMERICAN SIGN LANGUAGE II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=FRE%201120">FRE 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=FRE%201121">FRE 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY FRENCH I<br>
                                          and ELEMENTARY FRENCH II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=GER%201120">GER 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=GER%201121">GER 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY GERMAN I<br>
                                          and ELEMENTARY GERMAN II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=ITA%201120">ITA 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=ITA%201121">ITA 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY ITALIAN I<br>
                                          and ELEMENTARY ITALIAN II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=LAT%201120">LAT 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=LAT%201121">LAT 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY LATIN I<br>
                                          and ELEMENTARY LATIN II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=POR%201120">POR 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=POR%201121">POR 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY PORTUGESE I<br>
                                          and ELEMENTARY PORTUGESE II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=POR%201340">POR 1340</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=POR%201341">POR 1341</a>
                                          
                                       </td>
                                       
                                       <td>Portuguese for Heritage Speaker I<br>
                                          and Portuguese for Heritage Speakers II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=SPN%201120">SPN 1120</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=SPN%201121">SPN 1121</a>
                                          
                                       </td>
                                       
                                       <td>ELEMENTARY SPANISH I<br>
                                          and ELEMENTARY SPANISH II
                                       </td>
                                       
                                       <td>8</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <a href="http://catalog.valenciacollege.edu/search/?P=SPN%201340">SPN 1340</a><br>
                                          &amp; <a href="http://catalog.valenciacollege.edu/search/?P=SPN%201341">SPN 1341</a>
                                          
                                       </td>
                                       
                                       <td>SPANISH FOR HERITAGE SPEAKER<br>
                                          and SPANISH FOR HERITAGE SPEAKER II
                                       </td>
                                       
                                       <td></td>
                                       
                                    </tr>
                                    
                                    
                                    
                                 </tbody>
                                 
                              </table>
                              
                              
                              <p>
                                 
                              </p>
                              <p>
                                 
                                 <ul>
                                    
                                    <li>You may satisfy this requirement by successfully completing foreign language course
                                       work at the Elementary II college level with a minimum grade of "C."
                                    </li>
                                    
                                 </ul>
                                 
                                 <ul>
                                    
                                    <li>Demonstration of proficiency by passing a CLEP (College Level Examination Program)
                                       foreign language test or a foreign language proficiency test administered by the University
                                       of Central Florida. Students who earn college-level foreign language credits through
                                       course work or CLEP may apply these credits toward the 24 Elective credits.<br>
                                       
                                    </li>
                                    
                                    <li>If your native language is a language other than English, you may satisfy the foreign
                                       language proficiency requirement by successful completion of English for Academic
                                       Purposes (EAP) course requirements and successful completion of the Area I Communications
                                       requirements for the Associate in Arts degree.<br>
                                       
                                    </li>
                                    
                                    <li>Demonstrated college-level proficiency in American Sign Language (either through completion
                                       of eight semester credits or successful completion of <a href="http://catalog.valenciacollege.edu/search/?P=ASL%202150">ASL 2150</a>) can be used to fulfill this requirement at Valencia.<br>
                                       
                                    </li>
                                    
                                    <li>Demonstration of proficiency by passing Advanced Placement test with a score of 4
                                       or higher.<br>
                                       
                                    </li>
                                    
                                    <li>American Sign Language cannot be used to fulfill the foreign language graduation requirement
                                       at many universities. Students are responsible for verifying acceptability at the
                                       institution to which they plan to transfer.
                                    </li>
                                    
                                 </ul>
                                 								
                              </p>
                              
                              <p><strong>Note:</strong> Satisfaction of this graduation requirement for Valencia may also satisfy the foreign
                                 language admission requirement for Florida public universities; it may or may not
                                 satisfy a specific university graduation requirement. Students are encouraged to find
                                 out the specific requirements of institutions in which they are interested.
                              </p>
                              
                              <p> </p>
                              	
                           </li>
                        </ul>
                        
                        	
                        		
                     </div>
                     		
                  </div>								
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/course-requirements.pcf">©</a>
      </div>
   </body>
</html>