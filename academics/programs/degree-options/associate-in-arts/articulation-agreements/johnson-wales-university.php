<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Johnson and Wales University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="johnson and wales university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/johnson-wales-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/">Articulation Agreements</a></li>
               <li>Johnson and Wales University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Johnson and Wales University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Johnson and Wales?</h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/index.html">A.A. Degree</a>or specified <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degrees</a> or specified A.A.S. degrees
                                 </p>
                                 
                                 
                                 <h4>What degree will be earned at Johnson and Wales?</h4>
                                 
                                 <p>Bachelor's Degree</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>N/A</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>Johnson and Wales will give junior standing and all credit awarded by the student's
                                    <a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/index.html">A.A. Degree </a>provided they meet all degree prerequisites, general education and graduation requirements
                                    toward completion of the Bachelor’s degree at Johnson and Wales University. Appropriate
                                    courses within the A.S. or A.A.S. degrees will articulate to related Bachelor degree
                                    programs at Johnson and Wales.&nbsp; General education courses will apply toward the total
                                    general education hours required to earn the Bachelor’s degree at Johnson and Wales,
                                    provided that transfer credits must meet the specific degree course requirements.
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelor’s Degree will be awarded?</h4>
                                 
                                 <p>A minimum of 120 semester hours required of the degree being sought.</p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>None noted. (see Agreement) </p>
                                 
                                 
                                 <h4>Are there any scholarships available?</h4>
                                 
                                 <p>Yes, for A.A., A.S. and A.A.S. degree holders with a qualifying GPA scholarships are
                                    guaranteed.&nbsp; See agreement for specific details and amounts.
                                 </p>
                                 
                                 
                                 <h4>What date was the agreement signed?</h4>
                                 
                                 <p>May 2014 </p>
                                 
                                 
                                 <h4>Is there any special information?</h4>
                                 
                                 <p>None noted. </p>
                                 
                                 
                                 <h4>Who can be contacted for information at Johnson and Wales University?</h4>
                                 
                                 <p>Kristin Turchetta,<br>
                                    Articulation Agreements and University Transfer
                                 </p>
                                 
                                 <p>401-598-1481<br>
                                    or<br>
                                    <a href="mailto:kturchetta@jwu.edu">kturchetta@jwu.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://valencia.smoothesttransfer.com/">http://valencia.smoothesttransfer.com/</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/johnson-wales-university.pcf">©</a>
      </div>
   </body>
</html>