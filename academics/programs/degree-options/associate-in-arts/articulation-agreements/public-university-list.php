<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Public University List  | Valencia College</title>
      <meta name="Description" content="List of Florida's Public Colleges and Universities">
      <meta name="Keywords" content="list of colleges, florida's list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/public-university-list.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/">Articulation Agreements</a></li>
               <li>Public University List </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        
                        <h2>Public University List</h2>
                        
                        <p>Note: It is recommended that students check their financial aid status prior to transferring.</p>
                        
                        
                        <p>Florida's Public Colleges and Universities<br>
                           
                        </p>
                        
                        
                        <ul>
                           
                           <li>
                              <i class="icon_star"></i> <a href="http://www.famu.edu/" target="_blank">Florida Agricultural and Mechanical University</a>
                              
                           </li>
                           
                           <li><a href="http://www.fau.edu/index.php" target="_blank">Florida Atlantic University</a></li>
                           
                           <li>
                              <a href="http://www.fgcu.edu/" target="_blank">Florida Gulf Coast University</a> 
                           </li>
                           
                           <li><a href="http://www.fiu.edu/" target="_blank">Florida International University</a></li>
                           
                           <li><a href="http://www.fsu.edu/" target="_blank">Florida State University</a></li>
                           
                           <li><a href="http://art.fsu.edu/Undergraduate/Transfer-Students">(FSU) Art Student General Transfer Information</a></li>
                           
                           <li><a href="http://www.ncf.edu/" target="_blank">New College of Florida</a></li>
                           
                           <li>
                              <i class="icon_star-half"></i> <a href="https://www.seminolestate.edu/bachelor-degrees/information-systems-technology" target="_blank">Seminole State College</a>
                              
                           </li>
                           
                           <li>
                              <i class="icon_star-half_alt"></i> <a href="http://www.spcollege.edu/" target="_blank">St. Petersburg College</a>
                              
                           </li>
                           
                           <li><a href="http://www.ucf.edu/" target="_blank">University of Central Florida</a></li>
                           
                           <li> <a href="../UCFHonorstoHonors.cfm">University of Central Florida (Honors)</a>
                              
                           </li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofcentralflorida/#programrequirementstext#programrequirementstext"> Articulated Architecture (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/"> Articulated Computer Science (University of Central Florida)</a></li>
                           
                           <li><a href="UniversityofCentralFloridaAATransferPlaninInformationTechnology.cfm"> Articulated Information Technology (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/">Articulated Engineering (University of Central Florida)</a></li>
                           
                           <li>
                              <i class="icon_star_alt"></i> <a href="http://www.ufl.edu/" target="_blank">University of Florida</a>
                              
                           </li>
                           
                           <li><a href="http://www.unf.edu/" target="_blank">University of North Florida </a></li>
                           
                           <li><a href="http://www.usf.edu/index.asp" target="_blank">University of South Florida </a></li>
                           
                           <li><a href="http://www.uwf.edu/" target="_blank">University of West Florida</a></li>
                           
                        </ul>
                        
                        
                        <p> </p>
                        
                        <p><i class="icon_star"></i> For a  description and details of the FAMU articulation agreement, please <a href="documents/FAMUarticultionagreement8-1-11to7-30-15.pdf">click here</a> to read the full Agreement.
                           
                        </p>
                        
                        <p><i class="icon_star-half"></i> This is an agreement with guidelines and procedures for admission to SSC's Bachelor
                           of Science Degree program in Information Systems Technology after completing an A.S.
                           Degree in Computer Information Technology at Valencia College. For a full description
                           and details of the agreement, please <a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/documents/SSC-ValenciaBSInfoSysTechArticuAgrmntsigned2015.pdf">click here</a>. 
                        </p>
                        
                        
                        <p><i class="icon_star-half_alt"></i> This is an agreement designed to allow students who have graduated from a Florida
                           Community College with an Associate of Science (A.S.) Degree in Nursing to have increased
                           options while seeking further educational opportunities in nursing via online instruction.
                           It is a cooperative agreement between the Florida Community College System and the
                           Association of Jesuit Colleges and Universities. 
                        </p>
                        
                        <p><i class="icon_star_alt"></i> For information on the Warrington College's online Business Program, contact Samanta
                           Matadin at <a href="tel:407-582-2320">407-582-2320</a>; or visit Valencia College's East Campus, Building 3, Room 108. To contact Ms. Matadin
                           via e-mail, the address is <a href="mailto:smatadin@valenciacollege.edu">smatadin@valenciacollege.edu</a> 
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Association of Jesuit Colleges and Universities</h3>
                        
                        <p>This is an agreement designed to allow students who have graduated from a Florida
                           Community College with an Associate of Science (A.S.) Degree in Nursing to have increased
                           options while seeking further educational opportunities in nursing via online instruction.
                           It is a cooperative agreement between the Florida Community College System and the
                           Association of Jesuit Colleges and Universities. 
                           
                        </p>
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        <div class="col-md-5 col-sm-5">
                           
                           <ul>
                              
                              <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/documents/AJCUNursingAgreement.pdf">Articulation Agreement</a></li>
                              
                              <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/documents/AJCUOnlineInstitutions.pdf">AJCU Online Nursing Degree Program</a></li>
                              
                              <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/documents/AJCUProgramsandLocations.pdf">AJCU Programs and Participating Institutions</a></li>
                              
                              
                           </ul>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/public-university-list.pcf">©</a>
      </div>
   </body>
</html>