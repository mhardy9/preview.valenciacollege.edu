<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Columbia College  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="Columbia College, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/columbia-college.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/">Articulation Agreements</a></li>
               <li>Columbia College </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Columbia College</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What  Valencia degree is articulated with Columbia College? </h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/index.html">A.A. Degree </a> or <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degree </a></p>
                                 
                                 
                                 <h4>What degree will be earned at Columbia College?</h4>
                                 
                                 <p>B.A. Degree or B.S. Degree</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>No deadline </p>
                                 
                                 
                                 <h4>How many credit hours will be accepted? </h4>
                                 
                                 <p> At least sixty (60), and up to eighty-one (81), credit hours.</p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>Students are required to complete at Columbia a program of study which equals a minimum
                                    of 120 credit hours required of the degree being sought. Please refer to the <a href="documents/ColumbiaCollegearticulationagreement1-20-15.pdf">Agreement</a>, Section IIA for additional information regarding graduation requirements at Columbia
                                    College. 
                                 </p>
                                 
                                 
                                 <p>Are there mandatory courses needed for acceptance? </p>
                                 
                                 <p>None noted for acceptance; however, Section IIB of the <a href="documents/ColumbiaCollegearticulationagreement1-20-15.pdf">Agreement </a>outlines Columbia's General Education requirements, including the specific courses
                                    of ENC 1101 and ENC 1102 (or the equivalent Honors courses). 
                                 </p>
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>Associate Degree Transfer Grant is 5% of the tuition. <br>
                                    Eligibility for the Associate Transfer Grant includes the following criteria:
                                 </p>
                                 
                                 <p>The Associate Degree must have been earned within the past twelve   months and consist
                                    of 60 credit hours or its equivalent. The student   must not have received the Associate
                                    degree from Columbia College. The   student may not have completed additional college
                                    work since the award   of the Associate degree.
                                 </p>
                                 
                                 <p>The Associate Transfer Grant provides a reduction in tuition for each   eligible student.</p>
                                 
                                 <p>The reduction in tuition is 5%, and the award is for 6 sessions (3 semesters) per
                                    year for two years. The grant expires after two years. Students might qualify <a href="http://web.ccis.edu/offices/financialaid/scholarships.aspx">for other scholarships</a>. 
                                 </p>
                                 
                                 <p><a href="http://www.ccis.edu/offices/financialaid/scholarshipfinder/NationwideScholarships.asp" target="_blank">http://www.ccis.edu/offices/financialaid/scholarshipfinder/NationwideScholarships.asp</a></p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>May 2017 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Eligible students can immediately enroll in the Columbia College Associate to Bachelor’s
                                    program to lock in their academic catalog to the current year, thus avoiding the potential
                                    for future curriculum changes associated with degree plans.
                                 </p>
                                 Please see <a href="/documents/academics/programs/degree-options/associate-in-arts/PROGRAMADDENDUMGUIDE.doc">Program Addendum Guide</a> for specific degrees included in transfer program.
                                 
                                 
                                 
                                 <h4>Who can be contacted for information at Columbia College? </h4>
                                 
                                 <p>Diane Hibbs<br>
                                    Assistant Registrar, Special Processes<br>
                                    Columbia College<br>
                                    1001 Rogers Street<br>
                                    Columbia, MO 65216<br>
                                    <a href="mailto:dehibbs@ccis.edu">dehibbs@ccis.edu</a><br>
                                    1-573-875-7670
                                 </p>
                                 
                                 <p>Or</p>
                                 
                                 <p>Stephanie DeLaney <br>
                                    (573) 875-7507 <br>
                                    <a href="mailto:sldelaney@ccis.edu">sldelaney@ccis.edu</a>.
                                    
                                 </p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.ccis.edu/" target="_blank">Columbia Home Page</a><br>
                                       <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/ColumbiaCollegearticulationagreement1-20-15.pdf"> Agreement</a></strong><br>
                                       <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/PROGRAMADDENDUMGUIDE.doc">Program Addendum Guide </a> <br></strong></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/columbia-college.pcf">©</a>
      </div>
   </body>
</html>