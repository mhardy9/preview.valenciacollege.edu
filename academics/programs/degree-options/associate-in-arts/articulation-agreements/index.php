<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Articulation Agreements  | Valencia College</title>
      <meta name="Description" content="This site will give you information about all of Valencia's articulation agreements with other institutions">
      <meta name="Keywords" content="articulation agreements, articulation, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Arts (A.A.)</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li>Articulation Agreements</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        
                        <h2>Articulation Agreements</h2>
                        
                        <p>You may ask what <a href="/academics/programs/degree-options/associate-in-arts/documents/principles-of-articulation.pdf">articulation</a> is, and what it has to do with you. As a  Valencia student, you have options as to
                           where you would like to continue your academic career. The State of Florida already
                           has an articulation agreement between community colleges and the eleven State universities
                           (6A-10.024). This agreement states that any student who graduates with an A.A. Degree
                           will be accepted to one of the <a href="public-university-list.php">eleven state universities</a>. However, the articulation agreement does not guarantee that a student will be immediately
                           accepted into the major of his/her choice within that university. In addition, the
                           agreement guarantees acceptance into one of the state universities, but not necessarily
                           the student's first choice (please refer to <a href="/academics/programs/degree-options/associate-in-arts/transfer-guarantees.php">Transfer Guarantees</a> for additional information). 
                        </p>
                        
                        <p>The Pre-Majors are included in the Associate in Arts Degree section of the catalog,
                           and by clicking
                           on the Pre-Majors and General Studies link below or on the Navigation Bar on the left
                           side of the screen.
                        </p>
                        
                        <p>In addition to the agreement within the State University System, Valencia is partnering
                           with many other institutions to expand student options for seamless transition. Some
                           of the agreements are created with an Articulated Pre-Major. They are designed for
                           students to transfer to a particular public or private university as a junior to complete
                           a four-year bachelor degree in a specific major.
                        </p>
                        
                        <p>This site will give you information about all of Valencia's articulation agreements
                           with other institutions (credits counted, scholarship opportunities, degree options,
                           etc.). <a href=" /academics/programs/degree-options/associate-in-arts/articulation-agreements/college-list.php">Click here to Link to specific Articulation Agreement information. </a></p>
                        
                        <p>If you have specific questions regarding these agreements, please contact Dr. Karen
                           Borglum at <a href="mailto:kborglum@valenciacollege.edu">kborglum@valenciacollege.edu</a>.<br>
                           <br>
                           
                        </p>
                        
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/index.pcf">©</a>
      </div>
   </body>
</html>