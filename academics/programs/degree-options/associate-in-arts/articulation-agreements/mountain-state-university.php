<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Mountain State University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="mountain state university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/mountain-state-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/">Articulation Agreements</a></li>
               <li>Mountain State University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Mountain State University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Mountain State University?</h4>
                                 
                                 <p> A.S. Degrees</p>
                                 
                                 
                                 <h4>What degree will be earned at Mountain State University?</h4>
                                 
                                 <p>Bachelor's Degree in Nursing</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>N/A</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>Valencia's A.S. Degree in Nursing Program, which satisfies approx. 68 hours of the
                                    132 credit hour requirement for MSU's program.
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelor’s Degree will be awarded?</h4>
                                 
                                 <p>Students will need to complete a program of study equalling approx. the number of
                                    hours required in the junior and senior years of the degree program being sought.
                                 </p>
                                 
                                 <p>For students who enter having completed the Valencia A.S. Nursing degree, they will
                                    complete 64 credit hours of additional coursework (for a total of 132 hours) Please
                                    refer to <a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitB.pdf"><strong><em>Exhibit B</em></strong></a>, which identifies specific coursework to be completed at MSU.
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>Please refer to Mountain State's Agreement, <a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitA.pdf"><em><strong> Exhibit A</strong></em></a>&nbsp;for course equivalencies. 
                                 </p>
                                 
                                 
                                 <h4>Are there any scholarships available?</h4>
                                 
                                 <p>Scholarships in the form of tuition waivers for all Program students equal to 10%
                                    of MSU's tuition rates currently in effect; scholarships will be provided for a period
                                    of four (4) semesters (only for students pursuing degree(s) related to this Agreement;
                                 </p>
                                 
                                 <p>Also, the $25.00 application fee will be waived for all Valencia graduates.</p>
                                 
                                 
                                 <h4>What date was the agreement signed?</h4>
                                 
                                 <p>September 2009</p>
                                 
                                 
                                 <h4>Is there any special information?</h4>
                                 
                                 <p>Please refer to <em><strong><a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitA.pdf">Exhibit A</a></strong></em> and <a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitB.pdf"><em><strong>Exhibit B </strong></em></a></p>
                                 
                                 
                                 <h4>Who can be contacted for information at Mountain State University?</h4>
                                 
                                 <p>Frank C. Lynah<br>
                                    Corporate Relation &amp; Enrollment Manager<br>
                                    407-774-6200<br>
                                    Fax: 407-774-6277<br>
                                    <a href="mailto:flynah@mountainstate.edu">flynah@mountainstate.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p> <strong><a href="http://www.mountainstate.edu/">Mountain State  Home Page</a></strong><br>
                                    <strong><a href="http://www.mountainstate.edu/programs/nursing/nursing.aspx">Mountain State (Nursing Page)</a> <br>
                                       <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/MSUAgreementNursing.pdf">Mountain State Nursing Agreement</a><br>
                                          <strong> <a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitA.pdf">Exhibit A (Course Equivalencies)</a><br>
                                             <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/MSUNursingExhibitB.pdf">Exhibit B (Articulated Bridge Program)</a> </strong></strong></strong></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/mountain-state-university.pcf">©</a>
      </div>
   </body>
</html>