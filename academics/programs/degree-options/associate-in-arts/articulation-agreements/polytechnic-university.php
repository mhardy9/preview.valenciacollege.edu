<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Polytechnic University of the Americas  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="polytechnic university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/articulation-agreements/polytechnic-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Arts (A.A.)</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/">Articulation Agreements</a></li>
               <li>Polytechnic University of the Americas </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Polytechnic University of the Americas</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Polytechnic University of the Americas? </h4>
                                 
                                 <p><a href="/documents/academics/programs/degree-options/associate-in-arts/AcademicPrograms.pdf">Articulated Pre-Major in Engineering (Polytechnic University of the Americas) </a></p>
                                 
                                 
                                 <h4>What degree will be earned at Polytechnic University?</h4>
                                 
                                 <p>B.S. Degree in Engineering</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>N/A</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>Minimum of 60, to include all specific courses in the Appendix with a grade of "C"
                                    or better in each course.
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>The number of credit hours required in the junior and senior years of the degree program
                                    being sought
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>Please refer to Polytechnic Agreement <em><a href="/documents/academics/programs/degree-options/associate-in-arts/PolytecAddendum.pdf">Appendix</a></em></p>
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>None noted; please contact Polytechnic for information</p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>February 20, 2006  </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>N/A</p>
                                 
                                 
                                 <h4>Who can be contacted for information at Polytechnic University? </h4>
                                 
                                 <p><strong>Lucy Negron</strong> <br>
                                    Admission and Promotion Agent <br>
                                    407-677-7000 ext. 808<br>
                                    <a href="mailto:Inegron@pupr.edu">Inegron@pupr.edu</a><br>
                                    or<br>
                                    President Ernesto Vazquez-Barquet<br>
                                    <a href="mailto:evazques@pupr.edu">evazques@pupr.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.pupr.edu/orlando/" target="_blank">Polytechnic Univ. of the Americas Home Page</a></strong><br>
                                    <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/PolytechnicAgreement.pdf">Polytechnic Agreement</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/articulation-agreements/polytechnic-university.pcf">©</a>
      </div>
   </body>
</html>