<ul>
                        <li><a href="/academics/programs/degree-options/associate-in-arts/index.php">Associate in Arts Degree</a>
                        </li>
                        <li><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.php">Pre-Majors List</a>

                        </li>

                        <li class="submenu"><a href="/academics/programs/degree-options/associate-in-arts/requirements-guarantees.php" class="show-submenu">Requirements and Guarantees <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>

                            <ul>
                                <li><a href="/academics/programs/degree-options/associate-in-arts/course-requirements.php">Course Requirements</a></li>
                                <li><a href="/academics/programs/degree-options/associate-in-arts/graduation-requirements.php">Graduation Requirements</a></li>
                                <li><a href="/academics/programs/degree-options/associate-in-arts/transfer-guarantees.php">Transfer Guarantees</a>
                            </ul>
                        </li>
                        <li class="submenu"><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/index.php" class="show-submenu">Articulation Agreements <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
                            <ul>
                                <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/college-list.php">College List</a></li>
                                <li><a href="/academics/programs/degree-options/associate-in-arts/articulation-agreements/public-university-list.php">Public University List</a></li>

                            </ul>
