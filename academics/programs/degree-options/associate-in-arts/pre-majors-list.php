<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Pre-Majors List  | Valencia College</title>
      <meta name="Description" content="Pre-Majors, General AA Degree &amp;amp; Transfer Plans">
      <meta name="Keywords" content="pre-majors, general aa degree, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/degree-options/associate-in-arts/pre-majors-list.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/degree-options/associate-in-arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/degree-options/">Degree Options</a></li>
               <li><a href="/academics/programs/degree-options/associate-in-arts/">Associate In Arts</a></li>
               <li>Pre-Majors List </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Pre-Majors List </h2>
                        
                        <p>Pre-Majors, General AA Degree &amp; Transfer Plans </p>
                        
                        <a name="GeneralAA" id="GeneralAA"></a><h3>General A.A.</h3>
                        
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#General_Education_#General_Education_">General</a></li>
                           
                        </ul>
                        <a name="Articulated_PreMajors" id="Articulated_PreMajors"></a>
                        
                        <h3 class="heading_divider_gray">Articulated Pre-Majors</h3>
                        
                        
                        <p> </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofcentralflorida/">Articulated Architecture (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/architectureuniversityofflorida/">Articulated Architecture (University of Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/artstudiofineartringlingcollege/">Articulated Art, Studio/Fine Art (Ringling College of Art and Design)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/aviationfit/">Articulated Aviation Management (Florida Institute of Technology)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/">Articulated Computer Science (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computersciencerollins/">Articulated Computer Science (Rollins College)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/electricalcomputerengineeringtechnologyvalenciacollege/">Articulated Electrical and Computer Engineering Technology (Valencia College)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringfloridainstituteoftechnology/">Articulated Engineering (Florida Institute of Technology)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringpolytechnicuniversity/">Articulated Engineering (Polytechnic University of Puerto Rico, Orlando campus)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/">Articulated Engineering (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofmiami/">Articulated Engineering (University of Miami)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/informationtechnologyuniversityofcentralflorida/">Articulated Information Technology (University of Central Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/informationtechnologyuniversityofsouthflorida/">Articulated Information Technology (University of South Florida)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/logisiticsmanagementfloridainstituteoftechnology/">Articulated Logistics Management (Florida Institute of Technology)</a></li>
                           
                        </ul>
                        
                        
                        <p><a name="PreMajors" id="PreMajors"></a></p>
                        					
                        <hr class="styled_2">
                        
                        <h3>Pre-Majors</h3>
                        
                        
                        <p>    </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/artstudiofineart/">Art, Studio/Fine Art</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Dance Performance</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/earlychildhoodeducation/">Early Childhood Education</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/elementaryeducation/">Elementary Education</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/musicperformance/">Music Performance</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/psychology/">Psychology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/signlanguageinterpretation/">Sign Language Interpretation</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/theatredramadramaticarts/">Theatre/Drama/Dramatic Arts</a></li>
                           
                        </ul>
                        
                        <p>
                           <a name="TransferPlans" id="TransferPlans"></a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Transfer Plans</h3>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/accounting/">Accounting</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/biology/">Biology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/biomedicalsciences/">Biomedical Sciences</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/">Business Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/chemistry/">Chemistry</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/computerscience/">Computer Science</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/economics/">Economics</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/educationgeneralpreparation/">Education</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/engineering/">Engineering</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/english/">English</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/healthservicesadministration/">Health Services Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/history/">History</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/horticulturalscience/">Horticulture Science</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/humanresourcesmanagement/">Human Resources Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/informationtechnology/">Information Technology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/journalism/">Journalism</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/managementinformationsystems/">Management Information Systems</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/marinebiology/">Marine Biology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/mathematics/">Mathematics</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/nutritionanddietetics/">Nutrition and Dietetics</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/philosophy/">Philosophy</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/politicalsciencegovernment/">Political Science</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/psychology/">Psychology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/publicadministration/">Public Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/publicrelationsorganizationalcommunication/">Public Relations/Organizational Communications</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/socialsciences/">Social Sciences</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/sociology/">Sociology</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/statistics/">Statistics</a></li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     	
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/degree-options/associate-in-arts/pre-majors-list.pcf">©</a>
      </div>
   </body>
</html>