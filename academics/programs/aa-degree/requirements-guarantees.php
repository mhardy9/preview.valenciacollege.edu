<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Requirements and Guarantees  | Valencia College</title>
      <meta name="Description" content="Associate in Arts Degree Course and Graduation Requirements at Valencia College">
      <meta name="Keywords" content="requirements, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/requirements-guarantees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Requirements and Guarantees </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list"> <a href="course-requirements.php"><img src="/_resources/img/course_1.jpg" alt="">
                                       
                                       <div class="short_info">
                                          
                                          <h3>Requirements and Guarantees</h3>
                                          
                                       </div>
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block"> </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Course Requirements</h3>
                                    
                                    <p> The Associate in Arts Degree requires a minimum of 60 college-level credit hours
                                       including 36 hours in general education, 24 hours of acceptable electives, and .....
                                    </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div> <a href="course-requirements.php" class="button-outline">Details</a> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list"> <a href="graduation-requirements.php"><img src="/_resources/img/course_2.jpg" alt="">
                                       
                                       <div class="short_info">
                                          
                                          <h3>Graduation Requirements</h3>
                                          
                                       </div>
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block"> </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Graduation Requirements</h3>
                                    
                                    <p> Responsibility for meeting the requirements for graduation with an Associate in Arts
                                       degree rests with the student. To be awarded an A.A. degree from Valencia College
                                       a student must have ..... 
                                    </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div> <a href="graduation-requirements.php" class="button-outline">Details</a> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list"> <a href="transfer-guarantees.php"><img src="/_resources/img/course_2.jpg" alt="">
                                       
                                       <div class="short_info">
                                          
                                          <h3>Transfer Guarantees</h3>
                                          
                                       </div>
                                       </a> 
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block"> </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Transfer Guarantees</h3>
                                    
                                    <p> Community College Associate in Arts graduates are guaranteed certain rights under
                                       the statewide Articulation Agreement 6A-10.024. ..... 
                                    </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div> <a href="transfer-guarantees.php" class="button-outline">Details</a> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/requirements-guarantees.pcf">©</a>
      </div>
   </body>
</html>