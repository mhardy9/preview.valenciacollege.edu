<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation Requirements  | Valencia College</title>
      <meta name="Description" content="Responsibility for meeting the requirements for graduation with an Associate in Arts degree rests with the student. ">
      <meta name="Keywords" content="graduation requirements, requirements, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/graduation-requirements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Graduation Requirements </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Graduation Requirements </h2>
                        
                        <p>for the Associate in Arts Degree</p>
                        
                        
                        <p>Responsibility for meeting the requirements for graduation with an Associate in Arts
                           degree rests with the student. To be awarded an A.A. degree from Valencia College
                           a student must have an active student record and do the following:
                        </p>
                        
                        <p> </p>
                        
                        <ol>
                           
                           <li>Complete a minimum of 60 acceptable college-level credits which may include the following:
                              
                              <ul>
                                 
                                 <li>a maximum of six elective credits may be in music ensemble courses and/or physical
                                    education activity courses; and/or
                                 </li>
                                 
                                 <li>a maximum of twelve credits may be in college-level English as a Second Language for
                                    Academic Purposes (EAP) courses; and/or
                                 </li>
                                 
                                 <li>a maximum of four credits may be in internship courses; and/or</li>
                                 
                                 <li>a maximum of four credits may be in <a href="http://valenciacollege.edu/search/?P=SLS%202940" title="SLS 2940" onclick="return showCourse(this, 'SLS 2940');">SLS 2940</a> SERVICE LEARNING; and/or
                                 </li>
                                 
                                 <li>a maximum of eight credits may be in military science courses; and/or</li>
                                 
                                 <li>a maximum of 45 credits may be in any combination of Advanced Placement (AP), College
                                    Level Examination Program (CLEP), credit-by-examination, DANTES, Excelsior, experiential
                                    learning, and International Baccalaureate.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Complete the 36 credits of General Education described in the A.A. degree course requirements
                              section. Honors sections of general education courses will satisfy this program requirement.
                           </li>
                           
                           <li>Complete the 24 credits of electives described in the A.A. degree course requirements
                              section.
                           </li>
                           
                           <li>Satisfy the foreign language proficiency requirement described in the A.A. degree
                              course requirements section.
                           </li>
                           
                           <li>Satisfy entry testing requirements and complete, with a minimum grade of C, all required
                              mandatory courses in reading, mathematics, English and/or English for Academic Purposes.
                           </li>
                           
                           <li>Earn a cumulative grade point average (GPA) of at least 2.0 in:
                              
                              <ul>
                                 
                                 <li>all earned credit hours at Valencia (Institutional GPA)</li>
                                 
                                 <li>all earned credit hours (Overall GPA)</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Submit official transcripts of all college course work, including course work attempted
                              at other institutions following initial enrollment at Valencia.
                           </li>
                           
                           <li>Complete at Valencia at least 25% of the college-level credits required for the degree.</li>
                           
                           <li>Complete with a grade of C or better the courses which satisfy the Gordon Rule requirements
                              (6A-10.30 Florida Administrative Code):
                              
                              <ul>
                                 
                                 <li>ENC 1101 or <a href="http://valenciacollege.edu/search/?P=ENC%201101H" title="ENC 1101H" onclick="return showCourse(this, 'ENC 1101H');">ENC 1101H</a> FRESHMAN COMPOSITION I - HONORS
                                 </li>
                                 
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/search/?P=ENC%201102" title="ENC 1102" onclick="return showCourse(this, 'ENC 1102');">ENC 1102</a> FRESHMAN COMPOSITION II or <a href="http://valenciacollege.edu/search/?P=ENC%201102H" title="ENC 1102H" onclick="return showCourse(this, 'ENC 1102H');">ENC 1102H</a> FRESHMAN COMPOSITION II - HONORS
                                 </li>
                                 
                                 
                                 <li>One three-credit Gordon Rule Humanities course</li>
                                 
                                 <li>One three-credit Gordon Rule Social Science course</li>
                                 
                                 <li>Six credits of general education mathematics courses.</li>
                                 
                              </ul>
                              
                              
                           </li>
                           
                           <li>Obtain a degree audit through your Atlas account and review it for readiness to submit
                              your application for graduation.
                           </li>
                           
                           <li>Submit an application for graduation online through Atlas by the deadline date listed
                              in the Academic Calendar in the online official catalog. You must have at least 60
                              college-level credits, including all courses for which you are currently registered,
                              in order to submit your graduation application.
                           </li>
                           
                           <li>Fulfill all financial obligations to Valencia.</li>
                           
                        </ol>
                        
                        
                        <p><strong>Important Notes:</strong></p>
                        
                        
                        <ol>
                           
                           <li>Valencia awards degrees to students at the end of each Fall, Spring, and Summer Full
                              Term, and holds an annual commencement ceremony at the end of Spring Term.
                           </li>
                           
                           <li>Your governing catalog is the Valencia College catalog in effect at the time of your
                              initial enrollment in associate or Bachelor's degree credit courses at Valencia. A
                              Valencia catalog is valid for five academic years. If you are applying to a limited
                              access program and the admissions requirements change within your five year catalog
                              time period, you will qualify for a transition plan to the new requirements. Your
                              governing catalog will be updated to the Valencia College catalog in effect at the
                              time of your enrollment in the limited access program. You may officially declare
                              any subsequent catalog as your governing catalog and follow its requirements for your
                              initial degree or certificate program until that catalog expires; however, if you
                              change your degree or certificate program, you are required to choose from the current
                              degree and certificate program offerings and follow the graduation requirements of
                              the current catalog. If your governing catalog has expired, your new governing catalog
                              will be the catalog in effect in your next term of enrollment. Should the District
                              Board of Trustees approve changes in program requirements to be effective within the
                              academic year, the revised requirements will be available in the catalog, academic
                              departments, and Career Program Advisor's offices. Students affected by the change
                              in requirements should contact the appropriate Program Advisor, Program Director,
                              or Academic Dean.
                           </li>
                           
                           <li>You may earn one Associate in Arts degree from Valencia. If you have earned an Associate
                              in Arts or Bachelor's  degree from an institution that has regional accreditation
                              in the U.S., or if you have earned the equivalent of an Associate in Arts or Bachelor's
                              degree in a foreign country, you are not eligible to be awarded an Associate in Arts
                              degree from Valencia. (In addition to the Associate in Arts degree, you may earn any
                              number of Associate in Science degrees.)<br>If you want to earn more than one degree at Valencia, you may complete the first degree
                              and then pursue another degree or you may pursue two degrees at the same time. Whenever
                              possible, a course will be applied to more than one degree.
                           </li>
                           
                           <li>You will not receive grade forgiveness for any course that counts toward your degree
                              and is repeated after you have been awarded the degree from Valencia.
                           </li>
                           
                        </ol>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/graduation-requirements.pcf">©</a>
      </div>
   </body>
</html>