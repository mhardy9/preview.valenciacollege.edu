<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transfer Guarantees  | Valencia College</title>
      <meta name="Description" content="This site will give you information about all of Valencia's articulation agreements with other institutions">
      <meta name="Keywords" content="articulation agreements, articulation, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/transfer-guarantees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Transfer Guarantees </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Transfer Guarantees</h2>
                        
                        <p>Community College Associate in Arts Transfer Guarantees</p>
                        
                        
                        <p>Community College Associate in Arts graduates are guaranteed certain rights under
                           the statewide Articulation Agreement 6A-10.024.  This Articulation Agreement governs
                           the transfer of students from Florida public community colleges to the state university
                           system.  The agreement addresses General Admission to a state university and Program
                           Admission. 
                        </p>
                        
                        <hr class="styled_2">
                        
                        
                        <h3>General Admission to Florida State Universities</h3>
                        
                        <p>Guarantees</p>
                        
                     </div>
                     
                     
                     <p>The Florida Articulation Agreement designates the Associate in Arts degree as the
                        transfer degree to Florida state universities.  In doing so, the Agreement guarantees
                        that:
                     </p>
                     
                     
                     <ol>
                        
                        <li>Community college A.A. Degree holders will be granted admission
                           to one of eleven (11) universities but not necessarily to limited
                           access programs. 
                        </li>
                        
                        <li>Upon transferring to a state university, A.A. Degree graduates will be awarded at
                           least 60 credit hours towards the baccalaureate degree.
                           
                        </li>
                        
                        <li>The university catalog in effect the year the A.A. Degree student first enrolled at
                           the community college will remain in effect for the student's program, provided the
                           student maintains continuous enrollment as defined in that catalog.
                           
                        </li>
                        
                        <li>Once a student has completed the General Education Core and this is so noted on the
                           transcript, regardless of whether or not an A.A. Degree is awarded, no other state
                           university or community college to which the student may transfer can require additional
                           courses to the General Education Core.
                           
                        </li>
                        
                        <li>When transferring among institutions participating in the statewide course numbering
                           system, a receiving institution must accept all courses taken at the transfer institution,
                           if the same course with the same course number is offered at the receiving institution.
                           
                        </li>
                        
                        <li>Credits earned through acceleration mechanisms (CLEP, AP, PEP, early enrollment, International
                           Baccalaureate, and dual enrollment courses) within the A.A. Degree at the community
                           college will be transferable to the state university.
                           
                        </li>
                        
                     </ol>
                     
                     <p>Students without an A.A. Degree who are seeking admission to a state university do
                        not have all the guarantees provided by the Articulation Agreement and may be denied
                        admission or lose credit when transferring.  In most cases students without an A.A.
                        Degree will have to meet freshman admissions standards.
                        
                     </p>
                     
                     
                     <hr class="styled_2">
                     
                     <h3>Admission to Specific Programs at Florida State Universities</h3>
                     
                     <p></p>
                     
                     
                     
                     <p>    The universities determine the courses and prerequisites that must be taken in
                        order to receive a baccalaureate degree for a chosen program.  Although all credit
                        earned towards an A.A. Degree will transfer to a university, not all credit may satisfy
                        the program prerequisites or the course requirements for a baccalaureate degree. 
                        Therefore, it is important to know the program requirements and to take as many courses
                        as possible at Valencia while completing the A.A. Degree.
                        
                     </p>
                     
                     
                     <p><strong>Limited Access</strong></p>
                     
                     <p>Due to limited resources, some programs have limited enrollments; in order to select
                        students these programs may have additional admission requirements which are more
                        restrictive than the university's general admission requirements.  These requirements
                        include one or more of the following: completion of specific courses, grade point
                        average, an interview, test scores, auditions, and submission of a portfolio and deadlines.
                     </p>
                     
                     <p>
                        
                     </p>
                     
                     <p><strong>Guarantees</strong></p>
                     A.A. graduates are not guaranteed admission into limited access programs but are guaranteed
                     that:
                     
                     <ol>
                        
                        <li>The community college student will have the same opportunity to enroll in a university
                           limited access program as the native university student.
                           
                        </li>
                        
                        <li>Selection and enrollment criteria for a university limited access program will be
                           established and published in catalogs and appropriate publications.  Notification
                           of any changes in a program will include sufficient time for prospective students
                           to adjust to meet program criteria.
                           
                        </li>
                        
                     </ol>
                     
                     <p> Resources are available for planning your transfer to a bachelor's degree program.
                        Counselors and advisors are available through the Advising Centers on each campus
                        at Valencia to help you plan your individual transfer program.
                        
                     </p>
                     
                     <p><strong>Appeals</strong></p>
                     
                     <p> Should any of these guarantees be denied, the student has the right to file an appeal.
                        Each state university has established appeal procedures.  Students may contact the
                        state university articulation officer for information about these procedures.
                        
                     </p>
                     
                     
                     
                     
                     
                  </div>
                  
                  
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/transfer-guarantees.pcf">©</a>
      </div>
   </body>
</html>