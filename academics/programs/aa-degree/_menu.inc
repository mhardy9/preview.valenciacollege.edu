<ul>
<li><a href="/academics/programs/aa-degree/index.php">A.A. Degree</a></li>
<li><a href="/academics/programs/aa-degree/pre-majors-general-studies.php">Pre-Majors &amp; General Studies</a></li>
<li class="submenu"><a class="show-submenu" href="#">Requirements <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/academics/programs/aa-degree/graduation-requirements.php">Graduation Requirements</a></li>
<li><a href="/academics/programs/aa-degree/requirements.php">General Education Requirements</a></li>
</ul>
	</li>	
<li class="submenu"><a class="show-submenu" href="#">Transfer <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/academics/programs/aa-degree/transfer-guarantees.php">Transfer Guarantees</a></li>
<li><a href="/academics/programs/aa-degree/transfer-plans.php">Transfer Plans</a></li>
</ul>
	</li>	
		
	
	<li class="submenu"><a class="show-submenu" href="#">Articulated Pre-Majors &amp; Agreements <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/academics/programs/aa-degree/articulation/index.php">Articulation Agreements</a></li>
<li><a href="/academics/programs/aa-degree/articulation/college-list.php">College List</a></li>
<li><a href="/academics/programs/aa-degree/articulation/public-university-list.php">Public University List</a></li>
</ul>
	</li>
</ul>






