<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College List  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/college-list.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Arts (A.A.)</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>College List</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        
                        <h2>College List</h2>
                        
                        <p>Note: It is recommended that students check their financial aid status prior to transferring.</p>
                        
                        
                        <p>Private Colleges, Universities, and Technical Schools that have Agreements with Valencia<br>
                           Community Colleges that have Agreements to articulate to Valencia's Bachelor Degree
                           Program(s)<i class="icon_star"></i> 
                        </p>
                        
                        
                        <ul>
                           
                           <li><a href="american-university-of-antigua.php">American University of Antigua</a></li>
                           
                           <li>
                              <a href="ashford-university.php">Ashford University</a> 
                           </li>
                           
                           <li><a href="barry-university.php">Barry University</a></li>
                           
                           <li><a href="belhaven-college.php">Belhaven University </a></li>
                           
                           <li><a href="bellevue-university.php">Bellevue University </a></li>
                           
                           <li><a href="capella-university.php">Capella University</a></li>
                           
                           <li>
                              <a href="charter-oak-state-college.php">Charter Oak State College</a> 
                           </li>
                           
                           <li>
                              <a href="columbia-college.php">Columbia College</a> 
                           </li>
                           
                           <li><a href="devry-university.php">DeVry University</a></li>
                           
                           
                           <li><a href="embry-riddle-university.php">Embry-Riddle University</a></li>
                           
                           <li><a href="embry-riddle-university-blue-gold-connection.php">Embry-Riddle University Blue-Gold Connection </a></li>
                           
                           
                           <li><a href="florida-institute-of-technology.php">Florida Institute of Technology (Logistics Management)</a></li>
                           
                           
                           <li><a href="heidelberg-college.php">Heidelberg College</a></li>
                           
                           <li><a href="heidelberg-college-honors.php">Heidelberg College (Honors)</a></li>
                           
                           <li><a href="hillsborough-community-college.php">Hillsborough Community College</a></li>
                           
                           <li><a href="hillsborough-community-college-respiratory-care.php">Hillsborough Community College (Respiratory Care)</a></li>
                           
                           <li><a href="johnson-wales-university.php">Johnson and Wales University</a></li>
                           
                           <li>
                              <a href="kaplan-university.php">Kaplan University</a> 
                           </li>
                           
                           <li><a href="montreat-college.php">Montreat College </a></li>
                           
                           <li><a href="mountain-state-university-nursing.php">Mountain State  University (Nursing)</a></li>
                           
                           <li><a href="national-university.php">National University (Online)</a></li>
                           
                           <li><a href="polytechnic-university.php">Polytechnic University</a></li>
                           
                           <li><a href="regent-university.php">Regent University</a></li>
                           
                           <li><a href="ringling-college-of-art-and-design.php">Ringling College of Art and Design</a></li>
                           
                           <li><a href="rollins-college.php">Rollins College</a></li>
                           
                           <li><a href="sistema-university.php">Sistema Universitario Ana G Mendez-Florida</a></li>
                           
                           <li><a href="strayer-university.php">Strayer University</a></li>
                           
                           <li><a href="temple-university.php">Temple University (Gen Ed-to-Gen Ed Agreement)</a></li>
                           
                           <li><a href="university-of-central-florida-computer-science-asociates-to-bachelors-program.php">University of Central Florida (Central Florida's A.A. to B.S. Computer Science Articulated
                                 Program)</a></li>
                           
                           <li><a href="university-of-central-florida-honors-to-honors.php">University of Central Florida (Honors to Honors Articulation Agreement)</a></li>
                           
                           <li>
                              <a href="university-of-central-florida-information-technology-transfer-plan.php"></a>University of Central Florida (Articulated A.A. Transfer Plan in Information Technology)
                           </li>
                           
                           <li>
                              <a href="university-of-maryland-university-college.php">University of Maryland University College (UMUC)</a> 
                           </li>
                           
                           <li><a href="university-of-miami.php">University of Miami</a></li>
                           
                           <li><a href="university-of-phoenix.php">University of Phoenix</a></li>
                           
                           <li><a href="western-kentucky-university.php">Western Kentucky University (Online)</a></li>
                           
                           <li>
                              <a href="western-kentucky-univ.pdf">Temple University, Japan (TUJ) Campus Affiliation Agreement</a> - is based on Valencia College's and TUJ commitment to promoting international education,
                              providing their students with access to attractive educational options that specifically
                              aims to facilitate Valencia students applying to TUJ.
                              
                           </li>
                           
                           <li>
                              <i class="icon_star"></i> <a href="hillsborough-community-college.php" align="left">Hillsborough Community College - A.S. to B.S. Radiologic and Imaging Sciences</a>
                              
                           </li>
                           
                           <li>
                              <i class="icon_star"></i> <a href="hillsborough-community-college-respiratory-care.php">Hillsborough Community College - A.S. Respiratory Care to B.S Cardiopulmonary Sciences
                                 </a>
                              
                           </li>
                           
                           
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/college-list.pcf">©</a>
      </div>
   </body>
</html>