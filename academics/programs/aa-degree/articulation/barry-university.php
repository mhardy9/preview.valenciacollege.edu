<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Barry University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="barry university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/barry-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Barry University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Barry University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What  Valencia degree is articulated with Barry University?</h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.html">A.A. Degree</a> and <a href="/academics/programs/">A.S. Degree</a> 
                                 </p>
                                 
                                 
                                 
                                 <h4>What degree will be earned at Barry University?</h4>
                                 
                                 <p>B.S. Degree</p>
                                 
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>Please check with Barry University's Office of Admissions </p>
                                 
                                 
                                 
                                 <h4>How many credit hours will be accepted? </h4>
                                 
                                 <p>For students transferring who do not have an A.A. Degree from Valencia, up to 64 credits
                                    in college level coursework numbered 1000 and above, with a grade of C or better.
                                    
                                 </p>
                                 
                                 <p>An additional 26 credits, for a total of 90 credits, may come from a combination of
                                    portfolio, testing, non-traditional credit, and an acquired degree. 
                                 </p>
                                 
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>Students must have a minimum of 48 upper-division credits to meet graduation requirements.
                                    
                                 </p>
                                 
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance? </h4>
                                 
                                 <p>None noted in Agreement </p>
                                 
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>Valencia graduates are offered a 20% discount on tuition for full-time ACE students
                                    (please refer to <a href="documents/BarryUnivArticulationfinalsignedJan2017.pdf">Agreement</a> for definition of full-time) 
                                 </p>
                                 
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>January 2017</p>
                                 
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Please refer to <a href="documents/BarryUnivArticulationfinalsignedJan2017.pdf">Barry Agreement</a> for complete details.
                                 </p>
                                 
                                 
                                 
                                 <h4>Who can be contacted for information at Barry University? </h4>
                                 
                                 <p> Michelle Czechowski <br>
                                    Recruiter /Administrative Support Specialist
                                    <br>
                                    <a href="mailto:MCzechowski@barry.edu">MCzechowski@barry.edu</a><br>
                                    Office: 407-438-4150<br>
                                    Cel: 407-271-5544<br>
                                    OR<br>
                                    Liz Francisco <br>
                                    <a href="mailto:alhernandez@barry.edu">efrancisco@barry.edu</a><br>
                                    (305) 899-4830<br>
                                    OR<br>
                                    <a href="mailto:aceorlando@barry.edu"></a>Rachel Sangiovanni<br>
                                    <a href="mailto:rsangiovanni@barry.edu">rsangiovanni@barry.edu</a><br>
                                    305-899-3302<br>
                                    OR <br>
                                    Pres. Linda Bevilacqua<br>
                                    <a href="mailto:LBevilacqua@mail.barry.edu">LBevilacqua@mail.barry.edu</a><br>
                                    OR<br>
                                    Cynthia Chruszczyk<br>
                                    <a href="mailto:cchruszczyk@barry.edu%20">cchruszczyk@barry.edu</a><br>
                                    OR<br>
                                    <a href="mailto:aceorlando@barry.edu">
                                       aceorlando@barry.edu</a>
                                    
                                 </p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><a href="http://www.barry.edu/ed/default.htm">Barry Home Page</a><br>
                                    <a href="/documents/academics/programs/degree-options/associate-in-arts/BarryUnivArticulationfinalsignedJan2017.pdf">Barry Agreement</a><br>
                                    <a href="/documents/academics/programs/degree-options/associate-in-arts/BarryUnivPartnerPubValenciaupdatedJan2017.pdf">VALENCIA ALUMNI 20% tUITION DISCOUNT INFO</a>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/barry-university.pcf">©</a>
      </div>
   </body>
</html>