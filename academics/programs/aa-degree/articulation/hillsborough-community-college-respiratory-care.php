<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hillsborough Community College - A.S. Respiratory Care  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="hillsborough community college, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/hillsborough-community-college-respiratory-care.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Hillsborough Community College - A.S. Respiratory Care </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Hillsborough Community College - A.S. Respiratory Care</h3>
                           
                           <p>to the Valencia College B.S. Cardiopulmonary Sciences Degree</p>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What degree will be earned at Valencia College </h4>
                                 
                                 <p><a href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobscardiopulmonarysciences/#programrequirementstext#programrequirementstext">Cardiopulmonary Sciences, B.S. Degree </a> 
                                 </p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>None specified </p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>Transcripts must show completion of an A.S. in Respiratory Care and a minimum of 70
                                    accepted credits including 18 specified credits in General Education.  
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p> Graduation requirements include, but are not limited to the completion of 40 semester
                                    credits of upper division discipline-specific credits, plus the credits needed to
                                    satisfy the General Education requirements.
                                    Twenty-five percent (25%) of course work (32 semester credits) must be completed in
                                    residency. (See <a href="/documents/academics/programs/degree-options/associate-in-arts/HillsboroughCCAttachATransferGuideBSCARDIO.pdf">Attachment A, Transfer Guide</a>) 
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance? </h4>
                                 
                                 <p>A minimum of 70 credits and an Associate's Degree in Respiratory Care, Cardiovascular
                                    Technology or Cardiopulmonary Technology, including 18 credits of pre-requisite course
                                    work, as specified: <a href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobscardiopulmonarysciences/#text">BSCARDIO Overview</a>. The following additional documentation is required: 1) Official copies of transcript
                                    showing degree completion and all course work within the degree; 2) Provide proof
                                    of current professional certification, including one of the following credentials:
                                    RRT, RCIS, RCES, RCS or RCCS. Please refer to complete <a href="/documents/academics/programs/degree-options/associate-in-arts/HillsboroughCCtoVCBSCARDIOfinalsignedagreement8-17-16.pdf">  BSCARDIO articulation agreement</a> for all requirements.
                                 </p>
                                 
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>None specified </p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>August 2016 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>See <a href="/documents/academics/programs/degree-options/associate-in-arts/HillsboroughCCAttachATransferGuideBSCARDIO.pdf">Attachment A, Transfer Guide </a></p>
                                 
                                 
                                 <h4>Who can be contacted for information at Hillsborough Community College? </h4>
                                 
                                 <p>Brian Mann, Ed.D.<br>
                                    Director, Associate in Science Degree Programs Hillsborough Community College<br>
                                    39 Columbia Drive<br>
                                    Tampa, FL 33606<br>
                                    (813) 253-7022<br>
                                    <a href="mailto:bmann@hccfl.edu%3cmailto:bmann@hccfl.edu">bmann@hccfl.edu</a></p>
                                 
                                 
                                 
                                 <h4>Who can be contacted for information at Valencia College? </h4>
                                 
                                 <p>Ms. Julia Ribley<br>
                                    Allied Health Care Advisor,<br>
                                    Adv. Technical Certificates and Bachelor Degrees<br>
                                    (407) 582-1898<br>
                                    <a href="mailto:jribley@valenciacollege.edu">jribley@valenciacollege.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.hccfl.edu" target="_blank">Hillsborough Community College Home Page</a></strong><br>
                                    <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/HillsboroughCCtoVCBSCARDIOfinalsignedagreement8-17-16.pdf">Hillsborough Community College Agreement</a></strong> <br>
                                    <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/HillsboroughCCAttachATransferGuideBSCARDIO.pdf">Attachment A-Transfer Guide</a></strong><br>
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/hillsborough-community-college-respiratory-care.pcf">©</a>
      </div>
   </body>
</html>