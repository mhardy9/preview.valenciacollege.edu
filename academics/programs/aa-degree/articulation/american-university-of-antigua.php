<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>American University of Antigua  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/american-university-of-antigua.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>American University of Antigua </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>American University of Antigua</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with American University of Antigua? </h4>
                                 
                                 <p>AUA will offer admission to its educational program leading to the Doctor of Medicine
                                    degree to graduates of Valencia College who have met the following requirements: complete
                                    an AA at Valencia College plus 30 additional credits which include the pre-requisite
                                    courses as noted in <a href="documents/AmUnivofAntiguafinalsignedagreement7-28-16.pdf">the agreement</a>. 
                                 </p>
                                 
                                 
                                 
                                 <h4>What degree will be earned at American University of Antigua?</h4>
                                 
                                 <p>Students will earn a U.S. medical degree from AUA upon completion of the program.</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>There is no deadline for applying as we are rolling admissions. Students can apply
                                    once they have met the requirements for Basic Sciences, which is having at least 90
                                    college credits completed including all of the science pre-requisites.&amp;gt;
                                    
                                    
                                 </p>
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>AUA will award a Bachelor’s of Human Health Sciences Degree after the student successfully
                                    completes year 2 of the MD program. The student must maintain a 2.00 grade point average
                                    in all coursework at AUA.
                                    
                                    
                                 </p>
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p><a href="https://www.auamed.org/admissions/tuition-and-financial-services/#scholarship-opportunities">https://www.auamed.org/admissions/tuition-and-financial-services/#scholarship-opportunities<br>
                                       </a>AUA offers qualified students a variety of scholarship opportunities. Please click
                                    link above.
                                 </p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>July  2016 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Please refer to <a href="/documents/academics/programs/degree-options/associate-in-arts/AmUnivofAntiguafinalsignedagreement7-28-16.pdf">American University of Antigua agreement</a> 
                                 </p>
                                 
                                 
                                 <h4>Who can be contacted for information at American University of Antigua? </h4>
                                 
                                 <p><strong>Brianna Galmiche, MS<br>
                                       </strong><em>Senior Associate Director of Admissions<br>
                                       </em>P: (917) 940-4007<br>
                                    <a href="mailto:bgalmiche@auamed.org">bgalmiche@auamed.org</a> 
                                 </p>
                                 
                                 <p>Manipal Education Americas, LLC representative for<br>
                                    <a href="http://www.auamed.org/"><strong>American University of Antigua College of Medicine</strong></a> 
                                 </p>
                                 
                                 <p><a href="http://www.aicasa.org/">American International College of Arts and Sciences</a><br>
                                    <u>Antigua</u> <br>
                                    <u>1 Battery Park Plaza, 33rd floor</u><br>
                                    <u>New York, NY 10004</u> 
                                 </p>
                                 
                                 <p><u><a href="http://www.auamed.org">www.auamed.org</a></u> 
                                 </p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="/documents/academics/programs/degree-options/associate-in-arts/AmUnivofAntiguafinalsignedagreement7-28-16.pdf">American University of Antigua Agreement </a></strong><br>
                                    <strong><a href="http://go.auamed.org/aboutaua?gclid=CNy26Y63pc4CFdVZhgodsr8Nsg">American University of Antigua Home Page</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/american-university-of-antigua.pcf">©</a>
      </div>
   </body>
</html>