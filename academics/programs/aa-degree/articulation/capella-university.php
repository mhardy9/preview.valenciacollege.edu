<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Capella University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="capella university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/capella-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Capella University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Capella University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Capella University? </h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/index.html">A.A. Degree</a><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.html"></a> (minimum grade of C in course work)<br>
                                    <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degree</a>  <a href="http://valenciacollege.edu/asdegrees/default.cfm"></a> (will evaluate and transfer courses that meet Capella's degree requirements on a
                                    course-by-course basis. General Education core will be taken as a block.) 
                                 </p>
                                 
                                 
                                 <h4>What degree will be earned at Capella University?</h4>
                                 
                                 <p>B.A. Degree or B.S. Degree </p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>None specified </p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>All college-level courses meeting Capella University's transfer requirements will
                                    be evaluated for transfer into its Baccalaureate degree programs up to a maximum of
                                    88 semester hours of academic credit.
                                    
                                    
                                 </p>
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>Not specified. However, learners must complete a minimum of 50% of their core and
                                    specialization courses at Capella University.
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>None specified</p>
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>None listed. A 10% tuition reduction will be made available to all eligible Valencia
                                    faculty, staff, and graduates on all Capella courses, excluding fees. This will begin
                                    as of the date of the Agreement and is not retroactive.
                                 </p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>January 2016 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Please refer to <a href="documents/Capella-VCArticulationAgreementfinalsigned2015-16.pdf">Capella  Agreement </a></p>
                                 
                                 
                                 <h4>Who can be contacted for information at Capella University? </h4>
                                 
                                 <p><strong>John Rydberg</strong> <br>
                                    <strong>Enrollment Services</strong> <br>
                                    <strong>Capella Education Company</strong> <br>
                                    Capella Tower<br>
                                    225 S. 6th St.<br>
                                    Minneapolis, MN 55402<br>
                                    Direct: 1-888-227-9896<br>
                                    Fax: 612- 977-5060
                                 </p>
                                 
                                 <p>E-mail: <a href="mailto:jon.rydberg@capella.edu">jon.rydberg@capella.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.capella.edu">Capella Home Page</a>
                                       <a href="http://www.capella.edu/partners/Default.aspx?partnerid=F0oR4KFJrsgpa8OzKUFb+w==&amp;pid=2911&amp;linkID=23940&amp;Refr="><br>
                                          Capella Welcome Page for Valencia </a>
                                       <a href="http://www.capella.edu/partner/college/valencia.asp?linkID=23940&amp;Refr="></a><br>
                                       <a href="documents/Capella-VCArticulationAgreementfinalsigned2015-16.pdf">Capella Agreement</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/capella-university.pcf">©</a>
      </div>
   </body>
</html>