<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>National University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="national university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/national-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>National University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>National University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with National University? </h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.html">A.A. Degree</a> or <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degree</a><br>
                                    National will not give credit for any course work with a grade less than a "C"
                                 </p>
                                 
                                 
                                 <h4>What degree will be earned at National University?</h4>
                                 
                                 <p><a href="/documents/academics/programs/degree-options/associate-in-arts/NationalUniv2015-2016VCGenEdcourse-programareas.doc">Bachelor Degree Program areas</a> 
                                 </p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>N/A</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>All general education courses for the Valencia degree and the student's entire degree
                                    will be accepted towards the general education component and/or general elective component
                                    of National's bachelor degree program. Other general education requirements will be
                                    identified and evaluated in advance.
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>Students are required to complete a program of study offered online that will vary,
                                    depending on the program of study and degree being sought. The total program will
                                    satisfy all requirements for the degrees at Valencia and at National University.
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>None noted</p>
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p>Yes, the following scholarships are offered; details are on page 3 of the <a href="/documents/academics/programs/degree-options/associate-in-arts/NationalUniv-VCagreementfinalsigned5-11-16.pdf">Agreement</a>:
                                 </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Promising Scholar</li>
                                    
                                    <li>Transfer to Triumph</li>
                                    
                                 </ul>
                                 
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>May 2016 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Valencia graduates will be given first priority for admissions to courses and programs
                                    conducted as part of the Agreement;
                                 </p>
                                 
                                 <p>The $60.00 application fee is waived for all Valencia graduates </p>
                                 
                                 
                                 <h4>Who can be contacted for information at National University? </h4>
                                 
                                 <p><strong>Jorge Salas</strong> <br>
                                    Supervisor of Articulation<br>
                                    Services<br>
                                    National University - Scripps Ranch <br>
                                    9980 Carroll Canyon Road<br>
                                    San Diego, CA 92131<br>
                                    858-642-8248 <br>
                                    or 1-858-642-8260<br>
                                    <a href="mailto:jsalas@nu.edu">jsalas@nu.edu </a><br>
                                    or <br>
                                    <a href="http://www.nu.edu">www.nu.edu</a><br>
                                    or<br>
                                    <a href="mailto:registrar@nu.edu">registrar@nu.edu</a></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.nu.edu/" target="_blank">National University Home Page</a></strong><br>
                                    <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/NationalUniv-VCagreementfinalsigned5-11-16.pdf">National University Agreement</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/national-university.pcf">©</a>
      </div>
   </body>
</html>