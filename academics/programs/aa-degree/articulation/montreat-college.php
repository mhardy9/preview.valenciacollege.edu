<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Montreat College  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="montreat college, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/montreat-college.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Montreat College </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Montreat College</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Montreat College? </h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.html">A.A. Degree</a> or <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degree</a></p>
                                 
                                 
                                 <h4>What degree will be earned at Montreat College?</h4>
                                 
                                 <p>B.A. Degree or B.S. Degree</p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>None specified</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>At least  60, with an overall GPA of 2.0 or higher</p>
                                 
                                 
                                 <h4>How many credit hours will be accepted? </h4>
                                 
                                 <p>Complete a program of study equaling a minimum of 126 credit hours required of the
                                    degree being sought 
                                 </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance?</h4>
                                 
                                 <p>Depends upon program chosen; check with Montreat</p>
                                 
                                 
                                 <h4>Are there any scholarships available?</h4>
                                 
                                 <p>Academic scholarships* for transfer students are available. Transfer scholarships
                                    range from $4,000 – 8,000, depending on college GPA. Students must have a minimum
                                    GPA of 2.5 to be eligible. 
                                 </p>
                                 
                                 <p>*Note that student-athletes are packaged differently, so the above range does not
                                    necessarily apply to all students. 
                                 </p>Contact Montreat College for additional information.
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>September 2015 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Please refer to Montreat Agreement, <em><a href="/documents/academics/programs/degree-options/associate-in-arts/Montreatprogramaddendumguide9-22-15.pdf">Program Addendum Guide</a> </em></p>
                                 
                                 
                                 
                                 <h4>Is there a Video Link and special program information?</h4>
                                 
                                 <p>View a Montreat Cybersecurity Video Link with President Maurer <a href="https://vimeo.com/152312038">https://vimeo.com/152312038</a></p>
                                 
                                 
                                 <h4>Who can be contacted for information at Montreat College? </h4>
                                 <strong>Kristin Janes,</strong> <br>
                                 VP for Enrollment Management<br>
                                 <a href="mailto:schamberlain@montreat.edu%20">kjanes@montreat.edu</a><br>
                                 310 Gaither Circle<br>
                                 Montreat, NC 28757<br>
                                 800-622-6968 <br>
                                 OR<br>
                                 <strong>Nicole M. Millard</strong><br>
                                 Special Assistant to the President, Florida<br>
                                 <a href="mailto:nicole.millard@Montreat.edu">nicole.millard@Montreat.edu</a><br>
                                 P.O. Box 1267 |Montreat, NC 28757<br>
                                 407-739-4033<br>
                                 Main Campus: 828-669-8012
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 <p><strong><a href="http://www.montreat.edu">Montreat Home Page</a></strong><br>
                                    <strong><a href="/documents/academics/programs/degree-options/associate-in-arts/Montreat-ValenciaArticAgreementsigned9-22-15.pdf">Montreat Agreement</a><br>
                                       <strong><em><a href="/documents/academics/programs/degree-options/associate-in-arts/Montreatprogramaddendumguide9-22-15.pdf">Program Addendum Guide</a> </em><br></strong></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/montreat-college.pcf">©</a>
      </div>
   </body>
</html>