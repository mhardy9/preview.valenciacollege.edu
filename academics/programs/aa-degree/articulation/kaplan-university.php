<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Kaplan University  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="johnson and wales university, college list, list of universities, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/articulation/kaplan-university.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/aa-degree/">Aa Degree</a></li>
               <li>Kaplan University </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in"> <i class="pe-7s-news-paper"></i>
                           
                           <h3>Kaplan University</h3>
                           
                           <p>What the Student Needs to Know</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 
                                 <h4>What Valencia degree is articulated with Kaplan University? </h4>
                                 
                                 <p><a href="/academics/programs/degree-options/associate-in-arts/pre-majors-list.html">A.A. Degree</a> and <a href="http://valenciacollege.edu/asdegrees/default.cfm">A.S. Degree</a></p>
                                 
                                 
                                 <h4>What degree will be earned at Kaplan University?</h4>
                                 
                                 <p>Bachelor's Degree </p>
                                 
                                 
                                 <h4>What is the deadline for application?</h4>
                                 
                                 <p>None specified </p>
                                 
                                 
                                 <h4>How many credit hours will be accepted?</h4>
                                 
                                 <p>Completion of A.A. or A.S. degree programs, with a minimum of 60 semester hours, will
                                    be eligible for a block transfer of up to those 60 semester hours and acceptance into
                                    the "Advanced Start" baccalaureate option 
                                 </p>
                                 
                                 
                                 <h4>How many credit hours until a Bachelors Degree will be awarded?</h4>
                                 
                                 <p>Not specified in Agreement; please refer to various Academic Worksheets </p>
                                 
                                 
                                 <h4>Are there mandatory courses needed for acceptance? </h4>
                                 
                                 <p>No; however, Valencia students who are unable to fulfill Kaplan University prerequisites
                                    at Valencia College must complete them at Kaplan University. 
                                 </p>
                                 
                                 
                                 <h4>Are there any scholarships available? </h4>
                                 
                                 <p><em>Please refer to the <a href="/documents/academics/programs/degree-options/associate-in-arts/Kaplan-VCArticulationAgreementfinalsigned2-9-15.pdf">Agreement </a>for specific information</em></p>
                                 
                                 
                                 <h4>What date was the agreement signed? </h4>
                                 
                                 <p>February 9, 2015 </p>
                                 
                                 
                                 <h4>Is there any special information? </h4>
                                 
                                 <p>Valencia students must fulfill Kaplan University requirements, and complete no less
                                    than 25% of their program requirements at Kaplan including the capstone course, at
                                    Kaplan. Credit earned through any combination of Transfer Credit, Challenge Credit,
                                    or Experiential Credit will not exceed 75% of total credits required for graduation.
                                 </p>
                                 
                                 <p><em>Please refer to the <a href="/documents/academics/programs/degree-options/associate-in-arts/Kaplan-VCArticulationAgreementfinalsigned2-9-15.pdf">Agreement</a> for additional requirements </em></p>
                                 
                                 
                                 <h4>Who can be contacted for information at Kaplan University? </h4>
                                 
                                 <p>Erin Carr-Jordan, Ph.D.<br>
                                    Associate Dean<br>
                                    College of Social &amp; Behavioral Sciences
                                    <br>
                                    Kaplan University<br>
                                    866-397-9456<br>
                                    <a href="mailto:ecarrjordan@kaplan.edu%20">ecarrjordan@kaplan.edu</a><br>
                                    or/and<br>
                                    <strong>Michael Lorenz<br>
                                       University Registrar<br>
                                       550 West Van Buren Street<br>
                                       Chicago, IL 60607<br>
                                       Tel: <a href="tel:312.777.6480">312.777.6480</a><br>
                                       Cell: <a href="tel:312-771-8781">312-771-8781<br>
                                          </a><a href="mailto:mlorenz@kaplan.edu">mlorenz@kaplan.edu</a></strong></p>
                                 
                                 
                                 <h4>Links</h4>
                                 
                                 
                                 <p></p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li><a href="http://go.kaplanuniversity.edu/?adpos=1t1&amp;creative=110157103226&amp;device=c&amp;matchtype=e&amp;network=g&amp;source=SF08907&amp;ve=61599&amp;utm_source=Google&amp;utm_medium=CPC&amp;utm_campaign=kuo_lg_mer_BR_KW_EXT&amp;utm_term=85529680-VQ2-g-VQ6-110157103226-VQ16-c&amp;gclid=CJHGzae_380CFVJahgod1SQI4g">Kaplan Home Page</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/Kaplan-VCArticulationAgreementfinalsigned2-9-15.pdf">Kaplan Agreement</a></li>
                                    
                                    <li>
                                       <a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSAcct.pdf">Kaplan Academic Worksheets</a> 
                                    </li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/AdvStartBSBusiness.pdf">B.S. in Accounting</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSBusAdmin.pdf">B.S. in Business Admin</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSCommunications.pdf">B.S. in Communication</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSCJ.pdf">B.S. in Criminal Justice</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/.pdf">B.S. in CJ-Homeland Security</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSEarlyChildhoodAdmin.pdf">B.S. in Early Childhood Admin</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSHealthCareAdmin.pdf">B.S. in Health Care Admin</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSHealthScience.pdf">B.S. in Health Science</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSHealth-Wellness.pdf">B.S. in Health- Wellness</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSInfoTechnology.pdf">B.S. in Information Tech</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/AAinPR-OrgCommtoBSinCommunication-ValenciaCollege.pdf">B.S. in Communication</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/KaplanBSPsychinAppliedBehavAnalysis.pdf">B.S. in Psych - Applied Behavior Analysis</a></li>
                                    
                                    <li><a href="/documents/academics/programs/degree-options/associate-in-arts/Kaplan3-1programagreements.pdf">3+1 Programs</a></li>
                                    
                                    <li>
                                       <a href="http://www.kaplanuniversity.edu/transfer-credits/community-college-students.aspx?wid=cc&amp;source=SF01745&amp;ve=60956" target="_blank">Kaplan Community College Home Page</a> or call 1-866-397-9456 
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner"> <i class="icon-book-1"></i>
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <p></p>
                        <a href="http://valenciacollege.edu/academic-affairs/curriculum-assessment/" class="banner_bt">Click here</a> 
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">APPLY NOW</a>
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Karen Marie Borglum</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Karen Marie Borglum<br>
                           Asst VP, Curriculum Dev &amp; Art, <br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <h5>Krissy Brissett</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        <p>Administrative Assistant,<br>
                           Curriculum &amp; Articulation
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/articulation/kaplan-university.pcf">©</a>
      </div>
   </body>
</html>