<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Associate in Arts Degree  | Valencia College</title>
      <meta name="Description" content="The general education program at Valencia is designed to contribute to the student's educational growth by providing a basic liberal arts education and it is an integral part of the A.A. Degree program.">
      <meta name="Keywords" content="aa degrees, associate in arts, associate in arts degrees, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/aa-degree/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/aa-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Aa Degree</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Associate in Arts Degree</h2>
                        
                        <p>University Parallel Programs</p>
                        
                        
                        <p>Valencia's Pre-Majors are designed for a student who plans to earn the Associate
                           in Arts Degree and transfer to one of the eleven state universities
                           in Florida as a junior to complete a bachelor's degree in one of
                           the specific majors. Each Pre-Major includes the courses to satisfy
                           Valencia's General Education requirements for the A.A. Degree and
                           the Statewide Common Course Prerequisites for the specific major
                           (which count as the elective credits for the A.A. Degree). 
                        </p>
                        
                        <p>The Pre-Majors are included in the Associate in Arts Degree section of the catalog,
                           and by clicking
                           on the Pre-Majors and General Studies link below or on the Navigation Bar on the left
                           side of the screen.
                        </p>
                        
                        <p> The Associate in Arts Degree: General Studies is available for students who want
                           a two-year college degree and have
                           not selected a Pre-Major for transfer to a state university in Florida and for students
                           who plan to transfer to a private and/or out-of-state
                           institution. Although the Pre-Majors provide the best preparation
                           for transfer to specific majors in Florida's State University System,
                           a student still may choose the Associate in Arts: General Studies
                           for a variety of reasons.
                           
                        </p>
                        
                        <ul>
                           
                           
                           <li><a href="pre-majors-list.html" class="headSub_1">Pre-Majors &amp; General Studies</a></li>
                           
                           <li><a href="transfer-guarantees.html" class="headSub_1">Transfer Plans</a></li>
                           
                           <li><a href="articulation-agreements.html" class="headSub_1"> Articulated Pre-Majors and Agreements</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Office of Curriculum and Articulation</h3>
                        
                        <ul class="list_staff">
                           <li>
                              <figure><img src="/academics/programs/degree-options/associate-in-arts/images/no-photo-female-thumb.png" alt="" class="img-circle"></figure>
                              <h4>Karen Marie Borglum</h4>
                              <p>Asst VP, Curriculum Dev &amp; Art, 
                                 Curriculum &amp; Articulation
                              </p>
                           </li>
                           <li>
                              <figure><img src="/academics/programs/degree-options/associate-in-arts/images/no-photo-female-thumb.png" alt="" class="img-circle"></figure>
                              <h4>Krissy Brissett</h4>
                              <p>Administrative Assistant,
                                 Curriculum &amp; Articulation
                              </p>
                           </li>
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Need Help?</h3>
                        	
                        <p>For general enrollment questions, assistance is available on a walk-in basis at the
                           <a href="/students/answer-center/index.php">Answer Center</a> or contact or contact Enrollment Services.
                        </p> 
                        				
                        <p> <i class="far fa-phone"></i> <a href="tel:407-582-1507">407-582-1507</a><br>
                           			<i class="far fa-envelope"></i>							 <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                        	
                     </div>
                     
                  </div>
                  
               </div>	
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/aa-degree/index.pcf">©</a>
      </div>
   </body>
</html>