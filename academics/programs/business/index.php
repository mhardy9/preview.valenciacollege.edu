<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business  | Valencia College</title>
      <meta name="Keywords" content="business, degree, as, a.s., associate, science, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/business/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/departments/business/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Business</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="main-title">
                           
                           <h2>Business</h2>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <p>Developed in collaboration with some of the top employers and industry leaders in
                           the area, Valencia’s business programs will immerse you in the latest business theories
                           and practices. From learning to run your own business, to mastering accounting, to
                           providing office support, we’ll give you the foundation you need to enter this diverse
                           field.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                           
                           <h3>Career Coach</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore related careers, salaries and job listings.</p>
                           
                           <p><a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=business&amp;Clusters=4&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" class="button small" target="_blank">Learn More</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h2>Programs &amp; Degrees</h2>
                        
                        <ul>
                           
                        </ul>
                        
                        <h3>Associate in Arts Transfer Plan</h3>
                        
                        <p><a href="http://catalog.valenciacollege.edu/transferplans/" target="_blank">Transfer Plans</a> are designed to prepare students to transfer to a Florida public university as a
                           junior.  The courses listed in the plans are the common prerequisites required for
                           the degree. They are placed within the general education requirements and/or the elective
                           credits.  Specific universities may have additional requirements, so it is best check
                           your transfer institution catalog and meet with a Valencia advisor.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/accounting/">Accounting</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/">Business Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/businessmarketingmanagement/">Business Marketing Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/economics/">Economics</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/healthservicesadministration/">Health Services Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/humanresourcesmanagement/">Human Resources Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/managementinformationsystems/">Management Information Systems</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/publicadministration/">Public Administration</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/publicrelationsorganizationalcommunication/">Public Relations/Organizational Communication</a></li>
                           
                        </ul>
                        
                        <h3>Associate in Science</h3>
                        
                        <p>The A.S. degree prepares you to enter a specialized career field in about two years.
                           It also transfers to the Bachelor of Applied Science program offered at some universities.
                           If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                           at a designated university.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/accounting-technology/">Accounting Technology</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/business-administration/">Business Administration</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/medical-office-administration/">Medical Office Administration</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/office-administration/">Office Administration</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/industrial-management-technology/">Supervision and Management for Industry</a></li>
                           
                        </ul>
                        
                        <h3>Certificate</h3>
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/accountingtechnology/#certificatestext">Accounting Applications</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/accountingtechnology/#certificatestext">Accounting Operations</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/accountingtechnology/#certificatestext">Accounting Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Business Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Business Operations</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Business Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Customer Service Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Customer Service Operations</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Customer Service Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Entrepreneurship</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Human Resources Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Human Resources Operations</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Human Resources Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">International Business Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Medical Office Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Medical Office Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/medicalofficeadministration/#certificatestext">Medical Office Support </a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Management</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/officeadministration/#certificatestext">Office Support</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Operations Support and Services</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#certificatestext">Real Estate Specialist</a></li>
                           
                        </ul>
                        
                        <h3>Continuing Education</h3>
                        
                        <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                           development, language courses, certifications and custom course development for organizations.
                           Day, evening, weekend and online learning opportunities are available.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-action home">Apply Now</a>
                           
                           <a href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm" class="button-action_outline home">Request Information</a>
                           
                           <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" class="button-action_outline home split">Visit Valencia</a>
                           
                           <a href="https://vcchat.valenciacollege.edu/CCPROChat/CPChatRequest.jsp?form.ServiceID=29&amp;form.CustName=WebCaller&amp;form.SEcureChat=1" class="button-action_outline home split" target="foo" onsubmit="window.open('', 'foo', 'width=450,height=500,status=yes,resizable=yes,scrollbars=yes,location=no')">Chat With Us</a>
                           
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3>Need Help?</h3>
                           
                           <p>For general enrollment questions, assistance is available on a walk-in basis at the
                              <a href="http://valenciacollege.edu/answer-center/" onclick="__gaTracker('send', 'event', 'outbound-widget', 'http://valenciacollege.edu/answer-center/', 'Answer Center');" title="Answer Center" target="_blank">Answer Center</a> or contact Enrollment Services.<br>
                              <i class="far fa-phone"></i> <a href="tel:14075821507" style="margin-left:10px;">407-582-1507</a><br>
                              <i class="far fa-envelope"></i>&nbsp;<a href="mailto:enrollment@valenciacollege.edu?subject=Website%20Inquiry" style="margin-left:10px;">enroll@valenciacollege.edu</a>
                              
                           </p>
                           
                           
                        </div> 
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/business/index.pcf">©</a>
      </div>
   </body>
</html>