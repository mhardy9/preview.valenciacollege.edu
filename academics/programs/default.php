<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Degree and Career Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Valencia Degree and Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Valencia Degree and Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <p>No matter your style, Valencia has a program that fits you.</p>
                        
                        <p>We have several university pre-majors to try on for size, and an A.A. degree that
                           guarantees admission to a Florida state university.
                        </p>
                        
                        <p>We also offer 33 A.S. career programs that give you the option to pursue a bachelor's
                           degree or strut directly into a new career with a job placement rate of 93% – 95%.
                        </p> 
                        
                        <p>Check out our full line of programs.</p>
                        
                        
                        <h3>Degree and Career Programs</h3>
                        
                        <p><strong><a href="../../../aadegrees/index.html">University Parallel Programs</a></strong>  
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="../../../aadegrees/premajors_list.html">A.A.  Degree:&nbsp; General, Pre-Majors, and Transfer Plans</a>
                              
                           </li>
                           
                           <li> <a href="../honors/index.html">Honors Program</a> 
                           </li>
                           
                           <li><a href="../honors/prospective-students/inter_studies.html">Interdisciplinary Studies</a></li>
                           
                        </ul>
                        
                        <p><strong><a href="asdegrees/index.html">Career Programs</a></strong> 
                        </p>
                        
                        <ul>
                           
                           <li><a href="asdegrees/index.html">A.S. Degree &amp; Certificate Programs </a></li>
                           
                           <li><a href="asdegrees/transferagreements.html">A.S. Degree Award of Credit Agreements</a></li>
                           
                           <li><a href="../../../departments/index.html">Academic Departments</a></li>
                           
                           <li><a href="../../../helpas/index.html">Career Program Advisors</a></li>
                           
                           <li><a href="../../../internship/index.html">Internship Opportunities</a></li>
                           
                           <li><a href="../../../epi/index.html">Educator Preparation Institute (EPI) Alternative Certification Program</a></li>
                           
                        </ul>
                        
                        <p><strong><a href="http://net5.valenciacollege.edu/schedule/">Credit Course Search</a></strong></p>
                        
                        <p><strong><a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/">Bachelor Degrees</a></strong></p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/" title="Electrical and Computer Engineering Technology ">Electrical and Computer Engineering Technology </a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/" title="Radiologic and Imaging Sciences">Radiologic and Imaging Sciences</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/" title="Cardiopulmonary Sciences">Cardiopulmonary Sciences</a></li>
                           
                        </ul>
                        
                        
                        
                        <h3>Educational Enhancements</h3>
                        <strong>Current Students</strong>
                        
                        <ul>
                           
                           <li><a href="../../../aadegrees/articulationagreements.html">A.A. Degree  Articulation Agreements</a></li>
                           
                           <li>
                              <a href="asdegrees/transferagreements.html">A.S. Degree Award of Credit Agreements</a> 
                           </li>
                           
                           <li><a href="../CareerCenter/index.html">Career Development Services </a></li>
                           
                           <li><a href="../../../assessments/clep/index.html">College Level Examination (CLEP)</a></li>
                           
                           <li><a href="../english-for-academic-purposes/index.html">English as a Second Language for Academic
                                 Purposes</a></li>
                           
                           <li><a href="../honors/index.html">Honors</a></li>
                           
                           <li><a href="../../../internship/index.html">Internship Program</a></li>
                           
                           <li><a href="../../../linc/index.html">Learning in Community (LinC)</a></li>
                           
                           <li><a href="../../../about/lifemap/index.html">LifeMap</a></li>
                           
                           <li><a href="../../service-learning/index.html">Service Learning</a></li>
                           
                           <li><a href="../../../statway/index.html">Statway</a></li>
                           
                           <li><a href="../studentsuccess/index.html">Student Success</a></li>
                           
                           <li><a href="../../../international/studyabroad/index.html" title="Study Abroad and Global Exchange">Study Abroad and Global Experiences</a></li>
                           
                           
                        </ul>
                        
                        <strong>International Students</strong>
                        
                        <ul>
                           
                           <li><a href="../english-for-academic-purposes/index.html" title="English for Academic Purposes ">English for Academic Purposes </a></li>
                           
                           
                           <li><a href="http://international.valenciacollege.edu//" title="International Student Services">International Student Services</a></li>
                           
                           
                           <li>
                              <a href="../../../international/intensive-english-program/index.html" title="Intensive English Program">Intensive English Program</a> 
                           </li>
                           
                           
                           <li><a href="../../../international/exchange/index.html" title="Study Abroad and Global Exchange">J Exchange Visitor Program</a></li>
                           
                           
                        </ul>
                        
                        
                        <p><strong>High School Students                  
                              </strong>
                           
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../offices-services/bridges-to-success/index.html">Bridges to Success</a></li>
                           
                           <li><a href="../../admissions/dual-enrollment/index.html">Dual and Early Enrollment</a></li>
                           
                           <li><a href="../career-pathways/index.html">Career Pathways</a></li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        <hr>
                        <a href="http://preview.valenciacollege.edu/future-students/degree-options/" title="Choosing a Degree"><img alt="Choosing a Degree" border="0" height="63" src="choosingDegree.png" width="209"></a>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/default.pcf">©</a>
      </div>
   </body>
</html>