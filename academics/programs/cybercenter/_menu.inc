<!-- Site Menu================================================== -->
<ul>
<li class="submenu"><a class="show-submenu" href="/about/"> About Us </a>
<ul class="dropdown-menu-left">
<li>Cybersecurity at Valencia</li>
<li>Industry Partnerships</li>
<li>People</li>
<li>Advisory Council</li>
<li>Contact Us</li>
</ul>
</li>
<li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);"> Cybersecurity Program </a>
<div class="menu-wrapper">
<div class="col-md-6">
<ul class="dropdown-menu-left">
<li>
<div><a href="/continuing-education/"><span>Continuing Education</span></a></div>
</li>
<li>
<div><a href="/students/future-students/admissions/"><span>Domestic Students</span></a></div>
</li>
<li>
<div><a href="/students/future-students/international-students/"><span>International Students</span></a></div>
</li>
<li>
<div><a href="/continuing-education/programs/languages/"><span>Language Programs</span></a></div>
</li>
<li>
<div><a href="/students/offices-services/veterans-affairs/"><span>Military and Veterans</span></a></div>
</li>
<li>
<div><a href="/students/future-students/admissions/parents-of-future-students/"><span>Parents and Families</span></a></div>
</li>
<li>
<div><a href="/students/future-students/admissions/return-to-college/"><span>Return to College</span></a></div>
</li>
<li>
<div><a href="/students/future-students/undocumented-students/"><span>Undocumented Students</span></a></div>
</li>
</ul>
</div>
</div>
<!-- End menu-wrapper --></li>
<li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);"> Outreach </a>
<div class="menu-wrapper c3">
<div class="col-md-6">
<ul>
<li><a href="/students/future-students/programs/general-studies/">General Studies</a></li>
<li><a href="/students/future-students/programs/arts-and-entertainment/">Arts and Entertainment</a></li>
<li><a href="/students/future-students/programs/business/">Business</a></li>
<li><a href="/students/future-students/programs/communications/">Communications</a></li>
<li><a href="/students/future-students/programs/education/">Education</a></li>
<li><a href="/students/future-students/programs/engineering-and-technology/">Engineering and Technology</a></li>
<li><a href="/students/future-students/programs/health-sciences/">Health Sciences and Nursing</a></li>
<li><a href="/students/future-students/programs/hospitality-and-culinary/">Hospitality and Culinary</a></li>
<li><a href="/students/future-students/programs/information-technology/">Information Technology</a></li>
<li><a href="/students/future-students/programs/plant-science-and-agricultural-technology/">Plant Science and Agricultural Technology</a></li>
<li><a href="/students/future-students/programs/public-safety-and-paralegal-studies/">Public Safety and Paralegal Studies</a></li>
<li><a href="/students/future-students/programs/science-and-mathematics/">Science and Mathematics</a></li>
<li><a href="/students/future-students/programs/social-sciences/">Social Sciences<br /></a><hr /><a href="/students/future-students/programs/social-sciences/"><br /></a></li>
</ul>
</div>
<div class="col-md-6">
<ul>
<li>
<div><a href="/students/future-students/degree-options/advanced-technical-certificates/">Advanced Technical Certificates</a></div>
</li>
<li>
<div><a href="/students/offices-services/bridges-to-success/">Bridges to Success</a></div>
</li>
<li>
<div><a href="/students/offices-services/career-pathways/">Career Pathways</a></div>
</li>
<li>
<div><a href="/students/future-students/degree-options/certificates/">Certificate Programs</a></div>
</li>
<li>
<div><a href="/students/future-students/directconnect-to-ucf/">DirectConnect to UCF</a></div>
</li>
<li>
<div><a href="/faculty/education-preparation-institute/">Educator Preparation Institute</a></div>
</li>
<li>
<div><a href="/students/future-students/admissions/honors-program/">Honors Program</a></div>
</li>
<li>
<div><a href="/students/offices-services/career-pathways/">Tech Express to Valencia College</a></div>
</li>
<li>
<div><a href="http://preview.valenciacollege.edu/continuing-education/programs/english-as-a-second-language/">Intensive English Programs</a></div>
</li>
</ul>
</div>
</div>
<!-- End menu-wrapper --></li>
<li class="submenu"><a href="/about/"> Cybersecurity Resources </a>
<ul class="dropdown-menu-left">
<li><a href="/academics/programs/cybercenter/untitled.pcf">DoD Applications</a></li>
<li><a href="/about/about-us/trustees.php">Board of Trustees</a></li>
<li><a href="#">Community Affairs</a></li>
<li><a href="/employees/human-resources/">Employment</a></li>
<li><a href="/about/">Facts</a></li>
<li><a href="/about/about-us/history.php">History</a></li>
<li><a href="/locations/">Locations</a></li>
<li><a href="https://news.valenciacollege.edu/">News and Media</a></li>
<li><a href="/about/">Office of the President</a></li>
<li><a href="/students/offices-services/security/">Safety and Security</a></li>
<li><a href="/about/sustainability/">Sustainability</a></li>
</ul>
</li>
<li class="submenu"><a href="#">Competitions </a>
<div class="menu-wrapper">
<ul class="dropdown-menu-right">
<li><a href="/academics/calendar">Academic Calendar</a></li>
<li><a href="/locations/">Campuses</a></li>
<li><a href="http://preview.valenciacollege.edu/students/future-students/career-coach/">Career Coach / Career Center</a></li>
<li><a href="/academics/courses/">Courses and Schedules</a></li>
<li><a href="/students/offices-services/locations-store/">Campus Stores (Bookstores)</a></li>
<li><a href="https://events.valenciacollege.edu">Events Calendar</a></li>
<li><a href="/students/offices-services/library/">Libraries</a></li>
<li><a href="#">Schools and Colleges</a></li>
</ul>
</div>
</li>
<li class="submenu"><a href="#">Institutional Resources </a>
<div class="menu-wrapper">
<ul class="dropdown-menu-right">
<li><a href="/academics/calendar">Academic Calendar</a></li>
<li><a href="/locations/">Campuses</a></li>
<li><a href="http://preview.valenciacollege.edu/students/future-students/career-coach/">Career Coach / Career Center</a></li>
<li><a href="/academics/courses/">Courses and Schedules</a></li>
<li><a href="/students/offices-services/locations-store/">Campus Stores (Bookstores)</a></li>
<li><a href="https://events.valenciacollege.edu">Events Calendar</a></li>
<li><a href="/students/offices-services/library/">Libraries</a></li>
<li><a href="#">Schools and Colleges</a></li>
</ul>
</div>
</li>
</ul>
<!-- End Site Menu Sub-nav  -->