<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Information Technology  | Valencia College</title>
      <meta name="Description" content="Health Information Technology | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/health-information-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/health-information-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Health Information Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Health Information Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Health Information Technology</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p> The Health Information Technology Associate in Science (A.S.) degree at Valencia
                        College is a two-year program
                        with online courses that prepares you to go directly into a specialized career in
                        medical records. 
                     </p>
                     
                     <p> Health information professionals play a critical role in health care—even though
                        they rarely interact with
                        patients. Every time you hear a health statistic on the news, read a doctor’s diagnosis
                        in your medical record
                        or use your health insurance card, you can be sure a medical record technician has
                        played a part. Health
                        information technicians are specialists in preparing, analyzing and managing medical
                        records. They play a key
                        role in coding and reimbursement and are considered experts in assuring the privacy
                        and security of health data.
                        In Valencia’s HIT program, students learn how to collect, analyze, monitor and report
                        health data, using the
                        latest technology. As more doctors and hospitals move toward electronic health records,
                        trained technicians in
                        this field will become more in demand than ever. 
                     </p>
                     
                  </div>
                  
               </div>
               
               <div class="container margin-30">
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/technology/office-of-information-technology/learning-technology-services/student-resources/"> <i class="far fa-laptop"></i>
                           
                           <h3>Convenient Classes</h3>
                           
                           <p>This program is offered completely online, so students can take classes any time,
                              any where<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/health-information-technology/course-guide.html"> <i class="far fa-calendar"></i>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 5 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/asdegrees/credit_alternative.html"> <i class="far fa-share-alt"></i>
                           
                           <h3>Alternative Credit</h3>
                           
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? You may receive
                              college credit for approved Industry Certifications.
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-dollar-sign"></i>
                           
                           <h3>Average Salary &amp; Placement</h3>
                           
                           <p>Valencia’s A.S. Degree and Certificate programs placement rate ranges between 90%
                              – 95%. Health Info Tech
                              average salaries ranges from $32,000 - $38,000<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-list-alt"></i>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college
                              catalog.<br>
                              <br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.php"> <i class="far fa-question-circle"></i>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Attend a Health Sciences Information Sessions for an overview of the limited access
                              Health Science programs
                              at Valencia College and the procedures for enrolling in them. 
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Health Information Technology (A.S.)</h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="program-guide.php">Current Program Guide</a></li>
                              
                              <li><a href="estimated-costs.php">Estimated Program Costs</a></li>
                              
                              <li>
                                 <a href="course-guide.php">Course Planning Guide </a> 
                              </li>
                              
                              <li><a href="/academics/programs/departments/health-sciences/health-information-technology/index.php#FAQ">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li>
                                 <strong>Application Deadline:</strong> June 15, 2018
                              </li>
                              
                              <li>
                                 <strong>Begins:</strong> Fall 2018
                              </li>
                              
                              <li>
                                 <strong>Application:</strong> Will be available March 2018
                              </li>
                              
                           </ul>
                           <br>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <tr>
                              
                              <th scope="col">Date Updated</th>
                              
                              <th scope="col">Effective Date</th>
                              
                              <th scope="col">Change</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>10/20/14</td>
                              
                              <td>For Fall 2015</td>
                              
                              <td>Starting fall 2015, program is now fully online.</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>11/8/16</td>
                              
                              <td>For Spring 2017</td>
                              
                              <td>Starting spring 2017, a Technical Certificate in Medical Coder/Biller is being offered.
                                 You can find the
                                 Intent to Register form in the "Apps &amp; Deadlines" section of this website. 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/6/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>
                                 <ul>
                                    
                                    <li>The overall GPA Admission Points will not be in consideration for the total admission</li>
                                    
                                    <li>Points will no longer be awarded for Overall GPA</li>
                                    
                                    <li>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1. Note: SLS 1122
                                       points will remain
                                       the same--A: 3, B:2, C: 1 
                                    </li>
                                    
                                 </ul>
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/20/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>International Students are eligible to apply for this Health Sciences Program. <a href="../International-Student-Eligibility.cfm">Click here for details.</a>
                                 
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              			
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              
                              <p>Dean of Allied Health</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                              
                              <h4>Kelli Lewis</h4>
                              
                              <p>Chair of Health Information Technology</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/_resources/img/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="No Photo Available" class="img-circle"></figure>
                              
                              <h4>Health Sciences Advising</h4>
                              
                              <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>Steps for Admission to Program</h4>
                           
                           <ul class="list_order">
                              
                              <li>
                                 <span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 are not local and cannot attend the information session, review the <a href="http://valenciacollege.edu/west/health/documents/HealthInformationTechnology.pdf">program guide</a> and read the <a href="http://valenciacollege.edu/west/health/documents/FAQ-Health-Health-Info-Tech.pdf">Frequently Asked
                                    Questions</a>. 
                              </li>
                              
                              <li>
                                 <span>2</span><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply</a> to Valencia College. 
                              </li>
                              
                              <li>
                                 <span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              
                              <li>
                                 <span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 requirements, you can apply to the program. 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               			
               <div class="container_gray_bg">
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        		
                        <p>Valencia College accepted its first class of Health Information Technology students
                           in January, 2014. The health information technology occupation is one of the fastest
                           growing professions in one of the fastest growing industries. Health information professionals
                           provide reliable and valid information that drives health care. Health information
                           technicians are specialists in managing medical records, coding and reimbursement,
                           and possess the skills to critically think and problem solve. 
                        </p>
                        
                        
                        <p>Health information technology professionals play a key role in preparing, analyzing
                           and maintaining health records and are considered experts in assuring the privacy
                           and security of health data. The health information field is increasingly focusing
                           on electronic patient records, database management, privacy and security. Health information
                           technology professionals work in a variety of settings, such as hospitals, physician
                           practices, long term care, home health care, and insurance companies.
                        </p>
                        
                        <p>The program is a limited access, two year Associate in Science degree program.</p>
                        
                        <p>The Associates Degree Health Information Technology Program is in Candidacy Status,
                           pending accreditation review by the Commission on Accreditation for Health Informatics
                           and Information Management Education (CAHIIM).
                        </p>
                        
                        			  
                        <p>The 2017 graduation rate for the Health Information Technology Program is 83%.</p>
                        	  
                     </div>
                     				
                  </div>
                  		
               </div>
               		
               		
               <a name="FAQ" id="FAQ"></a>
               
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Frequently Asked Questions</h2>
                     
                     <p>A.S. Health Information Technology</p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           
                           <p>Yes. You must complete the admission process to the college before you submit a Health
                              Information Technology
                              program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              questions. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can International students apply?</h3>
                           
                           <p>Yes. Students must be a U.S. Citizen, a U.S. permanent resident, or hold a non-immigrant
                              visa with employment
                              authorization. Non-immigrant status students can visit the <a href="https://travel.state.gov/content/visas/en/visit/visitor.html">U.S. Department of State directory of
                                 visa categories</a> to verify employment eligibility. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What courses do I need to take prior to applying?</h3>
                           
                           <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                              Te pri facete latine
                              salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                              posse exerci
                              volutpat has in.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Are the prerequisite courses offered online?</h3>
                           
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available
                              every term.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I have to take all prerequisites at a particular campus?</h3>
                           
                           <p>No, you may take the prerequisite courses at any Valencia campus.</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous
                              institution. The admissions office will evaluate all transcripts once they are received
                              to determine transfer
                              credit. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           
                           <p>No. The Health Information Technology program does not require the TEAS.</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the minimum overall GPA required to apply?</h3>
                           
                           <p>The overall GPA Admission Points will not be in consideration for the total admission.</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the deadline to apply?</h3>
                           
                           <p>The next application period for Fall 2018 will begin March 2018, with applications
                              due June 15, 2017.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many students are accepted into the program?</h3>
                           
                           <p>NThe Health Information Technology program accepts 25 students each Fall term.</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-8">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           
                           <p>Starting Fall 2015, the Health Information Technology program will be fully online.
                              Health Information
                              Technology courses must be taken in the order according to the course sequence guide.
                              The practicums in the
                              program will be completed at an assigned clinical site. Students should plan to spend
                              1-2 days per week Monday
                              through Friday during the hours 8am-5pm at their assigned practicum site during the
                              semester in which they are
                              enrolled in the practicum course.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>How do I apply?</h3>
                           
                           <p>Download &amp; fill out the Health Information Technology Application. Return your completed
                              application to
                              Health Sciences Advising along with a non-refundable Health Sciences program application
                              fee of $15
                           </p>
                           
                           <ul>
                              
                              <li>
                                 
                                 <h4>By mail:</h4>
                                 Send the application along with a check or money order payable to Valencia College
                                 to
                                 the address listed below:
                                 
                                 <address class="wrapper_indent">
                                    Valencia College<br>
                                    Business Office<br>
                                    P.O. Box 4913.<br>
                                    Orlando, FL
                                    32802
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 
                                 <h4>In person:</h4>
                                 Make payment in the Business Office on any Valencia Campus &amp; request that the
                                 Business Office forward your program application &amp; fee receipt to the Health Sciences
                                 Advising office at
                                 mail code 4-30 
                              </li>
                              
                           </ul>
                           
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              documentation or requirements can result in denied admission. All items listed on
                              the <a href="/documents/academics/programs/health-sciences/health-information-technology-program-guide.pdf">Program Guide
                                 Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/health-information-technology/index.pcf">©</a>
      </div>
   </body>
</html>