<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Sciences  | Valencia College</title>
      <meta name="Description" content="Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Health Sciences</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Health Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Health Sciences Programs</h2>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The health sciences field offers a diverse range of careers, but they all have a shared
                        goal—to help people. Whether you see yourself as a nurse providing patient care, a
                        radiographer taking X-rays of injured patients, or an EMT responding to accident scenes,
                        Valencia can help you develop the skills and expertise you need to be successful in
                        this high-demand career field.
                     </p>
                     			
                  </div>
                  			
                  <h3>Featured Health Sciences Programs </h3>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/health-information-technology/"><img src="/images/academics/programs/health-sciences/featured-health-info-tech.jpg" alt="Featured Program - Health Information Technology">
                           					
                           <h3>Health Information Technology</h3>
                           					
                           <p>In Valencia’s A.S. degree program Health Information Technology, students learn how
                              to use technology to collect, analyze, monitor, maintain and report health data. This
                              program is designed to prepare students for employment in a variety of health care
                              settings. 
                           </p>
                           					
                           <div class="button-outline">Learn More</div>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/emergency-medical-services-tech/"><img src="/images/academics/programs/health-sciences/featured-ems.png" alt="Featured Program - Emergency Medical Services Technology">
                           					
                           <h3>Emergency Medical Services Technology</h3>
                           					
                           <p>Good at making split-second decisions and staying calm in a crisis? This may be the
                              job for you. EMTs and paramedics respond to medical emergencies, assess the patient’s
                              injuries or illness, stabilize the patient by administering emergency medical care
                              and transport patients to medical facilities. 
                           </p>
                           					
                           <div class="button-outline">Learn More</div>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/cardiovascular-technology/"><img src="/images/academics/programs/health-sciences/featured-cardiovascular.png" alt="Featured Program - Cardiovascular Technology">
                           					
                           <h3>Cardiovascular Technology</h3>
                           					
                           <p>As a cardiovascular technologist, you would work side-by-side with cardiologists and
                              physicians to diagnose and treat heart disease and vascular problems. Exciting career
                              opportunities are available in diagnostic and interventional cardiac catheterization
                              labs in acute care hospitals, outpatient facilities and clinics.
                           </p>
                           					
                           <div class="button-outline">Learn More</div>
                           					</a></div>
                     			
                  </div>
                  
                  
                  		
               </div>
               
               
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Health Sciences Programs &amp; Degrees</h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     
                     				
                     <h3 class="subheader">Associate in Science</h3>
                     				
                     <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                        also transfers to the Bachelor of Applied Science program offered at some universities.
                        If the program is 'articulated,' it will also transfer to a B.A. or B.S. degree program
                        at a designated university.
                     </p>
                     				
                     <ul>
                        								
                        <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/dental-hygiene/">Dental Hygiene</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/">Diagnostic Medical Sonography</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/emergency-medical-services/">Emergency Medical Services Technology</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/health-information-technology/">Health Information Technology</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/nursing/">Nursing Program Tracks</a>
                           									
                           <ul>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/traditional-daytime.php">Nursing Traditional, Daytime</a>
                                 											
                                 <ul>
                                    <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                 </ul>
                                 										
                              </li>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/traditional-evening.php">Nursing Traditional, Evening/Weekend</a>
                                 											
                                 <ul>
                                    <li><a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index">Nursing, Valencia/UCF Concurrent A.S.N. to B.S.N. Option</a></li>
                                 </ul>
                                 										
                              </li>
                              										
                              <li><a href="/academics/programs/health-sciences/nursing/accelerated-track.php">Nursing, Accelerated Track in Nursing</a></li>
                              									
                           </ul>
                           								
                        </li>
                        								
                        <li><a href="/academics/programs/health-sciences/radiography/">Radiography</a></li>
                        								
                        <li><a href="/academics/programs/health-sciences/respiratory-care/">Respiratory Care</a></li>
                        								
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Veterinary Technology</a>
                           									
                           <ul>
                              <li>This AS Degree is offered through a Cooperative Agreement with <a href="http://www.spcollege.edu/vt/">St. Petersburg College</a>.  Students interested in this program may take the general education requirements
                                 at Valencia and complete the rest of the degree online through St. Petersburg College.
                                 Valencia Catalog has more information about the <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Cooperative Agreement with St. Petersburg College</a>.
                                 										
                              </li>
                           </ul>
                        </li>
                        							
                     </ul>
                     				
                     <h3 class="subheader">Bachelor of Science</h3>
                     				
                     <p>The B.S. degree&nbsp;prepares you to enter an advanced position within a specialized career
                        field.&nbsp;The upper-division courses build on an associate degree and take about two
                        years to complete.
                     </p>
                     				
                     <ul>
                        					
                        <li><a href="/academics/programs/health-sciences/cardiopulmonary-sciences/">Cardiopulmonary Sciences</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/nursing/bachelors.php">Nursing</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/radiologic-imaging-science/">Radiologic and Imaging Sciences</a></li>
                        				
                     </ul>
                     				
                     <h3>Advanced Technical Certificate</h3>
                     				
                     <p>The <a href="https://preview.valenciacollege.edu/future-students/degree-options/advanced-technical-certificates/" target="_blank">Advanced Technical Certificate</a>, an extension of a specific Associate degree program, consists of at least nine (9),
                        but less than 45, credits of college-level course work. Students who have already
                        received an eligible Associate degree and are seeking a specialized program of study
                        to supplement their associate degree may seek an Advanced Technical Certificate.
                     </p>
                     				
                     <ul>
                        					
                        <li><a href="/academics/programs/health-sciences/computed-tomography/">Computed Tomography</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/echocardiography/">Echocardiography</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/leadership-healthcare/">Leadership in Healthcare</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/">Magnetic Resonance Imaging</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/mammography/">Mammography</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/vascular-sonography/">Vascular Sonography</a></li>
                        				
                     </ul>
                     				
                     <h3 class="subheader">Certificate</h3>
                     				
                     <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                        for job advancement. Most can be completed in one year or less. Credits earned can
                        be applied toward a related A.S. degree program.
                     </p>
                     				
                     <ul>
                        					
                        <li><a href="/academics/programs/health-sciences/emergency-medical-services/emergency-medical-technician.php">Emergency Medical Technician</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/medical-info-coder/">Medical Information Coder/Biller</a></li>
                        					
                        <li><a href="/academics/programs/health-sciences/emergency-medical-services/paramedic.php">Paramedic Technology</a></li>
                        				
                     </ul>
                     				
                     <h3 class="subheader">Continuing Education</h3>
                     				
                     <p><a href="/academics/departments/health-sciences/continuing-education/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                        development, language courses, certifications and custom course development for organizations.
                        Day, evening, weekend and online learning opportunities are available.
                     </p>
                     
                     			
                  </div>
                  
                  		
               </div>
               
               		
               <div class="container margin-30">
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4">
                        					<a class="box_feat" href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=health+science&amp;Clusters=8&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all"> <i class="far fa-life-ring"></i>
                           						
                           <h3>Career Coach</h3>
                           						
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              						
                           </p>
                           						
                           <div class="button-outline">Learn More</div>
                           					</a> 
                     </div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/nursing/"> <i class="far fa-dollar-sign"></i>
                           					
                           <h3>Career Opportunities</h3>
                           					
                           <p>Nursing A.S. grads make the highest salaries of all Valencia graduates, averaging
                              $48,344 annually.<br>
                              						<br>
                              					
                           </p>
                           					
                           <div class="button-outline">Nursing Program</div>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/emergency-medical-services/"> <i class="far fa-share"></i>
                           					
                           <h3>Job Placement</h3>
                           					
                           <p>Valencia’s Emergency Medical Services program is mentored by local fire and EMS agencies,
                              who also help with job placement.
                           </p>
                           					
                           <div class="button-outline">EMS Program</div>
                           					</a></div>
                     
                     
                     			
                  </div>
                  
                  
                  		
               </div>
               
               
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Health Sciences Advising</h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div>
                     				
                     <p>Health Sciences Advising staff offer specialized educational advisors who can guide
                        students in the successful completion of an Associate in Science Degree or Bachelor
                        of Science Degree or Certificate Program in allied health or nursing. Attendance at
                        a Health Sciences Information Session is mandatory before meeting with a Health Sciences
                        Advisor for an associate-level program but not for meeting about a bachelor-level
                        program.
                     </p>
                     				
                     <p>The advising staff can answer program-specific questions, assist in career and educational
                        planning, and offer expert educational guidance for students seeking career opportunities
                        through the completion of an A.S. or B.S. Degree or a Certificate program.
                     </p>
                     				
                     <p>Associate in Science students are expected to complete their educational goals and
                        be professionally marketable in a much shorter period of time than many others. This
                        is why the early development of career and education plans is essential to a successful
                        transition from school to work.
                     </p>
                     				
                     <div class="row">
                        					
                        <div class="col-md-8 col-sm-8">
                           						<img src="/images/academics/programs/health-sciences/health-sciences-advising.jpg" class="img-responsive">
                           					
                        </div>
                        					
                        <div class="col-md-4 col-sm-4">
                           						
                           <h5 class="val">Location</h5>
                           						<i class="far fa-map-marker fa-fw"></i>West Building 1, Room 130 <br>
                           						
                           <h5 class="val">Hours</h5>
                           						<i class="far fa-calendar fa-fw"></i>Monday - Thursday 8 a.m. - 5 p.m.<br>
                           						<i class="far fa-calendar fa-fw"></i>Friday 9 a.m. – 5 p.m. <br>
                           						
                           <h5 class="val">Contact Us</h5>
                           						<i class="far fa-envelope fa-fw"></i><a href="mailto:HealthScienceAdvising@valenciacollege.edu">HealthScienceAdvising@valenciacollege.edu</a> <br>
                           						<i class="far fa-phone fa-fw"></i>407-582-1228<br>
                           						<i class="far fa-fax fa-fw"></i>407-582-5462 (fax) 
                        </div>
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/index.pcf">©</a>
      </div>
   </body>
</html>