<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Magnetic Resonance Imaging  | Valencia College</title>
      <meta name="Description" content="Magnetic Resonance Imaging | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/magnetic-resonance-imaging/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/magnetic-resonance-imaging/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Magnetic Resonance Imaging</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Magnetic Resonance Imaging</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     			
                     <h2>Magnetic Resonance Imaging</h2>
                     			
                     <p>Advanced Technical Certificate</p>
                     		
                  </div>
                  		
                  <div class="row box_style_1">
                     			
                     <p>The Advanced Technical Certificate (ATC) in Magnetic Resonance Imaging is for health
                        professionals who have completed an associate degree, or higher, in Radiography, Diagnostic
                        Medical Sonography, Nuclear Medicine Technology or Radiation Therapy from a regionally
                        accredited institution.
                     </p>
                     			
                     <p>The Magnetic Resonance Imaging (MRI) Certificate can lead to a Bachelor’s degree in
                        Radiologic and Imaging Sciences for students who meet the specific Bachelor’s degree
                        requirements.
                     </p>
                     		
                  </div>
                  		
               </div>
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Magnetic Resonance Imaging</h2>
                     				
                     <p>&nbsp;</p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/magnetic-resonance-imaging-program-guide.pdf">Current Program Guide</a></li>
                              							
                              <li><a href="http://catalog.valenciacollege.edu/degrees/advancedtechnicalcertificates/">Current Program Requirements</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li><strong>Application Deadline:</strong> No Deadline
                              </li>
                              							
                              <li><strong>Begins:</strong> Fall &amp; Spring
                              </li>
                              							
                              <li><strong>Application:</strong> <a href="/documents/academics/programs/health-sciences/Application-BSRAD-and-ATCs.pdf">Download the Application</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <h3>Program Changes</h3>
                        					
                        <table class="table table table-striped cart-list add_bottom_30">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Date Updated</th>
                                 								
                                 <th scope="col">Effective Date</th>
                                 								
                                 <th scope="col">Change</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td colspan="3">No Changes</td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <ul class="list_staff">
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              							
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              							
                              <p>Dean of Allied Health</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available"></figure>
                              							
                              <h4>Susan Gosnell </h4>
                              							
                              <p>Chair of Magnetic Resonance Imaging</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img class="img-circle" src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising"></figure>
                              							
                              <h4>Health Sciences Advising</h4>
                              							
                              <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                              						
                           </li>
                           					
                        </ul>
                        				
                     </div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>Steps for Admission to Program</h4>
                           						
                           <ul class="list_order">
                              							
                              <li><span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 are not local and cannot attend the information session, and review the <a href="/documents/academics/programs/health-sciences/magnetic-resonance-imaging-program-guide.pdf">program guide</a>.
                              </li>
                              							
                              <li><span>2</span><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply</a> to Valencia College.
                              </li>
                              							
                              <li><span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              							
                              <li><span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 requirements, you can apply to the program.
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Information About the Magnetic Resonance Imaging Field</h2>
                     				
                     <p>&nbsp;</p>
                     			
                  </div>
                  			
                  <p>Magnetic resonance imaging (MRI) uses a strong magnetic field and radio waves with
                     computerized equipment to generate sectional images of patient anatomy.  MR technologists
                     use problem solving and critical skills to adapt procedural requirements to the patient
                     and specific area of study.  An MR technologist must be able to perform magnetic resonance
                     procedures without constant supervision to technical detail.  It is important that
                     the technologist have a thorough knowledge of anatomy in every dimension.  Judgments
                     about the MRI pulsing sequence, gradient magnetic fields and anatomic section orientation
                     must be made independently and with regard to patient care and safety.  Knowledge
                     of the characteristics of magnetic fields, electromagnets and atomic structure is
                     useful in this type of imaging.  This advanced technology uses highly specialized
                     equipment and requires specialized education and training.
                  </p>
                  			
                  <p>Increasingly, it is being required that imaging professionals be credentialed in any
                     modality that they may be performing diagnostic procedures.  This Advanced Technical
                     Certificate Program is designed for individuals who are currently certified in Radiography
                     (ARRT), Radiation Therapy (ARRT), Nuclear Medicine Technology (ARRT)/(NMTCB) or Sonography
                     (ARRT)/(ARDMS).  Program content follows the American Society of Radiologic Technologists
                     (ASRT) curriculum to prepare program completers to take the American Registry of Radiologic
                     Technologists (ARRT) certification exam in Magnetic Resonance Imaging.  Completion
                     of the program does not ensure permission to take the credentialing examination. With
                     the exception of clinical education, program courses are offered online.
                  </p>
                  			
                  <p>Magnetic Resonance Imaging is a selective admission, limited enrollment program. 
                     Admission to Valencia does not imply acceptance to the Magnetic Resonance Imaging
                     program.  Students must submit an application and be accepted to the program.  
                  </p>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/magnetic-resonance-imaging/index.pcf">©</a>
      </div>
   </body>
</html>