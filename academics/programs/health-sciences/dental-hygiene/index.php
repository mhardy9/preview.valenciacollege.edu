<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dental Hygiene  | Valencia College</title>
      <meta name="Description" content="Dental Hygiene | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/dental-hygiene/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/dental-hygiene/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dental Hygiene</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Dental Hygiene</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Dental Hygiene</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Dental Hygiene Associate in Science (A.S.) degree program at Valencia College
                        is a two-year program that prepares you to go directly into a specialized career as
                        a dental hygienist.
                     </p>
                     
                     <p>As a dental hygienist you will work as part of a dental team in examining patients
                        for signs of oral disease and providing preventative dental care. In addition to cleanings
                        and X-rays, hygienists also administer sedation, assist with root canals, apply fluoride
                        and sealants, and educate patients on ways to improve and maintain good oral health.
                        Valencia’s dental hygiene program prepares you to become licensed as a primary oral
                        health care professional and is dedicated to helping you develop the specific skills
                        you need to ensure that the highest quality of dental care is provided for patients.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/dentalhygiene/#programrequirementstext"> <i class="far fa-laptop"></i>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://valenciacollege.edu/academics/programs/health-sciences/documents/2016-17-Course-Planning-Guide-Health-Information-Tech.pdf"> <i class="far fa-calendar"></i>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 6 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/asdegrees/credit_octc.cfm"> <i class="far fa-share-alt"></i>
                           
                           <h3>Alternative Credit</h3>
                           
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? Tech Center Programs – Receive college credit for completing an approved
                              OCPS Tech Center program
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-dollar-sign"></i>
                           
                           <h3>Average Salary &amp; Placement</h3>
                           
                           <p>Valencia’s A.S. Degree and Certificate programs placement rate ranges between 90%
                              – 95%. Dental Hygiene 
                              average salaries ranges from $40,000 - $450,000<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4">
                        <a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <i class="far fa-life-ring"></i>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.php"> <i class="far fa-question-circle"></i>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Attend a Health Sciences Information Sessions for an overview of the limited access
                              Health Science programs
                              at Valencia College and the procedures for enrolling in them. <br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  <!--End row--> 
                  
               </div>
               
               <!--End container --> 
               
               
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Dental Hygiene </h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="/documents/academics/programs/health-sciences/dental-hygiene-program-guide.pdf">Current Program Guide</a></li>
                              
                              <li><a href="/documents/academics/programs/health-sciences/dental-hygiene-estimated-costs.pdf">Estimated Program Costs</a></li>
                              
                              <li>
                                 <a href="/documents/academics/programs/health-sciences/dental-hygiene-course-planning-guide.pdf">Course Planning Guide </a> 
                              </li>
                              
                              <li><a href="index.php#FAQ">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li>
                                 <strong>Application Deadline:</strong> January 15, 2018
                              </li>
                              
                              <li>
                                 <strong>Begins:</strong> Summer 2018
                              </li>
                              
                              <li>
                                 <strong>Application:</strong> Download should be available November 2017 
                              </li>
                              
                           </ul>
                           <br>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        
                        <h3>Program Changes</h3>
                        
                        <table class="table table table-striped cart-list add_bottom_30">
                           
                           <tr>
                              
                              <th scope="col">Date Updated</th>
                              
                              <th scope="col">Effective Date</th>
                              
                              <th scope="col">Change</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>&nbsp;</td>
                              
                              <td>Fall 2016 </td>
                              
                              <td>
                                 
                                 <p>If you are planning to meet the HUN2015 (Diet Therapy) prerequisite for Fall 2016,
                                    please note: 
                                 </p>
                                 
                                 <p>The prerequisite for HUN2015 (Diet Therapy) will be HUN1201 (Essentials of Nutrition).
                                    Note: HUN1001 will no longer meet the prerequisite for HUN2015.
                                 </p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>9/4/14 (posted on website) </td>
                              
                              <td>12/10</td>
                              
                              <td>
                                 
                                 <p>TEAS Version 5 (V) minimum Composite score of<strong> *58.7% </strong>required.  TEAS scores are valid for 5 years.<strong>*Subject to change. You must meet required score in effect at the time you apply to
                                       the program. </strong></p>
                                 
                                 <p> TEAS information is available at <a href="../../assessments/teas">valenciacollege.edu/assessments </a>and &nbsp;<a href="https://www.atitesting.com/Home.aspx">ATItesting.com&nbsp; </a></p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>8/4/16 (posted on website) </td>
                              
                              <td>9/15/17</td>
                              
                              <td>
                                 
                                 <p><span class="notice">TEAS for program entry after September 15, 2017 must submit the new version: ATI TEAS.</span>&nbsp; The minimum composition score of *58.7% is required.&nbsp; Scores are valid for 5 years.
                                    *Subject to change.&nbsp; You must meet required score in effect at the time you apply
                                    to the program.&nbsp; 
                                 </p>
                                 
                                 <p>TEAS information is available at <a href="../../assessments/teas">valenciacollege.edu/assessments </a>and <a href="https://www.atitesting.com/Home.aspx">ATItesting.com </a></p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>8/17/16</td>
                              
                              <td>Fall 2017 </td>
                              
                              <td>
                                 
                                 <p>Students who have completed HUN1001 prior to Fall 2016 are permitted to enroll in
                                    HUN2015 for fall 2016 through summer 2017. 
                                 </p>
                                 
                                 <p>Effective Fall 2017, all students must have completed HUN1201 with a minimum grade
                                    of "C" to satisfy the prerequisite for HUN2015.
                                 </p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/6/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>
                                 <ul>
                                    
                                    <li>HUN2202 (Essentials of Nutrition with Diet Therapy) points increased: A: 10, B: 8,
                                       C:6
                                    </li>
                                    
                                    <li>The overall GPA Admission Points will not be in consideration for the total admission</li>
                                    
                                    <li>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1. Note: SLS 1122
                                       points will remain the same--A: 3, B:2, C: 1
                                    </li>
                                    
                                 </ul>
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/20/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>International Students are eligible to apply for this
                                 Health Sciences Program. <a href="International-Student-Eligibility.cfm">Click here for details.</a>
                                 
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                        <ul class="list_staff">
                           		          
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              			
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              
                              <p>Dean of Allied Health</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/pam-sandy-picture.jpg" alt="Pamela Sandy" class="img-circle"></figure>
                              
                              <h4>Pamela Sandy, CRDH, BS, MA</h4>
                              
                              <p>Program Chair, Dental Hygiene</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/robin-poole-picture.jpg" alt="Robin L. Poole" class="img-circle"></figure>
                              
                              <h4>Robin L. Poole, RDH, BS, MA</h4>
                              
                              <p>Professor, Dental Hygiene </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/rebekah-pittman-picture.jpg" alt="Rebekah Pittman" class="img-circle"></figure>
                              
                              <h4>Rebekah Pittman, CRDH, BSDH</h4>
                              
                              <p>Professor, Dental Hygiene </p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/tiffany-baggs-picture.jpg" alt="Tiffany Baggs" class="img-circle"></figure>
                              
                              <h4>Tiffany Baggs, CRDH, MSDH </h4>
                              
                              <p>Instructional lab supervisor</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="April Montallana" class="img-circle"></figure>
                              
                              <h4>April Montallana </h4>
                              
                              <p>Administrative Assistant I</p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>Steps for Admission to Program</h4>
                           
                           <ul class="list_order">
                              
                              <li>
                                 <span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                    Questions</a>. 
                              </li>
                              
                              <li>
                                 <span>2</span><a href="#">Apply</a> to Valencia College. 
                              </li>
                              
                              <li>
                                 <span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              
                              <li>
                                 <span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 requirements, you can apply to the program. 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <div class="container_gray_bg">
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        
                        <p> Valencia College's dental hygiene program was established in 1977 and graduated its
                           charter class of 23 students in 1978.  The class of 2007 was the thirtieth graduating
                           class of the dental hygiene program.  The graduates of the program are employed throughout
                           the United States as clinicians, educators, and public health hygienists.  Numerous
                           graduates have continued their education in dental hygiene, dentistry, education,
                           and public health. 
                        </p>
                        
                        
                        <p>Valencia's  dental hygiene program supports a fully-functional dental hygiene clinic
                           on West Campus where program students provide preventive dental hygiene services to
                           the student population as well as to the general public.  In addition, students have
                           clinical experiences at community-based health centers.  The dental hygiene program
                           is fully accredited by the American Dental Association's Commission on Dental Accreditation.
                           Students who complete the program are prepared to take the Dental Hygiene National
                           Board Examination and state or regional licensing examinations to apply for state
                           licensure and future employment. 
                        </p>
                        
                        
                        <p>Students and faculty have the opportunity to benefit from membership in the American
                           Dental Hygienists' Association, the Florida Dental Hygienists' Association, and other
                           professional organizations. With the support of Valencia College's student development
                           office, dental hygiene students regularly participate in professional association
                           activities and have won numerous  awards.
                        </p>
                        
                     </div>
                     	  
                     <div class="row">
                        		  
                        <div class="col-md-6">
                           			  
                           <h3> National Board Results 2003-2016</h3>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">YEAR</th>
                                 
                                 <th scope="col"># of Candidates</th>
                                 
                                 <th scope="col">% Passing</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2003</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2004</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2005</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2006</td>
                                 
                                 <td>18</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2007</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2008</td>
                                 
                                 <td>18</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2009</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2010</td>
                                 
                                 <td>20</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2011</td>
                                 
                                 <td>17</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2012</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2013</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2014</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2015</td>
                                 
                                 <td>19</td>
                                 
                                 <td>95%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2016</td>
                                 
                                 <td>17</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                           </table>
                           			  
                        </div>
                        		  
                        <div class="col-md-6"> 
                           <h3>Florida State Board/Regional Board Results</h3>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">YEAR</th>
                                 
                                 <th scope="col"># of Candidates</th>
                                 
                                 <th scope="col">% Passing</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2003</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2004</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2005</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2006</td>
                                 
                                 <td>18</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2007</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2008</td>
                                 
                                 <td>18</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2009</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2010</td>
                                 
                                 <td>20</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2011</td>
                                 
                                 <td>17</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2012</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2013</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2014</td>
                                 
                                 <td>23</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2015</td>
                                 
                                 <td>19</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                           </table>  
                           			  
                        </div>
                        		  
                        
                        		  
                     </div>
                     
                  </div>
                  
               </div>
               <a name="FAQ" id="FAQ"></a>
               
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Frequently Asked Questions</h2>
                     
                     <p>Dental Hygiene</p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           
                           <p>Yes. You must complete the admission process to the college before you submit a Dental
                              Hygiene
                              program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              questions. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can International students apply?</h3>
                           
                           <p>Yes. Students must be a U.S. Citizen, a U.S. permanent resident, or hold a non-immigrant
                              visa with employment
                              authorization. Non-immigrant status students can visit the <a href="https://travel.state.gov/content/visas/en/visit/visitor.php">U.S. Department of State directory of
                                 visa categories</a> to verify employment eligibility. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What courses do I need to take prior to applying?</h3>
                           
                           <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                              Te pri facete latine
                              salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                              posse exerci
                              volutpat has in.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Are the prerequisite courses offered online?</h3>
                           
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available every term. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           
                           <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                              if accepted, Cardiovascular Technology courses are only offered on the West campus.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           
                           <p>Yes, you must complete the TEAS 5. The examination fee is your responsibility. More
                              information on the TEAS is available at <a href="https://valenciacollege.edu/assessments/TEAS.cfm">valenciacollege.edu/assessments/TEAS.cfm</a></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can I use TEAS scores from another institution?</h3>
                           
                           <p>Yes. Students who complete the TEAS at another institution must request an official
                              score report from ATI as a paper copy is not accepted. Requests can be made online
                              via <a href="www.atitesting.com">www.atitesting.com</a></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many students are accepted into the program?</h3>
                           
                           <p>The Dental Hygiene program accepts 25 students for the Summer term.</p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           
                           <p>The Dental Hygiene program is a daytime program located on the West campus; its time
                              demands should be considered the equivalent of a fulltime job as in addition to lecture,
                              lab and clinicals, students are expected to study between 15-25 hours a week - if
                              a student chooses to work while in the program, a maximum of 15 hours a week is recommended
                              due to the demands of the program.  Time management is crucial to successful program
                              completion!  The specific schedule can vary from term to term and from year to year.
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many clinical hours are in the Dental Hygiene program and what credential would
                              I be eligible to sit for upon successful completion?
                           </h3>
                           
                           <p>The total number of clinical hours in the program is 738.  Upon graduation and successful
                              completion of the National Board Dental Hygiene Examination and the ADEX clinical
                              exam, the graduate is eligible to apply for a license as a registered dental hygienist
                              and for certification in local anesthesia. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How successful are your Dental Hygiene graduates in finding employment?</h3>
                           
                           <p>The five year average Licensure Rate for Dental Hygiene graduates is 100% and the
                              five year average Placement Rate for Dental Hygiene graduates is 97%.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>How do I apply?</h3>
                           
                           <p>Download &amp; fill out the Program Name Application. Return your completed application
                              to
                              Health Sciences Advising along with a non-refundable Health Sciences program application
                              fee of $15
                           </p>
                           
                           <ul>
                              
                              <li>
                                 
                                 <h4>By mail:</h4>
                                 Send the application along with a check or money order payable to Valencia College
                                 to
                                 the address listed below:
                                 
                                 <address class="wrapper_indent">
                                    Valencia College<br>
                                    Business Office<br>
                                    P.O. Box 4913.<br>
                                    Orlando, FL
                                    32802
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 
                                 <h4>In person:</h4>
                                 Make payment in the Business Office on any Valencia Campus &amp; request that the
                                 Business Office forward your program application &amp; fee receipt to the Health Sciences
                                 Advising office at
                                 mail code 4-30 
                              </li>
                              
                           </ul>
                           
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              documentation or requirements can result in denied admission. All items listed on
                              the <a href="/documents/academics/programs/health-sciences/dental-hygiene-program-guide.pdf">Program Guide
                                 Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/dental-hygiene/index.pcf">©</a>
      </div>
   </body>
</html>