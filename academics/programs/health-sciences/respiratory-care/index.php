<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Respiratory Care  | Valencia College</title>
      <meta name="Description" content="Respiratory Care | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/respiratory-care/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/respiratory-care/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Respiratory Care</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Respiratory Care</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Respiratory Care</h2>
                     				
                     <p>Associate in Science</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The Respiratory Care Associate in Science (A.S.) degree at Valencia College is a two-year
                        program that prepares you to go directly into a specialized career as a respiratory
                        therapist.
                     </p>
                     				
                     <p>Respiratory therapists are an integral member of any medical team and serve as life-support
                        specialists who work to restore the heart and lung system to normal function. A respiratory
                        therapist’s main role is to assist physicians in treating patients who experience
                        heart or lung problems and breathing disorders such as chronic asthma or emphysema.
                        Valencia’s program provides a strong base in therapeutic techniques and state-of-the-art
                        equipment, as well as hands-on experience at some of the community’s major medical
                        centers. Through this combined educational experience, students learn to use their
                        classroom knowledge in real-life situations.
                     </p>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/respiratorycare/#programrequirementstext"> <span class="far fa-laptop"></span>
                           					
                           <h3>Program Overview</h3>
                           					
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              						<br><br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/respiratory-care-program-guide.pdf"> <span class="far fa-calendar"></span>
                           					
                           <h3>Program Length</h3>
                           					
                           <p>Program lasts for 6 semesters after all of the pre-admission course requirements have
                              been met.<br><br><br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           					
                           <h3>Want more information?</h3>
                           					
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              					
                           </p>
                           					</a></div>
                     			
                  </div>
                  			
                  <!--End row-->
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           					
                           <h3>Average Salary</h3>
                           					
                           <p>Respiratory Care average salaries range from $40,000 – $48,000<br><br><br>
                              						<br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           					
                           <h3>Career Coach</h3>
                           					
                           <p>Explore Health Science related careers, salaries and job listings. <br><br><br>
                              						<br>
                              					
                           </p>
                           					</a> 
                     </div>
                     
                     			
                  </div>
                  			
                  <!--End row--> 
                  
                  
                  		
               </div>
               
               		
               <div class="container margin-30 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Radiography </h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/respiratory-care-program-guide.pdf">Current Program Guide</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/respiratory-care-estimated-costs.pdf">Estimated Program Costs</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/respiratory-care-course-planning-guide.pdf">Course Planning Guide</a></li>
                              							
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/respiratorycare/#programrequirementstext">Current Program Requirements</a></li>
                              							
                              <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li>Application Deadline: January 15, 2019</li>
                              							
                              <li>Begins: Summer 2019</li>
                              							
                              <li>Application: Download should be available November 2018</li>
                              						
                           </ul><br><br>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <h3>Program Changes</h3>
                        					
                        <table class="table table table-striped cart-list add_bottom_30">
                           						
                           <tr>
                              							
                              <th scope="col">Date Updated</th>
                              							
                              <th scope="col">Effective Date</th>
                              							
                              <th scope="col">Change</th>
                              						
                           </tr>  
                           						
                           <tr>
                              							
                              <td>8/22/2017</td>
                              							
                              <td>&nbsp;</td>
                              							
                              <td>
                                 <p>NOTE: There was a discrepancy on the catalog page in the 2017-2018 printed Program
                                    Guide for the Respiratory Care Program. The program is 5 terms and the sequence of
                                    the program is as follows:
                                 </p>
                                 								
                                 <p>Year 1: Spring and Summer<br>
                                    									Year 2: Fall, Spring and Summer
                                 </p>
                                 								
                                 <p><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/respiratorycare/#programrequirementstext" target="_blank">2017-2018 catalog "Plan of Study Grid"</a></p>
                              </td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <td>3/20/17</td>
                              							
                              <td>Fall 2017</td>
                              							
                              <td>International Students are eligible to apply for this
                                 								Health Sciences Program. <a href="/academics/departments/health-sciences/international-student-eligibility.php">Click here for details.</a></td>
                              						
                           </tr>  
                           						
                           <tr>
                              							
                              <td>3/6/17</td>
                              							
                              <td>Fall 2017</td>
                              							
                              <td>
                                 <ul>
                                    								
                                    <li>The overall GPA Admission Points will not be in consideration for the total admission</li>
                                    								
                                    <li>Points will no longer be awarded for Overall GPA </li>
                                    								
                                    <li>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1.  Note: SLS 1122
                                       points will remain the same--A: 3, B:2, C: 1 
                                    </li>
                                    								
                                 </ul>
                                 								
                                 <p></p>
                              </td>
                              						
                           </tr> 
                           					
                        </table>
                        
                        					
                        <ul class="list_staff">
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/Jamy-chulak.jpg" alt="Jamy Chulak, M.S., RRT Photo" class="img-circle"></figure>
                              							
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              							
                              <p>Dean of Allied Health</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/Jamy-chulak.jpg" alt="Jamy Chulak, M.S., RRT Photo" class="img-circle"></figure>
                              							
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              							
                              <p>Program Chair</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/kimberlee-harvey.jpg" alt="Penny Conners Photo" class="img-circle"></figure>
                              							
                              <h4>Kimberlee Harvey, BS, RRT-NPS</h4>
                              							
                              <p>Clinical Coordinator</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                              							
                              <h4>Health Sciences Advising</h4>
                              							
                              <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                              						
                           </li>
                           					
                        </ul>
                        				
                     </div>		
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>Steps for Admission to Program</h4>
                           						
                           <ul class="list_order">
                              							
                              <li><span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    								Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 								are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                    								Questions</a>. 
                              </li>
                              							
                              <li><span>2</span><a href="#">Apply</a> to Valencia College. 
                              </li>
                              							
                              <li><span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              							
                              <li><span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 								requirements, you can apply to the program. 
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <!--End row --> 
                  		
               </div>
               		
               <div class="container margin-30 box_style_1">
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <h3>Learn How To</h3>
                        					
                        <ul>
                           <li>Evaluate and treat patients’ breathing problems</li>
                           						
                           <li>Sample blood to test the oxygen and carbon dioxide levels and report results to the
                              physician
                           </li>
                           						
                           <li>Use a ventilator to pump pressurized air-oxygen mixes through a tube into patients’
                              lungs
                           </li>	   
                           					
                        </ul>
                        
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <h3>Job Outlook</h3>
                        					
                        <p>Respiratory therapists work mainly in hospitals, in the departments of respiratory
                           care, pulmonary medicine or anesthesiology. Other career opportunities are available
                           in medical clinics, physicians’ offices, home care, sales, education, research and
                           consulting. Employment of respiratory therapists is projected to grow faster than
                           the average for all occupations. Growth in the middle-aged and elderly population
                           will lead to an increased incidence of respiratory conditions such as emphysema, chronic
                           bronchitis and pneumonia.
                        </p>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <h3>Potential Careers</h3>
                        					
                        <ul>
                           						
                           <li>Respiratory Therapist</li>
                           						
                           <li>Respiratory Care Practitioner</li>
                           					
                        </ul>
                        
                        				
                     </div>
                     
                     			
                  </div>
                  			
                  <div class="container margin-30 box_style_1">
                     				
                     <div class="main-title">
                        					
                        <h2>General Radiography Program Information</h2>
                        				
                     </div>
                     				
                     <p>The Respiratory Care Program started out as a hospital based program at Orange Memorial
                        Hospital in Orlando back in 1968.  All students took their academic course work at
                        Valencia Junior College on Oak Ridge Road.  All clinical practicum was implemented
                        and carried out at Orange Memorial Hospital (now a division of the Orlando Regional
                        Healthcare System).
                     </p>
                     
                     				
                     <p>The Respiratory Care Program at Valencia College has been fully accredited since 1972.
                        In 1977, a Self-Study process for reaccreditation was initiated and, after the on-site
                        visit, corrections were made and the program received full accreditation status. 
                        This process was repeated every five years until 2003 whereupon with repeated successful
                        annual reports the program was transitioned to a ten-year cycle.
                     </p>
                     
                     				
                     <p>Today the Respiratory Care Program is considered one of the longest running programs
                        in the country with some of the most excellent facilities available to it for clinical
                        practice.  One such affiliate is the Orlando Regional Healthcare System consisting
                        of Orlando Regional Medical Center, Arnold Palmer, Winnie Palmer, Dr. P. Phillips,
                        South Lake, South Seminole and St. Cloud divisions staffed with very dedicated and
                        competent therapists.  The other affiliate is the Florida Hospital System.
                     </p>
                     
                     				
                     <p>The success of the program has been proven by the job placement rate which has been
                        100% for every graduating class so far with many of the graduates holding at least
                        supervisor status.
                     </p>
                     				
                     <p>Our communities of interest include the local health care facilities, the University
                        of Central Florida, Seminole State College, and our students.  The local Health Care
                        facilities offer a wide range of services.  Florida is a dynamic state for respiratory
                        care and can be viewed as a broad community of interest.  Our citizens’ health care
                        is a primary concern given the number of older people residing here.  Respiratory
                        care delivery by professionals is regulated by the Florida Department of Health, Division
                        of Medical Quality Assurance, Board of Respiratory Care.
                     </p>
                     
                     				
                     <p>The Respiratory Care program at Valencia College is committed to providing our community
                        with information regarding program outcomes as a continued effort to assess program
                        viability, effectiveness, and success. 
                     </p>
                     				
                     <h3>Accreditation</h3>
                     				
                     <p>The Commission on Accreditation for Respiratory Care (CoARC) has completed the verification
                        of the <a href="http://www.coarc.com/47.html">Programmatic Outcomes Data</a> from the 2011 Annual Report of Current Status (RCS). This outcome data for Respiratory
                        Care (Program #200152) and Polysomnography (Program #400152) at Valencia College are
                        based on a 3 year average. 
                     </p>
                     				
                     <h3>Vision</h3>
                     				
                     <p>Respiratory Care is a premier learning program that transforms lives, strengthens
                        the healthcare community and inspires individuals to excellence.
                     </p>
                     				
                     <h3>Mission</h3>
                     				
                     <p>The Valencia Respiratory Care Program is committed to the development of competent,
                        entry-level Respiratory Therapists specializing in the diagnosis and treatment of
                        patients with acute and chronic cardiopulmonary disease.  The program strives to graduate
                        therapists to meet the growing needs of the healthcare community. Valencia’s program
                        provides a strong educational base in therapeutic techniques using online courses,
                        a state-of-the art simulation lab, and hands-on experience at some of the community’s
                        major medical centers and physician offices.  Students will gain diverse clinical
                        experiences through interaction with physicians, adult, pediatric and neonatal patients.
                        The program emphasizes critical thinking, effective communication, professionalism,
                        collaboration within the healthcare team and the pursuit of life long learning. 
                     </p>
                     
                     				
                     <div class="container margin-30">
                        					
                        <div class="main-title">
                           						
                           <h2>Bachelor of Science Degree</h2>
                           					
                        </div>						
                        
                        					
                        <p>Students with a completed Associate degree in Respiratory Care, <a href="/academics/programs/health-sciences/cardiovascular-technology/index.php">Cardiovascular Technology</a>, or Cardiopulmonary Technology may continue their education by pursuing a <a href="/academics/programs/health-sciences/cardiopulmonary-sciences/index.php">Bachelor of Science degree in Cardiopulmonary Sciences</a> at Valencia College. The Cardiopulmonary Sciences professions are challenging and
                           growing professions with career opportunities in areas such ascardiopulmonary diagnostics,
                           cardiopulmonary rehabilitation, community health, and polysomnography. With professional
                           experience and additional education at the baccalaureate level, opportunities for
                           management and education career options are enhanced.
                        </p>
                        				
                     </div>
                     			
                  </div>
                  
                  
                  
                  			
                  <div class="main-title">
                     				
                     <h2>Frequently Asked Questions</h2>
                     				
                     <p>Respiratory Care</p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           						
                           <p>Yes. You must complete the admission process to the college before you submit a Health
                              Information Technology
                              							program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              							email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              							questions. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What courses do I need to take prior to applying?</h3>
                           						
                           <p>Please see the <a href="/documents/academics/programs/health-sciences/respiratory-care-program-guide.pdf">Current Program Guide</a> for detailed information.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Are the prerequisite courses offered online?</h3>
                           						
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available every term.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           						
                           <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                              if accepted, Respiratory Care courses are only offered on the West campus.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           						
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit; equivalent courses will be posted
                              in your Valencia Atlas account.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           						
                           <p>No. The Respiratory Care program does not require the TEAS.</p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the minimum overall GPA to apply to the Respiratory Care program?</h3>
                           						
                           <p>You must have a minimum overall college GPA of 2.0 including all undergraduate transfer
                              work.  Must be in good academic standing.  
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many students are accepted into the program?</h3>
                           						
                           <p>The Respiratory Care program accepts 29 students for the Spring term.</p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do students in the Respiratory Care program conduct peer-to-peer examinations?</h3>
                           						
                           <p>Yes, students in the Respiratory program conduct peer-to-peer exams such as Palpating
                              for pulses at appropriate anatomic landmarks (except femoral pulse which is done on
                              a mannequin), Observe chest wall movement for symmetry, movement, and excursion, Auscultating
                              the chest and back for normal and abnormal breath sounds, Demonstrating therapeutic
                              techniques for breathing, coughing, spirometry, and force.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What are the grooming policies for the Respiratory Care program?</h3>
                           						
                           <p>Hair must be neatly groomed and well kept. Finger nails must be trimmed and no acrylic
                              or fake nails are permissible. Perfumes, colognes and scented lotions are not permitted
                              for patient safety and pulmonary complications. Personal Protective Equipment (PPE)
                              are utilized for a variety of patient care situations when isolation procedures are
                              indicated.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can students accepted into the Respiratory Care program be exposed to blood borne
                              pathogens?
                           </h3>
                           						
                           <p>The Respiratory Care program may have clinical experiences that expose the student
                              to blood borne pathogens via contact with bodily fluids such as blood and saliva.
                              Students accepted into the program will be expected to adhere to Centers for Disease
                              Control (CDC) guidelines for the use of Standard Precautions. The CDC defines standard
                              precautions as "a set of precautions designed to prevent the transmission of HIV,
                              Hepatitis B virus (HBV), other blood borne pathogens when providing first aid or health
                              care.” Under standard precautions, blood and certain body fluids are considered potentially
                              infectious for HIV, HBV, and other blood borne pathogens. Students will be expected
                              to adhere to hand hygiene protocols and consistent use of personal protective equipment
                              including masks, gloves and eyewear. In some of the health sciences programs, students
                              may need to conduct peer-to-peer examinations in lab settings.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           						
                           <p>The Respiratory Care program is a daytime program located on the West campus; its
                              time demands should be considered the equivalent of a fulltime job as in addition
                              to lecture, lab and clinical, students are expected to study between 15-25 hours a
                              week - if a student chooses to work while in the program, a maximum of 15 hours a
                              week is recommended due to the demands of the program. Time management is crucial
                              to successful program completion! The specific schedule can vary from term to term
                              and from year to year.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What are the body art policies for the Respiratory Care program?</h3>
                           						
                           <p>Body art and skin carvings must be concealed and clinical sites may limit the number
                              of ear piercings appropriate based on gender.
                           </p>
                           					
                        </div>
                        				
                     </div>				
                     			
                  </div>		
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How successful are your Respiratory Care graduates in finding employment?</h3>
                           						
                           <p>The five year average Licensure Rate for Respiratory Care graduates is 95% and the
                              five year average Placement Rate for Respiratory Care graduates is 95%.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many clinical hours are in the Respiratory Care program and what credential would
                              I be eligible to sit for upon successful completion?
                           </h3>
                           						
                           <p>Twenty four hours of clinical time is scheduled each week during 3 clinical semesters
                              in 2 twelve hour shifts. Some clinical shifts may vary. Upon completion of the A.S.
                              Degree in Respiratory Care, graduates are eligible to take the Therapists Multiple
                              Choice Examination to earn the Certified Respiratory Therapist (CRT) credential and
                              then, if eligible, take the clinical simulation to earn the Registered Respiratory
                              Therapist (RRT) credential.
                           </p>
                           					
                           <p>Specialty credentials are also available, for those who qualify, that include the
                              Advanced Critical Care Specialty (ACCS), Certified Pulmonary Function Technologist
                              (CPFT), Registered Pulmonary Function Technologist (RPFT), Neonatal/Pediatric Specialty
                              (NPS) and Sleep Disorder Specialist (SDS).
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>	
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How do I apply?</h3>
                           						
                           <p>Download &amp; fill out the Health Information Technology Application. Return your completed
                              application to
                              							Health Sciences Advising along with a non-refundable Health Sciences program
                              application fee of $15
                           </p>
                           						
                           <ul>
                              							
                              <li>
                                 								
                                 <h4>By mail:</h4>
                                 								Send the application along with a check or money order payable to Valencia
                                 College to
                                 								the address listed below:
                                 								
                                 <address class="wrapper_indent">
                                    									Valencia College<br>
                                    									Business Office<br>
                                    									P.O. Box 4913.<br>
                                    									Orlando, FL
                                    									32802
                                    								
                                 </address>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h4>In person:</h4>
                                 								Make payment in the Business Office on any Valencia Campus &amp; request that
                                 the
                                 								Business Office forward your program application &amp; fee receipt to the Health
                                 Sciences Advising office at
                                 								mail code 4-30 
                              </li>
                              						
                           </ul>
                           						
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              							documentation or requirements can result in denied admission. All items listed
                              on the <a href="http://valenciacollege.edu/west/health/documents/HealthInformationTechnology.pdf">Program Guide
                                 							Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/respiratory-care/index.pcf">©</a>
      </div>
   </body>
</html>