<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Cardiopulmonary Sciences  | Valencia College</title>
      <meta name="Description" content="Cardiopulmonary Sciences | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/cardiopulmonary-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/cardiopulmonary-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Cardiopulmonary Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Cardiopulmonary Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Cardiopulmonary Sciences</h2>
                     
                     <p>Bachelor of Science (B.S.) Degree</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Cardiopulmonary Sciences Bachelor of Science (B.S.) degree at Valencia College
                        provides an opportunity for A.S. degree Respiratory Therapists (RRT) and Cardiovascular
                        Technologists (RCIS or RCES) to further their education and increase their potential
                        for career advancement. The curriculum offers advanced courses in the cardiopulmonary
                        system, the health care system and an introduction to research, leadership and education.
                        The majority of the program is presented in an online format, which allows for a great
                        deal of flexibility for the working professional. The cardiac ultrasound labs are
                        held on the West Campus. Valencia is the only school in Central Florida to offer this
                        B.S. degree.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobscardiopulmonarysciences/#programrequirementstext"> <span class="far fa-laptop"></span>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br><br><br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/cardiopulmonary-sciences-program-guide.pdf"> <span class="far fa-calendar"></span>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 2-4 years. You can complete the entire B.S. degree program in as
                              little as four years. Or, if you already have an eligible associate degree in the
                              field, you can complete your upper-division studies in as little as two years.
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  <!--End row-->
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4 box_feat"><span class="far fa-share-alt fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Degrees that Transfer to this Bachelor's</h3>
                        
                        <ul>
                           		  
                           <li><a href="/academics/programs/health-sciences/respiratory-care/index.php">Respiratory Care</a></li>
                           		  
                           <li><a href="/academics/programs/health-sciences/cardiovascular-technology/index.php">Cardiovascular Technology</a></li>
                           	  
                        </ul><br></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           
                           <h3>Average Salary</h3>
                           
                           <p>Cardiopulmonary Sciences average salaries ranges from $47,000 – $65,000 ($22.60 –
                              $31.25 per hour)
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     
                  </div>
                  
                  <!--End row--> 
                  
                  
                  
               </div>
               
               
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Cardiopulmonary Sciences </h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="/documents/academics/programs/health-sciences/cardiopulmonary-sciences-program-guide.pdf">Current Program Guide</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobscardiopulmonarysciences/#programrequirementstext">Current Program Requirements</a></li>
                              
                              <li><a href="#">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li><strong>Application Deadline:</strong> No Deadline
                              </li>
                              
                              <li><strong>Begins:</strong> Fall, Spring, Summer
                              </li>
                              			
                              <li><strong>Application:</strong> <a href="/documents/academics/programs/health-sciences/Application-BSCARDIO-and-ATC.pdf">Download Cardiopulmonary Application</a></li>
                              
                           </ul>
                           <br>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        		
                        <div class="row">
                           		
                           <h3>Program Changes</h3>
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <tr>
                                 
                                 <th scope="col">Date Updated</th>
                                 
                                 <th scope="col">Effective Date</th>
                                 
                                 <th scope="col">Change</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="3">No Changes at this time</td>
                                 
                              </tr>
                              
                              
                           </table>
                           		
                        </div>
                        		
                        <div class="row">
                           			
                           <h3>Cardiopulmonary Department</h3>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                                 			
                                 <h4>Jamy Chulak, M.S., RRT</h4>
                                 
                                 <p>Dean of Allied Health</p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/SShenton.jpg" alt="Sharon Shenton Photo" class="img-circle"></figure>
                                 			
                                 <h4>Sharon Shenton</h4>
                                 
                                 <p>Chair of Cardiopulmonary Sciences Program</p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                                 
                                 <h4>Health Sciences Advising</h4>
                                 
                                 <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                                 
                              </li>
                              
                           </ul>
                           		
                        </div>
                        		
                        <div class="row">
                           		 
                           <div class="col-md-6">
                              				
                              <h3>Job Outlook</h3>
                              
                              <p>By the year 2018, employment is expected to grow by 21 percent for respiratory therapists
                                 and 24 percent for cardiovascular technologists.
                              </p>
                              
                              
                              <p>More and more hospitals and medical facilities are requiring cardiopulmonary and respiratory
                                 care practitioners to hold bachelor’s degrees. Completing this program will open many
                                 doors for advancement in your professional career. In an advanced position within
                                 the cardiopulmonary sciences, you will manage high-tech equipment and oversee technologists
                                 and the procedures they perform.
                              </p>
                              			
                           </div>
                           			
                           <div class="col-md-6">
                              				
                              <h3>Potential Careers</h3>
                              				
                              <ul>
                                 					
                                 <li>Respiratory Care Management/Administration</li>
                                 
                                 <li>Cardiovascular Management/Administration</li>
                                 
                                 <li>Higher Education</li>
                                 
                                 <li>Community Health Educator</li>
                                 
                                 <li>Clinical Practice in Non-Invasive Cardiology</li>
                                 				
                              </ul>
                              
                              			
                           </div>
                           			
                           	
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>7 Steps to Your Bachelor's Degree</h4>
                           
                           <ul class="list_order">
                              
                              <li><span>1</span>Complete your associate degree in one of three eligible fields (including all prerequisites):
                                 			  
                                 <div class="indent_title_in"><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a><br>
                                    				  <a href="/academics/departments/health-sciences/respiratory-care/">Respiratory Care</a></div>
                              </li>
                              
                              <li><span>2</span>Hold professional certification in your respective discipline.
                              </li>
                              
                              <li><span>3</span>Submit the online Valencia College application for the bachelor’s program, as well
                                 as all college transcripts, and pay the $35 application fee. This fee is waived for
                                 active Valencia College students who hold an Associate in Arts degree from Valencia
                                 or an Associate in Science degree from Valencia to a bachelor degree at Valencia.
                              </li>
                              
                              <li><span>4</span>Once you’ve been accepted to Valencia College as a candidate for B.S. Cardio, submit
                                 the Health Sciences B.S. Cardio application, along with your professional certification,
                                 and contact the program advisor.
                              </li>
                              			
                              <li><span>5</span>Once you’ve been accepted to the B.S. degree, complete the online bachelor’s orientation.
                              </li>
                              
                              			
                              <li><span>6</span>Register for classes.
                              </li>
                              
                              			
                              <li><span>7</span>Graduate with your B.S. degree. 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <!--End row --> 
                  
                  <hr class="styled_2">
                  	
                  	
                  <div class="row">
                     			
                     <h3>Lower Division Coursework</h3>
                     
                     
                     <p>To fulfill the lower-division (first half) of the Cardiopulmonary Sciences bachelor’s
                        degree, you must complete an Associate in Science (A.S.) degree from a regionally
                        accredited institution in one of the following areas.
                     </p>
                     
                     			
                     <h4>Choose from Two A.S. Degrees:</h4>
                     
                     			
                     <h5><a href="/academics/departments/health-sciences/respiratory-care/">A.S. in Respiratory Care</a></h5>
                     
                     <p>A respiratory therapist’s main function is to assist physicians in treating patients
                        who experience heart-lung problems that interfere with breathing, such as chronic
                        asthma or emphysema. You’ll work to restore the heart-lung system to normal function
                        using therapeutic techniques and state-of-the-art equipment.
                     </p>
                     
                     			
                     <h5><a href="/academics/programs/health-sciences/cardiovascular-technology/">A.S. in Cardiovascular Technology</a></h5>
                     
                     <p>Cardiovascular technologists work side-by-side with cardiologists to diagnose and
                        treat heart disease. You’ll use the latest medical technology to conduct tests on
                        pulmonary or cardiovascular systems of patients and assist in electrocardiograms,
                        cardiac catheterizations and similar tests.
                        
                     </p>
                     
                     <p>(Valencia will also accept a completed A.S. degree in Cardiopulmonary Technology,
                        offered by some other institutions, for admission into the upper-division coursework.)
                     </p>
                     
                     <h3>Upper Division Coursework</h3>
                     
                     <p>
                        The BS degree includes 40 credits of upper division course work, including 27 credits
                        of Cardiopulmonary Core Requirements, 3 credits of Cardiopulmonary Sciences Electives
                        and a 10 credit concentration.
                     </p>
                     
                     
                     <p>After completing a qualifying lower-division associate degree, you may move on to
                        the upper-division (second half) of the Cardiopulmonary Sciences bachelor’s degree.
                        Students choose from a concentration in Community Health or Non-Invasive Cardiology,
                        as well as from electives in Teaching in the Health Professions, Principles of Human
                        Resource Management and Administration and Supervision.
                     </p>
                     
                     			
                     <h4>Concentrations</h4>
                     			
                     <div class="col-md-4">
                        				
                        <h5>Community Health</h5>
                        
                        <p>This concentration will prepare you to provide health education to help people within
                           the community manage acute and chronic cardiopulmonary illness. You’ll gain an understanding
                           of clinical approaches for prevention and treatment of chronic illness, as well as
                           an overview of the public health system. You’ll apply this knowledge to work within
                           acute care facilities, long term care, outpatient centers and homecare environments.	
                        </p>
                        			
                     </div>
                     			
                     <div class="col-md-4">
                        				
                        <h5>Leadership</h5>
                        
                        <p>The concentration provides specialized course work for those aspiring to, or already
                           in, leadership positions within their respective health professions. You will gain
                           knowledge and skills related to healthcare administrative and supervisory duties which
                           will enable you to more effectively perform leadership roles within healthcare.
                        </p>	
                        			
                     </div>
                     			
                     <div class="col-md-4">
                        				
                        <h5>Non-Invasive Cardiology</h5>
                        
                        <p>From diagnosis to treatment, you’ll focus on managing cardiovascular disease from
                           the patient perspective. Learn basic techniques of noninvasive ultrasound modalities
                           used to evaluate disorders of cardiac circulation, cardiac valves and myocardium,
                           pre-and post-procedure care and therapeutic measures to treat cardiovascular diseases.
                           The Non-Invasive Cardiology concentration also includes a lab and clinical practice
                           component, the labs are held on West campus.
                        </p>
                        			
                     </div>
                     
                     
                     
                  </div>
                  		
               </div>
               	
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/cardiopulmonary-sciences/index.pcf">©</a>
      </div>
   </body>
</html>