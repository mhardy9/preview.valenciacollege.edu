<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Medical Information Coder/Biller | Valencia College</title>
      <meta name="Description" content="Medical Information Coder/Biller| Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/medical-info-coder/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/medical-info-coder/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Medical Information Coder/Biller</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Medical Information Coder/Biller</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     			
                     <h2>Medical Information Coder/Biller</h2>
                     			
                     <p>Technical Certificate</p>
                     		
                  </div>
                  		
                  <div class="row box_style_1">
                     			
                     <p>Program Information Here</p>
                     
                     
                     			
                     <div class="container margin-60 box_style_1">
                        				
                        <div class="main-title">
                           					
                           <h2>Admission into Medical Information Coder/Biller</h2>
                           					
                           <p>&nbsp;</p>
                           				
                        </div>
                        				
                        <div class="row">
                           					
                           <div class="col-md-6 col-sm-6">
                              						
                              <div class="box_style_4">
                                 							
                                 <h3>Important Information</h3>
                                 							
                                 <ul>
                                    								
                                    <li><a href="/documents/academics/programs/health-sciences/medical-information-coder-biller-program-guide.pdf">Current Program Guide</a></li>
                                    								
                                    <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                                    							
                                 </ul>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <div class="col-md-6 col-sm-6">
                              						
                              <div class="box_style_4">
                                 							
                                 <h3>Application Information</h3>
                                 							
                                 <ul>
                                    								
                                    <li><strong>Application Deadline:</strong> Classes begin May 7, 2018
                                    </li>
                                    								
                                    <li><strong>Begins:</strong> Summer 2018
                                    </li>
                                    								
                                    <li><strong>Application:</strong> Summer Intent to Register form available in March 2018.
                                    </li>
                                    							
                                 </ul>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="row">
                           					
                           <div class="col-md-8 col-sm-8">
                              						
                              <h3>Program Changes</h3>
                              						
                              <table class="table table table-striped cart-list add_bottom_30">
                                 							
                                 <tbody>
                                    								
                                    <tr>
                                       									
                                       <th scope="col">Date Updated</th>
                                       									
                                       <th scope="col">Effective Date</th>
                                       									
                                       <th scope="col">Change</th>
                                       								
                                    </tr>
                                    								
                                    <tr>
                                       									
                                       <td colspan="3">There are currently no program changes</td>
                                       								
                                    </tr>
                                    							
                                 </tbody>
                                 						
                              </table>
                              						
                              <ul class="list_staff">
                                 							
                                 <li>
                                    								
                                    <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                                    								
                                    <h4>Jamy Chulak, M.S., RRT</h4>
                                    								
                                    <p>Dean of Allied Health</p>
                                    							
                                 </li>
                                 							
                                 <li>
                                    								
                                    <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available"></figure>
                                    								
                                    <h4>Female Staff Member Sample</h4>
                                    								
                                    <p>Staff role</p>
                                    							
                                 </li>
                                 							
                                 <li>
                                    								
                                    <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available"></figure>
                                    								
                                    <h4>Male Staff Member Sample</h4>
                                    								
                                    <p>Staff Role</p>
                                    							
                                 </li>
                                 						
                              </ul>
                              					
                           </div>
                           					
                           <div class="col-md-4 col-sm-4">
                              						
                              <div class="box_style_4">
                                 							
                                 <h4>Steps for Admission to Program</h4>
                                 							
                                 <ul class="list_order">
                                    								
                                    <li><span>1</span></li>
                                    								
                                    <li><span>2</span></li>
                                    								
                                    <li><span>3</span></li>
                                    								
                                    <li><span>4</span></li>
                                    							
                                 </ul>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <!--End row -->
                     </div>
                     			
                     <!--End container --> <a id="faq"></a>
                     			
                     <div class="main-title">
                        				
                        <h2>Frequently Asked Questions</h2>
                        				
                        <p>Medical Information Coder/Biller</p>
                        			
                     </div>
                     			
                     <div class="row">
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>Do I need to be a student at Valencia to apply?</h3>
                              						
                              <p>Yes. You must complete the admission process to the college before you submit the
                                 required Intent to Register form in order to register for the program courses. You
                                 can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any questions.
                              </p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>Can Dual Enrollment students participate in the T.C. Medical Information Coder Biller
                                 program?
                              </h3>
                              						
                              <p>No. You must be a high school graduate with either a Standard High School Diploma
                                 or GED to be eligible.
                              </p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>What courses do I need to take prior to applying?</h3>
                              						
                              <p>The program is open-entry with no prerequisite. </p>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     			
                     <div class="row">
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>Will my courses from other colleges transfer to Valencia?</h3>
                              						
                              <p>Once you have applied to Valencia, you will need to request official transcripts sent
                                 from your previous institution. The admissions office will evaluate all transcripts
                                 once they are received to determine transfer credit ; equivalent courses will be posted
                                 in your Valencia Atlas account .
                              </p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I register for
                                 the program courses?
                              </h3>
                              						
                              <p>No. The T.C. Medical Information Coder Biller program does not require the TEAS.</p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="row">
                           					
                           <div class="col-md-4">
                              						
                              <div class="box_style_2">
                                 							
                                 <h3>What is the minimum overall GPA to register for the program courses?</h3>
                                 							
                                 <p>You must be in ‘Good Standing’ which requires a minimum 2.0 overall GPA.</p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>What is the deadline to register for the program classes?</h3>
                              						
                              <p>If seats are still available, it is possible to register up until just before the
                                 first day of class. However, classes fill up quickly, so early submission of the Intent
                                 to Register form is highly recommended.
                              </p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>How will I know what the available program courses are for each term?</h3>
                              						
                              <p>Course offerings are listed on each new term’s Intent to Register form. Summer 2018
                                 Intent to Register form will be available in March 2018.
                              </p>
                              					
                           </div>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>Do students in the T.C. Medical Information Coder Biller program conduct peer-to-peer
                                 examinations?
                              </h3>
                              						
                              <p>No. The T.C. Medical Information Coder Biller program is an online program.</p>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     			
                     <div class="row">
                        				
                        <div class="col-md-4">
                           					
                           <div class="box_style_2">
                              						
                              <h3>What is the schedule like if I participate in the T.C. Medical Information Coder Biller
                                 program?
                              </h3>
                              						
                              <p>The T.C. Medical Information Coder Biller program is fully online, including a simulated
                                 professional practice
                              </p>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/medical-info-coder/index.pcf">©</a>
      </div>
   </body>
</html>