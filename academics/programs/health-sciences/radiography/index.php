<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Radiography  | Valencia College</title>
      <meta name="Description" content="Radiography | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/radiography/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/radiography/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Radiography</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Radiography</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Radiography</h2>
                     				
                     <p>Type of Program (AS/AA/BS/Cert)</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The Radiography Associate in Science (A.S.) degree at Valencia College is a two-year
                        program that prepares you to go directly into a specialized career as a radiographer.
                     </p>
                     				
                     <p>If a picture is worth a thousand words, imagine the value of clear images generated
                        by high-tech medical equipment for a person in need of medical care. And imagine the
                        value that someone trained in the use of such equipment brings to a medical team.
                        The radiographer is an integral member of the medical team. Radiographers operate
                        high-tech imaging equipment and perform diagnostic imaging examinations such as X-ray’s,
                        CAT scans, MRI’s and mammograms, that help doctors accurately diagnose and treat their
                        patients for injury and disease. Valencia’s A.S. degree in Radiography prepares students
                        to become highly skilled professionals in the safe and effective use of radiation
                        to treat patients.
                     </p>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/radiography/#programrequirementstext"> <span class="far fa-laptop"></span>
                           					
                           <h3>Program Overview</h3>
                           					
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              						<br><br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/radiography-program-guide.pdf"> <span class="far fa-calendar"></span>
                           					
                           <h3>Program Length</h3>
                           					
                           <p>Program lasts for 6 semesters after all of the pre-admission course requirements have
                              been met.<br><br><br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           					
                           <h3>Want more information?</h3>
                           					
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              					
                           </p>
                           					</a></div>
                     			
                  </div>
                  			
                  <!--End row-->
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4 box_feat"><span class="far fa-share-alt fa-5x v-red"></span>
                        					
                        <h3 class="h4 text-uppercase">Related Certificates</h3>
                        					
                        <p>Once you complete your A.S. degree, you can continue get an Advanced Technical Certificate
                           (ATC) in the following areas:
                        </p>
                        					
                        <ul>
                           						
                           <li><a href="/academics/programs/health-sciences/computed-tomography/index.php">Computed Tomography (CT)</a></li>
                           						
                           <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/index.php">Magnetic Resonance Imaging (MRI)</a></li>
                           					
                        </ul><br></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           					
                           <h3>Average Salary</h3>
                           					
                           <p>Radiography average salaries range from $35,000 – $40,000<br><br><br>
                              						<br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           					
                           <h3>Career Coach</h3>
                           					
                           <p>Explore Health Science related careers, salaries and job listings. <br><br><br>
                              						<br>
                              					
                           </p>
                           					</a> 
                     </div>
                     
                     			
                  </div>
                  			
                  <!--End row--> 
                  
                  
                  		
               </div>
               
               		
               <div class="container margin-30 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Radiography </h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/radiography-program-guide.pdf">Current Program Guide</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/radiography-estimated-costs.pdf">Estimated Program Costs</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/radiography-course-planning-guide.pdf">Course Planning Guide</a></li>
                              							
                              <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/radiography/#programrequirementstext">Current Program Requirements</a></li>
                              							
                              <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li>Application Deadline: January 15, 2019</li>
                              							
                              <li>Begins: Summer 2019</li>
                              							
                              <li>Application: Download should be available November 2018</li>
                              						
                           </ul><br><br>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <h3>Program Changes</h3>
                        					
                        <table class="table table table-striped cart-list add_bottom_30">
                           						
                           <tr>
                              							
                              <th scope="col">Date Updated</th>
                              							
                              <th scope="col">Effective Date</th>
                              							
                              <th scope="col">Change</th>
                              						
                           </tr>  
                           						
                           <tr>
                              							
                              <td>3/20/17</td>
                              							
                              <td>Fall 2017</td>
                              							
                              <td>International Students are eligible to apply for this
                                 								Health Sciences Program. <a href="/academics/departments/health-sciences/international-student-eligibility.php">Click here for details.</a></td>
                              						
                           </tr> 
                           						
                           <tr>
                              							
                              <td>3/6/17</td>
                              							
                              <td>Fall 2017</td>
                              							
                              <td>
                                 <p>The overall GPA Admission Points will not be in consideration for the total admission</p>
                                 								
                                 <p>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1<br>
                                    									HSC1531 (Medical Terminology) points increased: A: 8, B: 5, C: 3. Note: (SLS
                                    1122 points will remain the same--A: 3, B:2, C: 1)
                                 </p>
                                 
                                 							
                              </td>
                              						
                           </tr>  
                           						
                           <tr>
                              							
                              <td>8/4/16 (posted on website)</td>
                              							
                              <td>9/15/17</td>
                              							
                              <td>
                                 <p>TEAS for program entry after September 15, 2017 must submit the new version: ATI TEAS.
                                    The minimum composition score of *58.7% is required.&nbsp; Scores are valid for 5 years.
                                    *Subject to change.&nbsp; You must meet required score in effect at the time you apply
                                    to the program.&nbsp; 
                                 </p>
                                 								
                                 <p>TEAS information is available at <a href="/students/assessments/test-of-essential-academic-skills/index.php">valenciacollege.edu/assessments </a>and <a href="https://www.atitesting.com/Home.aspx">ATItesting.com </a></p>
                              </td>
                              						
                           </tr>
                           
                           						
                           <tr>
                              							
                              <td>4/20/15</td>
                              							
                              <td>For the 1/15/2017 application deadline to begin program in May 2017 </td>
                              							
                              <td>MAC 1105 College Algebra and HSC 1531 Medical Terminology must be completed with 
                                 a minimum grade of C by the application deadline.
                              </td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <td>9/4/14 (posted on website) </td>
                              							
                              <td>12/10</td>
                              							
                              <td>
                                 <p>TEAS Version 5 (V) minimum Composite score of *58.7% required.  TEAS scores are valid
                                    for 5 years. *Subject to change. You must meet required score in effect at the time
                                    you apply to the program. 
                                 </p>
                                 								
                                 <p> TEAS information is available at <a href="/students/assessments/test-of-essential-academic-skills/index.php">valenciacollege.edu/assessments </a>and &nbsp;<a href="https://www.atitesting.com/Home.aspx">ATItesting.com&nbsp; </a></p>
                                 								
                                 <p>&nbsp;</p>
                              </td>
                              						
                           </tr>
                           					
                        </table>
                        
                        					
                        <ul class="list_staff">
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              							
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              							
                              <p>Dean of Allied Health</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/beverly-bond.jpg" alt="Beverly Bond Photo" class="img-circle"></figure>
                              							
                              <h4>Beverly Bond, M.Ed, RT(R)</h4>
                              							
                              <p>Program Chair, Radiography</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/julie-kloft.jpg" alt="Julie Kloft Photo" class="img-circle"></figure>
                              							
                              <h4>Julie Kloft, MSRS., RT (R)</h4>
                              							
                              <p>Clinical Coordinator, Radiography</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/billie-jo-gioioso.jpg" alt="Billie Jo Gioioso Photo" class="img-circle"></figure>
                              							
                              <h4>Billie Jo Gioioso, A.S., RT(R) (CT)</h4>
                              							
                              <p>Adjunct Clinical Professor</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/joan-gibson.jpg" alt="Joan Gibson Photo" class="img-circle"></figure>
                              							
                              <h4>Joan Gibson, RT(R)</h4>
                              							
                              <p>Adjunct Clinical Professor</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/victor-bernardi.jpg" alt="Victor Bernardi Photo" class="img-circle"></figure>
                              							
                              <h4>Victor Bernardi, RT(R)</h4>
                              							
                              <p>Adjunct Didactic and Clinical Professor</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/trina-sloan.jpg" alt="Trina Sloan Photo" class="img-circle"></figure>
                              							
                              <h4>Trina Sloan, RT(R)</h4>
                              							
                              <p>Adjunct Didactic and Clinical Professor</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/images/academics/programs/health-sciences/james-mauzy.jpg" alt="James Mauzy Photo" class="img-circle"></figure>
                              							
                              <h4>James Mauzy, BS RT(R)</h4>
                              							
                              <p>Radiography Adjunct Professor</p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <figure><img src="/_resources/img/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                              							
                              <h4>Health Sciences Advising</h4>
                              							
                              <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                              						
                           </li>
                           					
                        </ul>
                        				
                     </div>		
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>Steps for Admission to Program</h4>
                           						
                           <ul class="list_order">
                              							
                              <li><span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    								Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 								are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                    								Questions</a>. 
                              </li>
                              							
                              <li><span>2</span><a href="#">Apply</a> to Valencia College. 
                              </li>
                              							
                              <li><span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              							
                              <li><span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 								requirements, you can apply to the program. 
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <!--End row --> 
                  		
               </div>
               		
               <div class="container margin-30 box_style_1">
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <h3>Learn How To</h3>
                        					
                        <ul>
                           						
                           <li>Apply knowledge of human anatomy to position the patient’s body so that the correct
                              areas can be radiographed
                           </li>
                           						
                           <li>Produce quality radiographs (X-rays) for use in diagnosing and treating medical problems</li>
                           						
                           <li>Protect patients and self from radiation</li>
                           						
                           <li> Provide respectful care to patients	</li>	   
                           					
                        </ul>
                        
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <h3>Job Outlook</h3>
                        					
                        <p>Radiographers are employed primarily in hospitals. Career opportunities are also available
                           in radiological/imaging centers, urgent care clinics, private physicians’ offices,
                           and public health service facilities. Employment within the radiologic profession
                           is projected to grow faster than the average for all occupations. As the population
                           grows older, there will be an increase in medical conditions, such as breaks and fractures
                           caused by osteoporosis, which usually requires imaging to diagnose them.
                        </p>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <h3>Potential Careers</h3>
                        					
                        <ul>
                           						
                           <li>Radiographer</li>
                           						
                           <li>Radiologic Technologist</li>
                           					
                        </ul>
                        
                        				
                     </div>
                     
                     			
                  </div>
                  			
                  <div class="container margin-30 box_style_1">
                     				
                     <div class="main-title">
                        					
                        <h2>General Radiography Program Information</h2>
                        				
                     </div>
                     				
                     <p>The Valencia Radiography Program is the current continuation of the School of Radiologic
                        Technology that originated at Orange Memorial Hospital in 1954 under the leadership
                        of Dr. Robert Curry, the first radiologist at the hospital.  During the mid-1980s,
                        Orlando Regional Medical Center (ORMC) affiliated with Valencia to allow diploma graduates
                        to obtain an AS degree and it became a college credit degree program in 1990 co-sponsored
                        by Valencia and ORMC.  Valencia became the program’s sponsor in 1998 with financial
                        support from Orlando Regional Healthcare (ORH) and relocated to Valencia’s West Campus.
                        The radiologists of Medical Center Radiology Group (MCRG) continue to be a strong
                        support for the program.  A 50th reunion was held on June 30, 2007, to celebrate the
                        program’s 565 graduates which have provided a continuous supply of competent, qualified
                        radiographers to the central Florida community.
                     </p>
                     
                     				
                     <p>The program currently accepts 25 students, once a year, beginning in May.  Application
                        deadline is January 15th of each year.  Class size is limited by clinical space availability
                        so that we can guarantee our students will graduate with optimal technical skills.
                        We are very proud to affiliate with:  Arnold Palmer Hospital for Children, Boston
                        Diagnostic Imaging, Jewett Orthopaedic Clinic, Kissimmee Outpatient Center, ORH Lucerne
                        Hospital, ORH Orlando Regional Medical Center, ORH Orthopaedic Faculty Practice, ORH
                        Dr. P. Phillips Hospital, ORH South Lake Hospital, Osceola Regional Medical Center,
                        St. Cloud Regional Medical Center, and the Veterans’ Administration Outpatient Clinic.
                     </p>
                     
                     				
                     <p>The most recent addition to the Radiography Department is Advanced Technical Certificate
                        Programs for <a href="/academics/programs/health-sciences/computed-tomography/index.php">Computed Tomography</a> and <a href="/academics/programs/health-sciences/magnetic-resonance-imaging/index.php">Magnetic Resonance Imaging</a>.  These programs are available to credentialed radiographers, nuclear medicine technologists,
                        radiation therapists and sonographers.
                     </p>
                     
                     				
                     <h3>Accreditation</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-10">
                           <p>The program is accredited by the Southern Association of Colleges and Secondary Schools
                              (SACS) and the <a href="http://www.jrcert.org/">Joint Review Committee on Education in Radiologic Technology</a> (JRCERT), thus qualifying our graduates to take the national registry examination
                              offered by the American Registry of Radiologic Technologists.  On successfully passing
                              the registry exam, graduates are eligible for a Florida general radiographer’s license.
                              You can find <a href="http://www.jrcert.org/resources/program-effectiveness-data">JCERT's Program Effectiveness Data here</a>.
                           </p> 
                           					
                        </div>
                        					
                        <div class="col-md-2">
                           						
                           <p>JRCERT<br>
                              							20 N. Wacker Drive<br>
                              							Suite 2850<br>
                              							Chicago, IL 60606<br>
                              							ph: 312-704-5300<br>
                              							fax: 312-704-5304<br>
                              							<a href="mailto:mail@jrcert.org">mail@jrcert.org</a><br>
                              							<a href="http://www.jcert.org">http://www.jcert.org</a></p>	
                           					
                        </div>
                        				
                     </div>
                     				
                     <h4>Valencia's Radiography Program Effectiveness Data </h4>
                     				
                     <table class="table table">
                        					
                        <tbody>
                           						
                           <tr>
                              							
                              <th scope="col">Year</th>
                              							
                              <th scope="col">Pass rate on 1st attempt</th>
                              							
                              <th scope="col">Job place within 6 months of graduation</th>
                              							
                              <th scope="col">Completion rate</th>
                              							
                              <th scope="col">Sample size</th>
                              							
                              <th scope="col">1st time examinees taking</th>
                              							
                              <th scope="col">1st time examinees taking</th>
                              							
                              <th scope="col">Grads seeking employ</th>
                              							
                              <th scope="col">Grads finding employ</th>
                              							
                              <th scope="col">Students initially enrolled</th>
                              							
                              <th scope="col">Student Grads</th>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <th scope="row">2012</th>
                              							
                              <td>100%</td>
                              							
                              <td>81%</td>
                              							
                              <td>84%</td>
                              							
                              <td>16</td>
                              							
                              <td>16</td>
                              							
                              <td>16</td>
                              							
                              <td>16</td>
                              							
                              <td>12</td>
                              							
                              <td>19</td>
                              							
                              <td>16</td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <th scope="row">2013</th>
                              							
                              <td>100%</td>
                              							
                              <td>89%</td>
                              							
                              <td>79%</td>
                              							
                              <td>19</td>
                              							
                              <td>19</td>
                              							
                              <td>19</td>
                              							
                              <td>18</td>
                              							
                              <td>15</td>
                              							
                              <td>24</td>
                              							
                              <td>19</td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <th scope="row">2014</th>
                              							
                              <td>100%</td>
                              							
                              <td>85.7%</td>
                              							
                              <td>87.5%</td>
                              							
                              <td>21</td>
                              							
                              <td>21</td>
                              							
                              <td>21</td>
                              							
                              <td>21</td>
                              							
                              <td>18</td>
                              							
                              <td>24</td>
                              							
                              <td>21</td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <th scope="row">2015</th>
                              							
                              <td>100%</td>
                              							
                              <td>90%</td>
                              							
                              <td>83.3%</td>
                              							
                              <td>20</td>
                              							
                              <td>20</td>
                              							
                              <td>20</td>
                              							
                              <td>20</td>
                              							
                              <td>18</td>
                              							
                              <td>24</td>
                              							
                              <td>20</td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <th scope="row">2016</th>
                              							
                              <td>100%</td>
                              							
                              <td>83.3%</td>
                              							
                              <td>75%</td>
                              							
                              <td>18</td>
                              							
                              <td>18</td>
                              							
                              <td>18</td>
                              							
                              <td>18</td>
                              							
                              <td>15</td>
                              							
                              <td>24</td>
                              							
                              <td>18</td>
                              						
                           </tr>
                           					
                        </tbody>
                        				
                     </table>
                     
                     				
                     <h3>Mission</h3>						
                     				
                     <p>The Valencia Radiography Program is committed to the development of competent, entry-level
                        radiographers.
                     </p>				
                     				
                     <h3>Goals and Student Learning Outcomes</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-3">
                           						
                           <h4>Students will be clinically competent</h4>
                           						
                           <ul>
                              							
                              <li>Students will practice radiation protection.</li>
                              							
                              <li>Students/Graduates will pass the certification exam.</li>
                              							
                              <li>Students/Graduates will be adequately prepared to perform as entry-level practitioners.</li>
                              							
                              <li>Students will select appropriate technical factors.</li>
                              							
                              <li>Students/Graduates will apply positioning skills.</li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-3">
                           						
                           <h4>Students will be able to communicate</h4>
                           						
                           <ul>
                              							
                              <li>Students will verify patient’s identity.</li>
                              							
                              <li>Student will be able to obtain patient’s clinical history.</li>
                              							
                              <li>Student will be able to explain the procedure to the patient.</li>
                              							
                              <li>Student will inquire about pregnancy.</li>
                              							
                              <li>Student will be able to communicate with the physician and healthcare team with oral
                                 and written summary of procedure.
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-3">
                           						
                           <h4>Students will evaluate the importance of professional growth &amp; development</h4>
                           						
                           <ul>
                              							
                              <li>Students will arrive to clinical site on time.</li>
                              							
                              <li>Students will report to supervisor.</li>
                              							
                              <li>Students are prepared to start work on time and equipped with necessary tools (markers,
                                 badge, etc).
                              </li>
                              							
                              <li>Students accept constructive criticism and make changes as needed.</li>
                              							
                              <li>Students regard authority and are respectful.</li>
                              							
                              <li>Students will display professional conduct, language and mannerisms while in the clinical
                                 site.
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-3">
                           						
                           <h4>Students will use critical thinking &amp; problem-solving skills.</h4>
                           						
                           <ul>
                              							
                              <li>Student is adaptable to new and unfamiliar situations.</li>
                              							
                              <li>Student will perform one trauma lower extremity competency.</li>
                              							
                              <li>Student will perform one trauma upper extremity competency.</li>
                              						
                           </ul>
                           					
                        </div>
                        
                        				
                     </div>
                     				
                     <div class="container margin-30">
                        					
                        <div class="main-title">
                           						
                           <h2>Bachelor of Science Degree</h2>
                           					
                        </div>						
                        
                        					
                        <p>Students with a completed AS degree in Radiography, Sonography, Nuclear Medicine Technology
                           or Radiation Therapy may continue their education by pursuing a <a href="/academics/programs/health-sciences/radiologic-imaging-sciences/index.php">Bachelor of Science degree in Radiologic &amp; Imaging Sciences</a> at Valencia College. The BS degree is recognized by the American Society of Radiologic
                           Technology as the professional level of radiologic science education, and opens many
                           opportunities for leadership positions within the imaging field. These include administration
                           and supervision, education, and clinical skills in the advanced imaging modalities
                           such as CT, MRI and QM.
                        </p>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="main-title"><a id="faq"></a>
                     				
                     <h2>Frequently Asked Questions</h2>
                     				
                     <p>Radiography</p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           						
                           <p>Yes. You must complete the admission process to the college before you submit a Radiography
                              							program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              							email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              							questions. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Are the prerequisite courses offered online?</h3>
                           						
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available every term. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What courses do I need to take prior to applying?</h3>
                           						
                           <p></p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           						
                           <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                              if accepted, Radiography courses are only offered on the West campus.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           						
                           <p>Once you have applied to Valencia, you will need to request official transcripts be
                              sent from your previous institutions. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit; equivalent courses will be posted
                              in your Valencia Atlas account.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           						
                           <p>Yes, you must complete the ATI TEAS. The examination fee is your responsibility. <a href="/students/assessments/test-of-essential-academic-skills/index.php">More information on the TEAS</a>
                              							.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can I use TEAS scores from another institution?</h3>
                           						
                           <p>Yes. Students who complete the TEAS at another institution must request an official
                              score report from ATI as a paper copy is not accepted. Requests can be made online
                              via <a href="http://www.atitesting.com">www.atitesting.com</a></p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the minimum overall GPA to apply to the Radiography program?</h3>
                           						
                           <p></p>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the deadline to apply to the Radiography program?</h3>
                           						
                           <p></p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many students are accepted into the program?</h3>
                           						
                           <p>The Radiography program accepts 25 students each Summer term.</p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do students in the Radiography program conduct peer-to-peer examinations?</h3>
                           						
                           <p>Yes, students in the Radiography program conduct peer-to-peer exams such as Vital
                              signs, positioning of all procedures during lab testing (one student is the patient
                              and the other student is the radiographer).
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What are the body art policies for the Radiography program</h3>
                           						
                           <p>All visible tattoos,to include skin carving,must be covered during clinical hours.
                              All body piercing must be removed with the exception of one PAIR of post-style earring
                              for females.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What are the grooming policies for the Radiography program?</h3>
                           						
                           <p>Hair should be neatly groomed and no longer than collar length or secured back and
                              off the face for men and women. Clear or pastel nail polish may be worn; no artificial
                              nails or gels and the length is to be no more than ¼ inch long. Beards, sideburns
                              and mustaches shall ne neatly trimmed. Jewelry may be worn in moderation (one ring,
                              one fitted watch, one necklace worn inside the uniform).Earrings (females) must be
                              post type, non-dangling and not larger than a nickel. No other visible piercing (including
                              tongue piercing) may be worn.  Females, makeup may be used in moderation and should
                              not be distracting or offensive.  Hygiene must be maintained daily to include deodorant,
                              no strong colognes or perfumes should be worn, preferably none.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How successful are your Radiography graduates in finding employment?</h3>
                           						
                           <p>The five year average Licensure Rate for Radiography graduates is 100% and the five
                              year average Placement Rate for Radiography graduates is 98%
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           						
                           <p> The Radiography program is a daytime program located on the West campus; its time
                              demands should be considered the equivalent of a fulltime job as in addition to lecture,
                              lab and clinical, students are expected to study between 15-25 hours a week -if a
                              student chooses to work while in the program, a maximum of 15 hours a week is recommended
                              due to the demands of the program. Time management is crucial to successful program
                              completion! The specific schedule can vary from term to term and from year to year.
                              
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many clinical hours are in the Radiography program and what credential would I
                              be eligible to sit for upon successful completion?
                           </h3>
                           						
                           <p>Graduates of the Radiography program complete 1,440 clinical hours and are eligible
                              to sit for the ARRT National Registry in Radiography(R).
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can students accepted into the Radiography program be exposed to blood borne pathogens?</h3>
                           						
                           <p>The Radiography program may have clinical experiences that expose the student to blood
                              borne pathogens via contact with bodily fluids such as blood and saliva. Students
                              accepted into the program will be expected to adhere to Centers for Disease Control
                              (CDC) guidelines for the use of Standard Precautions. The CDC defines standard precautions
                              as "a set of precautions designed to prevent the transmission of HIV, Hepatitis B
                              virus (HBV), other blood borne pathogens when providing first aid or health care.”
                              Under standard precautions, blood and certain body fluids are considered potentially
                              infectious for HIV, HBV, and other blood borne pathogens. Students will be expected
                              to adhere to hand hygiene protocols and consistent use of personal protective equipment
                              including masks, gloves and eyewear. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How do I apply?</h3>
                           						
                           <p>Download &amp; fill out the Radiography Application. Return your completed application
                              to
                              							Health Sciences Advising along with a non-refundable Health Sciences program
                              application fee of $15
                           </p>
                           						
                           <ul>
                              							
                              <li>
                                 								
                                 <h4>By mail:</h4>
                                 								Send the application along with a check or money order payable to Valencia
                                 College to
                                 								the address listed below:
                                 								
                                 <address class="wrapper_indent">
                                    									Valencia College<br>
                                    									Business Office<br>
                                    									P.O. Box 4913.<br>
                                    									Orlando, FL
                                    									32802
                                    								
                                 </address>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h4>In person:</h4>
                                 								Make payment in the Business Office on any Valencia Campus &amp; request that
                                 the
                                 								Business Office forward your program application &amp; fee receipt to the Health
                                 Sciences Advising office at
                                 								mail code 4-30 
                              </li>
                              						
                           </ul>
                           						
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              							documentation or requirements can result in denied admission. All items listed
                              on the <a href="http://valenciacollege.edu/west/health/documents/HealthInformationTechnology.pdf">Program Guide
                                 							Admission Checklist</a> must be completed PRIOR to submitting a Radiography Application 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/radiography/index.pcf">©</a>
      </div>
   </body>
</html>