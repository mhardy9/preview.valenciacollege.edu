<ul>
            <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);">Health Sciences - Degrees | Certificates | Continuing Ed <span class="caret" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-6">
                  <h3>AS Degrees</h3>
                  <ul>
                    <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
                    <li><a href="/academics/programs/health-sciences/dental-hygiene/">Dental Hygiene</a></li>
                    <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/">Diagnostic Medical Sonography</a></li>
                    <li><a href="/academics/programs/health-sciences/emergency-medical-services-tech/">Emergency Medical Services Technology</a></li>
                    <li><a href="/academics/programs/health-sciences/health-information-technology/">Health Information Technology</a></li>
                    <li><a href="/academics/programs/health-sciences/nursing/">Nursing</a></li>
                    <li><a href="/academics/programs/health-sciences/radiography/">Radiography</a></li>
                    <li><a href="/academics/programs/health-sciences/respiratory-care/">Respiratory Care</a></li>
                    <li><a href="/academics/programs/health-sciences/veterinary-tech/">Veterinary Technology</a></li>
                  </ul>
                  <h3>BS Degrees</h3>
                  <ul>
                    <li><a href="/academics/programs/health-sciences/cardiopulmonary-sciences/">Cardiopulmonary Sciences</a></li>
                    <li><a href="/academics/programs/health-sciences/radiologic-imaging-sciences/">Radiologic and Imaging Sciences</a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <h3>Technical Certificates</h3>
                  <ul>
                    <li><a href="/academics/programs/health-sciences/computed-tomography/">Computed Tomography (ATC)</a></li>
                    <li><a href="/academics/programs/health-sciences/echocardiography/">Echocardiography (ATC)</a></li>
                    <li><a href="/academics/programs/health-sciences/leadership-healthcare/">Leadership in Healthcare (ATC)</a></li>
                    <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/">Magnetic Resonance Imaging (ATC)</a></li>
                    <li><a href="/academics/programs/health-sciences/mammography/">Mammography (ATC)</a></li>
					<li><a href="/academics/programs/health-sciences/medical-info-coder/">Medical Information Coder/Biller (TC)</a></li>
                    <li><a href="/academics/programs/health-sciences/emergency-medical-services-tech/">Emergency Medical Technology (TC)</a></li>
                    <li><a href="/academics/programs/health-sciences/paramedic-tech/">Paramedic Technology (TC)</a></li><li><a href="/academics/programs/health-sciences/vascular-sonography/">Vascular Sonography (ATC)</a></li>
                  </ul>
                  <h3>Continuing Ed</h3>
                  <ul>
                    <li><a href="/academics/programs/health-sciences/continuing-education/">Health Sciences Continuing Education</a></li>
                  </ul>
                </div>
              </div>
              <!-- End menu-wrapper --></li>
            <li class="submenu"><a class="show-submenu" href="/academics/programs/health-sciences/radiography/index.php">Radiography<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
              <ul>
                <li><a href="/documents/academics/programs/health-sciences/radiography-program-guide.pdf">Program Guide</a></li>
                <li><a href="/documents/academics/programs/health-sciences/radiography-course-planning-guide.pdf">Course Guide</a></li>
                <li><a href="/documents/academics/programs/health-sciences/radiography-estimated-costs.pdf">Estimated Costs</a></li>
                <li><a href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm">Contact Health Sciences Advising</a></li>
              </ul>
            </li>
          </ul>
      