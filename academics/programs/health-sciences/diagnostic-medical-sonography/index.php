<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Diagnostic Medical Sonography  | Valencia College</title>
      <meta name="Description" content="Diagnostic Medical Sonography | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/diagnostic-medical-sonography/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/diagnostic-medical-sonography/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Diagnostic Medical Sonography</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               				
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     
                     <h2>Diagnostic Medical Sonography</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Diagnostic Medical Sonography Associate in Science (A.S.) degree at Valencia College
                        is a two-year program that prepares you to go directly into a specialized career in
                        sonography.
                     </p>
                     
                     <p>If you’re fascinated by the inner-workings of the human body, then you may want to
                        consider becoming a sonographer. Sonographers play a dynamic role in the treatment
                        and care of patients. When a doctor needs to study any of the soft tissue organs of
                        a patient—like the liver, gallbladder, kidneys, thyroid or breast—it is the sonographer
                        who provides the images using high frequency sound waves via ultrasound. You may also
                        get to share in an experience most people remember for a lifetime—providing expectant
                        mothers with their first glimpse of an unborn child—a moment most people remember
                        for the rest of their lives.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/diagnosticmedicalsonography/#programrequirementstext"> <span class="far fa-laptop"></span>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/diagnostic-medical-sonography-course-planning-guide.pdf"> <i class="far fa-calendar"></i>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 6 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/asdegrees/credit_alternative.cfm"> <i class="far fa-share-alt"></i>
                           
                           <h3>Alternative Credit</h3>
                           
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? You may receive
                              college credit for approved Industry Certifications.
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-dollar-sign"></i>
                           
                           <h3>Average Salary &amp; Placement</h3>
                           
                           <p>Valencia’s A.S. Degree and Certificate programs placement rate ranges between 90%
                              – 95%. Sonography 
                              average salaries ranges from $40,000 - $50,000<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4">
                        <a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <i class="far fa-life-ring"></i>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.php"> <i class="far fa-question-circle"></i>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Attend a Health Sciences Information Sessions for an overview of the limited access
                              Health Science programs
                              at Valencia College and the procedures for enrolling in them. <br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  <!--End container -->
                  
                  <div class="container margin-60 box_style_1">
                     
                     <div class="main-title">
                        
                        <h2>Admission into Diagnostic Medical Sonography </h2>
                        
                        <p></p>
                        
                     </div>
                     
                     <div class="row">
                        
                        <div class="col-md-6 col-sm-6">
                           
                           <div class="box_style_4">
                              
                              <h3>Important Information</h3>
                              
                              <ul>
                                 
                                 <li><a href="/documents/academics/programs/health-sciences/diagnostic-medical-sonography-program-guide.pdf">Current Program Guide</a></li>
                                 
                                 <li><a href="/documents/academics/programs/health-sciences/diagnostic-medical-sonography-estimated-costs.pdf">Estimated Program Costs</a></li>
                                 
                                 <li><a href="/documents/academics/programs/health-sciences/diagnostic-medical-sonography-course-planning-guide.pdf">Course Planning Guide </a> 
                                 </li>
                                 
                                 <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-6 col-sm-6">
                           
                           <div class="box_style_4">
                              
                              <h3>Application Information</h3>
                              
                              <ul>
                                 
                                 <li>Application Deadline: May 15, 2018</li>
                                 
                                 <li>Begins: Fall 2018</li>
                                 
                                 <li>Application: Download should be available March 2018</li>
                                 
                              </ul>
                              <br>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <div class="row">
                        
                        <div class="col-md-8 col-sm-8">
                           		
                           <h3>Program Changes</h3>
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <tr>
                                 
                                 <th scope="col">Date Updated</th>
                                 
                                 <th scope="col">Effective Date</th>
                                 
                                 <th scope="col">Change</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>9/4/14 (posted on website) </td>
                                 
                                 <td>12/10</td>
                                 
                                 <td>
                                    <p>TEAS Version 5 (V) minimum Composite score of *58.7% required.  TEAS scores are valid
                                       for 5 years. *Subject to change. You must meet required score in effect at the time
                                       you apply to the program. 
                                    </p>
                                    
                                    <p> TEAS information is available at <a href="/assessments/teas/">valenciacollege.edu/assessments </a>and &nbsp;<a href="https://www.atitesting.com/Home.aspx">ATItesting.com&nbsp; </a> 
                                    </p>
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>8/4/16 (posted on website) </td>
                                 
                                 <td>9/15/17</td>
                                 
                                 <td>
                                    <p>TEAS for program entry after September 15, 2017 must submit the new version: ATI TEAS.&nbsp;
                                       The minimum composition score of *58.7% is required.&nbsp; Scores are valid for 5 years.
                                       *Subject to change.&nbsp; You must meet required score in effect at the time you apply
                                       to the program.&nbsp; 
                                    </p>
                                    
                                    <p>TEAS information is available at <a href="/assessments/teas/">valenciacollege.edu/assessments </a>and <a href="https://www.atitesting.com/Home.aspx">ATItesting.com </a> 
                                    </p>
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>12/7/16</td>
                                 
                                 <td>Fall 2017 </td>
                                 
                                 <td>
                                    <p>NEW process for Fall 2017 CVT Program</p>
                                    
                                    <p>In early to mid-June, the Health Sciences Admission Committee will conduct a Panel
                                       Interview in the selection of qualified students to be admitted to the Cardiovascular
                                       Technology Program (CVT). Applicants with sufficient points will receive an ATLAS
                                       email in late May with specific dates and related interview information. Students
                                       can earn a maximum of 26 interview points through this process. (All other program
                                       course requirements and admission points will remain the same)
                                    </p>
                                 </td>
                                 
                              </tr>
                              
                              <tr bgcolor="#FFFFFF">
                                 
                                 <td>3/6/17</td>
                                 
                                 <td>Fall 2017</td>
                                 
                                 <td>
                                    <ul>
                                       
                                       <li>The overall GPA Admission Points will not be in consideration for the total admission</li>
                                       
                                       <li>Points will no longer be awarded for Overall GPA </li>
                                       
                                       <li>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1. Note:SLS 1122
                                          points will remain the same--A: 3, B:2, C: 1 
                                       </li>
                                       
                                    </ul>
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/20/17</td>
                                 
                                 <td>Fall 2017</td>
                                 
                                 <td>International Students are eligible to apply for this
                                    Health Sciences Program. <a href="International-Student-Eligibility.cfm">Click here for details.</a></td>
                                 
                              </tr>
                              
                           </table>
                           
                           <ul class="list_staff">
                              		          
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                                 			
                                 <h4>Jamy Chulak, M.S., RRT</h4>
                                 
                                 <p>Dean of Allied Health</p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/carolyn-deleo-picture.jpg" alt="Carolyn DeLeo" class="img-circle"></figure>
                                 
                                 <h4>Carolyn DeLeo, RDMS, RVT, RT (R), M.A, ED </h4>
                                 
                                 <p>Program chair &amp; Clinical and Lab Coordinator </p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/michelle-sanchez-picture.jpg" alt="Michelle Sanchez" class="img-circle"></figure>
                                 
                                 <h4>Michelle Sanchez, AA, AS, RDMS</h4>
                                 
                                 <p>Clinical and Lab Instructor</p>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <div class="col-md-4 col-sm-4">
                           
                           <div class="box_style_4">
                              
                              <h4>Steps for Admission to Program</h4>
                              
                              <ul class="list_order">
                                 
                                 <li><span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                       Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                    are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                       Questions</a>. 
                                 </li>
                                 
                                 <li><span>2</span><a href="#">Apply</a> to Valencia College. 
                                 </li>
                                 
                                 <li><span>3</span>Complete required general education courses before applying to the program.
                                 </li>
                                 
                                 <li><span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                    requirements, you can apply to the program. 
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <!--End row --> 
                     
                  </div>
                  
                  <div class="container margin-60 box_style_1">
                     
                     <div class="main-title">
                        
                        <h2>Career Outlook</h2>
                        
                     </div>
                     
                     <div class="row">
                        
                        <div class="col-md-4 col-sm-4">
                           
                           <div class="box_style_4">
                              
                              <h3>Learn How To</h3>
                              
                              <ul>
                                 
                                 <li>Analyze and present diagnostic data for interpretation and diagnosis by the physician</li>
                                 
                                 <li>Position the patient’s body so that the correct areas can be seen on an ultrasound
                                    screen
                                 </li>
                                 
                                 <li>Perform sonograms and record normal anatomy and pathologic data for interpretation</li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4 col-sm-4">
                           
                           <div class="box_style_4">
                              
                              <h3>Job Outlook</h3>
                              
                              <p>A variety of exciting career opportunities are available for sonographers. They may
                                 work in clinics, physicians’ offices, diagnostic imaging centers, ambulatory care
                                 facilities, and hospitals in trauma/emergency and intensive/critical care settings.
                                 With rapidly developing new technologies and increased use of diagnostic ultrasound
                                 procedures, the sonography profession is projected to continue to grow in the future.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4 col-sm-4">
                           
                           <div class="box_style_4">
                              
                              <h3>Potential Careers</h3>
                              
                              <ul class="list_4">
                                 
                                 <li>Diagnostic Medical Sonographer</li>
                                 
                                 <li>Ultrasound Technologist</li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        	  
                     </div>
                     	  
                     <div class="row">
                        		  
                        <h3>Program Outcomes</h3>
                        		  
                        <table class="table table">
                           		  
                           <tr valign="top">
                              
                              <th scope="col">Class Year</th>
                              
                              <th scope="col">Attrition Rate </th>
                              
                              <th scope="col">Job Placement </th>
                              
                              <th scope="col">Credential Pass Rate </th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Class of 2015</td>
                              
                              <td>42%</td>
                              
                              <td>100%</td>
                              
                              <td>100%</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Class of 2016</td>
                              
                              <td>08%</td>
                              
                              <td>100%</td>
                              
                              <td>100%</td>
                              
                           </tr>
                           		  
                        </table>
                        		  
                     </div>
                     	
                  </div>
                  		
               </div> 
               		  
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Sonography Videos</h2>
                     
                  </div>
                  	  
                  <section class="grid">
                     	  
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <figure><img src="/images/academics/programs/health-sciences/sonography-video-thumb.png" alt="Diagnostic Medical Sonography">
                              
                              <figcaption>
                                 
                                 <div class="caption-content"><a href="http://www.kaltura.com/tiny/m7m2f" class="video_pop" title="Diagnostic Medical Sonography"> <i class="far fa-film" aria-hidden="true"></i>
                                       
                                       <p>Valencia's program is one of the best for preparing for a career in sonography.</p> 
                                       </a></div>
                                 
                              </figcaption>
                              
                           </figure>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <figure><img src="https://img.youtube.com/vi/2tmGm7YhRVs/0.jpg" alt="Sonographer, Career Video">
                              
                              <figcaption>
                                 
                                 <div class="caption-content"><a href="https://youtu.be/2tmGm7YhRVs" class="video_pop" title="Sonographer, Career Video"> <i class="far fa-film" aria-hidden="true"></i>
                                       
                                       <p>In this interview, a Sonographer discusses her typical day at work.</p>
                                       </a></div>
                                 
                              </figcaption>
                              
                           </figure>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           
                        </div>
                        
                     </div>
                     	  
                  </section>
                  		
               </div>
               	
               <div class="container_gray_bg"> 
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        		
                        <p>The Diagnostic Medical Sonography Program was first accredited as a one year certificate
                           program for the General learning concentration in 1989. The program matriculated with
                           the first AS degree class in 1990, as part of a cooperative partnership between Valencia
                           College and Orlando Regional Healthcare.
                        </p>
                        
                        <p>In 1992, the DMS program received its first continuing accreditation for General learning
                           concentration and was granted accreditation.&nbsp; Diagnostic Medical Sonography remains
                           as an accredited program serving the Central Florida community.
                        </p>
                        
                        <p>Valencia's program is one of the best for preparing men and women for a career in
                           sonography, by connecting students directly to situations sonographers face today.
                           The program offers real-life lessons to help students build skills and confidence.
                        </p>
                        
                        <p>Students who complete the program are prepared to take the National Registry Exams
                           and can go on to work as a sonographer and/or pursue higher degrees.
                        </p>
                        
                     </div>
                     
                     <!--End row--> 
                     
                  </div>
                  
               </div>
               
               
               <div class="container margin-60 ">					
                  
                  <div class="main-title">
                     
                     <h2>Frequently Asked Questions</h2>
                     
                     <p>Diagnostic Medical Sonography</p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           
                           <p>Yes. You must complete the admission process to the college before you submit a Diagnostic
                              Medical Sonography
                              program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              questions. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What courses do I need to take prior to applying?</h3>
                           
                           <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                              Te pri facete latine
                              salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                              posse exerci
                              volutpat has in.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Are the prerequisite courses offered online?</h3>
                           
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available every term. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           
                           <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                              if accepted, Cardiovascular Technology courses are only offered on the West campus.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           
                           <p>Yes, you must complete the TEAS 5. The examination fee is your responsibility. More
                              information on the TEAS is available at <a href="/assessments/TEAS.cfm">Assessments_TEAS</a></p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can I use TEAS scores from another institution?</h3>
                           
                           <p>Yes. Students who complete the TEAS at another institution must request an official
                              score report from ATI as a paper copy is not accepted. Requests can be made online
                              via <a href="www.atitesting.com">www.atitesting.com</a></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many students are accepted into the program?</h3>
                           
                           <p>The Cardiovascular Technology program accepts 18 students each Fall term.</p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>What are the grooming policies for the Diagnostic Medical Sonography program?</h3>
                           
                           <p>From the program Handbook:</p>
                           
                           <h4>Requirements for Women</h4>
                           
                           <ol>
                              
                              <li>Hair shall be kept cut above the collar or neatly secured with a simple hair tie or
                                 barrette in an off-the-face style (unusual hair color not permitted.).
                              </li>
                              
                              <li>Clear nail polish may be worn.  Artificial nails may not be worn.</li>
                              
                              <li>Jewelry may be worn in moderation:  one ring, one fitted watch or bracelet, one necklace
                                 worn inside the uniform, one pair of earrings.  Earrings must be post-type, non-dangling
                                 and not larger than a nickel.
                              </li>
                              
                              <li>No jumpsuits or culottes are acceptable.</li>
                              
                              <li>Makeup may be used in moderation; it should not be distracting or offensive.</li>
                              
                              <li>No facial jewelry is allowed at clinical sites.  (This includes tongue piercings.)</li>
                              
                              <li>Visible tattoos must be covered at clinical sites.</li>
                              
                           </ol>
                           
                           <h4>Requirements for Men</h4>
                           
                           <ol>
                              
                              <li>Hair shall be no longer than one-half the collar length of a standard dress shirt.</li>
                              
                              <li>Beards, sideburns and mustaches shall be neatly trimmed.</li>
                              
                              <li>Jewelry may be worn in moderation:  one ring, one fitted watch, one necklace worn
                                 inside the uniform.
                              </li>
                              
                              <li>No facial jewelry is allowed at clinical sites.  (This includes tongue piercings.)</li>
                              
                              <li>Visible tattoos must be covered at clinical sites.</li>
                              
                           </ol>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           
                           <p>The Diagnostic Medical Sonography program is a daytime program located on the West
                              campus; its time demands should be considered the equivalent of a fulltime job as
                              in addition to lecture, lab and clinicals, students are expected to study between
                              15-25 hours a week - if a student chooses to work while in the program, a maximum
                              of 15 hours a week is recommended due to the demands of the program.  Time management
                              is crucial to successful program completion!  The specific schedule can vary from
                              term to term and from year to year. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-8">
                        
                        <div class="box_style_2">
                           
                           <h3>Can students accepted into the Diagnostic Medical Sonography program be exposed to
                              blood borne pathogens?
                           </h3>
                           
                           <p>The Diagnostic Medical Sonography program may have clinical experiences that expose
                              the student to blood borne pathogens via contact with bodily fluids such as blood
                              and saliva. Students accepted into the program will be expected to adhere to Centers
                              for Disease Control (CDC) guidelines for the use of Standard Precautions. The CDC
                              defines standard precautions as "a set of precautions designed to prevent the transmission
                              of HIV, Hepatitis B virus (HBV), other blood borne pathogens when providing first
                              aid or health care.” Under standard precautions, blood and certain body fluids are
                              considered potentially infectious for HIV, HBV, and other blood borne pathogens. Students
                              will be expected to adhere to hand hygiene protocols and consistent use of personal
                              protective equipment including masks, gloves and eyewear. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What are the body art policies for the Diagnostic Medical Sonography program?</h3>
                           
                           <p>All must be covered when at clinical sites.</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many clinical hours are in the Diagnostic Medical Sonography program and what
                              credential would I be eligible to sit for upon successful completion?
                           </h3>
                           
                           <p>Clinical Practicum hours total to approximately 1488 hours including labs. Graduates
                              of the Diagnostic Medical Sonography program are eligible to sit for the OB/GYN and
                              Abdomen registry exams.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How successful are your Diagnostic Medical Sonography graduates in finding employment?</h3>
                           
                           <p>The five year average Licensure Rate for Diagnostic Medical Sonography graduates is
                              100% and the five year average Placement Rate for Diagnostic Medical Sonographygraduates
                              is 96%.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>How do I apply?</h3>
                           
                           <p>Download &amp; fill out the Health Information Technology Application. Return your completed
                              application to
                              Health Sciences Advising along with a non-refundable Health Sciences program application
                              fee of $15
                           </p>
                           
                           <ul>
                              
                              <li>
                                 
                                 <h4>By mail:</h4>
                                 Send the application along with a check or money order payable to Valencia College
                                 to
                                 the address listed below:
                                 
                                 <address class="wrapper_indent">
                                    Valencia College<br>
                                    Business Office<br>
                                    P.O. Box 4913.<br>
                                    Orlando, FL
                                    32802
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 
                                 <h4>In person:</h4>
                                 Make payment in the Business Office on any Valencia Campus &amp; request that the
                                 Business Office forward your program application &amp; fee receipt to the Health Sciences
                                 Advising office at
                                 mail code 4-30 
                              </li>
                              
                           </ul>
                           
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              documentation or requirements can result in denied admission. All items listed on
                              the <a href="http://valenciacollege.edu/west/health/documents/HealthInformationTechnology.pdf">Program Guide
                                 Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/diagnostic-medical-sonography/index.pcf">©</a>
      </div>
   </body>
</html>