<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Radiologic and Imaging Sciences  | Valencia College</title>
      <meta name="Description" content="Radiologic and Imaging Sciences | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/radiologic-imaging-sciences/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/radiologic-imaging-sciences/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Radiologic and Imaging Sciences</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Radiologic Imaging Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     
                     <h2>Radiologic and Imaging Sciences</h2>
                     
                     <p>Bachelor of Science (B.S.) Degree</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The bachelor’s degree in Radiologic and Imaging Sciences is for imaging professionals
                        who have completed an AS degree in Radiography, Sonography, Nuclear Medicine Technology
                        or Radiation Therapy. The AS to BS degree program will provide the opportunity for
                        you to gain valuable knowledge and skills in radiology management/administration,
                        teaching and in an advanced clinical modality of your choice.
                     </p>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobsradiologicimagingsciences/#programrequirementstext"> <span class="far fa-laptop"></span>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br><br><br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/radiologic-imaging-sciences-program-guide.pdf"> <span class="far fa-calendar"></span>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 5 years. If you already have an eligible associate degree in the
                              field, you can complete your bachelor’s degree in as little as two years.
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  <!--End row-->
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4 box_feat"><span class="far fa-share-alt fa-5x v-red"></span>
                        
                        <h3 class="h4 text-uppercase">Degrees that Transfer to this Bachelor's</h3>
                        
                        <ul>
                           		  
                           <li><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/index.php">Diagnostic Medical Sonography</a></li>
                           		  
                           <li><a href="/academics/programs/health-sciences/radiography/index.php">Radiography</a></li>
                           	  
                        </ul><br></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           
                           <h3>Average Salary</h3>
                           
                           <p>Radiologic and Imaging Sciences average salaries ranges from $45,000 – $65,000  ($21.63
                              – $31.25 per hour)
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     
                  </div>
                  
                  <!--End row--> 
                  
                  
                  
               </div>
               
               
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Radiologic and Imaging Sciences </h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="/documents/academics/programs/health-sciences/radiologic-imaging-sciences-program-guide.pdf">Current Program Guide</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/degrees/bachelorofscience/astobsradiologicimagingsciences/#programrequirementstext">Current Program Requirements</a></li>
                              
                              <li><a href="#">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li><strong>Application Deadline:</strong> No Deadline
                              </li>
                              
                              <li><strong>Begins:</strong> Fall, Spring, Summer
                              </li>
                              			
                              <li><strong>Application:</strong> <a href="/documents/academics/programs/health-sciences/Application-BSRAD-and-ATCs.pdf">Download Radiologic and Imaging Sciences Application</a></li>
                              
                           </ul>
                           <br>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        		
                        <div class="row">
                           		
                           <h3>Program Changes</h3>
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <tr>
                                 
                                 <th scope="col">Date Updated</th>
                                 
                                 <th scope="col">Effective Date</th>
                                 
                                 <th scope="col">Change</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>8/12/16</td>
                                 
                                 <td>8/12/16</td>
                                 
                                 <td> Students who hold the appropriate healthcare professional certification and an Associate
                                    degree or higher in any field may be considered for admission.&nbsp;  
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>4/27/16</td>
                                 
                                 <td>Fall Term 2016</td>
                                 
                                 <td>BS Degree Concentrations are: Computed Tomography (CT), Magnetic Resonance Imaging
                                    (MRI), Mammography, Cardiac Ultrasound, and Leadership (non-clinical).
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    <p>4/27/16</p>
                                 </td>
                                 
                                 <td>
                                    <p>Summer Term 2016 
                                       
                                    </p>
                                 </td>
                                 
                                 <td>
                                    <p>Quality Management no longer is a Concentration in the BS Degree.</p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              
                           </table>
                           		
                        </div>
                        		
                        <div class="row">
                           			
                           <h3>Radiologic and Imaging Sciences Department</h3>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 
                                 <figure><img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                                 			
                                 <h4>Jamy Chulak, M.S., RRT</h4>
                                 
                                 <p>Dean of Allied Health</p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                                 			
                                 <h4>Susan Gosnell</h4>
                                 
                                 <p>Chair of Radiologic and Imaging Sciences Program</p>
                                 
                              </li>
                              
                              <li>
                                 
                                 <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                                 
                                 <h4>Health Sciences Advising</h4>
                                 
                                 <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                                 
                              </li>
                              
                           </ul>
                           		
                        </div>
                        		
                        <div class="row">
                           		 
                           <div class="col-md-6">
                              				
                              <h3>Job Outlook</h3>
                              
                              <p>A completed bachelor’s degree opens many doors for advancement in your professional
                                 career. In an advanced position within the radiologic and imaging sciences, you will
                                 manage high-tech equipment and oversee technologists and the procedures they perform.
                              </p>
                              			
                           </div>
                           			
                           <div class="col-md-6">
                              				
                              <h3>Potential Careers</h3>
                              				
                              <ul>
                                 					
                                 <li>Radiology Management/Administration</li>
                                 
                                 <li>Higher Education</li>
                                 
                                 <li>Quality Management</li>
                                 
                                 <li>Advanced Clinical Practice in Computed Tomography or Magnetic Resonance Imaging</li>
                                 				
                              </ul>
                              
                              			
                           </div>
                           			
                           	
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>7 Steps to Your Bachelor's Degree</h4>
                           
                           <ul class="list_order">
                              
                              <li><span>1</span>Complete your associate degree in one of four eligible fields (including all prerequisites):
                                 			  
                                 <div class="indent_title_in">Diagnostic Medical Sonography, Radiation Therapy, Nuclear Medicine Technology, or
                                    Radiography.
                                    
                                 </div>
                                 			
                                 <p>If your associate degree is in another field, you must undergo a formal evaluation.</p>
                              </li>
                              
                              <li><span>2</span>Hold professional certification in your respective discipline.
                              </li>
                              
                              <li><span>3</span>Submit the online Valencia College application for the bachelor’s program, as well
                                 as all college transcripts, and pay the $35 application fee. This fee is waived for
                                 active Valencia College students who hold an Associate in Arts degree from Valencia
                                 or an Associate in Science degree from Valencia to a bachelor degree at Valencia.
                              </li>
                              
                              <li><span>4</span>Once you’ve been accepted to Valencia College as a candidate for B.S. Radiology, submit
                                 the Radiologic and Imaging Sciences Bachelor’s Program application, available through
                                 Valencia’s Health Sciences Advising department, and contact the program advisor.
                              </li>
                              			
                              <li><span>5</span>Once you’ve been accepted to the B.S. degree, complete the online bachelor’s orientation.
                              </li>
                              
                              			
                              <li><span>6</span>Register for classes.
                              </li>
                              
                              			
                              <li><span>7</span>Graduate with your B.S. degree. 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <!--End row --> 
                  
                  <hr class="styled_2">
                  	
                  	
                  <div class="row">
                     			
                     <h3>Lower Division Coursework</h3>
                     
                     
                     <p>To fulfill the lower-division (first half) of the Radiologic and Imaging Sciences
                        bachelor’s degree, you must complete an Associate in Science degree from a regionally
                        accredited institution in one of the following areas.
                     </p>
                     
                     			
                     <h4>Choose from Two A.S. Degrees:</h4>
                     
                     			
                     <h5><a href="/academics/programs/health-sciences/diagnostic-medical-sonography/index.php">A.S. in Diagnostic Medical Sonography</a></h5>
                     
                     <p>Sonographers take images of patients’ soft tissue organs—like the liver, gallbladder,
                        kidneys, thyroid, uterus or breast—using high frequency sound waves via ultrasound.
                        They must analyze and present this diagnostic data for interpretation and diagnosis
                        by a doctor.
                     </p>
                     
                     			
                     <h5><a href="/academics/programs/health-sciences/radiography/index.php">A.S. in Radiography</a></h5>
                     
                     <p>Radiographers operate high-tech imaging equipment and perform diagnostic imaging examinations
                        such as X-ray images, CAT scans, MRIs and mammograms that help doctors accurately
                        diagnose and treat their patients for injury and disease.
                        
                     </p>
                     
                     <p>(Valencia will also accept a completed A.S. degree in Radiation Therapy or Nuclear
                        Medicine Technology, offered by some other institutions, for admission into the upper-division
                        coursework.)
                     </p>
                     
                     <h3>Upper Division Coursework</h3>
                     
                     <p>After completing a qualifying lower-division associate degree, you may move on to
                        the upper-division (second half) of the Radiologic and Imaging Sciences bachelor’s
                        degree.
                     </p>
                     
                     			
                     <h4>Concentrations</h4>
                     			
                     <div class="col-md-6">
                        				
                        <h5>Computed Tomography (CT)</h5>
                        
                        <p>This patient-focused concentration enables you to develop skills to become a CT technologist.
                           You will gain an understanding of the physical principles and instrumentation involved
                           in CT, as well as quality management. This includes procedure protocols, evaluating
                           equipment operation, ensuring accurate diagnosis and safe patient care.
                        </p>
                        			
                     </div>
                     			
                     <div class="col-md-6">
                        				
                        <h5>Magnetic Resonance Imaging (MRI)</h5>
                        
                        <p>This patient-focused concentration enables you to develop skills to become an MRI
                           technologist. You will gain an understanding of the basic concepts of magnetic resonance,
                           including magnet types, the generation of magnetic resonance signal and magnet safety.
                           You will also learn procedures covering anatomy, pathology and equipment applications
                           for the various regions of the human body.
                        </p>	
                        			
                     </div>
                     
                     
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/radiologic-imaging-sciences/index.pcf">©</a>
      </div>
   </body>
</html>