<ul>
            <li><a href="/academics/programs/health-sciences/index.php">Health Sciences</a></li>
            <li class="submenu">
<a href="#">AS Degrees <i class="icon-down-open-mini"></i></a>
              <ul>
                <li><a href="/academics/programs/health-sciences/cardiovascular-technology/index.php">Cardiovascular Technology</a></li>
                <li><a href="/academics/programs/health-sciences/dental-hygiene/index.php">Dental Hygiene</a></li>
                <li><a href="/academics/programs/health-sciences/sonography/index.php">Diagnostic Medical Sonography</a></li>
                <li><a href="/academics/programs/health-sciences/emergency-medical-services/index.php">Emergency Medical Services</a></li>
                <li><a href="/academics/programs/health-sciences/health-information-technology/index.php">Health Information Technology</a></li>
                <li><a href="/academics/programs/health-sciences/nursing/index.php">Nursing</a></li>
                <li><a href="/academics/programs/health-sciences/radiography/index.php">Radiography</a></li>
                <li><a href="/academics/programs/health-sciences/respiratory-care/index.php">Respiratory Care</a></li>
                <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/">Veterinary Technology</a></li>
              </ul>
            </li>
            <li class="submenu">
<a href="#">Bachelors of Science <i class="icon-down-open-mini"></i></a>
              <ul>
                <li><a href="/academics/programs/health-sciences/cardiopulmonary-sciences/index.php">Cardiopulmonary Sciences</a></li>
                <li><a href="/academics/programs/health-sciences/radiologic-imaging-sciences/index.php">Radiologic and Imaging Sciences</a></li>
              </ul>
            </li>
            <li class="submenu">
				<a href="#" class="show-submenu">Certificates <span class="caret"></span></a>
              <ul>
                <li><a href="/academics/programs/health-sciences/computed-tomography/index.php">Computed Tomography</a></li>
                <li><a href="/academics/programs/health-sciences/echocardiography/index.php">Echocardiography</a></li>
                <li><a href="/academics/programs/health-sciences/leadership-healthcare/index.php">Leadership in Healthcare</a></li>
                <li><a href="/academics/programs/health-sciences/magnetic-resonance-imaging/index.php">Magnetic Resonance Imaging</a></li>
                <li><a href="/academics/programs/health-sciences/mammography/index.php">Mammography</a></li>
                <li><a href="/academics/programs/health-sciences/emergency-medical-services-tech/emergency-medical-technician.php">Emergency Medical Technology</a></li>
		<li><a href="/academics/programs/health-sciences/medical-info-coder/">Medical Information Coder/Biller</a></li>
				
                <li><a href="/academics/programs/health-sciences/paramedic-tech/index.php">Paramedic Technology</a></li>
              </ul>
            </li>
            <li><a href="/LOCATIONS/west/departments/health-sciences/continuing-education/index.php">Continuing Education</a></li>
          </ul>
