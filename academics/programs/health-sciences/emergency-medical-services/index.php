<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Emergency Medical Services Technology | Valencia College</title>
      <meta name="Description" content="Emergency Medical Services Technology | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/emergency-medical-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/emergency-medical-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Emergency Medical Services Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Emergency Medical Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Emergency Medical Services Technology</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Emergency Medical Services Technology Associate in Science (A.S.) degree at Valencia
                        College is a two-year program that prepares you to go directly into a specialized
                        career as an EMT or paramedic.
                     </p>
                     
                     <p>Considered one of the most progressive programs available in Florida, Valencia’s EMS
                        program offers advanced training in out-of-hospital emergency medicine and prepares
                        students for State Board certification as emergency medical technicians and paramedics.
                        EMTs and paramedics assess injuries, administer advanced emergency medical care and
                        transport injured or ill people to medical facilities. Valencia’s program teaches
                        theory and clinical applications, and provides hands-on field internships to assure
                        that students gain the skills necessary to offer the most extensive emergency life
                        support services available. If you are good at making split-second decisions and staying
                        calm in a crisis, then this may be the career for you.
                     </p>
                     
                  </div>
                  
               </div>
               
               <div class="container margin-30">
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/emergencymedicalservicestechnology/#text"> <i class="far fa-laptop"></i>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/emergency-medical-technology-course-planning-guide.pdf"> <i class="far fa-calendar"></i>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 5 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/asdegrees/credit_alternative.php"> <i class="far fa-share-alt"></i>
                           
                           <h3>Alternative Credit</h3>
                           
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? You may receive
                              college credit for approved Industry Certifications.
                           </p>
                           </a></div>
                     
                  </div>
                  
                  <!--End row-->
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-dollar-sign"></i>
                           
                           <h3>Average Salary &amp; Placement</h3>
                           
                           <p>Valencia’s A.S. Degree and Certificate programs placement rate ranges between 90%
                              – 95%. Emergency Medical Services Technology AS Degree 
                              average salaries ranges from $38,000 – $45,000<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <i class="far fa-life-ring"></i>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.php"> <i class="far fa-question-circle"></i>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Attend a Health Sciences Information Sessions for an overview of the limited access
                              Health Science programs
                              at Valencia College and the procedures for enrolling in them. <br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  <!--End row-->
               </div>
               
               <!--End container -->
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Progression to EMS Associate in Science Degree</h2>
                     
                  </div>
                  
                  <div class="row">
                     
                     <p>In September of 2013 Valencia College’s EMT Program was named one of the nations top
                        forty programs. More than 1,100 colleges in the U.S. offer EMT training. Our program
                        was identified for having one of the highest return on investment, they used national
                        salary information, tuition, enrollment and accreditation data to make their selections.
                        The EMS faculty is extremely proud of this accomplishment. Offering affordable high
                        quality education is what it is all about.
                     </p>
                     
                     <p>These programs are approved by the Department of Health-Bureau of EMS, State of Florida
                        Department of Education, and the Committee on Accreditation of Educational Programs
                        for the EMS Professions (CoAEMSP). The EMT-Paramedic Program is designed for students
                        who are interested in providing pre-hospital emergency care to acutely ill or injured
                        patients. The programs (Certificates / A.S. Degree) will prepare the graduate for
                        State Board Certification or National Registry as an Emergency Medical Technician
                        (EMT) and/or Paramedic.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <h3>Step 1: Emergency Medical Technician (EMT) Certificate</h3>
                        
                        <p>1 Semester. Offered every semester.</p>
                        
                        <p>The EMT Certificate is the first step toward the Paramedic Certificate or A.S. Degree
                           in EMS.
                        </p>
                        
                        <p>The advanced level of EMS care is provided by the paramedic. This individual may perform
                           Advanced Life Support (ALS) procedures. ALS is defined as treatment of life-threatening
                           emergencies through the use of specific techniques identified by the U.S. Department
                           of Transportation. These procedures must be performed under the supervision of a licensed
                           physician.
                        </p>
                        
                        <div class="button-action_outline"><a href="/academics/programs/health-sciences/emergency-medical-services/emergency-medical-technician.php">EMT Certificate Details </a></div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <h3>Step 2: Paramedic Technology Certificate</h3>
                        
                        <p>3 Semesters. Begins each fall on West Campus and begins each spring on Osceola Campus.</p>
                        
                        <p>The Paramedic Certificate is a limited access program for students who hold a current
                           Florida EMT Certificate.
                        </p>
                        
                        <p>Admission to Valencia does not imply acceptance to the Paramedic Certificate<br> Program.
                        </p>
                        
                        <p>Students must apply and be accepted into the program.</p>
                        
                        <div class="button-action_outline"><a href="/academics/programs/health-sciences/emergency-medical-services/paramedic.php">Paramedic Certificate Details </a></div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <h3>Step 3: Emergency Medical Services Technology A.S. Degree</h3>
                        
                        <p>To earn the Emergency Medical Services (EMS) Degree, a student must complete the EMT
                           Certificate and the Paramedic Certificate as well as the General Education and Elective
                           Component.
                        </p>
                        
                        <p>All degree-seeking students must satisfy entry testing requirements and satisfactorily
                           complete all mandatory courses in reading, student success, mathematics, English,
                           and English for Academic Purposes (EAP) in which the student is placed.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Emergency Medical Services Technology</h2>
                     
                     <p>&nbsp;</p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technology-program-guide.pdf">Current Program Guide</a></li>
                              
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technology-estimated-costs.pdf">Estimated Program Costs</a></li>
                              
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technology-course-planning-guide.pdf">Course Planning Guide </a></li>
                              
                              <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li><strong>Application Deadline:</strong> Program Application Deadline
                              </li>
                              
                              <li><strong>Begins:</strong> Program Start Semester
                              </li>
                              
                              <li><strong>Application:</strong> Link or info about Application
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        
                        <h3>Program Changes</h3>
                        
                        <table class="table table table-striped cart-list add_bottom_30">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Date Updated</th>
                                 
                                 <th scope="col">Effective Date</th>
                                 
                                 <th scope="col">Change</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/6/2017</td>
                                 
                                 <td>Fall 2017</td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Points will no longer be awarded for Overall GPA</li>
                                       
                                       <li>BSC1084 and EMS1010 (Essentials of Human Structure and Functions) will be considered
                                          equivalent pre-requisite courses
                                       </li>
                                       
                                       <li>SPC 1608 (Fundamentals of Speech) will no longer be a requirement for the program</li>
                                       
                                       <li>SLS 1122 (New Student Experience) will be a requirement for the Program</li>
                                       
                                       <li>HSC 1004 (Professions of Caring) will not be accepted for additional points</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/20/17</td>
                                 
                                 <td>Fall 2017</td>
                                 
                                 <td>International Students are eligible to apply for this Health Sciences Program. <a href="http://valenciacollege.edu/west/health/International-Student-Eligibility.cfm">Click here for details.</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <ul class="list_staff">
                           	        
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              			
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              
                              <p>Dean of Allied Health</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Cindy Bell</h4>
                              
                              <p>Program Chair</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Jamie Lowery</h4>
                              
                              <p>EMS Faculty</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Ray Taylor</h4>
                              
                              <p>EMS Faculty</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Robert Bruce Weisenbarger</h4>
                              
                              <p>EMS Faculty</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Bobbie Sartor, EMT-P</h4>
                              
                              <p>EMT Program Coordinator</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available"></figure>
                              
                              <h4>Tom Hickman</h4>
                              
                              <p>Instructional Lab Supervisor</p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>Steps for Admission to Program</h4>
                           
                           <ul class="list_order">
                              
                              <li><span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 are not local and cannot attend the information session, review the <a href="/documents/academics/programs/health-sciences/emergency-medical-technology-program-guide.pdf">program guide</a> and read the <a href="#faq">Frequently Asked Questions</a>.
                              </li>
                              
                              <li><span>2</span><a href="#">Apply</a> to Valencia College.
                              </li>
                              
                              <li><span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              
                              <li><span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 requirements, you can apply to the program.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <!--End row -->
               </div>
               
               <!--End container -->
               
               <div class="container_gray_bg">
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        
                        <p>In September of 2013 Valencia College’s EMT Program was named one of the nations top
                           forty programs. More than 1,100 colleges in the U.S. offer EMT training. Our program
                           was identified for having one of the highest return on investment, they used national
                           salary information, tuition, enrollment and accreditation data to make their selections.
                           The EMS faculty is extremely proud of this accomplishment. Offering affordable high
                           quality education is what it is all about.
                        </p>
                        
                        <p>These programs are approved by the Department of Health-Bureau of EMS, State of Florida
                           Department of Education, and the Committee on Accreditation of Educational Programs
                           for the EMS Professions (CoAEMSP). The EMT-Paramedic Program is designed for students
                           who are interested in providing pre-hospital emergency care to acutely ill or injured
                           patients. The programs (Certificates / A.S. Degree) will prepare the graduate for
                           State Board Certification or National Registry as an Emergency Medical Technician
                           (EMT) and/or Paramedic.
                        </p>
                        
                        <p>The first level of EMS care is the Emergency Medical Technician (EMT). This individual
                           is trained in basic emergency medical procedures and must be certified by the State
                           to perform these procedures. State Board Certification for EMT is a prerequisite to
                           all paramedic courses.
                        </p>
                        
                        <p>The advanced level of EMS care is provided by the paramedic. This individual may perform
                           Advanced Life Support (ALS) procedures. ALS is defined as treatment of life-threatening
                           emergencies through the use of specific techniques identified by the U.S. Department
                           of Transportation. These procedures must be performed under the supervision of a licensed
                           physician.
                        </p>
                        
                     </div>
                     
                     <!--End row-->
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/emergency-medical-services/index.pcf">©</a>
      </div>
   </body>
</html>