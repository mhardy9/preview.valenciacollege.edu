<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Emergency Medical Technician  | Valencia College</title>
      <meta name="Description" content="Emergency Medical Technician | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/emergency-medical-services/emergency-medical-technician.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/emergency-medical-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/emergency-medical-services/">Emergency Medical Services</a></li>
               <li>Emergency Medical Technician </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Emergency Medical Technician (EMT)</h2>
                     				
                     <p>Technical Certificate</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The Certificate in Emergency Medical Technology (EMT) is a complete program and also
                        can be the first part of the <a href="/academics/programs/health-sciences/emergency-medical-services/index.php">Associate in Science (A.S.) Degree in Emergency Medical Services Technology (EMS)</a>, which has three parts. The second part of the degree is the <a href="/academics/programs/health-sciences/emergency-medical-services/paramedic.php">Technical Certificate in Paramedic Technology</a>, and the third part consists of the General Education and Elective courses required
                        for the degree. To earn this degree, all three parts must be completed. The EMT Technical
                        Certificate must be completed successfully as a major requirement for continuing to
                        the limited access Paramedic Certificate; however, the General Education and Elective
                        courses for the A.S. Degree can be completed at any time.
                     </p>
                     			
                  </div>
                  		
                  <div class="row">
                     			
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/emergencymedicalservicestechnology/#certificatetext"> <span class="far fa-laptop"></span>
                           				
                           <h3>Certificate Overview</h3>
                           				
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              					<br><br><br>
                              				
                           </p>
                           				</a></div>
                     			
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/emergency-medical-technician-program-guide.pdf"> <span class="far fa-calendar"></span>
                           				
                           <h3>Program Length</h3>
                           				
                           <p>EMT TC lasts for 1 semester. If you also complete the Paramedic TC (3 semesters),
                              you only need to take the general education courses to earn your EMS degree.
                           </p>
                           				</a></div>
                     			
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           				
                           <h3>Want more information?</h3>
                           				
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              				
                           </p>
                           				</a></div>
                     		
                  </div>
                  		
                  <!--End row-->
                  		
                  <div class="row">
                     			
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           				
                           <h3>Average Salary</h3>
                           				
                           <p>EMT average salaries ranges from $18,000 - $24,000
                              					<br>
                              				
                           </p>
                           				</a></div>
                     
                     			
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           				
                           <h3>Career Coach</h3>
                           				
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              					<br>
                              				
                           </p>
                           				</a> 
                     </div>
                     
                     		
                  </div>
                  		
                  <!--End row-->
                  
                  		
               </div>
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Emergency Medical Technician </h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technician-program-guide.pdf">Current Program Guide</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technician-estimated-costs.pdf">Estimated Program Costs</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/emergency-medical-technician-course-planning-guide.pdf">Course Planning Guide </a> 
                              </li>
                              							
                              <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/">EMT Sample Schedule</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li>
                                 								<strong>Application Deadline:</strong> No Application needed for this TC
                              </li>
                              							
                              <li>
                                 								<strong>Begins:</strong> 
                              </li>
                              							
                              <li>
                                 								<strong>Application:</strong> 
                              </li>
                              						
                           </ul>
                           						<br>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <h3>Program Changes</h3>
                        					
                        <table class="table table table-striped cart-list add_bottom_30">
                           						
                           <tr>
                              							
                              <th scope="col">Date Updated</th>
                              							
                              <th scope="col">Effective Date</th>
                              							
                              <th scope="col">Change</th>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <td>12/2/15</td>
                              							
                              <td>12/2/15</td>
                              							
                              <td>Osceola daytime block schedule (3 days a week) changed from MTW, to MTR (Mon, Tues,
                                 Thurs) in Spring 2016. 
                              </td>
                              						
                           </tr>
                           						
                           <tr>
                              							
                              <td>8/12/14</td>
                              							
                              <td>8/12/14</td>
                              							
                              <td>
                                 <p>To be eligible to receive financial aid for the EMT courses, you may need to complete
                                    additional course prerequisites and you need to: 
                                 </p>
                                 								
                                 <p>Declare the AA degree as your<strong> primary</strong> major 
                                 </p>
                                 								
                                 <p>Declare the EMT Technical Certificate as your&nbsp;<strong>secondary </strong>major
                                 </p>
                                 								Have at least 11 Elective credits remaining in the AA Degree 
                              </td>
                              						
                           </tr>
                           					
                        </table>
                        
                        				
                     </div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>Steps for Admission to Program</h4>
                           						
                           <ul class="list_order">
                              							
                              <li>
                                 								<span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    								Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 								are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                    								Questions</a>. 
                              </li>
                              							
                              <li>
                                 								<span>2</span><a href="#">Apply</a> to Valencia College. 
                              </li>
                              							
                              <li>
                                 								<span>3</span>Declare that you are seeking the EMT Technical Certificate (TC).
                              </li>
                              							
                              <li>
                                 								<span>4</span>Register for the three EMT Program courses (EMS 1119, EMS 1119L and EMS 1431L) in
                                 the same time block (day or evening) on the same campus.
                              </li>
                              							
                              <li>
                                 								<span>5</span>Complete the Compliance Requirements &amp; attend the mandatory EMT Program Orientation.
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  		
               </div>
               		<a id="faq"></a>
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Frequently Asked Questions</h2>
                     				
                     <p>Emergency Medical Technician</p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           						
                           <p>Yes. You must complete the admission process to the college before you submit a Health
                              Information Technology
                              							program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              							email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              							questions. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can Dual Enrollment students participate in the EMT program?</h3>
                           						
                           <p>No. You must be a high school graduate with either a Standard High School Diploma
                              or GED to be eligible.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What courses do I need to take prior to registering for the 3 linked EMT courses?</h3>
                           						
                           <p>No prior courses are needed</p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           						
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit; equivalent courses will be posted
                              in your Valencia Atlas account.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I register for
                              the 3 linked EMT courses?
                           </h3>
                           						
                           <p>No. The EMT program does not require the TEAS.</p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the minimum overall GPA to register for the 3 linked EMT courses?</h3>
                           						
                           <p>You must be in ‘Good Standing’ which requires a minimum 2.0 overall GPA.</p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the deadline to register for the 3 linked EMT classes?</h3>
                           						
                           <p>EMT classes are closed for registration 4 business days before the first day of class.
                              However, classes fill up quickly, so early registration is highly recommended.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How will I know what the 3 linked EMT courses are for the campus I wish to attend?</h3>
                           						
                           <p>Search the EMS prefix in Valencia’s <a href="http://net5.valenciacollege.edu/schedule/ ">Credit Class Schedule</a> and record the CRNs for the EMT classes (EMS1119, EMS1119L, EMS1431L) to be used
                              in your Atlas account for registration.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How will I be notified about the mandatory program orientation and the program compliance
                              requirements?
                           </h3>
                           						
                           <p>Approximately 1 minute after you successfully register for the 3 linked EMT courses,
                              there will be an ‘Ask Atlas’ email in your Atlas email inbox with a subject line of
                              “Student Registration Notification".
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many students are accepted into the EMT program and how often?</h3>
                           						
                           <p>The EMT program accepts approximately 130 students for the Fall, Spring and Summer
                              terms.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do students in the EMT program conduct peer-to-peer examinations?</h3>
                           						
                           <p>Yes, students in the EMT program conduct peer-to-peer exams such as Vital signs, Splinting,
                              Sling and swath, KED, Traction Splint, LBB, C-collar applications.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can students accepted into the EMT program be exposed to blood borne pathogens?</h3>
                           						
                           <p>The EMT program may have clinical experiences that may expose the student to blood
                              borne pathogens via contact with bodily fluids such as blood and saliva. Students
                              accepted into these programs will be expected to adhere to Centers for Disease Control
                              guidelines regarding the use of Universal Precautions which includes the use of personal
                              protective equipment such as masks, gloves, and eyewear.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many clinical hours are in the EMT program and what credential would I be eligible
                              to sit for upon successful completion?
                           </h3>
                           						
                           <p>The EMT program is 255 hours including 56 clinical hours. Graduates of the EMT program
                              are eligible sit for the NREMT to qualify for professional certification.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How successful are your EMT graduates in finding employment?</h3>
                           						
                           <p>The five year average Licensure Rate for EMT graduates is 90% and the five year average
                              Placement Rate for EMT graduates is 87%.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the schedule like if I am accepted into the EMT program?</h3>
                           						
                           <p>The EMT program has both a daytime and night-time program on the West and Osceola
                              campuses; its time demands should be considered the equivalent of a fulltime job as
                              students are expected to devote a minimum of 30 hours a week to the program - if a
                              student chooses to work while in the program, a maximum of 20 hours a week is recommended
                              due to the demands of the program. Time management is crucial to successful program
                              completion!
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What are the grooming policies for the EMT program?</h3>
                           						
                           <p>Required uniform items: uniform shirt, white undershirt, pants, belt, shoes, socks,
                              optional jacket as described below:
                           </p>
                           						
                           <ol>
                              							
                              <li>Uniform shirt: from All Tech Stitching- no other uniform shirt acceptable. Shirts
                                 are solid gray, collared polo shirts, embroidered with your name and college logo.
                                 								
                                 <ol>
                                    									
                                    <li>May purchase long or short sleeve uniform shirts (if visible tattoos on arms, long
                                       sleeve uniform shirt required).
                                    </li>
                                    									
                                    <li>Shirt to be worn tucked in, without ‘blousing’ over belt, long enough to stay tucked
                                       in.
                                    </li>
                                    								
                                 </ol>
                                 							
                              </li>
                              							
                              <li>Undershirt/t-shirt: solid white, logo-less, short sleeve (long sleeve t-shirt allowed
                                 only if uniform shirt is also long sleeve), worn under uniform shirt.
                              </li>
                              							
                              <li>Uniform pants: Dark blue or black uniform style pants. Pants can be purchased from
                                 All Tech Stitching (or be the identical uniform pants sold by All Tech). Pant legs
                                 must hang straight, not tucked into the boots. ‘Low-rise’, ‘hip hugger’ pants are
                                 NOT acceptable uniform pants.
                              </li>
                              							
                              <li>Belt: black leather or leather-type belt with plain belt buckle. Military fabric belts
                                 NOT acceptable.
                              </li>
                              							
                              <li>Shoes: Logo-less black, leather work shoes or boots with non-skid soles (solid black,
                                 logo-less tennis shoe acceptable- as long as they are ‘polishable’ leather- suede
                                 is not acceptable).
                              </li>
                              							
                              <li>Socks: CALF LENGTH OR LONGER- black, navy, or white logo-less socks. Ankle socks unacceptable.</li>
                              							
                              <li>Jacket: black, gray, or dark blue LOGO-LESS jacket or sweatshirt. Student name must
                                 be displayed on the outside of the jacket: All Tech will embroider a sweatshirt with
                                 your name and college logo, or you may purchase a VC name tag for the outside of a
                                 solid black, gray, or dark blue logo-less plain jacket or sweatshirt.
                              </li>
                              						
                           </ol>
                           						
                           <p>Dress Code/Grooming Policy:</p>
                           						
                           <ol>
                              <li>Clean-shaven=no measurable facial hair (except moustache- see below). Student must
                                 be clean shaven- no beards, soul-patches, face/chin/neck clean shaven, sideburns no
                                 longer than earlobe. A moustache is acceptable but it cannot extend beyond the corner
                                 of the mouth. May be sent home and accrue an absence if not clean shaven.
                              </li>
                              							
                              <li>Hair: All students- hair must be a ‘natural’, non-distracting color. Female students-
                                 hair must be pulled back if long enough to do so, cut in a non-distracting, professional
                                 style (no ‘dreadlocks’). Male students- hair must be cut short in a non-distracting,
                                 professional style, not touching the collar- may not be worn pulled back or up (no
                                 ‘dreadlocks’).
                              </li>
                              							
                              <li>Visible tattoos: ALL tattoos must be covered with uniform during class, lab, and clinical
                                 rotations. Student must wear a long sleeved uniform shirt to cover all visible tattoos
                                 on arms. Tattoo covering ‘sleeves’, long sleeved t-shirts under short sleeved uniform
                                 shirt, jackets, or sweatshirts are not acceptable for covering tattoos on arms or
                                 any part of body that would normally be covered with a proper uniform shirt. Only
                                 a long sleeved uniform shirt is acceptable covering for visible tattoos on any part
                                 of the body that would normally be covered with a proper uniform shirt.
                              </li>
                              							
                              <li>Jewelry: All jewelry must be removed- except wedding bands and wrist watches (wrist
                                 watch mandatory for Lab). If student is wearing a necklace, it must not be visible
                                 over the shirt.
                              </li>
                              							
                              <li>All visible body piercings must be REMOVED ANY TIME THE UNIFORM IS WORN- includes
                                 tongue and ALL ear piercings for both male and female students. Flesh colored “spacers”
                                 are NOT acceptable. Please make it a habit to remove visible body piercings BEFORE
                                 putting on the uniform. Failure to remove piercings is a violation of the uniform
                                 and professionalism policy. The student will be penalized as listed in the syllabus.
                              </li>
                              							
                              <li>Acrylic or other “fake” nails are not allowed in class, lab, or clinical rotations.
                                 Fingernail polish not permitted.
                              </li>
                              							
                              <li>Hats not to be worn when in uniform. Sunglasses are not to be worn in class, lab,
                                 or clinical (hospital) setting (on rescue/ambulance rotations- may wear sunglasses
                                 when outside or in vehicle without patient).
                              </li>
                              						
                           </ol>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/emergency-medical-services/emergency-medical-technician.pcf">©</a>
      </div>
   </body>
</html>