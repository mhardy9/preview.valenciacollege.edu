<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Paramedic Technology  | Valencia College</title>
      <meta name="Description" content="Paramedic Technology | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/emergency-medical-services/paramedic.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/emergency-medical-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Paramedic Technology</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/emergency-medical-services/">Emergency Medical Services</a></li>
               <li>Paramedic Technology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Paramedic Technology</h2>
                     				
                     <p>Technical Certificate</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The Certificate in Paramedic Technology is a complete program and also can be the
                        second part of the <a href="/academics/programs/health-sciences/emergency-medical-services/index.php">Associate in Science (A.S.) Degree in Emergency Medical Services Technology (EMS)</a>, which has three parts. The first part of the degree is the <a href="/academics/programs/health-sciences/emergency-medical-services/paramedic.php">Technical Certificate in Emergency Medical Technician</a>, and the third part consists of the General Education and Elective courses required
                        for the degree. To earn this degree, all three parts must be completed. The EMT Technical
                        Certificate must be completed successfully as a major requirement for continuing to
                        the limited access Paramedic Certificate; however, the General Education and Elective
                        courses for the A.S. Degree can be completed at any time.
                     </p>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/emergencymedicalservicestechnology/#certificatetext"> <span class="far fa-laptop"></span>
                           					
                           <h3>Certificate Overview</h3>
                           					
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              						<br><br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/paramedic-technology-program-guide.pdf"> <span class="far fa-calendar"></span>
                           					
                           <h3>Program Length</h3>
                           					
                           <p>EMT TC lasts for 1 semester. If you also complete the Paramedic TC (3 semesters),
                              you only need to take the general education courses to earn your EMS degree.
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           					
                           <h3>Want more information?</h3>
                           					
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              					
                           </p>
                           					</a></div>
                     			
                  </div>
                  			
                  <!--End row-->
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <span class="far fa-dollar-sign"></span>
                           					
                           <h3>Average Salary</h3>
                           					
                           <p>EMT average salaries ranges from $32,000 – $42,000
                              						<br>
                              					
                           </p>
                           					</a></div>
                     
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           					
                           <h3>Career Coach</h3>
                           					
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              						<br>
                              					
                           </p>
                           					</a> 
                     </div>
                     
                     			
                  </div>
                  			
                  <!--End row-->
                  
                  		
               </div>
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Paramedic Technology </h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/paramedic-technology-program-guide.pdf">Current Program Guide</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/paramedic-technology-estimated-costs.pdf">Estimated Program Costs</a></li>
                              							
                              <li><a href="/documents/academics/programs/health-sciences/paramedic-technology-course-planning-guide.pdf">Course Planning Guide </a> 
                              </li>
                              							
                              <li><a href="#faq">FAQs (Frequently Asked Questions)</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li>
                                 								<strong>Application Deadline:</strong> June 15, 2018
                              </li>
                              							
                              <li>
                                 								<strong>Begins:</strong> Fall 2018 
                              </li>
                              							
                              <li>
                                 								<strong>Application:</strong> Download should be available March 2018
                              </li>
                              						
                           </ul>
                           						<br>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <h3>Program Changes</h3>
                        					
                        <table class="table table table-striped cart-list add_bottom_30">
                           						
                           <tr>
                              							
                              <th scope="col">Date Updated</th>
                              							
                              <th scope="col">Effective Date</th>
                              							
                              <th scope="col">Change</th>
                              						
                           </tr>
                           
                           <tr>
                              
                              <td>4/27/17</td>
                              
                              <td>Summer 2018 </td>
                              
                              <td>The summer Paramedic Technology (TC) Program on West Campus will not be available.
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/6/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>
                                 <ul>
                                    								  
                                    <li>Points will no longer be awarded for Overall GPA</li>
                                    
                                    <li>BSC1084 and EMS1010 (Essentials of Human Structure and Functions) will be considered
                                       equivalent pre-requisite courses
                                    </li>
                                    
                                    <li>SPC 1608 (Fundamentals of Speech) will no longer be a requirement for the program</li>
                                    
                                    <li>SLS 1122 (New Student Experience) will be a requirement for the Program</li>
                                    
                                    <li>HSC 1004 (Professions of Caring) will not be accepted for additional points</li>
                                    								  
                                 </ul>
                              </td>
                              
                           </tr>
                           						
                           <tr>
                              
                              <td>
                                 <p>11/8/16 </p>
                                 
                              </td>
                              
                              <td>Summer 2017 </td>
                              
                              <td>
                                 <p>Starting in the summer 2017, we will be offering a Paramedic Technology (TC) Program
                                    on West Campus. 
                                 </p>
                              </td>
                              
                           </tr>
                           					
                        </table>
                        
                        				
                     </div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>Steps for Admission to Program</h4>
                           						
                           <ul class="list_order">
                              							
                              <li>
                                 								<span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    								Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 								are not local and cannot attend the information session, review the <a href="http://valenciacollege.edu/west/health/documents/HealthInformationTechnology.pdf">program guide</a> and read the <a href="http://valenciacollege.edu/west/health/documents/FAQ-Health-Health-Info-Tech.pdf">Frequently Asked
                                    								Questions</a>. 
                              </li>
                              							
                              <li>
                                 								<span>2</span><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Apply</a> to Valencia College. 
                              </li>
                              							
                              <li>
                                 								<span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              							
                              <li>
                                 								<span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 								requirements, you can apply to the program. 
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  		
               </div>
               		<a id="faq"></a>
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Frequently Asked Questions</h2>
                     				
                     <p>Paramedic Technology</p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           						
                           <p>Yes. You must complete the admission process to the college before you submit a Paramedic
                              							program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              							email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              							questions. 
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What courses do I need to take prior to applying to the Paramedic program?</h3>
                           						
                           <p>Complete EMS1010 or BSC1084 Essentials of Human Structure and Function, the Prerequisite
                              for Admission, with a minimum grade of C. EMS1010 or BSC1084 may be in progress at
                              the time of program application but must be completed with a minimum grade of C by
                              the program start date.
                           </p>
                           
                           <p>To be competitive for admission, additional Science and General Education courses
                              should be completed with high grades. See the point system in the <a href="/documents/academics/programs/health-sciences/paramedic-technology-program-guide.pdf">Current Program Guide</a>.
                           </p>
                           
                           <p>BSC2093C and BSC2094C may substitute for EMS1010 or BSC1084. BSC 2094C may be in progress
                              at the time of application and completed with a minimum grade of C by the program
                              start date.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Are the prerequisite courses offered online?</h3>
                           						
                           <p>No. But the General Education courses that award additional points usually are offered
                              online.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           						
                           <p>Yes, the Essentials of Human Structure and Function course is only offered on the
                              West campus
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           						
                           <p>No. The Paramedic program does not require the TEAS.</p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           						
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit; equivalent courses will be posted
                              in your Valencia Atlas account.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the minimum overall GPA to apply to the Paramedic program?</h3>
                           						
                           <p>You must be in ‘Good Standing’ which requires a minimum 2.0 overall GPA, including
                              all undergraduate transfer work.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many students are accepted into the Paramedic program and how often?</h3>
                           						
                           <p>The West Paramedic program accepts approximately 50 students for the Fall term, and
                              the Osceola Paramedic program accepts approximately 30 students for the Spring term.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Do students in the Paramedic program conduct peer-to-peer examinations?</h3>
                           						
                           <p>In the Paramedic Program simulation is used for all hands on skills.</p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-8">
                        					
                        <div class="box_style_2">
                           						
                           <h3>Can students accepted into the Paramedic program be exposed to blood borne pathogens?</h3>
                           						
                           <p>The Paramedic program may have clinical experiences that may expose the student to
                              blood borne pathogens via contact with bodily fluids such as blood and saliva. Students
                              accepted into these programs will be expected to adhere to Centers for Disease Control
                              guidelines regarding the use of Universal Precautions which includes the use of personal
                              protective equipment such as masks, gloves, and eyewear.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How many clinical hours are in the Paramedic program and what credential would I be
                              eligible to sit for upon successful completion?
                           </h3>
                           						
                           <p>The Paramedic program consists of 1,155 hours: 533 lab &amp; clinical hours, 452 lecture
                              hours and 170 field hours. Graduates of the Paramedic program are eligible to sit
                              for the National Registry Paramedic exam to earn certification.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>How successful are your Paramedic graduates in finding employment?</h3>
                           						
                           <p>The five year average Licensure Rate for Paramedic graduates is 91% and the five year
                              average Placement Rate for Paramedic graduates is 90%.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        					
                        <div class="box_style_2">
                           						
                           <h3>What is the schedule like if I am accepted into the EMT program?</h3>
                           						
                           <p>The Paramedic program has both a daytime and night-time program on the West and Osceola
                              campuses and is shift-friendly; its time demands should be considered the equivalent
                              of a fulltime job as students are expected to devote a minimum of 30 hours a week
                              to the program - if a student chooses to work while in the program, a maximum of 30
                              hours a week is recommended due to the demands of the program. Time management is
                              crucial to successful program completion!
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>How do I apply?</h3>
                           
                           <p>Download &amp; fill out the Health Information Technology Application. Return your completed
                              application to
                              Health Sciences Advising along with a non-refundable Health Sciences program application
                              fee of $15
                           </p>
                           
                           <ul>
                              
                              <li>
                                 
                                 <h4>By mail:</h4>
                                 Send the application along with a check or money order payable to Valencia College
                                 to
                                 the address listed below:
                                 
                                 <address class="wrapper_indent">
                                    Valencia College<br>
                                    Business Office<br>
                                    P.O. Box 4913.<br>
                                    Orlando, FL
                                    32802
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 
                                 <h4>In person:</h4>
                                 Make payment in the Business Office on any Valencia Campus &amp; request that the
                                 Business Office forward your program application &amp; fee receipt to the Health Sciences
                                 Advising office at
                                 mail code 4-30 
                              </li>
                              
                           </ul>
                           
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              documentation or requirements can result in denied admission. All items listed on
                              the <a href="/documents/academics/programs/health-sciences/health-information-technology-program-guide.pdf">Program Guide
                                 Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/emergency-medical-services/paramedic.pcf">©</a>
      </div>
   </body>
</html>