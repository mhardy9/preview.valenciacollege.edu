<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nursing Program Tracks  | Valencia College</title>
      <meta name="Description" content="Nursing Program Tracks | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/nursing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Nursing Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Nursing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     
                     <h2>Nursing Programs</h2>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Nursing Associate in Science (A.S.) degree program at Valencia College is a two-year
                        program that prepares you to go directly into a specialized career in the nursing
                        field. Due to the current shortage of skilled nurses, there are tremendous opportunities
                        in nursing. Valencia’s nationally-accredited program is one of the best for preparing
                        men and women for nursing careers, by connecting students directly to situations nurses
                        face today. The program offers classroom curriculum together with real life clinical
                        experiences to help nurses build skills and confidence. Valencia’s Nursing program
                        is limited-access, meaning that admission to the college does not imply acceptance
                        to the Nursing program. Students must apply and be accepted to the program. Upon completion
                        of the program, you will be prepared to take the National Council Licensure Exam to
                        become a registered nurse (RN).
                     </p>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/health-sciences/nursing/traditional-daytime.php"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Nursing Traditional, Daytime">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Nursing Traditional, Daytime
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Nursing Traditional, Daytime<br>
                                    Associate in Science Degree
                                    
                                 </h3>
                                 
                                 <p>This program is designed for students who seek employment in the field of nursing
                                    and who may decide to continue to any Florida public university as a junior to complete
                                    a four-year bachelor’s degree in nursing through a R.N. to B.S.N. (Bachelor of Science
                                    in Nursing) program. Courses for this program are offered during the daytime. A separate
                                    program exists for courses offered during evening &amp; weekend hours.
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/health-sciences/nursing/traditional-daytime.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/health-sciences/nursing/traditional-evening.php"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Nursing Traditional, Evening/Weekend">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Nursing Traditional, Evening/Weekend
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Nursing Traditional, Evening/Weekend<br>
                                    Associate in Science Degree
                                    
                                 </h3>
                                 
                                 <p>This program is designed for students who seek employment in the field of nursing
                                    and who may decide to continue to any Florida public university as a junior to complete
                                    a four-year bachelor’s degree in nursing through a R.N. to B.S.N. (Bachelor of Science
                                    in Nursing) program.  Courses for this program are offered during the evening &amp; weekend
                                    hours. A separate program exists for courses offered during daytime hours.
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/health-sciences/nursing/traditional-evening.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     		
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/health-sciences/nursing/accelerated-track.php"> <img src="/images/academics/programs/health-sciences/nursing-in-action.jpg" alt="Accelerated track in Nursing">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Accelerated track in Nursing (ATN)
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Accelerated track in Nursing (ATN)<br>
                                    Associate in Science Degree
                                    
                                 </h3>
                                 
                                 <p>
                                    This option is designed for LPNs (Licensed Practical Nurses), Paramedics, Registered
                                    Invasive Cardiovascular Specialists (CVTs) and RRTs (Registered Respiratory Therapists)
                                    who want to earn an A.S. degree in Nursing.
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/health-sciences/nursing/accelerated-track.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index"> <img src="/images/academics/programs/health-sciences/nursing-meds.jpg" alt="Nursing - Valencia/UCF Concurrent A.S.N. to B.S.N.">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Valencia/UCF Concurrent A.S.N. to B.S.N. Option
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Valencia/UCF Concurrent A.S.N. to B.S.N. Option<br>
                                    Associate in Science (Valencia) and Bachelor of Science (UCF)
                                    
                                 </h3>
                                 
                                 <p>
                                    This option provides a unique dual enrollment opportunity, allowing students to seek
                                    the Associate of Science in Nursing (A.S.N.) from Valencia and the Bachelor of Science
                                    in Nursing (B.S.N.) from the University of Central Florida simultaneously. Students
                                    must be enrolled in a Traditional Nursing Track to add this option.
                                    
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="http://www.nursing.ucf.edu/admissions/undergraduate-programs/dual-enrollment-concurrent-asn-bsn/valencia-college/index" class="button-outline">For more information, visit UCF’s College of Nursing website.</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     	
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/health-sciences/nursing/bachelors.php"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Nursing Traditional, Evening/Weekend">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Bachelor of Science in Nursing
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Nursing<br>
                                    Bachelor Science Degree
                                    
                                 </h3>
                                 
                                 <p>Nurses play an important role in healthcare teams and are responsible for many aspects
                                    of patient care. A bachelor’s degree in Nursing is designed to expand your knowledge
                                    on integrated care, build nursing skills under the guidance of experienced practitioners
                                    and prepare you for advanced positions within the medical field.
                                    
                                 </p>
                                 
                                 <p>Valencia will add a Bachelor of Science Degree in Nursing in Fall 2018.</p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/health-sciences/nursing/bachelors.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     	
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/nursing/index.pcf">©</a>
      </div>
   </body>
</html>