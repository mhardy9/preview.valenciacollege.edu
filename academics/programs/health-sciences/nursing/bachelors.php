<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Bachelor of Science in Nursing  | Valencia College</title>
      <meta name="Description" content="Bachelor of Science in Nursing | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/nursing/bachelors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Bachelor of Science in Nursing</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/nursing/">Nursing</a></li>
               <li>Bachelor of Science in Nursing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Nursing</h2>
                     				
                     <p>Bachelor of Science (B.S.) Degree</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <h3>Valencia will add a Bachelor of Science Degree in Nursing in Fall 2018.</h3>
                     				
                     <p>Nurses play an important role in healthcare teams and are responsible for many aspects
                        of patient care. A bachelor’s degree in Nursing is designed to expand your knowledge
                        on integrated care, build nursing skills under the guidance of experienced practitioners
                        and prepare you for advanced positions within the medical field.
                     </p>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/forms/west/health/contact.cfm"> <span class="far fa-question-circle"></span>
                           					
                           <h3>Want more information?</h3>
                           					
                           <p>Our Health Science Advising Office is available to answer your questions, provide
                              details about the program and courses, and to help you understand the admission requirements.
                              Fill out this form to have the program staff contact you.
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4 box_feat"><span class="far fa-share-alt fa-5x v-red"></span>
                        					
                        <h3 class="h4 text-uppercase">Degrees that Transfer to this Bachelor's</h3>
                        					
                        <ul>
                           						
                           <li><a href="/academics/programs/health-sciences/nursing/traditional-daytime.php">Nursing Traditional, Daytime</a></li>
                           						
                           <li><a href="/academics/programs/health-sciences/nursing/traditional-evening.php">Nursing Traditional, Evening/Weekend</a></li>
                           					
                        </ul><br></div>
                     				
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://valenciacollege.emsicareercoach.com/"> <span class="far fa-life-ring"></span>
                           					
                           <h3>Career Coach</h3>
                           					
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              						<br>
                              					
                           </p>
                           					</a> 
                     </div>
                     
                     			
                  </div>
                  			
               </div>
               			
               <!--End row-->
               			
               
               		
               <div class="container margin-60 box_style_1">
                  			
                  <div class="main-title">
                     				
                     <h2>Admission into Bachelor of Science in Nursing </h2>
                     				
                     <p></p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="#">Current Program Guide</a></li>
                              							
                              <li><a href="#">Current Program Requirements</a></li>
                              							
                              <li><a href="#">FAQs (Frequently Asked Questions)</a></li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="col-md-6 col-sm-6">
                        					
                        <div class="box_style_4">
                           						
                           <h3>Application Information</h3>
                           						
                           <ul>
                              							
                              <li><strong>Application Deadline:</strong> 
                              </li>
                              							
                              <li><strong>Begins:</strong> Fall 2018
                              </li>
                              							
                              <li><strong>Application:</strong> <a href="#">Download Bachelor of Science in Nursing Application</a></li>
                              						
                           </ul>
                           						<br>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-8 col-sm-8">
                        					
                        <div class="row">
                           						
                           <h3>Program Changes</h3>
                           						
                           <table class="table table table-striped cart-list add_bottom_30">
                              							
                              <tr>
                                 								
                                 <th scope="col">Date Updated</th>
                                 								
                                 <th scope="col">Effective Date</th>
                                 								
                                 <th scope="col">Change</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>No Program Changes</td>
                                 							
                              </tr>
                              
                              						
                           </table>
                           					
                        </div>
                        					
                        <div class="row">
                           						
                           <h3>Bachelor of Science in Nursing Department</h3>
                           						
                           <ul class="list_staff">
                              							
                              <li>
                                 								
                                 <figure><img src="_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                                 								
                                 <h4>Risë Sandrowitz , MSN</h4>
                                 								
                                 <p>Dean of Nursing</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                                 								
                                 <h4>Health Sciences Advising</h4>
                                 								
                                 <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        
                        				
                     </div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <div class="box_style_4">
                           						
                           <h4>7 Steps to Your Bachelor's Degree</h4>
                           						
                           <ul class="list_order">
                              							
                              <li><span>1</span>Complete your associate degree in Nursing (including all prerequisites).
                              </li>
                              							
                              <li><span>2</span>Hold professional certification in your respective discipline.
                              </li>
                              							
                              <li><span>3</span>Submit the online Valencia College application for the bachelor’s program, as well
                                 as all college transcripts, and pay the $35 application fee. This fee is waived for
                                 active Valencia College students who hold an Associate in Arts degree from Valencia
                                 or an Associate in Science degree from Valencia to a bachelor degree at Valencia.
                              </li>
                              							
                              <li><span>4</span>Once you’ve been accepted to Valencia College as a candidate for B.S. Nursing, submit
                                 the Bachelor of Science in Nursing Bachelor’s Program application, available through
                                 Valencia’s Health Sciences Advising department, and contact the program advisor.
                              </li>
                              							
                              <li><span>5</span>Once you’ve been accepted to the B.S. degree, complete the online bachelor’s orientation.
                              </li>
                              
                              							
                              <li><span>6</span>Register for classes.
                              </li>
                              
                              							
                              <li><span>7</span>Graduate with your B.S. degree. 
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  			
                  <!--End row --> 
                  
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/nursing/bachelors.pcf">©</a>
      </div>
   </body>
</html>