<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Accelerated Track in Nursing  | Valencia College</title>
      <meta name="Description" content="Accelerated Track in Nursing | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/nursing/accelerated-track.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/nursing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Accelerated Track in Nursing</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/nursing/">Nursing</a></li>
               <li>Accelerated Track in Nursing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30">
                  			
                  <div class="main-title">
                     				
                     <h2>Accelerated Track in Nursing</h2>
                     				
                     <p>Associate in Science (A.S.)</p>
                     			
                  </div>
                  			
                  <div class="row box_style_1">
                     				
                     <p>The Nursing Associate in Science (A.S.) degree program at Valencia College is a two-year
                        program that prepares you to go directly into a specialized career in the nursing
                        field. Due to the current shortage of skilled nurses, there are tremendous opportunities
                        in nursing. Valencia’s nationally-accredited program is one of the best for preparing
                        men and women for nursing careers, by connecting students directly to situations nurses
                        face today. The program offers classroom curriculum together with real life clinical
                        experiences to help nurses build skills and confidence. Valencia’s Nursing program
                        is limited-access, meaning that admission to the college does not imply acceptance
                        to the Nursing program. Students must apply and be accepted to the program. Upon completion
                        of the program, you will be prepared to take the National Council Licensure Exam to
                        become a registered nurse (RN).
                     </p>
                     				
                     <p>The Accelerated Track in Nursing option is designed for LPNs (Licensed Practical Nurses),
                        Paramedics, Registered Invasive Cardiovascular Specialists (CVTs) and RRTs (Registered
                        Respiratory Therapists) who want to earn an A.S. degree in Nursing.
                     </p>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/nursing/nursingadvancedstandingtrack/"> <i class="far fa-laptop"></i>
                           					
                           <h3>Program Overview</h3>
                           					
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              						<br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/documents/academics/programs/health-sciences/nursing-advanced-track-advanced-track-course-planning-guide.pdf"> <i class="far fa-calendar"></i>
                           					
                           <h3>Program Length</h3>
                           					
                           <p>Program lasts for 3 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              						<br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/asdegrees/credit_alternative.php"> <i class="far fa-share-square"></i>
                           					
                           <h3>Alternative Credit</h3>
                           					
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? You may receive
                              						college credit for approved Industry Certifications.
                           </p>
                           					</a></div>
                     			
                  </div>
                  
                  			
                  <div class="row">
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://www.aftercollege.com/groups/ccenter.asp?fct=1&amp;ID=-82501801"> <i class="far fa-dollar-sign"></i>
                           					
                           <h3>Job Opportunities</h3>
                           					
                           <p>Targeted job listings for Valencia Nursing students &amp; alumni, provided by AfterCollege
                              Healthcare.<br><br>
                              					
                           </p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					<a class="box_feat" href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=health+science&amp;Clusters=8&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all"> <i class="far fa-life-ring"></i>
                           						
                           <h3>Career Coach</h3>
                           						
                           <p>Explore Health Science related careers, salaries and job listings. <br><br>
                              
                              						
                           </p>
                           					</a> 
                     </div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.php"> <i class="far fa-question-circle"></i>
                           					
                           <h3>Want more information?</h3>
                           					
                           <p>Attend a Health Sciences Information Session for an overview of the limited access
                              Health Science programs
                              						at Valencia College and the procedures for enrolling in them.
                           </p>
                           					</a></div>
                     			
                  </div>
                  			
                  <div class="container margin-60 box_style_1">
                     				
                     <div class="main-title">
                        					
                        <h2>Admission into Nursing Traditional, Daytime </h2>
                        					
                        <p></p>
                        				
                     </div>
                     				
                     <div class="row">
                        					
                        <div class="col-md-6 col-sm-6">
                           						
                           <div class="box_style_4">
                              							
                              <h3>Important Information</h3>
                              							
                              <ul>
                                 								
                                 <li><a href="/documents/academics/programs/health-sciences/nursing-advanced-track-program-guide.pdf">Current Program Guide</a></li>
                                 								
                                 <li><a href="/documents/academics/programs/health-sciences/nursing-advanced-track-estimated-costs.pdf">Estimated Program Costs</a></li>
                                 								
                                 <li>
                                    									<a href="/documents/academics/programs/health-sciences/nursing-advanced-track-course-planning-guide.pdf">Course Planning Guide </a> 
                                 </li>
                                 								
                                 <li><a href="http://net1.valenciacollege.edu/future-students/nursing/nursing-frequently-asked-questions/" target="_blank">FAQs (Frequently Asked Questions)</a></li>
                                 							
                              </ul>
                              						
                           </div>
                           					
                        </div>
                        					
                        <div class="col-md-6 col-sm-6">
                           						
                           <div class="box_style_4">
                              							
                              <h3>Application Information</h3>
                              							
                              <p>Applications are accepted on an ongoing basis. Although applications submitted before
                                 the priority application deadlines are more likely to be processed for the upcoming
                                 term, applications can be submitted after the priority deadline. Entry into the upcoming
                                 term is based on capacity at the time of application. If the upcoming term is at capacity,
                                 eligible applicants will be offered acceptance into the next term.
                              </p>
                              							
                              <ul>
                                 								
                                 <li>
                                    									September 15, 2018 (for Spring 2019) <br>
                                    									January 15, 2018 (for Summer 2018) <br>
                                    									May 15, 2018 (for Fall 2018)
                                 </li>
                                 								
                                 <li>
                                    									Begins: Fall, Spring, Summer
                                 </li>
                                 								
                                 <li>
                                    									Application: <a href="/documents/academics/programs/health-sciences/Application-2018-Nursing-ATN.pdf">Download Application</a>  
                                 </li>
                                 							
                              </ul>
                              
                              						
                           </div>
                           					
                        </div>
                        				
                     </div>
                     				
                     <div class="row">
                        					
                        <div class="col-md-8 col-sm-8">
                           						
                           <h3>Program Changes</h3>
                           						
                           <table class="table table table-striped add_bottom_30">
                              							
                              <tr>
                                 								
                                 <th scope="col">Date Updated</th>
                                 								
                                 <th scope="col">Effective Date</th>
                                 								
                                 <th scope="col">Change</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <th>&nbsp;</th>
                                 								
                                 <th>Fall 2017 </th>
                                 								
                                 <th>
                                    <p>HUN2202 is required; however, for those who need HUN2015 (Diet Therapy) must meet
                                       the new prerequisite effective fall 2016. 
                                    </p>
                                    									
                                    <p>The prerequisite for HUN2015 (Diet Therapy) will be HUN1201 (Essentials of Nutrition).
                                       Note: HUN1001 will no longer meet the prerequisite for HUN2015.
                                    </p>
                                 </th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>8/28/2017</td>
                                 								
                                 <td>Fall 2017</td>
                                 								
                                 <td>
                                    <p>Students who have completed a college Nutrition course of two or more credits (e.g.
                                       HUN 1201) that did not include Diet Therapy may request permission in the Health Sciences
                                       Advising Office (email HealthScienceApplications@valenciacollege.edu) to complete
                                       the one-credit HUN 2015 Diet Therapy for Health Care Professionals, which may be in
                                       progress at the time of program application, but must be completed with a minimum
                                       grade of C (or minimum grade needed to maintain 2.5 general education prerequisite
                                       GPA) by the program start date.  If taken, HUN 2015 and the initial Nutrition course
                                       will combine to substitute for HUN 2202 unless the student has already attempted the
                                       required HUN2202 course. Therefore, if a student has HUN2202 on his/her record, then
                                       HUN2015 with the initial nutrition course is not an option to meet program entry requirements.
                                       
                                    </p>
                                    									
                                    <p>There is a typo on the printed 2017-2018 ATN Program Guide (page 2).  If you have
                                       downloaded or received this document before 8/28/2017, please note the changes that
                                       have been updated
                                    </p>
                                    									
                                    <p>Correction: (or minimum grade needed to maintain 2.5 overall GPA)</p>
                                    									
                                    <p>For those who are in progress with HUN2015 at another college must provide proof of
                                       enrollment in the course (name and institution must be visible) by submitting a copy
                                       of course schedule. 
                                    </p>
                                    									
                                    <p>You can email documentation to: Healthscienceapplications@valenciacollege.edu. </p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>3/8/17</td>
                                 								
                                 <td>Fall 2017 </td>
                                 								
                                 <td>
                                    <p>NEW name: Accelerated Track in Nursing (ATN) formally called Advanced Standing Track
                                       (AVS)
                                    </p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>8/17/16</td>
                                 								
                                 <td>Fall 2017 </td>
                                 								
                                 <td>
                                    <p>Students who have completed HUN1001 prior to Fall 2016 are permitted to enroll in
                                       HUN2015 for fall 2016 through summer 2017.
                                    </p>
                                    									
                                    <p>Effective Fall 2017, all students must have completed HUN1201 with a minimum grade
                                       of "C" to satisfy the prerequisite for HUN2015.
                                    </p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>8/4/16 (posted on website) </td>
                                 								
                                 <td>9/15/17</td>
                                 								
                                 <td>
                                    <p>TEAS for program entry after September 15, 2017 must submit the new version: ATI TEAS.
                                       NOTE: On September 16th, 2017 and  thereafter only ATI TEAS will be accepted. The
                                       minimum composition score of *64.9% is required.&nbsp; Scores are valid for 5 years. *Subject
                                       to change.&nbsp; You must meet required score in effect at the time you apply to the program.&nbsp;
                                       
                                    </p>
                                    									
                                    <p>TEAS information is available at <a href="/students/assessments/teas/">Valencia's TEAS site </a>and <a href="https://www.atitesting.com/Home.aspx">ATItesting.com </a></p>
                                 </td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>9/4/14 (posted on website) </td>
                                 								
                                 <td>8/1/12</td>
                                 								
                                 <td>
                                    <p>TEAS minimum Composite score of *64.9% required. Only TEAS Version 5 (V) is accepted.
                                       TEAS scores are valid for 5 years.*Subject to change. You must meet required score
                                       in effect at the time you apply to the program. 
                                    </p>
                                    									
                                    <p>TEAS information is available at <a href="/students/assessments/teas/">Valencia's TEAS site </a>and &nbsp;<a href="https://www.atitesting.com/Home.aspx">ATItesting.com&nbsp; </a></p>
                                 </td>
                                 							
                              </tr>
                              
                              
                              						
                           </table>
                           						
                           <ul class="list_staff">
                              							
                              <li>
                                 								
                                 <figure><img src="_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                                 								
                                 <h4>Risë Sandrowitz , MSN</h4>
                                 								
                                 <p>Dean of Nursing</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                                 								
                                 <h4>Health Sciences Advising</h4>
                                 								
                                 <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4 col-sm-4">
                           						
                           <div class="box_style_4">
                              							
                              <h4>Steps for Admission to Program</h4>
                              							
                              <ul class="list_order">
                                 								
                                 <li>
                                    									<span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                       									Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                    									are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                       									Questions</a>. 
                                 </li>
                                 								
                                 <li>
                                    									<span>2</span><a href="#">Apply</a> to Valencia College. 
                                 </li>
                                 								
                                 <li>
                                    									<span>3</span>Complete required general education courses before applying to the program.
                                 </li>
                                 								
                                 <li>
                                    									<span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                    									requirements, you can apply to the program. 
                                 </li>
                                 							
                              </ul>
                              						
                           </div>
                           					
                        </div>
                        				
                     </div>
                     
                     			
                  </div> 
                  
                  			
                  <div class="row">
                     				
                     <div class="main-title">
                        					
                        <h2>General Nursing Program Information</h2>
                        				
                     </div>
                     				
                     <p>The Valencia Nursing Program was organized in 1970 with the first nursing class of
                        44 students admitted in September 1971. The nursing program was implemented in response
                        to the closure of a local diploma program offered by Orange Memorial Hospital, now
                        Orlando Regional Health Care System. The program added a Licensed Practical Nurse
                        bridge option in the Fall of 1983. Today, Valencia's program is one of the best for
                        preparing men and women for a nursing career, by connecting students directly to situations
                        nurses face today. The program offers real-life lessons to help nurses build skills
                        and confidence.
                     </p>
                     
                     				
                     <p> Students submit applications once all of the admission criteria and <a href="/students/assessments/teas/">Test of Essential Academic Skills</a> requirements have been met. Valencia admits 120 students to the Traditional Nursing
                        Track in the Fall term, 120 students in the Spring Term, and 70 students in the Summer
                        Term.
                     </p>
                     				
                     <p> First-term Nursing students select either West or Osceola Campus from the available
                        offerings at the time of course registration. Nursing I and II are taught on Osceola
                        Campus; all other Nursing courses are taught only on West Campus. The Nursing Program
                        is full-time during the week. We do not offer an evening or weekend program. 
                     </p>
                     				
                     <p>The Valencia Nursing program, both the Traditional Track and the Accelerated Track
                        in Nursing (ATN), have been coordinated with local health care agencies, the State
                        Department of Education and the State Board of Nursing and is accredited by the Accreditation
                        Commission for Education in Nursing (ACEN).
                     </p>
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        					<img src="/images/academics/programs/health-sciences/ACEN-Seal-Color-Web.jpg" class="img-responsive">
                        				
                     </div>
                     				
                     <div class="col-md-8">
                        					
                        <h3>Accreditation</h3>
                        					
                        <p>The Valencia College Nursing Program has been coordinated with local health care agencies,
                           the State Department of Education and the State Board of Nursing. 
                        </p>
                        					
                        <p>While all teaching programs must be approved by the State Board of Nursing, Valencia's
                           program is also accredited by the Accreditation Commission for Education in Nursing
                           (ACEN), which certifies that the program meets both the highest state and national
                           standards. Many universities, including the armed forces, require transfer students
                           to have completed their associate's degree at an ACEN-accredited institution. Students
                           who complete the program are prepared to take the National Council Licensure Exam
                           (NCLEX-RN) and can go on to work in a nursing career and/or pursue higher degrees.
                           
                        </p>
                        					
                        <p>The Valencia College Nursing Program is approved by the State Board of Nursing and
                           accredited by the &nbsp;Accreditation Commission for Education in Nursing (ACEN).
                        </p>
                        
                        					
                        <p>3343 Peachtree Road NE, Suite 850 <br>
                           						Atlanta, GA 30326<br>
                           						<a href="http://acenursing.org/">http://acenursing.org/</a><br>
                           						Phone:&nbsp; 404-975-5000<br>
                           						Fax: &nbsp;404-975-5020
                        </p>
                        				
                     </div>
                     
                     			
                  </div>
                  			
                  <div class="row">
                     				
                     <div class="col-md-4">
                        <h3>Statement of Philosophy</h3>
                        					
                        <p>The Valencia College Nursing faculty believes learning is a lifelong process. Faculty
                           facilitates a collaborative, inclusive, safe, and supportive learning environment
                           promoting nursing reasoning, clinical decision making, and compassionate caring among
                           a diverse student population. The Valencia College Nursing faculty promotes a culturally
                           competent nursing workforce by exemplifying for students the values of individual
                           knowledge and personal beliefs. 
                        </p>
                        
                        					
                        <p>The Valencia College Nursing faculty believes in multiple pathways to access nursing
                           education.The Nursing Program provides students with opportunities for academic excellence,
                           personal growth, and career development. The Nursing Program is founded on the values
                           of honesty, integrity, professionalism, high ethical standards, and civic responsibility.
                        </p>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        <h3>Program Educational Outcomes </h3>
                        					
                        <p>Evidence of success will be demonstrated as the Valencia College Nursing graduate’s
                           ability to:
                        </p>
                        
                        					
                        <ul>
                           						
                           <li>Foster open communication, mutual respect and shared decision-making to achieve quality
                              patient outcomes with respect to value, safety, preferences, needs, and diversity
                              for patient family and multidisciplinary teams. (T, V, C, A).
                           </li>
                           						
                           <li>Integrate clinical decision-making that demonstrates evidence- based practice, competent
                              delivery of patient care, and coordination of multidisciplinary teams to achieve safe
                              patient centered outcomes (T, V, C, A).
                           </li>
                           						
                           <li>Utilize technology to communicate and promote coordination of information to mitigate
                              errors, apply knowledge when managing resources, and facilitate safe patient care.
                              (T, V, C, A). 
                           </li>
                           
                           						
                           <li>Promote lifelong learning and professionalism through exploration of evidence-based
                              practice and professional development. (T, V, C, A). 
                           </li>
                           					
                        </ul>
                        					
                        <p>* Think (T), Value (V), Communicate (C), Act (A)</p>
                        				
                     </div>
                     				
                     <div class="col-md-4">
                        <h3>Mission</h3>
                        					
                        <p>The Associate of Science in Nursing degree program at Valencia College prepares students
                           for RN licensure, employment in healthcare, and the pursuit of advanced degrees in
                           nursing by promoting academic excellence and high ethical standards.  
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/nursing/accelerated-track.pcf">©</a>
      </div>
   </body>
</html>