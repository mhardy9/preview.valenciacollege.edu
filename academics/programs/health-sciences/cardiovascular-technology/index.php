<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Cardiovascular Technology  | Valencia College</title>
      <meta name="Description" content="Cardiovascular Technology | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/cardiovascular-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/cardiovascular-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Cardiovascular Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li>Cardiovascular Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Cardiovascular Technology</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     
                     <p>The Cardiovascular Technology Associate in Science (A.S.) degree at Valencia College
                        is a two-year program that prepares you to go directly into a specialized career as
                        a cardiovascular technologist.
                     </p>
                     
                     <p>As a cardiovascular technologist, you would work side-by-side with cardiologists and
                        physicians to diagnose and treat heart disease and vascular problems. You would also
                        serve as an integral member of the cardiac catheterization laboratory team, whose
                        primary role is to conduct tests on pulmonary or cardiovascular systems of patients
                        and assist with electrocardiograms, heart catheterizations and similar tests. Valencia’s
                        program provides students with the latest training available for this challenging
                        and rewarding career.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/cardiovasculartechnology/#programrequirementstext"> <i class="far fa-laptop"></i>
                           
                           <h3>Program Overview</h3>
                           
                           <p>Review course descriptions, important dates and deadlines and other programs details
                              in the official college catalog.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/health-sciences/cardiovascular-technology/course-guide.html"> <i class="far fa-calendar"></i>
                           
                           <h3>Program Length</h3>
                           
                           <p>Program lasts for 5 semesters, after all the pre-admission course requirements have
                              been met.<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/asdegrees/credit_alternative.html"> <i class="far fa-share-alt"></i>
                           
                           <h3>Alternative Credit</h3>
                           
                           <p>Did you know that you may already be eligible to receive college credit toward this
                              program? You may receive
                              college credit for approved Industry Certifications.
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="#"> <i class="far fa-dollar-sign"></i>
                           
                           <h3>Average Salary &amp; Placement</h3>
                           
                           <p>Valencia’s A.S. Degree and Certificate programs placement rate ranges between 90%
                              – 95%. Cardiovascular Technology 
                              average salaries ranges from $40,000 - $50,000<br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                     <div class="col-md-4 col-sm-4">
                        <a class="box_feat" href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=health+science&amp;Clusters=8&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all"> <i class="far fa-life-ring"></i>
                           
                           <h3>Career Coach</h3>
                           
                           <p>Explore Health Science related careers, salaries and job listings. <br>
                              <br>
                              <br>
                              <br>
                              <br>
                              
                           </p>
                           </a> 
                     </div>
                     
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/departments/health-sciences/advising/information-sessions.html"> <i class="far fa-question-circle"></i>
                           
                           <h3>Want more information?</h3>
                           
                           <p>Attend a Health Sciences Information Sessions for an overview of the limited access
                              Health Science programs
                              at Valencia College and the procedures for enrolling in them. <br>
                              <br>
                              
                           </p>
                           </a></div>
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Admission into Cardiovascular Technology </h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Important Information</h3>
                           
                           <ul>
                              
                              <li><a href="/documents/academics/programs/health-sciences/cardiovascular-technology-program-guide.pdf">Current Program Guide</a></li>
                              
                              <li><a href="estimated-costs.html">Estimated Program Costs</a></li>
                              
                              <li>
                                 <a href="course-guide.html">Course Planning Guide </a> 
                              </li>
                              
                              <li><a href="index.html#FAQ">FAQs (Frequently Asked Questions)</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-6 col-sm-6">
                        
                        <div class="box_style_4">
                           
                           <h3>Application Information</h3>
                           
                           <ul>
                              
                              <li>
                                 <strong>Application Deadline:</strong> May 15, 2017
                              </li>
                              
                              <li>
                                 <strong>Begins:</strong> Fall 2017
                              </li>
                              
                              <li>
                                 <strong>Application:</strong> Download should be available March 2018
                              </li>
                              
                           </ul>
                           <br>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-8 col-sm-8">
                        
                        <h3>Program Changes</h3>
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <tr>
                              
                              <th scope="col">Date Updated</th>
                              
                              <th scope="col">Effective Date</th>
                              
                              <th scope="col">Change</th>
                              
                           </tr>      
                           
                           
                           <tr>
                              
                              <td>3/20/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>International Students are eligible to apply for this
                                 Health Sciences Program. <a href="International-Student-Eligibility.cfm">Click here for details.</a>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>3/6/17</td>
                              
                              <td>Fall 2017</td>
                              
                              <td>
                                 <ul>
                                    
                                    <li>The overall GPA Admission Points will not be in consideration for the total admission</li>
                                    
                                    <li>Points will no longer be awarded for Overall GPA </li>
                                    
                                    <li>HSC 1004 (Professions of Caring) points increased: A: 5, B: 3, C: 1. Note:SLS 1122
                                       points will remain the same--A: 3, B:2, C: 1 
                                    </li>
                                    
                                 </ul>
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>12/7/16</td>
                              
                              <td>Fall 2017 </td>
                              
                              <td>
                                 
                                 <p><strong>NEW process for Fall 2017 CVT Program</strong></p>
                                 
                                 <p>In early to mid-June, the Health Sciences Admission Committee will conduct a Panel
                                    Interview in the selection of qualified students to be admitted to the Cardiovascular
                                    Technology Program (CVT). Applicants with sufficient points will receive an ATLAS
                                    email in late May with specific dates and related interview information. Students
                                    can earn a maximum of 26 interview points through this process. (All other program
                                    course requirements and admission points will remain the same).
                                 </p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>8/4/16 (posted on website) </td>
                              
                              <td>9/15/17</td>
                              
                              <td>
                                 
                                 <p>TEAS for program entry after September 15, 2017 must submit the new version: ATI TEAS.&nbsp;
                                    The minimum composition score of *58.7% is required.&nbsp; Scores are valid for 5 years.
                                    *Subject to change.&nbsp; You must meet required score in effect at the time you apply
                                    to the program.&nbsp; 
                                 </p>
                                 
                                 <p>TEAS information is available at <a href="http://valenciacollege.edu/assessments/teas/">valenciacollege.edu/assessments </a>and <a href="https://www.atitesting.com/Home.aspx">ATItesting.com </a> 
                                 </p>
                                 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              <td colspan="3" align="right"><a href="archive-changes.php">See Archived Changes</a></td>
                           </tr>
                           
                        </table>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/jamy-chulak.jpg" alt="No Photo Available" class="img-circle"></figure>
                              			
                              <h4>Jamy Chulak, M.S., RRT</h4>
                              
                              <p>Dean of Allied Health</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="Default Female Avatar" class="img-circle"></figure>
                              
                              <h4>Sarah Powers</h4>
                              
                              <p>Chair of Cardiovascular Technology Program</p>
                              
                           </li>
                           
                           <li>
                              
                              <figure><img src="/images/academics/programs/health-sciences/health-sciences-advising-avatar.png" alt="Health Sciences Advising" class="img-circle"></figure>
                              
                              <h4>Health Sciences Advising</h4>
                              
                              <p><a href="mailto:healthscienceadvising@valenciacollege.edu">Email Us</a></p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h4>Steps for Admission to Program</h4>
                           
                           <ul class="list_order">
                              
                              <li>
                                 <span>1</span>Attend a <a href="/academics/departments/health-sciences/advising/information-sessions.php">Program
                                    Information Session</a>. Please bring a copy of the program guide for your desired track with you. If you
                                 are not local and cannot attend the information session, review the <a href="#">program guide</a> and read the <a href="#">Frequently Asked
                                    Questions</a>. 
                              </li>
                              
                              <li>
                                 <span>2</span><a href="#">Apply</a> to Valencia College. 
                              </li>
                              
                              <li>
                                 <span>3</span>Complete required general education courses before applying to the program.
                              </li>
                              
                              <li>
                                 <span>4</span>Once your transcripts have been received and evaluated, and if you meet eligibility
                                 requirements, you can apply to the program. 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               
               <div class="container_gray_bg">
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        
                        <h3>Bachelor of Science Degree</h3>
                        
                        <p>Students with a completed Associate degree in Respiratory Care, Cardiovascular Technology,
                           or Cardiopulmonary Technology may continue their education by pursuing a Bachelor
                           of Science degree in Cardiopulmonary Sciences at Valencia College. The Cardiopulmonary
                           Sciences professions are challenging and growing professions with career opportunities
                           in areas such ascardiopulmonary diagnostics, cardiopulmonary rehabilitation, community
                           health, and polysomnography. With professional experience and additional education
                           at the baccalaureate level, opportunities for management and education career options
                           are enhanced.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Cardiovascular Tech Videos</h2>
                     
                  </div>
                  	  
                  <section class="grid">
                     	  
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <figure><img src="/images/academics/programs/health-sciences/Cardiovascular-Video-Preview-1.png" alt="An Inside Look at Cardiovascular Technology">
                              
                              <figcaption>
                                 
                                 <div class="caption-content"><a href="https://www.youtube.com/watch?v=5D01m7XEG-k" class="video_pop" title="An Inside Look at Cardiovascular Technology"> <i class="far fa-film" aria-hidden="true"></i>
                                       
                                       <p>An Inside Look at Cardiovascular Technology</p>
                                       </a></div>
                                 
                              </figcaption>
                              
                           </figure>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <figure><img src="/images/academics/programs/health-sciences/Cardiovascular-Video-Preview-2.png" alt="Electrophysiological Study Catheter Ablation">
                              
                              <figcaption>
                                 
                                 <div class="caption-content"><a href="http://www.kaltura.com/tiny/wyto2" class="video_pop" title="Electrophysiological Study Catheter Ablation"> <i class="far fa-film" aria-hidden="true"></i>
                                       
                                       <p>Electrophysiological Study Catheter Ablation</p>
                                       </a></div>
                                 
                              </figcaption>
                              
                           </figure>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <figure><img src="/images/academics/programs/health-sciences/Cardiovascular-Video-Preview-3.png" alt="Cardiac Cath with Angioplasty">
                              
                              <figcaption>
                                 
                                 <div class="caption-content"><a href="http://www.kaltura.com/tiny/pdpbq" class="video_pop" title="Cardiac Cath with Angioplasty"> <i class="far fa-film" aria-hidden="true"></i>
                                       
                                       <p>Cardiac Cath with Angioplasty</p>
                                       </a></div>
                                 
                              </figcaption>
                              
                           </figure>
                           
                        </div>
                        
                     </div>
                     	  
                  </section>
                  		
               </div>
               
               <div class="container_gray_bg">
                  
                  <div class="container margin-60 ">
                     
                     <div class="row">
                        
                        <p>The Cardiovascular Technology Program was established in 2002 and graduated its inaugural
                           class of students in 2004.  The program educates and prepares the student to become
                           an Invasive Cardiovascular Specialist known as a Cardiovascular Technologist (CVT).
                           The Invasive Cardiovascular Specialist is a health care professional and an integral
                           member of the cardiac catheterization and electrophysiology laboratory teams.  The
                           CVT's primary role is to perform, at the direction of a qualified physician, technical
                           procedures for the diagnosis and treatment of cardiovascular injury and disease. 
                           The CVT is trained in the competent use of specific high-technology equipment, normal
                           and abnormal cardiovascular anatomy, physiology, pathophysiology, electrophysiology,
                           hemodynamic monitoring, sterile technique, radiation safety, and patient care techniques.
                           
                        </p>
                        
                        <p>Upon graduation, positions are available in diagnostic and interventional cardiac
                           catheterization labs in acute care hospitals, outpatient facilities, and privately-owned
                           clinics. With professional experience and additional education, career opportunities
                           also are available in electrophysiology, echocardiography, management, education,
                           marketing and sales.  Graduates are eligible to take the national Registered Invasive
                           Cardiovascular Specialist exam administered by Cardiovascular Credentialing International.
                        </p>
                        
                        <p>The program is a limited access, two-year Associate in Science degree program.</p>
                        
                        <p>The program is accredited by the Joint Review Committee on Education in Cardiovascular
                           Technology (JRC-CVT) and the Commission on Accreditation of Allied Health Education
                           Programs (CAAHEP) located at 1361 Park Street, Clearwater, FL 33756, phone 727-210-2350
                           (www.CAAHEP.org).
                        </p>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               
               <div class="container margin-60 box_style_1">
                  
                  <div class="main-title">
                     
                     <h2>Career Outlook</h2>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h3>Learn How To</h3>
                           
                           <ul>
                              
                              <li>Assist in the diagnosis and treatment of cardiac disease and blood vessel ailments</li>
                              
                              <li>Assist cardiologists with cardiac catheterization and balloon angioplasty procedures</li>
                              
                              <li>Monitor blood pressure and heart rates of patients</li>
                              
                              <li>Work closely with physicians in caring for cardiac patients</li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h3>Job Outlook</h3>
                           
                           <p>Exciting career opportunities are available in diagnostic and interventional cardiac
                              catheterization labs in acute care hospitals, outpatient facilities and privately-owned
                              clinics.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class="box_style_4">
                           
                           <h3>Potential Careers</h3>
                           
                           <ul class="list_4">
                              
                              <li>Cardiovascular Technologist</li>
                              
                              <li>Cardiovascular Specialist</li>
                              
                              <li>Cardiovascular Technician</li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  	  
                  <h3>Program Outcomes</h3>
                  		 
                  <table class="table table">
                     
                     <tr>
                        
                        <th scope="col">&nbsp;</th>
                        
                        <th scope="col">Student Retention </th>
                        
                        <th scope="col">Graduate National RCIS Exam Pass Rate </th>
                        
                        <th scope="col">Graduate Positive Job Placement <br>
                           (1 Year post Graduation) 
                        </th>
                        
                     </tr>
                     
                     <tr>
                        
                        <th scope="row">Class of 2012 </th>
                        
                        <td>47%</td>
                        
                        <td>100%</td>
                        
                        <td>100%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <th scope="row">Class of 2013 </th>
                        
                        <td>31%</td>
                        
                        <td>100%</td>
                        
                        <td>100%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <th scope="row">Class of 2014 </th>
                        
                        <td>44%</td>
                        
                        <td>100%</td>
                        
                        <td>100%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <th scope="row">Class of 2015 </th>
                        
                        <td>69%</td>
                        
                        <td>100%</td>
                        
                        <td>100%<br>
                           
                        </td>
                        
                     </tr>
                     
                     <tr>
                        
                        <th scope="row">Class of 2016</th>
                        
                        <td>69%</td>
                        
                        <td>Graduation<br>
                           7/29/2016
                        </td>
                        
                        <td>Graduation<br>
                           7/29/2016
                        </td>
                        
                     </tr>
                     
                  </table>
                  		
               </div>
               		
               <a name="FAQ" id="FAQ"></a>
               
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Frequently Asked Questions</h2>
                     
                     <p>Cardiovascular Technology</p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to be a student at Valencia to apply?</h3>
                           
                           <p>Yes. You must complete the admission process to the college before you submit a Cardiovascular
                              Technology
                              program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                              email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                              questions. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can International students apply?</h3>
                           
                           <p>Yes. Students must be a U.S. Citizen, a U.S. permanent resident, or hold a non-immigrant
                              visa with employment
                              authorization. Non-immigrant status students can visit the <a href="https://travel.state.gov/content/visas/en/visit/visitor.html">U.S. Department of State directory of
                                 visa categories</a> to verify employment eligibility. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>What courses do I need to take prior to applying?</h3>
                           
                           <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                              Te pri facete latine
                              salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                              posse exerci
                              volutpat has in.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Are the prerequisite courses offered online?</h3>
                           
                           <p>Yes, some are offered online, however the lab science courses have limited offerings
                              and may not be available every term. 
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                           
                           <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                              if accepted, Cardiovascular Technology courses are only offered on the West campus.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Will my courses from other colleges transfer to Valencia?</h3>
                           
                           <p>Once you have applied to Valencia, you will need to request official transcripts sent
                              from your previous institution. The admissions office will evaluate all transcripts
                              once they are received to determine transfer credit.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                           
                           <p>Yes, you must complete the TEAS 5. The examination fee is your responsibility. More
                              information on the TEAS is available at <a href="/assessments/TEAS.cfm">Assessments_TEAS</a></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>Can I use TEAS scores from another institution?</h3>
                           
                           <p>Yes. Students who complete the TEAS at another institution must request an official
                              score report from ATI as a paper copy is not accepted. Requests can be made online
                              via <a href="www.atitesting.com">www.atitesting.com</a></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4">
                        
                        <div class="box_style_2">
                           
                           <h3>How many students are accepted into the program?</h3>
                           
                           <p>The Cardiovascular Technology program accepts 18 students each Fall term.</p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>What is the schedule like if I am accepted into the program?</h3>
                           
                           <p>The Cardiovascular Technology program is a daytime program located on the West campus;
                              its time demands should be considered the equivalent of a fulltime job as in addition
                              to lecture, lab and clinicals, students are expected to study between 15-25 hours
                              a week - if a student chooses to work while in the program, a maximum of 15 hours
                              a week is recommended due to the demands of the program.  Time management is crucial
                              to successful program completion!  The specific schedule can vary from term to term
                              and from year to year. 
                           </p>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_2">
                           
                           <h3>How do I apply?</h3>
                           
                           <p>Download &amp; fill out the Cardiovascular Technology Application. Return your completed
                              application to
                              Health Sciences Advising along with a non-refundable Health Sciences program application
                              fee of $15
                           </p>
                           
                           <ul>
                              
                              <li>
                                 
                                 <h4>By mail:</h4>
                                 Send the application along with a check or money order payable to Valencia College
                                 to
                                 the address listed below:
                                 
                                 <address class="wrapper_indent">
                                    Valencia College<br>
                                    Business Office<br>
                                    P.O. Box 4913.<br>
                                    Orlando, FL
                                    32802
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 
                                 <h4>In person:</h4>
                                 Make payment in the Business Office on any Valencia Campus &amp; request that the
                                 Business Office forward your program application &amp; fee receipt to the Health Sciences
                                 Advising office at
                                 mail code 4-30 
                              </li>
                              
                           </ul>
                           
                           <p>Please make sure you have met all admission requirements and your application is complete,
                              as missing
                              documentation or requirements can result in denied admission. All items listed on
                              the <a href="/documents/academics/programs/health-sciences/cardiovascular-technology-program-guide.pdf">Program Guide
                                 Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/cardiovascular-technology/index.pcf">©</a>
      </div>
   </body>
</html>