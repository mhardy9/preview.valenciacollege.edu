<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Planning Guide  | Valencia College</title>
      <meta name="Description" content="Course Planning Guide for Cardiovascular Technology students">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/cardiovascular-technology/course-guide.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/cardiovascular-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Cardiovascular Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
               <li>Course Planning Guide </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  
                  <div class="main-title">
                     
                     <h2>Cardiovascular Technology</h2>
                     
                     <p>Associate in Science (A.S.)</p>
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <h2>Course Planning Guide</h2>
                     
                     <p><strong>Declaring Primary:</strong> AA General Studies<br>
                        <strong>Declaring Secondary:</strong> AS Cardiovascular Technology (Pending)
                     </p>
                     
                     <p>This course planning guide is designed for a First Time in College (FTIC), freshman
                        student.  It starts with Developmental 1 classes.
                     </p>
                     
                     <p>This is the suggested Course Sequencing for best benefit to a first time in college
                        student working approximately 25 hours weekly.
                     </p>
                     
                     <h3>First Year Pending</h3>
                     
                     <div class="indent_title_in margin-30">
                        <br>
                        
                        <h4>1st Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25-100%<br>
                                 if eligible
                              </td>
                              
                              <td>(ENC 0017 &amp; ENC 0027)<br>OR REA 0017<br>OR ENC 0025 
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25-100%<br>
                                 if eligible
                              </td>
                              
                              <td>HSC 1004<br>
                                 OR SLS 1122
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25-100%<br>
                                 if eligible
                              </td>
                              
                              <td>(MAT 0018C &amp; MAT 0028C)<br>OR MAT 0022C
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                        <h4>2nd Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>ENC 1101</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>BSC 1105C<br>
                                 Not required, but recommended
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>MAT 1033C</td>
                              
                           </tr>
                           
                        </table>
                        
                        <h4>3rd Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr> 
                              <td>50%<br>
                                 if eligible
                              </td>
                              
                              <td>CHM 1025C<br>
                                 Not required, but recommended
                              </td>
                              
                           </tr>
                           
                           <tr> 
                              <td>50%<br>
                                 if eligible
                              </td>
                              
                              <td>MAC 1105</td>
                              
                           </tr>
                           
                        </table>
                        
                     </div>
                     
                     <h3>Second Year Pending</h3>
                     
                     <div class="indent_title_in">
                        
                        <h4>1st Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>  
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>BSC 1010C</td>
                              
                           </tr>
                           
                           <tr>  
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>HSC 1531<br>
                                 Optional for admission points
                              </td>
                              
                           </tr>
                           
                           <tr>  
                              <td>75%<br>
                                 if eligible
                              </td>
                              
                              <td>MAC 1114<br>
                                 Not required, but recommended
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                        <h4>2nd Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>50%<br>
                                 if eligible
                              </td>
                              
                              <td>BSC 2093C</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>50%<br>
                                 if eligible
                              </td>
                              
                              <td>PHY 1007C</td>
                              
                           </tr>
                           
                        </table>
                        
                        <h4>3rd Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25%<br>
                                 if eligible
                              </td>
                              
                              <td>BSC 2094C</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25%<br>
                                 if eligible
                              </td>
                              
                              <td>PSY2012</td>
                              
                           </tr>
                           
                        </table>
                        
                     </div>
                     
                     <h3>Third Year Pending</h3>
                     
                     <div class="indent_title_in">
                        
                        <h4>1st Term</h4>
                        
                        <table class="table table add_bottom_30 table-striped">
                           
                           <tr>
                              
                              <th scope="col" width="25%">Financial Aid</th>
                              
                              <th scope="col">Courses</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25%<br>
                                 if eligible
                              </td>
                              
                              <td>MCB 2010C</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>25%<br>
                                 if eligible
                              </td>
                              
                              <td>Any Humanities Course</td>
                              
                           </tr>
                           
                        </table>
                        
                     </div> 
                     
                     
                     
                     <p>Courses not required are recommended to promote successful completeion of future science
                        courses
                     </p>
                     
                     <p>Student will be eligible &amp; competitively ready to apply to the Health Information
                        Technology program after successful completion of the Third Year, First Term if all
                        other checklist criteria are successfully completed.
                     </p>
                     
                     <p>Financial Aid Eligibility Estimation is for this path only.  Confirm financial aid
                        eligility at the Answer Center.  Note that loans require 50% minimum.
                     </p>
                     
                  </div>
                  
                  
                  
                  
                  <div class="row">
                     
                     <h3>Possible Financial Aid Assistance for THIS Cardiovascular Sequencing Path</h3>
                     
                     <p>For First Time in College students, Financial Aid will only pay fro prerequisite courses
                        for a limited access program ('Pending' status) if those courses satisfy requirements
                        within the AA General Studies degree
                     </p>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Developmental Education</h4>
                           
                           <p>ENC 0017<br>
                              ENC 0027<br>
                              MAT 0018C<br>
                              MAT 0028C<br>
                              REA 0017<br>
                              ENC 0025<br>
                              MAT 0022C
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Communications</h4>
                           
                           <p>ENC 1101<br>
                              HSC 1004<br>
                              SLS 1122
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Humanities</h4>
                           
                           <p>Student Choice</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Mathematics</h4>
                           
                           <p>MAC 1105<br>
                              MAC 1114
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <div class="row">
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Sciences</h4>
                           
                           <p></p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Social Sciences</h4>
                           
                           <p>PSY 2012</p>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-3">
                        
                        <div class="box_style_2">
                           
                           <h4>Electives</h4>
                           
                           <p>MAT 1033C<br>
                              BSC 1010C<br>
                              HSC 1531<br>
                              BSC 2093C<br>
                              BSC 2094C<br>
                              MCB 2010C
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/cardiovascular-technology/course-guide.pcf">©</a>
      </div>
   </body>
</html>