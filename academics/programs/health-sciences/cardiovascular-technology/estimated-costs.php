<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Estimated Degree Costs  | Valencia College</title>
      <meta name="Description" content="Estimated Degree Costs for Cardiovascular Technology students">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/cardiovascular-technology/estimated-costs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/cardiovascular-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Cardiovascular Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
               <li>Estimated Degree Costs </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div>
                  
                  <div class="container margin-30">
                     
                     <div class="row">
                        
                        <div class="col-md-12">
                           
                           <div class="box_style_1">
                              
                              <h2>Estimated Degree Costs</h2>
                              
                              
                              <p>Last Updated August 2016</p>
                              
                              <table class="table table table-striped cart-list add_bottom_30">
                                 
                                 <th scope="col">Expense</th>
                                 
                                 <th scope="col">Cost</th>
                                 
                                 <tr>
                                    
                                    <td>Test of Essential Academic Skills (TEAS)</td>
                                    
                                    <td align="right" valign="bottom">$65.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Health Sciences Program Application Fee<br><em>per application</em>
                                       
                                    </td>
                                    
                                    <td align="right" valign="bottom">$15.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Background Check/Fingerprint Scan/Drug Testing and Document Manager/Tracker</td>
                                    
                                    <td align="right" valign="bottom">$210.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Immunizations/Physical<br><em>depends on immunizations needed</em>
                                       
                                    </td>
                                    
                                    <td align="right" valign="bottom">variable</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>AHA Basic Life Support CPR certification</td>
                                    
                                    <td align="right" valign="bottom">$40.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>ACLS certification</td>
                                    
                                    <td align="right" valign="bottom">$130.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PALS certification</td>
                                    
                                    <td align="right" valign="bottom">$155.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Uniforms, lab coat, shoes</td>
                                    
                                    <td align="right" valign="bottom">$250.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Littman Stethoscope</td>
                                    
                                    <td align="right" valign="bottom">$65.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Leaded Eyewear</td>
                                    
                                    <td align="right" valign="bottom">non-prescription $170.00 <br>prescription $270.00
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Miscellaneous supplies &amp; Materials</td>
                                    
                                    <td align="right" valign="bottom">$75.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Tuition <br><em>Includes Program Prerequisites and General Education courses</em><br><em>$103.06 per credit hour x 77 credit</em>
                                       
                                    </td>
                                    
                                    <td align="right" valign="bottom">$7,935.62</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Special/Lab Fees <br><em>does not include 
                                          prerequisites/general education courses</em>
                                       
                                    </td>
                                    
                                    <td align="right" valign="bottom">$1,152.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="right">
                                       <strong>Estimated Total</strong><br><em>Does not include textbooks</em>
                                       
                                    </td>
                                    
                                    <td align="right" valign="bottom"><strong>$10,262.62</strong></td>
                                    
                                 </tr>
                                 
                              </table>
                              
                              <h3>Other Expenses</h3>
                              
                              <p>To be nationally registered, the graduate must complete one of the following examinations:<br>
                                 Registered Cardiovascular Invasive Specialist (RCIS) or Registered Cardiac Electrophysiology
                                 Specialist (RCES) Exam<strong>: $350.00</strong></p>
                              
                              <p>Note:  costs not controlled by Valencia, but required to be nationally registered</p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_1">
                              
                              <p>Tuition costs shown are based on 2016-17 In-State Tuition.</p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_1">
                              
                              <p>For more information about tuition and fees, please visit the <a href="https://valenciacollege.edu/businessoffice/">Business Office</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="col-md-4">
                           
                           <div class="box_style_1">
                              
                              <p>These expenses are subject to change without notice.</p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/cardiovascular-technology/estimated-costs.pcf">©</a>
      </div>
   </body>
</html>