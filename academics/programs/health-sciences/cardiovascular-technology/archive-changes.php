<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Archived Program Changes  | Valencia College</title>
      <meta name="Description" content="Estimated Degree Costs for Cardiovascular Technology students">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/health-sciences/cardiovascular-technology/archive-changes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/health-sciences/cardiovascular-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Cardiovascular Technology</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/health-sciences/">Health Sciences</a></li>
               <li><a href="/academics/programs/health-sciences/cardiovascular-technology/">Cardiovascular Technology</a></li>
               <li>Archived Program Changes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div>
                  
                  <div class="container margin-30">
                     
                     <div class="row">
                        
                        <div class="col-md-12">
                           
                           <div class="box_style_1">
                              
                              <h3>Cardiovascular Technology Archived Program Changes</h3>
                              
                              <table class="table table table-striped cart-list add_bottom_30">
                                 
                                 <tr>
                                    
                                    <th scope="col">Date Updated</th>
                                    
                                    <th scope="col">Effective Date</th>
                                    
                                    <th scope="col">Change</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>9/4/14 (posted on website) </td>
                                    
                                    <td>12/10</td>
                                    
                                    <td>
                                       
                                       <p>TEAS Version 5 (V) minimum Composite score of<strong> *58.7% </strong>required.  TEAS scores are valid for 5 years. <strong>*Subject to change. You must meet required score in effect at the time you apply to
                                             the program. </strong></p>
                                       
                                       <p> TEAS information is available at <a href="http://valenciacollege.edu/assessments/teas/">Valencia Assessments - TEAS </a>and &nbsp;<a href="https://www.atitesting.com/Home.aspx">ATItesting.com</a> 
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </table>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  
                  
                  
                  
                  <main role="main"> 
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </main>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/health-sciences/cardiovascular-technology/archive-changes.pcf">©</a>
      </div>
   </body>
</html>