<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Building Construction Tchnology  | Valencia College</title>
      <meta name="Description" content="Building Construction Tchnology | Engineering and Technology | Valencia College">
      <meta name="Keywords" content="college, school, educational, engineering, technology, building, construction">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/engineering-technology/engineering-program-template.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/engineering-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/engineering-technology/">Engineering Technology</a></li>
               <li>Building Construction Tchnology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="main-title">
                  
                  <h2>Building Construction Tchnology</h2>
                  
                  <p>Associate in Science (A.S.)</p>
                  
               </div>
               
               <div class="row box_style_1">
                  
                  <p>The <strong>Building Construction Technology</strong> Associate in Science (A.S.) degree at Valencia College is a two-year program that
                     prepares you with a building construction degree to go directly into a specialized
                     career in the construction and building industry.
                     
                     Valencia’s program has a proven track record of providing competent and skilled estimators,
                     schedulers and construction managers that are in high demand to create the built environment
                     in Central Florida. You’ll learn how to study building plans, estimate materials and
                     labor, obtain building permits and licenses, and oversee the work of other employees.
                     Valencia’s program provides up-to-the-minute courses in the latest construction methods,
                     materials and technology so that you’ll be prepared to work with the best architects,
                     engineers, contractors and building officials in the business.
                  </p>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="/academics/programs/engineering-technology/building-construction-technology/index.html#program_details"> <i class="far fa-laptop"></i>
                        
                        <h3>Program Details</h3>
                        
                        <p>Review course descriptions, important dates and deadlines and other programs details
                           in the official college catalog.
                        </p></a>
                     
                  </div>
                  
                  <div class="col-md-4 col-sm-4">
                     <a class="box_feat" href="/academics/programs/engineering-technology/building-construction-technology/index.html#career_outlook"> <i class="far fa-laptop"></i>
                        
                        <h3>Career Outlook</h3>
                        
                        <p>The average placement rate for Valencia’s A.S. Degree and Certificate programs ranges
                           between 90 – 95% according to the latest FETPIP data.
                        </p></a>
                     
                  </div>
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm"> <i class="far fa-laptop"></i>
                        
                        <h3>Request Information</h3>
                        
                        <p>If you are a&nbsp;<strong>future student</strong>&nbsp;and would like more information about this degree program, you may contact us here.
                           
                        </p>
                        </a></div>
                  
               </div>
               
               
               
               
               
               <div class="main-title">
                  
                  <h2>Frequently Asked Questions</h2>
                  
                  <p>Engineering and Technology</p>
                  
               </div>
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Do I need to be a student at Valencia to apply?</h3>
                        
                        <p>Yes. You must complete the admission process to the college before you submit a Program
                           Name
                           program application. You can <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply online</a> or
                           email <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a> if you have any
                           questions. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Can International students apply?</h3>
                        
                        <p>Yes. Students must be a U.S. Citizen, a U.S. permanent resident, or hold a non-immigrant
                           visa with employment
                           authorization. Non-immigrant status students can visit the <a href="https://travel.state.gov/content/visas/en/visit/visitor.html">U.S. Department of State directory of
                              visa categories</a> to verify employment eligibility. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>What courses do I need to take prior to applying?</h3>
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Are the prerequisite courses offered online?</h3>
                        
                        <p>Yes, some are offered online, however the lab science courses have limited offerings
                           and may not be available every term. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Do I have to take all the prerequisites at a particular campus?</h3>
                        
                        <p>No, you may take the prerequisite courses at any Valencia campus location. However,
                           if accepted, Cardiovascular Technology courses are only offered on the West campus.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Will my courses from other colleges transfer to Valencia?</h3>
                        
                        <p>Once you have applied to Valencia, you will need to request official transcripts sent
                           from your previous institution. The admissions office will evaluate all transcripts
                           once they are received to determine transfer credit.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Do I need to take the Test of Essential Academic Skills (TEAS) before I apply?</h3>
                        
                        <p>Yes, you must complete the TEAS 5. The examination fee is your responsibility. More
                           information on the TEAS is available at <a href="https://valenciacollege.edu/assessments/TEAS.cfm">valenciacollege.edu/assessments/TEAS.cfm</a></p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>Can I use TEAS scores from another institution?</h3>
                        
                        <p>Yes. Students who complete the TEAS at another institution must request an official
                           score report from ATI as a paper copy is not accepted. Requests can be made online
                           via <a href="www.atitesting.com">www.atitesting.com</a></p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>How many students are accepted into the program?</h3>
                        
                        <p>The Program Name program accepts 25 students for the Summer term.</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>What is the schedule like if I am accepted into the program?</h3>
                        
                        <p>The Program Name program is a daytime program located on the West campus; its time
                           demands should be considered the equivalent of a fulltime job as in addition to lecture,
                           lab and clinicals, students are expected to study between 15-25 hours a week - if
                           a student chooses to work while in the program, a maximum of 15 hours a week is recommended
                           due to the demands of the program.  Time management is crucial to successful program
                           completion!  The specific schedule can vary from term to term and from year to year.
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>How many clinical hours are in the Program Name program and what credential would
                           I be eligible to sit for upon successful completion?
                        </h3>
                        
                        <p>The total number of clinical hours in the program is 738.  Upon graduation and successful
                           completion of the National Board Program Name Examination and the ADEX clinical exam,
                           the graduate is eligible to apply for a license as a registered dental hygienist and
                           for certification in local anesthesia. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h3>How successful are your Program Name graduates in finding employment?</h3>
                        
                        <p>The five year average Licensure Rate for Program Name graduates is 100% and the five
                           year average Placement Rate for Program Name graduates is 97%.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_2">
                        
                        <h3>How do I apply?</h3>
                        
                        <p>Download &amp; fill out the Program Name Application. Return your completed application
                           to
                           Health Sciences Advising along with a non-refundable Health Sciences program application
                           fee of $15
                        </p>
                        
                        <ul>
                           
                           <li>
                              
                              <h4>By mail:</h4>
                              Send the application along with a check or money order payable to Valencia College
                              to
                              the address listed below:
                              
                              <address class="wrapper_indent">
                                 Valencia College<br>
                                 Business Office<br>
                                 P.O. Box 4913.<br>
                                 Orlando, FL
                                 32802
                                 
                              </address>
                              
                           </li>
                           
                           <li>
                              
                              <h4>In person:</h4>
                              Make payment in the Business Office on any Valencia Campus &amp; request that the
                              Business Office forward your program application &amp; fee receipt to the Health Sciences
                              Advising office at
                              mail code 4-30 
                           </li>
                           
                        </ul>
                        
                        <p>Please make sure you have met all admission requirements and your application is complete,
                           as missing
                           documentation or requirements can result in denied admission. All items listed on
                           the <a href="/documents/academics/programs/health-sciences/program-name-program-guide.pdf">Program Guide
                              Admission Checklist</a> must be completed PRIOR to submitting a Health Information Technology Application
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/engineering-technology/engineering-program-template.pcf">©</a>
      </div>
   </body>
</html>