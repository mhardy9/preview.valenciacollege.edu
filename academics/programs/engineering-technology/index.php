<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Engineering and Technology  | Valencia College</title>
      <meta name="Keywords" content="engineering, technology, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/engineering-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/engineering-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Engineering Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="main-title">
                  
                  <h2>Engineering and Technology</h2>
                  
                  <p>Associate in Science (A.S.)</p>
                  
               </div>
               
               <div class="row box_style_1">
                  
                  <p>Whether you are building houses, websites, mobile apps, laser systems or computer
                     networks, you will need the analytical skills and technological expertise to do the
                     job. Valencia has a wide variety of programs that will teach you to solve problems
                     using science and technology as your tools. And with high-tech equipment, internships
                     and advanced laboratories, getting your degree or certificate will be a hands-on adventure.
                  </p>
                  
               </div>
               
               
               
               <h3>Featured Engineering and Technology </h3>
               
               <div class="row">
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/engineering-technology/building-construction-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/building-construction-technology/', 'LEARN MORE');"><img src="/_resources/img/academics/programs/engineering-technology/featured-building-construction.jpg" alt="Building Construction Technology">
                        
                        <h3>Building Construction Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in Building Construction Technology will give you the
                           skills to work with the best architects, engineers, contractors and building officials
                           in the business.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/engineering-technology/civil-surveying-engineering-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/civil-surveying-engineering-technology/', 'LEARN MORE');"><img src="/_resources/img/academics/programs/engineering-technology/featured-surveying.jpg" alt="Civil Surveying Engineering Technology">
                        
                        <h3>Civil/Surveying Engineering Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in Civil Surveying Engineering Technology will give
                           you the skills to work as a qualified technician in civil engineering and land surveying.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/engineering-technology/drafting-and-design-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/drafting-and-design-technology/', 'LEARN MORE');"><img src="/_resources/img/academics/programs/engineering-technology/featured-drafting.jpg" alt="Drafting and Design Technology">
                        
                        <h3>Drafting and Design Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in Drafting and Design Technology teaches you how to
                           master drafting and design techniques with&nbsp;computer-aided drafting and design equipment.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
               </div>
               
               
               
               <div class="row">
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/engineering-technology/building-construction-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://preview.valenciacollege.edu/future-students/degree-options/associates/energy-management-and-controls-technology/', 'LEARN MORE');"><img src="/_resources/img/academics/programs/engineering-technology/featured-energy-management-technology.jpg" alt="Energy Management and Controls Technology">
                        
                        <h3>Energy Management and Controls Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in<strong>&nbsp;Energy Management and Controls Technology</strong><strong> </strong>teaches students how to manage and control a building’s electrical and mechanical
                           systems using current and emerging technologies.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="https://preview.valenciacollege.edu/future-students/degree-options/associates/energy-management-and-controls-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/electronics-engineering-technology/', 'LEARN MORE');"><img src="/_resources/img/academics/programs/engineering-technology/featured-electronics-technology.jpg" alt="Electronics Engineering Technology">
                        
                        <h3>Electronics Engineering Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in Electronics Engineering Technology transforms students
                           into highly skilled technicians in the fields of electronics, lasers and photonics,
                           robotics and mechatronics, and telecommunication and wireless systems.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
                  
                  <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/engineering-technology/netowrk-engineering-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/network-engineering-technology/', 'LEARN MORE');" title="Learn More About Network Engineering Technology" target="_blank"><img src="/_resources/img/academics/programs/engineering-technology/featured-network-technology.jpg" alt="Network Engineering Technology">
                        
                        <h3>Network Engineering Technology</h3>
                        
                        <p>Valencia’s A.S. degree program in Network Engineering Technology will train you to
                           design, administer, maintain and support local and wide area network systems.
                        </p>
                        
                        <div class="button-outline">Learn More</div>
                        </a></div>
                  
               </div>
               
               
               
               <h2>Programs &amp; Degrees</h2>
               
               
               <h3>Associate in Arts Transfer Plan</h3>
               
               <p><a href="http://catalog.valenciacollege.edu/transferplans/" target="_blank">Transfer Plans</a> are designed to prepare students to transfer to a Florida public university as a
                  junior.  The courses listed in the plans are the common prerequisites required for
                  the degree. They are placed within the general education requirements and/or the elective
                  credits.  Specific universities may have additional requirements, so it is best check
                  your transfer institution catalog and meet with a Valencia advisor.
               </p>
               
               <ul>
                  
                  <li><a href="http://catalog.valenciacollege.edu/transferplans/engineering/"> Engineering (General)</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/transferplans/computerscience/">Computer Science</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/transferplans/informationtechnology/">Information Technology</a></li>
                  
               </ul>
               
               <h3>Associate in Science</h3>
               
               <p>The A.S. degree prepares you to enter a specialized career field in about two years.
                  It also transfers to the Bachelor of Applied Science program offered at some universities.
                  If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                  at a designated university.
               </p>
               
               <ul>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/building-construction-technology/"> Building Construction Technology </a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/civil-surveying-engineering-technology/">Civil/Surveying Engineering Technology </a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/computer-information-technology/">Computer Information Technology</a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/computer-programming-and-analysis/">Computer Programming and Analysis</a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/drafting-and-design-technology/">Drafting and Design Technology</a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/electronics-engineering-technology/">Electronics Engineering Technology</a></li>
                  
                  <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/network-engineering-technology/">Network Engineering Technology</a></li>
                  
               </ul>
               
               <h3>Bachelor of Science</h3>
               
               <p>The <a href="https://preview.valenciacollege.edu/future-students/degree-options/bachelors/">B.S. degree</a>&nbsp;prepares you to enter an advanced position within a specialized career field.&nbsp;The
                  upper-division courses build on an associate degree and take about two years to complete.
               </p>
               
               <ul>
                  
                  <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/bachelors/electrical-and-computer-engineering-technology/">Electrical and Computer Engineering Technology</a></li>
                  
               </ul>
               
               <h3>Certificate</h3>
               
               <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                  for job advancement. Most can be completed in one year or less. Credits earned can
                  be applied toward a related A.S. degree program.
               </p>
               
               <ul>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#certificatestext">Advanced Electronics Technician</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificates">Advanced Network Administration</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Advanced Network Infrastructure</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#certificatestext">Basic Electronics Technician</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#certificatestext">Computer Information Data Specialist</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#certificatestext">Computer Information Technology Analyst</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerinformationtechnology/#certificatestext">Computer Information Technology Specialist</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerprogramminganalysis/#certificatestext">Computer Programming</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/informationtechnology/computerprogramminganalysis/#certificatestext">Computer Programming Specialist</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Cyber Security</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Digital Forensics</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology/#certificatestext">Drafting</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/draftinganddesigntechnology/#certificatestext">Drafting – Auto CAD</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#certificatestext">Laser and Photonics Technician</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Network Administration</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Network Infrastructure</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/networkengineeringtechnology/#certificatestext">Network Support</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/engineeringtechnology/electronicsengineeringtechnology/#certificatestext">Robotics and Mechatronics Technician</a></li>
                  
               </ul>
               
               <h3>Continuing Education</h3>
               
               <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                  development, language courses, certifications and custom course development for organizations.
                  Day, evening, weekend and online learning opportunities are available.
               </p>
               
               <ul>
                  
                  <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                  
               </ul>
               
               <h3>Associate in Arts Articulated Pre-Major</h3>
               
               <ul>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/informationtechnologyuniversityofsouthflorida/"> Information Technology (University of South Florida)</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/computerscienceuniversityofcentralflorida/">Computer Science (University of Central Florida)</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringfloridainstituteoftechnology/">Engineering (Florida Institute of Technology)</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringpolytechnicuniversity/">Engineering (Polytechnic University of Puerto Rico, Orlando Campus)</a></li>
                  
                  <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofcentralflorida/">Engineering (University of Central Florida)</a></li>
                  
                  <li>
                     <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/articulatedpremajorsatvalencia/engineeringuniversityofmiami/">Engineering (University of Miami)</a>
                     
                     
                  </li>
                  
               </ul>
               
               
               <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                  
                  <h3>Career Coach</h3>
                  
               </div>
               
               <div class="wrapper_indent">
                  
                  <p>Explore related careers, salaries and job listings.</p>
                  <a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=engineering&amp;Clusters=15&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=engineering&amp;Clusters=15&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all', 'LEARN More');" target="_blank" class="button small">Learn More</a>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/engineering-technology/index.pcf">©</a>
      </div>
   </body>
</html>