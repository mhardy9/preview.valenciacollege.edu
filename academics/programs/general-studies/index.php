<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>General Studies  | Valencia College</title>
      <meta name="Description" content="The Associate in Arts (A.A.) degree is the equivalent to the first two years at a state university.">
      <meta name="Keywords" content="aa, a.a., associate in arts, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/general-studies/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/general-studies/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>General Studies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="main-title">
                     			
                     <h2>General Studies</h2>
                     			
                     <p>Associate in Arts Degree</p>
                     		
                  </div>
                  		
                  <div class="row box_style_1">
                     			
                     <p> If you’re planning on getting a bachelor’s degree, then the Associate in Arts (A.A.)
                        degree is for you. It’s equivalent to the first two years at a state university. The
                        only difference? At about half the cost, it’s a much more affordable way to start
                        out. And when you’re finished at Valencia, you’ll be prepared to transfer to a four-year
                        university as a junior, including the University of Central Florida, which guarantees
                        admission through DirectConnect to UCF.
                     </p>
                     		
                  </div>
                  
                  
                  		
                  <div class="box_style_1">
                     
                     			
                     <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                        				
                        <h3>Career Coach</h3>
                        			
                     </div>
                     			
                     <div class="wrapper_indent">
                        				
                        <p>Explore related careers, salaries and job listings.</p>
                        				
                        <p><a href="https://valenciacollege.emsicareercoach.com/#action=loadCourseInfo&amp;Search=general+studies&amp;OccupationTags=&amp;EdLevel=all&amp;WageLimit=0&amp;CourseCategory=0&amp;CourseLocation=0&amp;SearchType=course&amp;editMode=0&amp;OccupationId=13-2011.01&amp;OccSearchSort=employment&amp;CourseId=259340" class="button small" target="_blank">Learn More</a></p>
                        			
                     </div>
                     			
                     <hr class="styled_2">
                     			
                     <div class="indent_title_in"> <i class="pe-7s-check"></i>
                        				
                        <h3>Guaranteed Admission</h3>
                        			
                     </div>
                     			
                     <div class="wrapper_indent">
                        				
                        <p>All A.A. grads are guaranteed admission into one of Florida’s 11 state universities.</p>
                        			
                     </div>
                     			
                     <hr class="styled_2">
                     			
                     <div class="indent_title_in"> <i class="pe-7s-display2"></i>
                        				
                        <h3>Coursework</h3>
                        			
                     </div>
                     			
                     <div class="wrapper_indent">
                        				
                        <p>The general studies A.A. provides a basic liberal arts education, with courses focused
                           in five key areas: communications, humanities, mathematics, science and social science.
                           It also includes plenty of elective courses (courses you choose) so that you can focus
                           your studies toward a specific major or simply explore subjects that interest you.
                        </p>
                        			
                     </div>
                     			
                     <hr class="styled_2">
                     			
                     <div class="indent_title_in"> <i class="pe-7s-way"></i>
                        				
                        <h3>Transfer Plans</h3>
                        			
                     </div>
                     			
                     <div class="wrapper_indent">
                        				
                        <p>If you already have an intended university major, you can start preparing for it by
                           following a Valencia Transfer Plan. It will serve as a guide for which courses to
                           take in order start fulfilling the prerequisites that you’ll need.
                        </p>
                        			
                     </div>
                     			
                     <hr class="styled_2">
                     			
                     <h2>Programs &amp; Degrees</h2>
                     			
                     <h3>Associate in Arts Pre-Major</h3>
                     			
                     <p>Some pre-majors are based upon articulation agreements with specific universities.
                        They are designed for students to transfer to a particular public or private university
                        as a junior to complete a four-year Bachelor’s degree in a specific major. For more
                        information, visit <a href="http://valenciacollege.edu/aadegrees/articulationagreements.cfm" target="_blank">Articulation Agreements for Pre-Majors</a>.
                     </p>
                     			
                     <ul>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/artstudiofineart/">Art, Studio/Fine Art</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Dance Performance</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/musicperformance/">Music Performance</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/signlanguageinterpretation/">Sign Language Interpretation</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/theatredramadramaticarts/">Theatre/Drama/Dramatic Arts</a></li>
                        			
                     </ul>
                     			
                     <h3>Associate in Arts Transfer Plan</h3>
                     			
                     <p><a href="http://catalog.valenciacollege.edu/transferplans/" target="_blank">Transfer Plans</a> are designed to prepare students to transfer to a Florida public university as a
                        junior. The courses listed in the plans are the common prerequisites required for
                        the degree. They are placed within the general education requirements and/or the elective
                        credits. Specific universities may have additional requirements, so it is best check
                        your transfer institution catalog and meet with a Valencia advisor.
                     </p>
                     			
                     <ul>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/accounting/">Accounting</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/biology/">Biology</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/biomedicalsciences/">Biomedical Sciences</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/businessadministration/">Business Administration</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/businessmarketingmanagement/">Business Marketing Management</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/cardiopulmonarysciences/">Cardiopulmonary Sciences</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/chemistry/">Chemistry</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/computerscience/">Computer Science</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/economics/">Economics</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/educationgeneralpreparation/">Education (General Preparation)</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/engineering/">Engineering</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/english/">English</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/healthservicesadministration/">Health Services Administration</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/history/">History</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/horticulturalscience/">Horticultural Sciences</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/humanresourcesmanagement/">Human Resources Management</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/informationtechnology/">Information Technology</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/journalism/">Journalism</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/managementinformationsystems/">Management Information Systems</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/marinebiology/">Marine Biology</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/mathematics/">Mathematics</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/philosophy/">Philosophy</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/politicalsciencegovernment/">Political Sciences and Government</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/psychology/">Psychology</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/publicadministration/">Public Administration</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/publicrelationsorganizationalcommunication/">Public Relations/Organizational Communication</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/socialsciences/">Social Sciences</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/sociology/">Sociology</a></li>
                        				
                        <li><a href="http://catalog.valenciacollege.edu/transferplans/statistics/">Statistics</a></li>
                        			
                     </ul>
                     			
                     <h3>Continuing Education</h3>
                     			
                     <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                        development, language courses, certifications and custom course development for organizations.
                        Day, evening, weekend and online learning opportunities are available.
                     </p>
                     			
                     <ul>
                        				
                        <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                        			
                     </ul>
                     			
                     <hr class="styled_2">
                     
                     
                     
                     			
                     <div class="wrapper_indent">
                        
                        				<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-action home">Apply Now</a>
                        
                        				<a href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm" class="button-action_outline home">Request Information</a>
                        
                        				<a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" class="button-action_outline home split">Visit Valencia</a>
                        
                        				<a href="https://vcchat.valenciacollege.edu/CCPROChat/CPChatRequest.jsp?form.ServiceID=29&amp;form.CustName=WebCaller&amp;form.SEcureChat=1" class="button-action_outline home split" target="foo" onsubmit="window.open('', 'foo', 'width=450,height=500,status=yes,resizable=yes,scrollbars=yes,location=no')">Chat With Us</a>
                        
                        				
                        <p>&nbsp;</p>
                        
                        				
                        <center>
                           
                           
                           					
                           <form id="chatForm">
                              
                              						<input type="hidden" id="firstName" value="Valencia">
                              						<input type="hidden" id="lastName" value="Student">
                              
                              						<button type="button" class="button" onclick="startPopupChat()">ONLINE CHAT</button>
                              					
                           </form>
                           
                           					
                           <div id="chatContainer"></div>
                           
                           
                           
                           
                           					<strong>Chat Hours</strong><br>
                           					<small>Monday – Thursday 8am - 6pm<br></small>
                           					<small>Friday 9am to 5pm</small>
                           				
                        </center>
                        
                        			
                     </div>
                     
                     		
                  </div>       
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/general-studies/index.pcf">©</a>
      </div>
   </body>
</html>