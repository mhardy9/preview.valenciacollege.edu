<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hospitality and Culinary  | Valencia College</title>
      <meta name="Keywords" content="hospitality, culinary, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/hospitality-culinary/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/hospitality-culinary/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Hospitality Culinary</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Hospitality and Culinary</h2>
                        
                        <hr class="styled_2">
                        
                        <p>For fledging chefs, hoteliers, restaurant managers and tourism professionals can get
                           a first class education at Valencia. Our programs stress hands-on experience, which
                           you’ll get through internships as well from the banquet rooms, commercial-sized instructional
                           kitchen, dynamic point-of-sale system, and hotel front desk simulation area housed
                           on the West Campus. From meeting planning to menu planning, we’ll help you prepare
                           for a career in this exciting industry.<br>
                           
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                           
                           <h3>Career Coach</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore related careers, salaries and job listings.</p>
                           
                           <p><a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=hospitality&amp;Clusters=9&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" class="button small" target="_blank">Learn More</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-coffee"></i>
                           
                           <h3>Valencia College Welcomes Students from Le Cordon Bleu</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li><a href="http://valenciacollege.edu/asdegrees/documents/LeCordonBleuagreement.pdf" target="_blank">View Transfer Agreement</a></li>
                              
                              <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/culinary-management/#advisor-form">Request Information</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h2>Programs &amp; Degrees</h2>
                        
                        <ul>
                           
                        </ul>
                        
                        <h3>Associate in Science</h3>
                        
                        <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                           also transfers to the Bachelor of Applied Science program offered at some universities.
                           If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                           at a designated university.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/baking-and-pastry-management/">Baking and Pastry Management</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/culinary-management/">Culinary Management</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/hospitality-and-tourism-management/">Hospitality and Tourism Management</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/restaurant-and-food-service-management/">Restaurant and Food Service Management</a></li>
                           
                        </ul>
                        
                        <h3>Certificate</h3>
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/baking-and-pastry-management/">Baking and Pastry Arts</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/culinary-management/">Chef’s Apprentice</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/culinary-management/">Culinary Arts</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/hospitalityculinary/culinarymanagement/#certificatestext">Culinary Arts Management Operations</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/hospitality-and-tourism-management/">Hospitality – Event Planning Management</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/hospitality-and-tourism-management/">Hospitality – Guest Services Specialist</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/hospitality-and-tourism-management/">Hospitality – Rooms Division Management</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/restaurant-and-food-service-management/">Restaurant and Food Service Management</a></li>
                           
                        </ul>
                        
                        <h3>Continuing Education</h3>
                        
                        <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                           development, language courses, certifications and custom course development for organizations.
                           Day, evening, weekend and online learning opportunities are available.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/hospitality-culinary/index.pcf">©</a>
      </div>
   </body>
</html>