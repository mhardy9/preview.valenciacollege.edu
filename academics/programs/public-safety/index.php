<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Public Safety and Paralegal Studies  | Valencia College</title>
      <meta name="Keywords" content="public safety, paralegal, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/public-safety/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/public-safety/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Public Safety</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Public Safety and Paralegal Studies</h2>
                        
                        <hr class="styled_2">
                        
                        <h3>School of Public Safety</h3>
                        
                        <p>Valencia College’s <a href="http://valenciacollege.edu/public-safety/">School of Public Safety</a> provides comprehensive education, training and resources for our communities’ first
                           responders. It houses three major program areas: <a href="http://valenciacollege.edu/public-safety/criminal-justice-institute/">Criminal Justice</a>, <a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/">Fire Rescue</a> and <a href="http://valenciacollege.edu/public-safety/safety-and-security-institute/">Safety and Security</a>. Our law enforcement and corrections academies and fire science academy and minimum
                           standards class for fire fighters meet Florida’s training requirements and qualify
                           students to take the state certification exams within their field. We also offer industry
                           certifications and advanced specialized training, as well as two-year Associate in
                           Science (A.S) degrees in Criminal Justice and Fire Science Technology.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>Paralegal Studies, Associate in Science (A.S.) degree</h3>
                        
                        <p>Valencia’s Paralegal Studies Associate in Science (A.S.) degree program will prepare
                           you to become a legal assistant in a law firm, bank, corporation and government agency.<br>
                           <i>Paralegals may not provide legal services directly to the public, except as permitted
                              by law.</i></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Advanced Specialized Training and Continuing Education</h3>
                        
                        <p>Valencia offers professional training courses, programs, seminars and workshops dedicated
                           to the public safety community, including specialized training for sworn Florida law
                           enforcement, corrections or probation officers, and fire service professionals of
                           all levels.
                        </p>
                        
                        <p>View upcoming courses for:</p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/criminal-justice/" target="_blank">Criminal Justice</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/fire-rescue/" target="_blank">Fire Rescue</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/government/" target="_blank">Government and Public Service</a></li>
                           
                        </ul>         
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                           
                           <h3>Career Coach</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore related careers, salaries and job listings.</p>
                           
                           <p><a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=public+safety&amp;Clusters=7,12&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" class="button small" target="_blank">Learn More</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h2>Programs &amp; Degrees</h2>
                        
                        
                        <h3>Associate in Science</h3>
                        
                        <p>The A.S. degree prepares you to enter a specialized career field in about two years.
                           It also transfers to the Bachelor of Applied Science program offered at some universities.
                           If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                           at a designated university.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/criminal-justice/">Criminal Justice</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/fire-science-technology/">Fire Science Technology</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/paralegal-studies/">Paralegal Studies</a></li>
                           
                        </ul>
                        
                        <h3>Fire Science Academy Track</h3>
                        
                        <p>The Fire Science Academy is a fast-paced degree-granting program that provides students
                           with three components of career development in just two years: A.S. degree in Fire
                           Science Technology, EMT (Emergency Medical Technology) Technical Certificate (must
                           pass a state exam), and Minimum Standards Training – Firefighter I and Firefighter
                           II (must pass a state exam). 
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/academy-track.cfm">Fire Science Academy Track</a></li>
                           
                        </ul>
                        
                        <h3>Certificate</h3>
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticeinstitute/#programrequirementstext">Auxiliary Law Enforcement Officer</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticeinstitute/#programrequirementstext">Correctional Officer</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Criminal Justice Technology Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticeinstitute/#programrequirementstext">Crossover: Corrections to Law Enforcement</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/firesciencetechnology/#certificatestext">Fire Fighter (Minimum Standards)</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/firesciencetechnology/#certificatestext">Fire Officer Supervisor</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#OFFICER1">Florida Bureau of Fire Standards and Training Certifications - Fire Officer I</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#OFFICER2">Florida Bureau of Fire Standards and Training Certifications - Fire Officer II</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#INSTRUCTOR">Florida Bureau of Fire Standards and Training Certifications - Fire Service Instructor</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#INSPECT1">Florida Bureau of Fire Standards and Training Certifications - Firesafety Inspector
                                 I</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#INSPECT2">Florida Bureau of Fire Standards and Training Certifications - Firesafety Inspector
                                 II</a></li>
                           
                           <li><a href="http://valenciacollege.edu/public-safety/fire-rescue-institute/florida-bureau-of-fire-standards-and-training-certifications.cfm#INVEST1">Florida Bureau of Fire Standards and Training Certifications - Firesafety Investigator
                                 I</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticetechnology/#certificatestext">Homeland Security – Law Enforcement Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticeinstitute/#programrequirementstext">Law Enforcement Officer</a></li>
                           
                        </ul>
                        
                        <h3>Continuing Education</h3>
                        
                        <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                           development, language courses, certifications and custom course development for organizations.
                           Day, evening, weekend and online learning opportunities are available.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/public-safety/index.pcf">©</a>
      </div>
   </body>
</html>