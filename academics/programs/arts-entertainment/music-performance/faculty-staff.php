<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty and Staff  | Valencia College</title>
      <meta name="Description" content="Valencia's Music Performance">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, Music Performance, Music Performance   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/music-performance/faculty-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/music-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Music Performance</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/music-performance/">Music Performance</a></li>
               <li>Faculty and Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Faculty and Staff</h2>
                        
                        <p>Music Faculty and Staff Biographies</p>
                        
                        <p>Valencia College is proud to have <strong>nationally recognized music faculty</strong>, all of whom are committed to providing the best guidance and education possible
                           to its students.
                        </p>
                        
                        <img src="/_resources/images/academics/programs/arts-entertainment/facultyrecitalspring08031_001.jpg" class="img-responsive circle" alt="Dr. Troy Gifford ">
                        		
                        <h3>Dr. Troy Gifford </h3>
                        		 
                        <p>Troy Gifford currently serves as the Music Program Director at Valencia College.&nbsp;
                           He holds Bachelor of Music and Master of Arts degrees in Guitar Performance and Composition
                           from Florida Atlantic University, and a Doctorate of Musical Arts in Composition from
                           the University of Miami.&nbsp; He is the recipient of numerous awards, including first
                           place in the 1999 Guitar Foundation of America Composition Competition, the 2002 University
                           of Miami Concerto Composition Competition, and the E.B. Griswold Award for Achievement
                           in the Performing Arts.&nbsp; His music has been performed worldwide and is published by
                           Doberman-Yppan and Mel Bay Publications.&nbsp; Before coming to Valencia, Dr. Gifford taught
                           at the University of Miami, Florida Atlantic University, and Palm Beach Community
                           College. 
                        </p>            
                        
                        <hr>
                        
                        <img src="/_resources/images/academics/programs/arts-entertainment/gerberpics003.gif" class="img-responsive circle" alt="Alan Gerber "><h3>Alan Gerber, Baritone </h3>
                        
                        <p>Born near Philadelphia, Pennsylvania, lyric baritone Alan Gerber studied choral conducting
                           and voice with Jane Sheppard and Joy Vandever at West Chester University, where he
                           received a Bachelor's degree in music education and a Master's degree in voice performance.&nbsp;
                           Mr. Gerber has performed as vocal soloist with several groups, including Pennsylvania
                           Pro Musica and Sarum Singers of Philadelphia, Western Wind Vocal Ensemble, Musica
                           Sacra and Amor Artis of New York City, Chorale Delaware of Wilmington, and Music Orlando,
                           Orlando XIII, and The University of Central Florida Chorus and Orchestra. Mr. Gerber
                           has been a frequent soloist in the Central Florida area in oratorio works such as
                           Handel’s <em>Messiah</em>. Mr. Gerber's operatic credits include El Remondado in <em>Carmen</em> and Peppe in <em>I Pagliacci</em> with the Berks Grand Opera of Reading, Pa, and Lucano and Liberto in <em>The Coronation of Poppea</em> with the Orlando Opera Company. He has also appeared with the Orlando Opera Chorus
                           in the productions of Rigoletto, La Traviata, Romeo et Juliette, Aida and Il Barbiere
                           di Siviglia.&nbsp; Mr. Gerber is the vocal/choral director at Valencia College, Orlando,
                           Florida, where he conducts the Valencia Singers, the Contemporary Ensemble and the
                           Early Music Ensemble. He co-founded and directed Bella Baroque, a local ensemble that
                           performed Baroque vocal works and dances in authentic costumes. Mr. Gerber is also
                           a pianist and violinist. Mr. Gerber is active in The National Association of Teachers
                           of Singing (NATS), and is a former president of the Central Florida Chapter of this
                           organization.&nbsp; His students are frequently winners in the NATS auditions.
                        </p>
                        
                        
                        <hr>
                        
                        <h3>Carla DelVillaggio, Soprano</h3>
                        		<img src="/_resources/images/academics/programs/arts-entertainment/40thconcert115.jpg" class="img-responsive circle" alt="Carla DelVillaggio "><br>	
                        
                        <p>Soprano Carla DelVillaggio (<a href="http://www.carladelvillaggio.com">www.carladelvillaggio.com</a>) has been critically acclaimed for her superb interpretations of leading roles in
                           operas such as Les Contes de Hoffmann, The Student Prince, La Boheme, Carmen, Die
                           Fledermaus, and La Cenerentola, among others.&nbsp; Ms. DelVillaggio grew up in the Orlando
                           area and received her early training at the University of Central Florida.&nbsp; She made
                           her operatic debut while still in undergraduate school in an Orlando Opera Company
                           production of The Merry Widow opposite Roberta Peters.&nbsp; She went on to receive her
                           Masters of Music degree with an Opera Specialization from SUNY-Binghamton, (NY).&nbsp;
                           Ms. DelVillaggio has appeared with the Baltimore Opera Company, Annapolis Opera Company,
                           Opera Camerata of Washington, Summer Opera Theatre Company (D.C.) Tri-Cities Opera
                           Company, Orlando Opera and the International Opera Workshop, among others.&nbsp; Additionally,
                           she has sung the soprano roles in such oratorio works as: Mozart’s Exsultate Jubilate,
                           Laudate Dominum, and Mass in c Minor, Handel’s Messiah, Mendelssohn’s Elijah, Poulenc’s
                           Gloria, Bach’s Magnificat, Vivaldi’s Gloria and Dvorak’s Te Deum.&nbsp; In 2001, she had
                           the honor of singing at a joint event for Czech, Slovak and American dignitaries,
                           including the head of the Czech Senate and Madeleine Albright, former U.S. Secretary
                           of State.&nbsp; The following summer she made her European debut, in the Czech Republic,
                           singing the title role in La Traviata.&nbsp; She was the soprano soloist for the Messiah
                           Choral Society’s performance of Handel’s Messiah at the Bob Carr Auditorium. Currently,
                           she is an adjunct professor of voice on the faculties of both Valencia College (<a href="http://frontdoor.valenciacollege.edu/?cdelvillageio">Valencia Faculty Webpage</a>) and Rollins College.&nbsp;Ms. DelVillaggio teaches private voice instruction as well
                           as Music Appreciation, Performance Lab and Voice Class.&nbsp; She developed the first-ever
                           Opera-Theatre Workshop at Valencia.&nbsp; In addition to her responsibilities at Valencia,
                           Ms DelVillaggio&nbsp;also enjoys singing jazz, and performs regularly as a <a href="http://www.simplystreisand.net">Barbra Streisand impersonator.</a></p>
                        
                        
                        <hr>
                        
                        
                        
                        <img src="/_resources/images/academics/programs/arts-entertainment/40thconcert035_000.jpg" class="img-responsive circle" alt="Fang Brill, ">
                        		
                        <h3>Fang Brill, Violinist</h3>
                        
                        <p> A graduate of the New England Conservatory of Music, the University of Kansas, and
                           the Nanking College of Fine Arts, Ms. Brill has won numerous concerto competitions,
                           including the Ohio Light Opera Concerto Competition in 1990, and has performed as
                           a soloist with the Nanking Philharmonic Orchestra in Nanking, China, as well as the
                           Ohio Light Opera Orchestra in Wooster, OH.&nbsp; She has studied violin with Eric Rosenblith,
                           Ben Sayevich, and Xue Sheng, as well as master classes in violin and chamber music
                           with Claude Frank, Robert Vernon, Michelle Auclaire, Eugene Lehner, Colin Carr, Michael
                           Haber, and Christopher Wellington. Born in Nanking, China, Ms. Brill holds a Bachelor
                           of Music degree in violin performance from the Nanking College of Fine Arts, where
                           after graduation, she joined the faculty of the College; later, she was accepted as
                           a graduate student, with full scholarship, at the University of Kansas, where she
                           received her Master of Music degree in violin performance.&nbsp; At the University of Kansas,
                           she also won the prestigious Katherine Nelson Award for outstanding performer.&nbsp; Following
                           her graduation from the University of Kansas, Ms. Brill went on to earn a Graduate
                           Diploma in violin performance from the New England Conservatory of Music, in Boston.&nbsp;
                           While at the Conservatory, she performed with the New England Conservatory Symphony
                           under Michael Tilson Thomas at Carnegie Hall. Ms. Brill enjoys teaching both in colleges
                           and privately.&nbsp; Currently, she is Adjunct Professor of Violin at Valencia College
                           in Orlando, Florida, where she also has been directing the string ensemble of the
                           college.&nbsp; She joined the faculty of VCC in the fall of 2004.&nbsp; Besides teaching, Ms.
                           Brill has been very active in performing with professional orchestras, such as the
                           New World Symphony Orchestra, the Kansas City Symphony Orchestra, and the Kansas City
                           Chamber Orchestra.&nbsp; Presently, she is a member of the Bach Festival Orchestra, and
                           the Orlando Philharmonic Orchestra in Orlando, Florida; she also performs with the
                           Brevard Symphony Orchestra in Melboune, Florida. &nbsp;&nbsp;
                        </p>
                        
                        
                        	
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/music-performance/faculty-staff.pcf">©</a>
      </div>
   </body>
</html>