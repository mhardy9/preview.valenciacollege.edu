<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Music Performance  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Music Performance one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, Music Performance, Music Performance   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/music-performance/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/music-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Music Performance</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Music Performance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Music Performance</h2>
                        			
                        <p>Because learning to sing or play music is all about being immersed in a creative community
                           while being guided by great teachers, Valencia is proud to introduce you to its two-year
                           music program. Since 1975, the music program has been dedicated to the idea of inspiring
                           future performers by providing students with musically trained faculty and an expanding
                           array of resources and experiences to enrich, stimulate and develop your gifts in
                           and outside the classroom.
                        </p>
                        		
                        		
                        <hr>
                        	
                        <h3>Why Major in Music at Valencia? </h3>
                        	
                        <p><em>Valencia's Music Program leads to an Associate in Arts degree</em></p>
                        						
                        <p>By studying at Valencia, you'll master the fundamentals of performance and build on
                           your career opportunities by weekly participation in one of a dozen ensembles.
                        </p>
                        						
                        <p>Students at Valencia get more opportunities for solo work and conducting than at many
                           four-year schools where upperclassmen often get primary roles.
                        </p>
                        						
                        <p>Take advantage of music-specific scholarships and other financial aid, along with
                           tuition that's almost half the cost of a state university.
                        </p>
                        						
                        <p>Enjoy the resources of Valencia's East Campus fine arts center, including instrumental
                           and choir rehearsal halls, a library/listening, a MIDI/electronic piano lab and a
                           550-seat performance hall. 
                        </p>
                        						
                        <p>By building your repertoire and improving your craft, you will gain an edge when you
                           apply for transfer to a four-year music school.&nbsp; 
                        </p>
                        						
                        <p><img src="/_reources/images/academics/programs/arts-entertainment/IMG_8282.JPG" alt=""></p>
                        						
                        <p>Our graduates can be found in many outstanding American music schools including Berkeley,
                           New World School of the Arts, Stetson, FSU, UF and USF. Others now perform with leading
                           symphony orchestras.
                        </p>
                        						
                        <p>Still others went on to have major recording careers. </p>
                        						
                        <p></p>
                        						
                        <p></p>
                        						
                        <p>Three famous pop groups have members who received their training in Valencia's Music
                           Department:
                        </p>
                        						
                        <ul class="list_style_1">
                           							
                           <li>Howie Dorough of the Backstreet Boys</li>
                           							
                           <li>Chris Kirkpatrick of 'N Sync</li>
                           							
                           <li>David Perez, Brody Martinez and Raul Molina of C Note</li>
                           						
                        </ul>
                        
                        	
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/music-performance/index.pcf">©</a>
      </div>
   </body>
</html>