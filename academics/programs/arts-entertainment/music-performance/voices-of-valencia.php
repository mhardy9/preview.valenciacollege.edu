<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Voices of Valencia  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Music Performance one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, Music Performance, Music Performance   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/music-performance/voices-of-valencia.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/music-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Music Performance</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/music-performance/">Music Performance</a></li>
               <li>Voices of Valencia </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Voices of Valencia</h2>
                        <img src="/_resources/images/academics/programs/arts-entertainment/VoicesOfValencia-logo.png" class="img-responsive" alt="Valencia Voices Logo"><br>		
                        
                        <p>For over 15 years, the Voices of Valencia has performed all around Central Florida.
                           Voices of Valencia is an open chorus without audition. We have a variety of skills,
                           talents, abilities and diversity. Voices of Valencia members are students, staff,
                           faculty and members of the Central Florida Community.
                        </p>
                        
                        
                        <p><strong>Truly "Music for EVERYONE!!"</strong></p>
                        
                        
                        
                        <p><strong>Founded &amp; Directed by: James C. Jones</strong><br>
                           <strong>Contact Info:</strong><br>
                           	<a href="mailto:jjones2@valenciacollege.edu" title="Email">jjones2@valenciacollege.edu</a></p>
                        
                        	
                        
                        <p>In 1996, Voices of Valencia was formed as part of a "music for everyone" program to
                           give people with various levels of experience and music backgrounds an opportunity
                           to sing. After years of hard work and countless volunteered hours of service by dedicated
                           choir members, the Voices of Valencia has now grown into a large community choir 
                        </p>
                        
                        
                        
                        <hr>
                        
                        <h3>Founder &amp; Director Bio</h3>
                        <img src="/_resources/images/academics/programs/arts-entertainment/VoV-Director.png " class="img-responsive" alt="James C. Jones"><br>	
                        
                        <p>With over 40 years of choral and ensemble directing, James C. Jones has coached and
                           taught college and community members how to sing &amp; perform.  From his years as a Music
                           Pastor, to his forming &amp; traveling with a ten member ensemble "Spirits Rising," his
                           background &amp; history is diverse.  Today, after forming the "Voices of Valencia", a
                           100+ voice choir, "Intone", an 8+ part A Cappella Vocal Orchestra, "The Juliettes",
                           a Ladies Barbershop Group and most recently "Valencia Chamber Chorus" (VCC West Campus)
                           &amp; "Valencia Candlelight Chorus" James continues his goal to bring music to the community!
                           
                        </p>
                        
                        <hr>
                        		
                        
                        <h3>EPCOT Candlelight Processional</h3>
                        <img src="/_resources/images/academics/programs/arts-entertainment/VoV7.png" class="img-responsive" alt="EPCOT Candlelight Processional"><br>
                        
                        <p>For over a decade, the Voices of Valencia participated in the EPCOT Candlelight Processional
                           be a part of a spectacular holiday show featuring a 300+ voice chorus, 50-piece orchestra,
                           the Voices of Liberty and a celebrity narrator. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/music-performance/voices-of-valencia.pcf">©</a>
      </div>
   </body>
</html>