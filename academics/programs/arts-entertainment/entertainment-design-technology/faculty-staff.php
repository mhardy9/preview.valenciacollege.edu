<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty/Staff  | Valencia College</title>
      <meta name="Description" content="Valencia's Entertainment Design &amp; Technology degree program is one of only a few in the nation and the only one in Central Florida that prepares students to work in the production aspects of the entertainment industry.">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Entertainment Design &amp; Technology   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/entertainment-design-technology/faculty-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/entertainment-design-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Entertainment Design &amp; Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/entertainment-design-technology/">Entertainment Design &amp; Technology</a></li>
               <li>Faculty/Staff</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        		
                        <h2>Faculty/Staff</h2>
                        		<strong>Kristin Abel , Entertainment Design and Technology Program Chair</strong>
                        								
                        <p>Kristin has worked in the entertainment industry in Orlando since 1996 as a scenic
                           artist, set designer, lighting designer and stage hand.&nbsp; Her work has appeared in
                           Disney theme parks, Universal Studios, Sea World, the Orange County Convention Center,
                           commercials, and at various Orlando area theaters.&nbsp; She has long since lost count
                           of how many productions she’s worked on. Kristin started her career at Minnesota State
                           University, Moorhead as a Technical Director and continues it Valencia College, also
                           as Technical Director.&nbsp; In addition, she designs scenery and lighting for VCC Theater
                           productions, and teaches various Entertainment Design and Technology courses.&nbsp; Kristin
                           indulges her fondness of puppets by working as a stage hand at Disney’s Animal Kingdom
                           during college breaks. Kristin has a B.A. in Theater Arts from Minnesota State University,
                           Moorhead
                        </p>
                        							
                        								
                        							<strong>Ann LaPietra, Instructor Entertainment Design and Technology Performing Arts Center
                           Manager</strong>
                        							
                        <p>Ann LaPietra has been working in the local entertainment industry for over 15 years.
                           Her vast experience includes stage lighting, audio, scenic painting, and stage management.
                           She is currently the manager for the Performing Arts Center at Valencia and also teaches
                           part time. Ann is a graduate of the Entertainment Design and Technology program and
                           also has a B.A. from UCF in communication. Mentored by Rick Rietveld (former Dean
                           and one of the founders of many A&amp;E programs at the college) Ann has learned the many
                           rewarding aspects of the industry and continues to pass those along to students. 
                        </p>
                        								
                        								
                        								<strong>Sonia M. Pasqual, Instructor Entertainment Design and Technology</strong>
                        									
                        <p>Sonia Pasqual has been workingin theatre and entertainment for18 years working as
                           an electrician, spotlight operator, designer and artist for various types of shows.She
                           has been a part of the "Lighting Design GlobalBench" forWalt Disney World Entertainment,
                           plus other roles and dutiesincludes, Production Planner for Downtown Disney and Resorts,teaching
                           "Lighting the Disney Way", pyrotechnics, video and camera technician.She has designed
                           for theatre's throughout the state of Florida and also in New York City, such as the
                           Marionette Swedish Cottage Theatre, Gallery de Degli, and several others.Sonia is
                           a graduate of the Entertainment Design and Technology prgram and also has a B.A from
                           Fordham University Lincoln Center in Theatre Production.Currently, Sonia designs for
                           local churches, private school theatre's, and more in the Greater Orlando area, while
                           teaching as an adjunct for Valencia College.
                        </p>
                        						
                        								
                        								<strong>David 'Ross' Rauschkolb, Instructor Technical Production and Technical Director</strong>
                        									
                        <p></p>
                        								
                        								<strong>Heather Sladick, Instructor Entertainment Design and Technology </strong>
                        									
                        <p>Heather Sladick has been involved with the film and live entertainment industry in
                           the Orlando area for the past 10 years, working on numerous local projects and crews.
                           She currently holds a degree in Film Production Technology, but has refocused most
                           of her time and interests back to live entertainment. While still active in the community,
                           she also enjoys being the part-time manager of the Performing Arts Center and teaching
                           as an adjunct at Valencia College. 
                        </p>
                        					
                     </div>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/entertainment-design-technology/faculty-staff.pcf">©</a>
      </div>
   </body>
</html>