<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links  | Valencia College</title>
      <meta name="Description" content="Valencia's Entertainment Design &amp; Technology degree program is one of only a few in the nation and the only one in Central Florida that prepares students to work in the production aspects of the entertainment industry.">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Entertainment Design &amp; Technology   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/entertainment-design-technology/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/entertainment-design-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Entertainment Design &amp; Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/entertainment-design-technology/">Entertainment Design &amp; Technology</a></li>
               <li>Links </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        		
                        <h2>Links</h2>
                        		
                        <div class="row">
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <li><strong>Broadway Across America, Orlando</strong>
                                    										
                                    <p><a href="http://www.broadwayacrossamerica.com/Orlando">http://www.broadwayacrossamerica.com/Orlando</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Stage Equipment and Lighting</strong>
                                    										
                                    <p><a href="http://www.seal-fla.com/sealdev/seal/About%2BUs">http://www.seal-fla.com/</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>FX Scenery and Display</strong>
                                    										
                                    <p><a href="http://www.fxgrouponline.com/">http://www.fxgrouponline.com/</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Orange County Convention Center</strong>
                                    										
                                    <p><a href="http://www.occc.net/">http://www.occc.net/</a></p>
                                 </li>
                                 								
                                 								
                                 								
                              </ul>
                              							
                           </div>
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <li><strong>PRG</strong>
                                    										
                                    <p><a href="http://www.prg.com/about-us">http://www.prg.com/</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Sea World, Orlando</strong>
                                    										
                                    <p><a href="http://www.seaworld.com/orlando/">http://www.seaworld.com/orlando/</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Walt Disney World</strong>
                                    										
                                    <p><a href="http://disneyworld.disney.go.com/">http://disneyworld.disney.go.com/</a></p>
                                 </li>
                                 								
                                 								
                              </ul>
                              							
                           </div>
                           						
                        </div>
                        					
                     </div>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/entertainment-design-technology/links.pcf">©</a>
      </div>
   </body>
</html>