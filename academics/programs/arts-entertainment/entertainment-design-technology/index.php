<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Entertainment Design &amp; Technology  | Valencia College</title>
      <meta name="Description" content="Valencia's Entertainment Design &amp; Technology degree program is one of only a few in the nation and the only one in Central Florida that prepares students to work in the production aspects of the entertainment industry.">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Entertainment Design &amp; Technology   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/entertainment-design-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/entertainment-design-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Entertainment Design &amp; Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Entertainment Design &amp; Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Entertainment Design &amp; Technology</h2>
                        		
                        <p>Valencia's Entertainment Design and Technology Degree Program</p>
                        		
                        <p>Valencia's Entertainment Design and Technology degree program is one of only a few
                           in the nation and the only one in Central Florida that prepares students to work in
                           the production aspects of the entertainment industry. It's goal is to produce skilled
                           designers and technicians for all facets of the live entertainment industry. Students
                           have an opportunity to specialize in Production Design or Live Show production. This
                           degree prepares students to function independently and as part of a creative team
                           in the preparation and production of live shows for concerts, theatre, theme parks,
                           and corporate presentations. With Orlando serving as the "undisputed entertainment
                           capital of the world," and with area businesses predicting continued growth, exciting
                           career opportunities abound for Valencia grads.
                        </p>
                        		
                        		
                        <hr>
                        		
                        <h4>This program offers specializations in:</h4>
                        						
                        <p>
                           <ul class="list_style_1">
                              							
                              <li>Production Design</li>
                              							
                              <li>Live Show Production</li>
                              						
                           </ul>
                        </p>
                        						
                        <p><br></p>
                        						
                        <h4>Primary Occupations:</h4>
                        						
                        <div class="row">
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <li>Lighting Technician</li>
                                 									
                                 <li>Technical Director</li>
                                 									
                                 <li>Stage Manager</li>
                                 									
                                 <li>Sound Technician</li>
                                 									
                                 <li>Scene Designer</li>
                                 									
                                 <li>Lighting Technician</li>
                                 								
                                 								
                                 								
                              </ul>
                              							
                           </div>
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <li>Audio/Visual Technician</li>
                                 									
                                 <li>Set Design Assistant</li>
                                 									
                                 <li>Scenic Carpenter</li>
                                 									
                                 <li>Sound Effects Specialist</li>
                                 									
                                 <li>Production Manager</li>
                                 									
                                 <li>Scenic Technician</li>
                                 								
                                 								
                              </ul>
                              							
                           </div>
                           						
                        </div>
                        		
                        <hr>
                        		
                        <h3>Contact Us</h3>
                        								
                        									
                        <div class="row">
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <p><strong>Current Students </strong></p>
                                 									
                                 <li>
                                    <p><strong>Kristin Abel </strong><br>
                                       										Program Chair Entertainment Design and Technology<br>
                                       										East Campus:&nbsp;<a href="mailto:407-582-2403">407-582-2403</a><br>
                                       										<a href="mailto:kabel@valenciacollege.edu">kabel@valenciacollege.edu</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Niurka Rivera </strong>
                                    										
                                    <p>Career Program Advisor<br>
                                       											Valencia Arts and Entertainment<br>
                                       											East Campus: &nbsp;<a href="tel:407-582-2179">407-582-2179</a><br>
                                       											<a href="mailto:nrivera4@valenciacollege.edu">nrivera4@valenciacollege.edu</a></p>
                                 </li>
                                 									
                                 									
                                 <li><strong>Wendy Givoglu</strong>
                                    										
                                    <p>Dean<br>
                                       											Valencia Arts and Entertainment<br>
                                       											East Campus: &nbsp;<a href="tel:407-582-2340">407-582-2340</a><br>
                                       											<a href="mailto:wgivoglu@valenciacollege.edu">wgivoglu@valenciacollege.edu</a></p>
                                 </li>
                                 									
                                 								
                                 								
                              </ul>
                              							
                           </div>
                           							
                           <div class="col-md-6 col-sm-6">
                              								
                              <ul class="list_style_1">
                                 									
                                 <p><strong>Prospective Students </strong></p>
                                 									
                                 <li>
                                    <p><strong>Prospective Students</strong><br>
                                       									Contact Enrollment Services at <a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a><br>
                                       											or call <a href=" tel:407-582-1507">407-582-1507</a></p>
                                 </li>
                                 									
                                 								
                              </ul>
                              							
                           </div>
                           						
                        </div>
                        					
                     </div>
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/entertainment-design-technology/index.pcf">©</a>
      </div>
   </body>
</html>