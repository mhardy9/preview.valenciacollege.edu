<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Certificate | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/certificate.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Entertainment</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Certificate</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Certificate</h2>
                        
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        
                        <ul class="list_style_1">
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/soundandmusictechnology/#certificatetext">Audio Electronics Specialist</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Media Development</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Media Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Media Video Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Media Web Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/soundandmusictechnology/#certificatetext">Digital Music Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Video Editing and Post-Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Digital Video Fundamentals</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/entertainmentdesigntechnology/#certificatetext">Entertainment – Stage Technology</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/filmproductiontechnology/#certificatetext">Film Production Fundamentals</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/graphicandinteractivedesign/#certificatestext">Graphic Design Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/graphicandinteractivedesign/#certificatestext">Graphic Design Support</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/graphicandinteractivedesign/#certificatestext">Graphics – Interactive Design Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/graphicandinteractivedesign/#certificatestext">Graphics – Interactive Design Support</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/soundandmusictechnology/#certificatetext">Sound and Music Production – AudioVisual Production</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Webcast Media</a></li>
                           	
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/artsentertainment/digitalmediatechnology/#certificatestext">Webcast Technology</a></li>
                           
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/certificate.pcf">©</a>
      </div>
   </body>
</html>