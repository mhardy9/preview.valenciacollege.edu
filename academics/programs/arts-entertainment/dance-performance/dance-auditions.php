<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/dance-auditions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Dance Auditions</h2>
                        
                        <h3>2017/2018 Associate in Arts Pre-Major in Dance Performance Degree Audition Schedule</h3>
                        
                        <h4>TO SCHEDULE AN AUDITION:</h4>
                        
                        <p><a href="https://net4.valenciacollege.edu/forms/artsandentertainment/dance/audition_form.cfm" target="_blank">Audition Form</a></p>
                        
                        <p>The AA in  Dance Performance audition will consist of a technique class: ballet barre
                           and  center, modern combination and jazz combination. Individual choreography or a
                           solo is not required. Participants are encouraged to wear black leotards and  pink
                           tights for the women and white shirt and black tights for the men. Please  bring all
                           dance shoes. The classes listed below (*) are required for enrollment  into the major
                           program. Prior to acceptance into the program, the student may  have arranged an academic
                           schedule of classes which may need to be changed. In  the event a new dance major
                           is having difficulty arranging their schedule to  include these dance classes, the
                           Department will provide assistance to  accommodate their needs.
                        </p>
                        
                        <p> The  following auditions correspond with Valencia College Dance performances. Each
                           audition participant is encouraged to attend a performance and is provided with  one
                           complimentary ticket for that corresponding event.
                        </p>
                        
                        
                        
                        <h3>2017 - 2018 Audition Schedule</h3>
                        
                        <h4>Fall  Semester</h4>
                        
                        <p><strong>Saturday, November 4, 2017:  9:00-10:30 a.m.</strong><strong> East Campus,  Building 6, Room 104 <br>
                              </strong>The audition will consist of a  technique class: ballet barre and center, modern combination
                           and jazz combination.  Individual choreography or a solo is not required. Participants
                           are encouraged  to wear black leotards and pink tights for the women and white shirt
                           and black  tights for the men. Please bring all dance shoes.
                        </p>
                        
                        
                        <h4>Spring  Semester</h4>
                        
                        <p><strong>Saturday, March 3, 2018: 9:00-10:30  a.m.</strong> <strong>East Campus, Building 6, Room  104 </strong><br>
                           The audition will consist of a technique class: ballet barre and center, modern  combination
                           and jazz combination. Individual choreography or a solo is not  required. Participants
                           are encouraged to wear black leotards and pink tights  for the women and white shirt
                           and black tights for the men. Please bring all  dance shoes.
                        </p>
                        
                        
                        <h4>Summer  Semester</h4>
                        
                        <p><strong>Saturday, July 7, 2018: 9:00-10:30  a.m.</strong> <strong>East Campus, Building 6, Room  104 </strong><br>
                           The audition will consist of a  technique class: ballet barre and center, modern combination
                           and jazz  combination. Individual choreography or a solo is not required. Participants
                           are encouraged to wear black leotards and pink tights for the women and white  shirt
                           and black tights for the men. Please bring all dance shoes.
                        </p>
                        
                        
                        <p>(*) All new Dance Majors are required to  reserve space in their schedule for the
                           classes listed below. 
                        </p>
                        
                        <p>After the  audition and upon acceptance into the degree program, students will be
                           registered in the following classes:
                        </p>
                        
                        <p> <strong>DAA 2610C Dance Composition and Improvisation I</strong>: M &amp; W 1:00-4:00  p.m.<br>
                           <strong>DAA 1104C Modern I for Pre-Majors</strong>: M &amp; W 8:30 - 10:00 a.m.<br>
                           <strong>DAA 1204C Ballet I for Pre-Majors</strong>: T &amp; TH 8:30-10:00 a.m.
                        </p>
                        
                        <p> Any additional questions, please  contact <a href="mailto:Kstevens9@valenciacollege.edu">Kristina Stevens</a>, Dance Performance Production Coordinator in the Dance  Department 407-582-2954.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/dance-auditions.pcf">©</a>
      </div>
   </body>
</html>