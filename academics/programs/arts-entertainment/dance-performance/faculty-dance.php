<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/faculty-dance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Dance Faculty</h2>
                        
                        <p><strong><a href="mailto:ssalapa@valenciacollege.edu">Dr. Suzanne R. Salapa </a></strong><br>
                           Chair,  Department of Dance
                        </p>
                        
                        <p>Dr.  Salapa began her training in Virginia with the Annandale Dance Theater and  continued
                           with the Washington Ballet, Maryland Youth Ballet and Columbia City  Ballet. She received
                           her Bachelor of Science from the Conservatory at Shenandoah  University and completed
                           her Master of Fine Arts in Dance from Florida State  University. While attending FSU,
                           she was awarded the Florida State Friends of  Dance Scholarship and a teaching assistantship.
                           She received her Doctor of Education  in Curriculum and Instruction at the University
                           of Central Florida. In  addition, she has served on the faculty of George Mason University
                           and  Shenandoah University. She is a member of the dance faculty at Valencia College
                           and Rollins College.  Dr. Salapa has  participated as an adjudicator for the Florida
                           Dance Performance Assessment/Florida  Dance Education Organization and participated
                           in the State of Florida  Department of Education K-12 Dance Curriculum Committee as
                           an author of middle  and high school dance curriculum course descriptions, standards
                           and benchmarks.  In 2006, she received the Dance Teacher Magazine Higher Education
                           Teacher of  the Year. In the summer of 2014, Dr. Salapa participated in the national/
                           internationally renowned program Dance for PD®. In collaboration between the  Mark
                           Morris Dance Group and the Brooklyn Parkinson Group, this dance teacher-training 
                           model is specifically designed to encourage creativity and movement exploration  for
                           those with Parkinson's disease. She teaches the bi-monthly Movement as  Medicine dance
                           class for Florida Hospital, Orlando.
                        </p>
                        
                        <p><strong><a href="mailto:lbrasseuxrodgers@valenciacollege.edu">Lesley Brasseux Rodgers</a></strong><br>
                           Artistic  Director, Valencia Dance and Valencia Dance Theatre
                        </p>
                        
                        <p>Ms. Brasseux Rodgers began her training at the school of  Southern Ballet in Atlanta,
                           Georgia. She has attended the Martha Graham School  of Contemporary Dance in New York
                           City and the American Dance Festival in  Durham, North Carolina. Ms. Brasseux Rodgers
                           holds a Bachelor of Science degree  from University of Florida and a Master of Fine
                           Arts in Dance from Louisiana  State University and has served on the faculty of a
                           variety of colleges,  universities, and professional dance schools. Since 1980, Ms.
                           Brasseux Rodgers  has choreographed works for Valencia College, Southern Ballet Theater,
                           the  University of Central Florida, the Florida Dance Association and Rollins  College.
                           She received the Monticello Scholarship for Young Choreographers from  the National
                           Association for Regional Ballet and in 1985 was nominated by the  Orlando Arts Council
                           for Outstanding Individual Achievement in the Arts. Ms.  Brasseux Rodgers is currently
                           a member of the dance faculty at Valencia  College, and a Dual-Enrollment instructor
                           for the Dance Arts Magnet program at  Dr. Phillips High School. She was chosen as
                           an associate for residency of Merce  Cunningham at the Atlantic Center of the Arts
                           in New Smyrna Beach, Florida and  is a member of the Center.
                        </p>
                        
                        <p> <strong><a href="mailto:kstevens9@valenciacollege.edu">Kristina Stevens</a></strong><br>
                           Dance  Performance Production Coordinator
                        </p>
                        
                        <p>Ms.  Stevens began her training at the Chico Community Ballet in Chico, CA. <br>
                           She  attended summer programs at the North Carolina School of the Arts, the <br>
                           Joffrey  Workshop in San Antonio, TX, and on a partial scholarship at the Joffrey
                           Ballet  School in New York. In addition to performing in Entertainment at Walt Disney
                           World in Orlando, FL, Ms. Stevens was awarded two contracts to perform at Tokyo  Disneyland
                           in Japan. She danced professionally with The Georgia Ballet in  Marietta, GA under
                           the artistic direction of Iris Hensley, and received  additional training from Janusz
                           Mazon and Gina Hyatt-Mazon. Ms. Stevens  received both an Associate in Arts degree
                           in Dance Performance and an Associate  in Science in Business Administration from
                           Valencia College, and received a  Bachelor of Arts in Humanities from Rollins College.
                           In addition to being the  Dance Performance Production Coordinator at Valencia College,
                           Ms. Stevens is an  adjunct faculty member in the Dance Department at both Valencia
                           College and  Rollins College.
                        </p>
                        
                        <p><strong><a href="mailto:kjudy1@valenciacollege.edu">Katrina Judy</a></strong><br>
                           Professor
                        </p>
                        
                        <p>Mrs. Judy began her dance training with the Royal School  of Dance in Winter Park,
                           Florida.  She  received a Bachelor's in Dance from Jacksonville University and a Master
                           of  Fine Arts in Dance from Florida State University.  While attending Florida State
                           University she  received a teaching assistantship in ballet.   During her time in
                           college, she had the opportunity to perform with the  Jacksonville Ballet Theater
                           and the Tallahassee Ballet Company.  She has been teaching in the Orlando area now
                           for twenty-one years.  She is a member of  the dance faculty at Valencia College,
                           University of Central Florida and  Rollins College where she teaches ballet and modern
                           dance.
                        </p>
                        
                        <p> <strong><a href="mailto:kfollensbee@valenciacollege.edu">Katherine Follensbee</a></strong><br>
                           Professor
                        </p>
                        
                        <p>Kathy Follensbee received a Bachelor of Science degree  in Physical Education from
                           Florida State University and a Masters of Fine Arts  in Choreography from Jacksonville
                           University.&nbsp; She is the full time Dance  Director at Dr. Phillips Visual &amp; Performing
                           Arts High School (DPHS - VPA)  as well as an adjunct professor of Modern with Valencia
                           College.&nbsp; Kathy is  a past President of Florida Dance Masters (FDM) and the founding
                           Secretary for  the Florida Dance Education Organization (FDEO).&nbsp; She has served on
                           writing teams for the Florida Next Generation Sunshine State Standards,  Florida's
                           Race To The Top and Orange County Public Schools End of Course Exams  (EOCs) for Dance.&nbsp;&nbsp;
                           Kathy was a Synchronized Swimmer and Dancer for  the Louisiana World's Fair Exposition
                           in 1984, and is a Master's National  Synchronized Swimming Champion as well. She has
                           judged dance contests and  taught master classes throughout Florida and New York.&nbsp;
                           
                        </p>
                        
                        <p><strong><a href="mailto:jsmith214@valenciacollege.edu">Jessica Smith</a></strong><br>
                           Professor
                        </p>
                        
                        <p>Ms. Smith began her dance training at Deltona Dance  Academy under the direction of
                           Shelleh Harkcom.   She continued her training at Focus Performing Arts Studio under
                           the direction  of Jenny Strickland-Clifton, where she has been an instructor/choreographer
                           for  twelve years.  A graduate of Pine Ridge  High School in Deltona, Ms. Smith was
                           a member and officer of their nationally  ranked dance team.  During this time, she
                           took many classes from noted choreographers from New York, Los Angeles, and San  Francisco.
                           Continuing her studies in  dance, she received her Associate in Arts in Dance Performance
                           degree from  Valencia College, where she was a member and officer of Art in Motion.
                           While attending Valencia, her choreographic  works were performed at Dr. Phillips
                           High School and the University of  Florida.  A graduate of the class of 2009  from
                           the University of Central Florida, her college volunteer hours were spent  working
                           with students at Dr. Phillips Visual &amp; Performing Arts High School  (DPHS - VPA) and
                           the dance curriculum at Seminole High School in Sanford,  Florida.  In addition to
                           her duties at  dance studios in the Orlando area, Ms. Smith currently choreographs
                           works for Dr.  Phillips Visual &amp; Performing Arts High School (DPHS - VPA) and Valencia
                           College.
                        </p>
                        
                        <p><strong><a href="mailto:asaxman@valenciacollege.edu">Amanda Saxman</a></strong><br>
                           Professor          
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Upcoming Performances</h3>
                        
                        
                        <div data-id="choreographers_showcase_6146">
                           
                           
                           <div>
                              Nov<br>
                              <span>17</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/choreographers_showcase_6146" target="_blank">Choreographers' Showcase at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="3_in_motion_3920">
                           
                           
                           <div>
                              Jan<br>
                              <span>26</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">3 in Motion at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_spring_dance_concert_7815">
                           
                           
                           <div>
                              Mar<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_spring_dance_concert_7815" target="_blank">Valencia Spring Dance Concert at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_dance_summer_repertory_concert_2055">
                           
                           
                           <div>
                              Jul<br>
                              <span>13</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_dance_summer_repertory_concert_2055" target="_blank">Valencia Dance Summer Repertory Concert at Valencia College East Campus Performing
                                 Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <p>Tickets for performances: 407-582-2900 or <a href="http://www.valenciacollege.edu/arts">valenciacollege.edu/arts</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <link href="../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <a href="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/39990372/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1504301346&amp;entry_id=1_72nf3a9z&amp;flashvars%5BstreamerType%5D=auto" target="_blank"><img alt="2017 Summer Repertory Concert Video" height="195" src="../../arts-and-entertainment/dance/Dance-2017-Summer-Repertory-Concert.jpg" width="245"><br>2017 Summer Repertory Concert Video</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           
                           <div>
                              <a href="https://www.facebook.com/ValenciaDance/" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>    
                           
                           <div>
                              <a href="https://www.instagram.com/valenciadance/" target="_blank" title="Instagram"><span>Instagram</span></a>
                              
                           </div>
                           
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/faculty-dance.pcf">©</a>
      </div>
   </body>
</html>