<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/valencia-summer-dance-institute.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Valencia Summer Dance Institute (for high school students)</h2>
                        
                        <p>A staple at Valencia College since 2000, Valencia’s four-week summer program  is designed
                           for 20-25 ninth through twelfth grade high school students.
                        </p>
                        
                        <p> Focusing on the development of students’ processing skills and meaningful  demonstration
                           of learning, the program’s multidisciplinary approach provides  students with authentic
                           learning tasks and environments, infused technology,  and enables students' continuous
                           learning, assessment, and reflection of  interrelated disciplines.
                        </p>
                        
                        <p> The project has three components: hands-on, field, and classroom  instruction. Students
                           will participate in the development of dance, cooperative  learning, development of
                           communication skills, work with various artists in the  field of dance, and have the
                           opportunity to demonstrate acquired skills in  these areas.
                        </p>
                        
                        <p> The program is also geared toward the recruitment and retention of dancers  for future
                           admittance to Valencia’s Associate in Arts in Dance Performance  Degree.
                        </p>
                        
                        
                        
                        <h3>Important Dates</h3>
                        
                        <p><strong>VSDI AUDITIONS FOR 2018:</strong><br>
                           SATURDAY, MAY 12, 2018: 9:00 - 10:30 A.M. EAST  CAMPUS, BUILDING 6-104
                           
                        </p>
                        
                        <p><strong>2018 Valencia Summer Dance Institute Program Dates: </strong><br>
                           Monday, June 18, 2018 - Saturday, July 14, 2017 
                        </p>
                        
                        <p><strong>Valencia Dance Summer Repertory and Valencia Summer Dance Institute Performance:</strong><br>
                           Friday and Saturday, July 13-14, 2018 <br>
                           East Campus Performing Arts Center, 8:00 p.m. <br>
                           For ticket information visit: <a href="http://valenciacollege.edu/arts/">http://valenciacollege.edu/arts</a></p>
                        
                        
                        <h4>FOR MORE INFORMATION AND A PRINTABLE APPLICATION, PLEASE CLICK ON THE LINKS BELOW:</h4>
                        
                        <ul>
                           
                           <li><a href="documents/vsdi-audition-application-2018.pdf" target="_blank">Printable Audition Application</a></li>
                           
                           <li><a href="documents/vsdi-parent-signature-forms-2018.pdf" target="_blank">VSDI Parent Signature Forms</a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/valencia-summer-dance-institute.pcf">©</a>
      </div>
   </body>
</html>