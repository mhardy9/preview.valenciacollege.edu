<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/performance-schedule.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Performance and Ticket Information</h2>
                        
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/choreographers_showcase_6146" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>Choreographers' Showcase at Valencia College East Campus Performing Arts Center</span> <br>
                                 
                                 
                                 <span>
                                    <strong>November 17</strong>
                                    
                                    at 8:00 PM</span> 
                                 
                                 <p>The Choreographers' Showcase is in its nineteenth year of performances!  Designed
                                    to showcase student choreography, this annual event provides students with a creative
                                    outlet...
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>3 in Motion at Valencia College East Campus Performing Arts Center</span> <br>
                                 
                                 
                                 <span>
                                    <strong>January 26</strong>
                                    
                                    at 8:00 PM</span> 
                                 
                                 <p>Dr. Phillips High School Dance Magnet Program, Yow Dance and Valencia College's Valencia
                                    Dance Theatre are pleased to present a joint performance all on one stage in this...
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/valencia_spring_dance_concert_7815" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>Valencia Spring Dance Concert at Valencia College East Campus Performing Arts Center</span> <br>
                                 
                                 
                                 <span>
                                    <strong>March 23</strong>
                                    
                                    at 8:00 PM</span> 
                                 
                                 <p>The concert, which highlights the work of guest choreographers and Valencia faculty
                                    members, features student, faculty and guest dancers performing modern dance and ballet....
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/valencia_dance_summer_repertory_concert_2055" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>Valencia Dance Summer Repertory Concert at Valencia College East Campus Performing
                                    Arts Center</span> <br>
                                 
                                 
                                 <span>
                                    <strong>July 13</strong>
                                    
                                    at 8:00 PM</span> 
                                 
                                 <p>The concert will showcase the work of high school students who participate in the
                                    Valencia Summer Dance Institute, as well as Valencia College dance majors.  The Valencia...
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Upcoming Performances</h3>
                        
                        
                        <div data-id="choreographers_showcase_6146">
                           
                           
                           <div>
                              Nov<br>
                              <span>17</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/choreographers_showcase_6146" target="_blank">Choreographers' Showcase at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="3_in_motion_3920">
                           
                           
                           <div>
                              Jan<br>
                              <span>26</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">3 in Motion at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_spring_dance_concert_7815">
                           
                           
                           <div>
                              Mar<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_spring_dance_concert_7815" target="_blank">Valencia Spring Dance Concert at Valencia College East Campus Performing Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="valencia_dance_summer_repertory_concert_2055">
                           
                           
                           <div>
                              Jul<br>
                              <span>13</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/valencia_dance_summer_repertory_concert_2055" target="_blank">Valencia Dance Summer Repertory Concert at Valencia College East Campus Performing
                                 Arts Center</a><br>
                              
                              <span>8:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <p>Tickets for performances: 407-582-2900 or <a href="http://www.valenciacollege.edu/arts">valenciacollege.edu/arts</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <link href="../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <a href="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/39990372/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1504301346&amp;entry_id=1_72nf3a9z&amp;flashvars%5BstreamerType%5D=auto" target="_blank"><img alt="2017 Summer Repertory Concert Video" height="195" src="../../arts-and-entertainment/dance/Dance-2017-Summer-Repertory-Concert.jpg" width="245"><br>2017 Summer Repertory Concert Video</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           
                           <div>
                              <a href="https://www.facebook.com/ValenciaDance/" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>    
                           
                           <div>
                              <a href="https://www.instagram.com/valenciadance/" target="_blank" title="Instagram"><span>Instagram</span></a>
                              
                           </div>
                           
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/performance-schedule.pcf">©</a>
      </div>
   </body>
</html>