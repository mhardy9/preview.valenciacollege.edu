<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Events &amp; Tickets | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/events-tickets.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Events &amp; Tickets</h2>
                        
                        
                        
                        
                        <h3>Ticket Information</h3>
                        
                        <p>Tickets are $12.00 for General  Seating, $10.00 for Valencia Employees, students and
                           seniors for all Valencia  Dance Performances held at Valencia, and $6.00 for children
                           12 years old and  under. For more information on tickets please call the Valencia
                           Box Office at  407-582-2900 or visit the box office website at: <a href="http://www.valenciacollege.edu/arts">http://valenciacollege.edu/arts</a></p>
                        
                        <p> <a href="performances.html">Entire 2017-2018 performance schedule</a>.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     	  
                     <hr>
                     
                     <h3>Upcoming Performances</h3>
                     
                     
                     <div data-id="choreographers_showcase_6146">
                        
                        
                        <div>
                           Nov<br>
                           <span>17</span>
                           
                        </div>
                        
                        
                        <div>        
                           <a href="http://events.valenciacollege.edu/event/choreographers_showcase_6146" target="_blank">Choreographers' Showcase at Valencia College East Campus Performing Arts Center</a><br>
                           
                           <span>8:00 PM</span>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <div data-id="3_in_motion_3920">
                        
                        
                        <div>
                           Jan<br>
                           <span>26</span>
                           
                        </div>
                        
                        
                        <div>        
                           <a href="http://events.valenciacollege.edu/event/3_in_motion_3920" target="_blank">3 in Motion at Valencia College East Campus Performing Arts Center</a><br>
                           
                           <span>8:00 PM</span>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <div data-id="valencia_spring_dance_concert_7815">
                        
                        
                        <div>
                           Mar<br>
                           <span>23</span>
                           
                        </div>
                        
                        
                        <div>        
                           <a href="http://events.valenciacollege.edu/event/valencia_spring_dance_concert_7815" target="_blank">Valencia Spring Dance Concert at Valencia College East Campus Performing Arts Center</a><br>
                           
                           <span>8:00 PM</span>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <div data-id="valencia_dance_summer_repertory_concert_2055">
                        
                        
                        <div>
                           Jul<br>
                           <span>13</span>
                           
                        </div>
                        
                        
                        <div>        
                           <a href="http://events.valenciacollege.edu/event/valencia_dance_summer_repertory_concert_2055" target="_blank">Valencia Dance Summer Repertory Concert at Valencia College East Campus Performing
                              Arts Center</a><br>
                           
                           <span>8:00 PM</span>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     
                     <p>Tickets for performances: 407-582-2900 or <a href="http://www.valenciacollege.edu/arts">valenciacollege.edu/arts</a></p>
                     
                     
                     
                     
                     	 
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/events-tickets.pcf">©</a>
      </div>
   </body>
</html>