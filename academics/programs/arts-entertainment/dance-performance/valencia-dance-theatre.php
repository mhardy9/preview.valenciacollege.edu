<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/valencia-dance-theatre.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <img alt="Valencia Dance Theatre " height="408" src="../../arts-and-entertainment/dance/VDTbanner_002.jpg" width="666">
                        
                        <p>Valencia Dance was established on  Valencia College’s East Campus in the early 1980s
                           as an outlet for dancers to  perform and enhance recognition for the arts. In the
                           early 1990s, Valencia was  presented with a unique opportunity to support the increased
                           interest in dance  and embarked upon a collaborative venture to jointly produce dance
                           performances  with Rollins College and the School for the Performing Arts. As the
                           dance  department grew, the program developed into Central Florida’s first dance 
                           degree, the Associate in Arts in Dance Performance. Valencia Dance was then  redesigned
                           to reflect the expanding scope of performance opportunities and  renamed Valencia
                           Dance Theatre (VDT). VDT presents four major dance concerts on  the East Campus each
                           year, tours to area Central Florida K-12 schools,  community centers, and conferences
                           throughout the country. VDT continues the  close relationship with community partner
                           Dr. Phillips High School Dance Magnet  Arts Program. Graduates of Valencia College’s
                           Pre-Major program and former  members of Valencia Dance Theatre have gone on to major
                           in dance at state and  nationwide colleges and universities as well as dance professionally
                           with dance  companies and nationally touring shows.
                        </p>
                        
                        
                        <h2>VALENCIA  DANCE THEATRE LECTURE DEMONSTRATIONS
                           AND COMMUNITY OUTREACH PARTICIPATION
                        </h2>
                        
                        <ul>
                           
                           <li>Cypress  Elementary School </li>
                           
                           <li>Daytona State College</li>
                           
                           <li>Deland High  School</li>
                           
                           <li>Dr. Phillips  High School</li>
                           
                           <li>East Lake  Elementary School</li>
                           
                           <li>Full Sail  University</li>
                           
                           <li>Good Samaritan Retirement Village</li>
                           
                           <li>Kissimmee  Elementary School</li>
                           
                           <li>Lawton Chiles Elementary School</li>
                           
                           <li>Poinciana  Academy of Fine Arts </li>
                           
                           <li>Thacker Avenue Elementary School for  International Studies</li>
                           
                           <li>University of Florida</li>
                           
                           <li>University of South Florida            </li>
                           
                        </ul>
                        
                        <p>If  you are interested in booking a Lecture Demonstration with Valencia Dance  Theatre
                           please contact Kristina Stevens, Dance Performance Production Coordinator,  through
                           our <a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Dance/contact.cfm" target="_blank">Contact Us Page</a>.
                        </p>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/valencia-dance-theatre.pcf">©</a>
      </div>
   </body>
</html>