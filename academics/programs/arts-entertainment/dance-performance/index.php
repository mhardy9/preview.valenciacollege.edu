<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance Performance  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance Performance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Dance Performance</h2>
                        		
                        <p>Associate in Arts Degree</p>
                        	
                        <h3>Who Are We?</h3>
                        		
                        <p>Welcome to the Valencia College Dance Program! Our goal is to provide quality technical
                           training and the enhancement of the overall dance experience. We are committed to
                           both students who desire dance as a professional career and novice students who would
                           like to experience a new form of artistic expression. A strong technical emphasis
                           is placed on a wide variety of dance disciplines available to students from any dance
                           background. We provide a variety of performance opportunities for our dancers with
                           the goal of strengthening their professional training experience. Valencia College
                           offers a solid dance education, one that will ensure its students a future in the
                           performing arts.
                        </p>
                        				
                        		
                        <hr>
                        
                        <h3>About Dance Department</h3>
                        
                        <p>Valencia  College Department of Dance offers classes geared for dance majors and 
                           non-majors. Designed to promote arts awareness, cultivate creativity through  choreography,
                           performance opportunities, enhance technical proficiency and  prepare the student
                           for a place in the arts, this dance program has a lot to  offer a dancer in a variety
                           of disciplines. For the dancer who is a general  Associate in Arts major, classes
                           will challenge and stimulate your technical  and performance abilities, simultaneously
                           providing for elective credit towards  your degree. For the avid performer who  aspires
                           for a future in the arts, the Associate in Arts in Dance Performance  Pre-Major Degree
                           is grounded in a conservatory approach that emphasizes a  strong technical base along
                           with a wide range of dance disciplines, styles and  viewpoints. The Associate in Arts
                           in Dance Performance program participates in  four annual on-campus dance performances
                           as well as regional performances with  resident dance company <a href="/academics/programs/art-entertainment/dance-performance/valencia-dance-theatre.php">Valencia Dance Theatre (VDT)</a>. VDT provides dance education to  Central Florida elementary, middle and high schools
                           and community partners that  span a four-county area throughout the state of Florida.
                           The mission of VDT is  to introduce students to the art form of dance and to recognize
                           the importance  of the arts. AA in Dance Performance majors focus on creativity in
                           the form of  choreography. These dancers vigorously hone their choreographic skills
                           to  create a voice through movement. Events are designed to further their  choreographic
                           development through on campus and off campus performances. 
                        </p>
                        		
                        <p>Valencia's pre-majors are designed  for a student who plans to earn the Associate
                           in Arts degree and transfer to  one of the state universities or state colleges in
                           Florida as a junior to  complete a Bachelor's degree in one of the specific majors.
                           Each pre-major  includes the courses to satisfy Valencia's general education requirements
                           for  the A.A. degree as well as the statewide common prerequisites for the specific
                           major. The Associate in Arts degree requires a minimum of 60 acceptable  college-level
                           credit hours, which include limited-access status occurs when  student demand exceeds
                           available resources such as faculty, instructional  facilities, equipment, etc. Admission
                           to Valencia does not guarantee acceptance  to a limited-access degree in which the
                           number of students who can enroll is  limited. Limited-access degrees have specific
                           admission requirements. The A.A.  Pre-Major: Dance Performance is the only pre-major
                           with limited-access status  with 36 credits of general education and 24 credit hours
                           of electives. 
                        </p>
                        	
                        	
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Degree Requirements</a> 
                        </p>
                        
                        <ul class="list_style_1">
                           			
                           <li> <a href="http://valenciacollege.edu/calendar/">Valencia College Calendar</a></li>
                           
                           <li><a href="http://www.fdeo.org/fdeonew/FDEO/">Florida Dance Education Association </a> 
                           </li> 
                           				
                           <li> <a href="http://www.ndeo.org/">National Dance Education Association</a></li>
                           
                           <li> <a href="http://www.acda.dance/">American College Dance Association</a></li>
                           
                           		
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/index.pcf">©</a>
      </div>
   </body>
</html>