<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/program-requirements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2> Program Requirements</h2>
                        
                        <h3>Associate in Arts in Dance Performance Program Outcomes</h3>
                        
                        <ul type="disc">
                           
                           <li>Demonstrate technical proficiency in dance technique       and performance.</li>
                           
                           <li>Demonstrate an individual choreographic voice through       the development of abstract
                              and structured creative work.
                           </li>
                           
                           <li>Show evidence of performance elements through artistry,       musicality, projection
                              of character and intent, and the refinement of       personal technical clarity and
                              movement comprehension.
                           </li>
                           
                           <li>Demonstrate readiness to transfer            </li>
                           
                        </ul>
                        
                        <h3>Communications Credits</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=ENC%201101" title="ENCÂ&nbsp;1101">ENC&nbsp;1101</a></div>
                                 
                                 <div data-old-tag="td">FRESHMAN COMPOSITION I +*~</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=SPC%201608" title="SPCÂ&nbsp;1608">SPC&nbsp;1608</a></div>
                                 
                                 <div data-old-tag="td">FUNDAMENTALS OF SPEECH ~</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=SPC%201017" title="SPCÂ&nbsp;1017">SPC&nbsp;1017</a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Interpersonal Communication</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=ENC%201102" title="ENCÂ&nbsp;1102">ENC&nbsp;1102</a></div>
                                 
                                 <div data-old-tag="td">Freshman Comp II +*~</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=SLS%201122" title="SLSÂ&nbsp;1122">SLS&nbsp;1122</a></div>
                                 
                                 <div data-old-tag="td">New Student Experience ~</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Humanities Credits</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> Core Requirement ~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Humanities</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> Institutional Requirement <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">(GR)</a> +*~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Mathematics Credits</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Recommended Core</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://catalog.valenciacollege.edu/search/?P=MGF%201106" title="MGFÂ&nbsp;1106">MGF&nbsp;1106</a> <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">(GR)</a> +*~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Recommended</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://catalog.valenciacollege.edu/search/?P=MGF%201107" title="MGFÂ&nbsp;1107">MGF&nbsp;1107</a> <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">(GR)</a> +*~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Science Credits</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Science</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> CoreRequirement ~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Science</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> Core or Institutional Requirement ~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Social Science Credits</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Social Science</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> Core Requirement ~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Social Science</div>
                                 
                                 <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">Gen. Ed.</a> Institutional Requirement <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/premajorsatvalencia/danceperformance/">(GR)</a> +*~
                                 </div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>Additional Common Prerequisites</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Course</div>
                                 
                                 <div data-old-tag="th">Title</div>
                                 
                                 <div data-old-tag="th">Credit</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201104C" title="DAAÂ&nbsp;1104C">DAA&nbsp;1104C</a></div>
                                 
                                 <div data-old-tag="td">MODERN DANCE I FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201204C" title="DAAÂ&nbsp;1204C">DAA&nbsp;1204C</a></div>
                                 
                                 <div data-old-tag="td">BALLET I FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202610C" title="DAAÂ&nbsp;2610C">DAA&nbsp;2610C</a></div>
                                 
                                 <div data-old-tag="td">DANCE COMPOSITION &amp;    IMPROVISATION I FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201105C" title="DAAÂ&nbsp;1105C">DAA&nbsp;1105C</a></div>
                                 
                                 <div data-old-tag="td">MODERN DANCE II FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201205C" title="DAAÂ&nbsp;1205C">DAA&nbsp;1205C</a></div>
                                 
                                 <div data-old-tag="td">BALLET II FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202611C" title="DAAÂ&nbsp;2611C">DAA&nbsp;2611C</a></div>
                                 
                                 <div data-old-tag="td">DANCE COMPOSITION &amp; IMPROVISATION    II FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202206C" title="DAAÂ&nbsp;2206C">DAA&nbsp;2206C</a></div>
                                 
                                 <div data-old-tag="td">BALLET III FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202682C" title="DAAÂ&nbsp;2682C">DAA&nbsp;2682C</a></div>
                                 
                                 <div data-old-tag="td">PERFORMANCE ENSEMBLE: VALENCIA    DANCE THEATRE FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201160C" title="DAAÂ&nbsp;1160C">DAA&nbsp;1160C</a></div>
                                 
                                 <div data-old-tag="td">MODERN DANCE III FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201650C" title="DAAÂ&nbsp;1650C">DAA&nbsp;1650C</a></div>
                                 
                                 <div data-old-tag="td">DANCE PRODUCTION +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201680C" title="DAAÂ&nbsp;1680C">DAA&nbsp;1680C</a></div>
                                 
                                 <div data-old-tag="td">REPERTORY I FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202106C" title="DAAÂ&nbsp;2106C">DAA&nbsp;2106C</a></div>
                                 
                                 <div data-old-tag="td">INTERMEDIATE MODERN DANCE I FOR    PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202107C" title="DAAÂ&nbsp;2107C">DAA&nbsp;2107C</a></div>
                                 
                                 <div data-old-tag="td">INTERMEDIATE MODERN DANCE II FOR    PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202207C" title="DAAÂ&nbsp;2207C">DAA&nbsp;2207C</a></div>
                                 
                                 <div data-old-tag="td">INTERMEDIATE BALLET I FOR    PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%202219C" title="DAAÂ&nbsp;2219C">DAA&nbsp;2219C</a></div>
                                 
                                 <div data-old-tag="td">INTERMEDIATE BALLET II FOR    PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">1</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=DAA%201681C" title="DAAÂ&nbsp;1681C">DAA&nbsp;1681C</a></div>
                                 
                                 <div data-old-tag="td">REPERTORY II FOR PRE-MAJORS +*#</div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Total Credit Hours</div>
                                 
                                 <div data-old-tag="td">60</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">+</div>
                                 
                                 <div data-old-tag="td">This course must be completed with    a grade of C or better.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">*</div>
                                 
                                 <div data-old-tag="td">This course has a prerequisite;    check description in Valencia catalog.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">(GR)</div>
                                 
                                 <div data-old-tag="td">Denotes a Gordon Rule course.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">~</div>
                                 
                                 <div data-old-tag="td">This is a General Education    course.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">#</div>
                                 
                                 <div data-old-tag="td">This course is a common    prerequisite stated for the degree listed in <a href="http://www.flvc.org/" target="_blank">flvc.org</a>.
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Note: </strong>Specialized courses may not be offered every session or on  every campus.        
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/program-requirements.pcf">©</a>
      </div>
   </body>
</html>