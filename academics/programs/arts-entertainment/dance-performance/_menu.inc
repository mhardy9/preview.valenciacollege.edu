 <ul>
            <li><a href="/academics/programs/arts-entertainment/dance-performance/index.php">Dance Performance </a></li>
            <li class="submenu">
<a href="#">Program of Study <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                <li><a href="/academics/programs/arts-entertainment/dance-performance/degree-in-dance.php">Degree in Dance</a></li>
                    <li><a href="/academics/programs/arts-entertainment/dance-performance/non-majors-programs.php">Non-Majors Programs</a></li>
                 
               </ul>
            </li>

            <li class="submenu">
<a href="#">Events|Performances <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
              <ul>
                <li><a href="/academics/programs/arts-entertainment/dance-performance/events-tickets.php">Events and Tickets</a></li>
                    <li><a href="/academics/programs/arts-entertainment/dance-performance/performance-schedule.php">Performance Schedule</a></li>
                    <li><a href="/academics/programs/arts-entertainment/dance-performance/dance-auditions.php">Dance Auditions</a></li>
              </ul>
            </li>
             <li><a href="/academics/programs/arts-entertainment/dance-performance/frequently-asked-questions.php">Frequently Asked Questions</a></li>
              <li><a href="/academics/programs/arts-entertainment/dance-performance/valencia-dance-theatre.php">Valencia Dance Theatre</a></li>

              <li><a href="/academics/programs/arts-entertainment/dance-performance/valencia-summer-dance-institute.php">VSDI</a></li>
                
         <li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Dance/contact.cfm" target="_blank">Contact</a></li>
    		
          </ul>
