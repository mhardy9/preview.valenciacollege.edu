<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/degree-in-dance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Degree in Dance</h2>
                        
                        <h3>Associate in Arts in Dance Performance Overview of the Degree</h3>
                        
                        <p><strong>Limited-Access</strong> <br>
                           This pre-major is designed for the  student who plans to transfer to a Florida public
                           university as a junior to  complete a four-year Bachelor’s degree in Dance. If this
                           pre-major transfers to  a limited-access program, you are responsible for completing
                           the specific  requirements of the institution to which you will transfer as completion
                           of  this pre-major does not guarantee admission to an upper division limited-access
                           program. For specific transfer information, meet with a Valencia advisor or  Academic
                           Dean to review your transfer plans, and check the transfer institution  catalog for
                           specific degree requirements. Students are strongly encouraged to  take electives
                           that relate to their intended baccalaureate degree program.
                        </p>
                        
                        <p><a name="startright" id="startright"></a><strong>Start Right</strong><br>
                           Degree-seeking students enrolling at  Valencia for the first time will have a limited
                           range of courses from which to  choose for their first 18 college-level credits. Within
                           the first 18 college  credit hours, you will be required to take SLS 1122 (3 credits),
                           ENC1101 (3  credits), and a mathematics course appropriate to your selected meta-major
                           (3  credits). The remaining courses should be chosen from the General Education  Core
                           Courses in humanities (3 credits), science (3 credits), or social science  (3 credits),
                           and/or the introductory courses within the A.A. Pre-Majors (or  articulated pre-majors
                           for those tabs). For specific courses see the <em>Foundation  Courses</em> on the “Program Requirements” tab. For course sequencing  recommendations, you can
                           create an education plan by logging into Atlas,  clicking on the LifeMap tab and clicking
                           My Education Plan.
                        </p>
                        
                        <p> <a name="financialaid" id="financialaid"></a><strong>Financial Aid</strong><br>
                           In order to be eligible for and  calculated&nbsp;in your enrollment status for financial
                           aid,&nbsp;courses for  which you are enrolled must count toward your Associate in Arts
                           degree  (36&nbsp;credits of general education and 24 credits of electives). Courses  listed
                           in this transfer plan may not be eligible for and calculated in your  enrollment status
                           financial aid if you took other course work outside of this  transfer plan (examples
                           include prior course work from prerequisites, changing  a major,&nbsp;and transfer course
                           work not included in this plan).
                        </p>
                        
                        <p> <a name="commonprerequisites" id="commonprerequisites"></a><strong>Common Prerequisites</strong><br>
                           These are courses that are required  for the noted major at Florida public universities.
                           For more information on  common prerequisites, check the statewide advising manual
                           at <a href="http://www.floridashines.org" target="_blank">floridashines.org</a>.
                        </p>
                        
                        <p> <a name="mathpathways" id="mathpathways"></a><strong>Math Pathways</strong><br>
                           A Math Pathway is the recommended  math course sequence that leads to completion of
                           the General Education math  requirement that a student would take within his/her Meta-Major.
                           The  Meta-Majors are a collection of eight academic clusters that have related  courses.
                           The intent of selecting a Meta-Major is to help you choose a major and  degree based
                           on your interests, knowledge, skills and abilities. The  recommended math pathway
                           for this transfer plan is the  Arts/Humanities/Communications/Design path; see the
                           program requirements tab  for specific courses. Advisors are available on all campuses
                           to discuss  Meta-Majors and to help you review transfer institution catalogs for specific
                           math requirements.
                        </p>
                        
                        <p> <a name="admissionrequirements" id="admissionrequirements"></a><strong>Admission Requirements</strong></p>
                        
                        <ul type="disc">
                           
                           <li>Submit a completed Valencia College application</li>
                           
                           <li>Submit a completed Valencia College A.A. Pre-Major:       Dance Performance Audition
                              
                           </li>
                           
                        </ul>
                        
                        <p> Application. <strong>PLEASE CLICK </strong><a href="auditions.html"><strong>HERE</strong></a><strong> FOR AUDITION INFORMATION</strong></p>
                        
                        <ul type="disc">
                           
                           <li>Have a successful audition</li>
                           
                           <li>Have degree-seeking student status indicated in your       official Valencia record</li>
                           
                        </ul>
                        
                        <p>Further information about the  admission process is available at <a href="index.html" target="_blank">valenciacollege.edu/dance</a> and from the Arts and Entertainment Division on East Campus.
                        </p>
                        
                        <p> Program expenses are given in the  Financial section of this catalog <a href="http://catalog.valenciacollege.edu/financialinformationfees/estimatedexpenses/aapremajordanceperformance/" target="_blank">http://catalog.valenciacollege.edu/financialinformationfees/estimatedexpenses/aapremajordanceperformance/</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/degree-in-dance.pcf">©</a>
      </div>
   </body>
</html>