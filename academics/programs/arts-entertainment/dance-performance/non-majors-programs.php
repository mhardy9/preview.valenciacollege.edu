<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dance | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/dance-performance/non-majors-programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/dance-performance/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Dance</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/dance-performance/">Dance</a></li>
               <li>Dance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Dance Program - Non-Majors</h2>
                        
                        
                        <p>The dance  program at Valencia College is open to all students. Valencia College's
                           Dance Program provides  quality technical training, performance opportunities and
                           enhancement of the  overall dance experience through individualized attention and
                           development.  Valencia offers a solid dance education for the dancer who aspires to
                           make a  future in the performing arts, as well as for the student who dances for 
                           recreation.
                        </p>
                        
                        <p>A strong  technical emphasis is placed on the traditional dance disciplines that enhance
                           opportunities for students from any dance background. If you are interested in  majoring
                           in Dance, please  review the <a href="auditions.html">audition information for the Dance Degree</a>.
                        </p>
                        
                        <ul>
                           
                           <li><a href="degree.html">List of courses required for the Dance Degree</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/daa/">Full description of all Dance Courses Offered, please see the current College Catalog</a></li>
                           
                        </ul>
                        
                        
                        <h3>Dance Courses Offered</h3>
                        
                        <p>Please note: Not all classes listed are offered every semester. Please check the <a href="http://valenciacollege.edu/schedule/">current class schedule</a> to see which DAA classes are offered that semester. Please see the current <a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/daa/">College Catalog</a> for a full list of all dance courses offered.
                        </p>
                        
                        <h3>DAA: Dance Activities</h3>
                        
                        <h4>Ballet</h4>
                        
                        <ul>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1200C" target="_blank">DAA 1200C</a> BALLET I
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1201C" target="_blank">DAA 1201C</a> BALLET II
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%201204C" target="_blank" title="DAA 1204C">DAA 1204C</a> BALLET I  FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%201205C" target="_blank" title="DAA 1205C">DAA 1205C</a> BALLET  II FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%202206C" target="_blank" title="DAA 2206C">DAA 2206C</a> BALLET  III FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%202207C" target="_blank" title="DAA 2207C">DAA 2207C</a> INTERMEDIATE  BALLET I FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%202219C" target="_blank" title="DAA 2219C">DAA 2219C</a> INTERMEDIATE BALLET II  FOR PRE-MAJORS
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Modern</h4>
                        
                        <ul>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1100C" target="_blank">DAA 1100C</a> MODERN DANCE I
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1101C" target="_blank">DAA 1101C</a> MODERN DANCE II
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%201104C" target="_blank" title="DAA 1104C">DAA 1104C</a> MODERN  DANCE I FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%201105C" target="_blank" title="DAA 1105C">DAA 1105C</a> MODERN  DANCE II FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%201160C" target="_blank" title="DAA 1160C">DAA 1160C</a> MODERN  DANCE III FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%202106C" target="_blank" title="DAA 2106C">DAA 2106C</a> INTERMEDIATE  MODERN DANCE I FOR PRE-MAJORS
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA%202107C" target="_blank" title="DAA 2107C">DAA 2107C</a> INTERMEDIATE MODERN DANCE  II FOR PRE-MAJORS
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Jazz</h4>
                        
                        <ul>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+2500C" target="_blank">DAA 2500C</a> JAZZ DANCE I
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+2501C" target="_blank">DAA 2501C</a> JAZZ DANCE II
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Tap</h4>
                        
                        <ul>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1520C" target="_blank">DAA 1520C</a> TAP I
                           </li>
                           
                           <li>
                              <a href="http://catalog.valenciacollege.edu/search/?P=DAA+1521C" target="_blank">DAA 1521C</a> TAP II<br>
                              
                           </li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/dance-performance/non-majors-programs.pcf">©</a>
      </div>
   </body>
</html>