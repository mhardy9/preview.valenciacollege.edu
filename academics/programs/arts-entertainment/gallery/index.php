<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/gallery/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/gallery/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Gallery</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../arts-and-entertainment/gallery/arrow.gif" width="8">  <img alt="" height="8" src="../../arts-and-entertainment/gallery/arrow.gif" width="8">  
                                 
                              </div>
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td">
                                          
                                          
                                          
                                          
                                          
                                          <center>
                                             
                                             
                                             <center>
                                                
                                                
                                                
                                                
                                                <div data-old-tag="table">
                                                   
                                                   <div data-old-tag="tbody">
                                                      
                                                      <div data-old-tag="tr">
                                                         
                                                         
                                                         <div data-old-tag="td">
                                                            
                                                            
                                                            <div data-old-tag="table">
                                                               
                                                               <div data-old-tag="tbody">
                                                                  
                                                                  <div data-old-tag="tr">
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="td">
                                                                        
                                                                        <div name="navi" id="navi">
                                                                           <img border="0" src="../../arts-and-entertainment/gallery/header-nav.png">
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           <div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                 </div>              
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div> 
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>                  
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                              </div>
                                                                              
                                                                           </div>
                                                                           
                                                                           
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="td">
                                                                        <font>
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           <a name="top" id="top"></a>
                                                                           
                                                                           <h3>Home</h3>       
                                                                           
                                                                           <p>The  Anita S. Wooten Gallery is a 
                                                                              teaching gallery dedicated to enhancing student understanding and 
                                                                              appreciation of art and providing both students and the extended 
                                                                              community exposure to a diverse agenda of visual art forms from 
                                                                              regional artists. <strong>Exhibitions are free and open to the public.</strong>  
                                                                           </p>
                                                                           <br>
                                                                           
                                                                           
                                                                           <h3>Schedule of Exhibitions</h3>
                                                                           
                                                                           
                                                                           <h2>
                                                                              <img alt="Janet Onofrey Silent Spaces" height="240" src="../../arts-and-entertainment/gallery/embrace-the-hope-installation-art.jpg" width="275">Current  Show: Embrace the Hope
                                                                           </h2>
                                                                           
                                                                           <h3>Hye Shin</h3>
                                                                           
                                                                           <h3>August 24, 2017 - October 17, 2017</h3>
                                                                           
                                                                           <p dir="auto">Opening Reception- Thursday September 7, 2017 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           
                                                                           <h2>Upcoming Show: Valencia Alumni Anniversary Exhibition</h2>
                                                                           
                                                                           <h3>November 9, 2017 - December 18, 2017</h3>
                                                                           
                                                                           <p>Opening Reception- Thursday November 9, 2017 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           <p><a href="http://events.valenciacollege.edu/eastcampusgallery">Arts and Events Calendar</a></p>
                                                                           
                                                                           
                                                                           
                                                                           <h3>Gallery Hours</h3>
                                                                           
                                                                           <p><strong>Monday through Friday from 8:30am to 4:30pm</strong> - <strong>The gallery is CLOSED on weekends</strong></p>
                                                                           
                                                                           <p><br>
                                                                              <strong>Summer Session: Closes on Friday at 12:00pm</strong></p>
                                                                           
                                                                           
                                                                           <h3>Gallery Location</h3>
                                                                           
                                                                           <h4>The Anita S. Wooten Gallery</h4>
                                                                           
                                                                           <p> Valencia College / East Campus<br>
                                                                              701 N. Econlockhatchee Trail<br>
                                                                              Orlando, FL 32825
                                                                           </p>
                                                                           
                                                                           <p>Room 3-112</p>
                                                                           
                                                                           <p><a href="http://www.facebook.com/AnitaS.WootenGallery"><strong>www.facebook.com/AnitaS.WootenGallery</strong></a> 
                                                                           </p>
                                                                           
                                                                           
                                                                           <p>For further information please contact Jackie Otto-Miller, Director, 407-582-2298</p>
                                                                           
                                                                           
                                                                           <p><a href="#top">TOP</a></p>
                                                                           
                                                                           
                                                                           <h2>&nbsp;</h2>
                                                                           
                                                                           <h3>Art Shows  2016 / 2017</h3>
                                                                           
                                                                           
                                                                           <h4>Leah Brown: Installations</h4>
                                                                           
                                                                           <p>January 15  , 2016 - March 11, 2016</p>
                                                                           
                                                                           <p>Opening Reception - Friday January 15, 2016 - 6:30pm - 8:30pm </p>
                                                                           
                                                                           <p>Artist Lecture - 7:30pm </p>
                                                                           
                                                                           
                                                                           <h4>Annual Juried Student Exhibition </h4>
                                                                           
                                                                           <p>April 15 , 2016 - May 20, 2016</p>
                                                                           
                                                                           <p>Opening Reception - Friday April 15, 2016 - 6:30pm - 8:30pm </p>
                                                                           
                                                                           <p>Award Ceremony- 7:30pm </p>
                                                                           
                                                                           
                                                                           <h4>Rose Casterline: Paintings </h4>
                                                                           
                                                                           <p>June 10 , 2016 - July 29, 2016</p>
                                                                           
                                                                           <p>Opening Reception - Friday June 10, 2016 - 6:30pm - 8:30pm </p>
                                                                           
                                                                           <p>Artist Lecture - 7:30pm </p>
                                                                           
                                                                           
                                                                           <h4>Carl knickerbocker: Paintings and film</h4>
                                                                           
                                                                           <p>August 18, 2016 - October 13, 2016 </p>
                                                                           
                                                                           <p>Opening Reception - Thursday September 8, 2016 - 6:00pm - 8:00pm </p>
                                                                           
                                                                           <p>Artist Lecture - 7:00pm</p>
                                                                           
                                                                           
                                                                           <h4>SELECTED FINE ART FACULTY EXHIBITION</h4>
                                                                           
                                                                           <p>November 3, 2016 - December 16, 2016</p>
                                                                           
                                                                           <p>Opening Reception- Thursday November 3, 2016 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           <h4>Kevin Haran: Drawings and Sculpture </h4>
                                                                           
                                                                           <p>January 12 , 2017 - March 3, 2017</p>
                                                                           
                                                                           <p>Opening Reception- Thursday January 12, 2017 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           <h4>Annual Juried Student Digital, Graphic, and fine art exhibition </h4>
                                                                           
                                                                           <p>April 14  , 2017 - May 19, 2017</p>
                                                                           
                                                                           <p>Opening Reception- Thursday April 14, 2017 - 6:00pm - 8:00pm</p>
                                                                           
                                                                           
                                                                           <h4>Janet Onofrey </h4>
                                                                           
                                                                           <p>June 8  , 2017 - August 3, 2017</p>
                                                                           
                                                                           <p>Opening Reception- Thursday June 8, 2017 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           <h4>Hye Shin </h4>
                                                                           
                                                                           <p>August 24 , 2017 - October 19, 2017</p>
                                                                           
                                                                           <p>Opening Reception- Thursday September 7, 2017 - 5:00pm - 7:00pm</p>
                                                                           
                                                                           
                                                                           <h4>Valencia Alumni Anniversary Exhibition</h4>
                                                                           
                                                                           <p> November 9 , 2017 - December 18, 2017</p>
                                                                           
                                                                           <p> Opening Reception- Thursday November 9, 2017 - 6:00pm - 8:00pm</p>
                                                                           
                                                                           <p><a href="#top">TOP</a></p>
                                                                           
                                                                           
                                                                           
                                                                           </font>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            <br>
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                             </center>
                                             
                                          </center>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <br>
                                          
                                          <center>
                                             
                                             <div data-old-tag="table">
                                                
                                                <div data-old-tag="tbody">
                                                   
                                                   <div data-old-tag="tr">
                                                      
                                                      <div data-old-tag="td">
                                                         
                                                         
                                                         <div data-old-tag="table">
                                                            
                                                            <div data-old-tag="tbody">
                                                               
                                                               <div data-old-tag="tr">
                                                                  
                                                                  
                                                                  <div data-old-tag="td">
                                                                     <img alt="Valencia College News and Events. Click here to read more." border="0" src="../../arts-and-entertainment/gallery/header-events.png"><br>
                                                                     <br>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     <p>There are no events this week.</p>
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                          </center>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/gallery/index.pcf">©</a>
      </div>
   </body>
</html>