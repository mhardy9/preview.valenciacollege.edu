<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">
Internal Page Title
</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<li><a href="../artstudio/index.php">Art Studio/Fine Art</a></li>
<li><a href="../artstudio/index.php">Home</a></li>
<li><a href="../artstudio/faculty.php">Faculty</a></li>
<li><a href="../artstudio/links.php">Links</a></li>
<li><a href="../artstudio/faq.php">FAQ</a></li>
<li><a href="index.php">Anita S. Wooten<br />East Campus Gallery</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Artstudio/contact.cfm" target="_blank">Contact Us</a></li>
<li><a href="../camps/index.php">Camps</a></li>
<li><a href="../camps/index.php">Home</a></li>
<li><a href="../dance/index.php">Dance</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../dance/index.php">Home</a></li>
<li><a href="../dance/degree.php">Degree In Dance</a></li>
<li><a href="../dance/auditions.php">Auditions</a></li>
<li><a href="../../arts-and-entertainment/dance/vdt.php">VDT - Valencia Dance Theatre</a></li>
<li><a href="../../arts-and-entertainment/dance/courses.php">Non-Majors Program</a></li>
<li><a href="../../arts-and-entertainment/dance/classschedule.php">Class Schedule</a></li>
<li><a href="../../arts-and-entertainment/dance/events.php">Events and Tickets</a></li>
<li><a href="../../arts-and-entertainment/dance/schedule.php">Performance Schedule</a></li>
<li><a href="../../arts-and-entertainment/dance/VSDI.php">VSDI - Summer Dance<br />Institute</a></li>
<li><a href="../../arts-and-entertainment/dance/faculty.php">Faculty</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../dance/faq.php">FAQ</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Dance/contact.cfm" target="_blank">Contact Us</a></li>
<li><a href="../digitalmedia/index.php">Digital Media</a></li>
<li><a href="../digitalmedia/index.php">Home</a></li>
<li><a href="../digitalmedia/faculty.php">Faculty &amp; Staff</a></li>
<li><a href="../digitalmedia/studentwork.php">Student Work</a></li>
<li><a href="../digitalmedia/links.php">Links</a></li>
<li><a href="../digitalmedia/lab.php">Lab</a></li>
<li><a href="../digitalmedia/faq.php">FAQ</a></li>
<li><a href="../digitalmedia/contact.php">Contact</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../entertainmentdesignandtech/index.php">Entertainment Design &amp; Technology</a></li>
<li><a href="../entertainmentdesignandtech/index.php">Home</a></li>
<li><a href="../entertainmentdesignandtech/faculty.php">Faculty &amp; Staff</a></li>
<li><a href="../entertainmentdesignandtech/links.php">Links</a></li>
<li><a href="../entertainmentdesignandtech/faq.php">FAQ</a></li>
<li><a href="../entertainmentdesignandtech/contact.php">Contact</a></li>
<li><a href="../filmtech/index.php">Film Technology</a></li>
<li><a href="../filmtech/index.php">Home</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Filmtech/contact.cfm" target="_blank">Contact</a></li>
<li><a href="../graphicstech/index.php">Graphics Technology</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../graphicstech/index.php">Home</a></li>
<li><a href="../graphicstech/degrees.php">Degrees</a></li>
<li><a href="../graphicstech/lab.php">Lab</a></li>
<li><a href="../graphicstech/faculty.php">Faculty &amp; Staff</a></li>
<li><a href="../graphicstech/studentwork.php">Student Work</a></li>
<li><a href="../graphicstech/faq.php">FAQ</a></li>
<li><a href="../graphicstech/resources.php">Resources</a></li>
<li><a href="../../arts-and-entertainment/Music/index.php">Music</a></li>
<li><a href="../../arts-and-entertainment/Music/index.php">Home</a></li>
<li><a href="../music/faculty.php">Faculty</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../../arts-and-entertainment/Music/VoicesofValencia.php">Voices of Valencia</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/artsandentertainment/Music/contact.cfm" target="_blank">Contact Us</a></li>
<li><a href="../../arts-and-entertainment/Musicandsoundtech/index.php">Music &amp; Sound Technology</a></li>
<li><a href="../../arts-and-entertainment/Musicandsoundtech/index.php">Home</a></li>
<li><a href="../../arts-and-entertainment/Theater/index.php">Theater</a></li>
<li><a href="../../arts-and-entertainment/Theater/index.php">Home</a></li>
<li><a href="../../arts-and-entertainment/Theater/schedule.php">Theatre Schedule</a></li>
<li><a href="../../arts-and-entertainment/Theater/tickets.php">Ticket Information</a></li>
<li><a href="../../arts-and-entertainment/Theater/faculty.php">Faculty</a></li>
<li><a href="../../arts-and-entertainment/Theater/contact.php">Contact</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="index.php">Anita S. Wooten Gallery</a></li>
<li><a href="index.php">Home</a></li>
<li><a href="gallery.php">Online Gallery</a></li>
<li><a href="../../arts-and-entertainment/Performingarts/index.php">Performing Arts Center</a></li>
</ul></div>
</div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
