<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Camps | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/camps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Camps</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Camps</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Camps</h2>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Rock and Roll Camp 2017</h3>
                        
                        <p><strong>GUITAR, DRUMS, VOICE, BASS, KEYS</strong><br> Serving Grades 5-12 (Younger but experienced music students please request information
                           <a href="tel:407-761-7730">407-761-7730</a>. The Original Rock and Roll camp in Orlando, serving young Rockers in Orlando since
                           1999.
                        </p>
                        
                        <p><strong>WEEK 1</strong>: June 5-June 9, 2017, 9 am to 4 pm<br> <strong>WEEK 2</strong>: July 24-July 28, 2017, 9 am to 4 pm
                        </p>
                        
                        <p><strong>Performing Arts Center<br> Valencia College, East Campus<br> 701 N. Econlockhatchee Tr, Orlando, Fl 32825</strong></p>
                        <object width="368" height="200">
                           <param name="movie" value="http://www.youtube.com/v/YioyklJOcOw?fs=1&amp;hl=en_US&amp;color1=0x2b405b&amp;color2=0x6b8ab6">
                           <param name="allowFullScreen" value="true">
                           <param name="allowscriptaccess" value="always">
                           <embed src="http://www.youtube.com/v/YioyklJOcOw?fs=1&amp;hl=en_US&amp;color1=0x2b405b&amp;color2=0x6b8ab6" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="allowfullscreen" width="368" height="200"></object>
                        
                        <p>Rock and Roll Camp is an intensive musical experience for students in middle and high
                           school. Be a part of the entire process of creating, rehearsing and performing a Rock
                           and Roll show. Guitar, Bass, Drums, Voice, Keyboards. Activities include instrumental
                           classes, band rehearsals, clinics, and guest performers. All students perform in the
                           final concert on Friday with lights and sound. Small groups, highly qualified instructors
                           and veteran rock musicians make this camp the original and best Rock and Roll camp
                           in Central Florida, since 1997.<br> <strong>$320 per week.</strong></p>
                        
                        <p><strong>For Rock and Roll Camp, contact Dr Diane Cardarelli, Director, at 407.761.7730.</strong><br> <a href="http://www.rockandrollcamp.net" target="_blank"><strong>Website: <a>www.rockandrollcamp.net</a></strong></a></p>
                        
                        <p>Please click the link below and choose "Save". Print the forms, fill out the form
                           and bring to get registered for Camp!<br> <a class="icon_for_pdf" title="Valencia College Music Camps Registration Form" href="/academics/programs/arts-entertainment/documents/Music-Camps-Registration-Form.pdf">Music Camps Registration Form</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/camps.pcf">©</a>
      </div>
   </body>
</html>