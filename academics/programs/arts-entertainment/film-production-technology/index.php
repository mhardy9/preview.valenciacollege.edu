<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Film Production Technology  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Film Production Technology one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, film production technology   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/film-production-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/film-production-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Film Production Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Film Production Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Film Production Technology</h2>
                        		
                        <p>Valencia's Film Production Technology program has been called "one of the best film
                           schools in the country" by renowned director Steven Spielberg.
                        </p>
                        		
                        <p>As Central Florida continues to grow as a major film production center with two major
                           movie giants - Universal Studios and Disney's MGM Studios- located right here in Orlando,
                           there is a corresponding need for trained technicians with skills in making movies.
                           Valencia's nationally recognized Film Production Technology program is a selective
                           admission, limited enrollment program and trains students for entry-level positions
                           in six major areas of film production. These areas include: gripping, electrical-lighting,
                           editing, sound, camera, and set construction. Students may specialize in Cinematography
                           or Post-Production. All of the classes are hands-on and are taught by professional
                           filmmakers. In fact, Valencia's Film Production program has been called "one of the
                           best film schools in the country" by renowned director Steven Spielberg.
                        </p>
                        		
                        		
                        <hr>
                        		
                        <h4>This program offers specializations in:</h4>
                        						
                        <p>
                           <ul class="list_style_1">
                              							
                              <li>Cinematography</li>
                              							
                              <li>Post-production</li>
                              						
                           </ul>
                        </p>
                        						
                        						
                        <h4>Primary Occupations:</h4>
                        						
                        <ul class="list_style_1">
                           							
                           <li>Camera Assistant</li>
                           							
                           <li>Editor</li>
                           							
                           <li>Lighting Technician</li>
                           							
                           <li>Set Design Assistant</li>
                           							
                           <li>Production Manager</li>
                           							
                           <li>Grip</li>
                           							
                           <li>Sound Technician</li>
                           							
                           <li>Post Production Technician</li>
                           						
                        </ul>
                        						
                        		
                        <hr>
                        		
                        <h3>Contact:</h3>
                        								
                        								
                        <p>Career Program Advisor<br>
                           									<a href="http://www.valenciacollege.edu">Niurka Rivera</a><br>
                           									<a href="tel:407-582-2179">407-582-2179</a></p>
                        								
                        								
                        <p>Dean<br>
                           									<a href="mailto:wgivoglu@valenciacollege.edu">Wendy Givoglu</a><br>
                           									<a href="tel:407-582-2340">407-582-2340</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/film-production-technology/index.pcf">©</a>
      </div>
   </body>
</html>