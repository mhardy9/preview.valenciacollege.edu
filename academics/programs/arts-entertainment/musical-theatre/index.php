<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Musical Theatre | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Musical Theatreone of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, Musical Theatre,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, Musical Theatre, Musical Theatre  ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/musical-theatre/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/musical-theatre/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Musical Theatre</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Musical Theater</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Musical Theatre</h2>
                        
                        <p>We are proud to introduce our newest addition to the Valencia College, School of Arts
                           and Entertainment degree programs. By combining the resources of the Departments of
                           Dance, Music and Theatre, the Associate in Arts in Musical Theatre degree is designed
                           to prepare the student for the demands of this interdisciplinary, performance based
                           degree. Auditions are once a year for admittance into this limited access program
                           and held on Saturday, April 7, 2018 from 8:00 a.m. - 3:00 p.m. Upon acceptance into
                           the program, classes begin on Monday, August 27, 2018 for Fall 2018 semester. For
                           more information, please see below.
                        </p>
                        
                        <p>In collaboration with the Dance, Music and Theatre programs, the musical theater degree
                           offers production opportunities throughout the year, including two musicals, two Opera
                           Theater Workshop performances and a student lead dance concert. Productions are held
                           on the East Campus, in our 500 seat Performing Arts Center and our more intimate Black
                           Box Theater. Along with the musical theatre productions, students will have multiple
                           opportunities to participate in additional theatre, music and dance offerings available
                           at the School of Arts &amp; Entertainment. For a complete listing of arts events, please
                           visit <a href="/academics/programs/arts-entertainment/">www.valenciacollege.edu/arts</a></p>
                        <strong>Admission Requirements</strong>
                        
                        <ul>
                           
                           <li>Submit a completed <a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank">Valencia College application</a></li>
                           
                           <li>Submit a completed Valencia College A.A. Pre-Major: <a href="http://net4.valenciacollege.edu/forms/artsandentertainment/musical-theatre/audition_form.cfm" target="_blank">Musical Theatre Audition form</a></li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/musical-theatre/index.pcf">©</a>
      </div>
   </body>
</html>