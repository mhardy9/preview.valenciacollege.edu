<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Auditions | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/musical-theatre/auditions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/musical-theatre/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Musical Theater</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/musical-theatre/">Musical Theater</a></li>
               <li>Auditions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Auditions</h2>
                        
                        <p>The Associates in Arts (A.A.) in Musical Theatre program will begin in the Fall 2018
                           semester with a cohort of 25 students who have successfully passed the audition. Audition
                           space is limited to 65 applicants who have completed their application by Thursday,
                           March 1, 2018. Acceptance is based on the following:
                        </p>
                        
                        <ul>
                           
                           <li>Have a successful audition</li>
                           
                           <li>Have degree-seeking student status indicated in your official Valencia record</li>
                           
                        </ul>
                        
                        <p>Further information about the admission process is available at <a href="http://valenciacollege.edu/artsandentertainment/musical-theatre/">valenciacollege.edu/musical-theatre</a> and from the School of Arts and Entertainment on East Campus. <strong>Please submit your application to <a href="mailto:musicaltheatre@valenciacollege.edu">musicaltheatre@valenciacollege.edu</a>. All applications are due on Thursday, February 1, 2018.</strong></p>
                        
                        <p><a class="button-download hvr-sweep-to-right" href="http://net4.valenciacollege.edu/forms/artsandentertainment/musical-theatre/audition_form.cfm" target="_blank">Audition Form</a></p>
                        
                        <hr>
                        
                        <h3>Associate in Arts in Musical Theatre Audition</h3>
                        
                        <p><strong>Saturday, April 7, 2018</strong><br> <strong>Valencia College East Campus</strong><br> <strong>Building 3</strong></p>
                        
                        <p><strong>8:00 a.m.</strong> Registration Building 3 Atrium<br> <strong>8:30 a.m.</strong> Group meeting/overview of program and expectations<br> <strong>9:00 a.m.- 11:00 a.m.</strong> Dance Auditions Performing Arts Center<br> <strong>11:00 a.m. - 12:00 p.m.</strong> LUNCH BREAK/Vocal Warm-Ups/Leadership discussion.<br> <strong>12:00 p.m.- 1:30 p.m.</strong> Acting Auditions I in Black Box Theater, Vocal Auditions I Bandroom 3-126 <br> <strong>1:30 p.m. - 1:45 p.m.</strong> BREAK<br> <strong>1:45 p.m. - 3:15 p.m.</strong> Acting Auditions II in Black Box Theater, Vocal Auditions II Bandroom 3-126<br> <strong>3:15 - 5:30 p.m.</strong> Additional time if needed
                        </p>
                        
                        <p><strong>Dance Auditions</strong> shall consist of students learning a ballet combination and later a jazz combination.
                           Students will be grouped and then perform. After the ballet portion, students have
                           the option of keeping their shoes on to use for the jazz portion of the audition.
                        </p>
                        
                        <p><strong>Vocal Auditions should consist of:</strong></p>
                        
                        <ul>
                           
                           <li>Up tempo selection, 16 bars</li>
                           
                           <li>Ballad selection, 16 bars</li>
                           
                           <li>Bring a copy of the music, marked</li>
                           
                        </ul>
                        
                        <p><strong>Acting Auditions should consist of:</strong></p>
                        
                        <ul>
                           
                           <li>Please be prepared to slate with your name and current educational institution and
                              give a simple end slate. (Slates will not be counted into your time limit)
                           </li>
                           
                           <li>Auditionee shall fully prepare (2) 1.5 minute monologues, one comedic, and one dramatic.
                              These can come from either Musical or Non-Musical Theater of a modern or contemporary
                              nature (no classical monologues). Monologues shall be fully memorized and presented
                              as a full performance, with the transition between both included in the three minute
                              time slot.
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/musical-theatre/auditions.pcf">©</a>
      </div>
   </body>
</html>