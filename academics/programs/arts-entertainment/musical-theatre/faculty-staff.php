<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Faculty/Staff | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/musical-theatre/faculty-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/musical-theatre/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Musical Theatre</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/musical-theatre/">Musical Theater</a></li>
               <li>Faculty/Staff</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Musical Theatre Faculty </h2>
                        				
                        <p><strong><a href="mailto:agerber@valenciacollege.edu">Alan Gerber</a></strong> <em>Program Chair Professor</em></p>
                        	
                        
                        
                        <h3>Theatre </h3>	
                        				
                        <p><strong><a href="mailto:jdidonna@valenciacollege.edu">John DiDonna</a></strong> <em>Artistic Director, Professor</em>	<br>
                           					<strong><a href="mailto:gkopf@valenciacollege.edu">Ginny Kopf</a></strong> <em>Professor</em><br>
                           					<strong><a href="mailto:ecraft2@valenciacollege.edu">Eric Craft</a> </strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:rlane13@valenciacollege.edu">Rebekah Lane</a></strong> <em>Adjunct Professor	</em><br>
                           					<strong><a href="mailto:klindseymoulds@valenciacollege.edu">Kathleen Lindsey-Moulds</a>	</strong><em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:rolson11@valenciacollege.edu">Robin Olson</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:drupe2@valenciacollege.edu">Donald Rupe</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:atiwari1@valenciacollege.edu">Aradhana Tiwari</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:twilliams219@valenciacollege.edu">Timothy Williams</a></strong> <em>Adjunct Professor</em></p>
                        
                        							
                        
                        <p>&nbsp;</p>
                        
                        <p></p>
                        
                        <h3>Entertainment Design and Technology/Performing Arts Center Staff </h3>
                        				
                        <p><strong><a href="mailto:kabel@valenciacollege.edu">Kristin Abel</a></strong> <em>Program Chair, Professor</em>	<br>
                           					<strong><a href="mailto:spasqual@valenciacollege.edu">Sonia Pasqual</a></strong> <em>Professor</em><br>
                           					<strong><a href="mailto:drauschkolb@valenciacollege.edu">David 'Ross' Rauschkolb</a>	</strong> <em>Technical Director, Professor</em><br>
                           					<strong><a href="mailto:alapietra@valenciacollege.edu">Ann LaPietra</a></strong> <em>Performing Arts Center Manager, Professor</em><br>
                           					<strong><a href="mailto:gloftus@valenciacollege.edu">Greg Loftus</a></strong> <em>Professor </em>	<br>
                           					<strong><a href="mailto:hsladick@valenciacollege.edu">Heather Sladick</a></strong> <em>Performing Arts Center Technical Supervisor, Professor</em><br>
                           					<strong><a href="mailto:jchoate2@valenciacollege.edu">Jehad Choate</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:tdebaun@valenciacollege.edu">Tim Debaun</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:clee63@valenciacollege.edu">Constance Lee</a></strong> <em>Adjunct Professor, Entertainment Design and Technology</em><br>
                           					<strong><a href="mailto:clapietra@valenciacollege.edu">Carmine LaPietra</a></strong> <em>Adjunct Professor, Entertainment Design and Technology</em>	<br>
                           					<strong><a href="mailto:mshugg@valenciacollege.edu">Michael Shugg</a></strong> <em>Adjunct Professor, Entertainment Design and Technology</em><br>
                           					<strong><a href="mailto:jwhiteley@valenciacollege.edu">Jonathan Whiteley</a></strong> <em>Performing Arts Center Staff, Lighting Designer, Professor</em>		
                        </p>
                        
                        				
                        
                        <h3>Music Faculty</h3>
                        				
                        <p><strong><a href="mailto:tgifford@valenciacollege.edu">Dr. Troy Gifford</a></strong> <em>Program Chair, Professor</em><br>
                           				<strong><a href="mailto:agerber@valenciacollege.edu">Alan Gerber</a></strong>,  <em>Professor</em><br>
                           				<strong><a href="mailto:cdelvillageio@valenciacollege.edu">Carla Delvillageio</a></strong>, <em>Adjunct Professor</em> <br>
                           				<strong>Anita Endsley</strong>, <em>Adjunct Professor</em><br>
                           				<strong><a href="mailto:kcleto@valenciacollege.edu">Kit Cleto</a></strong>, <em>Adjunct Professor</em><br>
                           				<strong><a href="mailto:aeschbach@valenciacollege.edu">Anna Eschbach</a></strong>, <em>Adjunct Professor</em><br>
                           				<strong><a href="mailto:spurser@valenciacollege.edu">Sarah Purser</a></strong>, <em>Adjunct Professor</em><br>
                           				<strong><a href="mailto:jmartinez186@valenciacollege.edu">Juan Tomas Martinez</a></strong>, <em>Adjunct Professor</em><br>
                           				<strong><a href="mailto:fbrill@valenciacollege.edu">Fang Brill</a></strong> <em>Adjunct Professor</em></p>
                        		
                        				
                        
                        <h3>Dance Faculty</h3>
                        				
                        <p><strong><a href="mailto:ssalapa@valenciacollege.edu">Dr. Suzanne R. Salapa</a></strong> <em>Program Chair, Professor</em><br>
                           					<strong><a href="mailto:lbrasseux@valenciacollege.edu">Leslie Brasseux Rodgers</a></strong> <em>Artistic Director, Valencia Dance/Valencia Dance Theater, Professor</em><br>
                           					<strong><a href="mailto:kjudy1@valenciacollege.edu">Katrina Judy</a></strong> <em>Adjunct Professor</em><br>
                           					<strong><a href="mailto:kstevens9@valenciacollege.edu">Kristina Stevens</a></strong> <em>Dance Performance Production Coordinator</em></p>
                        
                        
                        				
                        		
                        
                        	
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/musical-theatre/faculty-staff.pcf">©</a>
      </div>
   </body>
</html>