<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Theater  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Theater one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Theater   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/theater/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/theater/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Theater</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Theater</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Theater</h2>
                        			
                        <p>Valencia College Theater produces four major productions each year, as well as a series
                           of student-directed one-act plays. Productions are held either in our 558-seat Performing
                           Arts Center, or our more intimate Black Box Theater, both located on the East Campus.
                           
                        </p>
                        						
                        <p>The season runs from October through July each school year, with variety and educational
                           opportunities for both students and audiences driving the selection of plays. Recent
                           productions have included major musicals, avant-garde plays, classical drama and comedy,
                           contemporary Broadway hits, and favorite revivals. Acting and directing students also
                           produce two sets of student-directed one-act plays that are offered on a designated
                           weekend in April. (The Student-Directed One Act Festival is held at the Lowndes Shakespeare
                           Center, Loch Haven Park, Orlando and is free). 
                        </p>
                        		
                        		
                        <hr>
                        	
                        <h3>Contact Us</h3>
                        
                        <p><strong>John DiDonna</strong><br>
                           	<strong>Program Chair/Artistic Director</strong><br>
                           <strong>Theater:</strong> <a href="tel:407-582-2073">407-582-2073</a> <br>
                           	<a href="mailto:jdidonna@valenciacollege.edu">jdidonna@valenciacollege.edu</a><br>
                           
                        </p>
                        	
                        <hr>
                        	
                        <h3>Ticket Information</h3>
                        						
                        <p>Tickets - please visit <a href="http://valenciacollege.edu/arts" target="_blank">valenciacollege.edu/arts</a> for further information about ticket prices, discounts, and purchase tickets for
                           theater events.<br>
                           								<strong>Box Office:</strong> <a href="tel:407-582-2900">407-582-2900</a><br>
                           								<a href="mailto:boxoffice@valenciacollege.edu">boxoffice@valenciacollege.edu</a></p>
                        
                        		
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/theater/index.pcf">©</a>
      </div>
   </body>
</html>