<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Theater one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Theater   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/theater/faculty-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/theater/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Theater</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/theater/">Theater</a></li>
               <li>Faculty </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Faculty</h2>
                        		
                        <p>Full-time and Adjunct Theater Faculty</p>
                        		
                        <div>
                           			
                           <p></p>
                           						
                           <div class="row">
                              							
                              <div class="col-md-6 col-sm-6">
                                 								
                                 <ul class="list_teachers">
                                    									<strong>Full-time Faculty</strong>
                                    									
                                    <li><strong>John DiDonna</strong>
                                       										
                                       <p> Program Chair/Artistic Director, Theater </p>
                                    </li>
                                    									
                                    <li><strong>Kristin Abel </strong>
                                       										
                                       <p>Theater Technology</p>
                                    </li>
                                    									
                                    <li><strong>Ginny Kopf</strong>
                                       										
                                       <p> (Voice for the Actor, Movement, Stage Dialects, Acting)</p>
                                    </li>
                                    									
                                    <li><strong>Ann LaPietra</strong>
                                       										
                                       <p> Audio/Visual (PAC Manager)  </p>
                                    </li>
                                    									
                                    <li><strong>Greg Loftus</strong>
                                       										
                                       <p> Theater Technology</p>
                                    </li>
                                    									
                                    <li><strong>Sonia Pasqual </strong>
                                       										
                                       <p>Theater Technology </p>
                                    </li>
                                    									
                                    <li><strong>David 'Ross' Rauschkolb</strong>
                                       										
                                       <p> Technical Director, Theater</p>
                                    </li>
                                    									
                                    <li><strong>Heather Sladick</strong>
                                       										
                                       <p> Theater Technology (PAC Tech Spvr) </p>
                                    </li>
                                    									
                                    								
                                 </ul>
                                 							
                              </div>
                              							
                              <div class="col-md-6 col-sm-6">
                                 								
                                 <ul class="list_teachers">
                                    									<strong>Adjunct Faculty</strong>
                                    									
                                    <li><strong>Jehad Choate</strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Eric Craft </strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Timothy DeBaun</strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Rebekah Lane </strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Kathleen Lindsey-Moulds</strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Virginia McKinney</strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Donald Rupe </strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Michael Shugg </strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Aradhana Tiwari </strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Jonathan Whiteley</strong><p></p>
                                    </li>
                                    									
                                    <li><strong>Timothy Williams </strong><p></p>
                                    </li>
                                    								
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/theater/faculty-staff.pcf">©</a>
      </div>
   </body>
</html>