<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Digital Media Technology  | Valencia College</title>
      <meta name="Description" content="Valencia's Valencia's Digital Media Technology one of the best film schools in the country">
      <meta name="Keywords" content="arts and entertainment, associates in arts degree, associate in science,  certificate music, art studio/fine art, dance performance, music performance,  sound and music technology,  digital media technology, entertainment design technology,  as degrees in arts, theater, Digital Media Technology   ">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/digital-media-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/digital-media-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Digital Media Technology</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Digital Media</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Digital Media Technology</h2>
                        		
                        <p>The<strong> Digital Media Technology</strong> Associate in Science (A.S.) degree at Valencia College is a two-year program that
                           prepares you to go directly into a specialized career in digital media.
                        </p>
                        
                        <p>This program will train you in one of the hottest emerging career fields today—working
                           in video production, motion graphics, mobile journalism or web development. You’ll
                           learn to create persuasive, educational, informational and entertainment-based video,
                           audio and motion graphic content for use in multimedia, web, mobile devices, broadcast
                           and live events. And, with plenty of on-campus resources, including green screens,
                           specialized computer labs and industry-standard equipment, you can polish your skills
                           and gain the hands-on experience needed to land a job when you graduate.
                        </p>
                        		
                        		
                        <hr>
                        		
                        <h4>This program offers specializations in:</h4>
                        						
                        <p>
                           <ul>
                              							
                              <li>Live Event Video Production</li>
                              							
                              <li>Mobile Journalism</li>
                              							
                              <li>Video and Motion Graphics</li>
                              							
                              <li>Web Development</li>
                              						
                           </ul>
                        </p>
                        						
                        						
                        <h4>Primary Occupations:</h4>
                        						
                        <ul>
                           						
                           <li>Interactive Media Technician</li>
                           						
                           <li>Web Production Technician</li>
                           						
                           <li>Motion Graphics Designer</li>
                           						
                           <li>Web Developer</li>
                           						
                           <li>Digital Video Editor</li>
                           						
                           <li>Digital Videographer</li>
                           						
                           <li>Digital Audio Technician</li>
                           						
                           <li>Post-Production Specialist</li>
                           						
                        </ul>
                        						
                        		
                        <hr>
                        		
                        <h3>Contact:</h3>
                        								
                        								
                        <p> Program Chair/Professor Digital Media Technology<br>
                           									<strong>Robert McCaffrey</strong><br>
                           									<i class="far fa-envelope"></i>  <a href="mailto:rmccaffrey@valenciacollege.edu">rmccaffrey@valenciacollege.edu</a><br>
                           									<i class="far fa-phone"></i>  <a href="tel:407-582-2784">407-582-2784</a><br>
                           									<i class="far fa-map"></i>  East Campus Building 1A, Room 1-147
                        </p>
                        								
                        								
                        <p>Senior Lab Instructor<br>
                           									<strong>Kyle Snavely</strong><br>
                           										<i class="far fa-envelope"></i> <a href="mailto:dmt_lab@valenciacollege.edu">dmt_lab@valenciacollege.edu</a><br>
                           										<i class="far fa-phone"></i> <a href="tel:407-582-2570">407-582-2570</a><br>
                           										<i class="far fa-phone"></i> East Campus Building 1A, Room 1-152A
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/digital-media-technology/index.pcf">©</a>
      </div>
   </body>
</html>