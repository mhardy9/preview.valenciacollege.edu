<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Entertainment  | Valencia College</title>
      <meta name="Description" content="Nursing Program Tracks | Health Sciences | Valencia College">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Arts and Entertainment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Arts Entertainment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30">
                  		
                  <div class="main-title">
                     
                     <h2>Arts and Entertainment</h2>
                     
                  </div>
                  
                  <div class="row box_style_1">
                     	
                     <p>Consider yourself a tech freak? Do you feel a creative connection to the digital world?
                        Are you fascinated with graphic arts, music, movies, theater or film? If so, an arts
                        &amp; entertainment degree can help you turn what you love into what you do for a living.
                        From onstage to behind the scenes, recording studios to multimedia businesses, performers,
                        artists and technicians are finding satisfying careers in the central Florida entertainment
                        scene.
                     </p>
                     	
                     <p>Employers in the arts &amp; entertainment industry are looking for people who are creative
                        and entrepreneurial, enjoy collaboration in team environments, keep up with the hottest
                        trends in technology, good at multitasking, and keep cool in high stress situations.
                     </p>
                     
                     
                  </div>
                  
                  
                  <div class="row">
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/art-studio-fine-art/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/art-studio.png" alt="Art Studio/Fine Art">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Art Studio/Fine Art
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Art Studio/Fine Art<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>The Visual Arts Club is a group dedicated to fun artistic activities while helping
                                    to build skills and provide useful resources and knowledge to become successful in
                                    the art industry.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/art-studio-fine-art/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/dance-performance/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/dance_program.png" alt="Dance Performance">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Dance Performance
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Dance Performance<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p> The Valencia College Dance Program goal is to provide quality technical training
                                    and the enhancement of the overall dance experience.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/dance-performance/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     		
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/digital-media-technology/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/digital-media.png" alt="Digital Media Technology">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Digital Media Technology
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Digital Media Technology<br>
                                    Associate in Science Degree
                                    
                                 </h3>
                                 
                                 <p>This program will train you in one of the hottest emerging career fields today—working
                                    in video production, motion graphics, mobile journalism or web development. You’ll
                                    learn to create persuasive, educational, informational and entertainment-based video,
                                    audio and motion graphic content for use in multimedia, web, mobile devices, broadcast
                                    and live events.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/digital-media-technology/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     	
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/film-production-technology/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/film.png" alt="Film Production Technology">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Film Production Technology
                                          
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Film Production Technology<br>
                                    Associate in Science Degree
                                    
                                 </h3>
                                 
                                 <p>The Film Production Technology Associate in Science (A.S.) degree at Valencia College
                                    is a two-year program that prepares you to go directly into a specialized career in
                                    the film industry. Nationally recognized for making students employable, the program
                                    provides training for entry-level positions in six major areas of film production:
                                    gripping, electrical-lighting, editing, sound, camera and set construction.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/film-production-technology/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     	
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/graphic-interactive-design/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/graphics-technology.png" alt="Graphics Technology">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Graphics Technology
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Graphics Technology<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                                    educating some of the most talented designers in central Florida. Our students have
                                    received....
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/graphic-interactive-design/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/music-performance/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/music.png" alt="Music Performance">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Music Performance
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Music Performance<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>Since 1975, the music program has been dedicated to the idea of inspiring future performers
                                    by providing students with musically trained faculty and....
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/music-performance/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/musical-theatre/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/musical-theatre.png" alt="Musical Theatre">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Musical Theatre
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Musical Theatre<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>By combining the resources of the Departments of Dance, Music and Theatre, the Associate
                                    in Arts in Musical Theatre degree is designed to prepare the student for the demands
                                    of this interdisciplinary, performance based degree....
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/musical-theatre/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/sound-music-technology/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/music-technology.png" alt="Sound and Music Technology">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Sound and Music Technology
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Sound and Music Technology<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>Valencia's Sound and Music Technology program takes a unique approach to preparing
                                    you for an exciting career in the music and sound industry.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/sound-music-technology/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <div class="img_list">
                                 <a href="/academics/programs/arts-entertainment/theater/index.php"> <img src="/_resources/images/academics/programs/arts-entertainment/theater.png" alt="Theatre/Dramatic Arts">
                                    
                                    <div class="short_info">
                                       
                                       <h3>
                                          Theatre/Dramatic Arts
                                       </h3>
                                       
                                    </div>
                                    </a>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="clearfix visible-xs-block">
                              
                           </div>
                           
                           <div class="col-lg-6 col-md-6 col-sm-6">
                              
                              <div class="course_list_desc">
                                 
                                 <h3>Theatre/Dramatic Arts<br>
                                    Associate in Arts Degree
                                    
                                 </h3>
                                 
                                 <p>Valencia College Theater produces four major productions each year, as well as a series
                                    of student-directed one-act plays. Productions are held either in our 558-seat....
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-lg-2 col-md-2 col-sm-2">
                              
                              <div class="details_list_col">
                                 
                                 <div>
                                    <a href="/academics/programs/arts-entertainment/theater/index.php" class="button-outline">Details</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     	
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/index.pcf">©</a>
      </div>
   </body>
</html>