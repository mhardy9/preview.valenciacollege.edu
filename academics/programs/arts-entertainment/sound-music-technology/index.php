<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Sound and Music Technology | Valencia College</title>
      <meta name="Description" content="Sound and Music Technology"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/sound-music-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Sound and Music Technology</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Sound and Music Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <h2>Sound and Music Technology</h2>
                        	
                        <p><strong>From theatre to soundstage, studio and post-production</strong></p>
                        	
                        					
                        	
                        <p>Valencia's Music and Sound Technology program takes a unique approach to preparing
                           you for an exciting career in the music and sound industry. You will have the opportunity
                           to study studio and live sound techniques, post-production and MIDI, as well as, musicianship
                           and performance.
                        </p>
                        
                        
                        <p>This will ensure that you develop the technical skills and aesthetic judgement to
                           handle both the artistic and technical demands of music and sound production. This
                           practical approach gives you real world experience in a wide range of settings. From
                           theatre to soundstage, studio and post-production, and with Valencia's own completely
                           student-run record company, "FlatFoot Music:, you will have many opportunities to
                           develop, practice, and refine your craft on your way to a rewarding high wage career.
                        </p>
                        				
                        					
                        <hr>
                        					
                        	
                        <h3>This program offers specializations in:</h3>
                        	
                        <p>
                           <ul class="list_style_1">
                              		
                              <li>Sound Technology</li>
                              		
                              <li>Music Performance and Sound</li>
                              		
                           </ul>
                        </p>
                        
                        	
                        <h4>Primary Occupations:</h4>
                        	
                        <ul class="list_style_1">
                           		
                           <li>Music Producer/ Director</li>
                           		
                           <li>Sound Mixer/ Designer</li>
                           		
                           <li>Post-production Mixer/Editor</li>
                           		
                           <li>Assistant Audio Engineer</li>
                           		
                           <li>Sound Recordist</li>
                           		
                           <li>Sound Effects/ Dialog Editor</li>
                           		
                           <li>Surround Sound Mixer/Author</li>
                           		
                           <li>Sound Designer</li>
                           	
                        </ul>
                        
                        	
                        <hr>
                        
                        	
                        <h3>Contact:</h3>
                        
                        	
                        <p>Program Director<br>
                           		<a href="mailto:rvalery@valenciacollege.edu">Raul Valery </a><br>
                           		<a href="tel:407-582-2882">407-582-2882</a></p>
                        
                        	
                        <p>Career Program Advisor <br>
                           		<a href="mailto:kbell@valenciacollege.edu">Kristol Bell</a><br>
                           		<a href="tel:407-582-2097">407-582-2097</a></p>
                        
                        	
                        <p>Dean, Arts &amp; Entertainment<br>
                           		<a href="mailto:wgivoglu@valenciacollege.edu">Wendy Givoglu</a><br>
                           		<a href="tel:407-582-2340">407-582-2340</a></p>
                        	
                     </div>
                     	
                  </div>
                  		
               </div>
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/sound-music-technology/index.pcf">©</a>
      </div>
   </body>
</html>