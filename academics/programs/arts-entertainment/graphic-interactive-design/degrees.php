<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/graphic-interactive-design/degrees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/graphic-interactive-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Graphic and Interactive Design</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/graphic-interactive-design/">Graphic and Interactive Design</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Degrees We Offer</h2>
                        
                        <p><strong>AS Degree: Graphic Design</strong><br>
                           The Graphic Design Specialization prepares students to become graphic designers, publication
                           designers, layout artists, advertising designers, user interface designers, user experience
                           designers, production artists or creative technicians. It provides instruction in
                           courses directly related to developing job skills for entry-level positions in advertising
                           agencies, design studios and art departments for retail and other businesses. This
                           specialization is best suited for people who are artistic, creative and enjoy traditional
                           as well as computer design.<br>
                           <br>
                           <strong>AS Degree: Interactive Design</strong><br>
                           The Interactive Design specialization prepares students to become entry-level web
                           designers, user interface designers, user experience designers, multimedia artists
                           or interactive designers. If you enjoy creating graphics, developing interactive media
                           and/or designing web pages, you will enjoy this specialization. <br>
                           	<br></p>
                        	
                        <hr>
                        
                        <h3>Certificates We Offer:</h3>
                        
                        <p><strong>Graphic Design Production</strong><br>
                           This program is designed to provide students with the skills required to produce layouts,
                           imagery and graphic elements  for print, advertising design and web design. The certificate
                           focuses on the application of good design principles and the utilization of industry-standard
                           production techniques as well as software and hardware at an intermediate  level.
                           <br>
                           <br>
                           <strong>Graphic Design Support</strong><br>
                           This program is designed to provide students with the skills required to assist with
                           the production of&nbsp; layouts and graphics for design or presentation projects for print.
                           The certificate focuses on the application of good design principles and the utilization
                           of industry-standard production techniques as well as software and hardware at a basic
                           level. <br>
                           <br>
                           <strong>Interactive Design Production</strong><br>
                           This program is designed to provide students with the skills required to produce images,
                           design functional web layouts and interactive media for the internet or other user
                           interfaces. The certificate focuses on the application of appropriate production techniques,
                           web coding languages and the use of industry-standard software and hardware at an
                           intermediate level. <br>
                           <br>
                           <strong>Interactive Design Support</strong><br>
                           	This program is designed to provide students with the skills required to assist with
                           the production of graphic elements,  basic web design layouts and  interactive media
                           for the internet or other user interfaces. The certificate focuses on the application
                           of appropriate production techniques and the use of industry-standard software and
                           hardware at basic  level.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/graphic-interactive-design/degrees.pcf">©</a>
      </div>
   </body>
</html>