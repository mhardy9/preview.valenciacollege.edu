<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graphic and Interactive Design | Valencia College</title>
      <meta name="Description" content="Graphic and Interactive Design">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/graphic-interactive-design/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/graphic-interactive-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Graphic and Interactive Design</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Graphic and Interactive Design</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        
                        <h2>Graphic and Interactive Design</h2>
                        
                        
                        <p><strong>From theatre to soundstage, studio and post-production</strong></p>
                        
                        			
                        
                        <p>The<strong> Graphic and Interactive Design </strong>Associate in Science (A.S.) degree program is a two-year program that prepares you
                           to go directly into a specialized career in print design, advertising design, web
                           design or interactive design.
                        </p>
                        
                        <p>As a graphics student, you’ll learn what it means to kern type, crop photos and “lose
                           the widows.” From the creative process to interpersonal communication, our students
                           receive hands-on experience and learn employable skills that prepare them to enter
                           directly into a career. With a trusted reputation in the Orlando creative industry,
                           we pride ourselves in educating some of the most talented designers in Central Florida.
                           Our students have received recognition in local, regional, national and even international
                           competitions. Their accomplishments include over 100 ADDY Awards, Florida Print Awards,
                           the Create Awards, the ONE Show, Print Magazine and Siggraph.
                        </p>	
                        
                        
                        <hr>          
                        
                        <h3>Recharging Creative Minds</h3>
                        
                        <p>As a graphics major you'll know what it means to kern type, crop photos, and "lose
                           the widows." You'll become intrigued by art direction, and know a stage is not just
                           for acting it's also for animating. And you'll understand that speaking new languages
                           prepares you for your journey to designing for the World Wide Web. Valencia's A.S.
                           degree program in Graphics Technology prepares graduates for careers in print and
                           advertising design or web and interactive design, giving them the skills they need
                           and insight from real-world designers. Graduates have landed jobs in design studios,
                           advertising agencies, printing companies, web design firms, interactive design companies
                           and other highly creative environments.
                        </p>
                        
                        <p>Orlando boasts a wide-reaching and wildly diverse graphics arts community. Designers
                           here are involved in advertising, band poster design, web design, animation, interface
                           design and everything in between.
                        </p>
                        
                        <h4>Starting With Creative Excellence</h4>
                        
                        <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                           educating some of the most talented designers in central Florida. Our students have
                           received recognition in local, regional, national, and even international competitions.
                           Their accomplishments include over 100 ADDY Awards, Florida Print Awards, the Create
                           Awards, the ONE Show, Print Magazine, and Siggraph. And perhaps part of the reason
                           why many local employers are impressed with hiring our graduates is also because we
                           understand what the industry expects from graduates. Real world experience combined
                           with reinforcing various employable skills ranging from the creative process to communication
                           all make the graphics technology program a great start for anyone seeking to start
                           right in a creative career.
                        </p>    
                        
                        
                        <hr>
                        					
                        	
                        
                        <h3>This program offers specializations in:</h3>
                        	
                        
                        <p>
                           
                           <ul class="list_style_1">
                              		
                              
                              <li>Graphic Design</li>
                              
                              <li>Interactive Design</li>
                              		
                              
                           </ul>
                           
                        </p>
                        
                        	
                        
                        <h4>Primary Occupations:</h4>
                        	
                        
                        <ul class="list_style_1">
                           		
                           
                           <li>Graphic Designer/Artist</li>
                           	
                           <li>Multimedia Designer/Artist</li>
                           	
                           <li>Interactive Media Specialist</li>
                           	
                           <li>Illustrator</li>
                           	
                           <li>Web Designer</li>
                           	
                           <li>Production Artist</li>
                           	
                           
                        </ul>
                        
                        	
                        
                        <hr>
                        
                        	
                        
                        <h3>Contact:</h3>
                        
                        <p>To find out more information contact<br>              
                           	
                           <strong>Program Director</strong><br>
                           <strong>Kristy Pennino</strong><br>
                           <a href="mailto:kpennino@valenciacollege.edu">kpennino@valenciacollege.edu</a><br>
                           	<a href="tel:407.582.2864">407.582.2864</a></p>
                        
                        	
                        
                        <p>Career Program Advisor <br>
                           	<strong>Niurka Rivera</strong><br>
                           		<a href="mailto:ereynoso@valenciacollege.edu">nrivera4@valenciacollege.edu</a><br>
                           		<a href="tel:407-582-2179">407-582-2179</a></p>
                        
                        	
                        
                        <p>Dean, Arts &amp; Entertainment<br>
                           		<a href="mailto:wgivoglu@valenciacollege.edu">Wendy Givoglu</a><br>
                           		<a href="tel:407-582-2340">407-582-2340</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/graphic-interactive-design/index.pcf">©</a>
      </div>
   </body>
</html>