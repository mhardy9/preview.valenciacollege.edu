<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/graphic-interactive-design/studentwork.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/graphic-interactive-design/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/graphic-interactive-design/">Graphic and Interactive Design</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="../../arts-and-entertainment/graphics-technology/arrow.gif" width="8">  <img alt="" height="8" src="../../arts-and-entertainment/graphics-technology/arrow.gif" width="8"> 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                <img alt="Valencia College - Arts and Entertainment" border="0" src="../../arts-and-entertainment/graphics-technology/departmentsubbanner.png"><span><span><img alt="" src="../../arts-and-entertainment/graphics-technology/header.gif">
                                                      
                                                      </span></span>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          
                                          
                                          
                                          
                                          <center>
                                             
                                             
                                             <center>
                                                
                                                
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <img alt="" height="29" src="../../arts-and-entertainment/graphics-technology/ripped-paper-top.png.html" width="881">
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         <div>
                                                            
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     
                                                                     
                                                                     <div>
                                                                        
                                                                        <div name="navi" id="navi">
                                                                           <img border="0" src="../../arts-and-entertainment/graphics-technology/header-nav.png">
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           <div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                 </div>              
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div> 
                                                                                 
                                                                                 
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                 </div>                  
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                                 
                                                                              </div>
                                                                              
                                                                           </div>
                                                                           
                                                                           
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     <div>
                                                                        
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        <font>
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           <a name="top" id="top"></a>
                                                                           
                                                                           <h2>Student Work</h2> 
                                                                           
                                                                           <p>To review work completed by our students you can check out our showcase of student
                                                                              work in our <a href="http://www.flickr.com/photos/vccgraphics/" target="new_frame">vccgraphics flickr account</a>. If you are a Valencia student and would like to submit your work for consideration
                                                                              in the flickr showcase you should follow our set submission guidelines.
                                                                           </p>
                                                                           
                                                                           <div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>Submission Guidelines</div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                           </div>  
                                                                           
                                                                           <p>As you guys know most adjuncts stay very busy with their fulltime jobs. Some have
                                                                              moved on and are no longer teaching, others may not have kept all student work. So
                                                                              we now turn to you all to keep adding to the flickr showcase. But this doesnï¿½t mean
                                                                              stop and send us every single piece of work youï¿½ve done in the graphics program,
                                                                              only send us your best work. Based on feedback you were provided in your class as
                                                                              well as project grades you should have a good idea if your work is near portfolio
                                                                              quality. Here are a few guidelines for you to consider before sending your work.
                                                                           </p>
                                                                           
                                                                           <div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    
                                                                                    <div>Print Design</div>
                                                                                    
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                           </div>  
                                                                           
                                                                           <p><strong>Send only work that is near or at portfolio quality.</strong><br>
                                                                              Much like your own portfolios, we want to showcase the best work. If you arenï¿½t
                                                                              sure it doesnï¿½t hurt to send us your work. But if you know it still needs a lot
                                                                              of work please take the time to refine your project before sending it to us.
                                                                           </p>
                                                                           
                                                                           <p><strong>Send us the details.</strong><br>
                                                                              Because we weren't in class with you we really don't have any information but what
                                                                              you provide us, so please provide the following when you send your files: Title of
                                                                              piece, your name, your instructor's name, semester you completed the project, course
                                                                              name, any techniques used (if applicable: i.e. gradient mesh, etc.)
                                                                           </p>
                                                                           
                                                                           <p><strong>Only send us jpg files.</strong><br>
                                                                              To help us speed up the process of displaying your work please crop your work and
                                                                              send it in jpg format. Please keep your file over 1000px by 1000px but less than 2000px
                                                                              by 2000px. This will allow for a larger resolution version to be downloadable online.
                                                                              Do NOT send any files over 1MB, reducing your image size and saving for the web will
                                                                              keep your file size to a minimum.
                                                                           </p>
                                                                           
                                                                           <p><strong>Send a photo of the piece only if it compliments the project.</strong><br>
                                                                              Some project such as products, brochures, and self promotional pieces are most effective
                                                                              if they are photographed. There is a light booth on east campus that works well for
                                                                              photoshoots.
                                                                           </p>
                                                                           
                                                                           <p><strong>Web/Interactive Design</strong><br>
                                                                              I am currently working on adding more web and interactive work in the upcoming weeks,
                                                                              unfortunately it takes a bit more work because we will be linking to the actual interactive
                                                                              project which we will host on our program web site. Here are a few guidelines for
                                                                              interactive projects:<br>
                                                                              <br>
                                                                              <strong>Any Flash over 100k must have a preloader.</strong><br>
                                                                              If you did not incorporate a preloader for a project, before it is posted it must
                                                                              have a preloader.<br>
                                                                              <br>
                                                                              <strong>Incorporate your flash file into an html page.</strong><br>
                                                                              This will speed up posting. You can do this through Flash or Dreamweaver.<br>
                                                                              <br>
                                                                              <strong>Make sure your home page is named index.html</strong><br>
                                                                              <br>
                                                                              <strong>Zip your files</strong><br>
                                                                              You will need to zip your files to send them via email. If they are still several
                                                                              megabytes you may need to send to us using alternative methods such as ftp or using
                                                                              the dropbox on campus.<br>
                                                                              <br>
                                                                              <strong>We DONï¿½T need your source files</strong><br>
                                                                              Please donï¿½t send any FLA, PSD, or AI files. If we need them we will request them.<br>
                                                                              <br>
                                                                              <strong>Send us a screenshot.</strong><br>
                                                                              Obviously we canï¿½t post the interactive project directly in Flickr so weï¿½ll need
                                                                              a screen shot of your project like this. Send your screenshot in jpg format as described
                                                                              above for print design.<br>
                                                                              <br>
                                                                              <strong>Send us the details.</strong><br>
                                                                              Because we werenï¿½t in class with you we really donï¿½t have any information but
                                                                              what you provide us, so please provide the following when you send your files: Title
                                                                              of piece, your name, your instructors name, semester you completed the project, course
                                                                              name, and any special interactive features.<br>
                                                                              <br>
                                                                              So send us your work, we're eager to see more great projects! Send all work to <a href="mailto:akern@valenciacollege.edu">akern [at] valenciacollege.edu.</a> If you don't see your work in Flickr or you don't receive a response from me it's
                                                                              likely that your email might not have gone through so send us a follow up email if
                                                                              that happens. (Sorry, VCC's email is a tad bit secure!)<br>
                                                                              <br>
                                                                              <strong>Note:</strong><em>Sending your work to us does not guarantee it will be posted in Flickr. We reserve
                                                                                 the right to decline posting in Flickr, and of course we'll let you know why. </em></p>
                                                                           
                                                                           
                                                                           
                                                                           </font>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            <br>
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <img alt="" height="45" src="../../arts-and-entertainment/graphics-technology/ripped-paper-bottom.png" width="883">
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                             </center>
                                             
                                          </center>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          <br>
                                          
                                          <center>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  
                                                                  <div>
                                                                     <img alt="Valencia College News and Events. Click here to read more." border="0" src="../../arts-and-entertainment/graphics-technology/header-events.png"><br>
                                                                     <br>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     <p>There are no events this week.</p>
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  
                                                                  <div>
                                                                     &nbsp;&nbsp;<a href="../../calendar/calendar.cfm-catid=28.html" onmouseout="viewcal01.src='/arts-and-entertainment/images/viewcalendar1.png';" onmouseover="viewcal01.src='/arts-and-entertainment/images/viewcalendar2.png';" title="Click Here To View A Calendar of Arts And Entertainment Events."><img alt="Click Here To View A Calendar of Arts and Entertainment Events." border="0" name="viewcal01" src="../../arts-and-entertainment/graphics-technology/viewcalendar1.png" id="viewcal01"></a>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                          </center>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/graphic-interactive-design/studentwork.pcf">©</a>
      </div>
   </body>
</html>