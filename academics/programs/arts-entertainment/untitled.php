<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>1Page Title | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/untitled.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/_menu.inc"); ?>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>1Page Title</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>1Page Title</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               	     
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        				 
                        <div class="container_gray_bg">
                           						
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/art-studio/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/art-studio.png" alt="ART STUDIO">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>ART STUDIO</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Art Studio</strong></h3>
                                       
                                       
                                       <p>The Visual Arts Club is a group dedicated to fun artistic activities while helping
                                          to build skills and provide useful resources and knowledge to become successful in
                                          the art industry.
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div><a href="/academics/programs/arts-and-entertainment/art-studio/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/dance/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/dance_program.png" alt="">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>DANCE</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Dance</strong></h3>
                                       
                                       
                                       <p> The Valencia College Dance Program goal is to provide quality technical training
                                          and the enhancement of the overall dance experience.
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div><a href="/academics/programs/arts-and-entertainment/dance/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/digital-media/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/digital-media.png" alt="Digital Media ">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Digital Media</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Digital Media</strong></h3>
                                       
                                       
                                       <p>Valencia's Entertainment Design and Technology degree program is one of only a few
                                          in the nation and the only one in Central Florida that prepares students to work in
                                          the production aspects of the entertainment industry.
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div><a href="/academics/programs/arts-and-entertainment/digital-media/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/entertainment-design-technology/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/entertainment-design.png" alt="">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Entertainment Design Technology</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Entertainment Design Technology</strong></h3>
                                       
                                       
                                       <p>Valencia's Entertainment Design and Technology degree program is one of only a few
                                          in the nation and the only one in Central Florida that prepares students to work in
                                          the production aspects of the entertainment industry.
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div><a href="/academics/programs/arts-and-entertainment/entertainment-design-technology/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/film-technology/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/film.png" alt="">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Film</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Film Technology</strong></h3>
                                       
                                       
                                       <p>Valencia's nationally recognized Film Production Technology program is a selective
                                          admission, limited enrollment program and....
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div><a href="/academics/programs/arts-and-entertainment/film-technology/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="http://valtest:8888/academics/programs/arts-and-entertainment/graphics-technology/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/graphics-technology.png" alt="">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Graphics Technology</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Graphics Technology</strong></h3>
                                       
                                       
                                       <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                                          educating some of the most talented designers in central Florida. Our students have
                                          received....
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div>
                                          <a href="/academics/programs/arts-and-entertainment/entertainment-design-technology/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           					  
                           <!--End strip -->
                           
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/music/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/music.png" alt="Music">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Music</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Music</strong></h3>
                                       
                                       
                                       <p>Since 1975, the music program has been dedicated to the idea of inspiring future performers
                                          by providing students with musically trained faculty and....
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div>
                                          <a href="/academics/programs/arts-and-entertainment/music/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           <!--End strip -->
                           
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/music-sound-technology/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/music-technology.png" alt="Music Technology">
                                          
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Music and Sound Technology</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Music and Sound Technology</strong></h3>
                                       
                                       
                                       <p>Valencia's Music and Sound Technology program takes a unique approach to preparing
                                          you for an exciting career in the music and sound industry.
                                       </p>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div>
                                          <a href="/academics/programs/arts-and-entertainment/music-sound-technology/index.php" class="button_outline">Details</a></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <!--End strip -->
                           
                           
                           <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                              
                              <div class="row">
                                 
                                 <div class="col-lg-4 col-md-4 col-sm-4">
                                    
                                    <div class="img_list"><a href="/academics/programs/arts-and-entertainment/theater/index.php">
                                          <img src="/_resources/images/academics/programs/arts-and-entertainment/theater.png" alt="Theater">
                                          
                                          
                                          <div class="short_info">
                                             
                                             <h3>Theater</h3>
                                             
                                          </div>
                                          </a></div>
                                    
                                 </div>
                                 
                                 
                                 <div class="clearfix visible-xs-block"></div>
                                 
                                 
                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                    
                                    <div class="course_list_desc">
                                       
                                       <h3><strong>Theater</strong></h3>
                                       
                                       
                                       <p>Valencia College Theater produces four major productions each year, as well as a series
                                          of student-directed one-act plays. Productions are held either in our 558-seat....
                                       </p>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    
                                    <div class="details_list_col">
                                       
                                       <div>
                                          <a href="/academics/programs/arts-and-entertainment/theater/index.php" class="button_outline">Details</a></div>
                                       	
                                    </div>
                                    	
                                 </div>
                                 	
                              </div>
                              						 
                           </div>
                           					
                        </div>
                        				
                     </div>
                     			 
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/untitled.pcf">©</a>
      </div>
   </body>
</html>