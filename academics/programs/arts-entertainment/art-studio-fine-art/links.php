<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/art-studio-fine-art/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Art Studio/Fine Art</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/art-studio-fine-art/">Art Studio</a></li>
               <li>Links </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Related Links</h2>
                        		
                        		
                        		
                        		
                        <div class="wrapper_indent">
                           						
                           <p></p>
                           						
                           <div class="row">
                              							
                              <div class="col-md-6 col-sm-6">
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><strong>Axner Pottery Supply</strong>
                                       										
                                       <p><a href="http://www.axner.com/" target="_blank">http://www.axner.com/</a><br>
                                          											490 Kane Ct., Oviedo FL 32765<br>
                                          											800-843-7057 or 407-365-2600 <br>
                                          											<a href="http://maps.google.com/maps?oi=map&amp;q=490+Kane+Ct,+Oviedo,+FL+32765" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    <li><strong> Pearl Art Supply</strong>
                                       										
                                       <p><a href="http://www.pearlpaint.com/" target="_blank">http://www.pearlpaint.com/</a><br>
                                          											Altamonte Springs<br>
                                          											1220 E. Altamonte Drive, Altamonte Springs,
                                          											FL 32701<br>
                                          											Phone: 407-831-3000<br>
                                          											Store Hours: <br>
                                          											Monday to Saturday: 10 am to 7 pm<br>
                                          											Sunday: 11 am to 6 pm <span class="faculty_name"></span><br>
                                          								<a href="http://maps.google.com/maps?oi=map&amp;q=1220+E+Altamonte+Drive,+Altamonte+Springs,+FL+32701" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    <li><strong>Orlando Museum Of Art</strong>
                                       										
                                       <p><a href="http://www.omart.org/" target="_blank">http://www.omart.org/</a><br>
                                          											2416 North Mills Avenue<br>
                                          											Orlando, FL 32803-1483<br>
                                          											ph (407) 896-4231<br>
                                          											fx (407) 896-9920<br>
                                          											<a href="http://maps.google.com/maps?oi=map&amp;q=2416+North+Mills+Avenue,+Orlando,+FL+32803" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    <li><strong>Maitland Art Center</strong>
                                       										
                                       <p><a href="http://www.maitlandartcenter.org/" target="_blank">http://www.maitlandartcenter.org/</a><br>
                                          											231 W. Packwood Ave.<br>
                                          											Maitland Florida USA 32751-5596<br>
                                          											Phone: 407-539-2181 <br>
                                          											Fax: 407-539-1198<br>
                                          											<a href="http://maps.google.com/maps?oi=map&amp;q=231+W+Packwood+Ave,+Maitland,+FL+32751" target="_blank">map</a></p>
                                    </li>
                                    									
                                    								
                                 </ul>
                                 							
                              </div>
                              							
                              <div class="col-md-6 col-sm-6">
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><a href="http://www.dickblick.com/" target="_blank"><img src="/_resources/images/academics/programs/arts-entertainment/dick_blick.gif" alt="Dick Blick Art Supplies" width="158" height="35" border="0"></a><br>
                                       										
                                       <p>Online discount art supply store.</p>
                                    </li>
                                    									
                                    									
                                    <li><a href="http://www.samflax.com/" target="_blank"><img src="/_resources/images/academics/programs/arts-entertainment/sam_flax.gif" alt="Sam Flax" width="158" height="26" border="0"></a><br>
                                       										
                                       <p><a href="http://www.samflax.com/" target="_blank">http://www.samflax.com/</a><br>
                                          											1401 East Colonial Drive<br>
                                          											Orlando, FL 32803<br>
                                          											1-800-393-3529<br>
                                          											1-407-898-9785<br>
                                          											<a href="http://maps.google.com/maps?oi=map&amp;q=1401+East+Colonial+Drive,+Orlando,+FL+32803" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    									
                                    <li><a href="http://www.artsystemsfl.com/" target="_blank"><img src="/_resources/images/academics/programs/arts-entertainment/art-systems.gif" alt="Art Sytems" width="158" height="32" border="0"></a><br>
                                       										
                                       <p><a href="http://www.artsystemsfl.com/" target="_blank">http://www.artsystemsfl.com/</a><br>
                                          											1740 State Rd. 436<br>
                                          											Winter Park, FL 32792<br>
                                          											407-679-4700<br>
                                          											<a href="http://maps.google.com/maps?oi=map&amp;q=1740+State+Rd+436,+Winter+Park,+FL+32792" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    <li><a href="http://www.michaels.com" target="_blank"><img src="/_resources/images/academics/programs/arts-entertainment/michaels.gif" alt="Michaels" width="158" height="37" border="0"></a><br>
                                       										
                                       <p>Many Central Florida Locations <br>
                                          											<a href="http://www.michaels.com/art/online/home" target="_blank">http://www.michaels.com</a><br>
                                          											Phone: 1(800) MIC-HAEL<br>
                                          											<a href="http://www.google.com/local?hl=en&amp;lr=&amp;client=safari&amp;rls=en&amp;q=michaels+art+supply&amp;near=Orlando,+FL&amp;sa=X&amp;oi=localr" target="_blank">map</a></p>
                                    </li>
                                    									
                                    									
                                    <li><strong>Crealde Art</strong>
                                       										
                                       <p><a href="http://www.crealde.org/" target="_blank">http://www.crealde.org/</a><br>
                                          											600 Saint Andrews Blvd, Winter Park, FL<br>
                                          											(407) 671-1886<a href="http://www.google.com/maps?hl=en&amp;lr=&amp;q=crealde&amp;near=Orlando,+FL&amp;radius=0.0&amp;cid=28538056,-81379444,15167021609866748139&amp;li=lmd&amp;z=14&amp;t=m" target="_blank"><br>map</a></p>
                                    </li>
                                    								
                                    								
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>
                        
                        		
                        		
                        		
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/art-studio-fine-art/links.pcf">©</a>
      </div>
   </body>
</html>