<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Annual Student Art Sale | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/annual-student-art-sale.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/art-studio-fine-art/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Art Studio/Fine Art</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/art-studio-fine-art/">Art Studio</a></li>
               <li>Annual Student Art Sale</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div> 
                           						
                           <h2>Annual Student Art Sale</h2>
                           						
                           <p>In addition to an outstanding education, there are many opportunities outside the
                              classroom offered to our aspiring artists
                           </p>
                           			
                           <h4></h4>
                           						
                           <p>The Student Art Sale is open to all art students, and it occurs every year before
                              Christmas break out in the courtyard in front of the Library. Students simply sign
                              up for table space, show up with art to sell, and make money for themselves and for
                              the art program! 
                           </p>
                           					
                        </div>
                        	
                        		
                        <hr>
                        		
                        
                        <h3>Student Displays 2013</h3>
                        		
                        <p align="center"><img src="/_resources/images/academics/programs/arts-entertainment/SculptureIMGalletta.jpg" width="450" height="250" alt="Sculpture I M. Galletta" class="img-responsive img-rounded"><br>
                           								Sculpture I<br>
                           								Instructor : Michael Galletta 
                        </p>
                        								
                        								
                        <p align="center"><img src="/_resources/images/academics/programs/arts-entertainment/DesignICourtneyCanova.jpg" width="450" height="250" alt="Design I C. Canova" class="img-responsive img-rounded"><br>
                           								Design I<br>
                           								Instructor : Courtney Canova 
                        </p>
                        								
                        								
                        <p align="center"><img src="/_resources/images/academics/programs/arts-entertainment/DrawingICourtneyCanova.jpg" width="277" height="250" alt="Drawing I Courtney Canova" class="img-responsive img-rounded"> <br>
                           								Drawing I<br>
                           								Instructor : Courtney Canova  <br>
                           								<img src="/_resources/images/academics/programs/arts-entertainment/DesignIIJ_000.jpg" width="312" height="250" alt="Design II : J. Miller" class="img-responsive img-rounded"><br>
                           								Design II<br>
                           									Instructor: Jackie Otto-Miller 
                        </p>
                        		
                        		
                        <hr>
                        		
                        <h3>Student Displays 2012</h3>
                        		
                        <p>Art Department Annual Christmas</p>
                        		
                        <p align="center">
                           								<img src="/_resources/images/academics/programs/arts-entertainment/ChristmasSale-125.jpg" width="400" height="250" alt="Sale  -12  (5)" class="img-responsive img-rounded"> <br><br>
                           								<img src="/_resources/images/academics/programs/arts-entertainment/ChristmasSale-1215.jpg" width="400" height="250" alt="Sale - 12 (15)" class="img-responsive img-rounded"><br><br>
                           								<img src="/_resources/images/academics/programs/arts-entertainment/Christmassale-1212.jpg" width="400" height="250" alt="Sale - 12 (12)" class="img-responsive img-rounded"></p>
                        								
                        								
                        <p align="center">
                           								<img src="/_resources/images/academics/programs/arts-entertainment/ChristmasSale-1211.jpg" width="226" height="250" alt="Sale - 12  (11)" class="img-responsive img-rounded"><br><br>
                           								<img src="/_resources/images/academics/programs/arts-entertainment/ChristmasSale-1213.jpg" width="400" height="250" alt="Sale - 12  (13)" class="img-responsive img-rounded"><br><br>
                           								<img src="/_resources/images/academics/programs/arts-entertainment/ChristmasSale-127.jpg" width="400" height="250" alt="Sale - 12  (7)" class="img-responsive img-rounded"></p>
                        		
                        		
                        		
                        		
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/art-studio-fine-art/annual-student-art-sale.pcf">©</a>
      </div>
   </body>
</html>