<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty  | Valencia College</title>
      <meta name="Description" content="Faculty Art Studio/Fine Arts">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/art-studio-fine-art/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Art Studio/Fine Art</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/art-studio-fine-art/">Art Studio</a></li>
               <li>Faculty </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Faculty</h2>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <strong>Michael Galletta</strong>
                                       
                                       <p>Professor of Art, Program Director <br>
                                          Ceramics, Sculpture, &amp; Drawing<br>
                                          Office: 3 -118<br> Phone : 407-582-2328
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       <strong>Andrew Downey</strong>
                                       
                                       <p>Professor of Art <br>
                                          Drawing, Painting, Printmaking, &amp; 2D Design<br>
                                          Office: 3-117<br>
                                          Phone: 407-582-2327
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       <strong>Jackie Otto-Miller</strong>
                                       
                                       <p>Assistant Professor of Art, Gallery Director <br>
                                          2D &amp; 3D Design<br>
                                          Office: 3-112<br>
                                          Phone: 407-582-2298
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       <strong>Nancy Jay</strong>
                                       
                                       <p>Assistant Professor of Art, Senior Teaching Fellow <br>
                                          Drawing &amp; Painting
                                       </p>
                                       
                                    </li>
                                    
                                    
                                    <li>
                                       <strong>Rima Jabbur</strong>
                                       
                                       <p>Assistant Professor of Art<br>
                                          Drawing &amp; Painting
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Dennis Schmalstig</strong>
                                       
                                       <p>Assistant Professor of Art<br>
                                          Drawing &amp; Painting
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Camilo Velasquez</strong>
                                       
                                       <p>Assistant Professor of Art <br>
                                          2D Design &amp; Drawing
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Linda Ehmen</strong>
                                       
                                       <p>Assistant Professor of Art <br>
                                          Photography
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <strong>Allen Maxwell</strong>
                                       
                                       <p>Assistant Professor of Art <br> Photography
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>David Cumbie</strong>
                                       
                                       <p>Assistant Professor of Art<br> Sculpture
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Courtney Canova</strong>
                                       
                                       <p>Assistant Professor of Art<br> Design &amp;amp; Drawing
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Nargges Albekord</strong>
                                       
                                       <p>Assistant Professor of Art<br> Design &amp;amp; Drawing
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Jeffery Hendley</strong>
                                       
                                       <p>Assistant Professor of Art<br> Design &amp;amp; Drawing
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Nicholas Wozniak</strong>
                                       
                                       <p>Assistant Professor of Art<br> Digital Photography
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Vincent Sansone</strong>
                                       
                                       <p>Assistant Professor of Art<br> Ceramics
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>Cassandra Anselmo</strong>
                                       
                                       <p>Assistant Professor of Art<br> Photography
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>George Parker</strong>
                                       
                                       <p>Assistant Professor of Art <br> Ceramics
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/art-studio-fine-art/faculty.pcf">©</a>
      </div>
   </body>
</html>