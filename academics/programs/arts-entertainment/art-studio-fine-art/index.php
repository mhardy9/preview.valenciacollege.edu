<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Art Studio/Fine Art  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/art-studio-fine-art/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Art Studio/Fine Art</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li>Art Studio</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1"></div>
                        				
                        <h2>Art Studio/Fine Art</h2>
                        				
                        <p>Associate in Arts Degree</p>
                        				
                        <h3>Degree Offered</h3>
                        				
                        <p><strong>Associate in Arts Degree- Pre-Major: Art, Studio/ Fine Art</strong><br>
                           				<a href="/academics/programs/arts-entertainment/art-studio-fine-art/documents/art-studio-fine-art.pdf"><strong>View the Visual Arts Degree Requirements</strong></a></p>
                        				
                        <h4>Visual Arts Courses Offered</h4>
                        				
                        <p></p>
                        				
                        <ul>
                           					
                           <li><strong>DRAWING I &amp;amp; II</strong> : Andrew Downey, Dennis Schmalstig, Courtney Canova, Nargges Albekord, Allan Maxwell,
                              Kyle, Nancy Jay, Michael Galle
                           </li>
                           					
                           <li><strong>PAINTING I &amp;amp; II</strong> : Dennis Schmalstig
                           </li>
                           					
                           <li><strong>DESIGN I &amp;amp; II</strong> : Andrew Downey, Jackie Otto-Miller, Kyle, Camillo Velasquez
                           </li>
                           					
                           <li><strong>PRINTMAKING I &amp;amp; II</strong> : Andrew Downey
                           </li>
                           					
                           <li><strong>PHOTOGRAPHY I &amp;amp; II</strong> : Cassandra Anselmo, Allan Maxwell, Nicholas Wozniak (digital), Karen Lee (digital)
                           </li>
                           					
                           <li><strong>CERAMICS I, II, &amp;amp; III</strong> : Michael Galletta, George Parker, John Kellum, Vincent Sansone
                           </li>
                           					
                           <li><strong>SCULPTURE I &amp;amp; II</strong> : Michael Galletta, David Cumbie
                           </li>
                           					
                           <li><strong>LIFE DRAWING</strong>: Rima Jabbur
                           </li>
                           					
                           <li>Community partners who support the initiatives of the PJI, provide resources and education
                              to enhance our efforts, and collaborate to build the culture of peace and justice.
                           </li>
                           					
                           <li>Terminology related to Peace and Justice studies.</li>
                           					
                           <li>What We Are Reading, a list of books that have deeply informed our work.</li>
                           				
                        </ul><br>
                        				
                        <p></p>
                        				
                        <h4>Selected Topics Courses</h4>
                        				
                        <p>Potters Guild Ceramics III<br>
                           				Etching
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Financial Aid and Scholarships</h3>
                        				
                        <p></p>
                        				
                        <p>Scholarships for visual art students are given each year. These are based on merit
                           from a portfolio submission and may be granted in addition to other financial aid.
                           Submissions are taken every Spring semester in consideration of scholarships for the
                           following academic year. The due date of portfolio submissions will be announced soon
                           on this website as well as on the bulletin board in building 3 next to room 147.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/art-studio-fine-art/index.pcf">©</a>
      </div>
   </body>
</html>