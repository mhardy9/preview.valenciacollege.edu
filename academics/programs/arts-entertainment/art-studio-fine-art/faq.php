<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions (FAQ | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/arts-entertainment/art-studio-fine-art/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/arts-entertainment/art-studio-fine-art/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Art Studio/Fine Art</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/arts-entertainment/">Arts Entertainment</a></li>
               <li><a href="/academics/programs/arts-entertainment/art-studio-fine-art/">Art Studio</a></li>
               <li>Frequently Asked Questions (FAQ)</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Frequently Asked Questions (FAQ)</h2>         
                        
                        <p>
                           <strong>Do I have to buy all the supplies for my art classes?</strong><br>
                           Yes, most classes you will be required to pay for your own supplies.<br>
                           <br>
                           <strong>How much will I spend on supplies?</strong><br>
                           It depends on the specific course, but anywhere from $100 in a drawing/painting course
                           to $300 in Photography course which can include the purchasing of a camera.<br>
                           <br>
                           <strong>Will all my art classes transfer?</strong><br>
                           Yes, most art classes will transfer, best to check with the school you are planning
                           to attend, in case some do not.<br>
                           <br>
                           <strong>I took AP (advanced placement) art classes in high school; will I be able to get credit
                              for some VCC classes?</strong><br>
                           ï¿½APï¿½ students will need to submit their portfolio for review by Visual Art faculty,
                           The faculty will then determine if the portfolio is worthy of any credit courses,
                           The only course we do not give ï¿½APï¿½ credit for is ART 1201C Design I. Remember,
                           if you are given credit for a course you will still have to pay tuition for it.<br>
                           <br>
                           <strong>How do I put my art portfolio together to transfer to another college? </strong><br>
                           Check with the school you are planning to attend to see what they require, i.e. number
                           of pieces, media, slides of 3-D work. We offer a selected topics class in portfolio
                           preparation this would be a great class to take in your last semester before you transfer.<br>
                           <br>
                           <strong>Where can I get slides of my artwork made?</strong><br>
                           Check with local photography stores, they sometimes know of companies and or individuals
                           who can take photoï¿½s of your artwork. The portfolio preparation class has a photographer
                           come in to demonstrate how to photograph artwork yourself. Once the work is photographed
                           it can be processed into slides.<br>
                           <br>
                           <strong>What can I do with my visual art degree from Valencia Communtiy College?</strong><br>
                           Visual art is a pre-major A.A. degree, it is designed for students who plan to transfer
                           to a four year school as a junior and receive a Bachelorï¿½s degree in art.<br>
                           
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/arts-entertainment/art-studio-fine-art/faq.pcf">©</a>
      </div>
   </body>
</html>