<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Educator Preparation Institute  | Valencia College</title>
      <meta name="Description" content="Educator Preparation Institute">
      <meta name="Keywords" content="college, school, educational, educator, preparation, institute">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/educator-preparation-institute/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/educator-preparation-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Educator Preparation Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Teacher Preparation and Re-Certification</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>Educator Preparation Institute (EPI)</h3>
                           
                           <blockquote>The Educator Preparation Institute (EPI) program at Valencia College is designed for
                              individuals who want to teach, but hold a bachelor's degree in an area other than
                              education.&nbsp; This alternative certification program provides the knowledge and tools
                              necessary for earning a Florida Professional Educator's Certificate. To qualify for
                              the Educator Preparation Institute, you must have the following:
                           </blockquote>
                           
                           <ul>
                              
                              <li>Hold a bachelor's degree with a minimum overall GPA of 2.5 from a regionally accredited
                                 college or university
                              </li>
                              
                              <li>Obtain a statement of Status of Eligibility from the&nbsp;<a href="http://www.fldoe.org/edcert/steps.asp">Florida Department of Education (FLDOE)&nbsp;</a>stating "You are Eligible"
                              </li>
                              
                              <li>Have a passing score on the Basic Skills General Knowledge Test</li>
                              
                              <li>Attend a MANDATORY EPI Enrollment Session</li>
                              
                              <li>Submit a College Application on line at&nbsp;<a href="https://valenciacollege.edu/admissions-records/default.cfm">Admissions &amp; Records - Valencia College</a>
                                 
                              </li>
                              
                              <li>Submit a Proof of Residency Form on line at&nbsp;<a href="https://valenciacollege.edu/admissions-records/florida-residency/default.cfm">Florida Residency - Admissions &amp; Records - Valencia College</a>
                                 
                              </li>
                              
                              <li>Contact Donna Deitrick, Coordinator 407.582.5473 or Jennifer Walsh, Technical Documentation
                                 Specialist, 407.582.5581 with questions
                              </li>
                              
                              <li>Complete all required paperwork</li>
                              
                              <li>Submit required paperwork to Building 1 Room 255 (West Campus) or bring to the MANDATORY
                                 EPI enrollment session.
                              </li>
                              
                           </ul>
                           
                           <p align="center">Please click the&nbsp;<a href="https://valenciacollege.edu/epi/steps.cfm">"How to Enroll in EPI "</a>&nbsp;link for more details regarding the EPI program
                           </p>
                           
                           <h3>Teacher Re-Certification</h3>
                           
                           <p>Teachers who already hold a Professional Educator's Certificate and in need of certificate
                              renewal, may complete six (6) semester hours of college credit or equivalent earned
                              during each renewal period.&nbsp;Most of Valencia's courses meet the requirements of teachers
                              seeking re-certification. Please click the<a href="https://valenciacollege.edu/epi/teacher-recertification.cfm">&nbsp;"Teacher Re-Certification"&nbsp;</a>link for more details.
                           </p>
                           
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>The Educator Preparation Institute (EPI) Alternative Certification Program</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Educator Preparation Institute (EPI) program at Valencia College is designed for
                              individuals who want to teach, but hold a bachelor's degree in an area other than
                              education.&nbsp; This alternative certification program provides the knowledge and tools
                              necessary for earning a Florida Professional Teaching Certificate.
                           </p>
                           
                           <p>The program consists of 7 courses and a&nbsp;<a href="https://valenciacollege.edu/epi/FieldExp.cfm">field experience course&nbsp;</a>.&nbsp; The&nbsp;<a href="https://valenciacollege.edu/epi/FieldExp.cfm">field experience</a>&nbsp;course requires a total of 30 hours of observation in an approved school classroom.
                              Each course is 8 weeks in length and is presented in either an online or hybrid format.
                              Hybrid courses meet twice per 8 week term for 3 hours on campus with all other coursework
                              being online.
                           </p>
                           
                           <p><strong>Students who are not in a full-time teaching position or working as a full-time teacher's
                                 aide or paraprofessional will have to complete 5 hours of volunteer hours in K-12
                                 setting per 8 week term, not per course for hybrid courses.&nbsp;</strong>The exception will be when students are registered for their field experience courses.
                              Information about how to complete these volunteer hours will be communicated to students
                              by their instructors at the beginning of the term. Each course can require between
                              5-10 hours of work outside of your class. For this reason, it is important that you
                              consider commitments and register for the right amount of courses.&nbsp;<em><strong>You are only allowed to take 2 courses per 8 week term</strong></em>, but this requires a heavy time commitment.
                           </p>
                           
                           <p>We also offer fully-online courses, but if you are not a full-time teacher or a para-professional,
                              then you must complete&nbsp;<strong>10 hours of observation in a public, private, or charter school for each 8 week term
                                 that you are registered for an online course.&nbsp;</strong>Information about the observation will be provided by your instructor about the observation
                              requirement. These courses are intensive and require the completion of several projects
                              and research assignments along with readings, group work, and quizzes. You can estimate
                              about 10 hours worth of coursework per week, per course. Many students take 2 courses
                              at a time, but you are not required to take more than one. By taking 2 courses at
                              a time, it is possible to complete the program in two semesters, but it may take three
                              to four semesters. You must also complete all of the FTCE Exams and an EPI portfolio
                              in order to complete the program.
                           </p>
                           
                           <p>These courses will prepare you not only for the Florida Teacher Certification Exam
                              (Profession), but for success as a teacher in a classroom. Please refer to&nbsp;<a href="https://valenciacollege.edu/epi/courses.cfm">Courses&nbsp;</a>link for more information regarding courses.
                           </p>
                           <br>
                           
                           <h3><strong>Requirements for participation include:</strong></h3>
                           
                           <ul>
                              
                              <li>Hold a bachelor's degree with a minimum overall GPA of 2.5 from a regionally accredited
                                 college or university
                              </li>
                              
                              <li>Obtain a statement of Status of Eligibility from the&nbsp;<a href="http://www.fldoe.org/edcert/steps.asp">Florida Department of Education (FLDOE)&nbsp;</a>stating "You are Eligible"
                              </li>
                              
                              <li>Have a passing score on the Basic Skills General Knowledge Exam</li>
                              
                              <li>Attend a&nbsp;<em><strong>MANDATORY</strong></em>&nbsp;EPI Enrollment Session (email Donna Deitrick regarding enrollment session at&nbsp;<a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu</a>.
                              </li>
                              
                              <li>Submit a College Application on-line and pay admission fee</li>
                              
                              <li>Submit an EPI Application to Building 1 room 255 (West Campus), fax 407-582-5582 or
                                 scan and email to Donna Deitrick at&nbsp;<a href="mailto:ddeitrick@valenciacollege.edu">ddeitrick@valenciacollege.edu.</a>
                                 
                              </li>
                              
                              <li>Complete all required paperwork</li>
                              
                           </ul>
                           
                           <h3>Click here to view a sample&nbsp;<a href="http://valenciacollege.edu/epi/documents/HowdoIknowifIneedEPI.ppt">valid statement of eligibility</a>
                              
                           </h3>
                           
                           <p>If you have further questions, please refer to our&nbsp;<a href="https://valenciacollege.edu/epi/FrequentlyAskedQuestions.cfm">Frequently Asked Questions link&nbsp;</a>or use the&nbsp;<a href="http://net4.valenciacollege.edu/forms/epi/contact.cfm" target="_blank">Contact Us&nbsp;</a>link to email us your specific questions. We will respond in a timely manner.
                           </p>
                           
                           <p><strong><a href="https://valenciacollege.edu/epi/enroll/">Sign up for an Enrollment/Information Session here</a></strong></p>
                           <em><strong>NOTE: "Dates and times are subject to change, please check schedule before attending
                                 your selected enrollment session"</strong></em>
                           
                           
                        </div>        
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/educator-preparation-institute/index.pcf">©</a>
      </div>
   </body>
</html>