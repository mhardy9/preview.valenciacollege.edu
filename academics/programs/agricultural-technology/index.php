<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Plant Science and Agricultural Technology  | Valencia College</title>
      <meta name="Keywords" content="plant, science, agricultural, technology, transfer, university, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/agricultural-technology/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/agricultural-technology/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Agricultural Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <h2>Plant Science and Agricultural Technology</h2>
                        
                        <hr class="styled_2">
                        
                        <p>Through Valencia’s A.S. degree program in Plant Science and Agricultural Technology,
                           you can focus on either landscape (design, construction and maintenance) or horticulture
                           (greenhouse and nursery operations). The A.A. transfer plan provides the foundational
                           science courses you’ll need to transfer to a four-year horticulture program. Either
                           way, you’ll learn about everything from plant propagation to greenhouse construction
                           through labs, field trips, internships and greenhouses on both East and West Campuses.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <div class="row">
                           
                           <div class="col-lg-4 col-md-4 col-sm-4">
                              
                              <h3>FEATURED PROGRAM</h3>
                              
                              <p><img src="/_resources/img/academics/programs/agricultural-technology/featured-landscape-and-horticulture-tech.png"></p>
                              
                              <h5>Plant Science and Agricultural Technology</h5>
                              
                              <p>With tremendous advancement in computer-driven technologies, the approach to designing
                                 and planning of land developments has changed dramatically. Valencia’s A.S. degree
                                 program in Plant Science and Agricultural Technology takes a well-rounded approach,
                                 offering specializations in both landscape and horticulture.
                              </p>
                              
                              <p><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/landscape-and-horticulture-technology/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://preview.valenciacollege.edu/future-students/degree-options/associates/landscape-and-horticulture-technology/', 'LEARN MORE');" title="Learn More About Plant Science and Agricultural Technology"><span class="button small">LEARN MORE</span></a></p>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in"> <i class="pe-7s-gym"></i>
                           
                           <h3>Career Coach</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Explore related careers, salaries and job listings.</p>
                           
                           <p><a href="https://valenciacollege.emsicareercoach.com/#action=loadOccupationSearchResults&amp;amp%3BSearchType=occupation&amp;Search=horticulture&amp;Clusters=1&amp;Featured=&amp;WageLimit=0&amp;OccSearchSort=&amp;EdLevel=all" class="button small" target="_blank">Learn More</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h2>Programs &amp; Degrees</h2>
                        
                        <ul>
                           
                        </ul>
                        
                        <h3 class="subheader">Associate in Arts Transfer Plan</h3>
                        
                        <p><a href="http://catalog.valenciacollege.edu/transferplans/" target="_blank">Transfer Plans</a> are designed to prepare students to transfer to a Florida public university as a
                           junior.  The courses listed in the plans are the common prerequisites required for
                           the degree. They are placed within the general education requirements and/or the elective
                           credits.  Specific universities may have additional requirements, so it is best check
                           your transfer institution catalog and meet with a Valencia advisor.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/transferplans/horticulturalscience/">Horticulture Science</a></li>
                           
                        </ul>
                        
                        <h3 class="subheader">Associate in Science</h3>
                        
                        <p>The A.S. degree&nbsp;prepares you to enter a specialized career field in about two years.&nbsp;It
                           also transfers to the Bachelor of Applied Science program offered at some universities.
                           If the program is “articulated,” it will also transfer to a B.A. or B.S. degree program
                           at a designated university.
                        </p>
                        
                        <ul>
                           
                           <li><a href="https://preview.valenciacollege.edu/future-students/degree-options/associates/plant-science-and-agricultural-technology/">Plant Science and Agricultural Technology</a></li>
                           
                        </ul>
                        
                        <h3 class="subheader">Certificate</h3>
                        
                        <p>A certificate prepares you to enter a specialized career field or upgrade your skills
                           for job advancement. Most can be completed in one year or less. Credits earned can
                           be applied toward a related A.S. degree program.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/landscapehorticulturetechnology/#certificatestext">Landscape and Horticulture Professional</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/landscapehorticulturetechnology/#certificatestext">Landscape and Horticulture Specialist</a></li>
                           
                           <li><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/landscapehorticulturetechnology/#certificatestext">Landscape and Horticulture Technician</a></li>
                           
                        </ul>
                        
                        <h3 class="subheader">Continuing Education</h3>
                        
                        <p><a href="http://preview.valenciacollege.edu/continuing-education/programs/" target="_blank">Continuing Education</a> provides non-degree, non-credit programs including industry-focused training, professional
                           development, language courses, certifications and custom course development for organizations.
                           Day, evening, weekend and online learning opportunities are available.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/continuing-education/programs/">Continuing Education Programs</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/agricultural-technology/index.pcf">©</a>
      </div>
   </body>
</html>