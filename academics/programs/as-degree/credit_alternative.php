<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/credit_alternative.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Credit Earned&nbsp;for&nbsp;Approved Industry Certifications:</h2>
                        
                        <p>Below is a list of our current Industry Certification Agreements. Eligible students
                           may earn college credits toward an A.S. degree if you hold one of the approved industry
                           certifications listed below. However, you may only receive credit if your Program
                           of Study is in the designated program below, and you have received your industry certification
                           within the past three years.
                        </p>
                        
                        <p>To receive credit based upon one of these certifications, you must provide the following
                           <br> required documentation to the Dean's academic department for verification and approval:
                        </p>
                        
                        <ol>
                           
                           <li>Complete the Request Form listed under your Program of Study.</li>
                           
                           <li>Attach a copy of the appropriate Award of Credit Agreement.</li>
                           
                           <li>Attach a valid copy of the License or Certification required (must be within the past
                              <br> three years).
                           </li>
                           
                           <li>The Career Program Advisor or Program Director will verify the required criteria has
                              been met.
                           </li>
                           
                           <li>The Department Dean or Program Director will sign the Award of Credit Request Form,
                              and <br> send it with the required documentation to the Graduation Office to be processed.
                           </li>
                           
                        </ol>
                        
                        <hr class="styled_2">
                        
                        <h2>Industry Certification Agreements:</h2>
                        	<strong>Accounting Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-AccountingTechnology.pdf" target="_blank"><em>Request Form for Accounting Technology </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/AccountingTechnology-AccreditedLegalSecretary-ALS-S.pdf" target="_blank">Accredited Legal Secretary (ALS)</a> <strong>-</strong> (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/AccountingTechnology-CertifiedBookkeeper-S.pdf" target="_blank">Certified Bookkeeper</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/AccountinigTechnology-QuickBooksCertifiedUser-S.pdf" target="_blank">QuickBooks Certified User Certification</a> - (Statewide)
                           </li>
                           	
                        </ul> 
                        
                        <hr class="styled_2">
                        	<strong>Baking and Pastry Management:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-BakingandPastry.pdf" target="_blank"><em>Request Form for Baking and Pastry Management </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BakingandPastry-FoodSafetyManager-L.pdf" target="_blank">Food Safety Manager</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BakingandPastry-NationalProStart-L.pdf" target="_blank">National Pro Start Certificate of Achievement</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BakingandPastry-ServSafeFoodProtectionMgr-L.pdf" target="_blank">ServSafe Food Protection Manager</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Building Construction Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-BuildingConstruction.pdf" target="_blank"><strong><em>Request Form for Building Construction</em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BuildingConstruction-NCCERConstructionTechology-S.pdf" target="_blank">NCCER Construction Technology</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BuildingConstruction-LEEDProfessionalAccreditation.pdf" target="_blank">LEED Professional Accreditation</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Business Administration:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-BusinessAdministration.pdf" target="_blank"><em>Request Form for Business Administration </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/BusinessAdministration-FloridaRealEstateLicense-L-2013.pdf" target="_blank">Florida Real Estate License</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Cardiopulmonary Sciences (B.S. Degree):</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-Cardio-REVISED.pdf" target="_blank"><em><strong>Request Form for Cardiopulmonary Sciences </strong></em></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CardiopulmonarySciences-Final.pdf" target="_blank">Certification Agreement </a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Cardiovascular Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CardiovascularTechnology.pdf" target="_blank"><em>Request Form for Cardiovascular Technology </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CardiovascularTechnology-L.pdf" target="_blank">Cardiovascular Credential International </a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Civil/Surveying Engineering Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CivilSurveying.pdf" target="_blank"><em>Request Form for Civil/Surveying</em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Civil-Surveying-AutodeskCertifiedAssociate-AutoCADCivil3D-S.pdf" target="_blank">Autodesk Certified Associate AutoCAD Civil 3D</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Civil-Surveying-AutodeskCertifiedProf-AutoCADCivil3D-S.pdf" target="_blank">Autodesk Certified Professional AutoCAD Civil 3D </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CivilSurveying-SurveyTechnicianLevelIIField-L.pdf" target="_blank">Survey Technician Level II Field</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Computer Information Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-ComputerInformation.pdf" target="_blank">Request Form for Computer Information</a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-ArcGISDesktopEntryCertification-CIT-S.pdf" target="_blank">ArcGIS Desktop Entry</a> (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-CompTIAA-S.pdf" target="_blank">CompTIA A+ </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-CompTIALinuxCertification-LOCAL.pdf" target="_blank">CompTIA Linux+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-CompTIANetworkCertification-LOCAL.pdf" target="_blank">CompTIA Network+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CompInfoandCompPrgm-CompTIAProject-LOCAL.pdf" target="_blank">CompTIA Project+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-CompTIASecurity-LOCAL.pdf" target="_blank">CompTIA Security+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-CompTIAServer-S.pdf" target="_blank">CompTIA Server + </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-MicrosoftCertifiedDesktopSupportTechMCDSTCertification-S.pdf" target="_blank">Microsoft Certified Desktop Support Technician (MCDST) </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-MicrosoftCertifiedITProfMCIT-ConsumerSupportTechnicianCertification-S.pdf" target="_blank">Microsoft Certified IT Professional (MCIT) - Consumer Support Technician </a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-MicrosoftCertifiedITProfMCIT-EnterpriseSuptTechCertification-S.pdf" target="_blank">Microsoft Certified IT Professional (MCIT) - Enterprise Support Technician </a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-MicrosoftCertifiedTechSpecialistMCTSDistAppCertification-S.pdf" target="_blank">Microsoft Certified Technology Specialist (MCTS) - Distributed Applications </a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Computer-Information-Microsoft-Certified-Systems-Engineer-MCSE-Certification-S.pdf" target="_blank">Microsoft Certified Systems Engineer (MCSE) </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-MicrosoftTechAssociate-MTACertification-CIT-S.pdf" target="_blank">Microsoft Technology Associate-MTA</a> (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Computer Programming and Analysis:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-ComputerProgramming.pdf" target="_blank">Request Form for Computer Programming</a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-CIWAssociateDesignSpecialist-S.pdf" target="_blank">Certified Internet Web (CIW) Associate Design Specialist </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-CIWJavaScriptSpecialist-S.pdf" target="_blank">Certified Internet Web (CIW) JavaScript Specialist</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-CompTIAA-L.pdf" target="_blank">CompTIA A+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-CompTIALinuxCertification-S.pdf" target="_blank">CompTIA Linux+</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CompInfoandCompPrgm-CompTIAProject-LOCAL.pdf" target="_blank">CompTIA Project+</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-MCPDWindowsDeveloper-S.pdf" target="_blank">Microsoft Certified Professional Developer (MCPD) Windows Developer</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-MCTSWebApplications-S.pdf" target="_blank">Microsoft Certified Technology Specialist (MCTS) - Web Applications </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-MCTSWindowsApplications-S.pdf" target="_blank">Microsoft Certified Technology Specialist (MCTS) - Windows Applications</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-OracleCertifiedJavaAssociateProgrammer-S.pdf" target="_blank">Oracle Certified Java Associate Programmer </a>- (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Criminal Justice:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CriminalJustice.pdf" target="_blank">Request Form for Criminal Justice</a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Criminal-Justice-Correctional-and-Law-Enforcement-Officer.pdf" target="_blank">Correctional Officer and Law Enforcement Officer</a> (Statewide)
                           </li>
                           
                           <li><span><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CriminalJustice-PSLDC.pdf" target="_blank">Request Form for PSLDCP</a></span></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CriminalJustice-PublicSafetyLeadership-PSLDCP.pdf" target="_blank">Public Safety Leadership Development Certificate - PSLDCP</a> - (Local)
                           </li>
                           
                           <li><span><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CriminalJustice-PST-911.pdf" target="_blank">Request Form for 911 Certification </a></span></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CriminalJustice-911Certification-L.pdf" target="_blank">911 Public Safety Telecommunicator - PST - (Local) </a></li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Culinary Management:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-CulinaryManagement.pdf" target="_blank"><strong><em>Request Form for Culinary Management </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Culinary-CertifiedCulinarian-REV4-17-S.pdf" target="_blank">Certified Culinarian</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Culinary-NationalProStartCertificateofAchievement-S.pdf" target="_blank">National Pro Start Certificate of Achievement </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Culinary-ServSafeCertifiedProfessionalFoodServiceMgr-S.pdf" target="_blank">ServSafe Professional Food Service Manager</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CulinaryManagement-FoodSafetyManager-L.pdf" target="_blank">Food Safety Manager</a> (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CulinaryManagement-ServSafeFoodProtectionMgr-L.pdf" target="_blank">ServSafe Food Protection Manager</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Diagnostic Medical Sonography:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-DiagnosticMedicalSonography.pdf" target="_blank">Request Form for Diagnostic Medical Songraphy </a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/DiagnosticMedicalSonography-L.pdf" target="_blank">Registered Diagnostic Medical Sonographer (RDMS) </a>- (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Drafting and Design Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-DraftingandDesign.pdf" target="_blank"><strong><em>Request Form for Drafting and Design </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Drafting-ADDAApprenticeDrafter-AD-S.pdf" target="_blank">ADDA Apprentice Drafter (AD)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Drafting-AutodeskCertifiedAssociate-AutoCADArchitecture-S.pdf" target="_blank">Autodesk Certified Associate AutoCAD Architecture</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Drafting-AutodeskCertifiedAssociate-AutoCAD-S.pdf" target="_blank">Autodesk Certified Associate AutoCAD </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Drafting-AutodeskCertifiedUser-AutoCAD-S.pdf" target="_blank">Autodesk Certified User - AutoCAD Certification</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Drafting-AutodeskCertifiedProfessional-AutoCAD-S.pdf" target="_blank">Autodesk Certified Professional AutoCAD</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Electronics Engineering Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-ElectronicsEngineering.pdf" target="_blank"><strong><em>Request Form for Electronics Engineering </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ElectronicsEngineering-AssociateLevelCertElectronicTechnician-S.pdf" target="_blank">Associate Level Certified Electronic Technician</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ElectronicsEngineering-ElectronicsSystemsAssociateESA.pdf" target="_blank">Electronics Systems Associate (ESA)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/ElectronicsEngineering-MSSCCertifiedProductionTechnician-S.pdf" target="_blank">MSSC Certified Production Technician</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Emergency Medical Services Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-EmergencyMedicalServices.pdf" target="_blank"><strong><em>Request Form for Emergency Medical Services </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/EmergencyMedicalServices-EMT-2016-RevS.pdf" target="_blank">Emergency Medical Technician - Basic (EMT)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/EmergencyMedicalServices-Paramedic-2015-16-Rev-S.pdf" target="_blank">Paramedic</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Fire Science Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCredit-FireCertifications-2016.pdf" target="_blank"><strong><em>Request Form for Fire Science Technology </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-EMT-FireScience-2016.pdf" target="_blank">EMT</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireFighterI-FireScience-2016.pdf" target="_blank">Fire Fighter - Minimum Standards</a> (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireandLifesafetyEducator-FireScience-2017.pdf" target="_blank">Fire and Lifesafety Educator </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireOfficerI-FireScience-2016.pdf" target="_blank">Fire Officer I </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireOfficerII-FireScience-2016.pdf" target="_blank">Fire Officer II</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireSafetyInspectorI-FireScience-2016.pdf" target="_blank">Firesafety Inspector I</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireSafetyInspectorII-FireScience-2016.pdf" target="_blank">Firesafety Inspector II</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireInvestigatorI-FireScience-2016.pdf" target="_blank">Fire Investigator I </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireInstructorI-FireScience-2016.pdf" target="_blank">Fire Instructor I </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-FireInstructorII-FireScience-2016.pdf" target="_blank">Fire Instructor II</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-Paramedic-FireScience-2016.pdf" target="_blank">Paramedic</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Graphic and Interactive Design:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-GraphicandInteractiveDesign.pdf" target="_blank"><strong><em>Request Form for Graphic and Interactive Design </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/GraphicsTechnology-CIWMasterDesigner-S.pdf" target="_blank">Certified Internet Web (CIW) Master Designer</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Health Information Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-HealthInformation.pdf" target="_blank"><strong><em>Request Form for Health Information </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/HealthInformation-AHIMA-L.pdf" target="_blank">American Health Information Management Association - AHIMA </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/HealthInformation-AAPC-L.pdf" target="_blank">American Academy of Professional Coders - AAPC </a>- (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Hospitality and Tourism Management:</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-HospitalityandTourism.pdf" target="_blank"><strong><em>Request Form for Hospitality and Tourism </em></strong></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/HospitalityandTourism-FoodSafetyManager-L.pdf" target="_blank">Food Safety Manager </a>- (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/HospitalityandTourism-NationalProStart-L.pdf" target="_blank">National Pro Start Certificate of Achievement</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/HospitalityandTourism-ServSafeFoodProtectionMgr-L.pdf" target="_blank">ServSafe Food Protection Manager </a>- (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Network Engineering Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-NetworkEngineering.pdf" target="_blank">Request Form for Network Engineering </a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/NetworkEngineering-CISCOCCNACertification-NET-REV1-14.pdf" target="_blank">Cisco Certified Network Associate (CCNA)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/NetworkEngineering-CISCOCCNA-SecurityCertification-NET-REV1-14.pdf" target="_blank">Cisco Certified Network Associate Security (CCNA Security)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/NetworkEngineering-CompTIAACertification-NET-REV1-14.pdf" target="_blank">CompTIA A+</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/NetworkEngineering-CompTIANetworkCertification-NET-REV1-14.pdf" target="_blank">CompTIA Network+</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Network-Engineering-Microsoft-Certified-IT-Prof-MCITP-Server-Admin-Certification-NET.pdf" target="_blank">Microsoft Certified IT Professional (MCITP) - Server Administrator</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Nursing, RN:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-NursingRN.pdf" target="_blank"><em>Request Form for Nursing, RN </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Nursing-LicensedPracticalNursing-S.pdf" target="_blank">Licensed Practical Nurse (LPN) </a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Office Administration:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-OfficeAdministration.pdf" target="_blank"><em>Request Form for Office Administration </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/OfficeAdministration-AccreditedLegalSecretary-ALS-S.pdf" target="_blank">Accredited Legal Secretary (ALS) </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CPSandCAPAwardAgreement-11.17.2008.pdf" target="_blank">CAP &amp; CPS</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/OfficeAdministration-MicrosoftOfficeMaster-MOM-S.pdf" target="_blank">Microsoft Office Master (MOM)</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/OfficeAdministration-MicrosoftOfficeSpecialistMOS-3of5-Rev.pdf" target="_blank">Microsoft Office Specialist (MOS) Bundle (3 of 5)</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Plant Science and Agricultural Technology:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-LandscapeandHorticulture.pdf" target="_blank"><em>Request Form for Landscape and Horticulture </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/LandscapeandHorticulture-CertifiedHorticultureProfessional-CHP-S.pdf" target="_blank">Certified Horticulture Professional (CHP)</a> - (Statewide)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Radiography:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-Radiography.pdf" target="_blank"><em>Request Form for Radiography </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Radiography-RegisteredTechnologistR.T.R-L.pdf" target="_blank">Registered Technologist in Radiography R.T. (R)</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Radiologic and Imaging Sciences (B.S. Degree):</strong>
                        
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-RadiologicSciences-Final.pdf" target="_blank"><em><strong>Request Form for Radiologic and Imaging Sciences</strong></em></a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/CerttowardBSinRadiologicandImagingSciences-Final.pdf" target="_blank">Certification Agreement </a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Residential Property Management:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><em><a href="/academics/programs/as-degree/documents/Residential-Property-Management-Award-of-Credit-Request-Form.pdf" target="_blank">Request Form for Residential Property Management</a></em></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Residential-Property-Management-ARM-L.pdf" target="_blank">Accredited Residential Manager (ARM)</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Residential-Property-Management-CAM-L.pdf" target="_blank">Certified Apartment Manager (CAM)</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Residential-Property-Management-CPM-L.pdf" target="_blank">Certified Property Manager (CPM)</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Residential-Property-Management-NALP-L.pdf" target="_blank">National Apartment Leasing Professional (NALP)</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Residential-Property-Management-Florida-Real-Estate-License-L-2017.pdf" target="_blank">Florida Real Estate License</a></li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Respiratory Care:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-RespiratoryCare.pdf" target="_blank"><em>Request Form for Respiratory Care </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/RespiratoryCare-ResgisterdRespiratoryTherapist-RRT-L.pdf" target="_blank">Registered Respiratory Therapists (R.R.T.) in Respiratory Care</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Restaurant and Food Service Management:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-RestaurantandFoodService.pdf" target="_blank"><em>Request Form for Restaurant Management </em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Restaurant-CertifiedCulinarian-S.pdf" target="_blank">Certified Culinarian</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Restaurant-FoodServiceManagementProfessional-S.pdf" target="_blank">Food Service Management Professional</a> - (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/RestaurantandFoodService-NationalProStart-L.pdf" target="_blank">National Pro Start Certificate of Achievement</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Restaurant-ServSafeCertifiedProfessionalFoodServiceMgr-S.pdf" target="_blank">ServSafe Certified Professional Food Service Manager </a>- (Statewide)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/RestaruantandFoodService-FoodSafetyManager-L.pdf" target="_blank">Food Safety Manager</a> - (Local)
                           </li>
                           
                           <li><a href="/academics/programs/as-degree/documents/RestaurantandFoodService-ServSafeFoodProtectionMgr-L.pdf" target="_blank">ServSafe Food Protection Manager</a> - (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        <strong>Supervision and Management for Industry:</strong>
                        
                        
                        <ul>
                           
                           <li><strong><a href="/academics/programs/as-degree/documents/AwardofCreditRequestForm-IndustrialManagement.pdf" target="_blank"><em>Request Form for Orlando Utilities Commission</em></a></strong></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/IndustrialManagement-OUCLineTechnician-L.pdf" target="_blank">Orlando Utilities Commission (OUC) Line Technician </a>- (Local)
                           </li>
                           
                        </ul> 
                        <hr class="styled_2">
                        	
                     </div>
                     	
                  </div>
                  		
               </div>
               
               
               
               
               
               
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/credit_alternative.pcf">©</a>
      </div>
   </body>
</html>