<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>GEDT_Output | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/gainful-employment/cyber-security/11.1001-Gedt.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li><a href="/academics/programs/as-degree/gainful-employment/">Gainful Employment</a></li>
               <li><a href="/academics/programs/as-degree/gainful-employment/cyber-security/">Cyber Security</a></li>
               <li>GEDT_Output</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               
               <div>
                  
                  
                  <div class="container-fluid">
                     
                     
                     <div class="row">
                        
                        <div class="col-md-5 col-md-offset-3 first_block" id="divtitle" style="position:relative;">
                           
                           <div class="Institution_Name" id="schoolName" tabindex="0">Institution Name</div>
                           
                           <div class="Program_Info" tabindex="0">
                              <span id="spanAwardType">[Credential Level]</span> in <span id="spanProgramName">{Program Name}</span>  Program Length: <span id="spanNormalTimeToCompleteProgram"> [Program Length]</span> <span id="spanDurationTypeInWeeksMonthsYears"> [Weeks/Months/Years]</span>
                              
                           </div>
                           
                           <div class="Program_Info" style="position:absolute;bottom:-4px;right:11px;"><a href="gedt_outputprint.html" id="printlink">Print</a></div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                     
                     <div class="row" id="divwarning" style="display:none;">
                        
                        <div class="col-md-5 col-md-offset-3 warning_block">
                           <i class="icon-warning-sign icon-5x pull-left" alt="Warning Icon"></i>
                           <span class="alerttext" tabindex="0">This program has not passed standards established by the U.S. Department of Education.
                              The Department based these standards on the amounts students borrow for enrollment
                              in this program and their reported earnings. If in the future the program does not
                              pass the standards, students who are then enrolled may not be able to use federal
                              student grants or loans to pay for the program, and may have to find other ways, such
                              as private loans, to pay for the program.</span>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3 Info_Box">
                           
                           <div class="Title" tabindex="0">Students graduating on time</div>
                           
                           <div tabindex="0">
                              <span class="Values" tabindex="0"><span id="spanPercentOfStudentsCompletedProgramInNormalTime">NA*</span> </span>of Title IV students complete the program within <span id="spanNormalTimeToCompleteProgram2" tabindex="0">[Program Length]</span> <span id="spanDurationTypeInWeeksMonthsYears2" tabindex="0"> [Weeks/Months/Years]</span>
                              
                              <div class="icon-info-sign istudents" id="tooltipstudents" data-toggle="tooltip" data-placement="bottom" title="The share of students who completed the program within 100% of normal time" tabindex="0" alt="Additional Information Icon"></div>
                              
                              <div class="Footnote" id="studentgraduatingfootnote" tabindex="0">*Fewer than 10 students enrolled in this program. This number has been withheld to
                                 preserve the confidentiality of the students.
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3 Info_Box">
                           
                           <div class="Title" tabindex="0">Program Costs*</div>
                           
                           <div tabindex="0">
                              <span class="Values" id="spanTutionAndFees">$XX </span> for <span id="spanTuitionandFeesin">in-state </span>tuition and fees
                           </div>
                           
                           <div id="divTuitionAndFeesout" tabindex="0">
                              <span class="Values" id="spanTutionAndFeesout">$XX </span> for out-of-state tuition and fees
                           </div>
                           
                           <div tabindex="0">
                              <span class="Values" id="spanBooksAndSupplies">$XX </span> for books and supplies
                           </div>
                           
                           <div tabindex="0">
                              <span class="Values" id="spanRoomAndBoard2">$XX </span> for off-campus room and board
                           </div>
                           
                           <div id="divoncampus" tabindex="0">
                              <span class="Values" id="spanRoomAndBoard">$XX </span> for on-campus room and board
                           </div>
                           
                           
                           <a class="Plain_Text_Link" data-toggle="modal" href="11.1001-Gedt.html#myModal" style="cursor:pointer;" tabindex="0" data-keyboard="true">Other Costs</a>
                           
                           
                           <div class="modal fade" id="myModal" role="dialog"></div>
                           
                           
                           
                           
                           
                           <div class="Plain_Text_Link"><a id="hlUrlToProgramCost" style="color:#333333;" href="javascript:void(0);" target="_blank">Visit website for more program cost information</a></div>
                           
                           <div class="Footnote" tabindex="0">*The amounts shown above include costs for the entire program, assuming normal time
                              to completion.Note that this information is subject to change.
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3 Info_Box">
                           
                           <div class="Title" tabindex="0">Students Borrowing Money</div>
                           
                           <div tabindex="0">
                              <span class="Values" tabindex="0"><span id="spanPercentWhoUsedLoansForThisProgram" tabindex="0">NA*</span> </span>of students who attend this program borrow money to pay for it
                              
                              <div class="icon-info-sign" data-toggle="tooltip" data-placement="bottom" title="The share of students who borrowed Federal, private, and/or institutional loans to help pay for college." tabindex="0" alt="Additional Information Icon">
                                 
                              </div>
                              
                              <div class="Footnote" id="studentenrollingfootnote" tabindex="0">*Fewer than 10 students enrolled in this program. This number has been withheld to
                                 preserve the confidentiality of the students.
                              </div>
                              
                           </div>
                           
                           <div class="Title" tabindex="0">The typical graduate leaves with</div>
                           
                           <div tabindex="0">
                              <span class="Values" id="spanTitle4Loan">$XX </span> in debt
                              
                              <div class="icon-info-sign" data-toggle="tooltip" data-placement="bottom" title="The median debt of borrowers who completed this program. This debt includes federal, private, and institutional loans." tabindex="0" alt="Additional Information Icon">
                                 
                                 
                              </div>
                              
                              <div class="Footnote" id="studentborrowingfootnote">*Fewer than 10 students completed this program within normal time. This number has
                                 been withheld to preserve the confidentiality of the students.
                              </div>
                              
                           </div>
                           
                           <div class="Title" tabindex="0">The typical monthly loan payment</div>
                           
                           <div tabindex="0">
                              <span class="Values" id="LoanPerMonth">$XX </span> per month in student loans with <span class="Values" id="InterestRate"></span> interest rate.
                              
                              <div class="icon-info-sign iinterestrate" data-toggle="tooltip" data-placement="bottom" title="The median monthly loan payment for students who completed this program if it were repaid over ten years at a" tabindex="0" alt="Additional Information Icon">
                                 
                                 
                              </div>
                              
                              <div class="Footnote" id="studentborrowingfootnote2">*Fewer than 10 students completed this program within normal time. This number has
                                 been withheld to preserve the confidentiality of the students.
                              </div>
                              
                           </div>
                           
                           <div class="Title" tabindex="0">The typical graduate earns</div>
                           
                           <div tabindex="0">
                              <span class="Values" id="MedianEarnings">$XX </span> per year after leaving this program
                              
                              <div class="icon-info-sign" data-toggle="tooltip" data-placement="bottom" title="The median earnings of program graduates who received Federal aid." tabindex="0" alt="Additional Information Icon">
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="Footnote" id="studentborrowingEarningsfootnote" style="display:none" tabindex="0">*This institution has appealed the earnings data for this program.</div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3 Info_Box" style="padding-right:1px; overflow:auto;">
                           
                           <div class="Title" tabindex="0">Graduates who got jobs</div>
                           
                           <div id="divJobPlacementRateState" tabindex="0">
                              <span class="Values"><span id="spanJobPlacementRateState">N/A*</span> </span>of program graduates got jobs according to the&nbsp;
                              
                              
                              
                              <div id="divStatePopup" style="display: inline-block;" tabindex="0">
                                 <a class="Plain_Text_Link" data-toggle="modal" href="11.1001-Gedt.html#myModal4" style="cursor:pointer;" tabindex="0">state job placement rate</a>
                                 
                                 
                                 <div class="modal fade" id="myModal4" role="dialog"></div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           <div class="Footnote" id="statejobplacementfootnote" style="display:none; float:left;" tabindex="0">*Program does not have enough completers to calculate a placement rate as required.</div>
                           
                           
                           <div id="divJobPlacementRateAgency" tabindex="0" style="float:left;">
                              <span class="Values"><span id="spanJobPlacementRateAgency">N/A*</span> </span>of program graduates got jobs according to the&nbsp;
                              
                              <span id="divAgencyPopup" style="display:inline-block;">
                                 
                                 <a class="Plain_Text_Link" data-toggle="modal" href="11.1001-Gedt.html#myModal3" style="cursor:pointer;" tabindex="0">accreditor job placement rate</a>
                                 
                                 
                                 <div class="modal fade" id="myModal3" role="dialog"></div>
                                 </span>
                              
                              
                           </div>
                           
                           
                           
                           <div class="Footnote" id="accreditorjobplacementfootnote" style="display:none; float:left;" tabindex="0">*Program does not have enough completers to calculate a placement rate as required.</div>
                           
                           
                           <div id="divJobPlacementRateGeneral" style="display:none" tabindex="0">
                              <span class="Values"><span>N/A*</span> </span>of program graduates got jobs
                           </div>
                           
                           <div class="Footnote" id="jobplacementfootnote" style="display:none" tabindex="0">*We are not currently required to calculate a job placement rate for program completers.</div>
                           
                           <div class="Title" style="float:left;" tabindex="0">Program graduates are employed in the following fields:</div>
                           
                           <div class="Plain_Text_Link" id="divRelatedJobsToTheProgramContent" style="float:left;" tabindex="0">
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3 Info_Box" tabindex="0">
                           
                           <div class="Title">
                              Licensure Requirements
                              <span class="icon-info-sign" data-toggle="tooltip" data-placement="bottom" title="Some States require students to graduate from a state approved program in order to obtain a license to practice a profession in those States." tabindex="0" alt="Additional Information Icon"></span>
                              
                           </div>
                           
                           <div id="divvalidstates" tabindex="0">
                              
                              <div id="divSelectedStates" tabindex="0">
                                 
                                 <div tabindex="0">This program meets licensure requirements in</div>
                                 
                                 <div class="Values" tabindex="0"><span id="spanSelectedStates">StateA, StateB, StateC</span></div>
                                 
                              </div>
                              
                              <div id="divSelectedStatesN" tabindex="0">
                                 
                                 <div tabindex="0">This program does not meet licensure requirements in</div>
                                 
                                 <div class="N_Values" tabindex="0"><span id="spanSelectedStatesN">StateD, StateE</span></div>
                                 
                              </div>
                              
                              <div id="divSelectedStatesC" tabindex="0">
                                 
                                 <div tabindex="0">There are no licensure requirements for this profession in:</div>
                                 
                                 <div tabindex="0"><span id="spanSelectedStatesC" style="font-weight:bold;">StateF, StateG, StateH</span></div>
                                 
                              </div>
                              
                              <div class="Footnote" id="divLicensureSitFootnote" style="display:none;" tabindex="0">*State requires students to sit for licensure exam</div>
                              
                              
                           </div>
                           
                           <div class="Footnote" id="divnoLicensure" style="display:none" tabindex="0">*Program has no licensure requirements in any state.</div> 
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     <div class="row">
                        
                        
                        <div class="col-md-5 col-md-offset-3">
                           
                           
                           <a class="Plain_Text_Link" data-toggle="modal" href="11.1001-Gedt.html#myModal2" style="cursor:pointer;" tabindex="0">Additional Information</a>
                           
                           
                           <div class="modal fade" id="myModal2" role="dialog"></div>
                           
                           
                           
                           
                           <div tabindex="0">Date Created <span id="spanDateCreated">XX/XX/XX</span>
                              
                           </div>
                           
                           <div class="Footnote" style="margin-bottom:40px" tabindex="0">These disclosures are required by the U.S. Department of Education</div>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </div>
                  
               </div>
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/gainful-employment/cyber-security/11.1001-Gedt.pcf">©</a>
      </div>
   </body>
</html>