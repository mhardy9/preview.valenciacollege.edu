<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Gainful Employment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/gainful-employment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Gainful Employment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Gainful Employment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  				
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        <h2>Gainful Employment</h2>
                        
                        
                        <h3>Allied Health</h3>
                        			
                        <p>
                           <ul>
                              
                              <li><a href="Paramedic-Technology/51.0904-Gedt.html">Paramedic Technology</a></li>
                              
                           </ul>
                        </p>
                        	
                        <hr class="styled_2">
                        
                        <h3>Arts and Entertainment</h3>
                        
                        <p>  
                           <ul>
                              
                              <li><a href="Digital-Broadcast-Production/10.0202-Gedt.html">Digital Broadcast Production</a></li>
                              
                              <li><a href="Digital-Media-Webcast-Technology/50.0102-Gedt.html">Digital Media Webcast Technology</a></li>
                              
                              <li><a href="Digital-Video-Editing-and-Post-Production/09.0402-Gedt.html">Digital Video Editing and Post Production</a></li>
                              
                              <li><a href="Film-Production-Fundamentals/50.0602-Gedt.html">Film Production Fundamentals</a></li>
                              
                              <li><a href="Graphic-Design-Production/11.0803-Gedt.html">Graphic Design Production</a></li>
                              
                              <li><a href="Graphics-Interactive-Design-Production/11.0803-Gedt.html">Graphics - Interactive Design Production</a></li>
                              
                              
                           </ul>
                        </p>
                        					
                        <hr class="styled_2">     
                        
                        
                        <h3>Business</h3>    
                        
                        <ul>
                           
                           <li><a href="Accounting-Applications/52.0302-Gedt.html">Accounting Applications</a></li>
                           
                           <li><a href="Business-Management/52.0701-Gedt.html">Business Management</a></li>
                           
                           <li><a href="Customer-Service-Management/52.0701-Gedt.html">Customer Service Management</a></li>
                           
                           <li><a href="Human-Resources-Management/52.0701-Gedt.html">Human Resources Management</a></li>
                           
                           <li><a href="Medical-Office-Management/51.0716-Gedt.html">Medical Office Management</a></li>
                           
                           <li><a href="Accounting-Applications/52.0302-Gedt.html"></a></li>
                           			
                           <li>	<a href="Office-Management/52.0204-Gedt.html">Office Management</a></li>
                           
                           
                        </ul> 
                        		
                        <hr class="styled_2">     
                        
                        <h3>Criminal Justice and Paralegal Studies</h3>   
                        
                        <ul>
                           
                           <li><a href="Criminal-Justic-Technology-Specialist/43.0103-Gedt.html">Criminal Justice Technology Specialist</a></li>
                           
                           <li><a href="Law-Enforcement-Officer/43.0107-Gedt.html">Law Enforcement Officer</a></li>
                           
                           
                        </ul> 
                        			
                        <hr class="styled_2">     
                        
                        <h3>Engineering</h3>    
                        
                        <ul>
                           
                           <li><a href="Advanced-Electronics-Technician/15.0303-Gedt.html">Advanced Electronics Technician</a></li>
                           
                           <li><a href="Drafting-Architectural-Specialization/15.1301-Gedt.html">Drafting - Architectural Specialization</a></li>
                           
                           <li><a href="Drafting-Mechanical-Specialization/15.1301-Gedt.html">Drafting - Mechanical Specialization</a></li>
                           
                           <li><a href="Drafting-Surveying-Specialization/15.1301-Gedt.html">Drafting - Surveying Specialization</a></li>
                           
                           
                        </ul> 
                        <hr class="styled_2">     
                        
                        <h3>Hospitality and Culinary</h3>    
                        
                        <ul>
                           
                           <li><a href="Baking-and-Pastry-Arts/12.0501-Gedt.html">Baking and Pastry Arts</a></li>
                           
                           <li><a href="Culinary-Arts/12.0503-Gedt.html">Culinary Arts</a></li>
                           
                           <li><a href="Hospitality-Event-Planning-Management/52.0909-Gedt.html">Hospitality - Event Planning Management</a></li>
                           
                           <li><a href="Hospitality-Restaurant-and-Food-Service-Management/52.0905-Gedt.html">Hospitality - Restaurant and Food Service Management</a></li>
                           
                           <li><a href="Hospitality-Rooms-Division-Management/52.0904-Gedt.html">Hospitality - Rooms Division Management</a></li>
                           
                           
                        </ul> 
                        <hr class="styled_2">     
                        
                        <h3>Information Technology</h3>    
                        
                        <ul>
                           
                           <li><a href="Advanced-Network-Administration/11.1001-Gedt.html">Advanced Network Administration</a></li>
                           
                           <li><a href="Advanced-Network-Infrastructure/11.1001-Gedt.html">Advanced Network Infrastructure</a></li>
                           
                           <li><a href="Computer-Information-Technology-Analyst-IT-Security-Specialization/11.0103-Gedt.html">Computer Information Technology Analyst - IT Security Specialization</a></li>
                           
                           <li><a href="Computer-Information-Technology-Analyst-IT-Support-Specialization/11.0103-Gedt.html">Computer Information Technology Analyst - IT Support Specialization</a></li>
                           
                           <li><a href="Computer-Programming-Computer-Programming-33-credits/11.0202-Gedt.html">Computer Programming - Computer Programming 33 credits</a></li>
                           
                           <li><a href="Computer-Programming-Computer-Programming-Specialization/11.0202-Gedt.html">Computer Programming - Computer Programming Specialization</a></li>
                           
                           <li><a href="Computer-Programming-Game-Programming-Specialization/11.0202-Gedt.html">Computer Programming - Game Programming Specialization</a></li>
                           
                           <li><a href="Computer-Programming-Web-Development-Specialization/11.0202-Gedt.html">Computer Programming - Web Development Specialization</a></li>
                           
                           <li><a href="Cyber-Security/11.1001-Gedt.html">Cyber Security</a></li>
                           
                           <li><a href="Digital-Forensics/11.1001-Gedt.html">Digital Forensics</a></li>
                           
                           <li><a href="Network-Administration/11.1001-Gedt.html">Network Administration</a></li>
                           
                           
                        </ul> 
                        <hr class="styled_2">
                        
                        
                        <h3>Plant Science and Agricultural  Technology </h3>    
                        
                        <ul>
                           
                           <li><a href="Landscape-and-Horticulture-Technician-Horticulture-Specialization/01.0605-Gedt.html">Landscape and Horticulture Technician - Horticulture Specialization</a></li>
                           
                           <li><a href="Landscape-and-Horticulture-Technician-Landscape-Specialization/01.0605-Gedt.html">Landscape and Horticulture Technician - Landscape Specialization</a></li>
                           
                           	
                        </ul>
                        
                        
                        
                     </div>
                     	  
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/gainful-employment/index.pcf">©</a>
      </div>
   </body>
</html>