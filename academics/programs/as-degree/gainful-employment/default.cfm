<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Gainful Employment - Career Programs - Associate in Science (A.S.) Degree and Certificate Programs - Valencia College</title>
<cfinclude template="/includes/headTags.ssi">
<link rel="stylesheet" href="/includes/contribute.css" type="text/css" />
<link rel="stylesheet" href="/asdegrees/majors.css" type="text/css" />


<script type="text/javascript" src="/includes/jquery/jquery.cycle.all.js"></script> 

<script type="text/javascript">
$(document).ready(function(){
	function onAfter(curr, next, opts) { 
		$(this).children("div#transHide").show("slow");<!---     This shows the textbox after the image has loaded --->
			
	}
	function onBefore() { 
		$("div#transHide").hide();<!---     This hides the textbox before the image loads --->
	}
	
	$('#slideshow').hover(
		function() { $('#slideNav').show(); },
        function() { $('#slideNav').hide(); }
    );	
						   
	$('#mainImage')
	.cycle({
	fx:    'fade',
	timeout: 5000,
    before:   onBefore, 
    after:   onAfter,
	next:   '#next',
    prev:   '#prev'

	});

	
});
</script>




<cfinclude template="/includes/google.ssi">
</head>

<body>
<div id="containerHeaderAlert"><cfinclude template="/includes/incl_headeralert.ssi"></div>
<div id="containerTop"><cfinclude template="/includes/header.ssi"></div>
<div id="containerMain"> 

<div id="containerContent" role="main">
<div id="wrapper">
<div id="crumb"><cfinclude template="/asdegrees/crumb.ssi"></div>

<table id="tableMain" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
      <div id="contentNav"><cfinclude template="/asdegrees/nav.ssi"></div> 
      
    </td>
    <td valign="top">
      <table id="contentTable" border="0" cellspacing="0" cellpadding="0"><!--- 
  <!---*************************** If no image - turn off to end point **********************************--->    
       <tr>
          <td>&nbsp;</td>
          <td valign="top" colspan="3"><cfinclude template="/asdegrees/photoSlider.ssi"><br />
          </td>
       </tr>
         <!---***************************  end point **********************************--->     --->
        <tr>
          <td>&nbsp;</td>
          <td valign="top">
            <h1 id="headMain">Gainful Employment</h1>
            
            <h3>Allied Health</h3>
            <p><a href="Paramedic-Technology/51.0904-Gedt.html">Paramedic Technology</a></p>
            <p>&nbsp;</p>
            <h3>Arts and Entertainment</h3>
            <p><a href="Digital-Broadcast-Production/10.0202-Gedt.html">Digital Broadcast Production</a></p>
            <p><a href="Digital-Media-Webcast-Technology/50.0102-Gedt.html">Digital Media Webcast Technology</a></p>
            <p><a href="Digital-Video-Editing-and-Post-Production/09.0402-Gedt.html">Digital Video Editing and Post Production</a></p>
            <p><a href="Film-Production-Fundamentals/50.0602-Gedt.html">Film Production Fundamentals</a></p>
            <p><a href="Graphic-Design-Production/11.0803-Gedt.html">Graphic Design Production</a></p>
            <p><a href="Graphics-Interactive-Design-Production/11.0803-Gedt.html">Graphics - Interactive Design Production</a></p>
            <p>&nbsp;</p>
            <h3>Business</h3>
            <p><a href="Accounting-Applications/52.0302-Gedt.html">Accounting Applications</a></p>
            <p><a href="Business-Management/52.0701-Gedt.html">Business Management</a></p>
            <p><a href="Customer-Service-Management/52.0701-Gedt.html">Customer Service Management</a></p>
            <p><a href="Human-Resources-Management/52.0701-Gedt.html">Human Resources Management</a></p>
            <p><a href="Medical-Office-Management/51.0716-Gedt.html">Medical Office Management</a></p>
            <p><a href="Accounting-Applications/52.0302-Gedt.html"></a><a href="Office-Management/52.0204-Gedt.html">Office Management</a></p>
            <p>&nbsp;</p>
            <h3>Criminal Justice and Paralegal Studies</h3>
            <p><a href="Criminal-Justic-Technology-Specialist/43.0103-Gedt.html">Criminal Justice Technology Specialist</a></p>
            <p><a href="Law-Enforcement-Officer/43.0107-Gedt.html">Law Enforcement Officer</a></p>
            <p>&nbsp;</p>
            <h3>Engineering</h3>
            <p><a href="Advanced-Electronics-Technician/15.0303-Gedt.html">Advanced Electronics Technician</a></p>
            <p><a href="Drafting-Architectural-Specialization/15.1301-Gedt.html">Drafting - Architectural Specialization</a></p>
            <p><a href="Drafting-Mechanical-Specialization/15.1301-Gedt.html">Drafting - Mechanical Specialization</a></p>
            <p><a href="Drafting-Surveying-Specialization/15.1301-Gedt.html">Drafting - Surveying Specialization</a></p>
            <p>&nbsp;</p>
            <h3>Hospitality and Culinary</h3>
            <p><a href="Baking-and-Pastry-Arts/12.0501-Gedt.html">Baking and Pastry Arts</a></p>
            <p><a href="Culinary-Arts/12.0503-Gedt.html">Culinary Arts</a></p>
            <p><a href="Hospitality-Event-Planning-Management/52.0909-Gedt.html">Hospitality - Event Planning Management</a></p>
            <p><a href="Hospitality-Restaurant-and-Food-Service-Management/52.0905-Gedt.html">Hospitality - Restaurant and Food Service Management</a></p>
            <p><a href="Hospitality-Rooms-Division-Management/52.0904-Gedt.html">Hospitality - Rooms Division Management</a></p>
            <p>&nbsp;</p>
            <h3>Information Technology</h3>
            <p><a href="Advanced-Network-Administration/11.1001-Gedt.html">Advanced Network Administration</a></p>
            <p><a href="Advanced-Network-Infrastructure/11.1001-Gedt.html">Advanced Network Infrastructure</a></p>
            <p><a href="Computer-Information-Technology-Analyst-IT-Security-Specialization/11.0103-Gedt.html">Computer Information Technology Analyst - IT Security Specialization</a></p>
            <p><a href="Computer-Information-Technology-Analyst-IT-Support-Specialization/11.0103-Gedt.html">Computer Information Technology Analyst - IT Support Specialization</a></p>
            <p><a href="Computer-Programming-Computer-Programming-33-credits/11.0202-Gedt.html">Computer Programming - Computer Programming 33 credits</a></p>
            <p><a href="Computer-Programming-Computer-Programming-Specialization/11.0202-Gedt.html">Computer Programming - Computer Programming Specialization</a></p>
            <p><a href="Computer-Programming-Game-Programming-Specialization/11.0202-Gedt.html">Computer Programming - Game Programming Specialization</a></p>
            <p><a href="Computer-Programming-Web-Development-Specialization/11.0202-Gedt.html">Computer Programming - Web Development Specialization</a></p>
            <p><a href="Cyber-Security/11.1001-Gedt.html">Cyber Security</a></p>
            <p><a href="Digital-Forensics/11.1001-Gedt.html">Digital Forensics</a></p>
            <p><a href="Network-Administration/11.1001-Gedt.html">Network Administration</a></p>
            <p>&nbsp;</p>
            <h3>Plant Science and Agricultural  Technology </h3>
            <p><a href="Landscape-and-Horticulture-Technician-Horticulture-Specialization/01.0605-Gedt.html">Landscape and Horticulture Technician - Horticulture Specialization</a></p>
            <p><a href="Landscape-and-Horticulture-Technician-Landscape-Specialization/01.0605-Gedt.html">Landscape and Horticulture Technician - Landscape Specialization</a></p>
            <p>&nbsp;</p>
<p style="float:right;"><a href="#top">TOP</a></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            
            
<!---****************************End Body Content***************************************--->          
          </td>
          <td valign="top">&nbsp;</td>
          <td valign="top">
<!---***************************Right Column Content goes Here (255px)**********************************--->          
          
            <div id="rightColumn">
            	<cfinclude template="/asdegrees/rightColumn.ssi">
            </div>
          
<!---****************************End Right Column Content***************************************--->          
          </td>
        </tr>
        <tr>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="15" height="1" alt="" /></td>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="490" height="1" alt="" /></td>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="15" height="1" alt="" /></td>
          <td valign="top"><img class="no_print" src="/images/spacer.gif" width="265" height="1" alt="" /></td>
        </tr>
      </table>
      
      
    </td>
  </tr>
  <tr>
    <td><img class="no_print" src="/images/spacer.gif" width="162" height="1" alt="" /></td>
    <td><img class="no_print" src="/images/spacer.gif" width="798" height="1" alt="" /></td>
  </tr>
</table>

</div>
</div>
</div> 
<div id="containerBtm"><cfinclude template="/includes/footer.ssi"></div>
</body>
</html>
