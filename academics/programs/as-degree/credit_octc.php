<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/credit_octc.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2><font color="#bf311A">Credits Awarded at Valencia -- from OCPS Orange Technical Colleges: <br>
                              (Avalon, Mid Florida, Orlando, Westside and Winter Park Campuses) </font></h2>
                        
                        <h4>technical college  Industrial Related Programs that articulate into Valencia's supervision
                           &amp; 
                           Management for industry A.S. Degree:
                        </h4>
                        
                        <p>The Supervision &amp; Management for Industry program is an articulated 
                           program with Orange County Technical Colleges that is designed to provide 
                           opportunities for students to advance who have completed specific 
                           industrial related programs at the tech centers. This degree provides 
                           individuals currently working in industry an opportunity to pursue 
                           college level education that is appropriate for supervisory and 
                           management roles and upward mobility in their respective fields. 
                           
                        </p>
                        
                        <p>Please click on the agreement below to view the tech center programs that are eligible
                           for credit. Eligible students will be awarded 24 to 27 credits toward the technical
                           
                           skills requirement in the Supervision &amp; Management for Industry A.S. degree. 
                        </p>
                        
                        <h4>Credits Awarded from Valencia 
                           for programs taken at any Ocps technical college:
                        </h4>
                        
                        <p>In a continuing effort to provide career ladder opportunities for 
                           students in technical education programs, Valencia College 
                           agrees to extend at no cost to the eligible students (other than 
                           the application for admission fee) full college credit who have 
                           completed the following tech center programs.
                        </p>
                        
                        <p>Click on the Agreement (PDF) link below to view a copy of the actual 
                           agreement which lists which Valencia courses you can get college 
                           credit for. For more information, contact the person listed under 
                           the program you're interested in. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">OCPS - ORANGE TECHNICAL COLLEGE PROGRAM</div>
                                 
                                 <div data-old-tag="td">OCPS CAMPUS &amp; TECH EXPRESS ADVISOR</div>
                                 
                                 <div data-old-tag="td">VALENCIA ASSOCIATE IN SCIENCE (A.S.) DEGREE PROGRAM (AGREEMENT)</div>
                                 
                                 <div data-old-tag="td">VALENCIA CAREER PROGRAM ADVISOR</div>
                                 
                                 <div data-old-tag="td">POTENTIAL CREDIT</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Accounting Operations<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-Office-Administration-2017-18.pdf" target="_blank">Office Administration    A.S. Degree</a></strong><br>
                                    <br>
                                    <br>                  
                                    <br>
                                    <strong><a href="documents/OCPS-Accounting-Technology-2017-18.pdf" target="_blank">Accounting Technology A.S. Degree</a></strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Lisa Larson</strong><br>                  <strong>West Campus: <br> Beverly Johnson</strong><br>
                                    <br>                  
                                    <strong>East Campus: <br> 
                                       Liz Jusino</strong><br>                    <strong>West Campus: <br> Genevieve Hall</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administrative Office    Specialist  <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz<br>
                                       Winter Park Campus:  Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Office-Administration-2017-18.pdf" target="_blank">Office Administration    A.S. Degree</a><a href="documents/OCPS-Office-Administration-2017-18.pdf"></a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Lisa Larson</strong><br>                  <strong>West Campus: <br> Beverly Johnson</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">18</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Advanced Automotive    Technology (Toyota) <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls <br>
                                       West Campus: <br> Genevieve Hall </strong></div>
                                 
                                 <div data-old-tag="td">27 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Air Conditioning, Refrigeration and Heating</strong> <br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Westside Campus: Francisco Perez</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-Building-Construction-2017-18.pdf" target="_blank">Building Construction    Technology A.S. Degree</a></strong><br>
                                    <br>
                                    <strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf"></a></strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Beverly Johnson<br>
                                       </strong> <br>                  <strong>East Campus: <br> Kerry-Ann Rawls</strong> <br>                  <strong>West Campus: <br> Genevieve Hall<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">3<br>
                                    <br>
                                    <br>
                                    24
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Air Conditioning, Refrigeration /    Heating </strong><strong>Apprenticeship <br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf"></a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Automotive Collision    Repair / Refinishing <br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Automotive Service Technology <br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24 - 27<br>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Barbering<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Westside Campus: Francisco    Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Brick and Block    Masonry Apprenticeship <br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Building Construction    Technology<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Westside Campus: Francisco    Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Building-Construction-2017-18.pdf" target="_blank">Building Construction    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>West Campus: <br> Beverly Johnson</strong></div>
                                 
                                 <div data-old-tag="td">9 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Building Maintenance    Apprenticeship (Building Construction Technologies)<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Commercial Foods and    Culinary Arts </strong><a href="documents/CulinaryManagement-OCPS-2015.pdf.html"><br>
                                       </a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Orlando Campus: <br> Anita Gentz<br>
                                       Westside Campus: Francisco Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Culinary-Management-2017-18.pdf" target="_blank">Culinary Management    A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> D'Mya Clay </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">15</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Computer Systems &amp;    Information Technology <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Winter Park Campus:  Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Network-Engineering-2017-18.pdf" target="_blank">Network Engineering    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Jon Sowell </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">6 - 12</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Cosmetology <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Westside Campus: Francisco    Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Dental Assisting <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Dental-Hygiene-2017-18.pdf" target="_blank">Dental Hygiene A.S.    Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>West Campus: <br> Pam Sandy </strong></div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Diesel Technology    (Medium Heavy Truck &amp; Bus Technician) <br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">27 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Digital Audio    Production <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Sound-and-Music-2017-18.pdf" target="_blank">Sound and Music    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Kristol Bell </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Digital    Media / Multimedia Design<br>
                                       </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Avalon / Westside Campus:    <br>
                                       Francisco Perez</strong><br>                  <strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Winter Park Campus:  Anita Gentz</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Graphics-2017-18.pdf" target="_blank">Graphic and    Interactive Design A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Niurka Rivera</strong> <br>                  <strong>West Campus: <br> Genevieve Hall<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Digital Photography    Technology<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Digital Video    Production<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Digital-Media-2017-18.pdf" target="_blank">Digital Media    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Kristol Bell </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">16 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Drafting <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Drafting-and-Deisgn-2017-18.pdf" target="_blank">Drafting &amp; Design    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Beverly Johnson </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">12 - 18</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Early Childhood    Education (Child Development Associate - CDA)<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Child-Development-Associate-2017-18.pdf" target="_blank">Early Childhood    Education A.A. Degree</a><br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East and West Campus:</strong><br><strong>Molly McIntire    (Dean)</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Electrical    Apprenticeship<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Electronic Technology<br>
                                       <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-Electronics-Engineering-2017-18.pdf" target="_blank">Electronics    Engineering Technology A.S. Degree</a></strong><br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Jon Sowell </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">15 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>EMT<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa Bliss</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-EMS-Fire-2017-18.pdf" target="_blank">Emergency Medical    Services A.S. Degree</a></strong> <br>
                                    <br>                  
                                    <strong><a href="documents/OCPS-EMS-Fire-2017-18.pdf" target="_blank">Fire Science    Technology A.S. Degree</a></strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Danielle Montague</strong><br>                  <strong><br>
                                       School of Public Safety:</strong> <strong><br>
                                       Lizza    Burgos-Reynoso<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Fashion Technology    &amp; Production Services<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td">24 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Fire Sprinkler Systems    Apprenticeship<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Game / Simulation / Animation    - Audio/Video Effects <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Digital-Media-2017-18.pdf" target="_blank">Digital Media    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kristol Bell</strong></div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Game / Simulation / Animation    - Programming <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-Computer-Technology-2017-18.pdf" target="_blank">Computer Programming    &amp; Analysis A.S. Degree</a></strong><br>
                                    <strong><a href="documents/OCPS-Computer-Technology-2017-18.pdf" target="_blank">Computer Information Technology A.S. Degree</a></strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Launa Sickler </strong>
                                    <br>                  <strong>West Campus: <br> Jon Sowell<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Game / Simulation / Animation    - 
                                       Visual Design <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Digital-Media-2017-18.pdf" target="_blank">Digital Media    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kristol Bell</strong></div>
                                 
                                 <div data-old-tag="td">9</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Hotel &amp; Resort    Business Management (Lodging Operations) <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Winter Park Campus:  Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Hospitality-and-Tourism-2017-18.pdf" target="_blank">Hospitality &amp;    Tourism Management A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Rita Maldonado </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">Up    to 12 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Industrial Pipefitter    Apprenticeship<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Machining Technologies<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">27 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Mechanical    Apprenticeship<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Medical Administrative    Specialist<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Westside Campus: Francisco    Perez<br>
                                       Orlando / Winter Park Campus:  Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Medical-Office-2017-18.pdf" target="_blank">Medical Office    Administration A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Lisa Larson</strong><br>                  <strong>West Campus: <br> Beverly Johnson<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">18<br>                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Medical Assisting<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Avalon / Westside Campus: <br>
                                       Francisco    Perez<br>
                                       Winter Park Campus:  Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Medical-Office-2017-18.pdf" target="_blank">Medical Office    Administration A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Lisa Larson</strong><br>                  <strong>West Campus: <br> Beverly Johnson</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">6<br>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Medical Coder / Biller<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Winter Park Campus:  Anita    Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Health-Information-2017-18.pdf" target="_blank">Health Information    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>West Campus: <br> Danielle Montague</strong></div>
                                 
                                 <div data-old-tag="td">25<br>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Medical Laboratory    Assisting<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Biotechnology-2017-18.pdf" target="_blank">Biotechnology    Laboratory Sciences A.S. Degree</a><br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Lake Nona Campus:</strong> <strong>Sarah Ashby</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">4</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Medical Records    Transcribing<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Winter Park Campus:  Anita    Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Medical-Office-2017-18.pdf" target="_blank">Medical Office    Administration A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Lisa Larson</strong><br>                  <strong>West Campus: <br> Beverly Johnson<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">15</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Network Systems    Administration<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Winter Park Campus:  Anita    Gentz<br>
                                       Westside Campus: Francisco Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Network-Engineering-2017-18.pdf" target="_blank">Network Engineering    Technology A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Jon Sowell </strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">6 - 12 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Plumbing Technology    Apprenticeship<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Practical Nursing (LPN)    <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Orlando Campus: <br> Anita Gentz</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Nursing-2017-18.pdf" target="_blank">Nursing A.S. Degree - Accelerated    Track </a></strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>West Campus: <br> Danielle Montague</strong><br>                
                                 </div>
                                 
                                 <div data-old-tag="td">12</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Sport, Recreation and    Entertainment Marketing <br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Business-Administration-2017-18.pdf" target="_blank">Business    Administration A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Mario Richardson</strong></div>
                                 
                                 <div data-old-tag="td">3</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Structural Steel    Apprenticeship<br>
                                       </strong><strong> </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Web Development<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Winter Park Campus:  Anita    Gentz</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong><a href="documents/OCPS-Computer-Technology-2017-18.pdf" target="_blank">Computer Programming    &amp; Analysis A.S. Degree</a></strong><br>
                                    <strong><a href="documents/OCPS-Computer-Technology-2017-18.pdf" target="_blank">Computer Information Technology A.S. Degree</a></strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>East Campus: <br> Launa Sickler</strong><br>                  <strong>West Campus: <br> Jon Sowell<br>
                                       </strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Up to 9 <br>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Welding (Applied    Welding Technologies)<br>
                                       </strong></div>
                                 
                                 <div data-old-tag="td"><strong>Mid Florida Campus: Lisa    Bliss<br>
                                       Westside Campus: Francisco Perez</strong></div>
                                 
                                 <div data-old-tag="td"><strong><a href="documents/OCPS-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></div>
                                 
                                 <div data-old-tag="td"><strong>East Campus: <br> Kerry-Ann Rawls<br>
                                       West Campus: <br> Genevieve Hall</strong></div>
                                 
                                 <div data-old-tag="td">24 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <div> 
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    
                                    <div data-old-tag="td">In order to view PDF files, you will need Adobe Reader which is available 
                                       for free from the <a href="http://get.adobe.com/reader/" target="_blank">Adobe</a> 
                                       web site. 
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/credit_octc.pcf">©</a>
      </div>
   </body>
</html>