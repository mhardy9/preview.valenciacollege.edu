<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Teco Test | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/teco-test.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Teco Test</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Teco Test</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Credits Awarded at Valencia from TECO:</h2><em>(Technical Education Center Osceola)</em>
                        
                        <h3>Teco Industrial Related Programs into Valencia's Industrial Management Technology
                           A.S. Degree:
                        </h3>            
                        
                        <p>The Industrial Management Technology program is an articulated 
                           program with TECO that is designed to provide opportunities for 
                           students to advance who have completed specific industrial related 
                           programs at the tech centers. This degree provides individuals currently 
                           working in industry an opportunity to pursue college level education 
                           that is appropriate for supervisory and management roles and upward 
                           mobility in their respective fields. 
                        </p>
                        
                        <p>Please click on the agreement below to view the TECO programs that are eligible for
                           credit.. Eligible students will be awarded 24 to 27 credits toward the technical 
                           skills requirement in the Industrial Management A.S. degree.
                        </p>
                        	
                        	 
                        	   
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>CREDITS AWARED FROM VALENCIA FOR PROGRAMS TAKEN AT TECO:</h3>
                           
                           <p>Valencia College and the Technical Education Center of 
                              Osceola (TECO) have teamed up to offer you college credit for the 
                              following programs you complete at TECO. 
                           </p>
                           
                           <p>Click on the Agreement (PDF) link below to view a copy of the actual 
                              agreement which lists which Valencia courses you can get college 
                              credit for. For more information, contact the person listed under 
                              the program you're interested in. 
                           </p>
                           
                        </div>
                        
                        <div>
                           
                           <p>Required Courses - There are 6 required credits. Students must take three credits
                              hours from the Core offerings and three credit hours from Institutional offerings.
                              One course must be a Gordon Rule <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/#">(GR)</a> course, and completed with a minimum grade of C to fulfill the Gordon Rule Requirement
                              
                           </p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p></p>
                                 
                                 <table class="table table table-striped cart-list add_bottom_30">
                                    
                                    <thead>
                                       
                                       <tr>
                                          
                                          <th>TECO PROGRAM<br>
                                             (Tech Express Advisor: Francisco Perez)
                                          </th>
                                          			
                                          <th>VALENCIA ASSOCIATE IN SCIENCE (A.S.) DEGREE PROGRAM (AGREEMENT)</th>
                                          			
                                          <th>VALENCIA CAREER PROGRAM ADVISOR	</th>
                                          				
                                          <th>POTENTIAL CREDITS
                                             
                                          </th>
                                          
                                       </tr>
                                       
                                    </thead>
                                    
                                    <tbody>
                                       
                                       
                                       <tr>
                                          
                                          <td><strong>Air    Conditioning, Refrigeration &amp; Heating</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Automotive Service Technology</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24 - 27</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Barbering </strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Commercial Foods &amp; Culinary    Arts</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Culinary-Management-2017-18.pdf" target="_blank">Culinary Management A.S.    Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>West Campus: <br> D'Mya Clay<br>
                                                </strong>407-582-1972
                                          </td>
                                          
                                          <td>15 </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Correctional Officer</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Criminal-Justice-2017-18.pdf" target="_blank">Criminal Justice A.S.    Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Leonardo Rodriguez Braga<br>
                                                </strong>407-582-1583
                                          </td>
                                          
                                          <td>6</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Cosmetology</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Digital Design</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Graphics-2017-18.pdf" target="_blank">Graphic &amp;    Interactive Design A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Roxana Boulos<br>
                                                </strong>407-582-4231
                                          </td>
                                          
                                          <td>9</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Electricity</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Law Enforcement Officer</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Criminal-Justice-2017-18.pdf" target="_blank">Criminal Justice A.S.    Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Leonardo Rodriguez Braga<br>
                                                </strong>407-582-1583
                                          </td>
                                          
                                          <td>15</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Legal Administrative    Specialist&nbsp;</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Office-Administration-2017-18.pdf" target="_blank">Office Administration A.S.    Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Leonardo Rodriguez Braga<br>
                                                </strong>407-582-1583
                                          </td>
                                          
                                          <td>15<br>                
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Medical Administrative    Specialist&nbsp;</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Medical-Office-Administration-2017-18.pdf" target="_blank">Medical Office    Administration A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Leonardo Rodriguez Braga<br>
                                                </strong>407-582-1583
                                          </td>
                                          
                                          <td>18<br>                
                                          </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Medical Assisting</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Medical-Office-Administration-2017-18.pdf" target="_blank">Medical Office    Administration A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Leonardo Rodriguez Braga<br>
                                                </strong>407-582-1583
                                          </td>
                                          
                                          <td>6</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Medical Coder/Biller </strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Health-Information-2017-18.pdf" target="_blank">Health Information    Technology A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>West Campus: <br></strong> <strong>Danielle Montague<br>
                                                </strong>407-582-1565
                                          </td>
                                          
                                          <td>25</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Medical Laboratory Assisting </strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Biotechnology-2017-18.pdf" target="_blank">Biotechnology    Laboratory Sciences A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Sarah Ashby<br>
                                                </strong>407-582-7352
                                          </td>
                                          
                                          <td>3</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             <strong>Network Support Services</strong> 
                                          </td>
                                          
                                          <td><strong><a href="documents/TECO-Network-Engineering-2017-18.pdf" target="_blank">Network Engineering    Technology A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Osceola Campus: <br> Roxana Boulos<br>
                                                </strong>407-582-4231<br>                  <strong>West Campus: <br> Jon Sowell<br>
                                                </strong>407-582-1973
                                          </td>
                                          
                                          <td>6 - 15 </td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Pharmacy Technician</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Plumbing</strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Supervision-and-Management-2017-18.pdf" target="_blank">Supervision &amp; Management    for Industry A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Claudia Zequeira<br>
                                                </strong>407-582-4172
                                          </td>
                                          
                                          <td>24</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td>
                                             <strong>Practical Nursing</strong> <strong>(LPN)</strong>
                                             
                                          </td>
                                          
                                          <td><strong><a href="documents/TECO-Nursing-20171-18.pdf" target="_blank">Nursing A.S. Degree - Advanced    Standing Track </a></strong></td>
                                          
                                          <td>
                                             <strong>West Campus: <br> Danielle Montague<br>
                                                </strong>407-582-1165
                                          </td>
                                          
                                          <td>12</td>
                                          
                                       </tr>
                                       
                                       <tr>
                                          
                                          <td><strong>Web Development </strong></td>
                                          
                                          <td><strong><a href="documents/TECO-Computer-Technology-2017-18.pdf" target="_blank">Computer&nbsp;Programming&nbsp;&amp;&nbsp;Analysis    A.S. Degree and Computer Information Technology
                                                   A.S. Degree</a></strong></td>
                                          
                                          <td>
                                             <strong>Roxana Boulos<br>
                                                </strong>407-582-4231
                                          </td>
                                          
                                          <td>Up to 9 <br>                
                                          </td>
                                          
                                       </tr>
                                       
                                    </tbody>
                                    
                                 </table>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        	 
                     </div>
                     			
                  </div>
                  		
               </div>
               	 
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/teco-test.pcf">©</a>
      </div>
   </body>
</html>