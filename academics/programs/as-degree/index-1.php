<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Associate in Science (A.S.) Degree and Certificate Programs  | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/index-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	
                        <div class="main-title">
                           
                           <h2>Associate in Science (A.S.) Degree</h2>
                           		
                           <p>Associate in Science (A.S.) Degree and Certificate Programs</p>
                           		
                        </div>
                        
                        <div class="row box_style_1">
                           		
                           <p>Jump right into the job market with one of Valencia's exciting career programs. If
                              you are thinking about entering the workforce soon, Valencia has a program for you!
                              We offer more than 120 career programs, including 34 Associate in Science degrees
                              and 85 certificate programs, to prepare you to go directly into a specialized career.
                              Whether it's nursing, cyber security, software engineering, digital media or a host
                              of other high-demand occupations, Valencia has a degree or certificate program to
                              meet your career goals.
                           </p> 
                        </div>
                     </div>
                     
                     
                     
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/arts-and-entertainment.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Arts &amp; Entertainment</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/business.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Business</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/engineering-and-technology.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Engineering &amp; Technology</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/health-sciences.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Health Sciences</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/hospitality-and-culinary.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Hospitality &amp; Culinary</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/information-technology.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Information Technology</h3>
                           					
                           <p></p>
                           					</a></div>
                     	
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/landscape-and-horticulture.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Landscape &amp; Horticulture </h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4"><a class="box_feat" href="/academics/programs/as-degree/public-safety-and-legal.php"> <i class="far fa-graduation-cap"></i>
                           					
                           <h3>Public Safety &amp; Legal</h3>
                           					
                           <p></p>
                           					</a></div>
                     				
                     <div class="col-md-4 col-sm-4">
                        					
                        <h3></h3>
                        					
                        <p></p>
                        					
                     </div>
                     			
                  </div>
                  			
                  <div>
                     			
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/index-1.pcf">©</a>
      </div>
   </body>
</html>