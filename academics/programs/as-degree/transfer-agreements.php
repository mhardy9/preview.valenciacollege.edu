<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/transfer-agreements.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	
                        <h2>Transfer Agreements</h2>
                        
                        <p>
                           <h4>Alternative Ways To Earn College Credit toward Valencia's Associate in Science (A.S.)
                              Degrees
                           </h4>
                        </p>
                        
                        <h3>Alternative Award Of Credit Agreements</h3>
                        
                        
                        <h4>Credit for Industry Certifications:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/credit_alternative.php">Agreements for Approved Industry Certifications</a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for Orange Technical College Programs (Tech Express):</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/credit_octc.php">Agreements with Mid Florida, Orlando, Westside and Winter Park Campuses </a></li>
                           
                        </ul>
                        
                        <p>If you have earned your certification as a CDA from an OCPS Tech Center, you may be
                           eligible to receive credit toward Valencia's A.A. Degree in Early Childhood Education.
                           For more information, see below:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/OCPSChildDevAssociatearticulation8-24-15signedfinal.pdf">Child Development Associate (CDA) Certification and Valencia's Early Childhood Education
                                 A.A. Degree</a></li>
                           
                        </ul>
                        	
                        	
                        <hr class="styled_2">
                        
                        
                        <h4>Credit for TECO (Technical Education Center Osceola) Programs:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/credit_teco.php">Agreements with Technical Education Center Osceola (TECO)</a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for Lake Technical&nbsp;College Programs:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/IndustrialManagement-LakeTech-2013.pdf">Supervision and Management for Industry A.S. Degree</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/Lake-CulinaryManagement-2014.pdf">Commercial Foods and Valencia's Culinary Management A.S. Degree </a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for Lakeland Regional Health's Radiologic Technology Program:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-Radiography-Lakeland_Regional_Hospital_02-15-17.pdf">Valencia's Radiography A.S. Degree</a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for Academy of Construction Technologies (ACT) Programs:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-BuildingConstructionandACT-2017.pdf">Valencia's Building Construction Technology A.S. Degree </a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for courses from LeCordon Bleu:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/LeCordonBleuagreement.pdf">Valencia's Culinary Management A.S. Degree </a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Credit for Second Harvest Food Bank's Culinary Program:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-Culinary-SecondHarvestFoodBank-2015-16.pdf">Valencia's Culinary Management A.S. Degree </a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>OCPS High School Credit (Career Pathways/Tech Prep):</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/career-pathways/index.php">Career Pathways Credit </a></li>
                           
                        </ul>
                        
                        <p>If you have completed OCPS High School's Secondary Early Childhood Program, you may
                           be eligible to receive credit toward Valencia's A.A. Degree in Early Childhood Education.
                           For more information, see below:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/OCPSSecEarlyChildhoodProgarticulation8-24-15signedfinal.pdf">Secondary Early Childhood Education/Valencia's Early Childhood Education A.A. Degree
                                 </a></li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Pathways to Bachelor’s Degree Programs:</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/credit_private.php">Agreements with Private Universities</a></li>
                           
                           <li><a href="/academics/programs/as-degree/credit_public.php">Agreements with Public Universities</a></li>
                           
                           <li><a href="http://www.fldoe.org/core/fileparse.php/7525/urlt/astobaccalaureate_agreemnts.pdf" target="_blank">Statewide A.S. to B.S. Articulation Agreement</a>&nbsp;
                           </li>
                           
                        </ul>
                        	
                        <hr class="styled_2">
                        
                        <h4>Veterinary Technology - Cooperative Agreement with St. Petersburg College:</h4>
                        
                        <p>Through this agreement, you may take the required general education courses for this
                           program at Valencia, and take the remaining specialized courses through St. Petersburg
                           College online. The required clinical and work experience can be obtained within the
                           Central Florida area.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/vettech-2012.pdf">Veterinary Technology A.S. Degree Cooperative Agreement with SPCC</a><br>For more information on this Agreement, please call 727-302-6721 or visit St. Petersburg
                              Community College's web site at: <a href="http://www.spcollege.edu/courses/program/VETTC-AS">www.spj.edu/program/VETTC-AS</a>.
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
               </div>
               		
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/transfer-agreements.pcf">©</a>
      </div>
   </body>
</html>