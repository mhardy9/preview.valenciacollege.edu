<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Technology | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/information-technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Information Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Information Technology</h2> 
                           
                           <p>(Meta-Major: Science, Technology, Engineering  Math)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/computer-information-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Computer Information Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Computer Information Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Computer Information Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's Computer Information Technology degree program takes a well-rounded approach
                                       and gives you the skills to become a support technician who provides hands-on technical
                                       assistance such as computer repair and software installation, or a computer security
                                       specialist who provides systems support.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/computer-information-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/computer-programming-and-analysis/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Computer Programming and Analysis">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Computer Programming and Analysis 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Computer Programming and Analysis<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Students in this program develop skills in computer programming and analysis, while
                                       enjoying hands-on training in the foundations of computer technology and software
                                       development. Elective courses allow students to enhance their skill sets with advanced
                                       programming and Web development.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/computer-programming-and-analysis/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     	 
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/information-technology.pcf">©</a>
      </div>
   </body>
</html>