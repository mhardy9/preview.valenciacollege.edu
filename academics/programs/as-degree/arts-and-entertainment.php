<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Arts and Entertainment  | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/arts-and-entertainment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associates in Science Degree</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Arts and Entertainment</h2> 
                           
                           <p>(Meta-Major: Arts, Humanities, Communication &amp; Design)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/digital-media-technology/"> <img src="/_resources/images/academics/programs/arts-entertainment/digital-media.png" alt="Digital Media Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Digital Media Technology
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Digital Media Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>This program will train you in one of the hottest emerging career fields today—working
                                       in video production, motion graphics, mobile journalism or web development. You’ll
                                       learn to create persuasive, educational, informational and entertainment-based video,
                                       audio and motion graphic content for use in multimedia, web, mobile devices, broadcast
                                       and live events.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/digital-media-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/entertainment-design-and-technology/"> <img src="/_resources/images/academics/programs/arts-entertainment/entertainment-design.png" alt="Entertainment Design Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Entertainment Design Technology
                                             
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Entertainment Design Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>
                                       This program is one of only a handful in the country, and the only one in Central
                                       Florida that trains students to work in all facets of the entertainment industry.
                                       From designing and constructing scenery to rigging and operating lighting and sound
                                       equipment, you’ll gain hands-on experience working on real Valencia productions.
                                       
                                    </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/entertainment-design-and-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        		
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/film-production-technology/"> <img src="/_resources/images/academics/programs/arts-entertainment/film.png" alt="Film Production Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Film Production Technology
                                             
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Film Production Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>The Film Production Technology Associate in Science (A.S.) degree at Valencia College
                                       is a two-year program that prepares you to go directly into a specialized career in
                                       the film industry. Nationally recognized for making students employable, the program
                                       provides training for entry-level positions in six major areas of film production.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/film-production-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/sound-and-music-technology/"> <img src="/_resources/images/academics/programs/arts-entertainment/music-technology.png" alt="Sound and Music Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Sound and Music Technology
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Sound and Music Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>You will study studio and live sound techniques, post-production and audio engineering,
                                       as well as music business management, allowing you to develop the technical skills
                                       and aesthetic judgment to handle both the artistic and technical demands of music
                                       production.
                                    </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/sound-and-music-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        		
                     </div>
                     				
                  </div>
                  		
               </div>
               		
               
               	
               
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/arts-and-entertainment.pcf">©</a>
      </div>
   </body>
</html>