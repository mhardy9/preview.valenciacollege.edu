<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/credit_private.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Articulation Agreements for Valencia's A.S. Degrees transferring into Bachelors Degrees
                           with Private Universities:&nbsp;
                        </h2>
                        
                        <p>These agreements between Valencia and specific programs at various private universities
                           provide students with opportunities to continue their educational goals and transfer
                           credits from their A.S. degree toward a bachelor's degree.
                        </p>
                        <strong>Capella University:</strong>
                        
                        <p>This agreement makes it possible for any student from Valencia with an A.A. degree
                           to transfer into the Baccalaureate Degree program at Capella University, (Minneapolis,
                           MN). Also, Capella University will evaluate all A.S. degrees granted by Valencia and
                           transfer courses that meet its degree requirements on a course-by-course basis, but
                           it will take the general education core as a block.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/CapellaUniversityAgreement.pdf">Capella University Articulation Agreement</a></li>
                           
                        </ul>
                        <strong>DeVry University:</strong>
                        
                        <p>This agreement makes it possible for students with the follwoing A.S. Degrees from
                           Valencia to transfer into the related Bachelor's Degree programs at DeVry University,
                           in Orlando Florida:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Accounting-ASRevised2.6.06.pdf" target="_blank"> Accounting Technology A.S./Business Administration B.S.</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/BusinessAdministration-ASRevised2.6.06.pdf" target="_blank"> Business Administration A.S./Business Administration B.S.</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerInformation-AS11.1.06.pdf" target="_blank"> Computer Information Technology A.S./Network &amp; Communications Mgmt. B.S. </a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/ComputerProgramming-AS11.1.06.pdf" target="_blank"> Computer Programming &amp; Analysis A.S./Computer Information Systems B.S. </a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/DeVryArticulationAgreementRevised11.1.06.pdf"> Summary Agreement of Articulated Programs</a></li>
                           
                        </ul>
                        
                        <p><em>For more information on these agreements, please contact the Admissions Office at
                              DeVry University/Central Florida located at 4000 Millenia Blvd, Orlando, Florida (407)
                              370-3131.</em></p>
                        <strong>Kaplan University:</strong>
                        
                        <p>This agreement makes it possible for any student from Valencia with an A.S. degree
                           or an A.A. degree to transfer into Kaplan's baccalaureate program. Students who have
                           completed a minimum of 60 semester hours will be eligible for a block transfer of
                           up to 90 quarter credits into Kaplan's "advanced start" baccalaureate option.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/KaplanUniversity.pdf">AS/AA to Bachelor's Degree Articulation Agreement</a></li>
                           
                        </ul>
                        <strong>Montreat College:</strong>
                        
                        <p>This agreement makes it possible for students with the follwoing A.S. Degrees from
                           Valencia to transfer into the related Bachelor's Degree programs from Montreat College,
                           in North Carolina:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/MontreatArticulationAgreement.pdf">Montreat College Articulation Agreement </a></li>
                           
                        </ul>
                        
                        <p><strong>Associate in Science Degrees in: </strong></p>
                        
                        <ul>
                           
                           <li>Accounting Technology A.S. Degree</li>
                           
                           <li>Business Administration&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Computer Information Technology&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Computer Programming &amp; Analysis&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Criminal Justice Technology&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Hospitality and Tourism Management&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Paralegal Studies&nbsp;<span>A.S. Degree</span></li>
                           
                        </ul>
                        <strong>Strayer University:</strong>
                        
                        <p>This agreement makes it possible for students with the following A.S. Degrees from
                           Valencia to transfer into the related Bachelor's Degree programs from Strayer University:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Strayer-University-Valencia-Articulation_1.4.07.pdf">Strayer University Articulation Agreement </a></li>
                           
                        </ul>
                        
                        <p><strong>Associate in Science Degrees in: </strong></p>
                        
                        <ul>
                           
                           <li>Accounting Technology&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Business Administration&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Computer Information Technology&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Hospitality and Tourism Management&nbsp;<span>A.S. Degree</span></li>
                           
                           <li>Computer Programming &amp; Analysis&nbsp;<span>A.S. Degree</span></li>
                           
                        </ul>
                        <strong>University of Phoenix:</strong>
                        
                        <p>This agreement makes it possible for students with the following A.S. Degrees from
                           Valencia to transfer into the related Bachelor's Degree programs from&nbsp;the University
                           of Phoenix:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/Art-UniversityofPhoneix.pdf">University of Phoenix Articulation Agreement </a></li>
                           
                        </ul>
                        
                        <p><strong>Associate in Science Degrees in: </strong></p>
                        
                        <ul>
                           
                           <li>Criminal Justice Technology AS Degree - to BS in Org Security &amp; Management</li>
                           
                           <li>Criminal Justice Technology AS Degree - to BS in CJA Management</li>
                           
                           <li>Criminal Justice Technology AS Degree - to BSIT in Information Systems Security</li>
                           
                           <li>Health Information Technology AS Degree - to BSHA</li>
                           
                           <li>Health Information Technology AS Degree - to to BSM</li>
                           
                           <li>Nursing AS Degree - to BSN</li>
                           
                           <li><a href="/academics/programs/as-degree/documents/PTG-M-VC-ASBusinessAdministration-HumanResourcesMgt-BSBv26A.pdf">Business Administration AS Degree - to Human Resources Management BSB</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/PTG-M-VC-ASBusinessAdministration-Marketing-BSBv26A.pdf">Business Administration AS Degree - to Marketing BSB</a></li>
                           
                           <li><a href="/academics/programs/as-degree/documents/PTG-M-VC-ASCriminalJustice-HomelandSecurity-BSSEC01A.pdf">Criminal Justice Technology AS Degree - Homeland Security BS SEC </a></li>
                           
                        </ul>
                        <strong>Webster University:</strong>
                        
                        <p>This agreement makes it possible for any student from Valencia with an A.S. degree
                           or an A.A. degree to transfer into the General Business or Computer Science Bachelor's
                           Degree at Webster University, Orlando Metropolitan Campus.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/WebsterUniversity.pdf">AS/AA to Bachelor's Degree Articulation Agreement</a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/credit_private.pcf">©</a>
      </div>
   </body>
</html>