<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hospitality &amp; Culinary | Valencia College</title>
      <meta name="Description" content="A.S. Degree">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/hospitality-and-culinary.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Hospitality &amp; Culinary</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Hospitality &amp; Culinary</h2> 
                           
                           <p>(Meta-Major: Business)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/baking-and-pastry-management/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Baking and Pastry Management">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Baking and Pastry Management 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Baking and Pastry Management<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Students are taught by some of the best chefs in the world, including instructors
                                       from Walt Disney World and Universal Orlando. The only program of its kind in Florida,
                                       Valencia's Baking and Pastry Management program gives you the extraordinary advantage
                                       of starting an exciting culinary career in one of the most acclaimed tourist destinations
                                       in the world.
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/baking-and-pastry-management/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/culinary-management/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Culinary Management">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Culinary Management 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Culinary Management<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's Program provides you with some of the best chefs in the industry as your
                                       teachers, and takes a well-rounded approach, with instruction ranging from sanitation
                                       to nutritional analysis, to the basic elements for great classical, international
                                       and American cuisine. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/culinary-management/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/hospitality-and-tourism-management/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Hospitality and Tourism Management">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Hospitality and Tourism Management 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Hospitality and Tourism Management<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's program combines classroom instruction with on-the-job training to help
                                       you move into supervisory and mid-management positions in lodging, food service, tourism
                                       and hospitality. One of the most valuable aspects of the program is the 240-hour internship,
                                       which enables you to develop skills and expertise in guest services and management,
                                       as well as technical areas.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/hospitality-and-tourism-management/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/restaurant-and-food-service-management/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Restaurant and Food Service Management">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Restaurant and Food Service Management 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Restaurant and Food Service Management<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's program includes training on the most up-to-date technology in the restaurant
                                       and food service industry as well as classroom instruction and on-the-job training
                                       from some of the area's leaders in the restaurant field.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/restaurant-and-food-service-management/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     	 
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/hospitality-and-culinary.pcf">©</a>
      </div>
   </body>
</html>