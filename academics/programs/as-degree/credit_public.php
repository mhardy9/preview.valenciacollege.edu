<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/credit_public.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Career Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Public </h2>	
                        
                        <p>Articulation Agreements for Valencia's A.S. Degrees transferring into<span>&nbsp;</span>Bachelors Degrees with Public Universities:&nbsp;
                        </p>
                        
                        <p>These agreements between Valencia and specific programs at various public universities
                           provide students with opportunities to continue their educational goals and transfer
                           credits from their A.S. degree toward a bachelor's degree.
                        </p>
                        
                        <h3><strong>Western Kentucky University</strong></h3>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/WesternKentuckyUniv-ValenciaCITArticagreementfinalsigned1-31-17.pdf">Computer Information Technology A.S./Computer Information Technology B.S.</a><br> <em>For more information on this Agreement, please contact Valencia's Program Chair at
                                 407-582-2360.</em></li>
                           
                        </ul>
                        
                        <hr class="styled_2">	
                        
                        <h3>University of Central Florida (UCF)</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/programs/as-degree/documents/ParalegalStudiestoUCFLegalStudies3.21.06.pdf">Paralegal Studies A.S./Legal Studies B.A./B.S.</a><br> <em>For more information on this Agreement, please contact Valencia's Program Director
                                 at 407-582-2556.</em></li>
                           
                        </ul>
                        
                        <h4><strong>A.S. to B.S. Statewide Articulation Agreements:</strong></h4>
                        
                        <ul>
                           
                           <li><a href="http://www.fldoe.org/core/fileparse.php/7525/urlt/astobaccalaureate_agreemnts.pdf" target="_blank">Statewide A.S. to B.S. Articulation Agreement</a>&nbsp;
                           </li>
                           
                        </ul>
                        
                        <p>Valencia has four A.S. to B.S. Degrees:</p>
                        
                        <ul>
                           
                           <li>Business Administration A.S. Degree</li>
                           
                           <li>Criminal Justice Technology A.S. Degree</li>
                           
                           <li>Hospitality and Tourism Management A.S. Degree</li>
                           
                           <li>Nursing, R.N. A.S. Degree</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/credit_public.pcf">©</a>
      </div>
   </body>
</html>