<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Public Safety &amp; Legal | Valencia College</title>
      <meta name="Description" content="A.S. Degree">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/public-safety-and-legal.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associate in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Public Safety &amp; Legal</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Public Safety &amp; Legal</h2> 
                           
                           <p>(Meta-Major: Public Safety)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/criminal-justice/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Criminal Justice">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Criminal Justice 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Criminal Justice<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's Criminal Justice program is designed to provide the best possible education
                                       for students preparing for challenging careers in law enforcement, corrections, private/industry
                                       security and other criminal justice fields. It offers a broad background in the history
                                       and philosophy of criminal justice; the management and operation of modern criminal
                                       justice agencies.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/criminal-justice/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/fire-science-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Fire Science Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Fire Science Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Fire Science Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's program can also be useful for careers in other public service areas or
                                       to provide current fire fighters and staff with enhanced training and education opportunities
                                       for promotion and career advancement.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/fire-science-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/academics/programs/public-safety/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Fire Science Academy Track">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Fire Science Academy Track
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Fire Science Academy Track<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>The Fire Science Academy is a degree-granting program that provides students with
                                       the skills, abilities and education for a successful fire service career. It provides
                                       a strong educational foundation in fire science coupled with training and education
                                       in emergency medical services and firefighting.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/academics/programs/public-safety/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/future-students/degree-options/associates/paralegal-studies/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Paralegal Studies">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Paralegal Studies
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Paralegal Studies<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Students will develop a strong background in areas such as civil litigation, real
                                       property, business organizations, legal research and legal technology. Paralegal students
                                       will also gain an understanding of the ethical framework within which they work and
                                       be able to effectively analyze and communicate in these areas.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/future-students/degree-options/associates/paralegal-studies/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     	 
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/public-safety-and-legal.pcf">©</a>
      </div>
   </body>
</html>