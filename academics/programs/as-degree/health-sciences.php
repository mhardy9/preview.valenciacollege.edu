<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Health Sciences | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/health-sciences.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associates in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Health Sciences</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Health Sciences</h2> 
                           
                           <p>(Meta-Major: Health Sciences)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/biotechnology-laboratory-sciences/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Biotechnology Laboratory Sciences">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Biotechnology Laboratory Sciences 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Biotechnology Laboratory Sciences<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Through Valencia's program, you'll learn the fundamentals of biotechnology while gaining
                                       hands-on experience through lab work and internship experience. The program emphasizes
                                       broad biology and chemistry concepts, basic microbiology, algebraic and statistical
                                       analysis, biohazard and safety procedures and core biotechnical laboratory techniques.
                                       College-level math is required.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/biotechnology-laboratory-sciences/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/future-students/degree-options/associates/cardiovascular-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Cardiovascular Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Cardiovascular Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Cardiovascular Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>As a cardiovascular technologist, you would work side-by-side with cardiologists and
                                       physicians to diagnose and treat heart disease and vascular problems. You would also
                                       serve as an integral member of the cardiac catheterization laboratory team.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/cardiovascular-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/dental-hygiene/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Dental Hygiene">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Dental Hygiene 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Dental Hygiene<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's dental hygiene program prepares you to become licensed as a primary oral
                                       health care professional and is dedicated to helping you develop the specific skills
                                       you need to ensure that the highest quality of dental care is provided for patients.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/dental-hygiene/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/diagnostic-medical-sonography/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Diagnostic Medical Sonography">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Diagnostic Medical Sonography 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Diagnostic Medical Sonography<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Sonographers play a dynamic role in the treatment and care of patients. When a doctor
                                       needs to study any of the soft tissue organs of a patient—like the liver, gallbladder,
                                       kidneys, thyroid or breast—it is the sonographer who provides the images using high
                                       frequency sound waves via ultrasound. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/diagnostic-medical-sonography/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/emergency-medical-services-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Emergency Medical Services Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Emergency Medical Services Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Emergency Medical Services Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>EMTs and paramedics assess injuries, administer advanced emergency medical care and
                                       transport injured or ill people to medical facilities. Valencia's program teaches
                                       theory and clinical applications, and provides hands-on field internships to assure
                                       that students gain the skills necessary to offer the most extensive emergency life
                                       support services available.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/emergency-medical-services-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/health-information-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Health Information Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Health Information Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Health Information Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Health information professionals play a critical role in health care—even though they
                                       rarely interact with patients. Every time you hear a health statistic on the news,
                                       read a doctor's diagnosis in your medical record or use your health insurance card,
                                       you can be sure a medical record technician has played a part. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/health-information-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/nursing/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Nursing">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Nursing 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Nursing<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's Nursing program is limited-access, meaning that admission to the college
                                       does not imply acceptance to the Nursing program. Students must apply and be accepted
                                       to the program. Upon completion of the program, you will be prepared to take the National
                                       Council Licensure Exam to become a registered nurse (RN).
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/nursing/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/radiography/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Radiography">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Radiography 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Radiography<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Radiographers operate high-tech imaging equipment and perform diagnostic imaging examinations
                                       such as X-ray's, CAT scans, MRI’s and mammograms, that help doctors accurately diagnose
                                       and treat their patients for injury and disease. Valencia’s A.S. degree in Radiography
                                       prepares students to become highly skilled professionals in the safe and effective
                                       use of radiation to treat patients.
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/radiography/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/veterinary-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Veterinary Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Veterinary Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Veterinary Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia offers the general education core and support courses required in the program.
                                       Students meeting the admission requirements of this program at St. Petersburg College
                                       can access all of the remaining specialized courses through online distance learning.
                                       
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/veterinary-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     	 
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/health-sciences.pcf">©</a>
      </div>
   </body>
</html>