<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Engineering and Technology | Valencia College</title>
      <meta name="Description" content="Valencia's Associate in Science (A.S.) degrees are two-year programs that are designed to prepare students for immediate employment in a specialized career.">
      <meta name="Keywords" content="as degree,  associates in science,  degree program,  certificate programs, career programs, a.s. degree, valenciacolleged.edu/programes/as-degree, valencia college, as degree florida, orlando as degree,  central flroida as degree">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/engineering-and-technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Associates in Science Degree</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/as-degree/">AS Degree</a></li>
               <li>Engineering and Technology</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div class="main-title">
                           
                           <h2>Engineering and Technology</h2> 
                           
                           <p>(Meta-Major: Science, Technology, Engineering &amp; Math)</p>
                           
                        </div>
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/building-construction-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Building Construction Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Building Construction Technology
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Building Construction Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>You'll learn how to study building plans, estimate materials and labor, obtain building
                                       permits and licenses, and oversee the work of other employees. Valencia's program
                                       provides up-to-the-minute courses in the latest construction methods, materials and
                                       technology so that you'll be prepared to work with the best architects, engineers,
                                       contractors and building officials in the business
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/building-construction-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/drafting-and-design-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt="Drafting and Design Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Drafting and Design Technology
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3>Drafting and Design Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia’s program prepares you for this challenging field utilizing state-of-the-art
                                       drafting and design techniques with the latest in computer-aided drafting equipment,
                                       including 3-D printing, 3-D modeling, AutoCAD and GIS technology. With these skills,
                                       you’ll be prepared to play a vital role in the engineering process.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/drafting-and-design-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/civil-surveying-engineering-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Civil Surveying Engineering Technology ">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Civil Surveying Engineering Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Civil Surveying Engineering Technology <br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia's program, you’ll learn the skills that are desired by industry, as well
                                       as the latest software being used, to become a qualified technician in the civil/surveying
                                       engineering field. With Central Florida as one of the major areas of growth, this
                                       program offers many exciting career opportunities.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/civil-surveying-engineering-technology/" class="button-outline">Details</a>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/electronics-engineering-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Electronics Engineering Technology ">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Electronics Engineering Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Electronics Engineering Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>Valencia’s innovative program transforms students into highly skilled technicians
                                       capable of assisting in the design, production, operation and servicing of electronics,
                                       lasers and photonics, robotics and mechatronics, and telecommunication and wireless
                                       systems.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/electronics-engineering-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        	
                        	
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/graphic-and-interactive-design/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Graphic and Interactive Design">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Graphic and Interactive Design 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Graphic and Interactive Design<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>With a trusted reputation in the Orlando creative industry, we pride ourselves in
                                       educating some of the most talented designers in Central Florida. Our students have
                                       received recognition in local, regional, national and even international competitions.
                                       Their accomplishments include over 100 ADDY Awards, Florida Print Awards, the Create
                                       Awards, the ONE Show, Print Magazine and Siggraph.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/graphic-and-interactive-design/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                           
                           <div class="row">
                              
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                 
                                 <div class="img_list">
                                    <a href="/students/future-students/degree-options/associates/network-engineering-technology/"> <img src="/images/academics/programs/health-sciences/nursing-bedside.jpg" alt=" Network Engineering Technology">
                                       
                                       <div class="short_info">
                                          
                                          <h3>
                                             Network Engineering Technology 
                                          </h3>
                                          
                                       </div>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="clearfix visible-xs-block">
                                 
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                 
                                 <div class="course_list_desc">
                                    
                                    <h3> Network Engineering Technology<br>
                                       Associate in Science Degree
                                       
                                    </h3>
                                    
                                    <p>With Valencia's program, you will be trained to design, administer, maintain and support
                                       local and wide area network systems. Graduates will be qualified for exciting technological
                                       roles in network design and installation, infrastructure, security and maintenance,
                                       incident response, inter-network communications, network monitoring and administration,
                                       and cyber security.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-lg-2 col-md-2 col-sm-2">
                                 
                                 <div class="details_list_col">
                                    
                                    <div>
                                       <a href="/students/future-students/degree-options/associates/network-engineering-technology/" class="button-outline">Details</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     	 
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/engineering-and-technology.pcf">©</a>
      </div>
   </body>
</html>