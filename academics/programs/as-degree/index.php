<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/as-degree/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/as-degree/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>AS Degree</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Associate in Science (A.S.) Degree and Certificate Programs</h2>
                        
                        <p>Jump right into the job market with one of Valencia's exciting career programs. If
                           you are thinking about entering the workforce soon, Valencia has a program for you!
                           We offer more than 120 career programs, including 34 Associate in Science degrees
                           and 85 certificate programs, to prepare you to go directly into a specialized career.
                           Whether it's nursing, cyber security, software engineering, digital media or a host
                           of other high-demand occupations, Valencia has a degree or certificate program to
                           meet your career goals.
                        </p>
                        
                        <h3 style="color: #bf311a;">Associate in Science (A.S.) Degree Programs</h3>
                        
                        <p>Valencia's Associate in Science (A.S.) degree programs are two-year programs that
                           prepare you for immediate employment in a specialized career. They are all geared
                           toward exciting industries, and are designed to give you the best possible preparation
                           for the fastest-growing jobs in the region. These programs are developed under the
                           guidance of top business and industry employers who know the skills you need to succeed
                           in the real world. In addition, each A.S. degree program has its own Career Program
                           Advisor who has expertise in the field and can help ensure that you get into the right
                           program and the right classes once you are enrolled at Valencia.
                        </p>
                        
                        <h3 style="color: #bf311a;">College Credit Certificate Programs</h3>
                        
                        <p>Valencia's Certificate programs can put you on the fast-track to reaching your career
                           goals. They are shorter in length and most can be completed in one year or less. They
                           are designed to equip you with a specialized skill set for entry-level employment,
                           or to upgrade your skills for job advancement. The Certificates are building blocks
                           of success that help you progress through your A.S. degree.
                        </p>
                        
                        <p>Information about the certificate programs can be found on the linked A.S. degree
                           web pages below.
                        </p>
                        
                        <hr>
                        
                        <h3>Arts &amp; Entertainment</h3>
                        
                        <p>(Meta-Major: Arts, Humanities, Communication &amp; Design)</p>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/arts/dmg.cfm">Digital Media Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Digital Broadcast Production Certificate -&nbsp;<em>(24 Credits) </em></li>
                           
                           <li>Digital Media Development Certificate -&nbsp;<em>(12 Credits) </em></li>
                           
                           <li>Digital Media Video Production&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Digital Media Web Production&nbsp;Certificate<em>&nbsp;- (15 Credits)</em></li>
                           
                           <li>Digital Video Editing &amp; Post-Production&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Digital Video Fundamentals&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Webcast Media&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Webcast Technology&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/arts/ed.cfm">Entertainment Design &amp; Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Entertainment - Stage Technology Certificate -&nbsp;<em>(17 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/arts/fpt.cfm">Film Production Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Film Production Fundamentals&nbsp;Certificate&nbsp;-&nbsp;<em>24 Credits)</em></li>
                           
                        </ul>
                        
                        <h4>Graphic &amp; Interactive Design *see Engineering and Technology below</h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/arts/mpt.cfm">Sound &amp; Music Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Audio Electronics Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits) </em></li>
                           
                           <li>Audio Visual Production&nbsp;Certificate&nbsp;-&nbsp;<em>(15 Credits)</em></li>
                           
                           <li>Digital Music Production&nbsp;Certificate -&nbsp;<em>(12 Credits) </em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Business</h3>
                        
                        <p>(Meta-Major: Business)</p>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/business/at.cfm">Accounting Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Accounting Applications&nbsp;Certificate&nbsp;-&nbsp;<em>(27 Credits)</em></li>
                           
                           <li>Accounting Operations&nbsp;Certificate&nbsp;-&nbsp;<em>(18 Credits)</em></li>
                           
                           <li>Accounting Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/business/ba.cfm">Business Administration (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Business Management&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Business Operations&nbsp;Certificate&nbsp;-&nbsp;<em>(18 Credits)</em></li>
                           
                           <li>Business Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>12 Credits)</em></li>
                           
                           <li>Customer Service Management&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Customer Services Operations&nbsp;Certificate&nbsp;-&nbsp;<em>(18 Credits)</em></li>
                           
                           <li>Customer Service Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Entrepreneurship&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Human Resources Management&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Human Resources Operations&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Human Resources Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>International Business Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                           <li>Operations Support and Services&nbsp;Certificate&nbsp;-&nbsp;<em>9 Credits)</em></li>
                           
                           <li>Real Estate Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/business/moa.cfm">Medical Office Administration (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Medical Office Management&nbsp;Certificate&nbsp;- <em> (34 Credits) </em></li>
                           
                           <li>Medical Office Specialist&nbsp;Certificate&nbsp;- <em> (18 Credits)</em></li>
                           
                           <li>Medical Office Support&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/business/oa.cfm">Office Administration (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Office Management&nbsp;Certificate&nbsp;- <em> (27 Credits)</em></li>
                           
                           <li>Office Specialist&nbsp;Certificate&nbsp;- <em>(18 Credits)</em></li>
                           
                           <li>Office Support&nbsp;Certificate&nbsp;- <em>(12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="/future-students/degree-options/associates/residential-property-management/?_ga=2.241525442.1168185288.1515422653-1017967466.1424809152">Residential Property Management (A.S. Degree)</a></h4>
                        
                        <h4><a href="/future-students/degree-options/associates/supervision-and-management-for-industry/">Supervision and Management for Industry (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Operations Support and Services&nbsp;Certificate&nbsp;- <em>(9 Credits)</em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Engineering and Technology</h3>
                        
                        <p>(Meta-Major: Science, Technology, Engineering &amp; Math)</p>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/engineering/bct.cfm">Building Construction Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Building Construction Specialist&nbsp;Certificate&nbsp;- <em> (18 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/engineering/ddt.cfm">Drafting &amp; Design Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Drafting&nbsp;Certificate&nbsp;- <em>(24 Credits) </em></li>
                           
                           <li>Drafting - AutoCAD&nbsp;Certificate&nbsp;- <em>(15 Credits)</em></li>
                           
                           <li>Rapid Prototyping Specialist&nbsp;Certificate&nbsp;-<em> (12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/engineering/cset.cfm">Civil/Surveying Engineering Technology (A.S. Degree)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/engineering/eet.cfm">Electronics Engineering Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Advanced Electronics Technician&nbsp;Certificate&nbsp;- (<em>31 Credits)</em></li>
                           
                           <li>Basic Electronics Technician&nbsp;Certificate&nbsp;- <em> (14 Credits)</em></li>
                           
                           <li>Laser and Photonics Technician&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                           <li>Robotics and Simulation Technician&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="/future-students/degree-options/associates/energy-management-and-controls-technology/?_ga=2.145712372.1168185288.1515422653-1017967466.1424809152">Energy Management &amp; Controls Technology (A.S. Degree)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/arts/gt.cfm">Graphic &amp; Interactive Design (A.S. Degree)</a></h4>
                        
                        <p>(Meta-Major:&nbsp;Science, Technology, Engineering &amp; Math)</p>
                        
                        <ul>
                           
                           <li>Graphic Design Production&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Graphic Design Support&nbsp;Certificate&nbsp;-&nbsp;<em>(15 Credits)</em></li>
                           
                           <li>Interactive Design Production&nbsp;Certificate&nbsp;-&nbsp;<em>(24 Credits)</em></li>
                           
                           <li>Interactive Design Support&nbsp;Certificate&nbsp;-&nbsp;<em>(15 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/network-engineering-technology/">Network Engineering Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Advanced Network Administration&nbsp;Certificate&nbsp;- <em> (29 Credits)</em></li>
                           
                           <li>Advanced Network Infrastructure&nbsp;Certificate&nbsp;-<em> (28 Credits)</em></li>
                           
                           <li>Cyber Security&nbsp;Certificate&nbsp;- <em>(30 Credits)</em></li>
                           
                           <li>Digital Forensics&nbsp;Certificate&nbsp;-<em> (32 Credits)</em></li>
                           
                           <li>Network Administration&nbsp;Certificate&nbsp;- <em> (24 Credits)</em></li>
                           
                           <li>Network Infrastructure&nbsp;Certificate&nbsp;- <em>(21 Credits)</em></li>
                           
                           <li>Network Support&nbsp;Certificate&nbsp;- <em> (21 Credits)</em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Health Sciences</h3>
                        
                        <p>(Meta-Major: Health Sciences)</p>
                        
                        <h4><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/biotechnology-laboratory-sciences/">Biotechnology Laboratory Sciences (A.S. Degree)</a></h4>
                        
                        <p>(Meta-Major:&nbsp;Science, Technology, Engineering &amp; Math)</p>
                        
                        <ul>
                           
                           <li>Biotechnology Laboratory Specialist&nbsp;Certificate&nbsp;-&nbsp;<em>(21 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/ct.cfm">Cardiovascular Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Echocardiography -&nbsp;Advanced Certificate <em>(18 credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/dh.cfm">Dental Hygiene (A.S. Degree)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/dms.cfm">Diagnostic Medical Sonography (A.S. Degree)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/emst.cfm">Emergency Medical Services Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>EMT&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                           <li>Paramedic Technology&nbsp;Certificate&nbsp;- <em> (42 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/health-information-technology.cfm">Health Information Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Medical Coder Biller&nbsp;Certificate&nbsp;-&nbsp;<em>(37 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/nursing/nrn.cfm">Nursing, Traditional Track&nbsp;(A.S. Degree)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/nursing/nrn.cfm">Nursing, Accelerated Track&nbsp;(ATN)</a></h4>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/rad.cfm">Radiography (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Computed Tomography (CT) - Advanced Certificate -&nbsp;<em>(9 Credits)</em></li>
                           
                           <li>Magnetic Resonance Imaging (MRI) - Advanced Certificate<em>&nbsp;(12 Credits) </em></li>
                           
                           <li>Mammography<em>&nbsp;- </em>Advanced Certificate<em>&nbsp;(12 Credits) </em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/health/rc.cfm">Respiratory Care (A.S. Degree)</a></h4>
                        
                        <h4><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/alliedhealth/veterinarytechnology/#text%23text">Veterinary Technology (A.S. Degree)</a></h4>
                        
                        <p><em>(Offered through a Cooperative Agreement with St. Petersburg College)</em></p>
                        
                        <hr>
                        
                        <h3>Hospitality &amp; Culinary</h3>
                        
                        <p>(Meta-Major: Business)</p>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/hospitality/bpm.cfm">Baking &amp; Pastry Management (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Baking &amp; Pastry Arts&nbsp;Certificate&nbsp;- <em> (35 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/hospitality/cm.cfm">Culinary Management (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Chef's Apprentice&nbsp;Certificate&nbsp;- <em>(12 Credits) </em></li>
                           
                           <li>Culinary Arts Management&nbsp;Certificate&nbsp;- <em>(18 Credits)</em></li>
                           
                           <li>Culinary Arts&nbsp;Certificate&nbsp;- <em>(35 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/hospitality/htm.cfm">Hospitality &amp; Tourism Management (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Event Planning Management&nbsp;Certificate&nbsp;- <em> (24 Credits)</em></li>
                           
                           <li>Guest Service Specialist&nbsp;Certificate&nbsp;- <em> (15 Credits)</em></li>
                           
                           <li>Restaurant &amp; Food Service&nbsp;Management&nbsp;Certificate&nbsp;- <em>(30 Credits)</em></li>
                           
                           <li>Rooms Division&nbsp;Management&nbsp;Certificate&nbsp;-<em> <em> (30 Credits)</em> </em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/hospitality/rfsm.cfm">Restaurant &amp; Food Service Management (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Restaurant &amp; Food Service Management&nbsp;Certificate&nbsp;-<em> <em> (30 Credits)</em></em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Information Technology</h3>
                        
                        <p>(Meta-Major: Science, Technology, Engineering &amp; Math)</p>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/information-technology/cit.cfm">Computer Information Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Computer Information Data Specialist&nbsp;Certificate&nbsp;- <em>(9 Credits)</em></li>
                           
                           <li>Computer Information Analyst&nbsp;Certificate&nbsp;- <em>(27 Credits)</em></li>
                           
                           <li>Computer Information Specialist&nbsp;Certificate&nbsp;- <em>(18 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/information-technology/cpa.cfm">Computer Programming &amp; Analysis (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Computer Programming&nbsp;Certificate&nbsp;- <em>(33 Credits)</em></li>
                           
                           <li>Computer Programming Specialist&nbsp;Certificate&nbsp;- <em>(18 Credits)</em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Landscape &amp; Horticulture&nbsp;</h3>
                        
                        <p>(Meta-Major: Science, Technology, Engineering &amp; Math)</p>
                        
                        <h4><a href="/future-students/programs/plant-science-and-agricultural-technology/">Plant Science and Agricultural Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Landscape/Horticulture Professional&nbsp;Certificate&nbsp;-<em> (18 Credits)</em></li>
                           
                           <li>Landscape/Horticulture Specialist&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                           <li>Landscape/Horticulture Technician&nbsp;Certificate&nbsp;-<em> (30 Credits)</em></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Public Safety &amp;&nbsp;Legal</h3>
                        
                        <p>(Meta-Major: Public Safety)</p>
                        
                        <h4><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/criminaljusticeparalegalstudies/criminaljusticeinstitute/">Criminal Justice Institute Academy Programs</a></h4>
                        
                        <ul>
                           
                           <li>Auxiliary Law Enforcement Officer <em>(Career Certificate)</em></li>
                           
                           <li>Correctional Officer <em>(Career Certificate)</em></li>
                           
                           <li>Crossover: Corrections to Law Enforcement <em>(Career Certificate)</em></li>
                           
                           <li>Law Enforcement Officer <em>(Career Certificate)</em></li>
                           
                        </ul>
                        
                        <h4><a href="http://preview.valenciacollege.edu/future-students/degree-options/associates/criminal-justice/">Criminal Justice (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Criminal Justice Specialist&nbsp;Certificate&nbsp;-<em>(24 Credits)</em></li>
                           
                           <li>Homeland Security Specialist&nbsp;Certificate&nbsp;-<em> (9 Credits) </em></li>
                           
                           <li>Homeland Security - Law Enforcement Specialist&nbsp;Certificate&nbsp;-<em> (15 Credits) </em></li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/fire-services/fire-science-technology.cfm">Fire Science Technology (A.S. Degree)</a></h4>
                        
                        <ul>
                           
                           <li>Fire Officer Supervisor&nbsp;Certificate&nbsp;- <em> (12 Credits)</em></li>
                           
                        </ul>
                        
                        <h4><a href="/academics/programs/public-safety/fire-rescue-institute/academy-track.html">Fire Science Academy Track (A.S. Degree)<br> </a></h4>
                        
                        <ul>
                           
                           <li>Combined Program: (Fire Science A.S. Degree/EMT/Fire Fighter Minimum Standards)</li>
                           
                        </ul>
                        
                        <h4><a href="http://valenciacollege.edu/asdegrees/criminal-justice/ps.cfm">Paralegal Studies (A.S. Degree)</a></h4>
                        
                        <p>(Meta-Major:&nbsp;Business)</p>
                        
                        <div>&nbsp;</div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/as-degree/index.pcf">©</a>
      </div>
   </body>
</html>