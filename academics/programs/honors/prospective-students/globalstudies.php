<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/prospective-students/globalstudies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/prospective-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/prospective-students/">Prospective Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <h2>Global Studies  - Winter Park Campus</h2>
                        
                        <img alt="Suzette Dohany with students" height="249" src="/_resources/img/admissions/honors-program/prospective-students/SuzetteDohany.jpg" width="375" class="img-responsive pull-right">
                        
                        
                        <p>The <strong>Global Studies Track</strong> is designed to provide highly-motivated students a well-rounded general education
                           curriculum with thematically-integrated courses. At Winter Park, the focus is on creating
                           opportunities for students to become active, global citizens. Students will have the
                           opportunity to Skype with students at Koning Willem I College  and cap-off their coursework
                           with a study abroad trip to the Netherlands. Students completing this track will graduate
                           with both the<a href="/students/service-learning/index.html"> Service Learning</a> and <a href="/students/international/studyabroad/students/events/globaldistinction.html">Valencia Global Distinction</a> recognitions.
                        </p>
                        
                        <h3>Learning Outcomes</h3>
                        
                        <p>Students who complete the requirements of the Global Studies track will be able to:</p>
                        
                        <ul>
                           
                           <li>Articulate their values and how they fit into a global society</li>
                           
                           <li>Demonstrate an advocacy for change</li>
                           
                           <li>Explore multiple perspectives on an issue that impacts a global world</li>
                           
                           <li>Demonstrate an understanding to the ways in which people differ, including innate
                              characteristics
                           </li>
                           
                           <li>Demonstrate an understanding of how past events shape current realities</li>
                           
                           <li>Articulate a breadth of knowledge about nations and regions around the world, such
                              as their geographies, languages, religions, currencies, and cultures
                           </li>
                           
                           <li>Demonstrate the ability to communicate effectively and appropriately in intercultural
                              situations based on one’s intercultural knowledge, skills and attitudes
                           </li>
                           
                           <li>Demonstrate collaboration using a “win-win” approach that benefits all, within and
                              across perceived or actual cultural barriers
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Curricular Outline</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td colspan="3"><strong>Foundation Courses</strong></td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;1122H</a></td>
                                 
                                 <td>New Student Experience-Honors&nbsp;</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1101H</a></td>
                                 
                                 <td>FRESHMAN COMPOSITION I - HONORS&nbsp;* or </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC 1101</a></td>
                                 
                                 <td>FRESHMAN COMPOSITION I </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/hum/">HUM&nbsp;1020H</a></td>
                                 
                                 <td>INTRODUCTION TO HUMANITIES - HONORS&nbsp;</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;1501H</a></td>
                                 
                                 <td>HONORS SEMINAR</td>
                                 
                                 <td>1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Math</td>
                                 
                                 <td>See<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/"> Gen Ed</a> Core or Institutional Requirement&nbsp;*
                                 </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/spc/">SPC&nbsp;1017H</a></td>
                                 
                                 <td>INTERPERSONAL COMMUNICATION HONORS&nbsp;</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="3"><strong>Intermediate Courses</strong></td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/hum/">HUM 2250H</a></td>
                                 
                                 <td>TWENTIETH CENTURY - HONORS </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/enc/">ENC&nbsp;1102H</a></td>
                                 
                                 <td>FRESHMAN COMPOSITION II - HONORS&nbsp;*</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/ant/">ANT&nbsp;2000H</a></td>
                                 
                                 <td>INTRODUCTORY ANTHROPOLOGY HON&nbsp;</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Science</td>
                                 
                                 <td>See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Gen Ed</a> Core Course&nbsp;
                                 </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/sls/">SLS&nbsp;2940H</a></td>
                                 
                                 <td>SERVICE LEARNING</td>
                                 
                                 <td>2</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/inr">INR&nbsp;2002H</a></td>
                                 
                                 <td>INTERNATIONAL POLITICS&nbsp;*</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="3"><strong>Advanced Courses</strong></td>
                                 
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Math</td>
                                 
                                 <td>See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Gen Ed</a> Core or Institutional Requirement&nbsp;*
                                 </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Science</td>
                                 
                                 <td>See <a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Gen Ed</a> Core Course&nbsp;
                                 </td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://catalog.valenciacollege.edu/coursedescriptions/coursesoffered/idh/">IDH&nbsp;2028</a></td>
                                 
                                 <td>HONORS CAPSTONE&nbsp;*</td>
                                 
                                 <td>1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2">Electives&nbsp;*</td>
                                 
                                 <td>21</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td colspan="2"><strong>Total Credit Hours</strong></td>
                                 
                                 <td><strong>60</strong></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        The courses in the program will articulate to UCF’s International and Global Studies
                        degree if the student chooses POS 2041 instead of WOH 2003.
                        
                        
                        <h3>Co-curricular Component</h3>
                        
                        <p>Students will be required to participate in a minimum of 15 hours of approved co-curricular
                           activities each term of enrollment (excluding summer). This may include service learning,
                           short-term study abroad, mentoring, cultural field trips, civic projects, reading
                           groups, service to the Honors College, etc.
                        </p>            
                        
                        
                        <h3>See the Students in Action</h3>
                        
                        <p>Check out the <a href="https://youtu.be/S-lUXHjeFz4">trailer</a> produced after the Global Studies students made their first study abroad trip to
                           The Netherlands! 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/prospective-students/globalstudies.pcf">©</a>
      </div>
   </body>
</html>