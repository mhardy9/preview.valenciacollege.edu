<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions | Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Frequently Asked Questions | Seneff Honors College">
      <meta name="Keywords" content="college, school, educational, seneff, honors, faq, frequently, asked, quesitons">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/prospective-students/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/prospective-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>Prospective Students
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/prospective-students/">Prospective Students</a></li>
               <li>Frequently Asked Questions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>FAQs</h2>
                        
                        <h3>Frequently Asked Questions</h3>
                        
                        <ol>
                           
                           <li><a href="#Options"> What are the Honors options available to me?</a></li>
                           
                           <li><a href="#Apply">How do I apply for Honors?</a></li>
                           
                           <li><a href="#Processing">How long will it take for my Honors application to be processed?</a></li>
                           
                           <li>
                              <a href="#Scholarship"> Will I automatically get a scholarship if I am in Honors?</a> 
                           </li>
                           
                           <li><a href="#differences">What is different about honors classes?</a></li>
                           
                           <li><a href="#harder">Are honors classes harder?</a></li>
                           
                           <li><a href="#weighted">Are honors course grades weighted?</a></li>
                           
                           <li><a href="#dualenrolled">Can dual-enrolled students take honors classes?</a></li>
                           
                           <li><a href="#Percent">If I'm in Honors, do I have to register for all honors classes?</a></li>
                           
                           <li><a href="#Cost">Do Honors classes cost more?</a></li>
                           
                           <li><a href="#Fee">Is there an application fee to apply for the Seneff Honors College? </a></li>
                           
                           <li><a href="#ptk">Is Phi Theta Kappa (PTK) part of the Honors Program? </a></li>
                           
                           <li><a href="#hrc">What is an Honors Resource Center?</a></li>
                           
                           <li><a href="#officelocation">Where is the Honors office located? </a></li>
                           
                        </ol>
                        
                        
                        <h3> <a name="Options" id="Options"></a>What are the Honors options available to me? 
                        </h3>
                        
                        <p> Students who  take classes that have a curricular focus also have co-curricular requirements.&nbsp;
                           Students on curricular tracks can graduate as "honors scholars." There are 4 curricular
                           tracks: Global Studies, Interdisciplinary Studies, Leadership and Undergraduate Research.
                           
                        </p>
                        
                        <h3> <a name="Apply" id="Apply"></a>How do I apply for Honors? 
                        </h3>
                        
                        <p> The application for theSeneff Honors College and the application to take honors classes
                           can be found in Atlas on the Students tab under "Applications." 
                        </p>
                        
                        <h3> <a name="Processing" id="Processing"></a>How long will it take for my Honors application to be processed? 
                        </h3>
                        
                        <p> Decisions for students applying to the Seneff Honors College are sent in two to three
                           weeks.&nbsp; Students who apply to take an honors class should hear within two business
                           days 
                        </p>
                        
                        <h3>
                           <a name="Scholarship" id="Scholarship"></a>Will I automatically get a scholarship if I am in Honors? 
                        </h3>
                        
                        <p>The Seneff Honors College awards a limited number of full tuition, non-transferable
                           scholarships to admission candidates who demonstrate academic promise and who commit
                           to one of the four available curriculum tracks: Interdisciplinary Studies track, Leadership
                           track, Undergraduate Research track, or the Global Studies track. All students who
                           apply for a track in the Seneff Honors College will be automatically considered for
                           scholarship awards.
                        </p>
                        
                        <h3>
                           <a name="differences" id="differences"></a>What is different about honors classes?
                        </h3>
                        
                        <p>Honors classes at Valencia emphasize the program outcomes and help students to recognize
                           and weigh different perspectives in primary and secondary sources, to produce original
                           work and scholarly research, and to connect learning across academic disciplines.
                           Also, honors classes are smaller than non-honors classes, with enrollment capped at
                           20 students per class.
                           
                           
                        </p>
                        
                        <h3>
                           <a name="harder" id="harder"></a>Are honors classes harder?
                        </h3>
                        
                        <p>Many students mistakenly believe that honors classes will be harder than regular classes.
                           Honors courses provide students and faculty the opportunity to develop ideas and engage
                           in work that would be more difficult in a typical, lecture-style class.  Most students
                           find they actually do better in honors classes because of the close interaction between
                           professors and students and the sense of community that develops.
                        </p>
                        
                        <h3>
                           <a name="weighted" id="weighted"></a>Are honors course grades weighted?
                        </h3>
                        
                        <p>No. Unlike high school, college honors courses are not weighted.</p>
                        
                        <h3>
                           <a name="dualenrolled" id="dualenrolled"></a>Can dual-enrolled students take honors classes?
                        </h3>
                        
                        <p>Yes, dual-enrolled students who meet the program eligibility are encouraged to apply.</p>
                        
                        <h3>
                           <a name="Percent" id="Percent"></a>If I’m in Honors, do I have to register for all honors classes?
                        </h3>
                        
                        <p> No, most students take approximately one-quarter to one-half of their classes as
                           honors.
                        </p>
                        
                        <h3> <a name="Cost" id="Cost"></a>Do Honors classes cost more?
                        </h3>
                        
                        <p> No, tuition is the same for honors classes.</p>
                        
                        <h3> <a name="Fee" id="Fee"></a>Is there an application fee to apply for the Seneff Honors College? 
                        </h3>
                        
                        <p>No, there is no application fee for honors.</p>
                        
                        <h3>
                           <a name="ptk" id="ptk"></a>Is Phi Theta Kappa (PTK) part of the Honors Program?
                        </h3>
                        
                        <p>Although there is a close working relationship between the Valencia Honors Program
                           and Phi Theta Kappa, PTK operates independently with chapters on the East, West, and
                           Osceola campuses.  For more information, visit the Student Development page or drop
                           by the Student Development office on your campus.
                        </p>
                        
                        <h3>
                           <a name="hrc" id="hrc"></a>What is an Honors Resource Center?
                        </h3>
                        
                        <p> Students in the Seneff Honors College have access to the Honors Resource Centers
                           on East, Osceola, and West campuses. The Honors Resource Center provides a place for
                           honors students to study, access computers, and meet with fellow honors students.
                           
                        </p>
                        
                        <h3>
                           <a name="officelocation" id="officelocation"></a>Where is the Honors office located?
                        </h3>
                        
                        <p>The Honors office is located on West campus, 3-136.  Our office hours are Monday through
                           Friday, 7:30am - 4:30pm. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/prospective-students/faqs.pcf">©</a>
      </div>
   </body>
</html>