<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Honors Options | Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Honors Options | Seneff Honors College">
      <meta name="Keywords" content="college, school, educational, honors, programs, seneff, options">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/prospective-students/program.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/prospective-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>Prospective Students
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/prospective-students/">Prospective Students</a></li>
               <li>Honors Program Options</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Honors Options</h2>
                        
                        <p>The Seneff Honors College is designed for people with a passion for learning. In addition
                           to providing a challenging and in-depth course of study, it also emphasizes learning
                           outside of the classroom. International travel, cultural field trips, guest speakers,
                           community service, campus leadership and scientific research are all part of the Honors
                           experience. And, with an average class size of only 15 students, the Seneff Honors
                           College has a community feel—one that makes it easy to form great relationships with
                           fellow students and professors. 
                        </p>
                        
                        <p>While students have some flexibility in designing their honors program to meet individual
                           needs, there are four basic tracks that lead to graduation from the Seneff Honors
                           College.
                        </p>
                        
                        
                        <h3><a href="inter_studies.html" target="_blank">Interdisciplinary Studies Track (primarily on West Campus)</a></h3>
                        
                        <blockquote>
                           
                           <p>The interdisciplinary studies track has a humanities-style, multi-cultural curriculum
                              that features team-taught courses focusing on a wide range of subject matter – from
                              history to science, from art to philosophy.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3><a href="LeadershipTrack.html" target="_blank">Leadership Track (primarily on Osceola Campus)</a></h3>
                        
                        <blockquote>
                           
                           <p>The leadership track explores different styles of leadership and encourages students
                              to become agents of change in the college and community. The track includes a strong
                              service component, which leads to a service learning distinction at graduation.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3><a href="UndergraduateResearch.html" target="_blank"> Undergraduate Research Track (primarily on East Campus)</a></h3>
                        
                        <blockquote>
                           
                           <p>The undergraduate research track is designed to introduce students to the process
                              and practice of academic research and is especially suited for students who plan to
                              go to graduate or professional school.&nbsp; 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3><a href="GlobalStudies.html" target="_blank">Global Studies Track (primarily on Winter Park Campus)</a></h3>
                        
                        <blockquote>
                           
                           <p>The global studies track offers students opportunities to explore an interdependent
                              and multicultural world. The track includes a strong service component, which leads
                              to a service learning distinction at graduation. This track also leads to a global
                              distinction recognition at graduation. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Honors Courses</h3>
                        
                        <p>Honors classes are not just regular classes with additional work; they provide an
                           entirely unique intellectual and co-curricular experience.&nbsp; In addition, classes are
                           kept small; the average class size is 15 students.&nbsp; Students in honors courses study
                           and evaluate primary source materials rather than relying solely on textbooks.&nbsp; The
                           classes help students learn to fashion original interpretations and analyses of course
                           materials.
                        </p>
                        
                        <p>To see a list of honors classes that will be offered, visit the <a href="http://net5.valenciacollege.edu/schedule/">Credit Class Schedule Search</a> and select <strong>Show only Honors Classes</strong>. Honors courses include:
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Course #</th>
                                 
                                 <th>Course Title</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ANT2000H</td>
                                 
                                 <td>Introduction to Anthropology </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>AMH 2010H </td>
                                 
                                 <td>U.S. History to 1877 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>AMH 2020H</td>
                                 
                                 <td>U.S. History 1877 to Present</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>AML 2021H</td>
                                 
                                 <td>Survey in American Literature</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ARH 2051H</td>
                                 
                                 <td>Intro to Art History II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>AST 1002H</td>
                                 
                                 <td>Astronomy</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>BSC 1005H</td>
                                 
                                 <td>Biological Science</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>BSC 1010H</td>
                                 
                                 <td>Fundamentals of Biology I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>BSC 1011H</td>
                                 
                                 <td>Fundamentals of Biology II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>BSC 1026H</td>
                                 
                                 <td>Biology of Human Sexuality</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>BSC1050H</td>
                                 
                                 <td>Environmental Science </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>CHM 1045H</td>
                                 
                                 <td>General Chemistry with Qualitative Analysis I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>CHM 1046H</td>
                                 
                                 <td>General Chemistry with Qualitative Analysis II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>DEP 2004H</td>
                                 
                                 <td>Developmental Psychology</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ECO 2013H</td>
                                 
                                 <td>Principals of Economics-Macro</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ECO 2023H</td>
                                 
                                 <td>Principals of Economics-Micro</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ENC 1101H</td>
                                 
                                 <td>Freshman Composition I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>ENC 1102H</td>
                                 
                                 <td>Freshman Composition II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>GLY 2100H</td>
                                 
                                 <td>Historical Geology</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 1020H </td>
                                 
                                 <td>Introduction to Humanities </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2220H</td>
                                 
                                 <td>Humanities – Greek and Roman</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2223H</td>
                                 
                                 <td>Humanities – Late Roman and Medieval</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2232H</td>
                                 
                                 <td>Humanities – Renaissance and Baroque</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2234H</td>
                                 
                                 <td>Humanities – Enlightenment and Romanticism</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2250H</td>
                                 
                                 <td>Humanities – Twentieth Century</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>HUM 2310H</td>
                                 
                                 <td>Humanities – Mythology in Art and Literature</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 1110</td>
                                 
                                 <td>Interdisciplinary Studies in General Education I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 1111</td>
                                 
                                 <td>Interdisciplinary Studies in General Education II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 1112</td>
                                 
                                 <td>Interdisciplinary Studies in General Education</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2120</td>
                                 
                                 <td>Interdisciplinary Studies in General Education III</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2121</td>
                                 
                                 <td>Interdisciplinary Studies in General Education IV</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2028</td>
                                 
                                 <td>Honors Capstone </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2911</td>
                                 
                                 <td>Honors Research Process </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2912</td>
                                 
                                 <td>Honors Project </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>IDH 2955 </td>
                                 
                                 <td>Honors Study Abroad </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>INR 2002H</td>
                                 
                                 <td>International Politics</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>LIT 2090H</td>
                                 
                                 <td>Contemporary Literature</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>LIT 2120H</td>
                                 
                                 <td>Survey in World Literature – Enlightenment to Present</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>LIT 2174H</td>
                                 
                                 <td>Multimedia Lit &amp; The Holocaust</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAC 1105H</td>
                                 
                                 <td>College Algebra</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAC 2311H </td>
                                 
                                 <td>Calculus with Analytic Geometry I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAC 2312H</td>
                                 
                                 <td>Calculus with Analytic Geometry II</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>OCE 1001H</td>
                                 
                                 <td>Intro to Oceanography</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>PCB 1440H </td>
                                 
                                 <td>Florida Environmental Systems</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>PHY 2048H</td>
                                 
                                 <td>General Physics with Calculus I </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>PHY 2049H</td>
                                 
                                 <td>General Physics with Calculus II </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>POS 2041H</td>
                                 
                                 <td>U.S. Government I</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>POS 2112H </td>
                                 
                                 <td>State and Local Government </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>PSY 2012H</td>
                                 
                                 <td>General Psychology</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SLS 1501H</td>
                                 
                                 <td>Honors Seminar </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SLS 2261H</td>
                                 
                                 <td>Leadership Development </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SLS 2940H</td>
                                 
                                 <td>Service Learning </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SPC 1017H </td>
                                 
                                 <td>Interpersonal Communication </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SPC 1608H</td>
                                 
                                 <td>Fundamentals of Speech</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>STA 2023H</td>
                                 
                                 <td>Statistical Methods</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>SYG 2000H</td>
                                 
                                 <td>Intro to Sociology</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>** To see course descriptions, see the current <a href="/STUDENTS/catalog/index.html">Valencia catalog</a>. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/prospective-students/program.pcf">©</a>
      </div>
   </body>
</html>