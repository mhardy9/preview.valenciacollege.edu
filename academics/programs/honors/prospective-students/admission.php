<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Admission Requirements | Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Admission Requirements | Seneff Honors College">
      <meta name="Keywords" content="college, school, educational, admission, requirements, seneff, honors">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/prospective-students/admission.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/prospective-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>Prospective Students
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/prospective-students/">Prospective Students</a></li>
               <li>Admission Requirements</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Admission Requirements</h2>
                        
                        <p>Students can apply to the <strong> Seneff Honors College</strong>. Students in the <strong>Seneff Honors College</strong> can earn an <strong>Honors Diploma</strong> in one of the four available curriculum tracks: Global Studies track, Interdisciplinary
                           Studies track, Leadership track, and Undergraduate Research track.<br> <br> To apply for the Seneff Honors College, <a href="http://atlas.valenciacollege.edu/">log onto Atlas</a> and click the "Students" tab. In the middle, "Student Forms" section, click on "Applications".
                           The application deadline for priority scholarship consideration is April 7, 2018 for
                           Fall 2018 though applications will continue to be accepted through July 15, 2018.
                        </p>
                        
                        <p><em>Students are encouraged to apply as soon as possible for scholarship consideration.</em></p>
                        
                        <p>Admission of candidates into the <strong>Seneff Honors College</strong> is a consideration of a student’s potential beyond simply a test score or GPA. &nbsp;All
                           highly-motivated students who desire to make the most of their Valencia experience
                           are encouraged to apply.&nbsp; Students wishing admission to the Seneff Honors College
                           will:
                        </p>
                        
                        <ul>
                           
                           <li>Complete an application to Valencia College.</li>
                           
                           <li>Complete a typed, minimum 500-word essay answering all the following questions: How
                              do you envision the Seneff Honors College helping you find your purpose so that you
                              can plan and prepare for your pathway into your degree program?&nbsp; What personal connections
                              can you make both within and outside the college that will help you accomplish your
                              career goal? What do you expect to gain from and contribute to the Seneff Honors College?
                              The essay will be uploaded with your application.
                           </li>
                           
                           <li><a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A//gcp.valenciacollege.edu/CPIP/?sys=honors2013" target="_blank">Complete an online application to the Seneff Honors College through Atlas.</a> (Atlas &amp;gt; Students &amp;gt; Student Forms &amp;gt; Applications)
                           </li>
                           
                           <li>Submit standardized test scores to the Valencia Registrar’s Office or take the PERT
                              at one of the Valencia campuses (transfer students may be exempt from this requirement).
                              Prior to being admitted to the Seneff Honors College, a student must have college
                              level assessment scores or have completed any developmental mandate in reading and
                              writing.
                           </li>
                           
                           <li>Submit an unofficial copy of high school transcripts to the Honors Office (for students
                              who have graduated from high school within the last 2 years).
                           </li>
                           
                           <li>Submit an unofficial copy of all college transcripts to the Honors Office if the coursework
                              has not yet been officially evaluated by Valencia (for transfer students).
                           </li>
                           
                           <li>Submit a degree audit for your intended major. You can generate a degree audit in
                              Atlas under the Students tab in the Path to Graduation area.
                           </li>
                           
                           <li>Submit one of the following:
                              
                              <ul>
                                 
                                 <li>Letter of recommendation from a teacher, school official, supervisor or community
                                    leader.
                                 </li>
                                 
                                 <li>Letter of recommendation from a Valencia College faculty member (online recommendation
                                    form can be completed <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_0vMtgLWdPyO3alf">here)</a>.
                                 </li>
                                 
                              </ul>
                              Note: Please submit your application to the Seneff Honors College prior to submitting
                              your recommendation. Faliure to do so may result in your recommendation not being
                              matched to your application.
                           </li>
                           
                        </ul>
                        
                        <div class="box_style_3">
                           
                           <p>** Students who have not completed ENC1101 must meet test score requirements for ENC1101
                              or must have current enrollment in the highest level Reding and Writing developmental
                              education course to be eligible for the Seneff Honors College. For specifics, see
                              the <a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/" target="_blank">Valencia catalog</a>.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/prospective-students/admission.pcf">©</a>
      </div>
   </body>
</html>