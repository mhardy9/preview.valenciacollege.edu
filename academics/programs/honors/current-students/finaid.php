<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/current-students/finaid.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/current-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Seneff Honors College Scholarships</h2>
                        
                        <p>The Seneff Honors College awards a limited number of full tuition, non-transferable
                           scholarships to admission candidates who demonstrate academic promise and who commit
                           to one of the four available curriculum tracks: Global Studies track, Interdisciplinary
                           Studies track, Leadership track, and Undergraduate Research track. All eligible students
                           who apply for the Seneff Honors College will be considered for scholarship awards.
                           
                           
                           
                           Scholarships cover up to the credits needed for your primary degree program and any
                           additional Honors course work specific to your track.&nbsp; You must be eligible for the
                           Florida resident tuition rate to receive a Seneff Honors College scholarship. 
                        </p>
                        
                        
                        <h3>Initial Eligibility</h3>
                        
                        <ul>
                           
                           <li>
                              Recipients must submit a Free Application for Federal Student Aid or FAFSA (<a href="http://www.fafsa.ed.gov/">http://www.fafsa.ed.gov/</a>) so that processed results are available prior to the start of term. If selected
                              for verification, this must be satisfied prior to the start of the term for your scholarship
                              to award. 
                              <br><strong>Note:</strong> To complete a FAFSA, you must be an eligible US citizen or eligible non-citizen for
                              financial aid purposes. Please allow at least 3 weeks for processing.
                              
                           </li>
                           
                           <li>Recipients must meet Standards of Satisfactory Academic Progress for Financial Aid
                              Purposes.
                           </li>
                           
                           <li>Recipients must be seeking an Associate in Arts Degree.</li>
                           
                           <li>Recipients must be accepted into a Seneff Honors College track.</li>
                           
                           <li>
                              Recipients must enroll in and satisfactorily complete at least nine Honors credits
                              during each academic year. If you are admitted to the Seneff Honors College for the
                              Spring term, you must enroll in and satisfactorily complete at least three Honors
                              credits during the Spring term. <em>Failure to meet this requirement at the end of the drop/refund period will result
                                 in revocation of your scholarship for that term and your term fees will become due
                                 to the college immediately.</em> Contact the Honors Director or an honors advisor with questions about necessary classes.
                              <br><strong>Note</strong>: Withdrawal from any course may result in adjustment of your term financial aid (not
                              just your Honors scholarship).
                              
                           </li>
                           
                        </ul>
                        <a name="renewaleligibility" id="renewaleligibility"></a>
                        
                        <h3>Renewal Eligibility</h3>
                        
                        <ul>
                           
                           <li>The scholarship is renewed on a term-by-term basis (Fall and Spring) pending satisfaction
                              of renewal criteria and availability of funds.
                           </li>
                           
                           <li>Recipients must make good progress towards graduation in their degree program, their
                              curricular track and satisfy co-curricular requirements as determined by the Honors
                              Director. Recipients must attend three approved co-curricular activities per term
                              in each term the scholarship is received.
                           </li>
                           
                           <li>
                              Recipients must submit a Free Application for Federal Student Aid or FAFSA (<a href="http://www.fafsa.ed.gov/">http://www.fafsa.ed.gov/</a>) so that processed results are available prior to the start of term. If selected
                              for verification, this must be satisfied prior to the start of the term for your scholarship
                              to award. 
                              <br><strong>Note:</strong> To complete a FAFSA, you must be an eligible US citizen or eligible non-citizen for
                              financial aid purposes. Please allow at least 3 weeks for processing.
                              
                           </li>
                           
                           <li>Recipients must maintain Satisfactory Academic Progress to receive financial aid as
                              outlined in the college catalog.
                           </li>
                           
                           <li>Recipients must continue to be seeking an Associate's Degree. Once degree requirements
                              have been met students are no longer eligible for the scholarship regardless of awarding
                              of degree. 
                           </li>
                           
                           <li>
                              Recipients must enroll in and satisfactorily complete at least nine Honors credits
                              during each academic year. If you are admitted to the Seneff Honors College for the
                              Spring term, you must enroll in and satisfactorily complete at least three Honors
                              credits during the Spring term. <em>Failure to meet this requirement at the end of the drop/refund period will result
                                 in revocation of your scholarship for that term and your term fees will become due
                                 to the college immediately.</em> Contact the Honors Director or an honors advisor with questions about necessary classes.
                              <br><strong>Note</strong>: Withdrawal from any course may result in adjustment of your term financial aid (not
                              just your Honors scholarship).
                              
                           </li>
                           
                           <li>Recipients must maintain eligibility to be a student in the Seneff Honors College
                              (minimum GPA of 3.25).
                           </li>
                           
                           <li>Recipients must complete at least 75% of attempted credits to be renewed in subsequent
                              terms.
                           </li>
                           
                        </ul>
                        
                        <p>All students receiving any form of financial aid must meet the standards of academic
                           progress as outlined by the federal government. See the <a href="/admissions/finaid/satisfactory_progress.html">Satisfactory Academic Progress</a> web page for criteria, or refer to the SAP section of this catalog.
                        </p>
                        
                        <p>Signed Honors Scholarship Agreements must be received in the Honors office by the
                           deadline listed in the recipient's scholarship award notification before the scholarsip
                           will post to the recipient's account. Failure to return the Honors Scholarship Agreement
                           by the listed deadline may result in forfeiture of the scholarship for that term.
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/current-students/finaid.pcf">©</a>
      </div>
   </body>
</html>