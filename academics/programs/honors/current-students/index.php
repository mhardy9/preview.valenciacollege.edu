<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/current-students/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/current-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li>Current Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        <h2>Current Students</h2>
                        
                        <p>Current students are invited to explore the links to the left to find information
                           about advising, study abroad, service learning, academic policies, etc.  Honors student
                           are  also encouraged to become an active part of the Valencia Honors Program by becoming
                           part of the Honors Student Advisory Committee.
                        </p>
                        
                        <h3>
                           <a href="documents/2017-18-Student-Honors-Handbook.pdf" target="_blank"><img height="161" src="/_resources/img/admissions/honors-program/current-students/2017-18-Student-Honors-Handbook.jpg" width="104"> 2017-18 Honors Student Handbook</a> 
                        </h3>
                        
                        <h3>
                           <a href="documents/2016-17HonorsStudentHandbook.pdf" target="_blank"><img height="160" src="/_resources/img/admissions/honors-program/current-students/2016-17HonorsStudentHandbookCover.jpg" width="104"> 2016-17 Honors Student Handbook</a> 
                        </h3>
                        
                        <h3>
                           <a href="documents/15HON001-honors-student-hanbook-proof1.2.pdf" target="_blank"><img height="162" src="/_resources/img/admissions/honors-program/current-students/2015-16HonorsStudentHandbookCover.PNG" width="104"> 2015-16 Honors Student Handbook</a> 
                        </h3>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="../prospective-students/howtoapply.html" target="_blank" class="button-action_outline">How To Apply</a>        
                        
                        <center>
                           <strong>Spring 2018 Application deadline: November 3rd, 2017</strong><p> For admission criteria, visit <br><a href="../prospective-students/index.html">Prospective Students</a><br> For more information, contact <br>the Seneff Honors College at 407-582-1729 <strong>or <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a> </strong></p>
                           
                        </center>
                        
                        <strong>
                           
                           <div id="fb-root"></div>
                           
                           
                           
                           <div class="fb-like-box" data-href="https://www.facebook.com/ValenciaHonors" data-width="245" data-show-faces="false" data-stream="false" data-header="false"></div>
                           
                           </strong></aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/current-students/index.pcf">©</a>
      </div>
   </body>
</html>