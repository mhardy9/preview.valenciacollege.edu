<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/current-students/honors-calendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/current-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/current-students/">Current Students</a></li>
               <li>Seneff Honors College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Honors Calendar</h2>
                        
                        <p>Students in the Seneff Honors College are required to do three approved co-curricular
                           activities each term and, along with taking honors classes and earning a high Grade
                           Point Average, are a part of the requirement to graduate with an honors diploma.&nbsp;The
                           co-curricular requirement supports students' achievement of the Honors program outcomes
                           and builds community.&nbsp;Co-curricular activities are&nbsp;an integral, intentional part of
                           the Honors experience.&nbsp;
                        </p>
                        
                        <p>Our&nbsp;Honors Co-Curricular Principles for approved activities include:</p>
                        
                        <ol>
                           
                           <li>The activity is designed specifically for Honors students,</li>
                           
                           <li>If it is a non-Honors sponsored event, there must be an Honors specific add on/value
                              added (additional research, panel discussion, etc.),
                           </li>
                           
                           <li>The activity has a substantive learning component (minimum 2 hours of direct instruction
                              and student participation),
                           </li>
                           
                           <li>The activity is specifically designed to support an Honors class or program outcome
                              including building community amongst Honors students,
                           </li>
                           
                           <li>The activity is not for another club or organization (SGA, PTK, etc.)</li>
                           
                        </ol>
                        
                        <p>You can search the available co-curricular options&nbsp;on the Honors Co-Curricular Calendar.
                           The calendar will show the title and location of the activity, In order to earn credit
                           for participating in the activity, you must attend the entire event and complete the
                           Co-curricular Activity Confirmation. The form should be completed within 7 days of
                           the activity to make sure you keep an up-to-date list of your co-curricular activities,
                           but must be submitted before the term ends.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><a href="https://www.google.com/calendar/embed?src=honors%40valenciacollege.edu&amp;ctz=America/New_York" target="_blank">Honors Co-Curricular Calendar </a></td>
                                 
                                 <td>
                                    
                                    <p>Contains a full list of co-curricular activities and other upcoming program events.
                                       To count toward the 3 activity per term requirement, co-curricular activities must
                                       be completed by the last day of class for the full term (not the final exam period).
                                    </p>
                                    
                                    <p>Some events are offered more than one time (plays, resource fairs, etc.). You can
                                       only receive credit for attending an event one time - even if the event is repeated.
                                       You cannot receive duplicate credit.
                                    </p>
                                    
                                    <p>Many of these events are offered by other departments or programs at Valencia. We
                                       include them to provide a wide array of options. Time and location are posted as submitted
                                       to us by the departments.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_2sDhjZb3WXhYl0N" target="_blank">Co-curricular Activity Confirmation Form </a></td>
                                 
                                 <td>This form should be completed within <em>7 days</em> of a calendar event to earn co-curricular credit. To count toward the 3 activity
                                    per term requirement, co-curricular activities must be completed by the last day of
                                    class for the full term (not the final exam period).
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_3jusAzr9JsVHWjX" target="_blank">Request to Add Event to Honors Calendar </a></td>
                                 
                                 <td>Complete this form if you know of an event that you think should be added to the honors
                                    calendar that meets the above Co-Curricular Principles. You can also complete this
                                    form if you have an activity in mind you would like to create and offer for your peers.
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                     <aside class="col-md-3"><a class="button-action_outline" href="/academics/programs/honors/prospective-students/howtoapply.html" target="_blank">How To Apply</a><center><strong>Fall 2018 Application deadline for priority scholarship consideration: April 1, 2018.
                              Final application deadline: July 15, 2018.</strong>
                           
                           <p>For admission criteria, visit <br><a href="/academics/programs/honors/prospective-students/index.html">Prospective Students</a><br> For more information, contact <br>the Seneff Honors College at 407-582-1729 <strong>or <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a> </strong></p>
                           
                        </center>
                        
                        <div>
                           
                           <div id="fb-root">&nbsp;</div>
                           
                        </div>
                        
                        <div>
                           
                           <div class="fb-like-box" data-header="false" data-href="https://www.facebook.com/ValenciaHonors" data-show-faces="false" data-stream="false" data-width="245">&nbsp;</div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/current-students/honors-calendar.pcf">©</a>
      </div>
   </body>
</html>