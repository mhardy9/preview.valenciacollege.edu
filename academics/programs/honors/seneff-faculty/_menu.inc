<ul>
    <li><a href="../index.php">Seneff Honors College</a></li>
    <li class="submenu">
        <a href="../prospective-students/index.php">Prospective Students <span class="caret" aria-hidden="true"></span></a>
        <div class="menu-wrapper">
            <ul>
                <li><a href="../prospective-students/program.php">Honors Options</a></li>
                <li><a href="../prospective-students/admission.php">Admission</a></li>
                <li><a href="../prospective-students/finaid.php">Scholarships</a></li>
                <li><a href="../prospective-students/benefits.php">Benefits</a></li>
                <li><a href="../prospective-students/transfer.php">Transfer</a></li>
                <li><a href="../prospective-students/faqs.php">FAQs</a></li>
            </ul>
        </div>
    </li>

    <li class="submenu">
        <a href="../current-students/index.php">Current Students <span class="caret" aria-hidden="true"></span></a>
        <div class="menu-wrapper">
            <ul>
                <li><a href="../current-students/advisors.php">Advising</a></li>
                <li><a href="../current-students/finaid.php">Scholarships</a></li>
                <li><a href="../current-students/study-abroad.php">Study Abroad</a></li>
                <li><a href="../current-students/service-learning.php">Service Learning</a></li>
                <li><a href="../current-students/advisory-committee.php">Honors Student Advisory Committee</a></li>
                <li><a href="../current-students/policies.php">Policies</a></li>
                <li><a href="http://net4.valenciacollege.edu/forms/honors/gradapp.cfm" target="_blank">Graduation Application</a></li>
                <li><a href="../current-students/honors-calendar.php">Honors Co-Curricular Calendar</a></li>
                <li><a href="../connect.php">Connect</a></li>
            </ul>
        </div>
    </li>

    <li class="submenu">
        <a href="../seneff-faculty/index.php">Honors Faculty <span class="caret" aria-hidden="true"></span></a>
        <div class="menu-wrapper">
            <ul>
                <li><a href="../seneff-faculty/development-program.php"> Seneff Faculty Development Program</a></li>
                <li><a href="../seneff-faculty/advisory-board.php"> Honors Advisory Board</a></li>
            </ul>
        </div>
    </li>
    <li><a href="http://net4.valenciacollege.edu/forms/honors/contact.cfm" target="_blank">Contact Us</a></li>

</ul>