<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Honors Advisory Board | Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Honors Advisory Board | Seneff Honors College">
      <meta name="Keywords" content="college, school, educational, honors, seneff, advisory, board, members">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/seneff-faculty/advisory-board.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/seneff-faculty/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>Honors Faculty
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li><a href="/academics/programs/honors/">Honors Program</a></li>
               <li><a href="/academics/programs/honors/seneff-faculty/">Seneff Faculty</a></li>
               <li>Honors Advisory Board</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Honors Advisory Board</h2>
                        
                        <p>The Honors Advisory Board is composed of Valencia faculty, staff members, and representatives
                           from the Honors Student Advisory Committee. The board meets at least once each semester
                           to make policy decisions and recommendations concerning the program.
                        </p>
                        
                        <h3>2017 - 18 Membership</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>Dr. Nicholas Bekas</td>
                                 
                                 <td>Dean of Academic Affairs, West Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Karen Borglum</td>
                                 
                                 <td>Assistant Vice President, Curriculum &amp; Assessment</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Shauna Clifton</td>
                                 
                                 <td><span>Honors Student Advisory Committee Interim President</span></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>B. Clyburn</td>
                                 
                                 <td>Honors Program Assistant</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Kera Coyer</td>
                                 
                                 <td>Honors Program Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Susan Dunn</td>
                                 
                                 <td>Manager, Credit Programs, Winter Park and Global Studies Track Coordinator</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Michelle Foster</td>
                                 
                                 <td>Dean of Academic Affairs, East Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Shara Lee</td>
                                 
                                 <td>Campus Director, Faculty and Instructional Development</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jane Maguire</td>
                                 
                                 <td>Professor, Communications, East Campus and Undergraduate Research Track Coordinator</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Gwendolyn Noel</td>
                                 
                                 <td>Academic Advisor, Lake Nona Campus and Leadership Track Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Melissa Pedone</td>
                                 
                                 <td>Dean of Mathematics and Science, Osceola Campus and Leadership Track Coordinator</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Cheryl Robinson</td>
                                 
                                 <td>Honors Director</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Marcia Roman</td>
                                 
                                 <td>Counselor,&nbsp;Winter Park Campus and Global Studies Track&nbsp;Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Travis Rodgers</td>
                                 
                                 <td>Professor, Humanities, West Campus and Interdisciplinary Studies Track Coordinator</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Natali Shulterbrondt</td>
                                 
                                 <td>Counselor, Osceola Campus and Leadership Track Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dr. Jill Szentmiklosi</td>
                                 
                                 <td>Dean of Students, Osceola Campus</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Travis Taylor</td>
                                 
                                 <td>Academic Advisor, West Campus and Interdisciplinary Studies Track Advisor</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jennifer Washick</td>
                                 
                                 <td>Academic Advisor, East Campus and Undergraduate Research Track Advisor</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li>Valencia Honors Annual Report, 2016-2017</li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/2015-16SeneffHonorsCollegeAnnualReport.docx">Valencia Honors Annual Report, 2015-2016</a></li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/ValenciaHonorsAnnualReport2013-2014.pdf" target="_blank">Valencia Honors Annual Report, 2013-2014 </a></li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/ValenciaHonorsAnnualReport2012-2013.pdf" target="_blank">Valencia Honors Annual Report, 2012-2013</a></li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/ValenciaHonorsAnnualReport2011-2012.pdf" target="_blank">Valencia Honors Annual Report, 2011-2012</a></li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/ValenciaHonorsAnnualReport2010-2011.pdf" target="_blank">Valencia Honors Annual Report, 2010-2011</a></li>
                           
                           <li><a href="/academics/programs/honors/seneff-faculty/documents/ValenciaHonorsAnnualReport2009-2010.pdf" target="_blank">Valencia Honors Annual Report, 2009-2010</a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/seneff-faculty/advisory-board.pcf">©</a>
      </div>
   </body>
</html>