<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Seneff Honors College | Valencia College</title>
      <meta name="Description" content="Seneff Honors College">
      <meta name="Keywords" content="college, school, educational, seneff, honors">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/honors/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/honors/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Seneff Honors College</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Honors Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <img alt="Inaugural Class of the Seneff Honors College" src="images/seneff_inaugural_class.jpg" class="img-responsive">
                        <br>
                        
                        <blockquote>
                           “I will act as if what I do will make a difference.” <br>
                           <em>William James</em>
                           
                        </blockquote>
                        
                        <p>Aim higher. Become a part of the new Seneff Honors College at Valencia, offering four
                           distinct paths to an honors degree. This program is for students who want more from
                           their college experience—more challenges, more opportunities, and more connections
                           with fellow students and great professors. The Seneff Honors College is for people
                           with a passion for learning. 
                        </p>
                        
                        <p>Valencia offers this and more, all in a setting that nurtures the whole individual.
                           
                        </p>
                        
                        <ul>
                           
                           <li>smaller classes special scholarships</li>
                           
                           <li>recognition at commencement </li>
                           
                           <li>special service learning opportunities</li>
                           
                           <li>co-curricular events that contribute to learning and the community</li>
                           
                           <li>scholarships</li>
                           
                           <li>domestic and international travel</li>
                           
                        </ul>
                        	
                        	
                        <div align="center">
                           		<a href="prospective-students/howtoapply.php" target="_blank" class="button-action_outline">How To Apply</a>
                           
                           
                           
                           <center>
                              <strong>Spring 2018 Application deadline: November 3rd, 2017</strong><p> For admission criteria, visit <br><a href="prospective-students/index.php">Prospective Students</a><br> For more information, contact <br>the Seneff Honors College at 407-582-1729 <strong>or <a href="mailto:honors@valenciacollege.edu">honors@valenciacollege.edu</a> </strong></p>
                              
                           </center>
                           
                           <strong>
                              
                              <div id="fb-root"></div>
                              
                              
                              
                              <div class="fb-like-box" data-href="https://www.facebook.com/ValenciaHonors" data-width="245" data-show-faces="false" data-stream="false" data-header="false"></div>
                              
                              </strong>
                           		
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/honors/index.pcf">©</a>
      </div>
   </body>
</html>