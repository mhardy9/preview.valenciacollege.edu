<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Teacher Education Program  | Valencia College</title>
      <meta name="Description" content="Teacher Education Program">
      <meta name="Keywords" content="college, school, educational, teacher, certification, credential">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/programs/teacher-education-program/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/programs/teacher-education-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Teacher Education Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/programs/">Programs</a></li>
               <li>Teacher Education Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        					
                        <h2>Teacher Education Program</h2>
                        
                        					
                        <p><strong>Creativity, innovation, and passion – attributes of an effective teacher.</strong></p>
                        					
                        <p>We are excited that you are considering a career in education! Welcome to Valencia
                           College, East Campus where we can help you master the skills you will need to launch
                           your career in education. Teaching is more than a job; it’s a calling. Today’s teachers
                           work in accountability-driven, technology oriented environments with high standards
                           of performance. Choosing how you prepare yourself to meet the demands and rewards
                           of teaching is an important career decision.
                        </p>
                        					
                        <p>Our Teacher Education Program (TEP) offers a full range of opportunities to prepare
                           you with the knowledge, skills, and attitudes necessary to enter a Teacher Education
                           Bachelor of Science program.&nbsp; At East Campus only, we offer a unique program:
                        </p>
                        					
                        <ul class="list_style_1">
                           						
                           <li>All the TEP prerequisite courses in several formats to fit everyone’s needs (Face-to-Face,
                              Hybrid, Online, Saturday Specials)
                           </li>
                           						
                           <li>All the courses necessary to complete your pre-major in Early Childhood Education
                              and Elementary Education.
                           </li>
                           						
                           <li>An award-winning Valencia Future Educators Club</li>
                           						
                           <li>Service Learning opportunities that replace field observations allowing students to
                              earn a Service Learning medallion for graduation and designation on your diploma.
                           </li>
                           						
                           <li>A dedicated Teacher Education Advisor to assist you with your TEP choices.</li>
                           					
                        </ul>
                        					
                        					
                        <h3>Contact Us</h3>
                        					
                        <p>
                           						VFE email: <a href="mailto:valenciafutureeducatorsec@gmail.com">valenciafutureeducatorsec@gmail.com</a><br>
                           						Advisors: <a href="mailto:LLott1@valenciacollege.edu">Professor Lauri Lott</a> <br>
                           						<a href="mailto:yqadri@valenciacollege.edu">Dr. Yasmeen Qadri</a>
                           					
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/programs/teacher-education-program/index.pcf">©</a>
      </div>
   </body>
</html>