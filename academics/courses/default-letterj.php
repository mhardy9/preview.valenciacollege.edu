<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Descriptions | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/courses/default-letterj.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/courses/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Course Descriptions</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/courses/">Courses</a></li>
               <li>Course Descriptions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a href="index.html"></a>
                        <br>
                        <br>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>To view the description for a specific Valencia course, click on the first letter
                                       of the prefix for that course.  
                                    </p>
                                    
                                    <h3>      <a href="default.cfm-letter=A.html">A</a>&nbsp;&nbsp;<a href="default.cfm-letter=B.html">B</a>&nbsp;&nbsp;<a href="default.cfm-letter=C.html">C</a>&nbsp;&nbsp;<a href="default.cfm-letter=D.html">D</a>&nbsp;&nbsp;<a href="default.cfm-letter=E.html">E</a>&nbsp;&nbsp;<a href="default.cfm-letter=F.html">F</a>&nbsp;&nbsp;<a href="default.cfm-letter=G.html">G</a>&nbsp;&nbsp;<a href="default.cfm-letter=H.html">H</a>&nbsp;&nbsp;<a href="default.cfm-letter=I.html">I</a>&nbsp;&nbsp;<a href="default.cfm-letter=J.html">J</a>&nbsp;&nbsp;<a href="default.cfm-letter=K.html">K</a>&nbsp;&nbsp;<a href="default.cfm-letter=L.html">L</a>&nbsp;&nbsp;<a href="default.cfm-letter=M.html">M</a>&nbsp;&nbsp;<a href="default.cfm-letter=N.html">N</a>&nbsp;&nbsp;<a href="default.cfm-letter=O.html">O</a>&nbsp;&nbsp;<a href="default.cfm-letter=P.html">P</a>&nbsp;&nbsp;<a href="default.cfm-letter=Q.html">Q</a>&nbsp;&nbsp;<a href="default.cfm-letter=R.html">R</a>&nbsp;&nbsp;<a href="default.cfm-letter=S.html">S</a>&nbsp;&nbsp;<a href="default.cfm-letter=T.html">T</a>&nbsp;&nbsp;<a href="default.cfm-letter=U.html">U</a>&nbsp;&nbsp;<a href="default.cfm-letter=V.html">V</a>&nbsp;&nbsp;<a href="default.cfm-letter=W.html">W</a>&nbsp;&nbsp;<a href="default.cfm-letter=X.html">X</a>&nbsp;&nbsp;<a href="default.cfm-letter=Y.html">Y</a>&nbsp;&nbsp;<a href="default.cfm-letter=Z.html">Z</a></h3>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <br>
                        
                        
                        
                        <form action="prefix_detail.html" method="post">
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div> 
                                       From the dropdown box, choose the prefix of the course in which you are interested,
                                       and click go.<br><br>
                                       <select name="prefix">
                                          
                                          
                                          <option value="JOU">JOU</option>
                                          
                                          
                                          <option value="JPN">JPN</option>
                                          </select>
                                       &nbsp;&nbsp;<input type="Submit" value="go">
                                       <br>&nbsp;
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </form>
                        
                        <br>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/courses/default-letterj.pcf">©</a>
      </div>
   </body>
</html>