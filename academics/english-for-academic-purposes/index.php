<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>English for Academic Purposes  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/english-for-academic-purposes/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/english-for-academic-purposes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>English for Academic Purposes</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>English For Academic Purposes</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>About Us </h2>
                        
                        
                        <h3>What is EAP?</h3>
                        
                        <p>English for Academic Purposes is an English program for speakers of other languages.
                           The program is designed to bring English skills to a level that will help students
                           be successful in college courses. EAP is designed for students who have some background
                           in English.
                        </p>
                        
                        
                        <h3>What Kind of Classes are offered?</h3>
                        
                        <p>Day and evening classes teach English skills involved in speaking/listening, reading,
                           writing, and grammar. Five levels of EAP courses are offered during regular college
                           semesters – each semester is approximately 15 weeks long. While EAP courses are credit
                           courses, only the advanced levels count toward a degree.
                        </p>
                        
                        
                        <h3>How will EAP help me?</h3>
                        
                        <p>By strengthening English skills, students can improve grades, gain better jobs, and
                           earn higher wages. In Central Florida especially, bilingual individuals have more
                           opportunities for employment and promotion. EAP students find that their increased
                           vocabulary and improved pronunciation help them to communicate more clearly and be
                           more effective in their everyday lives.
                        </p>
                        
                        
                        <h3>Even though I am not interested in getting a degree, can I still take EAP classes?</h3>
                        
                        <p>Yes. Many students in the program already have degrees but wish to take EAP for professional
                           development.
                        </p>
                        
                        
                        <h3>How long will it take to complete the EAP program?</h3>
                        
                        <p>This depends on your skill level at the time of entering the program.</p>
                        
                        
                        <h3>How do I get placed into the level of EAP I need?</h3>
                        
                        <p>
                           After completing a Valencia application, go to the Assessment Center and take the
                           LOEP (Levels of English Proficiency) test.<br>
                           East: Bldg. 5, Room 237<br>
                           Osceola: Bldg. 1, Room 127<br>
                           West: SSB Bldg., Room 171<br>
                           Winter Park: Bldg. 1, Room 128
                           
                        </p>
                        
                        
                        <h3>Is financial aid available to cover the cost of EAP courses?</h3>
                        
                        <p>Yes. Many EAP students do qualify for financial aid, with the exception of international
                           students.
                        </p>
                        
                        
                        <h3>How do I apply for EAP courses?</h3>
                        
                        <p>
                           Go to the Admissions Office and complete an application for Valencia. You may also
                           complete the application form <a href="/admissions/admissions-records/">online</a>. There is a $35.00 application fee. <br>
                           East: Bldg. 5, Room 211<br>
                           Osceola: Bldg. 1, Room 150<br>
                           West: SSB Bldg., Room 106<br>
                           Winter Park: Bldg. 1, Room 210
                           
                        </p>
                        
                        
                        <h3>What other services does Valencia provide?</h3>
                        
                        <ul class="list_style_1">
                           
                           <li> Computer Labs with English language learning software </li>
                           
                           <li> Fully staffed Language Lab and Reading and English Lab </li>
                           
                           <li> Tutoring Center </li>
                           
                           <li> Career Center</li>
                           
                           <li> Bilingual staff and international advisors </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/english-for-academic-purposes/index.pcf">©</a>
      </div>
   </body>
</html>