<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/english-for-academic-purposes/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/english-for-academic-purposes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>English for Academic Purposes</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/english-for-academic-purposes/">English For Academic Purposes</a></li>
               <li>Frequntly Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        
                        <h3>How many levels are there in the EAP program?</h3>
                        
                        <p>There are six levels of EAP classes in the community college system. Valencia offers
                           levels 2-6.
                        </p>
                        
                        
                        <h3>How many courses make up a level? What are they?</h3>
                        
                        <p>There can be up to 4 courses in each level. They are Speech, Reading, Writing, and
                           Grammar.
                        </p>
                        
                        
                        <h3>How long is each course? </h3>
                        
                        <p>Each course is one semester long. The semesters are from August to December, January
                           to May, and May to August.
                        </p>
                        
                        
                        <h3>How many hours each week are the courses?</h3>
                        
                        <p>The courses are 3 hours per week plus lab work.</p>
                        
                        
                        <h3>How are the hours distributed each week?</h3>
                        
                        <p>Some courses are one hour per day on Monday, Wednesday, Friday.</p>
                        
                        <p>Some courses are an hour and a half on Tuesday, Thursday.</p>
                        
                        <p>Some courses are an hour and a half on Monday, Wednesday.</p>
                        
                        <p>Some courses are 3 hours on one day of the week Monday through Saturday.</p>
                        
                        
                        <h3>Can a student attend only one course per semester?</h3>
                        
                        <p>Yes, a student can take anywhere from one to four EAP courses in a semester.</p>
                        
                        
                        <h3>How long does it take to complete each EAP level?</h3>
                        
                        <p>It takes one semester to complete each level, when the student attends full time (takes
                           4 courses). However, a student may take up to 4 semesters to complete a level.
                        </p>
                        
                        
                        <h3>Are there evening or Saturday courses available? </h3>
                        
                        <p>Yes, there are both evening and Saturday courses offered. </p>
                        
                        
                        <h3>Can I take courses from my major while taking EAP classes?</h3>
                        
                        <p>No, you must complete all levels of EAP before you can begin the courses in your major.</p>
                        
                        
                        <h3>Where can I practice English outside of class?</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="/students/academic-success-east/eap/">The EAP Lab</a> - East Campus (4-105)
                           </li>
                           
                           <li>
                              <a href="/students/academic-success-east/writing/">The Communications Center</a> - East Campus (4-120)
                           </li>
                           
                           <li>
                              <a href="/students/learning-support/west/communications/">The Communications Center</a> - West Campus (5-155)
                           </li>
                           
                           <li>
                              <a href="/locations/osceola/mainlab/language.php">The Language Lab</a> - Osceola Campus (1-244)
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/english-for-academic-purposes/faqs.pcf">©</a>
      </div>
   </body>
</html>