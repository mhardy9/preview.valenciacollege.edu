<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Descriptions  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/english-for-academic-purposes/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/english-for-academic-purposes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>English for Academic Purposes</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/english-for-academic-purposes/">English For Academic Purposes</a></li>
               <li>Course Descriptions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>English for Academic Purposes (EAP)</h2>
                        
                        
                        <p>English for Academic Purposes is an English program for speakers of other languages.
                           The program is designed to bring English skills to a level that will help students
                           be successful in college courses. EAP is designed for students who have some background
                           in English.
                        </p>
                        
                        
                        <h3> How are students placed in EAP?</h3>
                        
                        <p>After students have an active admissions application, they go to the Assessment Center
                           and take the PERT. If needed, it will branch into ACCUPLACER LOEP (Levels of English
                           Proficiency) test for EAP placement.
                        </p>
                        
                        
                        <h3>EAP Levels</h3>
                        
                        
                        <div class="box_style_3">
                           
                           <h4>Level 2: High Beginning - LOEP: 66-75</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Combined Skills - EAP 0281C - 6 credits</li>
                              
                           </ul>
                           
                           
                           <h4>Level 3: Low Intermediate - LOEP: 76-85</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Combined Skills - EAP 0381C - 6 credits</li>
                              
                           </ul>
                           
                           
                           <h4>Level 4: Intermediate - LOEP: 86-95</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Speech - EAP 0400C - 3 credits</li>
                              
                              <li>Reading - EAP 0420C - 3 credits</li>
                              
                              <li>Composition - EAP 0440C - 3 credits</li>
                              
                              <li>Structure - EAP 0460C - 3 credits</li>
                              
                           </ul>
                           
                           
                           <h4>Level 5: High Intermediate - LOEP: 96-105</h4>
                           				
                           <div class="row">
                              					
                              <div class="col-md-6">
                                 						
                                 <ul class="list_style_1">
                                    							
                                    <li>Speech - EAP 1500C - 3 credits</li>
                                    							
                                    <li>Reading - EAP 1520C - 3 credits</li>
                                    							
                                    <li>Composition - EAP 1540C - 3 credits</li>
                                    							
                                    <li>Structure - EAP 1560C - 3 credits</li>
                                    						
                                 </ul>
                                 					
                              </div>
                              					
                              <div class="col-md-6">
                                 		<strong>OR:</strong>
                                 		
                                 <ul class="list_style_1">
                                    		
                                    <li>Integrated Writing &amp; Grammar - EAP 1585C - 6 credits</li>
                                    		
                                    <li>Integrated Reading, Speech, &amp; Listening - EAP 1586C - 6 credits</li>
                                    		
                                 </ul>
                                 					
                              </div>
                              				
                           </div>
                           
                           
                           <h4>Level 6: Advanced - LOEP: 106-115</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Reading - EAP 1620C - 3 credits</li>
                              
                              <li>Composition - EAP 1640C - 3 credits</li>
                              
                           </ul>
                           
                           
                           <h4>English Composition - ENC 1101</h4>
                           
                        </div>
                        
                        
                        <ul class="list_style_1">
                           
                           <li>These courses are designed to help you build a solid foundation for achieving academic
                              success in your college career
                           </li>
                           
                           <li>Math sequence: Consult with an Advisor or Counselor before selecting a math path,
                              as specific degrees have specific math requirements.
                           </li>
                           
                           <li>A maximum of 12 credits from EAP college-level courses can be applied as elective
                              credit toward an Associate in Arts degree
                           </li>
                           
                           <li>All courses must be completed with a C or better</li>
                           
                           <li>If you are seeking an Associate in Arts degree, speak with your Advisor or Counselor
                              about foreign language requirement
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Locations</h3>
                        
                        
                        <h4>Assessment Office: </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>East: Bldg. 5, Room 237</li>
                           
                           <li>Osceola: Bldg. 1, Room 127 </li>
                           
                           <li>West: SSB Bldg., Room 171 </li>
                           
                           <li>Winter Park: Bldg. 1, Room 128</li>
                           
                        </ul>
                        
                        
                        <h4>Academic Success Center: </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>East: Bldg. 4, Room 105 </li>
                           
                           <li>Osceola: Bldg. 3, Room 100 </li>
                           
                           <li>West: Bldg. 5, Room 155</li>
                           
                           <li>Winter Park: Bldg. 1, Room 136</li>
                           
                        </ul>
                        
                        
                        <h4>Advising Center:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>East: Bldg. 5, Room 210</li>
                           
                           <li>Osceola: Bldg. 2, Room 140</li>
                           
                           <li>West: SSB, Room 110</li>
                           
                           <li>Winter Park: Bldg. 1, Room 206</li>
                           
                           <li>Lake Nona: Bldg. 1, Room 149</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/english-for-academic-purposes/courses.pcf">©</a>
      </div>
   </body>
</html>