<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty &amp; Staff  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/english-for-academic-purposes/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/english-for-academic-purposes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>English for Academic Purposes</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/english-for-academic-purposes/">English For Academic Purposes</a></li>
               <li>Faculty &amp; Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>Faculty &amp; Staff</h2>
                        
                        					
                        <table class="table table">
                           						
                           <caption>East</caption>
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Name</th>
                                 								
                                 <th scope="col">Extension</th>
                                 								
                                 <th scope="col">E-mail Address</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>James May <a href="#May">(Read Bio)</a>
                                    								
                                 </td>
                                 								
                                 <td>ext. 2047</td>
                                 								
                                 <td><a href="mailto:JMay@valenciacollege.edu">JMay@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Nissa Hopkins <a href="#Hopkins">(Read Bio)</a>
                                    								
                                 </td>
                                 								
                                 <td>ext. 2501 </td>
                                 								
                                 <td><a href="mailto:nhopkins3@valenciacollege.edu">nhopkins3@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Tatiana Bizon <a href="#Bizon">(Read Bio)</a>
                                    								
                                 </td>
                                 								
                                 <td>ext. 2406</td>
                                 								
                                 <td><a href="mailto:tbizon@valenciacollege.edu">tbizon@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Sarah Melanson <a href="#Melanson">(Read Bio)</a>
                                    								
                                 </td>
                                 								
                                 <td>ext. 2457</td>
                                 								
                                 <td><a href="mailto:smelanson1@valenciacollege.edu">smelanson1@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <caption>Osceola</caption>
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Name</th>
                                 								
                                 <th scope="col">Extension</th>
                                 								
                                 <th scope="col">E-mail Address</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Christie Pickeral</td>
                                 								
                                 <td>ext. 4092</td>
                                 								
                                 <td><a href="mailto:cpickeral@valenciacollege.edu">cpickeral@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Steve Cunningham</td>
                                 								
                                 <td>ext. 4827</td>
                                 								
                                 <td><a href="mailto:scunnningham@valenciacollege.edu">scunnningham@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>John McFarland </td>
                                 								
                                 <td>ext. 4125 </td>
                                 								
                                 <td><a href="mailto:jmcfarland6@valenciacollege.edu">jmcfarland6@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Shawn Pollgreen </td>
                                 								
                                 <td>ext. 4292 </td>
                                 								
                                 <td><a href="mailto:spollgre@valenciacollege.edu">spollgre@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        					
                        <table class="table table">
                           						
                           <caption>West</caption>
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th scope="col">Name</th>
                                 								
                                 <th scope="col">Extension</th>
                                 								
                                 <th scope="col">E-mail Address</th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Wendy Wish-Bogue </td>
                                 								
                                 <td>ext. 1338</td>
                                 								
                                 <td><a href="mailto:wwishboque@valenciacollege.edu">wwishboque@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Richard Sansone <a href="#Sansone">(Read Bio)</a>
                                    								
                                 </td>
                                 								
                                 <td>ext. 1383 </td>
                                 								
                                 <td><a href="mailto:rsansone@valenciacollege.edu">rsansone@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Jennifer Britton </td>
                                 								
                                 <td>ext. 1470 </td>
                                 								
                                 <td><a href="mailto:jbritton@valenciacollege.edu">jbritton@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Aaron Powell </td>
                                 								
                                 <td>ext. 1356 </td>
                                 								
                                 <td><a href="mailto:apowell14@valenciacollege.edu">apowell14@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Karen Murray <a href="#Murray">(Read Bio)</a> 
                                 </td>
                                 								
                                 <td>ext. 1485 </td>
                                 								
                                 <td><a href="mailto:kmurray10@valenciacollege.edu">kmurray10@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>Gina Dalle Molle </td>
                                 								
                                 <td>ext. 1503 </td>
                                 								
                                 <td><a href="mailto:gdallemolle@valenciacollege.edu">gdallemolle@valenciacollege.edu</a></td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <h3>Professor Biographies:</h3>
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							<img src="images/hopkins.jpg" alt="" class="img-circle img-responsive styled_2">
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>Nissa Hopkins - East Campus <a name="Hopkins" id="Hopkins"></a></h4>
                              							
                              <p>Hello! I’m Nissa, and though I’m new to Valencia (I started here in August of 2011),
                                 I’ve been teaching ESOL and EAP for ten years, some in California, but mostly at Seminole
                                 State College. I have an M.S. in TESOL/Edu from Shenandoah University, and a BA in
                                 Asian Studies from California State University, Sacramento. I also served in the U.S.
                                 Army as a linguist/Japanese interpreter for eight years, and I graduated from the
                                 intensive Japanese course at the Defense Language Institute in Monterey, CA. I was
                                 an exchange student to Japan in high school, and I have traveled extensively; these
                                 experiences helped propel me into my teaching career. I love languages, other cultures,
                                 traveling, trying new and delicious food, yoga, martial arts, reading, and all things
                                 geeky. I feel fortunate to do what I truly love as a career!
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        					
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							<img src="images/bizon.jpg" alt="" class="img-circle img-responsive styled_2">
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>Tanya Bizon - East Campus <a name="Bizon" id="Bizon"></a></h4>
                              							
                              <p>I am Russian. I studied English in a Russian public school for 6 years and in a Russian
                                 university for 5 more years. I have an MA in English and Criminal Justice from Russia.
                                 When I moved to the US in 1999, I acquired an MA in English from UCF. I am married
                                 and have two kids- Anya and Alexander. I enjoy teaching English to ESL students; I
                                 love hearing their stories about their countries and experience in the US. I have
                                 always had high expectations of myself, and in class I push my students to achieve
                                 their maximum and best! I’ve been with Valencia since 2003 and love it. I also love
                                 educated people, beautiful things, traveling, and good chocolate!
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							<img src="images/may.jpg" alt="" class="img-circle img-responsive styled_2">
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>James May - East Campus <a name="May" id="May"></a></h4>
                              							
                              <p>Hi! A native to central Florida, I originally started teaching English as a foreign
                                 language while studying abroad in Mexico back in 1991. After studying Spanish literature
                                 and Portuguese at the University of Florida, I became a Korean lingust for the U.S.
                                 Army. My time in the military also gave me the oportunity to teach EFL in Korea. In
                                 1998, I went back to UF for a masters and a doctorate in multilingual, multicultural
                                 education. My writing and research interests include CALL, test development, standard
                                 setting, and EAP professional development. I also enjoy learning languages and working
                                 with students from varied linguistic and cultural backgrounds.
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							<img src="images/melanson.jpg" alt="" class="img-circle img-responsive styled_2">
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>Sarah Melanson - East Campus <a name="Melanson" id="Melanson"></a></h4>
                              							
                              <p>Hi! I'm Sarah, I'm British, and I started my Valencia career in May 2002 at Osceola
                                 Campus. I've been here at East Campus since August 2003. Before coming to the US I
                                 taught in Oxford, England, and Alicante, Spain. I have an MA in Teaching English for
                                 Academic and Specific Purposes from Oxford Brookes University. Before becoming a teacher
                                 I worked for ten years as a medical transcriptionist, and I've also spent three years
                                 working as a waitress in the German Alps. I have a working knowledge of Spanish, German,
                                 and French, and I love music, traveling, and experiencing different cultures.
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							<img src="images/sansone.jpg" alt="" class="img-circle img-responsive styled_2">
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>Richard Sansone - West Campus<a name="Sansone" id="Sansone"></a></h4>
                              							
                              <p>Hello, my name is Richard. Please don’t let the formal photo mislead you: I am really
                                 a down-to-earth person who really loves teaching here because I believe in Valencia
                                 and its students (although I wish they would change the slogan to a better place to
                                 finish...because it is!) Although I was originally hired here to teach English to
                                 non-natives, I was asked to develop a Portuguese language program in 1990 and have
                                 been happy to teach both Portuguese and EAP language classes here ever since. I think
                                 about teaching languages as a vehicle for cross-cultural understanding, access and
                                 empowerment… so I create a lot of opportunities for my students to enhance both their
                                 skills and horizons outside of the classroom. These include projects such as the Valencia
                                 Brazilian film festival, EAP career exploration project, summer study and Spring Break
                                 in Brazil, etc. I studied at Rollins College as an Undergraduate and completed two
                                 Masters Degrees at the University of Arizona thereafter – the first in Latin American
                                 Studies (Spanish and Portuguese) and the second in Teaching English to Speakers of
                                 Other Languages. Before coming to Valencia, I lived and worked in Europe and South
                                 America, mostly as academic director for college semester abroad programs, and I speak
                                 Portuguese, Spanish, Italian and French.
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="row">
                           						
                           <div class="col-md-3">
                              							&nbsp;
                              						
                           </div>
                           						
                           <div class="col-md-9">
                              							
                              <h4>Karen Murray - West Campus <a name="Murray" id="Murray"></a></h4>
                              							
                              <p>I started teaching English as a Second Language (ESL) at Valencia in the fall of 2004,
                                 and I really enjoy my job. My first experience teaching ESL was at the University
                                 of Seville in Spain in 1990, and I've been teaching language (ESL, English for native
                                 speakers, and Spanish) since 1985. I spent 3 years in Seville teaching at the university
                                 and two colleges, and before coming to Valencia I taught for 10 years at Indiana University
                                 in Bloomington, Indiana. My education includes three Master's degrees in various fields
                                 of education. I consider myself a lifelong student: I have studied German, Spanish,
                                 Italian, and most recently I have been studying Greek. I know how challenging it can
                                 be to learn a second language! What I love the most about my job is the opportunity
                                 it gives me to meet people from all over the world and learn about their cultures.
                                 One of my favorite hobbies is traveling, and being in an ESL classroom is a little
                                 like traveling around the world!
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/english-for-academic-purposes/faculty.pcf">©</a>
      </div>
   </body>
</html>