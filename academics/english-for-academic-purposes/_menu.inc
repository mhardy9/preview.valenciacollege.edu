<ul>
	<li><a href="index.php">English for Academic Purposes</a></li>
	<li class="submenu">
		<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="courses.php">Course Descriptions</a></li>
			<li><a href="faqs.php">FAQs</a></li>
			<li><a href="faculty.php">Faculty &amp; Staff</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/eap/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
</ul>