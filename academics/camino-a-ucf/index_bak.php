<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Camino a UCF  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, camino, road, ucf">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/camino-a-ucf/index_bak.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/camino-a-ucf/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/camino-a-ucf/">Camino A Ucf</a></li>
               <li>Camino a UCF </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h2>About</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Camino a UCF is designed for any DirectConnect to UCF student who intends to major
                              in Business, Criminal Justice, Hospitality Management or Psychology/Social Sciences
                              (i.e. Anthropology, Political Science or Sociology).
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>Why Camino</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Guaranteed schedule</li>
                              
                              <li>Cohort program</li>
                              
                              <li>Peer Mentors</li>
                              
                              <li>Enhanced pathway to UCF</li>
                              
                              <li>Dedicated UCF and Valencia Advisors</li>
                              
                              <li>Guaranteed admission to UCF</li>
                              
                              <li>Spirit Splash at UCF</li>
                              
                              <li>Fall Football Game</li>
                              
                              <li>Spring Basketball Game</li>
                              
                              <li>Exclusive Tour</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>New Recruits: Camino is for you if</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>You plan to&nbsp;<strong>DirectConnect</strong>&nbsp;to UCF.
                              </li>
                              
                              <li>Business, Psychology/ Social Sciences, Hospitality or Ciminal Justice are your target
                                 majors.
                              </li>
                              
                              <li>You are seeking a challenging, immersive and rewarding undergraduate experience.</li>
                              
                              <li>Are able to commit to a full-time schedule that may require 40-50 hours of class time
                                 and study.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">      
                        <img src="/_resources/img/academics/camino-a-ucf/camino_valencia_logo.png" height="357" width="203" align="right" alt="Camino a Valencia Logo">
                        <img src="/_resources/img/academics/camino-a-ucf/camino_ucf_logo.png" alt="Camino a UCF Logo">
                        
                        <p></p>
                        <br>
                        
                        <p>Camino NSE Faculty Advisors:<br>
                           Tanisha Castor - Business Cohort<br>
                           Jorge A. Valladares - Psychology Cohort
                        </p>
                        
                        <p>Camino Program Coordinator:<br>
                           Dr. Thomas Takayama, Dean of Humanities and Social Science
                        </p>         
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/camino-a-ucf/index_bak.pcf">©</a>
      </div>
   </body>
</html>