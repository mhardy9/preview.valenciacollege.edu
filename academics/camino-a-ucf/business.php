<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business Track  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, camino, road, ucf, academics, program, track, business">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/camino-a-ucf/business.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/camino-a-ucf/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/camino-a-ucf/">Camino A Ucf</a></li>
               <li>Business Track </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h2>Business</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>Year 1</h3>
                           
                           
                           <p>
                              
                           </p>
                           
                           <table class="table ">
                              
                              <tr>
                                 
                                 <th style="font-size: 10pt">Fall (15 hours)</th>
                                 
                                 <th style="font-size: 10pt">Spring (15 hours)</th>
                                 
                              </tr>
                              
                              <tr>
                                 <td colspan="2">&nbsp;</td>
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    Freshman Composition (English) - Tuesday/ Thursday 11:30-12:45pm<br>
                                    Introduction to Business - Tuesday/ Thursday 2:30-3:45pm<br>
                                    Intermediate Algebra - Tuesday/Thursday 4:00-5:15pm<br>
                                    New Student Experience - Friday 8:30-11:15<br>
                                    Computer Fundamentals and Applications - Online<br>
                                    
                                 </td>
                                 
                                 <td>
                                    Freshman Composition II - Tuesday/ Thursday 11:30-12:45pm<br>
                                    Principles of Management - Tuesday/ Thursday 2:30-3:45pm<br>
                                    College Algebra - Tuesday/Thursday 4:00-5:15pm<br>
                                    Fundamentals of Speech - Friday 8:30 - 11:15 (Hybrid)<br>
                                    Introduction to Humanities - Online<br>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           
                           
                           <h3>Year 2</h3>
                           
                           <p>
                              
                              
                           </p>
                           
                           <table class="table ">
                              
                              <tr>
                                 
                                 <th style="font-size: 10pt">Fall (15 hours) (15 hours)</th>
                                 
                                 <th style="font-size: 10pt">Spring</th>
                                 
                              </tr>
                              
                              <tr>
                                 <td colspan="2">&nbsp;</td>
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    PHI 2600 (Online) - Ethics and Critical Thinking<br>
                                    MAC 2233 - Calculus for Business<br>
                                    ECO 2013 - Principles of Economics - Macro<br>
                                    BSC 1005 - Biological Science<br>
                                    ACG 2021 - Principles of Financial Accounting<br>
                                    
                                 </td>
                                 
                                 <td>
                                    POS 2112 - State and Local Government<br>
                                    STA 2023 - Statistical Methods<br>
                                    ACG 2071 - Principles of Managerial Accounting<br>
                                    BSC 1026 (online) - Biology of Human Sexuality<br>
                                    ECO 2023 - Principles of Economics - Micro<br>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/camino-a-ucf/business.pcf">©</a>
      </div>
   </body>
</html>