<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Past President's and Dean's List  | Valencia College</title>
      <meta name="Description" content="Past Presidents and Deans List | Current Students | Students | Valencia College">
      <meta name="Keywords" content="college, school, educational, valencia, current, students, deans, list">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/deans-list/past.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/deans-list/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Deans List</h1>
            <p>Past President's and Dean's List
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/deans-list/">Deans List</a></li>
               <li>Past President's and Dean's List </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Past Lists</h2>
                        
                        				<a target="_blank" href="/academics/deans-list/fall2017.php">Fall 2017</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2017.php">Summer 2017</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2017.php">Spring 2017</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2016.php">Fall 2016</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2016.php">Summer 2016</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2016.php">Spring 2016</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2015.php">Fall 2015</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2015.php">Summer 2015</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2015.php">Spring 2015</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2014.php">Fall 2014</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2014.php">Summer 2014</a><br>                
                        				<a target="_blank" href="/academics/deans-list/spring2014.php">Spring 2014</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2013.php">Fall 2013</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2013.php">Summer 2013</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2013.php">Spring 2013</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2012.php">Fall 2012</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2012.php">Summer 2012</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2012.php">Spring 2012</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2011.php">Fall 2011</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2011.php">Summer 2011</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2011.php">Spring 2011</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2010.php">Fall 2010</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2010.php">Summer 2010</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2010.php">Spring 2010</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2009.php">Fall 2009</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2009.php">Summer 2009</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2009.php">Spring 2009</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2008.php">Fall 2008</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2008.php">Summer 2008</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2008.php">Spring 2008</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2007.php">Fall 2007</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2007.php">Summer 2007</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2007.php">Spring 2007</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2006.php">Fall 2006</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2006.php">Summer 2006</a><br>
                        				<a target="_blank" href="/academics/deans-list/spring2006.php">Spring 2006</a><br>
                        				<a target="_blank" href="/academics/deans-list/fall2005.php">Fall 2005</a><br>
                        				<a target="_blank" href="/academics/deans-list/summer2005.php">Summer 2005</a><br>
                        			
                     </div>
                     		
                  </div>
                  
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/deans-list/past.pcf">©</a>
      </div>
   </body>
</html>