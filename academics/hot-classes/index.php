<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hot Classes | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/hot-classes/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/hot-classes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Hot Classes</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Hot Classes</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Hot Classes - Fall 2017 </h2>
                        
                        <p><em>Note</em>: For additional information on any class listed - click on its CRN.
                        </p>
                        
                        <div>
                           
                           <h3>East Campus </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>LAT 1120 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=LAT&amp;crse_in=1120&amp;crn_in=12776" target="_blank">12776</a></td>
                                    
                                    <td>Elementary Latin I</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0200P - 0340P</td>
                                    
                                    <td>Jennifer Taylor</td>
                                    
                                    <td>EC-006-217A</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>SLS 2940 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=SLS&amp;crse_in=2940&amp;crn_in=17000" target="_blank">17000</a></td>
                                    
                                    <td>Service Learning</td>
                                    
                                    <td> &nbsp; &nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0230P - 0345P</td>
                                    
                                    <td>Nicole Valentino</td>
                                    
                                    <td>EC-003- </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>Lake Nona </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="7">Currently No Hot Classes for this Campus</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>Osceola Campus </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CJE 2062 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=CJE&amp;crse_in=2062&amp;crn_in=17326" target="_blank">17326</a></td>
                                    
                                    <td>Peace, Conflict and the Police</td>
                                    
                                    <td> - </td>
                                    
                                    <td> - </td>
                                    
                                    <td> STAFF</td>
                                    
                                    <td> - </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>School of Public Safety </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="7">Currently No Hot Classes for this Campus</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>West Campus </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>AFA 2000 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=AFA&amp;crse_in=2000&amp;crn_in=16459" target="_blank">16459</a></td>
                                    
                                    <td>Intro African-American Exp</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Carmen Laguer Diaz</td>
                                    
                                    <td>WC-011-348</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>CLP 1001 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=CLP&amp;crse_in=1001&amp;crn_in=12811" target="_blank">12811</a></td>
                                    
                                    <td>Psychology of Adjustment</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Nancy Rizzo</td>
                                    
                                    <td>WC-003-213</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>ENC 1101 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=ENC&amp;crse_in=1101&amp;crn_in=17560" target="_blank">17560</a></td>
                                    
                                    <td>Freshman Comp I</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Stacey DiLiberto</td>
                                    
                                    <td>WC-HSB-221</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>HLP 2550C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HLP&amp;crse_in=2550C&amp;crn_in=14860" target="_blank">14860</a></td>
                                    
                                    <td>Concepts of Personal Training</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1130A - 1245P</td>
                                    
                                    <td>Pam Hodges</td>
                                    
                                    <td>WC-007-231</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>HSC 1400C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HSC&amp;crse_in=1400C&amp;crn_in=15512" target="_blank">15512</a></td>
                                    
                                    <td>First Aid/CPR</td>
                                    
                                    <td>M&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0400P - 0645P</td>
                                    
                                    <td>Colleen Ryan</td>
                                    
                                    <td>WC-011-340</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>IDH 2120 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=IDH&amp;crse_in=2120&amp;crn_in=10148" target="_blank">10148</a></td>
                                    
                                    <td>IDH in General Ed III Honors</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0830A - 1115A</td>
                                    
                                    <td>Adrienne Mathews</td>
                                    
                                    <td>WC-HSB-221</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>IDH 2120 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=IDH&amp;crse_in=2120&amp;crn_in=17564" target="_blank">17564</a></td>
                                    
                                    <td>IDH in General Ed III</td>
                                    
                                    <td> - </td>
                                    
                                    <td> - </td>
                                    
                                    <td> STAFF</td>
                                    
                                    <td> - </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 1121C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=1121C&amp;crn_in=17346" target="_blank">17346</a></td>
                                    
                                    <td>Yoga</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0830A - 0920A</td>
                                    
                                    <td>Mary Berg</td>
                                    
                                    <td>WC-HSB-123</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 1121C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=1121C&amp;crn_in=17350" target="_blank">17350</a></td>
                                    
                                    <td>Yoga</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1050A</td>
                                    
                                    <td>Mary Berg</td>
                                    
                                    <td>WC-HSB-123</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 1131C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=1131C&amp;crn_in=14863" target="_blank">14863</a></td>
                                    
                                    <td>Weight Training I</td>
                                    
                                    <td> - </td>
                                    
                                    <td> - </td>
                                    
                                    <td>Ronald Owens</td>
                                    
                                    <td>WC-HSB-124</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 1171 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=1171&amp;crn_in=17352" target="_blank">17352</a></td>
                                    
                                    <td>Belly Dance Aerobics</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1050A</td>
                                    
                                    <td>Melanie LaJoie</td>
                                    
                                    <td>WC-HSB-123</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 1405 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=1405&amp;crn_in=11576" target="_blank">11576</a></td>
                                    
                                    <td>Self Defense For Women</td>
                                    
                                    <td> &nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0700P - 0845P</td>
                                    
                                    <td>Calvin Thomas</td>
                                    
                                    <td>WC-HSB-123</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 2104C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=2104C&amp;crn_in=17516" target="_blank">17516</a></td>
                                    
                                    <td>Personal Fitness and Wellness</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Ronald Owens</td>
                                    
                                    <td>WC-001-239</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 2104C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=2104C&amp;crn_in=17517" target="_blank">17517</a></td>
                                    
                                    <td>Personal Fitness and Wellness</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Ronald Owens</td>
                                    
                                    <td>WC-007-233</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PEM 2163C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PEM&amp;crse_in=2163C&amp;crn_in=15895" target="_blank">15895</a></td>
                                    
                                    <td>Zumba Fitness</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0530P - 0620P</td>
                                    
                                    <td>Lorie Bellot</td>
                                    
                                    <td>WC-HSB-123</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PET 2622C </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PET&amp;crse_in=2622C&amp;crn_in=15547" target="_blank">15547</a></td>
                                    
                                    <td>Care/Prev of Athletic Injuries</td>
                                    
                                    <td>M&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0700P - 0945P</td>
                                    
                                    <td>Colleen Ryan</td>
                                    
                                    <td>WC-011-340</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PHI 2010 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PHI&amp;crse_in=2010&amp;crn_in=17558" target="_blank">17558</a></td>
                                    
                                    <td>Philosophy</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0830A - 0945A</td>
                                    
                                    <td>Travis Rodgers</td>
                                    
                                    <td>WC-HSB-221</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>PSY 2930 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=PSY&amp;crse_in=2930&amp;crn_in=15556" target="_blank">15556</a></td>
                                    
                                    <td>Cognitive Psychology</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1000A - 1115A</td>
                                    
                                    <td>Melonie Sexton</td>
                                    
                                    <td>WC-011-350</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>SOP 2772 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=SOP&amp;crse_in=2772&amp;crn_in=10503" target="_blank">10503</a></td>
                                    
                                    <td>Human Sexuality</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1130A - 1245P</td>
                                    
                                    <td>Nancy Rizzo</td>
                                    
                                    <td>WC-003-213</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>WOH 2003 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=WOH&amp;crse_in=2003&amp;crn_in=15255" target="_blank">15255</a></td>
                                    
                                    <td>A History of Genocide</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0100P - 0215P</td>
                                    
                                    <td>Alyce Miller</td>
                                    
                                    <td>WC-011-217</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>Winter Park Campus </h3>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Days</th>
                                    
                                    <th>Time</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>ENC 1101 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=ENC&amp;crse_in=1101&amp;crn_in=13640" target="_blank">13640</a></td>
                                    
                                    <td>Freshman Comp I</td>
                                    
                                    <td>M&nbsp; &nbsp;W&nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0100P - 0215P</td>
                                    
                                    <td>Jean Marie Fuhrman</td>
                                    
                                    <td>WP-001-143</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>HUM 2310 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=HUM&amp;crse_in=2310&amp;crn_in=17148" target="_blank">17148</a></td>
                                    
                                    <td>Mythology</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0830A - 0945A</td>
                                    
                                    <td>Paul Chapman</td>
                                    
                                    <td>WP-001-242</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>SLS 1122 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=SLS&amp;crse_in=1122&amp;crn_in=17374" target="_blank">17374</a></td>
                                    
                                    <td>New Student Experience</td>
                                    
                                    <td> &nbsp;T&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>0700P - 0815P</td>
                                    
                                    <td>Aaron Bergeson</td>
                                    
                                    <td>WP-001-108</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>SPC 1017 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=SPC&amp;crse_in=1017&amp;crn_in=15575" target="_blank">15575</a></td>
                                    
                                    <td>Interpersonal Communication</td>
                                    
                                    <td> &nbsp; &nbsp; &nbsp;R&nbsp; &nbsp; &nbsp; </td>
                                    
                                    <td>1130A - 1245P</td>
                                    
                                    <td>Suzette Ashton</td>
                                    
                                    <td>WP-001-115</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <h3>Online Courses  </h3>
                           
                           <p>Online Courses have no face to face sessions with the faculty member.</p>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Course</th>
                                    
                                    <th>CRN</th>
                                    
                                    <th>Title</th>
                                    
                                    <th>Instructor</th>
                                    
                                    <th>Room</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>ARH 1000 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=ARH&amp;crse_in=1000&amp;crn_in=14769" target="_blank">14769</a></td>
                                    
                                    <td>Art Appreciation</td>
                                    
                                    <td>Sheila Levi</td>
                                    
                                    <td>ONLINE</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>INP 1301 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=INP&amp;crse_in=1301&amp;crn_in=11787" target="_blank">11787</a></td>
                                    
                                    <td>Psychology In Bus-Industry</td>
                                    
                                    <td>Stephanie Grimes</td>
                                    
                                    <td>ONLINE</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>MTB 1103 </td>
                                    
                                    <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201810&amp;subj_in=MTB&amp;crse_in=1103&amp;crn_in=14983" target="_blank">14983</a></td>
                                    
                                    <td>Business Mathematics</td>
                                    
                                    <td>Marva Pryor</td>
                                    
                                    <td>ONLINE</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/hot-classes/index.pcf">©</a>
      </div>
   </body>
</html>