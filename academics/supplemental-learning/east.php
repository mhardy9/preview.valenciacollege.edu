<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Campus  | Valencia College</title>
      <meta name="Description" content="East Campus | Supplemental Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, supplemental, learning, east, campus">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/supplemental-learning/east.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/supplemental-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Supplemental Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/supplemental-learning/">Supplemental Learning</a></li>
               <li>East Campus </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>East Campus</h2>
                        
                        
                        <p><img height="387" src="/academics/supplemental-learning/images/2015-08-2818.59.26.jpg" width="500"><br>
                           <br>
                           
                        </p>
                        
                        <h3>Welcome Students!</h3>
                        Supplemental Learning courses are enhanced by small group sessions led by Supplemental
                        Learning Leaders (former students) to assist students in becoming proactive, independent
                        learners. <br>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Location &amp; Hours </h3>
                        
                        <p>Supplemental Learning Sessions take place on East Campus in room 4-216.<u><br>
                              </u><br>
                           Supplemental Learning Sessions often take place before and/or after a class meeting.
                           Schedules are coordinated between Instructors, S.L. Leaders and Students. Please see
                           your individual S.L. Leader for the dates and times of your Sessions.
                           
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Social Media &amp; Applications</h3>
                        
                        <strong>Like us on Facebook</strong><a href="https://www.facebook.com/pages/Supplemental-Learning-VC-East/245622725448066?fref=pb&amp;hc_location=profile_browser"><br>
                           <img border="0" height="41" src="/academics/supplemental-learning/images/Facebook_000.jpg" width="41"></a><br>
                        <br>
                        Are you interested in becoming a Supplemental Learning Leader?<br>
                        Click <a href="/academics/supplemental-learning/documents/LeaderApplication_000.pdf"><em>here</em></a> to find the application &amp; click <a href="/academics/supplemental-learning/documents/LeaderEmployeeOverview-forinterviewday.pdf"><em>here</em></a> for a expectation overview.<br>
                        <br>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Schedules</h3>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/supplemental-learning/east.pcf">©</a>
      </div>
   </body>
</html>