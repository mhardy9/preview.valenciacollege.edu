<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Contact Us  | Valencia College</title>
      <meta name="Description" content="Contact Us | Supplemental Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, supplemental, learning, contact">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/supplemental-learning/contact.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/supplemental-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Supplemental Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/supplemental-learning/">Supplemental Learning</a></li>
               <li>Contact Us </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>SL Coordinators</h2>
                        
                        <p><strong>Jennifer Adams</strong> - East Campus Mathematics Coordinator<br>
                           <img height="115" src="/_resources/img/academics/supplemental-learning/JenniferAdams.jpg" width="115"></p>
                        
                        <p>  <a href="mailto:jmccormick5@valenciacollege.edu">jmccormick5@valenciacollege.edu</a><br>
                           407-582-2023 
                        </p>
                        
                        <p><br>
                           <strong>Daeria Tenery</strong> - East Campus Science Coordinator<br>
                           407-582-2583<br>
                           <a href="mailto:dtenery@valenciacollege.edu">dtenery@valenciacollege.edu</a><br>
                           <strong><br>
                              Robert Schachel</strong> - East Campus English Coordinator<br>
                           407-582-2634<br>
                           <a href="mailto:rschachel@valenciacollege.edu">rschachel@valenciacollege.edu</a><strong><br>
                              <br>
                              </strong><strong>Boris Nguyen</strong> - West Campus Coordinator<br>
                           <a href="mailto:BNguyen@valenciacollege.edu">bnguyen@valenciacollege.edu</a><br>
                           407-582-1649 <br>
                           <br>
                           <strong> James Coddington- </strong>Osceola Campus Mathematics Coordinator<br>
                           <img height="115" src="/_resources/img/academics/supplemental-learning/JamesCoddington.jpg" width="115"></p>
                        
                        <p>  <a href="mailto:jcoddington@valenciacollege.edu">jcoddington@valenciacollege.edu</a></p>
                        
                        
                        <p>407-582-4346<br>
                           <br>
                           <strong>Nelson Torres</strong> - Osceola Campus Science Coordinator                  
                        </p>
                        
                        <p><img height="115" src="/_resources/img/academics/supplemental-learning/NelsonTorres.jpg" width="115"></p>
                        
                        <p>                      <a href="mailto:ntorresarroyo@valenciacollege.edu">ntorresarroyo@valenciacollege.edu<br>
                              </a> 407-582-4341<br>
                           <strong><br>
                              Tanyi Colón</strong> - Osceola Campus English Coordinator<br>
                           <strong><img height="115" src="/_resources/img/academics/supplemental-learning/TanyiColon.jpg" width="115"></strong>  <br>
                           <a href="mailto:tcolon3@valenciacollege.edu">tcolon3@valenciacollege.edu</a><br>
                           407-582-4903
                        </p>
                        
                        <p>                    <strong>Damion Hammock</strong> - Winter Park Campus Coordinator<br>
                           <a href="mailto:dhammock@valenciacollege.edu">dhammock@valenciacollege.edu</a><br>
                           407-582-6917 
                        </p>
                        
                        <p><br>
                           <strong>Michael Blackburn</strong> - Lake Nona Campus Coordinator <br>
                           <img height="115" src="/_resources/img/academics/supplemental-learning/MichaelBlackburn.jpg" width="115">                  <br>
                           <a href="mailto:mblackburn4@valenciacollege.edu">mblackburn4@valenciacollege.edu</a><br>
                           407-582- 7222
                           
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/supplemental-learning/contact.pcf">©</a>
      </div>
   </body>
</html>