<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Supplemental Learning  | Valencia College</title>
      <meta name="Description" content="Supplemental Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, students, academics, supplemental, learning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/supplemental-learning/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/supplemental-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Supplemental Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Supplemental Learning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a> 
                        
                        
                        <h2>Program Information</h2>
                        
                        <p>These courses are enhanced by Supplemental Learning (SL). SL classes are supported
                           by small group sessions led by Supplemental Learning Leaders (former students) who
                           are selected because they passed the course with a high grade. These study sessions
                           are regularly scheduled, casual sessions in which students from your class compare
                           notes, discuss assignments, and develop organizational tools and study skills. Studies
                           have shown students who participate in these sessions make better grades in the course.
                        </p>
                        
                        <hr>
                        
                        <h3>Videos: Supplemental Learning: First Day / Introduction</h3>
                        
                        <ul>
                           
                           <li><a title="Welcome to Supplemental Learning (SL)" href="/academics/supplemental-learning/index.html#inline_content1">Welcome to Supplemental Learning (SL)</a></li>
                           
                           
                           <li><a title="The History of the SL" href="/academics/supplemental-learning/index.html#inline_content2">The History of the SL</a></li>
                           
                           <li><a title="How SL works" href="/academics/supplemental-learning/index.html#inline_content3">How SL works</a></li>
                           
                           <li><a title="Leaders &amp; Students" href="/academics/supplemental-learning/index.html#inline_content4">Leaders &amp; Students</a></li>
                           
                           <!--<li><a title="Supplemental Learning - Board of Trustees Meeting" href="https://youtu.be/9W3uwkZFdcA">Supplemental Learning - Board of Trustees Meeting</a></li>-->
                           
                        </ul>
                        	
                        <div class="aspect-ratio"><iframe class="iframe" allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/9W3uwkZFdcA" width="560"></iframe></div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/supplemental-learning/index.pcf">©</a>
      </div>
   </body>
</html>