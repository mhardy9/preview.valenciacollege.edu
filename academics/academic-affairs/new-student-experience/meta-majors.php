<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Experience | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/new-student-experience/meta-majors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/new-student-experience/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>New Student Experience</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/new-student-experience/">New Student Experience</a></li>
               <li>New Student Experience</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>What is a Meta-Major?</h2>
                        
                        <p>Meta-Majors are collections of academic majors that have related courses.&nbsp; Meta-Majors
                           cluster groups of majors that fit within a career area.&nbsp; There are eight Meta-Majors
                           used by colleges in the state of Florida.&nbsp; Within each Meta-Major are degrees and
                           certificates that have related courses.&nbsp; The intent of selecting a Meta-Major is to
                           help students choose a major and degree based on their interests, knowledge, skills
                           and abilities.&nbsp; Selecting a Meta-Major will also help students select classes that
                           relate to a specific degree.&nbsp; All degree-seeking students (AA and AS) need to decide
                           upon a Meta-Major.&nbsp; 
                        </p>
                        
                        <p>The eight Meta-Majors are:&nbsp; (a) Arts, Humanities, Communication and Design, (b) Business,
                           (c) Education, (d) Health Sciences, (e) Industry/Manufacturing and Construction, (f)
                           Public Safety, (g) Science, Technology, Engineering, and Mathematics, and (h) Social
                           and Behavioral Sciences and Human Services.&nbsp;
                        </p>
                        
                        <p> With a Meta-Major, students can narrow down their choice in major and begin developing
                           an educational plan that leads to degree completion.&nbsp; Valencia introduces Meta-Majors
                           on the application.&nbsp; During the assessment process students take a Career Review that
                           provides more in-depth information on Meta-Majors.&nbsp; Students select two Meta-Majors
                           of interest after reviewing specific careers related to each area.&nbsp; During New Student
                           Orientation, Advisors review these choices with students and discuss possible degree
                           programs and math pathways.&nbsp; Student register for their first term in the appropriate
                           courses that align with their educational path.
                        </p>
                        
                        <h2><a href="documents/Meta-Major-chart-1617.pdf">Valencia's Meta-Major and Programs of Study Chart (2016-17 version) </a></h2>
                        
                        <h2>
                           <span>START RIGHT GUIDES</span> provide the list of courses a new student should complete in the first few terms
                           of enrollment according to their Program of Study or Transfer Plan at Valencia. 
                        </h2>
                        
                        
                        
                        <div>
                           
                           
                           <h3>Associate in Science (AS)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Accounting-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:29 AM">Start-Right-Courses-Accounting-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Baking-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:29 AM">Start-Right-Courses-Baking-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Biotechnology-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Biotechnology-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Building-Construction-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Building-Construction-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Business-Admin-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Business-Admin-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Civil-Surveying-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Civil-Surveying-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Computer-Info-Tech-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Computer-Info-Tech-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Computer-Programming-&amp;-Analysis-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:30 AM">Start-Right-Courses-Computer-Programming-&amp;-Analysis-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Criminal-Justice-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-Criminal-Justice-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Culinary-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-Culinary-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-DM-Mobile-Journalism-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-DM-Mobile-Journalism-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-DM-Motion-Graphics-Specialization-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-DM-Motion-Graphics-Specialization-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-DM-Video-Production-Specialization-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-DM-Video-Production-Specialization-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-DM-Web-Dev-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:31 AM">Start-Right-Courses-DM-Web-Dev-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Drafting-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Drafting-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Electronics-Eng-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Electronics-Eng-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Entertainment-Design-&amp;-Tech-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Entertainment-Design-&amp;-Tech-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Film-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Film-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Fire-Sciences-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Fire-Sciences-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Graphics-&amp;-Interactive-Design-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:32 AM">Start-Right-Courses-Graphics-&amp;-Interactive-Design-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Hosptitality-Tourism-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:33 AM">Start-Right-Courses-Hosptitality-Tourism-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Medical-Office-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:33 AM">Start-Right-Courses-Medical-Office-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Network-Eng.-Tech-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:33 AM">Start-Right-Courses-Network-Eng.-Tech-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Office-Admin-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:33 AM">Start-Right-Courses-Office-Admin-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Paralegal-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:33 AM">Start-Right-Courses-Paralegal-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Plant-Science-and-Agricultural-Tech-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:34 AM">Start-Right-Courses-Plant-Science-and-Agricultural-Tech-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Restaurant-&amp;-Food-Svcs-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:34 AM">Start-Right-Courses-Restaurant-&amp;-Food-Svcs-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-S-&amp;-MT-Audio-&amp;-Bus-Mgmt-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:34 AM">Start-Right-Courses-S-&amp;-MT-Audio-&amp;-Bus-Mgmt-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-S-&amp;-MT-Audio-Engr-Special-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:35 AM">Start-Right-Courses-S-&amp;-MT-Audio-Engr-Special-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-S-&amp;-MT-Music-Prod-Spec-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:35 AM">Start-Right-Courses-S-&amp;-MT-Music-Prod-Spec-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-S-&amp;-MT-Sound-Production-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:35 AM">Start-Right-Courses-S-&amp;-MT-Sound-Production-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Courses-Supervision-and-Mgmt-Tech-AS.pdf" target="_blank" title="Last Updated 7/27/17 11:35 AM">Start-Right-Courses-Supervision-and-Mgmt-Tech-AS</a>
                                       
                                    </li> 
                                    <li> <a href="documents/AS/Start-Right-Guide-AA-(TC-EMT)-Pending-EMS.pdf" target="_blank" title="Last Updated 7/27/17 11:35 AM">Start-Right-Guide-AA-(TC-EMT)-Pending-EMS</a>
                                       
                                    </li> 
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Pre Health Sciences (AA)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Cardiovascular-Tech.pdf" target="_blank" title="Last Updated 7/27/17 11:36 AM">Start-Right-Courses-AA-Pending-Cardiovascular-Tech</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Dental-Hygiene.pdf" target="_blank" title="Last Updated 7/27/17 11:36 AM">Start-Right-Courses-AA-Pending-Dental-Hygiene</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Diagnotic-Sonography.pdf" target="_blank" title="Last Updated 7/27/17 11:36 AM">Start-Right-Courses-AA-Pending-Diagnotic-Sonography</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Health-Information-Tech.pdf" target="_blank" title="Last Updated 7/27/17 11:36 AM">Start-Right-Courses-AA-Pending-Health-Information-Tech</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Nursing.pdf" target="_blank" title="Last Updated 7/27/17 11:36 AM">Start-Right-Courses-AA-Pending-Nursing</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Pending-Radiography.pdf" target="_blank" title="Last Updated 7/27/17 11:37 AM">Start-Right-Courses-AA-Pending-Radiography</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Pre-Health-AA/Start-Right-Courses-AA-Prending-Respiratory.pdf" target="_blank" title="Last Updated 7/27/17 11:37 AM">Start-Right-Courses-AA-Prending-Respiratory</a>
                                       
                                    </li> 
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Articulated Pre-Major (AA)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Architecture-UCF-UF.pdf" target="_blank" title="Last Updated 7/27/17 11:26 AM">Start-Right-Courses-Articulated-AA-Architecture-UCF-UF</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Art-Ringling.pdf" target="_blank" title="Last Updated 7/27/17 11:26 AM">Start-Right-Courses-Articulated-AA-Art-Ringling</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Aviation-Mgmt-FIT.pdf" target="_blank" title="Last Updated 7/27/17 11:26 AM">Start-Right-Courses-Articulated-AA-Aviation-Mgmt-FIT</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Computer-Science-UCF.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-Articulated-AA-Computer-Science-UCF</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-ECET-Valencia.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-Articulated-AA-ECET-Valencia</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Engineering-UCF-plus.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-Articulated-AA-Engineering-UCF-plus</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Information-Technology-UCF.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-Articulated-AA-Information-Technology-UCF</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Articulated-AA-Logistics-Mgmt-FIT.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-Articulated-AA-Logistics-Mgmt-FIT</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Biomedical-Sciences.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-Pre-Major-Biomedical-Sciences</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Business-Administration.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-Pre-Major-Business-Administration</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Early-Child-Educ.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-Pre-Major-Early-Child-Educ</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Elem-Educ.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-Pre-Major-Elem-Educ</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Health-Sciences.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-Pre-Major-Health-Sciences</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Music-Performance.pdf" target="_blank" title="Last Updated 7/27/17 11:29 AM">Start-Right-Courses-Pre-Major-Music-Performance</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Psychology.pdf" target="_blank" title="Last Updated 7/27/17 11:29 AM">Start-Right-Courses-Pre-Major-Psychology</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-Pre-Major-Sign-Language.pdf" target="_blank" title="Last Updated 7/27/17 11:29 AM">Start-Right-Courses-Pre-Major-Sign-Language</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-PreMajor-AA-Dance-Perf.pdf" target="_blank" title="Last Updated 7/27/17 11:27 AM">Start-Right-Courses-PreMajor-AA-Dance-Perf</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Articulated-AA/Start-Right-Courses-PreMajor-AA-Theater-Drama.pdf" target="_blank" title="Last Updated 7/27/17 11:28 AM">Start-Right-Courses-PreMajor-AA-Theater-Drama</a>
                                       
                                    </li> 
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Associate in Arts - Transfer Plans (AA)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Accounting-TP.pdf" target="_blank" title="Last Updated 7/27/17 10:57 AM">Start-Right-Courses-Accounting-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Biology-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:07 AM">Start-Right-Courses-Biology-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Chemistry-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:07 AM">Start-Right-Courses-Chemistry-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Computer-Science-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:18 AM">Start-Right-Courses-Computer-Science-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Economics-TP.pdf" target="_blank" title="Last Updated 7/27/17 10:45 AM">Start-Right-Courses-Economics-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Education-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:20 AM">Start-Right-Courses-Education-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Engineering-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:22 AM">Start-Right-Courses-Engineering-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-English-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:22 AM">Start-Right-Courses-English-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-General-Studies-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:22 AM">Start-Right-Courses-General-Studies-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Health-Services-Administration-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:22 AM">Start-Right-Courses-Health-Services-Administration-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-History-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:23 AM">Start-Right-Courses-History-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Horticulture-Science-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:23 AM">Start-Right-Courses-Horticulture-Science-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Human-Resources-Management-TP.pdf" target="_blank" title="Last Updated 7/27/17 10:17 AM">Start-Right-Courses-Human-Resources-Management-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Information-Technology-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:23 AM">Start-Right-Courses-Information-Technology-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Journalism-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:23 AM">Start-Right-Courses-Journalism-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Management-Information-Systems-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:23 AM">Start-Right-Courses-Management-Information-Systems-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Marine-Biology-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:24 AM">Start-Right-Courses-Marine-Biology-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Mathematics-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:24 AM">Start-Right-Courses-Mathematics-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Nutrition-and-Dietetics-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:24 AM">Start-Right-Courses-Nutrition-and-Dietetics-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Philosophy-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:24 AM">Start-Right-Courses-Philosophy-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Political-Science-and-Government-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:24 AM">Start-Right-Courses-Political-Science-and-Government-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Public-Administration-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:25 AM">Start-Right-Courses-Public-Administration-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Public-Relations-Organizational-Communication-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:25 AM">Start-Right-Courses-Public-Relations-Organizational-Communication-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Social-Sciences-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:25 AM">Start-Right-Courses-Social-Sciences-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Sociology-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:25 AM">Start-Right-Courses-Sociology-TP</a>
                                       
                                    </li> 
                                    <li> <a href="documents/Transfer-Plans-AA/Start-Right-Courses-Statistics-TP.pdf" target="_blank" title="Last Updated 7/27/17 11:25 AM">Start-Right-Courses-Statistics-TP</a>
                                       
                                    </li> 
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Quality Enhancement Plan</h3>
                        
                        <div><a href="../../about/our-next-big-idea/documents/Valencia-College-Quality-Enhancement-Plan.pdf" target="_blank">Valencia College Quality Enhancement Plan</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/new-student-experience/meta-majors.pcf">©</a>
      </div>
   </body>
</html>