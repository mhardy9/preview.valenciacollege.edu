<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Experience | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/new-student-experience/course-information.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/new-student-experience/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>New Student Experience</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/new-student-experience/">New Student Experience</a></li>
               <li>New Student Experience</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2><img alt="NSE Wordle" height="305" src="NSEWordle.jpg" width="490"></h2>
                        
                        <p><strong>“My professor really is the main contributing factor to my success. There is an amazing
                              support system and really helped when things got tough.” – Student quote</strong></p>
                        
                        <p><strong>"I think this course is great and every student should take it. The assignments require
                              you to think, analyze, and decide what you really want to do. I would take it again!"
                              - Student Quote </strong></p>
                        
                        <h2>The NSE Course (SLS1122) </h2>
                        
                        <p>Among the most talked about and eagerly anticipated of the initiatives of the NSE
                           is the NSE Course. The new course is a required, credit-bearing  course that is aligned
                           to general education outcomes and will eventually be delivered in three formats: a
                           basic, meta-major, and an embedded outcome version of the NSE course. The course was
                           built from the ground up to solidify students’ purpose, pathways, personal connection,
                           sense of place, development of a plan and aspects of preparation for college (the
                           6 Ps). In addition, students in the NSE Course receive personalized advising from
                           their NSE instructor. 
                        </p>
                        
                        <p><a href="documents/NSECourse_ShowcaseSlides.pdf">NSE Course_The 6 Ps_Lesson Highlights</a></p>
                        
                        <p><a href="documents/Syllabus2015Full-PageBookletGeneric.pdf">Sample Syllabus_Current</a></p>
                        
                        <p>Facts About the NSE Course </p>
                        
                        <ul>
                           
                           <li>8,259 students have taken the NSE Course since Fall 2014 </li>
                           
                           <li>32% of the students in the NSE Course had a dedicated FT faculty advisor in Fall 2014</li>
                           
                           <li>65% of the students in the NSE Course had a dedicated FT faculty advisor in Spring
                              2015
                           </li>
                           
                        </ul>                  
                        
                        <p><strong>Students say the aspects of the NSE course that promoted learning and engagement were…
                              </strong></p>                  
                        
                        <ul>
                           
                           <li>the enthusiasm, support, and care of the professor</li>
                           
                           <li>learning to appreciate group work and the importance of team work </li>
                           
                           <li>learning the importance of establishing good communication skills</li>
                           
                           <li>learning how to manage their time, stay organized, and study</li>
                           
                           <li>focusing on finding and using their strengths</li>
                           
                        </ul>
                        
                        <p><strong>(Source: Student Feedback on Instruction Data)</strong></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Quality Enhancement Plan</h3>
                        
                        <div><a href="../../about/our-next-big-idea/documents/Valencia-College-Quality-Enhancement-Plan.pdf" target="_blank">Valencia College Quality Enhancement Plan</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/new-student-experience/course-information.pcf">©</a>
      </div>
   </body>
</html>