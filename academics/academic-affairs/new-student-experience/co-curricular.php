<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Experience | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/new-student-experience/co-curricular.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/new-student-experience/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>New Student Experience</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/new-student-experience/">New Student Experience</a></li>
               <li>New Student Experience</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>The Co-Curricular Experience</h2>
                        
                        <p><a href="documents/TheNSECoCurricular.pdf">The NSE CoCurricular</a></p>
                        
                        <p><a href="documents/GoBeSummaries.pdf">What are the GoBes? </a></p>
                        
                        <p><a href="../../lakenona/ontrack.html">Lake Nona: Get On Track: Invest in Yourself by Getting On Track</a></p>
                        
                        <p>What's Next for NSE Students: College Certificates </p>
                        
                        <p>Beginning Fall 2015, Valencia College will offer six College Certificates as part
                           of the evolving New Student Experience. These certificates are meant to be the answer
                           to the “W<em>hat’s next for our NSE students after they complete SLS1122 and the GoBes</em>?” question.
                        </p>
                        
                        <p>Students will be invited to choose from the following certificate tracks:</p>
                        
                        <ul>
                           
                           <li>Career Action</li>
                           
                           <li>Leadership</li>
                           
                           <li>Peace and Justice</li>
                           
                           <li>Global Learning</li>
                           
                           <li>Pre-Professional (with an emphasis in the Health field)</li>
                           
                           <li>Diversity</li>
                           
                        </ul>
                        
                        <p>Students will complete a specified number of workshops, activities, and events to
                           earn their certificate, will be awarded with a cord to wear at graduation, will be
                           able to print a co-curricular transcript to include in their professional portfolio,
                           will receive a recognition certificate at the Student Development Awards banquets,
                           and will be encouraged to make note of the earned certificate on their resume. 
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Quality Enhancement Plan</h3>
                        
                        <div><a href="../../about/our-next-big-idea/documents/Valencia-College-Quality-Enhancement-Plan.pdf" target="_blank">Valencia College Quality Enhancement Plan</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/new-student-experience/co-curricular.pcf">©</a>
      </div>
   </body>
</html>