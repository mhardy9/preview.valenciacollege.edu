<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Experience | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/new-student-experience/new-student-orientation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/new-student-experience/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>New Student Experience</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/new-student-experience/">New Student Experience</a></li>
               <li>New Student Experience</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h3>
                           <strong>New Student Orientation at Valencia</strong>
                           
                        </h3>
                        
                        <p><strong>New Student Orientation</strong><strong> </strong></p>
                        
                        <p>All new, degree-seeking students are required to participate in an orientation session
                           before registering for their first term. New Student Orientation for degree-seeking
                           students includes: 
                        </p>
                        
                        <ul>
                           
                           <li>Information on education planning</li>
                           
                           <li>Introduction to College resources including LifeMap tools</li>
                           
                           <li>Academic advising for first term</li>
                           
                           <li>Registration for first term </li>
                           
                        </ul>
                        
                        <p><strong>Transfer Student Orientation</strong></p>
                        
                        <p>All degree-seeking students transferring to Valencia from another institution are
                           required to participate in an orientation session before registering for their first
                           Valencia term. Transfer students complete an information orientation online before
                           signing up for an in-person advising session.&nbsp; Students will receive an unofficial
                           review of their transcripts and can develop an education plan to complete their degree
                           at Valencia.&nbsp; 
                        </p>
                        
                        <p><strong>Advancing the Redesign of New Student Orientation</strong></p>
                        
                        <p>Due to a changing state environment (including the implementation of new Developmental
                           Education processes in Fall 2014 and new General Education requirements in Fall 2015),
                           we began the redesign of NSO in Fall 2014.&nbsp; A part-time NSO Director is leading a
                           group of faculty and staff in redesigning the curriculum for the first-time-in-college
                           orientation and producing multimedia materials to support the redesigned curriculum.&nbsp;
                           We will pilot the new program in summer 2015. 
                        </p>
                        
                        <p>The curriculum will include: </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Student Learning Outcomes </strong>(SLOs) that align with other student learning outcomes of the New Student Experience,
                              including the New Student Experience Course and the Transition Orientation.
                           </li>
                           
                           <li>
                              <strong>A redesigned New Student Orientation </strong>which includes learning experiences that teach the SLOs and align with other learning
                              experiences in the New Student Experience.
                           </li>
                           
                           <li>
                              <strong>Creation of NSO Multi-Media Materials </strong>designed to facilitate successful student transition to college.
                           </li>
                           
                           <li>
                              <strong>Implementation of a Learning Assessment Plan </strong>that assesses student learning in New Student Orientation
                           </li>
                           
                        </ul>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Quality Enhancement Plan</h3>
                        
                        <div><a href="../../about/our-next-big-idea/documents/Valencia-College-Quality-Enhancement-Plan.pdf" target="_blank">Valencia College Quality Enhancement Plan</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/new-student-experience/new-student-orientation.pcf">©</a>
      </div>
   </body>
</html>