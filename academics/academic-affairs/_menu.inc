
<ul>
<li><a href="/academics/academic-affairs/index.php">Academic Affairs</a></li>
	<li class="submenu"><a class="show-submenu" href="#">Offices <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
<ul>
<li><a href="/academics/academic-affairs/career-workforce-education/index.php">Career &amp; Workforce Education</a></li>
<li><a href="/academics/academic-affairs/curriculum-assessment/index.php">Curriculum &amp; Assessment</a></li>
<li><a href="/faculty/development/index.php">Faculty &amp; Instructional Development</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.php">Institutional Research</a></li>
<li><a href="/academics/academic-affairs/resource-development/index.php">Resource Development</a></li>
</ul>
<li><a href="/academics/academic-affairs/organizational-chart.php">Organizational Chart</a></li>
</ul>

