<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness &amp; Planning  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Effectiveness &amp; Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li>Institutional Effectiveness Planning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <h2>About Us</h2>
                        
                        <p>The Office of Institutional Effectiveness &amp; Planning is organized to support the demands
                           of meaningful institutional effectiveness planning and implementation efforts within
                           Academic Affairs and throughout the college.  Institutional effectiveness requires
                           the  annual articulation of expected outcomes, the development and implementation
                           of  appropriate means for assessing results, the collection and analysis of data,
                           and on-going professional development in order to act on assessment  results.  The
                           department supports these  efforts through the work of <a href="http://preview.valenciacollege.edu/instassess/">Institutional Assessment</a> and <a href="http://preview.valenciacollege.edu/ir/">Institutional Research</a>.
                        </p>
                        
                        <p>The Assistant Vice President for Institutional  Effectiveness &amp; Planning co-chairs
                           the <a href="http://preview.valenciacollege.edu/strategicplan/">College Planning Council</a> - the college-wide governing body responsible for strategic  planning at Valencia
                           College.  Members of  the department provide college-wide leadership for Valencia's
                           learning-centered  work on a range of committees that include: The <a href="http://preview.valenciacollege.edu/instassess/dt/">College Data Team</a>, the <a href="../../employees/organizations-committees/institutional-review-board/index.html">Institutional Review Board</a>, and the <a href="../../about/our-next-big-idea/index.html">Quality  Enhancement Plan</a> leadership teams.  In order to support the College's learning assessment  goals;
                           the department hosts an annual <a href="http://preview.valenciacollege.edu/instassess/sam/">State Assessment  Meeting</a> for the 28 Colleges in the State College  System of Florida and is currently organizing
                           the <a href="http://wp.valenciacollege.edu/learningassessment/">Community College Conference on Learning Assessment</a> - a national conference scheduled for
                           February 22-24, 2015.
                        </p>
                        
                        <p>The Assistant Vice President also serves as Valencia's  accreditation liaison with
                           the Southern Association of Colleges and Schools,  Commission on Colleges (SACS-COC).
                           The  reaffirmation of Valencia's accreditation with SACS-COC is scheduled for 2014.
                           
                        </p>
                        
                        
                        <hr>
                        
                        
                        
                        <h3>Departments</h3>
                        
                        <ul class="list-style-1">
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.html" title="">Institutional Research</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.html" title="">Institutional Assessment</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/index.html" title="">Regional Accreditation with SACSCOC</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/index.html" title="">Planning at Valencia</a></li>
                           
                           <li><a href="/about/our-next-big-idea/index.html" title="">Quality Enhancement Plan</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/index.pcf">©</a>
      </div>
   </body>
</html>