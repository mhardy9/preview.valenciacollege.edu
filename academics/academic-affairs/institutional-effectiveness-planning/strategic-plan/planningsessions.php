<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/planningsessions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           <div>
                              
                              <h3>Big Meeting 2016</h3>
                              
                              <p>The Strategic Planning “Big Meeting” took place on Friday, February 5, 2016, at the
                                 Special Events Center on West Campus. 171 faculty and staff from Valencia College
                                 were present and had an opportunity to hear updates on <a href="http://thegrove.valenciacollege.edu/updates-on-our-strategic-planning-process/">Valencia’s next strategic plan</a>, provide feedback on the strategic initiatives’ goals and objectives, and learn more
                                 on how Valencia is <a href="http://thegrove.valenciacollege.edu/executive-council-meeting-summary-december-2-2015/">meeting the needs of our community</a>.
                              </p>
                              
                              
                              <p>As you may remember, we began our new plan based on themes that emerged during last
                                 spring’s <em>Year of Reflection </em>discussions. The strategic themes and initiatives that we identified included <a href="work-teams/assessment-for-learning.html">Evidence Based Practice</a>, <a href="work-teams/New-Student-Experience.cfmhttp-/valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/New-Student-Experience.html">New Student Experience</a>, <a href="work-teams/Direct-Connect-2.html">Direct Connect 2.0</a>, <a href="work-teams/online-learning.html">Online Learning</a>, and <a href="work-teams/Part-Time-Faculty-Engagement.html">Part-Time Faculty Engagement</a>. We also added an additional initiative based upon feedback from all of you, called
                                 <a href="TeachingforLearning.html">Teaching for Learning in the 21st Century</a>. All teams worked diligently under a tight timeline to produce a working theory and
                                 vision, along with goals and objectives for each initiative. These drafts can be found
                                 by visiting the work team webpages within the <a href="index.html">Strategic Planning</a> page.
                              </p>
                              
                              
                              <p><strong>Meeting Documents:</strong></p>
                              
                              <ul>
                                 
                                 <li><a href="documents/BigMeetingAgendaFINAL.docx">Big Meeting Agenda</a></li>
                                 
                                 <li>Impact on Community Session </li>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/Valencia-CommunityPerceptionPresentation-02022016.pptx">C3R - Community Perception Presentation</a></li>
                                    
                                    <li><a href="documents/BigMeeting2016-Cooney.pptx"> Michael B. Conney Strategic Advisors, LLC - Community Engagement Data </a></li>
                                    
                                 </ul>
                                 
                                 
                                 <p><strong>Meeting Results:</strong></p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/BigMeeting2016ImpactonStudentLearningActivityResults.pdf">Impact on Student Learning Goals &amp; Objectives Activity Results</a></li>
                                    
                                    <li><a href="documents/BigMeeting2016ImpactonCommunityActivityResults.pdf">Impact on Community Activity Results  </a></li>
                                    
                                 </ul>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/BigMeeting2016ParticipationResults.pptx">Participation Report</a></li>
                                    
                                    <li><a href="documents/BigMeeting2016SurveyFinalReport02-23-16.pdf">Survey Results</a></li>
                                    
                                 </ul>
                                 
                                 
                              </ul>
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Year of Reflection</h3>
                              
                              <p>The <em>Invitation to Reflection</em> provided &nbsp;an opportunity to engage faculty, staff, adminstrators and in some instances
                                 students in a dialogue about &nbsp;Valencia as a learning centered institutionand, the
                                 long-range vision for the College.&nbsp; The discussions prompted how we, as a College,
                                 will impact student learning and our community. The evens took place at each campus
                                 and the district office, beginning in February and ending April 2015, and they helped
                                 frame the strategic work over the next five years.&nbsp; Additionally, the college also
                                 hosted a Learning Symposium on March 20 and 21 where many of our adjunct faculty attended
                                 and learning about college initiatives and answered the question, “What impact would
                                 the 21st century college have on its community?
                              </p>
                              
                              <p>Additionally, the college hosted its first annual Learning Symposium on March 20 and
                                 21st.&nbsp; There were 103 participants and 19 facilitators.&nbsp; Of the participants, 58 were
                                 full-time faculty and staff, and 45 were part-time faculty and staff.&nbsp; The symposium
                                 focused on three main themes of work that the college in which the college is presently
                                 engage:&nbsp; Front Door Alignment, High Impact Practices, and Buidling Connections for
                                 Student Success.&nbsp; The symposium ended with a design challenge in which participants
                                 reflected on what they learned, how to integrate the information into their work,
                                 and how the information could inform our strategic plan.&nbsp; More specifically, participants
                                 were asked to answer, “What impact would the 21st century college have on its community?”
                              </p>
                              
                              <p>Below is a snapshot of common themes gathered from each session. The full report can
                                 be found <a href="documents/YearofReflectionReport04-23-15kb.pdf">here</a>. 
                              </p>
                              
                              <h3><strong><u>Questions for Invitation to Reflection: </u></strong></h3>
                              
                              <p><strong>Reflections on Learning Centered Journey</strong></p>
                              
                              <ol>
                                 
                                 <li>Where are we on our learning-centered journey?</li>
                                 
                                 <li>What are our strengths as a learning-centered institution? What do we do well? In
                                    what ways do we best promote learning and student success?
                                 </li>
                                 
                                 <li>Where are the gaps? In what ways might we improve?</li>
                                 
                              </ol>
                              
                              <p><strong>Reflections on Our Impact in the Community</strong></p>
                              
                              <ol>
                                 
                                 <li>What is and should be our place in the community?</li>
                                 
                                 <li>How have we effectively (or not effectively) impacted the community?</li>
                                 
                                 <li>How are we uniquely positioned to serve the community in Central Florida? Intersection
                                    of what we can do and the aspiration of the community.
                                 </li>
                                 
                                 <li>If the community continues to support us, we promise the community…</li>
                                 
                              </ol>
                              
                              
                              <p><strong><u>Common Themes:</u></strong></p>
                              
                              <p><strong>Reflections on Learning Centered Journey</strong></p>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <p>Strengths</p>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <p><strong>Gaps</strong></p>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <ul>
                                             
                                             <li>Faculty</li>
                                             
                                             <li>TLA</li>
                                             
                                             <li>Diversity</li>
                                             
                                             <li>Resources</li>
                                             
                                             <li>Brand Name/Aspen</li>
                                             
                                             <li>Faculty Development</li>
                                             
                                             <li>Learning Support</li>
                                             
                                             <li>Adaptability</li>
                                             
                                             <li>Collaboration</li>
                                             
                                             <li>Continuous Improvement</li>
                                             
                                             <li>Creativity</li>
                                             
                                             <li>Dedicated Employees</li>
                                             
                                             <li>Learning-Centered</li>
                                             
                                             <li>NSE</li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <ul>
                                             
                                             <li>Online</li>
                                             
                                             <li>Adjunct Development</li>
                                             
                                             <li>Advising</li>
                                             
                                             <li>College-Readiness/Motivation for Students</li>
                                             
                                             <li>Community Connection</li>
                                             
                                             <li>Communication</li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <p><strong>Reflections on Our Impact in the Community</strong></p>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <p><strong>Strengths</strong></p>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <p><strong>Gaps</strong></p>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <p><strong>Our Promise</strong></p>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <ul>
                                             
                                             <li>Direct Connect</li>
                                             
                                             <li>Learning Day/Service</li>
                                             
                                             <li>Advisory Boards</li>
                                             
                                             <li>Internships</li>
                                             
                                             <li>Partnerships</li>
                                             
                                             <li>Reputation</li>
                                             
                                             <li>Processes/Outreach (i.e. Got College)</li>
                                             
                                          </ul>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <ul>
                                             
                                             <li>Talent Pipeline/Alternative Training/Continuing Education</li>
                                             
                                             <li>Transitioning Veterans</li>
                                             
                                             <li>Service Learning Opportunities</li>
                                             
                                             <li>Internships</li>
                                             
                                             <li>Bringing Community into College/Community Forward</li>
                                             
                                             <li>Student/Faculty Community Connection</li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <ul>
                                             
                                             <li>Address and respond to community needs. Conduct needs analysis. </li>
                                             
                                             <li>Increase access to college. “College is Possible” message</li>
                                             
                                             <li>Raise standard of living, decrease/improve poverty levels</li>
                                             
                                             <li>Workforce needs (i.e. certificates, workshops, skillshops, training)</li>
                                             
                                             <li>Access to campuses for events</li>
                                             
                                             <li>Reach out to non-traditional students (i.e. older age group.)</li>
                                             
                                          </ul>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>Strategic Planning Kickoff Big Meeting </h3>
                              
                              <p>On Friday, September 25, 2015, the Strategic Planning Kick-Off event was held at the
                                 <a href="http://valenciacollege.us2.list-manage1.com/track/click?u=f9512075b988841ea877a0102&amp;id=d43b4ce8a7&amp;e=73277574b8">School of Public Safety</a>, where 132 employees attended.President Shugart welcomed us and urged us to keep
                                 our thinking strategic and not operational. Participants then divided into <a href="http://valenciacollege.us2.list-manage.com/track/click?u=f9512075b988841ea877a0102&amp;id=e3404f7baa&amp;e=73277574b8">five strategic planning work teams</a>: Assessment for Learning, DirectConnect 2.0, New Student Experience, Online Learning
                                 and Part-Time Faculty Engagement.<br>
                                 <br>
                                 Each of the five work teams had co-facilitators and assigned steering committee members
                                 who shared the history of the initiative and provided data relevant to the current
                                 state of the work.The teams then began to draft working theories, vision statements
                                 and goals for each strategic initiative.The day, though long, was exhilarating.<br>
                                 <br>
                                 <u><strong>Next Steps</strong></u><br>
                                 During October and November, the five teams will meet independently to write objectives
                                 and assessments for each goal, and a final draft report will be prepared by December.All
                                 progress on the goals will be updated on the <a href="http://valenciacollege.us2.list-manage.com/track/click?u=f9512075b988841ea877a0102&amp;id=7a5b578413&amp;e=73277574b8">Strategic Planning website</a>.<br>
                                 <br>
                                 Additionally, based on input and response to the call for participation, the Steering
                                 Committee determined that there was significant energy around the formation of a strategic
                                 teaching initiative, which is tentatively called Teaching for Learning in the 21st
                                 Century. The 21st century themes that were proposed included: education forsustainability,
                                 effective use of technology in the classroom, service learning, global learning, and
                                 peace, justice and inclusive excellence.<br>
                                 <br>
                                 For those of you interested in participating in this new area, please send an email
                                 to Noelia Maldonado at <a href="mailto:nmaldonado5@valenciacollege.edu">nmaldonado5@valenciacollege.edu</a> so that we can add you to the work team. The Steering Committee is meeting with the
                                 faculty and staff who made the initial suggestions on Thursday, October 22, 2015,
                                 from 9 - 10:30 a.m. at the District Office, Room 502.We will have more information
                                 about the logistics and the timeline posted on the <a href="http://valenciacollege.us2.list-manage1.com/track/click?u=f9512075b988841ea877a0102&amp;id=0f2ada1d6a&amp;e=73277574b8">Strategic Planning website</a>.
                              </p>
                              
                              <p><u><strong>Meeting Documents: </strong></u></p>
                              
                              <ul>
                                 
                                 <li><a href="documents/AGENDAStrategicPlanningKick-OffBigMeeting-September25.pdf">AGENDA Strategic Planning Kick-Off Big Meeting - September 25</a></li>
                                 
                                 <li><a href="documents/StrategicPlanningDefinitionsFINAL09-23-15.pptx">Strategic Planning Definitions</a></li>
                                 
                                 <li><a href="documents/ValenciaCollegeStrategicPlanningTimelineTable090115.pdf">Valencia College Strategic Planning Timeline Table</a></li>
                                 
                              </ul>
                              
                              
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Community Engagement Data Discussion </h3>
                              
                              <p><strong>Michael B. Cooney Strategic Advisors, LLC</strong> was asked by Dr. Sandy Shugart to gather a wide variety of information and data that
                                 would be used to prepare for Valencia College's 2015 Strategic Planning Meeting. It
                                 includes information and data about the local Educational Ecosystem, Workforce Issues,
                                 Economic Drivers, and compares the Orlando Metro Area to other metro areas throughout
                                 the U.S. From this information and data, a group of key Valencia leaders will decide
                                 what is the most relevant to present during the Strategic Planning Meeting.
                              </p>
                              
                              <p>The information and data is presented in a fairly raw format, but in most cases includes
                                 basic context so the reader understands why it was included. The intent is to create
                                 a dialog that will help determine what role Valencia should play (beyond education)
                                 in the communities the institution serves. 
                              </p>
                              
                              <p>Every effort has been made to format the information and data consistently; however,
                                 please note the document is still a draft version. The information and data deemed
                                 relevant for the Strategic Planning Meeting will be formatted in a slide presentation.
                                 
                              </p>
                              
                              <p>Michael B. Cooney Strategic Advisors, LLC (MBCSA) is a project management and consulting
                                 company supporting the needs of academic institutions, organizations, and companies.
                                 MBCSA's clients include UCF Regional Campuses (UCFRC), Valencia College, Seminole
                                 State College, Lake-Sumter State College, Eastern Florida State College, Orlando Economic
                                 Development Commission, and CPI Technologies. While working with UCFRC, MBCSA helped
                                 create UCF's Bachelor of Applied Science and DirectConnect to UCF program, organized
                                 the Curriculum Alignment Initiative, and managed numerous Working Groups, which included
                                 state college partners.
                              </p>
                              
                              <ul>
                                 
                                 <li><a href="documents/DRAFTVCPresentation.pptx">Executive Council Meeting Presentation / October 22, 2015</a></li>
                                 
                                 <li><a href="documents/DRAFTCOMMUNITYENGAGEMENT2.0Update090815.pdf">Community Engagement 2.0 </a></li>
                                 
                                 <li>
                                    
                                    <p><a href="documents/DRAFT081715ValenciaCollegeStrategicPlanningStakeholderAwarenessProjectv3.pdf">Stakeholder Awareness: Data-driven Analysis of the Communities Valencia College Serves</a></p>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/planningsessions.pcf">©</a>
      </div>
   </body>
</html>