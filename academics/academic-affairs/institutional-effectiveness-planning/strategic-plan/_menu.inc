<ul>
<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/index.php">Strategic Planning at Valencia</a>	
<ul>	 <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/goals.php">Vision, Mission, Values</a></li>
            <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/ValenciaCommitment.php">Valencia's Commitment</a></li>
	<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/MeetTheTeam.php">Meet the Team</a></li>  

	 <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/PlanningSessions.php">Planning Sessions</a></li>  
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/LearningSymposium.php">Learning Symposium</a></li>
	</ul>

<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Educational-Commitment.php">Educational Commitment</a>
<ul>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/Direct-Connect-2.php">DirectConnect 2.0</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/EducationEcosystem.php">Education Ecosystem</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/assessment-for-learning.php">Evidence Based Practice</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/New-Student-Experience.php"> New Student Experience</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/online-learning.php">Online Learning</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/opportunity-equity.php">Opportunity &amp; Equity</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/Part-Time-Faculty-Engagement.php">Part-time Faculty Engagement</a></li>
 </ul>
    <li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/default.php">Workforce &amp; Economy Commitment</a>
<ul>
       <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Career-Express.php">Center for Accelerated Training</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Knowledge-and-Innovation-Economy.php">Knowledge and Innovation Economy</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Tech-Express.php">Tech Express to Valencia</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Work-Based-Learning.php">Work-Based Learning</a></li>
 </ul>
    
    <li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Sustainable-Community-Commitment/default.php">Community Commitment</a>
<ul>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Knowledge-and-Innovation-Economy.php">Knowledge and Innovation Economy</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Tech-Express.php">Tech Express to Valencia</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Workforce-Economic-Commitment/Work-Based-Learning.php">Work-Based Learning</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Sustainable-Community-Commitment/default.php">Community Commitment</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Sustainable-Community-Commitment/Arts-Health-and-Civic-Engagement.php">Quality of Life: Arts, Health, and Civic Engagement</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Sustainable-Community-Commitment/Safety.php">Safety</a></li>
 </ul>   
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Archives.php">Archives</a></li>

</ul>
