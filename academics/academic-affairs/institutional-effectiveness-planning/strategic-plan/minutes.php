<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/minutes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Meeting Agendas and Minutes</h2>
                        
                        <ul>
                           
                           <li><a href="#council">College Planning Council</a></li>
                           
                           <li><a href="#committee">College Planning Committee</a></li>
                           
                           <li><a href="#situational">Data and Situational / Needs Analysis Task Force</a></li>
                           
                           <li><a href="#mission">Mission, Vision, Values Task Force</a></li>
                           
                           <li><a href="#communication">Communications Task Force</a></li>
                           
                           <li><a href="#evaluation">Evaluation Task Force</a></li>
                           
                        </ul>
                        <a></a>
                        
                        <h3>College Planning Council</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Agenda7-27-06.pdf" target="_parent">July                                    27, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCAgenda92806.pdf" target="_parent">September                                    28, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Agenda10-26-06.pdf" target="_parent">October                                    26, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Agenda-12507_1.doc" target="_parent">January             25, 2007</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/022207MtgAgenda.pdf" target="_parent">February 22, 2007 </a> 
                              </li>
                              
                              <li>
                                 <a href="documents/051007Agenda.pdf"> May 10, 2007</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/072607Agenda.pdf"> July 26, 2007</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCAgendaSeptember272007revised.pdf"> September 27, 2007 </a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCAgendaOctober252007.pdf"> October 25, 2007 </a> 
                              </li>
                              
                              <li><a href="documents/CPCAgendaJanuary2420081.doc"> January 24, 2008 </a></li>
                              
                              <li>
                                 <a href="documents/CPCAgendaMarch272008.pdf">March 27 2008</a> 
                              </li>
                              
                              <li><a href="documents/CPCAgendaApril242008.pdf">April 24, 2008 </a></li>
                              
                              <li>
                                 <a href="documents/CPCAgendaNovember202008.pdf">November 20, 2008</a> 
                              </li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/documents/CPCAgendaJanuary222009.pdf">January 22, 2009 </a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/%5CBIG%5Cdocuments%5CCPCAgendaFebruary262009.doc">February 26, 2009 </a></li>
                              
                              <li>
                                 <a href="documents/CPCAgendaJuly232009DRAFT_000.doc">July 23, 2009</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPC_Agenda_Sept_24_2009_DRAFT1.doc">September 24, 2009</a> 
                              </li>
                              
                              <li><a href="documents/CPCAgendaNov192009DRAFT.doc">November 19, 2009 </a></li>
                              
                              <li>January 2010 - Cancelled </li>
                              
                              <li>
                                 <a href="documents/CPCAgendaFebruary252010.doc">February 25, 2010</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCAgendaApril222010.docx">April 22, 2010</a> 
                              </li>
                              
                              <li>July 22, 2010 - Cancelled</li>
                              
                              <li>
                                 <a href="documents/CPCAgendaSept232010.doc">September 23, 2010</a>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/FutureTrendsinEducation.pptx">Future Trends in Education</a></li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>November 2010 - Cancelled</li>
                              
                              <li><a href="documents/CPCAgendaJan2720111.doc">January 27, 2011 </a></li>
                              
                              <li>
                                 <a href="documents/CPCAgendaApril282011.doc">April 28, 2011</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCAgendaJuly282011.docx">July 28, 2011</a> 
                              </li>
                              
                              <li><a href="documents/CPCAgendaSep2220111FINAL.docx">September 22, 2011 </a></li>
                              
                           </ul>
                           
                           <h3>Minutes</h3>
                           
                           <ul>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Minutes5-25-06.pdf">May 25, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Agenda7-27-06.pdf"> July 27, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Minutes9-28-06-1.pdf">September       28, 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Minutes-102606.doc">October 26       , 2006</a> 
                              </li>
                              
                              <li>
                                 <a href="meetingdocs/CPC-Agenda-12507_1.doc">January 25, 2007</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/022207CPCMeetingMinutes.pdf">February 22, 2007 </a> 
                              </li>
                              
                              <li>
                                 <a href="documents/051007MeetingMinutes.pdf">May 10, 2007</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/JulyMinutes0707.pdf">July 26, 2007 </a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCOctober252007AttachAMinutesofSept272007.doc">September 27, 2007 </a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCJan242007AttachmentAMinutesofOct252007.doc">October 25, 2007 </a> 
                              </li>
                              
                              <li><a href="documents/MinutesofMarch272008.pdf">January 24, 2008</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/documents/CPCMinutesNovember202008.pdf">November 20, 2008</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/documents/CPCJanuary22MeetingMinutes.pdf">January 22, 2009</a></li>
                              
                              <li><a href="documents/CPCAgendaFebruary262009.doc">February 26, 2009</a></li>
                              
                              <li><a href="documents/CPC_Sept_2009_Attach_B_Minutes_July_23_2009.docx">July 23, 2009 </a></li>
                              
                              <li>
                                 <a href="documents/CPCNov2009AttachAMinutesSept242009.docx">September 24, 2009</a> 
                              </li>
                              
                              <li><a href="documents/CPCFeb2010AttachAMinutesNov192009Draft.docx">November 19, 2009 </a></li>
                              
                              <li><a href="documents/CPCMinutesFeb2520101.docx">February 25, 2010</a></li>
                              
                              <li><a href="documents/CPCMinutesApril222010.docx"> April 22, 2010</a></li>
                              
                              <li>July 22, 2010 - Cancelled</li>
                              
                              <li><a href="documents/CPCJanuary2011AttachBminutes.docx">September 23, 2010</a></li>
                              
                              <li>November 2010 - Cancelled </li>
                              
                              <li>
                                 <a href="documents/CPCJanuary2011AttachBminutes.docx">January 27, 2011</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCApril2011Minutes-Approved.docx">April 28, 2011</a> 
                              </li>
                              
                              <li>
                                 <a href="documents/CPCJuly282011MeetingMinutesDRAFTAug82011.docx">July 28, 2011</a> 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        <h3>Budget &amp; Financial Advisory Group</h3>
                        
                        <h3> Meeting Agendas </h3>
                        
                        <ul>
                           
                           <li><a href="documents/BFAGMeetingAgendaSept282010.docx">September 28, 2010 </a></li>
                           
                           <li>October 25, 2010 </li>
                           
                        </ul>                  
                        
                        <h3>Minutes</h3>
                        
                        <ul>
                           
                           <li><a href="documents/BFAGMeetingMinutesSept282010.docx">September 28, 2010 </a></li>
                           
                           <li><span><a href="documents/BFAGMeetingMinutesOct252010.docx">October 25, 2010 </a></span></li>
                           
                        </ul>
                        
                        
                        
                        <p><a href="#top">TOP</a>
                           
                        </p>
                        
                        
                        <h3>College Planning Committee</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li><a href="documents/092906Agenda.pdf"> September 29, 2006 </a></li>
                              
                              <li><a href="documents/CPCagendno2112906.pdf"> November 29, 2006 </a></li>
                              
                              <li><a href="documents/032607MtgAgenda.pdf"> March 26, 2007</a></li>
                              
                              <li><a href="documents/042707Agenda.pdf">April 27, 2007 </a></li>
                              
                              <li><a href="documents/CPCommitteeAgendaApril72008revised.pdf">April 7, 2008 </a></li>
                              
                           </ul>
                           
                           <h3>Meeting Minutes</h3>
                           
                           <ul>
                              
                              <li><a href="documents/092906MeetingMinutes.pdf">September 29, 2006 </a></li>
                              
                              <li><a href="documents/112906MtgMinutes.pdf"> November 29, 2006</a></li>
                              
                              <li><a href="documents/032607Minutes.pdf"> March 26, 2007 </a></li>
                              
                              <li><a href="documents/042707MtgMinutes.pdf"> April 27, 2007 </a></li>
                              
                              <li><a href="documents/040708CommitteeMtgMinutes.pdf"> April 7, 2008</a></li>
                              
                              <li><a href="documents/CPCommitteMinutesSeptember42007.pdf"> September 4, 2007</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        <a></a>
                        
                        <h3>Data and Situational / Needs Analysis Task Force</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda9-15-06.pdf">September     15, 2006</a></li>
                              
                              <li><a href="documents/DataAnalysisTaskForceAgendaOct62006.pdf">October 6, 2006 </a></li>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda10-20-2006-1.pdf">October     20, 2006</a></li>
                              
                              <li><a href="documents/113006Minutes.pdf">November 30, 2006 </a></li>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda-01-16-2007%5B1%5D.doc">January     16, 2007</a></li>
                              
                           </ul>
                           
                           <h3>Minutes</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceMinutes9-15-06.pdf">September     15, 2006</a></li>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceMinutes10-6-2006.pdf">October     6, 2006</a></li>
                              
                              <li><a href="documents/DataAnalysisTaskForceMinutes102006.pdf">October 20, 2006</a></li>
                              
                              <li><a href="documents/113006MinutesDataTaskForce.pdf">November 30, 2006 </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        <a></a>
                        
                        <h3>Mission, Vision, Values Task Force</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-VisionValuesMissionTaskForceAgenda9-27-06.pdf">September 27, 2006&nbsp;</a></li>
                              
                              <li><a href="meetingdocs/CPC-VisionValuesMissionAgenda10-10-06.pdf">October 10, 2006 </a></li>
                              
                              <li><a href="documents/VVMagenda11-14-06_1.pdf">November 14, 2006 </a></li>
                              
                              <li><a href="documents/VVMagenda12-4-06_1.pdf">December 4, 2006</a></li>
                              
                              <li>
                                 <a href="meetingdocs/1-VVM-07Agenda.doc">January 16, 2007</a> 
                              </li>
                              
                              <li><a href="documents/VVMAgenda021307.pdf">February 13, 2007</a></li>
                              
                              <li><a href="documents/041607Agenda.pdf"> April 16, 2007 </a></li>
                              
                              <li><a href="documents/060607Agenda.pdf"> June 6, 2007 </a></li>
                              
                           </ul>
                           
                           <h3>Minutes</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-VisionValuesMissionTaskForceMinutes9-06-06.pdf">September 27, 2006</a></li>
                              
                              <li><a href="documents/VVMminutes10-10-06.pdf"> October 10, 2006 </a></li>
                              
                              <li><a href="meetingdocs/VVMminutes11-14-06.doc">November 14, 2006 </a></li>
                              
                              <li><a href="meetingdocs/VVMminutes12-4-06.doc">December 4, 2006</a></li>
                              
                              <li><a href="documents/VVMminutes021307.pdf">February 13, 2007</a></li>
                              
                              <li><a href="documents/060607MtgMinutes.pdf">June 6, 2007 </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        <a></a>
                        
                        <h3>Communications Task Force</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-CommunicationTaskForceAgenda10-10-2006.pdf">October                 10, 2006</a></li>
                              
                              <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda-11-30-2006%5B1%5D.doc"> November 20, 2006 </a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/docs/documents/CommunicationMeetingAgendaJan12_1.pdf">January 12, 2007</a></li>
                              
                              <li><a href="documents/Communicationagenda021607.pdf">February 16, 2007</a></li>
                              
                              <li><a href="documents/Communicationagenda040607.pdf">April 06, 2007</a></li>
                              
                              <li><a href="documents/040708CommunicationsTFAgenda.pdf"> April 22, 2008 </a></li>
                              
                              <li>
                                 <a href="documents/040708CommunicationsTFAgenda.pdf"> August 1, 2008 - no written agenda</a>
                                 
                              </li>
                              
                              <li>
                                 <a href="documents/091008MeetingAgenda.pdf">September 10, 2008 </a>
                                 
                              </li>
                              
                           </ul>
                           
                           <h3>Meeting Minutes</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-CommunicationTaskForceMinutes10-10-2006-1.pdf">October                 10, 2006 </a></li>
                              
                              <li><a href="meetingdocs/CPCCommunicationTaskForceMinutes11-20-06.doc">November             20, 2006</a></li>
                              
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/strategic-plan/docs/documents/CommunicationsTaskForceMinutesJan122007.pdf">Janaury             12, 2007</a></li>
                              
                              <li><a href="documents/Communicationminutes021607.pdf">February 16, 2007 </a></li>
                              
                              <li><a href="documents/Communicationminutes040607_000.pdf">April 06, 2007</a></li>
                              
                              <li>
                                 <a href="documents/042208MeetingMinutes.pdf">April 22, 2008 </a>
                                 
                              </li>
                              
                              <li>
                                 <a href="documents/080108MeetingMinutes.pdf">August 1, 2008 </a>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        <a></a>
                        
                        <h3>Evaluations Task Force</h3>
                        
                        <div>
                           
                           <h3>Meeting Agendas</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-EvaluationTaskForceAgenda10-23-06-1.pdf">October 23, 2006 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceAgenda1113061.pdf">November 13, 2006 </a></li>
                              
                              <li><a href="documents/021907Agenda.pdf">February 19, 2007 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceDesignTeamAgenda31907.pdf">March 19, 2007 </a></li>
                              
                              <li><a href="documents/041607Agenda.pdf"> April 16, 2007 </a></li>
                              
                              <li><a href="documents/EvaluationTaskForceAgenda61807.pdf"> June 18, 2007 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceAgendaNov192007.pdf">November 19, 2007 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceAgendaMarch520081.pdf">March 5, 2008 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceAgendaMarch2620081.pdf"> March 26, 2008 </a></li>
                              
                           </ul>
                           
                           <h3>Minutes</h3>
                           
                           <ul>
                              
                              <li><a href="meetingdocs/CPC-EvaluationTaskForceMinutes-102306%5B1%5D.doc">October 23, 2006 </a></li>
                              
                              <li><a href="meetingdocs/CPC-EvaluationTaskForceMinutes111306%5B1%5D.doc">November 13, 2006 </a></li>
                              
                              <li><a href="documents/021907Minutes.pdf"> February 19, 2007 </a></li>
                              
                              <li><a href="documents/031907Minutes.pdf">March 19, 2007 </a></li>
                              
                              <li><a href="documents/EvaluationTaskForceMinutes041607.pdf">April 16, 2007 </a></li>
                              
                              <li><a href="documents/MinutesDesignTeamMeetingJuly16.pdf">July 16, 2007 </a></li>
                              
                              <li><a href="documents/CPCEvaluationTaskForceMinutesNov1920071.pdf">November 19, 2007 </a></li>
                              
                              <li><a href="documents/030508MeetingMinutes.pdf">March 5, 2008 </a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/minutes.pcf">©</a>
      </div>
   </body>
</html>