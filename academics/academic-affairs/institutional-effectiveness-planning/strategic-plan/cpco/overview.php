<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/cpco/overview.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/cpco/">Cpco</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <p><strong> This webpage contains archive information <u>only</u>. Please check back with us in 2015 as we prepare the Strategic Plan for 2016-21.
                              </strong></p>
                        
                        <p>The <strong>College Planning Council (CPC)</strong> is one of Valencia’s four governing councils. Among the Council’s responsibilities
                           are designing and conducting 1) a collaborative strategic planning process, and 2)
                           a collaborative budgeting process that links the annual budget to the strategic plan.&nbsp;
                           The Strategic Plan for 2008-13 was approved by the District Board of Trustees on June
                           17, 2008. On September 9, 2011, the Board approved extending the <a href="documents/StrategicPlan2008-15RevisedSep92011.pdf">Strategic Plan</a> until 2015. You can access the summary Strategic Plan brochure <a href="../documents/StrategicPlanBrochure.pdf">here</a>. 
                        </p>
                        
                        <p>The Council works closely with and consults the Senior Leadership Team as it designs
                           and conducts these processes. The CPC created Task Forces to carry out the phases
                           of the planning process, and the charges, agendas, and minutes of those task forces
                           may be accessed through the strategic planning web page. Volunteers were recruited
                           collegewide for the various task forces. The task forces include: Vision, Values,
                           and Mission; Data and Situational/Needs Analysis; Communications; Evaluation; and
                           Strategies, Goals, and Objectives.
                        </p>
                        
                        <p> CPC is committed to a collaborative process with many opportunities for involvement
                           of those within the college and in the community that we serve. Throughout the planning
                           cycle, we will schedule big and small group meetings, community meetings, consultations
                           with key constituencies, and discussions by the governing councils and the senior
                           leadership team, all of which combine to enable broad based participation in the various
                           phases of creating, updating, and evaluating the Strategic Plan.
                        </p>
                        
                        <p>During the 2012-13 period, the Council is scheduled to meet on September 27, October
                           25, November 29, January 24, February 28, March 28, and April 25. 
                        </p>
                        
                        <p>To request additional information or for questions about the College Planning Council,
                           please contact its co-chairs: <a href="mailto:kewen@valenciacollege.edu">Kurt Ewen</a>, <a href="mailto:jfuhrman@valenciacollege.edu">Jean Marie Führman</a>, or <a href="mailto:sledlow@valenciacollege.edu">Susan Ledlow</a>. 
                        </p>
                        
                        <h3>2012-13 Council Membership</h3>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div> 
                                    <p>Member</p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p>Title</p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p>Represented</p>
                                    
                                    <p>Group</p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p>Contact</p>
                                    
                                    <p>Information</p>
                                    
                                 </div>
                                 
                                 <div> 
                                    <p>End of</p>
                                    
                                    <p>Term</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Susan Ledlow</strong></p>
                                    
                                    <p><strong>(Chair)</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><strong>VP Academic Affairs &amp; Planning</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>VP Academic Affairs &amp; Planning</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Downtown Center, DTC-4, x3423</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>N/A</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Kurt Ewen</strong></p>
                                    
                                    <p><strong>(Co-Chair)</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>AVP Assessment and Institutional Effectiveness</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Institutional Research</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-12, x1611 </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Jean Marie Führman</strong></p>
                                    
                                    <p><strong>(Co-Chair)</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Reading</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Faculty</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Winter Park Campus, 5-3, x6865</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Vivian Calderon</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Administrative Assistant</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Career Staff</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Osceola Campus, 6-3, x4862</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Donna Deitrick</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Staff Assistant I</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Career Staff</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-1, x5473</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Regina Falconer</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Biology</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty</strong></p>
                                    
                                    <p><strong>East Campus</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>East Campus, 3-23, x2025</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Damion Hammock</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Mathematics</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Faculty</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Winter Park Campus, 5-3, x6917</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Deidre Holmes DuBois</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Speech</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty Council,</strong></p>
                                    
                                    <p><strong>Vice President</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>Osceola Campus, 6-3, x4891 </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Keith Houck</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>VP Operations &amp; Finance</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Chief Financial Officer</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Downtown Center, DTC-3, x3300</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Dale Husbands</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Dean, Business Information Technology</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Academic Deans</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Osceola Campus, 6-8, x4841</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Stacey Johnson</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Campus President</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>East &amp; Winter Park Campus President</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>East, 3-1, x2822/Winter Park, 5-1, x6805</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Amy Kleeman</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>AVP College Transition</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Student Affairs Representative</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-25, x1238</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Kevin Matis</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Network Server Specialist</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professional Staff</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-12, x1775</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Rob McCaffrey</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Graphics Technology</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty Council,</strong></p>
                                    
                                    <p><strong>President</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>East Campus, 3-2, x2784</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>John McFarland</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, ESL</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty</strong></p>
                                    
                                    <p><strong>Osceola Campus</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>Osceola Campus, 6-2, x4125</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Kathleen Plinske</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Campus President</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Osceola &amp; Lake Nona</strong></p>
                                    
                                    <p><strong>Campus President</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>Osceola Campus, 6-2, x4100</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Jerry Reed</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Programmer Analyst</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professional Staff </strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-12, x5583</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2013</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Joyce Romano</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>VP Student Affairs</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>VP Student Affairs</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Downtown Center, DTC-4, x3402</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Deborah Simko</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Professor, Nursing</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty</strong></p>
                                    
                                    <p><strong>West Campus</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-14, x1739</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>David Sutton</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Dean, Humanities &amp; Foreign Language</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Academic Deans</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>East Campus, 3-2, x2753</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>04/2014</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Bill White</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Chief Information Officer</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Chief Information Officer</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-12, x1185</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Falecia Williams</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>Campus President</strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>West Campus</strong></p>
                                    
                                    <p><strong>Campus President</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p><strong>West Campus, 4-1, x1235</strong></p>
                                 </div>
                                 
                                 <div>
                                    <p><strong>N/A</strong></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <p><a href="overview.html#top">TOP</a></p>
                        
                        <h3>College Planning Council Staff</h3>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Member</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Title</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Represented</p>
                                    
                                    <p>Group</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Contact</p>
                                    
                                    <p>Information</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>End of</p>
                                    
                                    <p>Term</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Alys Arceneaux</p>
                                 </div>
                                 
                                 <div>
                                    <p>Research Analyst</p>
                                 </div>
                                 
                                 <div>
                                    <p>Institutional Research</p>
                                 </div>
                                 
                                 <div>
                                    <p>West Campus, 4-12, x1611</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A</p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Sherri Dixon</p>
                                 </div>
                                 
                                 <div>
                                    <p>AVP Budget &amp; Auxiliary Services</p>
                                 </div>
                                 
                                 <div>
                                    <p>Financial Services</p>
                                 </div>
                                 
                                 <div>
                                    <p>Downtown Center, DTC-3, x3306</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Sonya Joseph</p>
                                 </div>
                                 
                                 <div>
                                    <p>AVP Student Affairs</p>
                                 </div>
                                 
                                 <div>
                                    <p>Student Affairs </p>
                                 </div>
                                 
                                 <div>
                                    <p>Osceola Campus, 6-8, x4997</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Jackie Lasch</p>
                                 </div>
                                 
                                 <div>
                                    <p>AVP Financial Services</p>
                                 </div>
                                 
                                 <div>
                                    <p>Financial Services</p>
                                 </div>
                                 
                                 <div>
                                    <p>Downtown Center, DTC-3, x3302</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Helene Loiselle</p>
                                 </div>
                                 
                                 <div>
                                    <p>AVP Facilities &amp; Sustainability</p>
                                 </div>
                                 
                                 <div>
                                    <p>Facilities</p>
                                 </div>
                                 
                                 <div>
                                    <p>West Campus, 4-21, x1707</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Noelia Maldonado</p>
                                 </div>
                                 
                                 <div>
                                    <p>Executive Assistant</p>
                                 </div>
                                 
                                 <div>
                                    <p>Strategic Planning</p>
                                 </div>
                                 
                                 <div>
                                    <p>Downtown Center,  DTC-4, x3487</p>
                                 </div>
                                 
                                 <div>
                                    <p>N/A</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <p><a href="overview.html#top">TOP</a></p>
                        
                        
                        <h3><strong>College Planning Council Charge</strong></h3>
                        
                        <ul>
                           
                           <li> Coordinate the development and revision of the Strategic Learning Plan</li>
                           
                           <li>Coordinate the development of the annual budget</li>
                           
                           <li>Recommend multi-year budget and staffing plans</li>
                           
                           <li>Oversee the measurement of institutional effectiveness</li>
                           
                           <li>Function as Steering Committee for institutional accreditation by the Southern Association
                              of Colleges and Schools (SACS)
                           </li>
                           
                           <li>Oversee the College's Diversity Plan</li>
                           
                        </ul>            
                        
                        
                        <h3><strong>Design Principles for the Strategic Planning Process</strong></h3>
                        
                        <p><br>
                           The College Planning Council established the following principles to guide the strategic
                           planning work:<br>
                           (These are numbered for ease of reference, but are not listed in any priority order.)
                        </p>
                        
                        <ol>
                           
                           <li>The planning process and the plan that it yields will be learning-centered, will be
                              grounded in the College’s history of excellence, innovation, and community, and will
                              support the quality and aspiration that bequeath the College with its distinctive
                              place in higher education.<br>
                              
                           </li>
                           
                           <li>The process will be strategic by impacting the results the college aims to provide
                              to society and to students as they progress in their programs of learning.<br>
                              
                           </li>
                           
                           <li>The planning process will be collaborative by operating within our shared governance
                              structure that ensures broad-based participation and by providing a means for stakeholder
                              groups to be heard and to influence the plan. <br>
                              
                           </li>
                           
                           <li>The process will build trust through effective communication and negotiation, by making
                              it safe to identify and challenge assumptions, and by supporting agreements on shared
                              values and the making of mutual commitments that are the basis for the strategic plan,
                              and that are honored as the plan is implemented.<br>
                              
                           </li>
                           
                           <li>The process will be meaningful in that it will help the College to establish a vision
                              of the future that shapes, defines, and gives meaning to its strategic purpose, and
                              in that it will help to shape strategic decisions, some of which are identified in
                              advance.<br>
                              
                           </li>
                           
                           <li>The process will be data-driven, using qualitative and quantitative data, routinely
                              reviewed as the plan is implemented, with the aim of continuous improvement.<br>
                              
                           </li>
                           
                           <li>The plan will include formative and summative evaluation components that evaluate
                              the planning process itself, as well as the implementation of the plan, using agreed
                              upon performance indicators.<br>
                              
                           </li>
                           
                           <li>The process will have a clear cycle of activities, with a beginning and an end, and
                              timed and structured to coordinate well with SACS accreditation requirements.<br>
                              
                           </li>
                           
                           <li>The process will be as simple as possible while yielding a viable plan, avoiding the
                              trap of imposing more order than the College can tolerate, and integrating planning
                              into permanent governing structures and collegewide meetings, rather than creating
                              a separate set of activities removed from the governance and life of the College.<br>
                              
                           </li>
                           
                           <li>The process will support the integration of fiscal, learning, and facilities plans
                              with the strategic plan of the college, through careful timing and by clearly connecting
                              each of these plans to the College’s revised Vision, Mission, and Values.<br>
                              
                           </li>
                           
                           <li>The strategic plan will be useful to and therefore used by councils, campuses and
                              departments as they prepare their plans, and will encourage a future orientation to
                              their work.<br>
                              
                           </li>
                           
                           <li>The process, its language, its products, and the results of the plan will be communicated
                              to all employees internally.<br>
                              
                           </li>
                           
                           <li>The plan will be expressed clearly, with language that is understood by stakeholders
                              and with clear means of measuring progress.<br>
                              
                           </li>
                           
                           <li>The process will be truly comprehensive, and will have clearly assigned roles for
                              individuals and groups, including students.
                           </li>
                           
                        </ol>
                        
                        <p><a href="overview.html#top">TOP</a></p>
                        
                        <h3><strong>Work Products of the Planning Process</strong></h3>
                        
                        <p>The College Planning Council has named the following as the products of the planning
                           process:
                        </p>
                        
                        <ul>
                           
                           <li>needs assessment/situational analysis/environmental scan, providing a common understanding
                              of the present and the anticipated future, including information about our competitors
                              and clearly defined gaps in results at the mega, macro, and micro levels, as defined
                              by Roger Kaufman’s planning model
                           </li>
                           
                           <li>reviewed/revised mission (the role we will play), vision, and values statements</li>
                           
                           <li>a set of college strategies (the ways in which we will play our role and get results),
                              goals (what results we want to accomplish within our role), measurable outcomes objectives
                              (how much will we change specific results at the mega, macro, and micro levels as
                              used in Kaufman’s model), and related activities to achieve the objectives within
                              an agreed upon timeframe
                           </li>
                           
                           <li>an evaluation plan, including indicators/measures of institutional effectiveness,
                              revised as needed, and a means of assessing the extent to which College decisions
                              are consistent with the plan
                           </li>
                           
                           <li>a recommended assignment of responsibilities for objectives to the governing councils</li>
                           
                           <li>list of major decisions to be impacted by the plan, which could include decisions
                              such as:<br>
                              
                              <ul>
                                 
                                 <li>academic program plans for new campus(es) and evolution of programs on existing campuses</li>
                                 
                                 <li>the goals in the College enrollment plan</li>
                                 
                                 <li>Future Valencia Foundation fund raising goals</li>
                                 
                                 <li>Focus of the Quality Enhancement Plan for SACS in 2010-2012</li>
                                 
                                 <li>Professional development multi-year plan</li>
                                 
                                 <li>Multi-year financial plan and annual budgets</li>
                                 
                                 <li>Major external funding requests with collegewide impact, such as Title III and Title
                                    V
                                 </li>
                                 
                                 <li>Community relations priorities and programs</li>
                                 
                                 <li>Efforts designed to support student learning and to maintain academic excellence</li>
                                 
                                 <li>Strategic facilities plans</li>
                                 
                                 <li>Final proposed planning document for submission to the president and trustee</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p>For more information, visit the <a href="../../../../lci/StrategicPlan0813.html">Learning-Centered Initiative</a> site.
                        </p>
                        
                        <p>For more information about the College Budget, which is the fiscal expression of our
                           plans, visit the <a href="http://preview.valenciacollege.edu/budget">Budget Process</a> site.
                        </p>
                        
                        <p>View the Council's <a href="../minutes.html#council">meeting agendas and minutes</a>.
                        </p>
                        
                        <p><a href="overview.html#top">TOP</a></p>
                        
                        <h2>&nbsp;</h2>
                        
                        <h3>Organizational Overview</h3>
                        
                        <a href="../documents/org_chart.ppt" title="View the Organizational Chart for Strategic Planning in PowerPoint format"><img alt="" border="0" height="301" src="strat_plan_org.png" width="582"></a>
                        
                        <p>The College Planning Council will work closely with and consult the Senior Leadership
                           Team as it designs and conducts the planning process. The CPC will charge Task Forces
                           with designing and carrying out the phases of the process, ensuring that decisions
                           are made in a collaborative fashion, using Big and Small Group meetings, consulting
                           with key constituencies, and working through the governing councils and the Senior
                           Leadership Team.
                        </p> 
                        
                        
                        <p>The Planning Steering Committee will be composed of the President (1), the council
                           co-chairs (6), a Trustee (1), and a representative of Career Service and Professional
                           staff (2). The Steering Committee will make final recommenda-
                           tions regarding strategy and the content of the plan.
                        </p>
                        
                        <p>The full Committee will include 20 additional members: 9 senior staff not serving
                           as co-chairs, 2 deans;, 3 additional faculty, 3 additional professional staff, and
                           3 additional career service staff. This group of 30 will review work products, note
                           any additional information or consultation needed, and make needed adjustments  to
                           ensure a clear, consistent, and logical document.
                        </p>
                        
                        <p><a href="overview.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/cpco/overview.pcf">©</a>
      </div>
   </body>
</html>