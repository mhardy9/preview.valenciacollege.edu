<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/learningsymposium.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                   </div> 
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../about/budget/index.html">Budget Process</a></li>
                                                   
                                                   
                                                   <li><a href="../../../lci/plans.html">Learning Centered Initiative</a></li>
                                                   
                                                   <li><a href="http://wp.valenciacollege.edu/strategic-planning/">Strategic Planning Blog</a></li>        
                                                   
                                                </ul>
                                                
                                                
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      <h2><img alt="header" height="162" src="header.png" width="413"></h2>
                                                      
                                                      <h3><strong>Dates and times: Friday, March  (11:30am-5:00pm) or Saturday, March 21, 2015 (9:00am-2:00pm)</strong></h3>
                                                      
                                                      <h3><strong>Location: West Campus, Special Events Center</strong></h3>
                                                      
                                                      <h3><strong>Click <a href="documents/SymposiumonLearninginCommunity2014Flyer.pdf">here</a> to download the event flyer. </strong></h3>
                                                      
                                                      <h3>Overview</h3>
                                                      
                                                      <p>This symposium welcomes all college-wide Valencia faculty and staff to engage in conversations
                                                         focused on the front door alignment, high impact practices, and building connections
                                                         for student success. Within this collaborative community, participants will learn
                                                         from their colleagues and explore innovative strategies to enhance the teaching and
                                                         learning experience. 
                                                      </p>
                                                      
                                                      <p>Within the symposium’s three tracks, sessions will be offered to highlight Valencia’s
                                                         emerging work with curricular alignment, diagnostics and pathways, financial aid,
                                                         LinC, Service Learning, internationalizing the curriculum, co-curricular engagement,
                                                         MEP, CARE, and the NSE. In addition to a comprehensive overview, faculty and staff
                                                         will reflect and inform strategic planning through the participation of a design challenge.
                                                         
                                                      </p>
                                                      
                                                      <p>Speakers and facilitators will include Valencia President Dr. Sandy Shugart and colleagues
                                                         of this emerging work.
                                                      </p>
                                                      
                                                      <h3><img alt="shugart" height="263" hspace="0" src="shugart_000.jpg" vspace="0" width="175"></h3>
                                                      
                                                      <h2>Track 1: Front Door Alignment</h2>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 1: Curricular Alignment</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>Participants will join the Mythbusters team to learn more about the Front Door Alignment
                                                                  initiative and the myths and realities of the New Student Experience. Participants
                                                                  will leave with an understanding of their role in the FDA work and explore ways to
                                                                  infuse key College Success Skills into their classroom and/or learning environment.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 2: Diagnostics and Pathways</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>Participants in this session will be on a mission to unpack the language of our diverse
                                                                  perceptions of “pathways” and “diagnostics” as we work toward the common goal of creating
                                                                  tools to assist students in crafting a learning program that will lead efficiently
                                                                  to successful attainment of their academic goals.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 3: Financial Aid's Impact on Curriculum </p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>This workshop will review how curriculum decisions such as: prerequisite course work,
                                                                  pending degree status and excess hours can affect the cost of a student’s education
                                                                  and how much financial aid will or will not cover.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>
                                                      
                                                      <h2>Track 2: High Impact Practices</h2>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 1: LinC</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>Through the LinC (Learning in Community) initiative, Valencia faculty and staff can
                                                                  strategically offer courses within the new front door alignment. Doing so provides
                                                                  students with clearer pathways and builds community across multiple disciplines. In
                                                                  LinC, two or more courses are taught together with a cohort of students.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 2: Service Learning</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em> Service Learning is the opportunity to join course outcomes with meaningful community
                                                                  service to enhance all facets of student learning. This high impact practice creates
                                                                  avenues for academic engagement, pre-professional exploration, civic engagement and
                                                                  personal growth. Come learn the what, why and how of the ways in which Service Learning
                                                                  can be incorporated into your practice. </em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 3: Internationalizing the Curriculum</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>This session will explore methods to help prepare students to live and work in an
                                                                  interdependent and multicultural world and develop the knowledge, skills and attitudes
                                                                  of a competent global citizen through curricular and co-curricular activities. It
                                                                  will showcase Valencia’s current best practices for Global Distinction.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      <h2>Track 3: Building Connections for Student Success</h2>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 1: Co-curricular</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>What the co-curricular is happening at Valencia? Experience and learn the history
                                                                  and purpose of co-curricular programs at Valencia as well as the innovative opportunities
                                                                  currently in development for students. You’ll experience a taste of co-curricular
                                                                  programs, and understand how students benefit from participating</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 2: MEP</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>MEP 2.0 (My Education Plan) is the latest and greatest tool for students, staff and
                                                                  faculty. This session will focus on the utilization of MEP in our daily work at Valencia,
                                                                  and demonstrate how our students are using the tool to enhance their learning experience
                                                                  and plan their academic journey.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Session 3: C.A.R.E</p>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <p><em>This faculty-led initiative at the College aimed at establishing a systematic process
                                                                  for identifying and supporting struggling students at Valencia. CARE strategies, developed
                                                                  by faculty, teach students the skills to self-assess their study habits EARLY in the
                                                                  semester. Students also learn from CARE faculty what adjustments need to be made to
                                                                  improve their academic performance.</em></p>
                                                            
                                                         </blockquote>
                                                         
                                                      </blockquote>            
                                                      <p><em>NOTE: Participants can choose to attend two sessions from any of the above tracks.
                                                            Both sessions don't need to be within the same track. </em></p>
                                                      
                                                      <p>Agenda at a Glance </p>
                                                      
                                                      <h3>Friday, March 20, 2015</h3>
                                                      
                                                      <blockquote>
                                                         
                                                         <p>11:30: Registration and Boxed lunch</p>
                                                         
                                                         <p>12:00: Keynote address from Dr. Shugart</p>
                                                         
                                                         <p>12:30: Track overviews</p>
                                                         
                                                         <p>1:15: Congruent sessions 1</p>
                                                         
                                                         <p>2:30: Congruent sessions 2</p>
                                                         
                                                         <p>3:30: Design challenge</p>
                                                         
                                                         <p>5:00: Adjourn  </p>
                                                         
                                                      </blockquote>            
                                                      <h3>Saturday, March 21, 2015 </h3>
                                                      
                                                      <blockquote>
                                                         
                                                         <p>8:30: Registration </p>
                                                         
                                                         <p>9:00: Keynote address from Dr. Shugart</p>
                                                         
                                                         <p>9:30: Track overviews</p>
                                                         
                                                         <p>10:15: Congruent sessions 1</p>
                                                         
                                                         <p>11:30: Congruent sessions 2</p>
                                                         
                                                         <p>12:30: Design challenge (boxed lunch provided) </p>
                                                         
                                                         <p>2:00: Adjourn</p>
                                                         
                                                      </blockquote>            
                                                      
                                                      <p>Registration</p>
                                                      
                                                      <p>Open registration will be available from Monday, January 12th through Friday, March
                                                         13th. Participants can click on the day they would like to attend below to be redirected
                                                         to our Eventbrite registration pages. Participants will be asked to choose two sesssions
                                                         when they register, provide their Valencia VID number, and choose a method of compensation
                                                         if they are an adjunct instructor. 
                                                      </p>
                                                      
                                                      <h3>
                                                         <img alt="register" height="86" hspace="0" src="register_logo.jpg" vspace="0" width="200"> 
                                                      </h3>
                                                      
                                                      <blockquote>
                                                         
                                                         <h3>Click a day below to be directed to our Eventbrite registration page: </h3>
                                                         
                                                         <h3><a href="https://www.eventbrite.com/e/symposium-learning-in-our-community-friday-tickets-14755840115">Learning Symposium Registration (Friday, March 20, 2015)</a></h3>
                                                         
                                                      </blockquote>
                                                      
                                                      <h3><a href="https://www.eventbrite.com/e/symposium-learning-in-our-community-saturday-tickets-14756010625">Learning Symposium Registration (Saturday, March 21, 2015)</a></h3>
                                                      
                                                      <h3>Compensation (see below for details) </h3>
                                                      
                                                      <p>Valencia employees can choose to attend the symposium on either March 20th or March
                                                         21st and will receive five professional development hours and a boxed lunch. Adjunct
                                                         instructors, who don't have full-time roles with the college, will have the option
                                                         of receiving professional development hours or a one-time payment of $125 for their
                                                         participation. 
                                                      </p>            
                                                      
                                                      <p>Sponsors</p>
                                                      
                                                      <p>Partners and event sponsors include the offices of Academic Affairs, Faculty Development,
                                                         Organizational Development, and Curriculum Initiatives.
                                                      </p>
                                                      
                                                      <p>Contact</p>
                                                      
                                                      <p>Please direct your questions about the symposium to Robyn Brighton at <a href="mailto:rbrighton1@valenciacollege.edu">rbrighton1@valenciacollege.edu</a>, or 407-582-3895 
                                                      </p>
                                                      
                                                      <p><a href="#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/learningsymposium.pcf">©</a>
      </div>
   </body>
</html>