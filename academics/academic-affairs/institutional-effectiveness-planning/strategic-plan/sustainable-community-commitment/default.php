<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/sustainable-community-commitment/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/sustainable-community-commitment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/sustainable-community-commitment/">Sustainable Community Commitment</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>Impact on Quality of Life</strong></h2>
                        
                        
                        <p>At Valencia, community is part of our DNA. We started as a community college, intended
                           to serve the local community. In the next five years, we will expand our vision for
                           the community—as well as our efforts to build and forge deeper relationships within
                           the community.
                        </p>
                        
                        <p>Together, we want to bring arts into the community, address the health and quality
                           of life in Central Florida and expand opportunities in arts, health, safety and education
                           for underserved communities.
                        </p>
                        
                        
                        <p><strong>Initiative 10: Arts, Health and Civic Engagement</strong></p>
                        
                        <p>Just as Valencia is changing, so is our community. Today, Orlando is a rich melting
                           pot of race and culture. It’s also one of the fastest growing metropolitan areas in
                           the country, according to U.S. Census Bureau statistics. But the great challenge before
                           us is unifying a community of so many diverse interests and cultures, in a place that
                           many do not consider “home.”&nbsp; 
                        </p>
                        
                        
                        <p>One important—and often neglected—way to unite our community comes through the arts.
                           &nbsp;In a 2016 survey, 67% of Americans said they believe “the arts unify our communities
                           regardless of age, race, and ethnicity” and 62% agree that the arts “helps me understand
                           other cultures better.”
                        </p>
                        
                        
                        <p><strong>Initiative 11: Safety</strong></p>
                        
                        <p>With nearly 66 million annual visitors, Central Florida is one of the top tourist
                           destinations in the world. Coupled with the population growth expected over the coming
                           decades and the threat of natural disasters and terrorist attacks, it’s imperative
                           that Valencia College forge new public safety training and education partnerships
                           and raise the standards of public safety education and training.
                        </p>         
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/sustainable-community-commitment/default.pcf">©</a>
      </div>
   </body>
</html>