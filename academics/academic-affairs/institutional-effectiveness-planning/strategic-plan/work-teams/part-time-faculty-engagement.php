<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/part-time-faculty-engagement.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/">Work Teams</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Part-time Faculty Engagement</h2>
                        
                        <p> Our students engage regularly with our part-time faculty as partners in their learning
                           journey. The Part-time Faculty Engagement group is focused on developing and implementing
                           innovative models for ensuring our part-time faculty have access to and choose to
                           participate in ongoing professional development related to teaching and learning.
                           Creating pathways for part-time faculty to feel engaged by and with the College will
                           impact our ability to retain the best faculty who will provide excellent learning
                           opportunities for our students. 
                        </p>
                        
                        <p><strong><u>Vision Statement: </u></strong></p>
                        
                        <p>Valencia College is an engaged, collegial community of diverse professionals dedicated
                           to the best possible student learning outcomes.
                        </p>
                        
                        <p><strong><u>Working Theory: </u></strong></p>
                        
                        <p> Positive student learning outcomes result when supported, engaged part-time faculty
                           and Valencia College are mutually invested in each other. 
                        </p>
                        
                        <p><strong><u>Goals: </u></strong></p>
                        
                        <blockquote>
                           
                           <p>1. Part-time faculty will be developed both in the essential competencies of a Valencia
                              educator and in their respective teaching disciplines
                           </p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Review and redesign the Associate Faculty program.</li>
                                 
                                 <li> Explore opportunities for institutional financial support for part-time faculty development.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           <p>2. Create a new on-boarding, engagement, compensation, and evaluation process for
                              part-time faculty
                           </p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Implement a "New Faculty Experience" to orient and engage part-time faculty.</li>
                                 
                                 <li>Develop a consistent evaluation process for all part-time faculty.</li>
                                 
                                 <li> Design and develop a formal part-time faculty mentoring program to support communities
                                    of practice within the disciplines.
                                 </li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           <p>3. Enhance opportunities for part-time faculty to engage with students outside of
                              the classroom
                           </p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Identify opportunities for part-time faculty to engage with students outside the classroom.</li>
                                 
                                 <li>Design a program to support part-time faculty engagement with students outside of
                                    the classroom.
                                 </li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        <h3>Contacts</h3>
                        
                        <p>If you are interested in becoming part of this strategic initiatives work team, or
                           if you would like to learn more, please contact one of the sponsors below: 
                        </p>
                        
                        <p><strong>Amy Bosley </strong></p>
                        
                        <p>Lead Sponsor </p>
                        
                        <p>Vice President, Organizational Development &amp; Human Resources</p>
                        
                        <p><strong>Susan Ledlow </strong></p>
                        
                        <p>Sponsor</p>
                        
                        <p>Vice President, Academic Affairs &amp; Planning </p>
                        
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/PTFacEngWorkingTheoryandVisionTemplatePPTFINAL.pdf">Working Theory &amp; Vision Statement Report (Draft)</a></li>
                           
                           <li><a href="Part-timeFacultyengagementfinalreportdraft1.docx">Part-time Faculty engagement final report draft 1</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/part-time-faculty-engagement.pcf">©</a>
      </div>
   </body>
</html>