<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/new-student-experience.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/">Work Teams</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>New Student Experience</h2>
                        
                        <p>Valencia's New Student Experience focuses on creating a coordinated first year experience
                           for students new to Valencia. Since the Fall of 2013, faculty and staff have worked
                           to design and develop the NSE Course and meta-major versions; created and revised
                           the NSE co-curricular activities; revised the LiefMap College Success Skills list
                           to a NSE College Success Skills list utilized by faculty teaching the Start Right
                           Courses to support the NSE outcomes; developed a personalized advising plan utilized
                           by the NSE faculty teaching the NSE course;and redesigned the LifeMap/ MEP. The work
                           of the NSE is by no means complete; however. 
                        </p>
                        
                        <p>The NSE Strategic Planning work team will focus on the next steps of the NSE/QEP work
                           to include continued meta-major and guided pathway discussions, infusion of College
                           Success Skills in Start Right courses, integration and use of LifeMap 2.0/MEP, and
                           enhanced personalized advising for every student
                        </p>
                        
                        <p><strong><u>Vision Statement:</u></strong></p>
                        
                        <p>Students have personal connections within and beyond curricular and co-curricular
                           experiences that lead to a successful completion of 18 college-level credits.
                        </p>
                        
                        <p><u><strong>Working Theory:</strong></u></p>
                        
                        <p>Creating a comprehensive and holistic first-year experience that engages students’
                           academic pasts, strengthens the quality of student learning and engagement, and teaches
                           students to establish and follow academic and personal goals for the future will increase
                           student persistence, reduce time to graduation, and encourage students to embrace
                           lifelong learning.
                        </p>
                        
                        <p><u><strong>Goals:</strong></u></p>
                        
                        <blockquote>
                           
                           <p>1. Develop a comprehensive, year-long experience for new students</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Develop a coordinated, co-curricular experience.</li>
                                 
                                 <li> Develop a comprehensive first-year advising model.</li>
                                 
                                 <li> Create a peer mentorship model for new students.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           <p>2. Establish personal connections for all new students across the college</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Educate students about opportunities to get involved in curricular and co-curricular
                                    programs.
                                 </li>
                                 
                                 <li>Increase opportunities for students to participate in cohort-based programs.</li>
                                 
                                 <li>Create a mentorship model that matches students with industry or community partners.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        
                        <blockquote>
                           
                           <p>3. Infuse College Success Skills in Top 10 High Enrolled Courses</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Create supplementary materials for instructors who teach Top 10 courses.</li>
                                 
                                 <li> Create a faculty development plan for instructors who teach Top 10 courses.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        
                        <h3>Contacts</h3>
                        
                        <p>If you are interested in becoming part of this strategic initiatives work team, or
                           if you would like to learn more, please contact one of the sponsors below: 
                        </p>
                        
                        <p><strong>Falecia Williams</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, West &amp; Downtown </p>
                        
                        <p><strong>Kathleen Plinsk</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, Lake Nona, Osceola, Poinciana</p>
                        
                        <p><strong>Stacey Johnson</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, East &amp; Winter Park </p>      
                        <p><strong>Kim Sepich</strong></p>
                        
                        <p>Sponsor</p>
                        
                        <p>Vice President, Student Affairs </p>
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/NSEWorkingTheoryandVisionTemplatePPTFINAL.pdf">Working Theory &amp; Vision Statement Report (Draft) </a></li>
                           
                           <li><a href="StrategicPlanningTemplate_NSE.docx">Strategic Planning Template_NSE</a></li>
                           
                        </ul>
                        
                        <h3>&nbsp;</h3>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/new-student-experience.pcf">©</a>
      </div>
   </body>
</html>