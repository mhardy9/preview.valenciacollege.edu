<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/online-learning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/">Work Teams</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Online Learning </h2>
                        
                        <p>Valencia began offering alternative delivery courses in the early 1990’s, and as technology
                           evolved and student demand grew, Valencia continued to increase the number of course
                           and program offerings in both hybrid and online modalities (Meiller and Associates,
                           2013).&nbsp; Throughout the last decade, Valencia’s online learning practices evolved organically
                           amongst our academic community, while federal and state regulations and requirements
                           increased as the national demand for distance learning escalated and concerns regarding
                           the quality of online learning grew (Allen and Seaman, 2014).&nbsp; 
                        </p>
                        
                        <p>In 2013, Valencia commissioned Diane Meiller and Associates to perform a current state
                           assessment of online learning which was completed in June 2013.&nbsp; This report was intended
                           to “provide us with a shared understanding of where we are today, and lay the foundation
                           for our collaborative planning of how best to move forward in ways that are consistent
                           with Valencia’s learning-centered culture, while embracing the elasticity for change
                           that may be required” (Sandy Shugart, 2014). During the assessment, several recurring
                           themes emerged through interviews, group sessions, survey results and data analysis
                           (Meiller and Associates, 2013).&nbsp; These themes represent several areas where we have
                           opportunities to improve, particularly in terms of providing a consistent experience
                           to students and faculty.&nbsp; To explore these areas, short-term work teams will be commissioned
                           to design and in many cases implement strategies, processes, practices and/or tools
                           that will increase online student success, decrease the gap in success between instructional
                           modalities, and improve the student experience in online and hybrid courses.
                        </p>            
                        <div>
                           
                           <p><strong><u>Vision Statement: </u></strong></p>
                           
                           <p>Valencia is a leader in providing high quality online/hybrid learning and support
                              experiences to a diverse population of students.
                           </p>
                           
                           <p><strong><u>Working Theory: </u></strong></p>
                           
                           <p>Students succeed in online/hybrid courses when decisions are informed, choices are
                              clear, and quality learning and support experiences are consistently delivered.
                           </p>
                           
                           <p><strong><u>Goals: </u></strong></p>
                           
                           <blockquote>
                              
                              <p> 1. Establish a clearly articulated model for quality online/hybrid teaching and learning
                                 at Valencia
                              </p>
                              
                              <blockquote>
                                 
                                 <p><strong>Objectives:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Define the purpose, desired outcomes and intended student experience for online and
                                       hybrid delivery.
                                    </li>
                                    
                                    <li> Create the infrastructure and organizational capacity required to support the intended
                                       student experience.
                                    </li>
                                    
                                    <li> Institute college-wide enrollment and program planning for online and hybrid courses
                                       and programs.
                                    </li>
                                    
                                 </ul>
                                 
                              </blockquote>
                              
                              <p>2.Expand and enhance student service and learning support strategies for the fully
                                 online learner
                              </p>
                              
                              <blockquote>
                                 
                                 <p><strong>Objectives: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>Review and evaluate current student service and learning support strategies to discern
                                       additional needs 
                                    </li>
                                    
                                    <li>Implement additional student service and learning support strategies </li>
                                    
                                    <li>Create tools and practices that foster a sense of community and personal connection
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </blockquote>
                              
                              <p>3. Enhance quality in the online/hybrid environment</p>
                              
                              <blockquote>
                                 
                                 <p><strong>Objectives: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>Create a professional development program to support consistent quality in teaching,
                                       learning and design.
                                    </li>
                                    
                                    <li> Ensure that students "start right" in online and hybrid learning environments.</li>
                                    
                                    <li> Provide resources and guidelines for faculty and deans to incorporate quality course
                                       and curriculum design.
                                    </li>
                                    
                                 </ul>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                           
                           <p><u><strong>Goals removed during Big Meeting 2016</strong></u></p>
                           
                           <ul>
                              
                              <li><strong>Implement an on-going improvement cycle for online and hybrid learning that promotes
                                    informed decision-making</strong></li>
                              
                           </ul>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li>Develop key performance indicators consistent with outcomes identified in the strategic
                                    plan
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           
                           <h3>Contacts</h3>
                           
                           <p>If you are interested in becoming part of this strategic initiatives work team, or
                              if you would like to learn more, please contact one of the sponsors below: 
                           </p>
                           
                           <p><strong>Susan Ledlow</strong></p>
                           
                           <p>Lead Sponsor</p>
                           
                           <p>Vice President, Academic Affairs &amp; Planning </p>
                           
                           <p><strong>John Slot</strong></p>
                           
                           <p>Sponsor</p>
                           
                           <p>Vice President, Information Technology</p>
                           
                           <p><strong>Kim Sepich</strong></p>
                           
                           <p>Sponsor</p>
                           
                           <p>Vice President, Student Affairs  </p>
                           
                           <h3>Documents</h3>
                           
                           <ul>
                              
                              <li><a href="documents/OnlineWorkingTheoryandVisionTemplatePPTFINAL.pdf">Working Theory &amp; Vision Statement Report (Draft)</a></li>
                              
                              <li><a href="StrategicPlanningTemplate-OnlineLearningDraft-FinalGoals_Objectives-WithTeamandSteeringComm.docx">Strategic Planning Template - Online Learning Draft - Final Goals_Objectives - With
                                    Team and Steering Committee Feedback</a></li>
                              
                           </ul>
                           
                           <h3>&nbsp;</h3>
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/online-learning.pcf">©</a>
      </div>
   </body>
</html>