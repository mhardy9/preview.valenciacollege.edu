<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/direct-connect-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/">Work Teams</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>DirectConnect 2.0</h2>
                        
                        <p>In early 2006, Valencia College joined a consortium of state colleges to partner with
                           the University of Central Florida to form Direct Connect.&nbsp;&nbsp; Students will benefit
                           from this partnership in the following ways:
                        </p>
                        
                        <ul>
                           
                           <li>Guaranteed* admission to a bachelor's degree program at UCF. (* Consistent with university
                              policy.)
                           </li>
                           
                           <li>Joint advising from UCF and Valencia help ensure a smooth transition.</li>
                           
                           <li>Taking UCF courses at the main campus or on Valencia's West or Osceola campuses.</li>
                           
                           <li>Availability of UCF staff on-site at Valencia's West and Osceola campuses to help
                              with advising, admissions, financial aid and academic support.
                           </li>
                           
                        </ul>
                        
                        <p>We know that one out of four UCF graduates started at Valencia, what we need to imagine
                           is what the next iteration of this successful program should look like.&nbsp; How do we
                           see this partnership evolving over the next 5 years? What are our hopes for our students'
                           learning?&nbsp; How can this work have a greater impact in our community?
                        </p>
                        
                        <p><u><strong>Vision Statement:</strong></u></p>
                        
                        <p>Valencia students are prepared to transfer to and succeed at UCF.</p>
                        
                        <p><u><strong>Working Theory:</strong></u></p>
                        
                        <p>Transfer readiness requires preparation</p>
                        
                        <p><u><strong>Goals: </strong></u></p>
                        
                        <blockquote>
                           
                           <p>1. Design systems for students to track their progress and readiness</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <blockquote>
                                 
                                 <p>• Create a communication and feedback system for key transfer information.</p>
                                 
                                 <p>• Create online tracking tool for Valencia students to track their progress toward
                                    a UCF bachelor's degree.
                                 </p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                           <p>2. Establish curricular alignment in specific programs, courses, and career pathways</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <blockquote>
                                 
                                 <p>•Review data in transfer courses to determine curriculum alignment conversations needed.<br>
                                    •Develop an ongoing curricular alignment process to show changes across the discipline.<br>
                                    •Align course outcomes.<br>
                                    •Develop advising recommendations based upon curriculum alignment.
                                 </p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                           <p>3. Prepare faculty and staff to advise students and families in the transfer process</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <blockquote>
                                 
                                 <p>•Clarify a common understanding of the way students experience the transfer process.<br>
                                    •Develop, implement, and assess strategies for students to complete the transfer process.
                                 </p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                           <p>4. Prepare students for university experience</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <blockquote>
                                 
                                 <p>• Work with UCF to design readiness assessment modules for students.<br>
                                    
                                 </p>
                                 
                                 <p>• Provide students successful strategies for learning in a large-sized class.</p>
                                 
                                 <p>• Provide students successful strategies for social engagement.</p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                           <p>5. Develop and share a robust data process that informs decision-making at UCF and
                              Valencia
                           </p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <blockquote>
                                 
                                 <p>•Utilize the new Florida I.D. to connect Valencia student records for transferring
                                    to UCF.<br>
                                    •Create a super-user network that responds to data requests.<br>
                                    •Analyze data routinely for continuous improvement.
                                 </p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        
                        <h3>Contacts</h3>
                        
                        <p>If you are interested in becoming part of this strategic initiatives work team, or
                           if you would like to learn more, please contact one of the sponsors below: 
                        </p>
                        
                        <p><strong>Kim Sepich</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Vice President, Student Affairs </p>
                        
                        <p><strong>Joyce Romano </strong></p>
                        
                        <p>Sponsor </p>
                        
                        <p>Vice President, Educational Pathways </p>
                        
                        <p><strong>Susan Ledlow</strong></p>
                        
                        <p>Sponsor</p>
                        
                        <p>Vice President, Academic Affairs &amp; Planning </p>
                        
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li><a href="documents/DirectConnect2.0WorkingTheoryandVisionTemplatePPTFINAL.pdf">Working Theory &amp; Vision Statement (Draft)</a></li>
                           
                           <li><a href="documents/DirectConnect2.0VisionStatementActivitySlide.pdf">DirectConnect 2.0 Vision Statement Activity Slid</a></li>
                           
                           <li><a href="documents/DirectConnectIR30815UCFStudentSuccessFeedbackReports-VC.PDF">DirectConnect IR 30815 UCF Student Success Feedback Reports - VC</a></li>
                           
                           <li><a href="StrategicPlanningDoc-DirectConnect20Draft3.docx">Strategic Planning Doc-Direct Connect 2 0 Draft 3</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/direct-connect-2.pcf">©</a>
      </div>
   </body>
</html>