<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/assessment-for-learning.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/">Work Teams</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Evidence Based Practice</h2>
                        
                        <p><strong>(This entire initiative was subsequently moved under collegewide Capacity Plan)</strong></p>
                        
                        <p>The faculty and staff within the Assessment for Learning team are focused on the people
                           and processes that have the potential to directly impact student learning.&nbsp; The team
                           aims to bring what we are learning across the college into a cohesive and comprehensive
                           view, while also supporting and learning from the work of the other teams.
                        </p>
                        
                        <p><u><strong>Working Theories (Draft): </strong></u></p>
                        
                        <p> A stronger sense of shared purpose leads to meaningful assessment and improved student
                           learning (updated) 
                        </p>
                        
                        <p><strong><u>Vision Statement (Draft): </u></strong></p>
                        
                        <p>Valencia assessment activities lead to improved student learning</p>
                        
                        <p><u><strong>Goals/Objectives (Draft):</strong></u></p>
                        
                        <p>1<strong>. Develop a comprehensive annual assessment strategy</strong></p>
                        
                        <ul>
                           
                           <li>Develop a culture of assessment </li>
                           
                           <li>Create a schedule and budget to conduct Academic Initiative Reviews (AIR)</li>
                           
                           <li>Review General Education outcome assessments to determine student learning during
                              their time at Valencia
                           </li>
                           
                        </ul>
                        
                        <p>2..<strong> Develop an institutional effectiveness plan used to show student learning and to
                              make changes</strong></p>
                        
                        <ul>
                           
                           <li> Create an institutional effectiveness model that evaluates the strategic plan and
                              other initiatives
                           </li>
                           
                           <li>Document and archive cycle of institutional effectiveness to demonstrate how we used
                              data to enhance student learning
                           </li>
                           
                           <li>Utilize CCSSE data for decision making</li>
                           
                           <li>Examine the effectiveness of strategies, processes, and itnerventions resulting from
                              online work plans and the strategic plan 
                           </li>
                           
                           <li>Review QEP and make changes as appropriately based in data to improve student learning</li>
                           
                        </ul>
                        
                        <p>3. <strong>Increase capacity for data </strong></p>
                        
                        <ul>
                           
                           <li>Establish data needs for program viability </li>
                           
                           <li>Review data needs for new program launch</li>
                           
                        </ul>
                        
                        <p><strong>4. Create a cycle that supports new initiatives to sustain a culture of innovation
                              </strong></p>
                        
                        <p><strong><span>(Moved from Teaching &amp; Learning in the 21st Century)</span></strong></p>
                        
                        <ul>
                           
                           <li>Establish a college-wide team that reviews evidence (including student data, community
                              input, financial data) about new and existing initiatives
                           </li>
                           
                           <li>Devise a rubric, process, and timeline for evaluating innovative practices</li>
                           
                           <li> Establish a budget line to support implementation and scaling of selected initiatives
                              on a cyclical and predetermined interval
                           </li>
                           
                        </ul>
                        
                        <p><strong>5. Establish a process by which we ground ourselves in evidence-based practice for
                              teaching and learning (Academic Affairs) </strong></p>
                        
                        <p><strong><span>(Moved from Teaching &amp; Learning in the 21st Century)</span></strong></p>
                        
                        <ul>
                           
                           <li>Review current evidence-based practices that have been proven to improve student success</li>
                           
                           <li>Review and revise our development courses, practices, and programs to integrate evidence-based
                              practices into all our offerings
                           </li>
                           
                           <li>Review current evidence-based practices that have been proven to improve student success</li>
                           
                        </ul>
                        
                        <p><strong>6. Based upon the trends and what we know about evidenced-based practices, identify,
                              design, and implement innovative teaching and learning experiences for 21st Century
                              education (Sponsored by Learning Leadership Council) </strong></p>
                        
                        <p><strong><span>(Moved from Teaching &amp; Learning in the 21st Century)</span></strong></p>
                        
                        <ul>
                           
                           <li>Commission teams to explore and pilot new initiatives and strategies</li>
                           
                           <li>Review and explore promising existing initiatives and strategies</li>
                           
                        </ul>
                        
                        <h3>Contacts</h3>
                        
                        <p>If you are interested in becoming part of this strategic initiatives work team, or
                           if you would like to learn more, please contact one of the facilitators below: 
                        </p>
                        
                        <p><strong>Laura Blasi </strong></p>
                        
                        <p>Facilitator</p>
                        
                        <p>Director, Institutional Assessment </p>
                        
                        <p><strong>Roberta Carew </strong></p>
                        
                        <p>Co-Facilitator</p>
                        
                        <p>Professor, Mathematics</p>
                        
                        
                        <h3>Documents</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/AssessmentWorkingTheoryandVisionTemplatePPTFINAL.pdf">Working Theory &amp; Vision Statement Report (Draft) </a> 
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Meeting Dates</h3>
                        
                        <p><u><strong>Timeline of events: </strong></u></p>
                        
                        
                        <p>1. October 2 (Fri.): Team begins refining the interview questions we will use</p>
                        
                        <p>2. By October 9 (Fri.): Team members will have read the questions built on the format
                           we practiced Sept. 25th and suggested any additions
                        </p>
                        
                        <p>3. By October 16 (Fri.): Team members receive the final list of the questions (edited)
                           to use when interviewing (“each on reach one” activity)
                        </p>
                        
                        <p>4. By Oct. 30 (Fri.): Team members will conduct individual interviews and post results</p>
                        
                        <p>5. By November 13 (Fri.): Team compiles and makes observations based on results.</p>
                        
                        <p>6. By November 20 (Fri.): Ideas will be written up for formal report by select team
                           members.
                        </p>
                        
                        <p><u><strong>Upcoming Meetings: </strong></u></p>
                        
                        <p>None at this time. </p>
                        
                        
                        <p><u><strong>Past Meetings: </strong></u></p>
                        
                        <p><strong>Kick-Off Big Meeting </strong></p>
                        
                        <p>Date: Friday, September 25, 2015 </p>
                        
                        <p>Time: 11:30 to 4:30PM</p>
                        
                        <p>Location: School of Public Safety </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/work-teams/assessment-for-learning.pcf">©</a>
      </div>
   </body>
</html>