<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/knowledge-and-innovation-economy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/">Workforce Economic Commitment</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Knowledge &amp; Innovation Economy </h2>
                        
                        
                        <p><u><strong>Vision Statement:</strong></u></p>
                        
                        <p>All students should be prepared to compete in an economy that is increasingly globalized
                           and entrepreneurial
                        </p>
                        
                        
                        <p><u><strong>Working Theory:</strong></u></p>
                        
                        <p> Innovation transforms knowledge. <em>(Advancement of knowledge in relation to economic activities.)</em>  
                        </p>
                        
                        <p><strong><u>Goals:</u></strong></p>
                        
                        <blockquote>
                           
                           <p>1. Foster entrepreneurial spirit to encourage business growth</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Create certificates, programs or institutes in entrepreneurship.</li>
                                 
                                 <li>Provide continuing education courses for evolving industries.</li>
                                 
                                 <li>Develop new programming, degrees and certifications that integrate entrepreneurial
                                    skills.
                                 </li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           
                           <p>2. Entice business relocations by aligning educational programs to company needs</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Conduct job task and skill-gap analysis with businesses and educational partners to
                                    track modern-day needs and tailor programs appropriately.
                                 </li>
                                 
                                 <li>Hold collaboration summits to encourage cross-sector collaboration. </li>
                                 
                                 <li>Identify emerging career pathways and career progression in Central Florida.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           
                           <p>3. Create new pathways to well-paying careers</p>
                           
                           <blockquote>
                              
                              <p><strong>Objectives:</strong></p>
                              
                              <ul>
                                 
                                 <li>Create opportunities for students to interact with a wider cross-section of business
                                    leaders in sector summits.
                                 </li>
                                 
                                 <li>Connect Valencia College's students with CareerSource and other community partners
                                    for higher job placement rates.
                                 </li>
                                 
                                 <li>Expand students' understanding of multiple careers available within industries.</li>
                                 
                                 <li>Explore with industry partners new sectors that respond to emerging training and educational
                                    needs.
                                 </li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           
                           
                        </blockquote>            
                        <p><u><strong>Contacts:</strong></u></p>
                        
                        <p>If you are interested in becoming part of this strategic initiatives work team, or
                           if you would like to learn more, please contact one of the sponsors below: 
                        </p>
                        
                        <p><strong>Falecia Williams</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, West &amp; Downtown </p>
                        
                        <p><strong>Kathleen Plinske</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, Lake Nona, Osceola, Poinciana</p>
                        
                        <p><strong>Stacey Johnson</strong></p>
                        
                        <p>Lead Sponsor</p>
                        
                        <p>Campus President, East &amp; Winter Park   </p>
                        
                        <p><strong>Joe Battista</strong></p>
                        
                        <p>Sponsor</p>
                        
                        <p>Vice President, Global, Professional, and Continuing Education</p>
                        
                        <p><strong>Susan Ledlow</strong></p>
                        
                        <p>Sponsor</p>
                        
                        <p>Vice President, Academic Affairs &amp; Planning </p> 
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/knowledge-and-innovation-economy.pcf">©</a>
      </div>
   </body>
</html>