<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/">Workforce Economic Commitment</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2><strong>Impact on Economy and Workforce</strong></h2>
                        
                        
                        <p>Across the nation, we are witnessing the growing need for a new type of worker—skilled
                           workers whose jobs may not require a bachelor’s degree, but require more technical
                           education beyond a high-school diploma. By 2020, nearly two out of every three jobs
                           in the U.S. will require some post-secondary education or training.
                        </p>
                        
                        
                        <p>When we look outside our classrooms to the larger community, what we see is the tale
                           of two cities. Just as there is the bright, shiny, hopeful image of Orlando as a city
                           that is transforming itself into high-tech hub, there’s another Orlando, where workers
                           may spend a lifetime earning no more than the minimum wage. We lead the nation in
                           a category that we shouldn’t be proud of: The Orlando metropolitan area has the lowest
                           median wages in the nation ($29,781), among the top 50 metro areas—and, even worse,
                           25% of the jobs in the region pay $20,220 or less.
                        </p>
                        
                        
                        <p>As we look forward to the next five years, we are focusing our workforce efforts on
                           transforming our community—and helping the people at the lower rungs of our economy.
                           Given the pervasive skill shortages across the country, employers are desperate to
                           find cities where there’s a pipeline of ready, skilled workers. We believe that, in
                           partnership with the local community, we can “skill up” our community and lead a community-wide
                           effort to turn Central Florida into a region with a talented, skilled workforce ready
                           for hire.
                        </p>
                        
                        <p>As we survey the landscape of the Central Florida economy, all indicators point to
                           increased growth and job creation in Orange and Osceola Counties. Construction and
                           healthcare are expected to lead the way for job growth during the next seven years.
                           That provides Valencia College with the chance to train people in high-demand jobs
                           and careers. We have an opportunity to help those who will become engineers and computer
                           scientists or nurses and respiratory therapists, but also those others who want to
                           transform their lives, but don’t see an avenue to accomplish that.
                        </p>
                        
                        
                        <p>It’s no longer enough to open our doors, teach students, award them a two-year degree
                           and wish them well as they graduate. We <em>will </em>do more—for those students who plan to directly enter the workforce when they graduate
                           from Valencia, we will offer more opportunities for meaningful internships or part-time
                           jobs while they’re in college, so that they can complete college with skills that
                           make them more marketable. As other students seek a four-year degree, we will guide
                           them on the best pathway to transfer to a selective university, where they can feel
                           confident in their preparation to master any field or profession they may choose.
                        </p>
                        
                        
                        <p>For those on the bottom rungs on the job ladder, we will find ways to help them achieve
                           the next step, a more skilled job as they climb toward prosperity, by providing affordable,
                           relevant, and rapid training programs.
                        </p>
                        
                        
                        <p><strong>Initiative 6: Accelerated Training</strong></p>
                        
                        <p>One of the most neglected segments of the workforce is the unskilled, unemployed or
                           underemployed worker. And in Central Florida, where about 28% of adults over the age
                           of 25 have only a high school diploma and where another 20% have taken some college
                           classes but have no degree, we believe there’s a large population that can benefit
                           from short-term training that will provide an entry point into good-paying jobs. Valencia
                           College has already experienced great success in offering such training in two areas
                           of the Central Florida economy with proven skill shortages: advanced manufacturing
                           and construction. We plan to build on this success in additional areas.
                        </p>
                        
                        <p><strong>Initiative 8: Work-Based Learning</strong></p>
                        
                        <p>This initiative will increase the number of students who receive credit for prior
                           experience and will increase the number of Valencia students who participate in internships
                           or some type of work-based experiences.<strong> </strong></p>
                        
                        
                        <p><strong>Initiative 7: Tech Express to Valencia</strong></p>
                        
                        <p>In 1973, nearly three out of four U.S. jobs required only a high school education
                           or less. By 2020, two out of three jobs will require some postsecondary education
                           or training. &nbsp;Why? Because today’s jobs rely on computers and other technology, employers
                           are demanding worker with some post-secondary education, preferably with technical
                           certifications or degrees.
                        </p>
                        
                        
                        <p>Training for these “middle skill jobs” is typically found at career and technical
                           education centers, such as OCPS’s Orange Technical College and TECO in Osceola County.
                           In the Central Florida region, technical centers currently serve more than 30,000
                           students each year. We believe that, by partnering with the technical centers and
                           local K-12 school districts, we can help students expand on that technical education.
                        </p>
                        
                        
                        <p>Through Tech Express, students who are taking technical classes in high school or
                           after high school could transfer into Valencia programs and earn A.S. degrees that
                           would enable them to raise their standard of living. In their first year after graduation,
                           Valencia’s Associate in Science degree graduates earn an average annual salary of
                           $41,000 (about 60% more than a high school graduate), according to the latest data
                           from the Florida Education and Training Placement Information Program.
                        </p>
                        
                        
                        <p><strong>Initiative 9: Knowledge and Innovation Economy</strong></p>
                        
                        <p>In Central Florida’s diverse workforce, we need not just doctors, lawyers and engineers,
                           but the many support layers that make up a healthy, thriving economy. Employers need
                           engineering technicians and computer programmers, nurses and cyber-security specialists,
                           construction managers and digital media specialists. We need smart, highly-trained
                           people to run today’s smart buildings and a legion of skilled health-care workers
                           to staff our hospitals.
                        </p> 
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="../documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/workforce-economic-commitment/default.pcf">©</a>
      </div>
   </body>
</html>