<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/strategicplanning2010-11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Strategic Goal Teams 2010-11</h2>
                        
                        <p><strong> This webpage contains archive information <u>only</u>. </strong></p>
                        
                        <p>In 2009-10, the College Planning Council convened a <strong>Strategic Goal Teams</strong> for each of the four goals in the College Strategic Plan: 1) Build Pathways, 2) Learning
                           Assured, 3) Invest in Each Other, and 4) Partner with the Community. Each goal team
                           will monitor progress toward a goal, collect and interpret data, monitor related strategic
                           issues, make recommendations to the annual Big Meeting, and prepare an annual report
                           on progress toward the goal. 
                        </p>
                        
                        
                        <h3> Summaries of Progress on Goals 2009-10 </h3>
                        
                        <p>College Planning Council  2009-10 Co-chairs Amy Bosley and Susan Kelley drafted a
                           two-page summary of the findings of each 09-10 goal teams, presented in these four
                           documents. The first page of each summary lists the goal and its objectives, and states
                           the long-term aims set by the team. The second page provides a summary of progress
                           in 2009-10. 
                        </p>
                        
                        
                        <p><strong>Strategic Goal Team One: Build Pathways</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalOneSummaryOctober2010.docx">CPC Goal One Summary October 2010</a></li>
                           
                           <li><a href="documents/StrategicGoals1BoardPresentationNotesAdded21311.ppt">Strategic Goals One Board Presentation</a></li>
                           
                        </ul>
                        
                        <p><strong>Strategic Goal Team Two: Learning Assured</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTwoSummaryOctober2010.docx">CPC Goal Two Summary October 2010</a></li>
                           
                           <li><a href="documents/StrategicGoal2BoardPresentationApril16AMABKEedits.ppt">Strategic Goals Two Board Presentation</a></li>
                           
                        </ul>
                        
                        <p><strong>Strategic Goal Team Three: Invest in Each Other</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalThreeSummaryOctober2010.docx">CPC Goal Three Summary October 2010</a></li>
                           
                           <li><a href="documents/StrategicGoal3BoardPresentationMay15.pptx">Strategic Goals Three Board Presentation</a></li>
                           
                        </ul>
                        
                        <p><strong>Strategic Goal Team Four: Partner with the Community</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalFourSummaryOctober2010.docx">CPC Goal Four Summary October 2010</a></li>
                           
                           <li><a href="documents/StrategicGoal4BoardPresentationJune_20_AM_draft.ppt">Strategic Goals Four Board Presentation</a></li>
                           
                        </ul>
                        
                        
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                        <h3>Combined Strategic Goal Team Meeting October 22, 2010 </h3>
                        
                        <p>College Planning Council  2010-11 Co-chairs Ed Frame and Susan Kelley convened the
                           2010-11 Strategic Goal Teams in a combined meeting on October 22, 2010. Documents
                           distrubuted to the new goal team members are listed below.
                        </p>
                        
                        
                        <p><strong>Team Rosters</strong></p>
                        
                        <ul>
                           
                           <li> <a href="documents/CombinedStrategicGoalTeamRosterNov152010.docx">Combined Strategic Goal Team Roster Nov 15 2010 </a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><strong>Agenda &amp; Documents</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/JointGoalTeamMeetingAgendaOct2220101_000.docx">Joint Goal Team Meeting Agenda Oct 22 2010</a></li>
                           
                           <li><a href="documents/GoalTeamPrinciples09-10_000.docx">Goal Team Principles 09-10</a></li>
                           
                           <li><a href="documents/GoalTeamsNotesonTaxonomyOct222010.docx">Goal Teams Notes on Taxonomy Oct 22 2010</a></li>
                           
                           <li><a href="documents/GoalTeam1TableNotesonEightTaxonomyQuestions.docx">Goal Team 1 Table Notes on Eight Taxonomy Questions</a></li>
                           
                           <li><a href="documents/GoalTeam1OrganizationalSession.docx">Goal Team 1 Organizational Session</a></li>
                           
                           <li><a href="documents/GoalTeam2TableNotesonEightTaxonomyQuestions.docx">Goal Team 2 Table Notes on Eight Taxonomy Questions</a></li>
                           
                           <li><a href="documents/GoalTeam2OrganizationalSession.docx">Goal Team 2 Organizational Session</a></li>
                           
                           <li><a href="documents/GoalTeam3TableNotesonEightTaxonomyQuestions.docx">Goal Team 3 Table Notes on Eight Taxonomy Questions</a></li>
                           
                           <li><a href="documents/GoalTeam3OrganizationalSession.docx">Goal Team 3 Organizational Session</a></li>
                           
                           <li><a href="documents/GoalTeam4TableNotesonEightTaxonomyQuestions.docx">Goal Team 4 Table Notes on Eight Taxonomy Questions</a></li>
                           
                           <li><a href="documents/GoalTeam4OrganizationalSession.docx">Goal Team 4 Organizational Session</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>Team Meetings &amp; Instructions </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CombinedStrategicGoalTeamMeetingCalendar-Round1.docx"> Combined Strategic Goal Team Meeting Calendar - Round 1</a></li>
                           
                           <li> <a href="documents/WEAVEONLINEAccessInstructionsforGoalTeamsNov2010.docx">Weave Online Goal Report Instructions </a>
                              
                           </li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3>Strategic Goal Team One: Build Pathways</h3>
                        
                        <p><strong>Baseline Data:</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/Baselinedata1.1MarketShare.pdf">Baseline data 1.1 Market Share</a></li>
                           
                           <li><a href="documents/BaselineData1.1HighSchools.xlsx">Baseline Data 1.1 High Schools</a></li>
                           
                           <li><a href="documents/BaselineData1-1College-going.pptx">Baseline Data 1.1 College-going</a></li>
                           
                           <li><a href="documents/BaselineData1-1SeminoleCollege-going.xlsx">Baseline data 1.1 Seminole College-going</a></li>
                           
                           <li><a href="documents/BaselineData1.2.pdf">Baseline Data 1.2</a></li>
                           
                           <li><a href="documents/BaselineData1-2GatewayGaps.pptx">Baseline Data 1.2 Gateway Gaps</a></li>
                           
                           <li><a href="documents/BaselineData1.3withdrawals.xlsx">Baseline Data 1.3 withdrawals</a></li>
                           
                           <li><a href="documents/BaselineData1.3Completion.docx">Baseline Data 1.3 Completion</a></li>
                           
                           <li><a href="documents/BaselineData1-3GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 1.3 Gateway Gap Model FTIC Students 20110131</a></li>
                           
                           <li><a href="documents/BaselineData1-3GatewayGapAnalysisbyCourse.pptx">Baseline Data 1.3 Gateway Gap Analysis by Course</a></li>
                           
                           <li><a href="documents/BaselineData1-3StrategicIndicatorsReport.docx">Baseline Data 1.3 Strategic Indicators Report</a></li>
                           
                           <li><a href="documents/BaselineData1-3DFWITop10Courses.pptx">Baseline Data 1.3 DFWI Top 10 Courses</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="documents/BaselineData1.4EconDev.doc">Baseline Data 1.4 Econ Dev</a></li>
                           
                           <li><a href="documents/BaselineData1.5Scholarships.docx">Baseline Data 1.5 Scholarships</a></li>
                           
                           <li><a href="documents/Baseline1-5FinancialAid2008.pdf">Baseline Data 1.5 Financial Aid 2008</a></li>
                           
                           <li><a href="documents/Baselinedata1-5DirectConnectEnrollment2007-102.xlsx">Baseline data 1-5 Direct Connect Enrollment 2007-10 2</a></li>
                           
                           <li><a href="documents/BaselineData1-5AlternativeDelivery.pptx">Baseline Data 1.5 Alternative Delivery</a></li>
                           
                           <li><a href="documents/BaselineData1.5Facilities.docx">Baseline Data 1.5 Facilities</a></li>
                           
                           <li><a href="documents/BaselineData1-5DirectConnectSTEMdata.pptx">Baseline Data 1.5 Direct Connect STEM data</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>1st Goal Team 1 Meeting</strong></p>
                        
                        <p>Date: December 1, 2010 – 1:30PM to 3:30PM</p>
                        
                        <p>Location: CJI 143</p>
                        
                        <p><strong>Agenda &amp; Documents</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam1AgendaFirstSoloMeetingDec12010.docx"> CPC Goal Team 1 Agenda First Solo Meeting Dec 1 2010</a></li>
                           
                           <li><a href="documents/WeaveGoal1Nov182010.pdf">Weave Goal 1 Nov 18 2010</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>2nd Goal Team 1 Meeting</strong></p>
                        
                        <p>Date: February 7, 2011 - 9:00AM to 12:00PM</p>
                        
                        <p>Location: CJI 148</p>
                        
                        <p><strong>Agenda &amp; Documents </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam1AgendaSecondSoloMeetingFeb72011.doc">CPC Goal Team 1 Agenda Second Solo Meeting Feb 7 2011</a></li>
                           
                           <li><a href="documents/CPCGoalTeam1MinutesFirstSoloMeetingDec12010.docx">CPC Goal Team 1 Minutes First Solo Meeting Dec 1 2010</a></li>
                           
                           <li><a href="documents/CPCGoalTeam1ReferenceforIssuesInitiativesFeb72011.docx">CPC Goal Team 1 Reference for Issues Initiatives Feb 7 2011</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>3rd  Goal Team 1 Meeting</strong></p>
                        
                        <p>Date: April 11, 2011 - 2:00PM to 4:30PM</p>
                        
                        <p>Location: CJI 148</p>
                        
                        <p><strong>Agenda &amp; Documents </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam1AgendaSoloMeetingApril2011.doc">CPC Goal Team 1 Agenda Solo Meeting April 2011</a></li>
                           
                           <li><a href="documents/CPCGoalTeam1MinutesSoloMeetingFeb72011.docx">CPC Goal Team 1 Minutes Solo Meeting Feb 7 2011</a></li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                        
                        <h3>Strategic Goal Team Two: Learning Assured </h3>
                        
                        <p><strong>Baseline Data:</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/BaselineData2-1and2-2.docx">Baseline Data 2-1 and 2-2</a></li>
                           
                           <li><a href="documents/BaselineData2-3.pdf">Baseline Data 2-3</a></li>
                           
                           <li><a href="documents/BaselineData2-4.pdf">Baseline Data 2-4</a></li>
                           
                           <li><a href="documents/BaselineData2-415CollegeCredits.xlsx">Baseline Data 2-4 15 College Credits</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><a href="documents/BaselineData2-5GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 2-5 Gateway Gap Model FTIC Students 20110131</a></li>
                           
                           <li><a href="documents/BaselineData2-5GatewayGapAnalysisbyCourse.pptx">Baseline Data 2-5 Gateway Gap Analysis by Course</a></li>
                           
                           <li><a href="documents/Baseline2-5-StrategicIndicatorsReport.docx">Baseline 2-5 - Strategic Indicators Report</a></li>
                           
                        </ul>
                        
                        <p><strong>1st Goal Team 2 Meeting</strong></p>
                        
                        <p>Date: November 19, 2010 - 2:00 p.m. to 4:00 p.m.</p>
                        
                        <p>Location: CJI 150</p>
                        
                        <p><strong>Agenda &amp; Documents</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam2AgendaFirstSoloMeetingNov192010.doc"> CPC Goal Team 2 Agenda First Solo Meeting Nov 19 2010</a></li>
                           
                           <li><a href="documents/WeavePlansGoal2Nov122010.pdf">Weave Plans Goal 2 Nov 12 2010</a></li>
                           
                           <li><a href="documents/Goal2AttachmentNov19-ProgramLearningOutcomeAssessmentPlanTemplate.docx">Goal 2 Attachment Nov 19 - Program Learning Outcome Assessment Plan Template</a></li>
                           
                           <li><a href="documents/Goal2AttachmentNov19IEComponentGoal.doc">Goal 2 Attachment Nov 19 IE Component Goal</a></li>
                           
                           <li><a href="documents/Goal2AttachmentNov19AssessingCollegeLevelWritinginENC1101.doc">Goal 2 Attachment Nov 19 Assessing College Level Writing ENC1101</a></li>
                           
                           <li><a href="documents/Goal2AttachmentNov19AcademicPrograms.doc">Goal 2 Attachment Nov 19 Academic Programs</a></li>
                           
                           <li><a href="documents/Goal2AttachmentNov19LETRubricforWrittenCommunication.pdf">Goal 2 Attachment Nov 19 LET Rubric for Written Communication</a></li>
                           
                           <li><a href="documents/Goal2MinutesOctober222010.docx">Goal 2 Minutes October 22 2010</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>2nd Goal Team 2 Meeting</strong></p>
                        
                        <p>Date: February 3, 2011 - 1:30PM to 4:30PM </p>
                        
                        <p>Location: EC 3-113 </p>
                        
                        <p><strong>Agenda &amp; Documents </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam2AgendaSecondSoloMeetingFeb32011.doc">CPC Goal Team 2 Agenda Second Solo Meeting Feb 3 2011</a></li>
                           
                           <li><a href="documents/CPCGoal2AgendaFeb32011AttachmentB.docx">CPC Goal 2 Agenda Feb 3 2011 Attachment B</a></li>
                           
                           <li><a href="documents/CPCGoalTeam2MinutesFirstSoloMeetingNov192010.doc">CPC Goal Team 2 Minutes First Solo Meeting Nov 19 2010</a></li>
                           
                           <li><a href="documents/WeavePlansGoal2Nov122010_000.pdf">Weave Plans Goal 2 Nov 12 2010</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>3rd Goal Team 2 Meeting</strong></p>
                        
                        <p>Date: April 15, 2011 - 
                           
                           
                           2:00PM to 4:00PM
                        </p>
                        
                        <p>Location: CJI 140 </p>
                        
                        <p><strong>Agenda &amp; Documents </strong></p>
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                        
                        <h3>Strategic Goal Team Three: Invest in Each Other </h3>
                        
                        <p><strong>Baseline Data: </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/BaselineData3.2-StaffOrgDevPlan2008-09.pdf">Baseline Data 3.2 - StaffOrg Dev Plan 2008-09</a></li>
                           
                           <li><a href="documents/BaselineData3.2-LeadershipValenciaOfferings2008-09.pdf">Baseline Data 3.2 - Leadership Valencia Offerings 2008-09</a></li>
                           
                           <li><a href="documents/Baseline3.3-EmployeeSurvey.docx">Baseline 3.3 - Employee Survey</a></li>
                           
                           <li><a href="documents/Baseline3.3-CignaWellnessAggregateData.docx">Baseline 3.3 - Cigna Wellness Aggregate Data</a></li>
                           
                        </ul>
                        
                        
                        <p><strong><em>Subgroup 3.1 </em></strong></p>
                        
                        <blockquote>
                           
                           <p><strong>1st Goal Team 3.1 Meeting</strong></p>
                           
                           <p>Date: November 10, 2010 - 9:00 a.m. to 11:00 a.m.</p>
                           
                           <p>Location: EC 3-113 &amp; <em>GoToMeeting</em> 
                           </p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam31AgendaFirstSoloMeetingNov102010.doc">CPC Goal Team 3 1 Agenda First Solo Meeting Nov 10 2010</a></li>
                              
                              <li><a href="documents/WeaveGoal3OutcomeandObjective1Nov9report.pdf">Weave Goal 3 Outcome and Objective 1 Nov 9 report</a></li>
                              
                              <li><a href="documents/SharedGovernanceDocuments.docx">Shared Governance Documents</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <blockquote>
                           
                           <p><strong>2nd Goal Team 3.1 Meeting (Subcommittee)</strong></p>
                           
                           <p>Date: March 21, 2011 - 
                              
                              
                              2:00PM to 4:00PM
                           </p>
                           
                           <p>Location: 
                              
                              
                              WC SSB-171J
                           </p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam3-1AgendaThirdSoloMeetingMarch212011.doc">CPC Goal Team 3-1 Agenda Third Solo Meeting March 21 2011</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        
                        <blockquote>
                           
                           <p><strong>3rd Goal Team 3.1 Meeting</strong></p>
                           
                           <p>Date: April 14, 2011 - 
                              
                              
                              3:00PM to 4:30PM
                           </p>
                           
                           <p>Location: <em>GoToMeeting</em></p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           
                           
                        </blockquote>
                        
                        <p><strong><em>Subgroup 3.2 </em></strong></p>
                        
                        <blockquote>
                           
                           <p><strong>1st Goal Team 3.2 Meeting</strong></p>
                           
                           <p>Date: November 15, 2010 - 2:00 p.m. to 5:00 p.m.</p>
                           
                           <p>Location: CJI 219 </p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam32AgendaFirstSoloMeetingNov152010.doc"> CPC Goal Team 3 2 Agenda First Solo Meeting Nov 15 2010</a></li>
                              
                              <li><a href="documents/WeaveGoal3Outcome2andObjective2Nov10.pdf">Weave Goal 3 Outcome 2 and Objective 2 Nov 10</a></li>
                              
                           </ul>
                           
                           
                           <p><strong>2nd Goal Team 3.2 Meeting</strong></p>
                           
                           <p>Date: January 24, 2011 - 2:00 p.m. to 5:00 p.m.</p>
                           
                           <p>Location: CJI 219</p>
                           
                           <p><strong>Agenda &amp; Documents </strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam3-2AgendaSoloMeetingJan242011.docx">CPC Goal Team 3-2 Agenda Solo Meeting Jan 24 2011</a></li>
                              
                              <li><a href="documents/CPCGoalTeam3-2MinutesfromNov152010.doc">CPC Goal Team 3-2 Minutes from Nov 15 2010</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        <blockquote>
                           
                           <p><strong>3rd Goal Team 3.2 Meeting</strong></p>
                           
                           <p>Date: March 22, 2011 - 
                              
                              
                              2:00PM to 5:00PM
                           </p>
                           
                           <p>Location: CJI 219</p>
                           
                        </blockquote>
                        
                        <blockquote>
                           
                           <p><strong>Agenda &amp; Documents </strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam3-2AgendaThirdSoloMeetingMar222011.doc">CPC Goal Team 3-2 Agenda Third Solo Meeting Mar 22 2011</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        
                        <p><strong><em>Subgroup 3.3 </em></strong></p>
                        
                        <blockquote>
                           
                           <p><strong>1st Goal Team 3.3 Meeting</strong></p>
                           
                           <p>Date: December 1, 2010 - 9:00 a.m. to 10:30 a.m. </p>
                           
                           <p>Location: CJI 143 </p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam33AgendaFirstSoloMeetingDec120101.doc"> CPC Goal Team 3 3 Agenda First Solo Meeting Dec 1 20101</a></li>
                              
                              <li><a href="documents/Goal3.3WeavePlans.pdf">Goal 3.3 Weave Plans</a></li>
                              
                           </ul>
                           
                        </blockquote>
                        
                        
                        <blockquote>
                           
                           <p><strong>2nd Goal Team 3.3 Meeting</strong></p>
                           
                           <p>Date: January 28, 2011 - 
                              
                              
                              10:00AM to 12:00PM
                           </p>
                           
                           <p>Location: CJI 143 </p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam33AgendaSecondMeetingJan282011.docx">CPC Goal Team 3 3 Agenda Second Meeting Jan 28 2011</a></li>
                              
                              <li><a href="documents/CPCGoalTeam33MinutesFirstMeetingDec12010.doc">CPC Goal Team 3 3 Minutes First Meeting Dec 1 2010</a></li>
                              
                           </ul>
                           
                           
                           <p> <strong>3rd Goal Team 3.3 Meeting</strong></p>
                           
                           <p>Date: April 4, 2011 - 
                              
                              
                              2:30PM to 4:30PM
                           </p>
                           
                           <p>Location: CJI 148</p>
                           
                           <p><strong>Agenda &amp; Documents</strong></p>
                           
                           <ul>
                              
                              <li><a href="documents/CPCGoalTeam3-3AgendaThirdSoloMeetingApril420111.doc">CPC Goal Team 3-3 Agenda Third Solo Meeting April 4 20111</a></li>
                              
                              <li><a href="documents/CPCGoalTeam3-3MinutesSecondSoloMeetingJan282011.doc">CPC Goal Team 3-3 Minutes Second Solo Meeting Jan 28 2011</a></li>
                              
                           </ul>
                           
                           
                        </blockquote>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3>Strategic Goal Team Four: Parther with the Community </h3>
                        
                        <p><strong>Baseline Data:</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/Baselinedata4.1FundsRaised.xls">Baseline data 4.1 Funds Raised</a></li>
                           
                           <li><a href="documents/Baselinedata4.2AlumniGiving.docx">Baseline data 4.2 Alumni Giving</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>1st Goal Team 4 Meeting</strong></p>
                        
                        <p>Date: December 3, 2010 - 1:00PM to 4:00PM </p>
                        
                        <p>Room: CJI 143 </p>
                        
                        <p><strong>Agenda &amp; Documents</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam4AgendaFirstSoloMeetingDec32010_000.doc"> CPC Goal Team 4 Agenda First Solo Meeting Dec 3 2010</a></li>
                           
                           <li><a href="documents/Goal4WeaveReportNov2010.pdf">Goal 4 Weave Report Nov 2010</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>2nd Goal Team 4 Meeting</strong></p>
                        
                        <p>Date: January 26, 2011 - 9:00AM to 12:00PM</p>
                        
                        <p>Location: CJI 148</p>
                        
                        <p><strong>Agenda &amp; Documents </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam4AgendaSecondSoloMeetingJan262011.doc">CPC Goal Team 4 Agenda Second Solo Meeting Jan 26 2011</a></li>
                           
                           <li><a href="documents/CPCGoalTeam4MinutesofFirstSoloMeetingDec12010.doc">CPC Goal Team 4 Minutes of First Solo Meeting Dec 1 2010</a></li>
                           
                        </ul>
                        
                        
                        <p><strong>3rd Goal Team 4 Meeting</strong></p>
                        
                        <p>Date: March 1, 2011 - 9:00AM to 12:00PM</p>
                        
                        <p>Location: CJI 109</p>
                        
                        <p><strong>Agenda &amp; Documents</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/CPCGoalTeam4AgendaThirdSoloMeetingMarch12011.doc">CPC Goal Team 4 Agenda Third Solo Meeting March 1 2011</a></li>
                           
                           <li><a href="documents/CPCGoalTeam4MinutesSecondMeetingJan262011.doc">CPC Goal Team 4 Minutes Second Meeting Jan 26 2011    </a></li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        <h3>
                           <strong>Goal Teams </strong><strong>Roles and Responsibilities</strong>
                           
                        </h3>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Role</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Responsibility</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Convener</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Schedules Goal Team Meetings with members (help with room reservations is available
                                       through CPC staff); prepares agenda
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Recorder</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Records minutes of meetings and distributes to group and CPC staff</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Reporter</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Reports group progress, findings, etc. to CPC as needed</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Seekers</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Actively seeks information to answer group questions or needs (could be two or three
                                       people in this role)
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Questioners</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Encourages group to explore multiple scenarios and ideas when addressing data sets
                                       and assumptions(could be two or three people in this role)
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p>Principles Monitor</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Monitors the group’s adherence to design principles and helps the group through tough
                                       conversations by focusing on principles
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="#top">TOP</a> 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/strategicplanning2010-11.pcf">©</a>
      </div>
   </body>
</html>