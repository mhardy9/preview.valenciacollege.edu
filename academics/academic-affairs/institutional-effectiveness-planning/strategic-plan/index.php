<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li>Strategic Plan</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>The Power To Serve</strong></h2>
                        
                        <h3>Valencia’s Five-Year Impact Plan</h3>
                        
                        
                        <p>In Central Florida and across the nation, we are poised at the edge of a great generational
                           challenge. How do we create genuine opportunity for everyone, no matter their background?
                           This challenge has three parts. First, continuing to create real educational equity,
                           equity not just in access, but in outcomes. Second, especially here in Central Florida,
                           where we are blessed by one of the largest “entry” economies in the world, growing
                           the higher wage economy and fostering more economic mobility for those stranded on
                           the lower rungs of the economic ladder for lack of marketable skills. And third, improving
                           not just the economy, but also the quality of life in our communities.
                        </p>
                        
                        
                        <p>At Valencia College, we have spent the past fifteen years working to create a level
                           playing field for college students of all backgrounds, fashioning a college where
                           every student can succeed at historically higher levels. We did this by focusing internally:
                           by zeroing in on the landmines that face students after they enter our doors; by collaborating
                           to create innovative teaching strategies; by creating an environment on campus that
                           provides students with the learning support they need to succeed; and by building
                           clear pathways that connect students to their purposes.
                        </p>
                        
                        
                        <p>We have been successful. While we still have more to achieve, we have earned national
                           acclaim as the for our students’ graduation rates, for their success in transfer,
                           for career placement and the wages that our A.S. grads earn, and for closing the gaps
                           in achievement among students of different backgrounds. This is precisely why Valencia
                           won the first Aspen Prize for Community College Excellence in 2011. Further, we have
                           received national acclaim for DirectConnect to UCF and its impact on successful transfers
                           and the awarding of A.A. Degrees, as well as for our laser focus on student outcomes,
                           and for many innovations in teaching and learning. Today, these programs are models
                           that colleges and universities around the nation are trying to duplicate in their
                           communities.
                        </p>
                        
                        
                        <p>The focus on these student outcomes, on learning, will continue to be central to our
                           work. But as Valencia has earned the attention and acclaim of the nation and our community,
                           we have discovered new opportunities, in fact, new obligations to serve. Our community
                           needs us and we can have an impact beyond anything we have yet imagined if we focus
                           the same energy and imagination on making an impact on the quality of life in our
                           community—raising educational attainment levels, increasing economic opportunity by
                           growing the economy, and helping our community to flourish.
                        </p>
                        
                        
                        <p>As we focus on the next five years, let us seize the chance to show the nation how
                           we can transform Central Florida to a region recognized as an engine of opportunity.
                           I urge you to join me in this exciting new chapter in Valencia’s history, as we demonstrate
                           our heart and our character—and the change we can achieve.
                        </p>
                        
                        
                        <p>Sincerely,</p>
                        
                        
                        <p>Dr. Sanford C. Shugart</p>
                        
                        <p>President</p>
                        
                        <p>Valencia College</p>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <p>Strategic Planning Process Steering Committee</p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Susan Ledlow</strong></p>
                                    
                                    <p>Sponsor</p>
                                    
                                    <p>VP, Academic Affairs &amp; Planning </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Karen M. Borglum</strong></p>
                                    
                                    <p>Facilitator</p>
                                    
                                    <p>AVP, Curriculum &amp; Assessment</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Noelia Maldonado Rodriguez</strong></p>
                                    
                                    <p>Co-Facilitator</p>
                                    
                                    <p>Coordinator, Institutional Advancement </p>      
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>John Slot</strong><br>
                                       Vice President and CIO<br>
                                       Office of Information Technology
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Daryl Davis</strong></p>
                                    
                                    <p>Dir, Institutional Research</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Deidre Holmes DuBois </strong></p>
                                    
                                    <p>Past Faculty President, Professor of Speech</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Jeff Goltz</strong></p>
                                    
                                    <p>Executive Dean, School of Public Safety</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>John Niss</strong></p>
                                    
                                    <p>Professor of Mathematics</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Michelle Foster</strong></p>
                                    
                                    <p>Campus Dean, East Campus</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Mike Bosley</strong></p>
                                    
                                    <p>Executive Dean, Lake Nona Campus</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Suzette Dohany</strong></p>
                                    
                                    <p>Faculty President, Professor of Speech</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Chara Young</strong></p>
                                    
                                    <p>Dir, Organizational Communications</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Linda Herlocker</strong></p>
                                    
                                    <p>Dean of Students, West Campus</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Nicholas Bekas</strong></p>
                                    
                                    <p>Campus Dean, West Campus</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <strong>Laura Blasi</strong><br>
                                    Director of Institutional Assessment
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/index.pcf">©</a>
      </div>
   </body>
</html>