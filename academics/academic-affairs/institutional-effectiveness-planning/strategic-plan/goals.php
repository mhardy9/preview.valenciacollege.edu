<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/goals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>Mission</strong></h2>
                        
                        <blockquote>
                           
                           <p>Valencia provides opportunities for academic, technical and life-long learning in
                              a collaborative culture dedicated to inquiry, results and excellence.
                           </p>
                           
                        </blockquote>            
                        
                        <h2><strong>Vision</strong></h2>
                        
                        <blockquote>
                           
                           <p>Valencia is a premier learning college that transforms lives, strengthens community,
                              and inspires individuals to excellence.
                           </p>
                           
                        </blockquote>            
                        
                        <h2><strong>Values</strong></h2>
                        
                        <ul>
                           
                           <li>
                              <strong>Learning</strong> by committing to Valencia’s core competencies—Think, Value, Communicate, and Act—and
                              the potential of each person to learn at the highest levels of achievement for personal
                              and professional success.
                           </li>
                           
                           <li>
                              <strong>People</strong> by creating a caring, inclusive, and safe environment that inspires all people to
                              achieve their goals, share their successes, and encourage others.
                           </li>
                           
                           <li>
                              <strong>Diversity</strong> by fostering the understanding it builds in learning relationships and appreciating
                              the dimensions it adds to our quality of life.
                           </li>
                           
                           <li>
                              <strong>Access</strong> by reaching out to our communities, inviting and supporting all learners and partners
                              to achieve their goals.
                           </li>
                           
                           <li>
                              <strong>Integrity</strong> by respecting the ideals of freedom, civic responsibility, academic honesty, personal
                              ethics, and the courage to act.
                           </li>
                           
                        </ul>
                        
                        
                        <h2><strong>Valencia Yesterday and Today</strong></h2>
                        
                        
                        <p>When Valencia Community College opened its doors in 20 portables situated on a muddy
                           parking lot at Mid-Florida Tech in 1967, Orlando was poised at an intersection—on
                           the verge of great change. The city, once a sleepy citrus town, began to buzz with
                           activity. The Martin Company started building rockets for the space race at Kennedy
                           Space Center, and soon dozens of subcontractors set up shop in Central Florida. Walt
                           Disney had only recently revealed that he was planning to build Walt Disney World
                           on the outskirts of Orlando, and the stage was set for a growth trajectory that today’s
                           urban planners could only dream about.
                        </p>
                        
                        
                        <p>Orlando’s mushrooming population—along with growth throughout the state of Florida—created
                           both challenges and opportunities. With large numbers of Baby Boomers prepared to
                           enroll in college, Florida’s state universities could not handle the swelling tide
                           of freshmen and raised admissions standards.
                        </p>
                        
                        
                        <p>Suddenly, many prospective college students couldn’t get in the front door of their
                           state universities. Valencia Community College was one of more than a dozen community
                           colleges formed during that era—yet Valencia was among the last to open because of
                           disagreement in the community over whether all students, regardless of race or ethnicity,
                           should have access to a higher education.
                        </p>
                        
                        
                        <p>Valencia’s founders pushed back and insisted on creating a college that would provide
                           an opportunity to everyone. So when Valencia opened its doors, students from every
                           background walked through them, including those historically overlooked and excluded
                           by traditional higher education. Valencia opened the doors of opportunity then and
                           has continued to do so, through innovations that have made Valencia one of the nation’s
                           leading community colleges.
                        </p>
                        
                        
                        <p>Today, just as in 1967, Valencia stands again on the precipice of generational and
                           societal change. At Valencia, we continue to believe in our students and their ability
                           to change their lives for the better. Now we will turn our attention not just to students
                           on our campuses, but our community at large—and inject in our community the same belief,
                           that with the right guidance and help, we can work with our community to transform
                           Central Florida.
                        </p>
                        
                        
                        <h2><strong>Pillars</strong></h2>
                        
                        
                        <p>In the college’s previous <a href="documents/ValenciaStrategicPlan2008-15Brochure.pdf">Strategic Plan (2008-2015)</a>, we set out four distinct goals: Build Pathways; Learning Assured; Invest in Each
                           Other; and Partner with the Community. The work achieved by Valencia under that plan
                           has laid a solid foundation for the “big, hairy, audacious goals” we want to achieve
                           in the coming five years.
                        </p>
                        
                        
                        <p>To assure learning, we set out to create a campus environment where every student
                           is given an opportunity to learn and succeed with extravagant learning support systems,
                           and where educational practice is rooted in evidence. The college raised the performance
                           of every kind of student and specifically set out to reduce achievement gaps between
                           students from diverse backgrounds.
                        </p>
                        
                        
                        <p>By building pathways, we knew that no matter how much magic happened in the classroom,
                           we needed to make it easier for students to enroll, register and progress through
                           college. And, with tools such as LifeMap, Student Success, and other initiatives,
                           we developed programs and services so that current and future students could develop
                           personal and professional aspirations and find a clear path to realizing those dreams.
                        </p>
                        
                        
                        <p>Since 2008, the college has built out remarkable and robust systems of hiring, induction,
                           and development for faculty and staff. We invest in each other by making strategic
                           investments in our most important resources, our people. In addition, the governance
                           systems have been renewed to assure thoughtful inclusion and collaboration, fostering
                           common purpose, best practice, and a level of trust that ensures open communication—as
                           well as a healthy environment for personal growth.
                        </p>
                        
                        
                        <p>Finally, we set out in 2008 to “Partner with the Community,” by strengthening our
                           relationships with UCF and the public schools to improve educational outcomes at every
                           level, adding to our community of investors by significantly growing the Foundation’s
                           impact, and building new educational programs in response to community need.
                        </p>
                        
                        
                        <p>As we look forward to the next five years, we’ll build on those strengths and focus
                           on forging stronger bonds with the community to transform the region into an engine
                           of economic opportunity, one where not only do we have a thriving creative, business,
                           and scientific community, but a city where people at the bottom of society’s economic
                           ladder can move a few rungs up, and see a way to the middle class.
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/goals.pcf">©</a>
      </div>
   </body>
</html>