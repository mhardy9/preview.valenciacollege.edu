<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-window-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Strategic Planning at Valencia</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<li><a href="../goals.php">Vision, Mission, Values</a></li>
<li><a href="../ValenciaCommitment.php">Valencia's Commitment</a></li>
<li><a href="../Educational-Commitment.php">Educational Commitment</a></li>
<li><a href="../work-teams/Direct-Connect-2.php">DirectConnect 2.0</a></li>
<li><a href="../work-teams/EducationEcosystem.php">Education Ecosystem</a></li>
<li><a href="../work-teams/assessment-for-learning.php">Evidence Based Practice</a></li>
<li><a href="../work-teams/New-Student-Experience.php"> New Student Experience</a></li>
<li><a href="../work-teams/online-learning.php">Online Learning</a></li>
<li><a href="../work-teams/opportunity-equity.php">Opportunity &amp; Equity</a></li>
<li><a href="../work-teams/Part-Time-Faculty-Engagement.php">Part-time Faculty Engagement</a></li>
</ul></div>
<div class="col-md-4"><ul>
<li><a href="../Workforce-Economic-Commitment/default.php">Workforce &amp; Economy Commitment</a></li>
<li><a href="../Workforce-Economic-Commitment/Career-Express.php">Center for Accelerated Training</a></li>
<li><a href="../Workforce-Economic-Commitment/Knowledge-and-Innovation-Economy.php">Knowledge and Innovation Economy</a></li>
<li><a href="../Workforce-Economic-Commitment/Tech-Express.php">Tech Express to Valencia</a></li>
<li><a href="../Workforce-Economic-Commitment/Work-Based-Learning.php">Work-Based Learning</a></li>
<li><a href="../Sustainable-Community-Commitment/default.php">Community Commitment</a></li>
<li><a href="../Sustainable-Community-Commitment/Arts-Health-and-Civic-Engagement.php">Quality of Life: Arts, Health, and Civic Engagement</a></li>
<li><a href="../Sustainable-Community-Commitment/Safety.php">Safety</a></li>
<li><a href="../PlanningSessions.php">Planning Sessions</a></li>
<li><a href="../Archives.php">Archives</a></li>
</ul></div>
<div class="col-md-4"><ul><li><a href="../LearningSymposium.php">Learning Symposium</a></li></ul></div>
</div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
