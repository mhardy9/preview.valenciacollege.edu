<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Archives.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2> Strategic Planning and College Planning Council Archive Webpage </h2>
                        
                        
                        <h3>Welcome to the Strategic Planning Archives area. </h3>
                        
                        <h3></h3>
                        
                        <h3>These webpages contain information on taskforces, workgroups, and initiatives dating
                           back to 2006. Note that all information contained within these pages is for information
                           only. Should you need additional assistance or further information, please contact
                           <a href="../../../contact/Search_Detail2.cfm-ID=nmaldonado5.html">Noelia Maldonado</a> or <a href="../../../contact/Search_Detail2.cfm-ID=sledlow.html">Dr. Susan Ledlow</a>.
                        </h3>
                        
                        <h3></h3>
                        
                        
                        
                        <p><strong> This webpage contains archive information <u>only</u>. </strong></p>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>Big Meeting</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Big Meeting 2007-08</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The College Planning Council  convened a representative group of             around
                                          170 faculty, professional staff, and career service staff on <strong>Friday, March 2, 2007</strong>, to identify strategic issues             and develop goals for the College for 2008-13.
                                          <br>
                                          <br>
                                          At Valencia, we have come to call this type of meeting a "Big Meeting,"          
                                          since it involves a large, representative group in considering significant       
                                          recommendations to the President and the District Board of Trustees.             The
                                          recommended goals will steer the future of the College, and annual             budgets
                                          will reflect those goals. Dr. Shugart  joined us, as did             Mr. Slocum, who
                                          is chair of our District Board of Trustees and a member             of the College
                                          Planning Committee. <br>
                                          <br>
                                          Those invited to this Big Meeting included: 1) members of the four College       
                                          governing councils, 2) the College Planning Committee, 3) the planning           
                                          task forces, and 4) additional staff and faculty             who  expressed strong
                                          interest and/or whose role at the College             lends itself to assisting with
                                          this stage of the planning effort. <br>
                                          <br>
                                          This Big Meeting was very important to the creation of the Strategic Plan,       
                                          and we appreciate the commitment of time, thought and energy of those            
                                          involved. <br>
                                          <br>
                                          In advance of the meeting, the participants were asked to read and think         
                                          about the documents linked below, which include: 
                                       </p>
                                       
                                       <ol>
                                          
                                          <li>Proposed revisions to our statements of Mission, Vision and Values             developed
                                             by a task force of your colleagues. 
                                          </li>
                                          
                                          <li>The information in a "Situational/Needs Analysis", also developed             by a
                                             task force of your colleagues who studied data about the College             and the
                                             community. The Analysis looks at recent and projected trends             and poses
                                             important questions about the College's future.
                                          </li>
                                          
                                          <li>Key data points from which the Situational/Needs Analysis is based.</li>
                                          
                                          <li>The goals in the 2001-07 Strategic Learning Plan, and the most recent           refresh
                                             of that plan.
                                          </li>
                                          
                                          <li>Information helping to define what we mean by strategies, strategic             issues,
                                             and strategic goals.
                                          </li>
                                          
                                       </ol>
                                       
                                       <p>Questions about the meeting may be directed to Susan Kelley (skelley@valenciacollege.edu).
                                          
                                       </p>
                                       
                                       
                                       <h3>Big Meeting Documents</h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCDataAnalysisConclusionsforLearningDay.pdf">Seven             Key Points That Will Shape Valencia's Future</a></li>
                                          
                                          <li><a href="documents/vvm.pdf">Vision,             Value, Mission</a></li>
                                          
                                          <li><a href="BIG/documents/CPCDataAnalysisKeyPointsBigMeeting030207.pdf">Summary             of Key Points from Data Analyses</a></li>
                                          
                                          <li><a href="BIG/documents/CPCSituationalAnalysisBigMeeting030207.pdf">Situational             Needs Analysis</a></li>
                                          
                                          <li><a href="documents/CPCDefinitionofTermsforBigMeeting.pdf">Definition             of Terms for Big Meeting</a></li>
                                          
                                          <li><a href="documents/CPCBigMeetingDraftAgendaFeb22tableversion.pdf">Big             Meeting Agend</a></li>
                                          
                                          <li><a href="documents/BigMeetingGoalsFlipChartpagesMarch22007.pdf"> Big             Meeting Goals Sessions</a></li>
                                          
                                          <li><a href="documents/BigMeetingStrategicIssuesFlipChartpagesMarch22007.pdf">Big             Meeting Strategic Issues</a></li>
                                          
                                          <li><a href="documents/BigMeetingTopStrategicIssuesFirstDraft.pdf">Top             Strategic Issues Named - Big Meeting Plenary Discussion </a></li>
                                          
                                          <li><a href="documents/BigMeetingSWOTInternalQuestionsMarch22006.pdf"> SWOT             Analysis Internal Questions</a></li>
                                          
                                          <li><a href="documents/BigMeetingSWOTExternalQuestionsMarch22007.pdf"> SWOT             Analysis External Question</a></li>
                                          
                                          <li><a href="documents/VisionValuesMissionDraftPresentationVersion.pdf"> Vision,             Values and Mission Draft Presentation Versio</a></li>
                                          
                                          <li><a href="documents/VisionValuesMissionDraftComparisonChart.pdf"> Vision,           Values and Mission Draft Comparison Chart</a></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Big Meeting 2009-10</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The College Planning Council, working in collaboration with the Office of Institutional
                                          Assessment, planned a Big Meeting for March 19, 2010. The Big Meeting is an important
                                          component both of the College’s Institutional Effectiveness effort and of the annual
                                          evaluation of
                                          progress toward the goals and objectives in our Strategic Plan.
                                       </p>
                                       
                                       <p>At the  2010 Big Meeting, college leaders from the faculty, professional staff, career
                                          service staff, and administration were invited by the President to consider the baseline
                                          data
                                          gathered for use in evaluating the Strategic Plan for 2008-13, and review the plans
                                          developed by
                                          the various divisions and departments of the college that are aimed at achieving the
                                          strategic
                                          goals and objectives.
                                       </p>
                                       
                                       <p>The Big Meeting provided the opportunity to identify and make recommendations to the
                                          President about any missing pieces of the work to be done to achieve the strategic
                                          goals and
                                          objectives, to make recommendations regarding how the work might best be sequenced
                                          and
                                          coordinated, and to recommend any changes the group may deem advisable to the 2008-13
                                          strategic goals and objectives and/or the evaluation plan.
                                       </p>
                                       
                                       <p>The College Planning Council  provided a report to the President and the District
                                          Board of
                                          Trustees after the Big Meeting, updating the Trustees on progress made during the
                                          2009-10 year.
                                       </p>
                                       
                                       <p>Four strategic goal teams prepared recommendations for consideration at the Big Meeting
                                          during November 2009 - March 2010. The charge to the Goal Teams is reproduced below.
                                          
                                       </p>
                                       
                                       
                                       
                                       <h3>Big Meeting Work Products </h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BigMeeting2010NOTESGoalsPlenarySessionApril122010forCPC.docx">Big Meeting 2010 NOTES Goals Plenary Session April 12 2010 for CPC</a></li>
                                          
                                          <li><a href="documents/BigMeetingStrategicIssuesTableDiscussionNotes.docx">Big Meeting Strategic Issues Table Discussion Notes</a></li>
                                          
                                          <li><a href="documents/BigMeetingGoals1and2TableDiscussionNotes.docx">Big Meeting Goals 1 and 2 Table Discussion Notes</a></li>
                                          
                                          <li><a href="documents/BigMeetingGoals3and4TableDiscussionNotes.docx">Big Meeting Goals 3 and 4 Table Discussion Notes</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>Big Meeting Agenda for March 19, 2010</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCBigMeetingAgendaMarch192010FINAL.doc">CPC Big Meeting Agenda March 19 2010 FINAL</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>President Shugart's Goal Essays </h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalOneEssayOct2009_000.docx">Goal One Essay: Build Pathways</a></li>
                                          
                                          <li><a href="documents/Goal2EssayLearningAssuredMarch14FINAL_000.docx">Goal Two Essay: Learning Assured</a></li>
                                          
                                          <li><a href="documents/Goal3EssayInvestinEachOtherMarch14_000.docx">Goal Three Essay: Invest in Each Other </a></li>
                                          
                                          <li><a href="documents/GoalFourEssayMarch152010_000.docx">Goal Four Essay: Partner with the Community </a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Goal Team Reports on Issues and Initiatives</h3>
                                       
                                       <ul>
                                          
                                          <li> <a href="documents/CPCGoalTeamStrategicIssueReportsMergedMarch14.docx">Strategic Issues - Combined Goal Team Report</a>
                                             
                                          </li>
                                          
                                          <li><a href="documents/CPCGoalTeamOneInitiativeReportsMarch14.docx">CPC Goal Team One Initiative Reports</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeamTwoInitiativeReportsMarch14.docx">CPC Goal Team Two Initiatives Reports</a></li>
                                          
                                          <li><a href="documents/Goal3TeamInitiativereportscombinedMarch15.docx">CPC Goal Team Three Initiatives Reports</a></li>
                                          
                                          <li><a href="documents/Goal4TeamInitiativereportscombinedMarch18.docx">CPC Goal Team Four Initiatives Reports</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Goal Team Recommendations on Outcomes and Measures </h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam1RecommendationsforOutcomesandMeasuresMarch52010.doc">CPC Goal Team 1 Recommendations for Outcomes and Measures March 5 2010</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam2RecommendationsforOutcomesandMeasuresMarch52010.doc">CPC Goal Team 2 Recommendations for Outcomes and Measures March 5 2010</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam3RecommendationsforOutcomesandMeasuresMarch5.doc">CPC Goal Team 3 Recommendations for Outcomes and Measures March 5</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam4RecommendationsforOutcomesandMeasuresMarch5.doc">CPC Goal Team 4 Recommendations for Outcomes and Measures March 5</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Supplementary Report Materials</h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/ArceneuxStrategicBrief13Staffing11FEB18.xlsx">Arceneux Strategic Brief 13 Staffing 11FEB18</a></li>
                                          
                                          <li><a href="documents/BekasSLDataEastspring2010.xls">Bekas SL Data East spring 2010</a></li>
                                          
                                          <li><a href="documents/BekasSLDataOsceolaSpring2010.xls">Bekas SL Data Osceola Spring 2010</a></li>
                                          
                                          <li><a href="documents/BekasSLDataWestSpring2010.pdf">Bekas SL Data West Spring 2010</a></li>
                                          
                                          <li><a href="documents/BekasSLDataWinterParkSpring2010.xls">Bekas SL Data Winter Park Spring 2010</a></li>
                                          
                                          <li><a href="documents/CornettEnterpriseResourcePlanning.pptx">Cornett Enterprise Resource Planning</a></li>
                                          
                                          <li><a href="documents/CornettGradRatesbyEthnicity20100216.xlsx">Cornett Grad Rates by Ethnicity 20100216</a></li>
                                          
                                          <li><a href="documents/CornettGradRatesbyFAFSAIncome20100216.xlsx">Cornett Grad Rates by FAFSA Income 20100216</a></li>
                                          
                                          <li><a href="documents/CornettFLCollegeGraddata.xlsx">Cornett FL College Grad data</a></li>
                                          
                                          <li><a href="documents/CornettFull-TimeTrends.pdf">Cornett Full-Time Trends</a></li>
                                          
                                          <li><a href="documents/EwenFoEFinalReport-ApprovedbyCLC5-7-09.doc">Ewen FoE Final Report - Approved by CLC 5-7-09</a></li>
                                          
                                          <li><a href="documents/FiorentinoGoal4NamechangeSWOTReplys.docx">Fiorentino Goal 4 Name change SWOT Replys</a></li>
                                          
                                          <li><a href="documents/FLPreK-20StrategicPlanApproved.pdf">FL PreK-20 Strategic Plan Approved</a></li>
                                          
                                          <li><a href="documents/GoltzGoalTeam1LakeNonaCampusUpdate.docx">Goltz Goal Team 1 Lake Nona Campus Update</a></li>
                                          
                                          <li><a href="documents/GoltzGoalTeam1NewCampusesUpdate.docx">Goltz Goal Team 1 New Campuses Update</a></li>
                                          
                                          <li><a href="documents/GoltzGoalTeam1TwoforAllUpdate.docx">Goltz Goal Team 1 Two for All Update</a></li>
                                          
                                          <li><a href="documents/GoltzLakeNonaCampusUpdate.docx">Goltz Lake Nona Campus Update</a></li>
                                          
                                          <li><a href="documents/IPEDS2009DataReport.pdf">IPEDS 2009 Data Report</a></li>
                                          
                                          <li><a href="documents/IRKeyDataAnalysisSummary092409.pdf">IR Key Data Analysis Summary 092409</a></li>
                                          
                                          <li><a href="documents/KosloskiGoal3.3info.pptx">Kosloski Goal 3.3 info</a></li>
                                          
                                          <li><a href="documents/KosloskiGoal3.3Wellnessinfo.pdf">Kosloski Goal 3.3 Wellness info</a></li>
                                          
                                          <li><a href="documents/LookGoldStndArticPhase3Totals1.pdf">Look Gold Stnd Artic Phase 3 Totals1</a></li>
                                          
                                          <li><a href="documents/MatosCintronOnline_Hybrid_COursetrends.xlsx">Matos Cintron Online_Hybrid_COurse trends</a></li>
                                          
                                          <li><a href="documents/McKillopGoalTeam1StrategicIssuesReportColor.pdf">McKilllops Strategic Issue Report</a></li>
                                          
                                          <li><a href="documents/MoralesCrisisIntervention.doc">Morales Crisis Intervention</a></li>
                                          
                                          <li><a href="documents/MoralesFinancialLearning.ppt">Morales Financial Learning</a></li>
                                          
                                          <li><a href="documents/MoralesReadinessTestingSB1908.docx">Morales Readiness Testing SB 1908</a></li>
                                          
                                          <li><a href="documents/MoralesRecruitmentplanCollegewide.doc">Morales Recruitment plan Collegewide</a></li>
                                          
                                          <li><a href="documents/MoralesServicesandAssistanceforStudents.doc">Morales Services &amp; Assistance for Students</a></li>
                                          
                                          <li><a href="documents/MoralesSkillshopSpring2010.pdf">Morales Skillshop Spring 2010</a></li>
                                          
                                          <li><a href="documents/MoralesTransitionsPrograms.pdf">Morales Transitions Programs</a></li>
                                          
                                          <li><a href="documents/ReedIssue9report.doc">Reed Issue 9 report</a></li>
                                          
                                          <li><a href="documents/RobinsonGoal2Initiative9Fall2009actionplanschart1.doc">Robinson Goal 2 Initiative 9 Fall 2009 action plans chart1</a></li>
                                          
                                          <li><a href="documents/RobinsonGoal2Initiative9Mirror201020UtilizationFullModelFebruary2.xlsx">Robinson Goal 2 Initiative 9 Mirror 201020 Utilization Full Model February 2</a></li>
                                          
                                          <li><a href="documents/RodriguezReviewofStrategicPlanGoalTeam1.pdf">Rodriguez Review of Strategic Plan Goal Team 1</a></li>
                                          
                                          <li><a href="documents/RomanoDirectConnectEnrollmentReportDraft22010.xlsx">Romano Direct Connect Enrollment Report Draft 22010</a></li>
                                          
                                          <li><a href="documents/RomanoValenciaandUCFFeedbackReportforFTIC2Feb2010.docx">Romano Valencia and UCF Feedback Report for FTIC2 Feb 2010</a></li>
                                          
                                          <li><a href="documents/RomanoValenciaandUCFFeedbackReportforTransfersFeb2010.docx">Romano Valencia and UCF Feedback Report for Transfers Feb 2010</a></li>
                                          
                                          <li><a href="documents/WEAVE2008-09GoalsandInitiativesReportMarch52010.pdf">WEAVE 2008-09 Goals and Initiatives Report March 5 2010</a></li>
                                          
                                          <li><a href="documents/WEAVE2009-10GoalsandInitiativesReportMarch52010.pdf">WEAVE 2009-10 Goals and Initiatives Report March 5 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Big Meeting 2010-11</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p> <strong>Big Meeting</strong> is an important component both of the College’s Institutional Effectiveness effort
                                          and of the annual evaluation of progress toward the goals and objectives in our Strategic
                                          Plan. The meeting provides the opportunity to identify and make recommendations to
                                          the President about any missing pieces of the work to be done to achieve the strategic
                                          goals and objectives, to make recommendations regarding how the work might best be
                                          sequenced and coordinated, and to recommend any changes the group may deem advisable
                                          to the 2008-13 strategic goals and objectives and/or the evaluation plan. 
                                       </p>
                                       
                                       <p>The College Planning Council, working in collaboration with the Office of Institutional
                                          Assessment, have planned a Big Meeting for June 2, 2011.
                                       </p>
                                       
                                       <p>If you you have any questions regarding Big Meeting, please contact the College Planning
                                          Council co-chairs, <a href="../../../contact/Search_Detail2.cfm-ID=920.html">Joan Tiller</a> or <a href="../../../contact/Search_Detail2.cfm-ID=1485.html">Jean Marie Führman</a></p>
                                       
                                       <p>.</p>
                                       
                                       
                                       <h3>Big Meeting Agenda &amp; Materials</h3>
                                       
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BigMeeting2011AgendaOnePage.doc">Big Meeting 2011 Agenda </a></li>
                                          
                                          <li>
                                             
                                             <p><a href="documents/CPCGoalReportsSummariesCombined09-10Jan2011.docx">CPC Goal Reports Summaries Combined 09-10</a> - All Four Goal Teams
                                             </p>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <p><a href="documents/CPCGoalTeamStrategicIssuesReportsMerged2010-11May31.docx">Combined Strategic Issues Reports</a> - All Four Goal Teams 2009-10 with 2010-11 updates
                                             </p>
                                             
                                          </li>
                                          
                                          <li><a href="documents/BigMeeting2011MorningSessionTableNotesGoals1and2.docx">Big Meeting 2011 Morning Session Table Notes Goals 1 and 2</a></li>
                                          
                                          
                                       </ul>
                                       
                                       <h3> Goal Team Reports and Materials</h3>
                                       
                                       
                                       <p><strong>Goal 1</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p> <span><a href="documents/GoalTeam12011ReportMay25Draft.docx">2010-11 Team 1 Report</a></span></p>
                                          
                                          <p><a href="documents/GoalTeam1InitiativesReportJune120111.docx"><strong>2010-11 Team 1 Initiatives Report </strong></a></p>
                                          
                                          <p><strong><a href="documents/CPCGoalTeam1Report2009-10.docx">2009-10 Team 1 Report</a></strong></p>
                                          
                                          <p><span><a href="documents/CPCGoalOneEssayOct2009_000.docx">Goal One Essay: Build Pathways</a></span></p>
                                          
                                          <p><strong>2010-11 Team 1 Supplementary Report </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/StrategicIndicatorsReport20100825-SupplementaryTeam1.pdf">Strategic Indicators Report</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-IDHNotes1.pdf">Workgroup Notes - IDH Notes 1</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-JeffersonianNotes2.pdf">Workgroup Notes - Jeffersonian Notes 2</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-AdmissionsandScholarshipNotes3.pdf">Workgroup Notes - Admissions &amp; Scholarship Notes 3</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-LeadershipNotes4.pdf">Workgroup Notes - Leadership Notes 4</a></li>
                                             
                                             <li> <a href="documents/FinancialAidSymposiumTeam1.pptx">Financial Aid Symposium </a>
                                                
                                             </li>
                                             
                                             <li><a href="documents/Recruitment_Territories-TransitionsTeam1.pdf">Recruitment Territories - Transitions</a></li>
                                             
                                             <li><a href="documents/BridgesToSuccessWhitePaperTeam1.docx">Bridges To Success White</a></li>
                                             
                                             <li><a href="documents/Bridges_Application_WebTeam1.pdf">Bridges Application - Web</a></li>
                                             
                                             <li><a href="documents/CIEInternationalStudentEnrollmentSummary_2011Team1.xlsx">CIE International Student Enrollment Summary 2011</a></li>
                                             
                                             <li><a href="documents/CourseSummarybyDeliveryMode-FinalTeam1.xlsx">Course Summary by Delivery Mode - Final</a></li>
                                             
                                             <li><a href="documents/RecruitmentplanCollegewideTeam1.doc">Recruitment Plan Collegewide</a></li>
                                             
                                             <li><a href="documents/EnrollmentSummary2007-08to2010-2011Team1.xlsx">Enrollment Summary 2007-08 to 2010-2011</a></li>
                                             
                                             <li><a href="documents/IntStudentenrollmentfromIRStatHistoryFall2006-Fall2009Team1.pdf">Int Student enrollment from IR Stat History Fall 2006-Fall 2009</a></li>
                                             
                                             <li><a href="documents/FacultyCouncilForumontheWithdrawalPolicyNotesTeam1.docx">Faculty Council Forum on the Withdrawal Policy Notes</a></li>
                                             
                                             <li><a href="documents/FYI2011-02StudentFinancialAidTeam1.pdf">FYI 2011-02 Student Financial Aid </a></li>
                                             
                                             <li><a href="documents/2011BSRadiologicandImagingBrochureFinalTeam1.pdf">2011 BS Radiologic &amp; Imaging Brochure Final</a></li>
                                             
                                             <li><a href="documents/BS011111-03-ElecCompEng-broV9Team1.pdf">BS011111-03-ElecCompEng-broV9</a></li>
                                             
                                             <li><a href="documents/InitiativeReportTeam1.docx">Initiative Report</a></li>
                                             
                                             <li><a href="documents/GeneralEducationPrinandProc4FinalApr3Team1.doc">General Education Prin and Proc 4 Final</a></li>
                                             
                                             <li><a href="documents/edited2010-2011_AA-AS_General_Ed_AnalysisTeam1.xls">Edited 2010-2011_AA-AS_General_Ed_Analysis</a></li>
                                             
                                             <li><a href="documents/GeneralEducationAlignmentRecommendationsTeam1.doc">General Education Alignment Recommendations</a></li>
                                             
                                             <li><a href="documents/GeneralEducationAlignmentFeedbackFormTeam1.docx">General Education Alignment Feedback Form</a></li>
                                             
                                             <li><a href="documents/KBCPCGoalTeam1ReferenceforIssuesInitiativesFeb72011Team1.doc">KB CPC Goal Team 1 Reference for Issues Initiatives Feb 7 2011</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>Team 1 Baseline Data Reports:</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/Baselinedata1.1MarketShare.pdf">Baseline data 1.1 Market Share</a></li>
                                             
                                             <li><a href="documents/BaselineData1.1HighSchools.xlsx">Baseline Data 1.1 High Schools</a></li>
                                             
                                             <li><a href="documents/BaselineData1-1College-going.pptx">Baseline Data 1.1 College-going</a></li>
                                             
                                             <li><a href="documents/BaselineData1-1SeminoleCollege-going.xlsx">Baseline data 1.1 Seminole College-going</a></li>
                                             
                                             <li><a href="documents/BaselineData1.2.pdf">Baseline Data 1.2</a></li>
                                             
                                             <li><a href="documents/BaselineData1-2GatewayGaps.pptx">Baseline Data 1.2 Gateway Gaps</a></li>
                                             
                                             <li><a href="documents/BaselineData1.3withdrawals.xlsx">Baseline Data 1.3 withdrawals</a></li>
                                             
                                             <li><a href="documents/BaselineData1.3Completion.docx">Baseline Data 1.3 Completion</a></li>
                                             
                                             <li><a href="documents/BaselineData1-3GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 1.3 Gateway Gap Model FTIC Students 20110131</a></li>
                                             
                                             <li><a href="documents/BaselineData1-3GatewayGapAnalysisbyCourse.pptx">Baseline Data 1.3 Gateway Gap Analysis by Course</a></li>
                                             
                                             <li><a href="documents/BaselineData1-3StrategicIndicatorsReport.docx">Baseline Data 1.3 Strategic Indicators Report</a></li>
                                             
                                             <li><a href="documents/BaselineData1-3DFWITop10Courses.pptx">Baseline Data 1.3 DFWI Top 10 Courses</a></li>
                                             
                                             <li><a href="documents/BaselineData1.4EconDev.doc">Baseline Data 1.4 Econ Dev</a></li>
                                             
                                             <li><a href="documents/BaselineData1.5Scholarships.docx">Baseline Data 1.5 Scholarships</a></li>
                                             
                                             <li><a href="documents/Baseline1-5FinancialAid2008.pdf">Baseline Data 1.5 Financial Aid 2008</a></li>
                                             
                                             <li><a href="documents/Baselinedata1-5DirectConnectEnrollment2007-102.xlsx">Baseline data 1-5 Direct Connect Enrollment 2007-10 2</a></li>
                                             
                                             <li><a href="documents/BaselineData1-5AlternativeDelivery.pptx">Baseline Data 1.5 Alternative Delivery</a></li>
                                             
                                             <li><a href="documents/BaselineData1.5Facilities.docx">Baseline Data 1.5 Facilities</a></li>
                                             
                                             <li><a href="documents/BaselineData1-5DirectConnectSTEMdata.pptx">Baseline Data 1.5 Direct Connect STEM data</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       
                                       <hr>
                                       
                                       <p><strong>Goal 2</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p> <span><a href="documents/GoalTeam22011ReportDraftMay31revision.docx">2010-11 Team 2 Report</a></span></p>
                                          
                                          <p><strong><a href="documents/GoalTeam2InitiativesMay252011.doc">2010-11 Team 2 Initiatives Report </a></strong></p>
                                          
                                          <p><strong><a href="documents/CPCGoalTeam2Report2009-10.docx">2009-10 Team 2 Report</a></strong></p>
                                          
                                          <p><strong><a href="documents/Goal2EssayLearningAssuredMarch14FINAL_000.docx">Goal Two Essay: Learning Assured</a></strong></p>
                                          
                                          <p><strong>2010-11 Team 2 Supplementary Report </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/StrategicIndicatorsReport20100825-SupplementaryTeam2.pdf">Strategic Indicators Report</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-IDHNotes1.pdf">Workgroup Notes - IDH Notes 1</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-JeffersonianNotes2.pdf">Workgroup Notes - Jeffersonian Notes 2</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-AdmissionsandScholarshipNotes3.pdf">Workgroup Notes - Admissions &amp; Scholarship Notes 3</a></li>
                                             
                                             <li><a href="documents/WorkgroupNotes-LeadershipNotes4.pdf">Workgroup Notes - Leadership Notes 4</a></li>
                                             
                                             <li><a href="documents/BSECET_LearningOutcomesTeam2.doc">BSECET_ Learning Outcomes</a></li>
                                             
                                             <li><a href="documents/GatewayGapModelFTICStudents20110131Team2.xlsx">Gateway Gap Model FTIC Students</a></li>
                                             
                                             <li><a href="documents/WritingReadiness14APR11Team2.xlsx">Writing Readiness</a></li>
                                             
                                             <li><a href="documents/ExampleofalearningoutcomeTeam2.pptx">Example of a learning outcome</a></li>
                                             
                                             <li><a href="documents/FYI2011-03CommonlyDroppedCoursesTeam2.pdf">FYI 2011-03 Commonly Dropped Courses</a></li>
                                             
                                             <li><a href="documents/Outlines2YearProgramReviewCycleFilteredTeam2.xls">Outlines 2 Year Program Review Cycle Filtered</a></li>
                                             
                                             <li><a href="documents/CLASTeam2.docx">CLAS</a></li>
                                             
                                             <li><a href="documents/PrepWriting_ReadingGapAnalysisbyCourseTeam2.pptx">Prep Writing_Reading Gap Analysis by Course</a></li>
                                             
                                             <li><a href="documents/Prepcompletionin2yearsTeam2.xlsx">Prep completion in 2 years</a></li>
                                             
                                             <li><a href="documents/GatewayGapAnalysisbyCourseTeam2.pptx">Gateway Gap Analysis by Course</a></li>
                                             
                                             <li><a href="documents/15CRGradRatesTeam2.pdf">15 CR Grad Rates</a></li>
                                             
                                             <li><a href="documents/16CPGradRatesTeam2.pdf">16 CP Grad Rates</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>Team 2 Baseline Data Reports:</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/BaselineData2-1and2-2.docx">Baseline Data 2-1 and 2-2</a></li>
                                             
                                             <li><a href="documents/BaselineData2-3.pdf">Baseline Data 2-3</a></li>
                                             
                                             <li><a href="documents/BaselineData2-4.pdf">Baseline Data 2-4</a></li>
                                             
                                             <li><a href="documents/BaselineData2-415CollegeCredits.xlsx">Baseline Data 2-4 15 College Credits</a></li>
                                             
                                             <li><a href="documents/BaselineData2-5GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 2-5 Gateway Gap Model FTIC Students 20110131</a></li>
                                             
                                             <li><a href="documents/BaselineData2-5GatewayGapAnalysisbyCourse.pptx">Baseline Data 2-5 Gateway Gap Analysis by Course</a></li>
                                             
                                             <li><a href="documents/Baseline2-5-StrategicIndicatorsReport.docx">Baseline 2-5 - Strategic Indicators Report</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <hr>
                                       
                                       <p><strong>Goal 3</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p> <span><a href="documents/GoalTeam32011ReportMay26Draft.docx">2010-11 Team 3 Report </a></span></p>
                                          
                                          <p><strong><a href="documents/GoalTeam3InitiativeReports2010-11May262011.docx">2010-11 Team 3 Initiatives Report</a></strong></p>
                                          
                                          <p><strong><a href="documents/CPCGoalTeam3Report2009-10.docx">2009-10 Team 3 Report </a></strong></p>
                                          
                                          <p><strong><a href="documents/Goal3EssayInvestinEachOtherMarch14_000.docx">Goal Three Essay: Invest in Each Other </a></strong></p>
                                          
                                          <p><strong>2010-11 Team 3 Supplementary Report </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/orgdevelopmentsurveycollaborationcrosstabTeam3.pdf">Org Development Survey - Collaboration Cross Tab</a></li>
                                             
                                             <li><a href="documents/GovernanceReviewSteeringCommitteeRosterTeam3.pdf">Governance Review Steering Committee Roster</a></li>
                                             
                                             <li><a href="documents/GovernanceReviewResourcePanelmembershipTeam3.pdf">Governance Review Resource Panel Membership </a></li>
                                             
                                             <li><a href="documents/StrategicIssuesBriefdatamodel3-1Dec2010Team3.docx">Strategic Issues Brief Data Model 3-1 Dec 2010</a></li>
                                             
                                             <li><a href="documents/SharedGovernanceDocumentsTeam3.docx">Shared Governance Documents</a></li>
                                             
                                             <li><a href="documents/CollaborationSupport-ODBaselineEthnography-FinalSurveyReportTeam3.pdf">Collaboration Support - OD Baseline Ethnography-Final Survey Report</a></li>
                                             
                                             <li><a href="documents/CollaborationSupport-p56-59-ODBaselineEthnography-FinalSurveyReportTeam3.pdf">Collaboration Support - p56-59 - OD Baseline Ethnography-Final Survey Report</a></li>
                                             
                                             <li><a href="documents/StrategicCommunicationsPlan2010Team3.doc">Strategic Communications Plan 2010</a></li>
                                             
                                             <li><a href="documents/InitiativeReport-CollaborationTeam3.docx">Initiative Report - Collaboration</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>Team 3 Baseline Data Reports:</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/BaselineData3.2-StaffOrgDevPlan2008-09.pdf">Baseline Data 3.2 - StaffOrg Dev Plan 2008-09</a></li>
                                             
                                             <li><a href="documents/BaselineData3.2-LeadershipValenciaOfferings2008-09.pdf">Baseline Data 3.2 - Leadership Valencia Offerings 2008-09</a></li>
                                             
                                             <li><a href="documents/Baseline3.3-EmployeeSurvey.docx">Baseline 3.3 - Employee Survey</a></li>
                                             
                                             <li><a href="documents/Baseline3.3-CignaWellnessAggregateData.docx">Baseline 3.3 - Cigna Wellness Aggregate Data</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <p><a href="#top">TOP</a></p>
                                       
                                       <hr>
                                       
                                       <p><strong>Goal 4</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p> <span><a href="documents/GoalTeam42011ReportMay25Draft.docx">2010-11 Team 4 Report </a></span></p>
                                          
                                          <p><strong><a href="documents/GoalTeam4InitiativeReportsMay262011.docx">2010-11 Team 4 Initiatives Report </a></strong></p>
                                          
                                          <p><strong><a href="documents/CPCGoalTeam4Report2009-10.docx">2009-10 Team 4 Report </a></strong></p>
                                          
                                          <p><strong><a href="documents/GoalFourEssayMarch152010_000.docx">Goal Four Essay: Partner with the Community</a></strong></p>
                                          
                                          <p><strong>2010-11 Team 4 Supplementary Report </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/VeteransPRFinalTeam4.pdf">Veterans PR Final</a></li>
                                             
                                             <li><a href="documents/PlacementRates-Summaryand5-YearAverage2004-2008Team4.pdf">Placement Rates - Summary &amp; 5-Year Average 2004 - 2008</a></li>
                                             
                                             <li><a href="documents/PostSecEducInst_2007Team4.pdf">PostSecEducInst_2007</a></li>
                                             
                                             <li><a href="documents/SATACTScores_2010Team4.pdf">SATACTScores_2010</a></li>
                                             
                                             <li><a href="documents/StudentCharacteristicsFall2010Team4.pdf">Student Characteristics Fall 2010</a></li>
                                             
                                             <li><a href="documents/StudentCharacteristicsSpring2011Team4.pdf">Student Characteristics Spring 2011</a></li>
                                             
                                             <li><a href="documents/StudentCharacteristicsSummer2010Team4.pdf">Student Characteristics Summer 2010</a></li>
                                             
                                             <li><a href="documents/TECOArticulationTeam4.docx">TECO Articulation</a></li>
                                             
                                             <li><a href="documents/ANNUALEARNINGSOFVALENCIAGRADUATESTeam4.pdf">ANNUAL EARNINGS OF VALENCIA GRADUATES</a></li>
                                             
                                             <li><a href="documents/ASPerformanceSummaryforCompletersMajorsPlacementRates6.30.10Team4.doc">AS Performance Summary for Completers, Majors, Placement Rates 2010</a></li>
                                             
                                             <li><a href="documents/CampusGrowthStatHistoryFTE2009-2010Team4.pdf">Campus Growth Stat History FTE 2009-2010</a></li>
                                             
                                             <li><a href="documents/Completions2009-2010StatHistoryTeam4.pdf">Completions 2009-2010 Stat History</a></li>
                                             
                                             <li><a href="documents/DualEnrollment2009-2010StatHistory20101119Team4.pdf">Dual Enrollment 2009-2010 Stat History</a></li>
                                             
                                             <li><a href="documents/EducAttainbyCounty_2010Team4.pdf">EducAttainbyCounty_2010</a></li>
                                             
                                             <li><a href="documents/FLUnivRanked_2009Team4.pdf">FLUnivRanked_2009</a></li>
                                             
                                             <li><a href="documents/GraduatesCompleters-2005-2009-105-YearAverageTeam4.xlsx">Graduates Completers-2005-2009-10 5-Year Average</a></li>
                                             
                                             <li><a href="documents/K12StatsbyCounty_2010Team4.pdf">K12StatsbyCounty_2010</a></li>
                                             
                                             <li><a href="documents/EAP_SuccessDataTeam4.pdf">EAP_Success Data</a></li>
                                             
                                             <li> </li>
                                             
                                          </ul>
                                          
                                          <p><strong>Team 4 Baseline Data Reports:</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/Baselinedata4.1FundsRaised.xls">Baseline data 4.1 Funds Raised</a></li>
                                             
                                             <li><a href="documents/Baselinedata4.2AlumniGiving.docx">Baseline data 4.2 Alumni Giving</a></li>
                                             
                                             <li><a href="documents/BaselineData4.4-TransferStudentSuccessatUCF.pdf">Baseline Data 4.4 - Transfer Student Success at UCF</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <h3>Data Team Reports on External and Internal Changes </h3>
                                       
                                       <p><a href="documents/DataTaskForceTeamCombinedInternalReports2011.docx"><strong>Data Task Force Team Combined Internal Reports 2011</strong></a></p>
                                       
                                       <p><strong>Data files reviewed in 2011:</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/1-EAP_SuccessData2010-Ewen.pdf">1 - EAP_Success Data 2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/3-10YearRevenueAnalysis.pdf">3 - 10 Year Revenue Analysis</a></li>
                                          
                                          <li><a href="documents/3-PieCharts2008-13-Rivera.pdf">3 - PieCharts2008-13 - Rivera</a></li>
                                          
                                          <li><a href="documents/4-EnrollmentSummary2007-08to2010-2011-Romano.pdf">4 - Enrollment Summary 2007-08 to 2010-2011 - Romano</a></li>
                                          
                                          <li><a href="documents/4-IntStudentenrollmentfromIRStatHistoryFall2006-2009-Romano.pdf">4 - Int Student enrollment from IR Stat History Fall 2006-2009 - Romano</a></li>
                                          
                                          <li><a href="documents/4-Intl_Student_Chars_FALL_201110_20SEP2010-Romano.pdf">4 - Intl_Student_Chars_FALL_201110_20SEP2010 - Romano</a></li>
                                          
                                          <li><a href="documents/4-Intl_Student_Chars_SPRING_201120_19JAN2011-Romano.pdf">4 - Intl_Student_Chars_SPRING_201120_19JAN2011 - Romano</a></li>
                                          
                                          <li><a href="documents/4-Intl_Student_Chars_SUMMER_201030_17SEP2010-Romano.pdf">4 - Intl_Student_Chars_SUMMER_201030_17SEP2010 - Romano</a></li>
                                          
                                          <li><a href="documents/5-SummaryofArticulationTable2011-LJones.pdf">5 - Summary of Articulation Table 2011 - L Jones</a></li>
                                          
                                          <li><a href="documents/5-SummaryofArticulationWebListing2011-Online.pdf">5 - Summary of Articulation Web Listing 2011 - Online</a></li>
                                          
                                          <li><a href="documents/6-DualEnrollment2009-2010StatHistory20101119-Ewen.pdf">6 - Dual Enrollment 2009-2010 Stat History 20101119 - Ewen</a></li>
                                          
                                          <li><a href="documents/6-PlacementRates-Summaryand5-YearAverage2004-2008-Ewen.pdf">6 - Placement Rates - Summary &amp; 5-Year Average 2004 - 2008 - Ewen</a></li>
                                          
                                          <li><a href="documents/8-CampusGrowthStatHistoryFTE209-2010-Ewen.pdf">8 - Campus Growth Stat History FTE 209-2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/9-ANNUALEARNINGSOFVALENCIAGRADUATES2009-Ewen.pdf">9 - ANNUAL EARNINGS OF VALENCIA GRADUATES 2009 - Ewen</a></li>
                                          
                                          <li><a href="documents/9-ASPerformanceSummaryforCompleters-Majors-Placement-Rates2010-Ewen.pdf">9 - AS Performance Summary for Completers-Majors-Placement-Rates 2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/9-GraduatesCompleters-2005-2009-105-YearAverage-Ewen.pdf">9 - Graduates Completers-2005-2009-10 5-Year Average - Ewen</a></li>
                                          
                                          <li><a href="documents/10-Completions2009-2010StatHistory-Ewen.pdf">10 - Completions 2009-2010 Stat History - Ewen</a></li>
                                          
                                          <li><a href="documents/10-PlacementRates-Summaryand5-YearAverage2004-2008-Ewen.pdf">10 - Placement Rates - Summary &amp; 5-Year Average 2004 - 2008 - Ewen</a></li>
                                          
                                          <li><a href="documents/11-StudentCharacteristicsFall2010-Ewen.pdf">11 - Student Characteristics Fall 2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/11-StudentCharacteristicsSpring2011-Ewen.pdf">11 - Student Characteristics Spring 2011 - Ewen</a></li>
                                          
                                          <li><a href="documents/11-StudentCharacteristicsSummer2010-Ewen.pdf">11 - Student Characteristics Summer 2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/12-Dataanalysistaskforce_update040811-Gombash.pdf">12 - Data analysis task force_update 040811 - Gombash</a></li>
                                          
                                          <li><a href="documents/13and14EmployeeDataandEquityReport2009-10-Stone.pdf">13 &amp; 14 Employee Data and Equity Report 2009-10 - Stone</a></li>
                                          
                                          <li><a href="documents/13and14EmployeeDataandEquityReport2010-11-Stone.pdf">13 &amp; 14 Employee Data and Equity Report 2010-11 - Stone</a></li>
                                          
                                          <li><a href="documents/13and14EquityLetter2011-Stone.pdf">13 &amp; 14 Equity Letter 2011 - Stone</a></li>
                                          
                                          <li><a href="documents/15-ProgramAdvisorStatReport201110-Romano.pdf">15 - Program Advisor Stat Report 201110 - Romano</a></li>
                                          
                                          <li><a href="documents/16-AccountabilityDataTrackingYrs2006-2010-Ewen.pdf">16 - Accountability Data Tracking Yrs 2006-2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/18-NationalDataUSDOE2009-Online.pdf">18 - National Data USDOE 2009 - Online</a></li>
                                          
                                          <li><a href="documents/18-NationalDataUSDOE2010-Online.pdf">18 - National Data USDOE 2010 - Online</a></li>
                                          
                                          <li><a href="documents/19-ConstructionCostsData2011-Loiselle.pdf">19 – Construction Costs Data 2011 - Loiselle</a></li>
                                          
                                          <li><a href="documents/20-MyRegionProgressReport2009-Online.pdf">20 - MyRegion Progress Report 2009 - Online</a></li>
                                          
                                          <li><a href="documents/23-EnrollmentDataFor-ProfitChronicleReport052611-Online.pdf">23 - Enrollment Data For-Profit Chronicle Report 052611 - Online</a></li>
                                          
                                          <li><a href="documents/24-MetroOrlandoHouseingTrends2010-Look.pdf">24 - MetroOrlandoHouseingTrends 2010 - Look</a></li>
                                          
                                          <li><a href="documents/24-OrlandoApartmentVacancies2007-10-Look.pdf">24 - OrlandoApartmentVacancies 2007-10 - Look </a></li>
                                          
                                          <li><a href="documents/24-OrlandoKissimeeHousingComparedToUSAverage2008-10-Look.pdf">24 - OrlandoKissimeeHousingComparedToUSAverage 2008-10 - Look</a></li>
                                          
                                          <li><a href="documents/25-SJRWMDTechFactSheet2008-Online.pdf">25 - SJRWMD Tech Fact Sheet 2008 - Online</a></li>
                                          
                                          <li><a href="documents/25-SJRWMDTechFactSheet2009-Online.pdf">25 - SJRWMD Tech Fact Sheet 2009 - Online</a></li>
                                          
                                          <li><a href="documents/25-SJRWMDTechFactSheetPt12009-Online.pdf">25 - SJRWMD Tech Fact Sheet Pt 1 2009 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCAverageCommuteTimetoWork2000-08-Online.pdf">26 - Orlando EDC Average Commute Time to Work 2000-08 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCAverageSalarybyIndustry2010-Online.pdf">26 - Orlando EDC Average Salary by Industry 2010 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCAverageSalarybyOccupation2009-Online.pdf">26 - Orlando EDC Average Salary by Occupation 2009 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCLaborForceStatisticsMSAFLUS2011-Online.pdf">26 - Orlando EDC Labor Force Statistics MSA FL US 2011 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCLaborForceStatsbyCounty2011-Online.pdf">26 - Orlando EDC Labor Force Stats by County 2011 - Online</a></li>
                                          
                                          <li><a href="documents/26-OrlandoEDCLaborForcebyMSAFLUSA_2011-Online.pdf">26 - Orlando EDC LaborForcebyMSAFLUSA_2011 - Online</a></li>
                                          
                                          <li><a href="documents/29-EducAttainbyCounty_2010-Ewen.pdf">29 - EducAttainbyCounty_2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/29-FLUnivRanked_2009-Ewen.pdf">29 - FLUnivRanked_2009 - Ewen</a></li>
                                          
                                          <li><a href="documents/29-K12StatsbyCounty_2010-Ewen.pdf">29 - K12StatsbyCounty_2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/29-PostSecEducInst_2007-Ewen.pdf">29 - PostSecEducInst_2007 - Ewen</a></li>
                                          
                                          <li><a href="documents/29-SATACTScores_2010-Ewen.pdf">29 - SATACTScores_2010 - Ewen</a></li>
                                          
                                          <li><a href="documents/29-UCFMOEDCDegreesConferredEngineering2009-10-Online.pdf">29 - UCF MOEDC Degrees Conferred Engineering 2009-10 - Online</a></li>
                                          
                                          <li><a href="documents/30-FloridaHighTechIndustry2011-Online.pdf">30 - Florida High Tech Industry 2011 - Online</a></li>
                                          
                                          <li><a href="documents/30-FloridaHighTechReport2007.pdf">30 - Florida High Tech Report 2007</a></li>
                                          
                                          <li><a href="documents/30-FloridaHighTechSectorCorridorbytheNumbers2011-Online.pdf">30 - Florida High Tech Sector Corridor by the Numbers 2011 - Online</a></li>
                                          
                                          <li><a href="documents/30-FloridaHighTechSectorStatsandIndOrg2011.pdf">30 - Florida High Tech Sector Stats and Ind Org 2011</a></li>
                                          
                                          <li><a href="documents/30-FloridaHighTechWorkforce2011-Online.pdf">30 - Florida High Tech Workforce 2011 - Online</a></li>
                                          
                                          <li><a href="documents/31-NationalEmploymentData2008-11-Online.pdf">31 - National Employment Data 2008-11 - Online</a></li>
                                          
                                          <li><a href="documents/31-NationalEmploymentHouseholdDataAnnualAverages2010-Online.pdf">31 - National Employment Household Data Annual Averages 2010 - Online</a></li>
                                          
                                          <li><a href="documents/31-NationalEmploymentReportApril2011-Online.pdf">31 - National Employment Report April 2011 - Online</a></li>
                                          
                                          <li><a href="documents/31-StateEmploymentData2008-11-Online.pdf">31 - State Employment Data 2008-11 - Online</a></li>
                                          
                                          <li><a href="documents/34-CentralFloridaLeadershipSurvey2008-Online.pdf">34 - Central Florida Leadership Survey 2008 - Online</a></li>
                                          
                                          <li><a href="documents/37-BEBR2008_Population_Estimates-Online.pdf">37 - BEBR 2008_Population_Estimates - Online</a></li>
                                          
                                          <li><a href="documents/37-BEBR2009_Population_Estimates-Online.pdf">37 - BEBR 2009_Population_Estimates - Online</a></li>
                                          
                                          <li><a href="documents/38-CensusDataInternalReport2011.docx">38 - Census Data Internal Report 2011</a></li>
                                          
                                          <li><a href="documents/38-OrangeCountyCensusData2005-09-Online.pdf">38 - Orange County Census Data 2005-09 - Online</a></li>
                                          
                                          <li><a href="documents/38-OrlandoCensusData2005-09-Online.pdf">38 - Orlando Census Data 2005-09 - Online</a></li>
                                          
                                          <li><a href="documents/38-OsceolaCountyCensusData2005-09-Online.pdf">38 - Osceola County Census Data 2005-09 - Online</a></li>
                                          
                                          <li><a href="documents/39-OrangeandOsceolaFCATReport2009-10-Online.pdf">39 - Orange and Osceola FCAT Report 2009-10 - Online</a></li>
                                          
                                          <li><a href="documents/39-OrangeCountyPublicSchoolsAnnualReport2010-Online.pdf">39 - Orange County Public Schools Annual Report 2010 - Online</a></li>
                                          
                                          <li><a href="documents/39-OrangeCountyPublicSchoolsFacts2009-10-Online.pdf">39 - Orange County Public Schools Facts 2009-10 - Online</a></li>
                                          
                                          <li><a href="documents/39-OsceolaDistrictSummaryBudget2010-11-Online.pdf">39 - Osceola District Summary Budget 2010-11 - Online</a></li>
                                          
                                          <li><a href="documents/39-OsceolaSchoolDistrictAt-A-Glance2010-11-Online.pdf">39 - Osceola School District At-A-Glance 2010-11 - Online</a></li>
                                          
                                          <li><a href="documents/39-OsceolaStudentDemographics2010-Online.pdf">39 - Osceola Student Demographics 2010 - Online</a></li>
                                          
                                          <li><a href="documents/42-ARTICFY0910CCSMRYRPT2010-09-30-Andrews.pdf">42 - ARTIC FY0910 CCSMRYRPT 2010-09-30 - Andrews</a></li>
                                          
                                          <li><a href="documents/42-ARTICFY0910LVE1RPTUCF2010-09-30-Andrews.pdf">42 - ARTIC FY0910 LVE1RPT UCF 2010-09-30 - Andrews</a></li>
                                          
                                          <li><a href="documents/42-StratIndicatorsGraduationDatapg71-90-Andrews.pdf">42 - Strat Indicators Graduation Data pg 71-90 - Andrews</a></li>
                                          
                                          <li><a href="documents/42-ValenciaLongRangeTrends2007-10-Andrews.pdf">42 - Valencia Long Range Trends 2007-10 - Andrews</a></li>
                                          
                                          <li><a href="documents/42-StrategicIndicatorsGraphUpdates2010-Andrews.pdf">42 -Strategic Indicators Graph Updates 2010 - Andrews</a></li>
                                          
                                          <li><a href="documents/44-FLDOELong-RangeProgramPlan2009-10to2013-14-Online.pdf">44 - FLDOE Long-Range Program Plan 2009-10 to 2013-14 - Online</a></li>
                                          
                                          <li><a href="documents/44-FloridasNextGenerationPreK-202010-Online.pdf">44 - Floridas Next Generation PreK-20 2010 - Online</a></li>
                                          
                                          <li><a href="documents/45-BuildingAmericanSkillsThroughCommunityColleges-Online.pdf">45 - Building American Skills Through Community Colleges - Online</a></li>
                                          
                                          <li><a href="documents/45-EnsuringThatStudentLonasareAffordable2010-Online.pdf">45 - Ensuring That Student Lonas are Affordable 2010 - Online</a></li>
                                          
                                          <li><a href="documents/45-InvestinginPellGrants2009-Online.pdf">45 - Investing in Pell Grants 2009 - Online</a></li>
                                          
                                          <li><a href="documents/45-MakingCollegeMoreAffordable2009-Online.pdf">45 - Making College More Affordable 2009 - Online</a></li>
                                          
                                          <li><a href="documents/46-2011SessionReport_e-Mullowney.pdf">46 - 2011SessionReport_e - Mullowney</a></li>
                                          
                                          <li><a href="documents/46-FLGovernors-Bill-EducationPortion2011-Online.pdf">46 - FL Governors-Bill - Education Portion 2011 - Online</a></li>
                                          
                                          <li><a href="documents/46-FloridaCollegeSystemFoundationAnnualReport2011-Online.pdf">46 - Florida College System Foundation Annual Report 2011 - Online</a></li>
                                          
                                          <li><a href="documents/46-LegislativeUpdateScholarshipsandGrants2011-Kelley.pdf">46 - Legislative Update Scholarships and Grants 2011 - Kelley</a></li>
                                          
                                          <li><a href="documents/46-NAFSAProfileofProgramsinTitleIV2011-Kelley.pdf">46 - NAFSA Profile of Programs in Title IV 2011 - Kelley</a></li>
                                          
                                          <li><a href="documents/46-Student-Success-Act-SB736-Online.pdf">46 - Student-Success-Act-SB736 - Online</a></li>
                                          
                                          <li><a href="documents/53-CCSSEExecutiveSummary2011.pdf">53 - CCSSE Executive Summary 2011</a></li>
                                          
                                          <li><a href="documents/ARTICULATIONAGREEMENTS.DOCX">ARTICULATION AGREEMENTS</a></li>
                                          
                                          <li><a href="documents/PieCharts2008-11.ppt">PieCharts2008-11</a></li>
                                          
                                          <li><a href="documents/TECOArticulation2010-11-Ewen.pdf">TECO Articulation 2010-11 - Ewen</a></li>
                                          
                                          <li><a href="documents/VCCEquityReportforEmploymentAccountability2010-11.docx">VCC Equity Report for Employment Accountability 2010-11</a></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <h3> 2008-13 Strategic Plan </h3>
                                       
                                       <ul>
                                          
                                          <li> <span><a href="documents/StratPlanBOOKMARKED_000.pdf">Strategic Plan for 2008-13</a></span>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Strategic Goal Teams</h3>
                                       
                                       <p> In 2009-10, the College Planning Council convened a <strong>Strategic Goal Teams</strong> for each of the four goals in the College Strategic Plan: 1) Build Pathways, 2) Learning
                                          Assured, 3) Invest in Each Other, and 4) Partner with the Community. Each goal team
                                          will monitor progress toward a goal, collect and interpret data, monitor related strategic
                                          issues, make recommendations to the annual Big Meeting, and prepare an annual report
                                          on progress toward the goal. 
                                       </p>
                                       
                                       <p>CPC convened the 2010-11 Strategic Goal Teams on October 22, 2010. If you are interested
                                          in participating in a goal team and have not already volunteered, please email <a href="../../../contact/Search_Detail2.cfm-ID=920.html">Joan Tiller</a> or <a href="../../../contact/Search_Detail2.cfm-ID=1485.html">Jean Marie Führman</a>.
                                       </p>
                                       
                                       <p>You can access the complete <strong>Charge to the 2010-11 Goal Teams</strong> <a href="documents/CPCGoalteamsdraftchargeOct12010.docx">here.</a></p>
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Big Meeting 2011-12</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p> <strong>Big Meeting</strong> is an important component both of the College’s Institutional Effectiveness effort
                                          and of the annual evaluation of progress toward the goals and objectives in our Strategic
                                          Plan. The meeting provides the opportunity to identify and make recommendations to
                                          the President about any missing pieces of the work to be done to achieve the strategic
                                          goals and objectives, to make recommendations regarding how the work might best be
                                          sequenced and coordinated, and to recommend any changes the group may deem advisable
                                          to the 2008-13 strategic goals and objectives and/or the evaluation plan. 
                                       </p>
                                       
                                       <p>The College Planning Council, working in collaboration with the Office of Institutional
                                          Assessment, planned a Big Meeting for June 12, 2012.
                                       </p>
                                       
                                       <p>If you you have any questions regarding Big Meeting 2012, please contact the College
                                          Planning Council co-chairs, <a href="../../../contact/Search_Detail2.cfm-ID=920.html">Joan Tiller</a> or <a href="../../../contact/Search_Detail2.cfm-ID=1485.html">Jean Marie Führman</a></p>
                                       
                                       <p>.</p>
                                       
                                       
                                       <h3>Big Meeting Agenda &amp; Materials</h3>
                                       
                                       
                                       <ul>
                                          
                                          <li><a href="BIG/documents/BigMeeting2012Agenda-FinalDraft-6412_000.docx">Big Meeting 2012 Agenda </a></li>
                                          
                                          <li><a href="BIG/documents/NationalConversationaboutHigherEducation.pdf">National Conversation about Higher Education</a></li>
                                          
                                          <li><a href="BIG/documents/Recognition-ChampionAwards-BigMeeting2012.pdf">Recognition – Champion Awards </a></li>
                                          
                                          <li><a href="BIG/documents/BigMeeting2012ValenciaNoteworthyWork.pdf">Valencia Noteworthy Work 2011-12 </a></li>
                                          
                                          <li><a href="BIG/documents/BigMeeting2012-ClarifyingOurFocus-ProgressReportsonGoals.pdf">Strategic Plan - Clarifying our Focus</a></li>
                                          
                                          <li><a href="BIG/documents/BigMeeting2012GoalsObjectivesChanges-FINAL61112.pdf">Strategic Plan - Goals &amp; Objectives Suggested Changes </a></li>
                                          
                                          <li><a href="BIG/documents/StrategicPlanFeedback-EditedVersion081312.pdf">Strategic Plan - Feedback</a></li>
                                          
                                          <li><a href="BIG/documents/EastandWinterParkCampusPlan2012-2013.pdf">East &amp; Winter Park Campus Plan as of June 12, 2012</a></li>
                                          
                                          <li><a href="BIG/documents/OsceolaLakeNonaCampusPlanBigMeeting.pdf">Osceola &amp; Lake Nona Campus Plan as of June 12, 2012</a></li>
                                          
                                          <li><a href="BIG/documents/WestCampusPlan_BIGMeeting_061212FULL.pdf">West Campus Plan as of June 12, 2012</a></li>
                                          
                                          <li><a href="BIG/documents/StrategicPlanFeedback-EditedVersion081312_000.pdf">  Big Meeting 2012 - Working Theories</a></li>
                                          
                                          <li><a href="BIG/documents/BigMeeting2012SurveyResultsNOSTATS6_26_12.pdf">Big Meeting 2012 - Survey Report</a></li>
                                          
                                          <li>
                                             <a href="BIG/documents/BigMeeting2012AttendanceRosterJune132012-FINAL.pdf">Big Meeting 2012 - Attendance Report</a> 
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Task Forces</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Situational/Need Analysis</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Charge to the Data and Situational/Needs Analysis Task Force:</h3>
                                       
                                       <p> The Data and Situational/Needs Analysis Task Force of the College  Planning Council
                                          was charged with carrying out the following activities  and completing the work products
                                          named during the August – November  2006 period: 
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Studying a list of planning questions developed by the College Planning       Council,
                                             and the Senior Executive Staff, and determining relevant qualitative       and quantitative
                                             data that can be identified or generated that will assist       the College in addressing
                                             these questions (August – mid-September)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Securing the data identified and sharing it among the Task Force members       for
                                             discussion, with the aim of developing a narrative description that analyzes     
                                             the College’s and the community’s situation, focusing on needs,       defined as gaps
                                             in results(mid-September – mid-October)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Reporting on progress at the September 28 meeting of the College Planning       Council,
                                             working through the Council member who serves as a liaison from the       Task Force
                                             to the Council (2:30, East 3-113)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Collaborating to write the narrative situational/needs analysis and to present   
                                             the analysis and relevant data on which it is based to the entire College       at
                                             Learning Day, October 31, 2006. (Last two weeks of October)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Sharing a draft with the College Planning Council on October 26, working       through
                                             the liaison (2:30 p.m., Winter Park Campus).
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Assisting in evaluation of the work of the Task Force and documenting the       work
                                             as part of the planning process archives. (November 2006) 
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p>View the Situational Task Force's <a href="minutes.html#situational">meeting agendas and minutes</a>.
                                       </p>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Mission, Vision, Values</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Charge to the Strategic Planning Vision, Values, and Mission Task Force:</h3>
                                       
                                       <p>The Vision, Values, and Mission Task Force of the College Planning  Council was charged
                                          with carrying out the following activities and  completing the work products named
                                          during the August 2006 – February  2007 period:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>designing and conducting a process for the college as a  whole to consider the statements
                                             of vision, values, and mission at  Learning Day, October 31, 2006, in light of the
                                             situational/needs  analysis presented to the College by the Data and Situational/Needs
                                             Analysis Task Force (August – October 2006)
                                          </li>
                                          
                                          <li>receiving and  considering the comments from the College and the community and  drafting
                                             recommended updates to the Vision, Values, and Mission  statements, and sharing those
                                             proposed revisions collegewide, seeking a  high level of agreement on the statements
                                             to be recommended to the  president and the trustees (October 2006 – February 2007)
                                             
                                          </li>
                                          
                                          <li>reporting  on the activities of the Task Force at the regular meetings of the  College
                                             Planning Council, working through the liaison from the Council  who will serve on
                                             the Task Force.<br>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p>View the Mission Task Force's <a href="minutes.html#mission">meeting agendas and minutes</a>.
                                       </p>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Strategies, Goals, Objectives</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Charge to the Strategies, Goals and Objectives Task Force</h3>
                                       
                                       <p>
                                          The Strategies, Goals and        Objectives Task Force reviewed the processes used
                                          to create        the strategies and goals in the current SLP, and proposed a process
                                          to invite        broad input into decisions to be made about strategies, goals and
                                          objectives        for 2008-13.<br>
                                          <br>
                                          The work took place during 2007-2008. Resulting recommendations for new  strategies,
                                          goals, and objectives were made to the Planning Committee,  and the 2008-09 budget
                                          (and the budgets in subsequent years) reflects  the priorities in the plan. 
                                       </p>
                                       
                                       <h3>The Charge to the Strategies, Goals, and Objectives Task Force</h3>
                                       The Strategies, Goals, and Objectives Task Force of the College Planning Council 
                                       was charged with carrying out the following activities and completing      the work
                                       products named during February 2007 - April 2008: 
                                       
                                       <ul>
                                          
                                          <li>Reviewing the process used to develop the strategies and goals in the        current
                                             Strategic Learning Plan as background information to recommend        a collaborative
                                             process to develop those components of the 2008-13 plan.
                                          </li>
                                          
                                          <li>Attending the Big        Meeting on March 2, and based on a review of the discussion
                                             at that meeting        and the planning documents that support the discussion, developing
                                             a draft list of strategic issues and related strategic goals for 2008-13.
                                          </li>
                                          
                                          <li>Sharing        the draft list of strategic issues and goals with the college as a
                                             whole,        and with the College Planning Council, the President’s Staff,      and
                                             the College Planning Committee, for comment prior to finalizing the first    draft
                                             in March/ April 2007.
                                          </li>
                                          
                                          <li>Being available to provide assistance to the governing        councils, if requested
                                             by the councils, during May – October, as        the Councils recommend further refinements
                                             to the goals and strategies        and develop measurable objectives    to implement
                                             the goals.
                                          </li>
                                          
                                          <li>In Fall 2007, developing a chart that depicts the          goals and objectives in
                                             terms of the results levels that each addresses          on the Organizational Elements
                                             Model, demonstrating the integration of the goals and objectives in terms      of
                                             the results each is aimed at achieving.
                                          </li>
                                          
                                          <li>Receiving recommended goal and          objective statements from the governing councils
                                             in October, and suggesting          edits to address overlaps or incongruencies  
                                             among          the goals so that they may be combined into a clear and cohesive Strategic
                                             Plan. 
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>Resources</h3>
                                       <a href="documents/StudentLeaderdraftgoalsrankingAug2007.pdf" target="_self">Student Leader Draft Goals Ranking August 2007 </a><br>                     <a href="documents/StudentleadergoalideasAug2007.pdf" target="_self">Student Leader Goal Ideas August 2007 </a><br>
                                       <a href="BIG/documents/StudentleaderresponsetodraftgoalsAug2007.pdf" target="_self">Student Leader Response to Draft Goals August 2007 </a>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Communication</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Charge to the Strategic Planning Communications Task Force:</h3>
                                       
                                       <p> The Communications Task Force of the College Planning Council is  charged with carrying
                                          out the following activities and completing the  work products named:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>preparing and executing an internal communications plan for the               strategic
                                             planning process that ensures that all Valencia employees               are aware
                                             of the planning process and timeline, its products, and               their opportunities
                                             to be involved(Preparation of the plan - August               -September 2006)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>in consultation with College and Community Relations, the Provosts,              
                                             the President, and the Valencia Foundation, preparing a plan for communicating   
                                             with key stakeholders in the community regarding community needs and             
                                             their opportunities to be involved in the strategic planning process             
                                             (Preparation of the plan - August -October 2006)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>executing the internal communications plan, routinely providing               information
                                             to the college through identified means, including a planning               web site,
                                             notices in the Bulletin, presentations at appropriate council               meetings
                                             and campus-based meetings, and emails (August 2006 -November               2007)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>working with College and Community Relations to execute the communications       
                                             plan with community stakeholders (August 2006-November 2007)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>collaborating with the Evaluation Task Force on periodic evaluation              
                                             of communications throughout the process and in a summative evaluation           
                                             at the conclusion
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>reporting on communications at the regular meetings of the College               Planning
                                             Council working through the liaison from the Council who               will serve
                                             on the Task Force
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>working with College and Community Relations to design and communicate           
                                             plans for a collegewide celebration of the plan as it is launched               in
                                             2008-09. (April 2008 - April 2009)
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p>View the Communication Task Force's <a href="minutes.html#communication">meeting agendas and minutes</a>.
                                       </p>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Evaluation</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Charge to the Strategic Planning Evaluation Task Force:</h3>
                                       
                                       <p>The Evaluation Task Force of the College Planning Council was  charged with carrying
                                          out the following activities and completing the  work products named during the August
                                          2006 – February 2008 period: 
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>preparing and executing a plan for formative and summative  evaluation of the strategic
                                             planning process in terms of the extent to  which the process was carried out in keeping
                                             with its design  principles, and based on the evaluation, making recommendations for
                                             mid-course changes to improve the process and its products and  recommendations for
                                             changes to take place in the planning cycle for  2015-2020 (Preparation of the plan
                                             – August – October 2006)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>preparing and executing a plan for evaluation of each of  the work products of the
                                             planning process, working in collaboration  with the CPC, the Task Forces carrying
                                             out the phases of the process,  and the College Planning Committee (Preparation of
                                             the plan - August –  October 2006)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>preparing a recommended evaluation design and timetable for  the goals and objectives
                                             contained in the final strategic plan for  2008-15 (September 2007 – February 2008)
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>reporting on evaluation plans, activities, and results at  the regular meetings of
                                             the College Planning Council, working through  the liaison from the Council who will
                                             serve on the Task Force.
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p>View the Evaluation Task Force's <a href="minutes.html#evaluation">meeting agendas and minutes</a>.
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Work Products</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Meeting Agendas and Minutes</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Meeting Agendas and Minutes</h3>
                                       
                                       <ul>
                                          
                                          <li><a href="#council">College Planning Council</a></li>
                                          
                                          <li><a href="#committee">College Planning Committee</a></li>
                                          
                                          <li><a href="#situational">Data and Situational / Needs Analysis Task Force</a></li>
                                          
                                          <li><a href="#mission">Mission, Vision, Values Task Force</a></li>
                                          
                                          <li><a href="#communication">Communications Task Force</a></li>
                                          
                                          <li><a href="#evaluation">Evaluation Task Force</a></li>
                                          
                                       </ul>
                                       <a></a>
                                       
                                       
                                       <h3>College Planning Council</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Agenda7-27-06.pdf" target="_parent">July                                    27, 2006</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgenda92806.pdf" target="_parent">September                                    28, 2006</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Agenda10-26-06.pdf" target="_parent">October                                    26, 2006</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Agenda-12507_1.doc" target="_parent">January             25, 2007</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/022207MtgAgenda.pdf" target="_parent">February 22, 2007 </a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/051007Agenda.pdf"> May 10, 2007</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/072607Agenda.pdf"> July 26, 2007</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaSeptember272007revised.pdf"> September 27, 2007 </a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaOctober252007.pdf"> October 25, 2007 </a> 
                                             </li>
                                             
                                             <li><a href="documents/CPCAgendaJanuary2420081.doc"> January 24, 2008 </a></li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaMarch272008.pdf">March 27 2008</a> 
                                             </li>
                                             
                                             <li><a href="documents/CPCAgendaApril242008.pdf">April 24, 2008 </a></li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaNovember202008.pdf">November 20, 2008</a> 
                                             </li>
                                             
                                             
                                             <li><a href="documents/CPCAgendaFebruary262009.doc">February 26, 2009 </a></li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaJuly232009DRAFT_000.doc">July 23, 2009</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPC_Agenda_Sept_24_2009_DRAFT1.doc">September 24, 2009</a> 
                                             </li>
                                             
                                             <li><a href="documents/CPCAgendaNov192009DRAFT.doc">November 19, 2009 </a></li>
                                             
                                             <li>January 2010 - Cancelled </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaFebruary252010.doc">February 25, 2010</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaApril222010.docx">April 22, 2010</a> 
                                             </li>
                                             
                                             <li>July 22, 2010 - Cancelled</li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaSept232010.doc">September 23, 2010</a>
                                                
                                                <ul>
                                                   
                                                   <li><a href="documents/FutureTrendsinEducation.pptx">Future Trends in Education</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>November 2010 - Cancelled</li>
                                             
                                             <li><a href="documents/CPCAgendaJan2720111.doc">January 27, 2011 </a></li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaApril282011.doc">April 28, 2011</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCAgendaJuly282011.docx">July 28, 2011</a> 
                                             </li>
                                             
                                             <li><a href="documents/CPCAgendaSep2220111FINAL.docx">September 22, 2011 </a></li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCAgendaNovember102011REVISED.docx">November 10, 2011</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCMinutesFeb232012DRAFT.docx">February 23, 2012</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCAgenda-Draft-April262012-OsceolaCampus.docx">April 26, 2012</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCAgendaSep272012WestCampus092712.docx">September 27, 2012</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCAgendaOct252012EastCampus100912.docx">October 25, 2012</a> 
                                             </li>
                                             
                                             <li><a href="WP/documents/CPCAgendaNov292012WestCampus.docx">November 29, 2012</a></li>
                                             
                                             <li><a href="WP/documents/CPCAgendaFeb282013WinterParkCampus.docx">February 28, 2013 </a></li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCAgendaMar282013OsceolaCampus.docx">March 28, 2013</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                          <h3>Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Minutes5-25-06.pdf">May 25, 2006</a> 
                                             </li>
                                             
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Minutes9-28-06-1.pdf">September       28, 2006</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="meetingdocs/CPC-Minutes-102606.doc">October 26       , 2006</a> 
                                             </li>
                                             
                                             
                                             <li>
                                                <a href="documents/022207CPCMeetingMinutes.pdf">February 22, 2007 </a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/051007MeetingMinutes.pdf">May 10, 2007</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/JulyMinutes0707.pdf">July 26, 2007 </a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCOctober252007AttachAMinutesofSept272007.doc">September 27, 2007 </a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCJan242007AttachmentAMinutesofOct252007.doc">October 25, 2007 </a> 
                                             </li>
                                             
                                             <li><a href="documents/MinutesofMarch272008.pdf">January 24, 2008</a></li>
                                             
                                             
                                             
                                             <li><a href="documents/CPCAgendaFebruary262009.doc">February 26, 2009</a></li>
                                             
                                             <li><a href="documents/CPC_Sept_2009_Attach_B_Minutes_July_23_2009.docx">July 23, 2009 </a></li>
                                             
                                             <li>
                                                <a href="documents/CPCNov2009AttachAMinutesSept242009.docx">September 24, 2009</a> 
                                             </li>
                                             
                                             <li><a href="documents/CPCFeb2010AttachAMinutesNov192009Draft.docx">November 19, 2009 </a></li>
                                             
                                             <li><a href="documents/CPCMinutesFeb2520101.docx">February 25, 2010</a></li>
                                             
                                             <li><a href="documents/CPCMinutesApril222010.docx"> April 22, 2010</a></li>
                                             
                                             <li>July 22, 2010 - Cancelled</li>
                                             
                                             <li><a href="documents/CPCJanuary2011AttachBminutes.docx">September 23, 2010</a></li>
                                             
                                             <li>November 2010 - Cancelled </li>
                                             
                                             <li>
                                                <a href="documents/CPCJanuary2011AttachBminutes.docx">January 27, 2011</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCApril2011Minutes-Approved.docx">April 28, 2011</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/CPCJuly282011MeetingMinutesDRAFTAug82011.docx">July 28, 2011</a> 
                                             </li>
                                             
                                             
                                             <li>
                                                <a href="WP/documents/CPCNov102011MinutesDRAFT.docx">November 10, 2011</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCMinutesFeb232012.docx">February 23, 2012</a> 
                                             </li>
                                             
                                             <li><a href="WP/documents/CPCMinutesApr262012-DRAFT092612.docx">April 26, 2012 </a></li>
                                             
                                             <li><a href="WP/documents/CPCMinutesNov292012.docx">November 29, 2012</a></li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCMinutesFeb282013.docx">February 28, 2013
                                                   </a>
                                                
                                                <ul>
                                                   
                                                   <li>Learning Activity: <a href="http://prezi.com/umny7fydfd8t/media-tools-2013/">Media Tools 2013 by Rob McCaffrey</a>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="WP/documents/CPCMinutesMar282013OsceolaCampus.docx">March 28, 2013 
                                                   </a>
                                                
                                                <ul>
                                                   
                                                   <li>Learning Activity: <a href="WP/documents/LearningActivity-TransformingtheStudentExperience.pdf">Transforming the Student Experience</a>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       <h3>Budget &amp; Financial Advisory Group</h3>
                                       
                                       <h3> Meeting Agendas </h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BFAGMeetingAgendaSept282010.docx">September 28, 2010 </a></li>
                                          
                                          <li>October 25, 2010 </li>
                                          
                                       </ul>                  
                                       
                                       
                                       <h3>Minutes</h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BFAGMeetingMinutesSept282010.docx">September 28, 2010 </a></li>
                                          
                                          <li><a href="documents/BFAGMeetingMinutesOct252010.docx">October 25, 2010 </a></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       
                                       <h3>College Planning Committee</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/092906Agenda.pdf"> September 29, 2006 </a></li>
                                             
                                             <li><a href="documents/CPCagendno2112906.pdf"> November 29, 2006 </a></li>
                                             
                                             <li><a href="documents/032607MtgAgenda.pdf"> March 26, 2007</a></li>
                                             
                                             <li><a href="documents/042707Agenda.pdf">April 27, 2007 </a></li>
                                             
                                             <li><a href="documents/CPCommitteeAgendaApril72008revised.pdf">April 7, 2008 </a></li>
                                             
                                          </ul>
                                          
                                          <h3>Meeting Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/092906MeetingMinutes.pdf">September 29, 2006 </a></li>
                                             
                                             <li><a href="documents/112906MtgMinutes.pdf"> November 29, 2006</a></li>
                                             
                                             <li><a href="documents/032607Minutes.pdf"> March 26, 2007 </a></li>
                                             
                                             <li><a href="documents/042707MtgMinutes.pdf"> April 27, 2007 </a></li>
                                             
                                             <li><a href="documents/040708CommitteeMtgMinutes.pdf"> April 7, 2008</a></li>
                                             
                                             <li><a href="documents/CPCommitteMinutesSeptember42007.pdf"> September 4, 2007</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <a></a>
                                       
                                       
                                       <h3>Data and Situational / Needs Analysis Task Force</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda9-15-06.pdf">September     15, 2006</a></li>
                                             
                                             <li><a href="documents/DataAnalysisTaskForceAgendaOct62006.pdf">October 6, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda10-20-2006-1.pdf">October     20, 2006</a></li>
                                             
                                             <li><a href="documents/113006Minutes.pdf">November 30, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda-01-16-2007%5B1%5D.doc">January     16, 2007</a></li>
                                             
                                          </ul>
                                          
                                          <h3>Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceMinutes9-15-06.pdf">September     15, 2006</a></li>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceMinutes10-6-2006.pdf">October     6, 2006</a></li>
                                             
                                             <li><a href="documents/DataAnalysisTaskForceMinutes102006.pdf">October 20, 2006</a></li>
                                             
                                             <li><a href="documents/113006MinutesDataTaskForce.pdf">November 30, 2006 </a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <a></a>
                                       
                                       
                                       <h3>Mission, Vision, Values Task Force</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-VisionValuesMissionTaskForceAgenda9-27-06.pdf">September 27, 2006&nbsp;</a></li>
                                             
                                             <li><a href="meetingdocs/CPC-VisionValuesMissionAgenda10-10-06.pdf">October 10, 2006 </a></li>
                                             
                                             <li><a href="documents/VVMagenda11-14-06_1.pdf">November 14, 2006 </a></li>
                                             
                                             <li><a href="documents/VVMagenda12-4-06_1.pdf">December 4, 2006</a></li>
                                             
                                             <li>
                                                <a href="meetingdocs/1-VVM-07Agenda.doc">January 16, 2007</a> 
                                             </li>
                                             
                                             <li><a href="documents/VVMAgenda021307.pdf">February 13, 2007</a></li>
                                             
                                             <li><a href="documents/041607Agenda.pdf"> April 16, 2007 </a></li>
                                             
                                             <li><a href="documents/060607Agenda.pdf"> June 6, 2007 </a></li>
                                             
                                          </ul>
                                          
                                          <h3>Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-VisionValuesMissionTaskForceMinutes9-06-06.pdf">September 27, 2006</a></li>
                                             
                                             <li><a href="documents/VVMminutes10-10-06.pdf"> October 10, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/VVMminutes11-14-06.doc">November 14, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/VVMminutes12-4-06.doc">December 4, 2006</a></li>
                                             
                                             <li><a href="documents/VVMminutes021307.pdf">February 13, 2007</a></li>
                                             
                                             <li><a href="documents/060607MtgMinutes.pdf">June 6, 2007 </a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <a></a>
                                       
                                       
                                       <h3>Communications Task Force</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-CommunicationTaskForceAgenda10-10-2006.pdf">October                 10, 2006</a></li>
                                             
                                             <li><a href="meetingdocs/CPC-DataAnalysisTaskForceAgenda-11-30-2006%5B1%5D.doc"> November 20, 2006 </a></li>
                                             
                                             
                                             <li><a href="documents/Communicationagenda021607.pdf">February 16, 2007</a></li>
                                             
                                             <li><a href="documents/Communicationagenda040607.pdf">April 06, 2007</a></li>
                                             
                                             <li><a href="documents/040708CommunicationsTFAgenda.pdf"> April 22, 2008 </a></li>
                                             
                                             <li>
                                                <a href="documents/040708CommunicationsTFAgenda.pdf"> August 1, 2008 - no written agenda</a>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/091008MeetingAgenda.pdf">September 10, 2008 </a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          <h3>Meeting Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-CommunicationTaskForceMinutes10-10-2006-1.pdf">October                 10, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/CPCCommunicationTaskForceMinutes11-20-06.doc">November             20, 2006</a></li>
                                             
                                             
                                             <li><a href="documents/Communicationminutes021607.pdf">February 16, 2007 </a></li>
                                             
                                             <li><a href="documents/Communicationminutes040607_000.pdf">April 06, 2007</a></li>
                                             
                                             <li>
                                                <a href="documents/042208MeetingMinutes.pdf">April 22, 2008 </a>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/080108MeetingMinutes.pdf">August 1, 2008 </a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <a></a>
                                       
                                       
                                       <h3>Evaluations Task Force</h3>
                                       
                                       <div>
                                          
                                          <h3>Meeting Agendas</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-EvaluationTaskForceAgenda10-23-06-1.pdf">October 23, 2006 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceAgenda1113061.pdf">November 13, 2006 </a></li>
                                             
                                             <li><a href="documents/021907Agenda.pdf">February 19, 2007 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceDesignTeamAgenda31907.pdf">March 19, 2007 </a></li>
                                             
                                             <li><a href="documents/041607Agenda.pdf"> April 16, 2007 </a></li>
                                             
                                             <li><a href="documents/EvaluationTaskForceAgenda61807.pdf"> June 18, 2007 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceAgendaNov192007.pdf">November 19, 2007 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceAgendaMarch520081.pdf">March 5, 2008 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceAgendaMarch2620081.pdf"> March 26, 2008 </a></li>
                                             
                                          </ul>
                                          
                                          <h3>Minutes</h3>
                                          
                                          <ul>
                                             
                                             <li><a href="meetingdocs/CPC-EvaluationTaskForceMinutes-102306%5B1%5D.doc">October 23, 2006 </a></li>
                                             
                                             <li><a href="meetingdocs/CPC-EvaluationTaskForceMinutes111306%5B1%5D.doc">November 13, 2006 </a></li>
                                             
                                             <li><a href="documents/021907Minutes.pdf"> February 19, 2007 </a></li>
                                             
                                             <li><a href="documents/031907Minutes.pdf">March 19, 2007 </a></li>
                                             
                                             <li><a href="documents/EvaluationTaskForceMinutes041607.pdf">April 16, 2007 </a></li>
                                             
                                             <li><a href="documents/MinutesDesignTeamMeetingJuly16.pdf">July 16, 2007 </a></li>
                                             
                                             <li><a href="documents/CPCEvaluationTaskForceMinutesNov1920071.pdf">November 19, 2007 </a></li>
                                             
                                             <li><a href="documents/030508MeetingMinutes.pdf">March 5, 2008 </a></li>
                                             
                                          </ul>
                                          
                                       </div>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Data Analysis Results</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Key Points That Will Shape Valencia's Future</h3>
                                       
                                       <p><strong> This webpage contains archive information <u>only</u>. </strong></p>
                                       
                                       <p>A Fall 2006 review of data by the Data and Situational/Needs  Analysis Task Force
                                          revealed seven key points that the task force  believes are critically important to
                                          the College as we plan our future. 
                                       </p>
                                       
                                       <p><strong>1. Demand for higher education will grow in Central Florida</strong> due to continuing population increases, changes in the employment  market, and workforce
                                          vacancies as baby boomers retire. While high  school graduates will increase, the
                                          proportion earning a standard  diploma will decline. University admissions limits
                                          will increase the  number of students starting at community colleges.
                                       </p>
                                       
                                       <p><strong>2. The students in our future will be increasingly diverse in background and needs.</strong> Younger students will prefer non-traditional methods of learning.  Prospective students
                                          over age 44 (a group that will increase at a  higher rate than will the younger population)
                                          will be interested in  career changes and growth, weighing the investment of their
                                          time in  education as a cost.
                                       </p>
                                       
                                       <p><strong>3. Educational options available to students will continue to evolve.</strong> Private institutions will increase in number and enrollments, and  financial aid
                                          policies and availability will make it possible for  students to attend higher cost
                                          private institutions if they choose. 
                                       </p>
                                       
                                       <p><strong>4. Working to improve learning results, and to document  those improvements, will
                                             continue to challenge the College and our  students.</strong> The College can expect increased community interest  in how it can partner to increase
                                          high school graduation rates and  improve college readiness. As students move on to
                                          their first  experiences at Valencia, a large number will struggle, and we will need
                                          more information about why this is the case. As learning technologies  evolve, the
                                          College will need more information about which students can  be successful with different
                                          learning modalities, such as web-based  courses.&nbsp; 
                                       </p>
                                       
                                       <p><strong>5. The community's needs and related expectations are changing.</strong> More employees will be required who readily learn and adapt to new  technologies,
                                          who work effectively and serve people from other  cultures, and who contribute to
                                          solving societal and global problems.  Employment in the biological sciences, health
                                          care, high-technology  fields, business, construction, hospitality, and teaching will
                                          experience highest local demand. Cries for public accountability will  intensify,
                                          adding to the need to collect, analyze and report to the  public, and to improve our
                                          assessment of learning. 
                                       </p>
                                       
                                       <p><strong>6. Valencia's costs of doing business will continue to rise, and so must our revenues.
                                             </strong>Our  needs will exceed available State funds, meaning that we must continue  to seek
                                          alternative revenue through gifts, grants, and  revenue-generating activities. Significant
                                          investments in land, new  facilities, renovations, and technologies will be required
                                          in a  marketplace in which scarcity of many resources will drive up costs.  Competition
                                          for qualified personnel and the need for the development of  new leaders will intensify
                                          as baby boomers retire.
                                       </p>
                                       
                                       <p><strong>7. Defining community (which is, in fact, our middle name) is increasingly complicated
                                             and increasingly important.</strong> Just as the members of our immediate families are less likely to all  live in one
                                          geographic location, it is increasingly difficult to  pinpoint geographically the
                                          "community" that we serve or could serve,  both due to technology opening up distance
                                          learning options, and due to  growing numbers of people world-wide looking to Orlando
                                          as a place to  come for higher education. Valencia will be expected to contribute
                                          to  solving problems, both natural and human-made, that have no geographic  boundaries,
                                          such as worldwide health crises, disaster recovery, or the  prevention of violence.
                                          What we do here and now as we plan for 2008-13  will make a difference to our community,
                                          no matter how broadly or  narrowly we define it. 
                                       </p>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Vision Scan</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The Vision, Values, and Mission Task Force, based on collegewide input at    Learning
                                          Day on October 31, 2006, has  developed the following first draft    of revised statements
                                          of Vision, Values, and Mission for collegewide consideration.    Your comments are
                                          welcome. Please contact Task Force convener David Rogers    (drogers@valenciacollege.edu)
                                          to share your thoughts. We appreciate your taking  time to consider these and share
                                          your ideas.
                                       </p>
                                       
                                       <h3>Vision</h3>
                                       
                                       <p>“Valencia Community College: an extraordinary learning community” </p>
                                       
                                       <h3>Statutory Purpose</h3>
                                       
                                       <p>Valencia is a publicly supported, comprehensive community college  that continually
                                          identifies and addresses the changing learning needs  of the communities it serves.
                                          The College provides::
                                       </p>
                                       
                                       <ul>
                                          
                                          <li> Associate-degree programs that prepare learners to succeed in university studies.</li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li> Courses and services that provide learners with the right start in their college
                                             careers.
                                          </li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li>Associate degree, certificate, and continuing professional  education programs that
                                             prepare learners for entering and progressing  in the workforce
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>Mission</h3>
                                       
                                       <p>Valencia Community College supports the success of each learner by  embracing our
                                          values and committing to partnerships with public, civic,  and workforce organizations
                                          throughout our communities.&nbsp; Our mission is  to provide opportunities for academic,
                                          technical, and life-long  learning in a collaborative culture dedicated to inquiry,
                                          results, and  excellence.
                                       </p>
                                       
                                       <h3>Values</h3>
                                       
                                       <p>We value:</p>
                                       
                                       <ul>
                                          
                                          <li>
                                             <strong>Learning</strong> by committing to Valencia’s core  competencies – Think, Value, Communicate, and Act
                                             – and the potential  of each person to learn at the highest levels of achievement
                                             for  personal and professional success.
                                          </li>
                                          
                                          <li>
                                             <strong>People</strong> by creating a caring, inclusive, and safe environment that inspires all  people to
                                             achieve their goals, share their successes, and encourage  others.
                                          </li>
                                          
                                          <li>
                                             <strong>Diversity</strong> by fostering the  understanding it builds in learning relationships and appreciating
                                             the  dimensions it adds to our quality of life.
                                          </li>
                                          
                                          <li>
                                             <strong>Access</strong> by reaching out to our communities, inviting and supporting all learners and partners
                                             to achieve their goals.
                                          </li>
                                          
                                          <li>
                                             <strong>Integrity</strong> by respecting the ideals of freedom, civic responsibility, academic honesty, personal
                                             ethics, and the courage to act.
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>
                                             How did the initial, discussion draft version of the Vision, Values, and Mission statements
                                             come about?</strong></p>
                                       
                                       <p>Responses from full-time college employees were collected during the  morning Visioning
                                          Session on Learning Day, October 31, 2006.&nbsp; Two  activities during that session produced
                                          over 1200 comments on  Valencia’s existing Vision, Values, and Mission statements.
                                       </p>
                                       
                                       <p>An initial sort of those responses was conducted by the Vision,  Values, and Mission
                                          Task Force Core Editing Team.&nbsp; The team sorted  responses based on the existing Vision,
                                          Values, and Mission statements  to identify duplications and additions.
                                       </p>
                                       
                                       <p> In November, the Vision, Values, and Mission Task Force identified  six emergent
                                          themes in the sorted data and recommended the Core Editing  Team perform another sorting
                                          around those themes.&nbsp; The Task Force  reduced those six themes to five at their December
                                          meeting.&nbsp; There was  consensus that these five, with brief explanatory statements,
                                          addressed  both the comments received on Learning Day and the spirit of the  existing
                                          Vision, Values, and Mission statements.&nbsp; The revisions  presented the concepts in
                                          a simpler and more direct format.&nbsp; The Task  Force authorized the Core Editing Team
                                          to proceed with a draft of the  explanatory statements. 
                                       </p>
                                       
                                       <p>Preliminary drafts of the edited Values, Vision, and Mission  statements were shared
                                          with the Vision, Values, and Mission Task Force  for review and comments in mid-December
                                          and on January 16, 2007.&nbsp; This  initial discussion draft was the product of that review.
                                       </p>
                                       
                                       <p><strong><br>
                                             Why do we need Vision, Values, and Mission statements? </strong></p>
                                       
                                       <p>One of the primary ways that our accrediting body, the Southern  Association of Colleges
                                          and Schools, evaluates the integrity of  “educational quality” is by examining how
                                          well the college’s defined  learning mission and core values impact student learning.&nbsp;
                                          When serious  discussions of mission, goals, teaching, student aspirations, learning
                                          outcomes, and support services are regular and productive, college  processes are
                                          more coherent, more meaningful, and more useful to  students, employees, and the local
                                          community.
                                       </p>
                                       
                                       <p>The Vision, Values, and Mission statements should guide discussions  and actions toward
                                          improving learning, realizing aspirations, and  identifying challenges and solutions.&nbsp;
                                          They should drive strategic  thinking at Valencia. 
                                       </p>
                                       
                                       <p><strong><br>
                                             Comments are welcome</strong></p>
                                       
                                       <p>In the coming weeks, opportunities for discussion on these draft  revisions will be
                                          available on all campuses.&nbsp; For more information, for  comments, or for scheduling
                                          a discussion time for your department or  group, please contact <a href="../../../contact/Search_Detail2.cfm-ID=776.html">David Rogers</a>, Special Assistant,  Learning-Centered Initiative.
                                       </p>
                                       
                                       <p>Please click on link below to download:</p>
                                       
                                       <ul>
                                          <li><a href="meetingdocs/A-VVMdataThemeDraft.xls" target="_parent">Data Sort     of "Visioning Scan"</a></li>
                                       </ul>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Draft Statements</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Draft Strategic Issues</h3>
                                       
                                       <p>Strategic issues are those issues that must be resolved if the College is to achieve
                                          its mission. The strategic goals are aimed at helping to resolve the strategic issues
                                          and to move the College toward its vision and the achievement of the Big Hairy Audacious
                                          Goals. Strategic issues that Valencia faces in 2007 are:
                                       </p>
                                       
                                       <ol>
                                          
                                          <li>Many students from diverse backgrounds do not have equal access to college.</li>
                                          
                                          <li>Many students whose stated goal is to obtain a certificate or a degree are not successful
                                             in reaching critical milestones in their educational plans.
                                          </li>
                                          
                                          <li>Gaps in student achievement exist among student cohorts related to ethnicity and income.</li>
                                          
                                          <li>The composition of the group that graduates each year does not match the diversity
                                             of entering cohorts.
                                          </li>
                                          
                                          <li>Diversity is not fully utilized as a strength throughout the curriculum and the College.</li>
                                          
                                          <li>While the College does not control all factors that lead to student success, many
                                             students fail to complete their courses with a grade of C or better for reasons potentially
                                             under the control of the College.
                                          </li>
                                          
                                          <li>Not all students receive academic support tailored to their needs as they move through
                                             the curriculum.
                                          </li>
                                          
                                          <li>The college lacks the human, fiscal, technological, and physical resources to meet
                                             all of the current needs of students and all who want to be students.
                                          </li>
                                          
                                          <li>Students, faculty and staff do not always have access to up-to-date, effective technologies
                                             that support learning and professional success.
                                          </li>
                                          
                                          <li>Many of the issues that stand in the way of our students’ success require resources
                                             and expertise from other organizations with complementary missions, if the issues
                                             are to be resolved.
                                          </li>
                                          
                                          <li>Valencia can expect delays and challenges in hiring due to the highly competitive
                                             market for key positions.
                                          </li>
                                          
                                          <li>Current faculty and staff have unrealized potential that should be developed to further
                                             the College’s goals.
                                          </li>
                                          
                                          <li>College staffing levels, systems and processes can sometimes lead students to feel
                                             as if they are “numbers” rather than persons to be served.
                                          </li>
                                          
                                          <li>The College’s operating, technology and capital budgets do not always align clearly
                                             with the strategic plan.
                                          </li>
                                          
                                       </ol>
                                       
                                       
                                       <h3>August 2, 2007 Draft</h3>
                                       
                                       <p><em>Draft Strategic Goals for 2008-2013</em></p>
                                       
                                       <ol>
                                          
                                          <li>Build Pathways:</li>
                                          
                                       </ol>
                                       
                                       <ul>
                                          
                                          <li>Remove barriers to college.</li>
                                          
                                          <li>Create connections that raise personal aspirations of students and enable them to
                                             achieve their aspirations.
                                          </li>
                                          
                                          <li>Develop and renew programs.</li>
                                          
                                       </ul>
                                       
                                       <li>Learning Assured:</li>
                                       
                                       <ul>
                                          
                                          <li>Create optimal conditions for student learning.</li>
                                          
                                          <li>Partner with students to improve their contribution to achieving their potential.</li>
                                          
                                          <li>Close achievement gaps.</li>
                                          
                                       </ul>
                                       
                                       <li>Invest in Each Other:</li>
                                       
                                       <ul>
                                          
                                          <li>Strengthen our collaborative institutional culture to foster deep stewardship of our
                                             work.
                                          </li>
                                          
                                          <li>Support the professional development, career growth and healthy lives of Valencia’s
                                             employees.
                                          </li>
                                          
                                       </ul>
                                       
                                       <li>Partner with the Community:</li>
                                       
                                       <ul>
                                          
                                          <li>Cooperate with community partners in meeting students’ needs and College goals.</li>
                                          
                                          <li>Involve the college in meeting the community’s needs and goals.</li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Next Steps</h3>
                                       
                                       <p>This document will be shared College-wide and with the College Planning Committee.
                                          The Committee will take action in August 2007, and once approved by the Committee,
                                          it will be presented to the District Board of Trustees for its consideration.
                                       </p>
                                       
                                       <p>Goal teams for each of the four strategic goals will draft measurable objectives that
                                          name the results that we want to change during 2008-13 if we are to achieve each goal.
                                       </p>
                                       
                                       <p>Members of the goals teams will collaborate on essays that clarify the meaning of
                                          each goal.
                                       </p>
                                       
                                       <p>The objectives and the essays will be shared College-wide for comment in late October
                                          or early November 2007, and edited based on feedback received. The final version will
                                          be incorporated into the 2008-13 strategic plan in November 2007. The strategic plan
                                          will be presented to the College Planning Committee in November 2007, and with the
                                          approval of the Committee, to the District Board of Trustees in December 2007.
                                       </p>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Updates</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>July 2008</h3>
                                       
                                       <p>Valencia’s <a href="documents/StratPlanBOOKMARKED_001.pdf">Strategic Plan for 2008-13</a> was approved by the District Board of  Trustees on June 17, 2008, following a two-year
                                          planning period that  involved constituencies from throughout the college and from
                                          the  community that we serve.
                                       </p>
                                       
                                       <p>As  we begin the 2008-09 academic year, the College is completing the task  of gathering
                                          the baseline data named in the Evaluation section of the  Strategic Plan. Planning
                                          units throughout the College are creating  plans for their work and linking those
                                          plans to the goals and  objectives in the new Strategic Plan. (Each subdivision of
                                          the College  that is assigned a separate budget is considered to be a planning unit.)
                                       </p>
                                       
                                       <p>The  Communications Task Force, led by <a href="../../../contact/Search_Detail2.cfm-ID=1972.html">Karen Blondeau</a>, will be sharing  information about the new plan with the college and community in
                                          a  variety of ways. If you have ideas, or are interested in working with  the Task
                                          Force, contact <a href="../../../contact/Search_Detail2.cfm-ID=1972.html">Karen</a>.
                                       </p>
                                       
                                       <p>Also  in Fall 2008, a series of essays will be released which explore and  invite
                                          Collegewide conversations about the meaning of the four  strategic goals. These essays
                                          are being co-authored by President Sandy  Shugart and members of the Goals Teams who
                                          worked in 2007-08 to develop  the objectives for each goal.
                                       </p>
                                       
                                       <p>On March 20, 2009, a <a href="BIG/index.html">Big Meeting</a> will be held to explore progress made in 2008-09.
                                       </p>
                                       
                                       
                                       <h3>Detailed Updates</h3>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/102706UpdateforWeb.pdf">October 27, 2006</a></li>
                                          
                                          <li><a href="documents/MonthlyUpdatesNovember302006.pdf"> November 30, 2006</a></li>
                                          
                                          <li><a href="documents/MonthlyUpdatesJanuary2007.pdf">January, 2007</a></li>
                                          
                                          <li><a href="documents/MonthlyUpdatesMarch12007.pdf"> March 1, 2007 </a></li> 
                                          <li><a href="documents/MonthlyUpdatesMarch292007.pdf"> March 29, 2007</a></li> 
                                          <li><a href="documents/MonthlyUpdatesJune242007.pdf"> June 24, 2007 </a></li>
                                          
                                          <li><a href="documents/080907StrategicPlanningUpdate.pdf"> August 9, 2007 </a></li>
                                          
                                          <li><a href="documents/StrategicPlanningUpdateAugust2007.pdf"> August 31, 2007 </a></li>
                                          
                                       </ul>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>Strategic Planning</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Budget and Financial Advisory Group</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>
                                          
                                          The <strong>College Planning Council</strong> welcomes you to the <strong>Budget and Financial Advisory Group</strong> page. 
                                       </p>
                                       
                                       <p>If you have questions about our work, are interested in getting involved, or have
                                          comments to offer, please contact either of our co-chairs, Keith Houck or Rob McCaffrey.
                                       </p>
                                       
                                       <p><strong><u>Charge of the Budget &amp; Financial Advisory Group</u></strong> 
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Design and implement a college-wide process for gathering input and feedback that
                                             will be used in the development of budget goals, principles and priorities and the
                                             development of the annual budget.
                                          </li>
                                          
                                          <li>Review processes that guide decisions having budgetary implications at the request
                                             of the College Planning Council.
                                          </li>
                                          
                                          <li>Research, at the request of the College Planning Council, issues that impact the annual
                                             college budget.
                                          </li>
                                          
                                          <li>Review annually and revise as needed the Strategic Budget Initiative Process.</li>
                                          
                                          <li>Review annually and revise as needed the Research and Development for Innovations
                                             Processes.
                                          </li>
                                          
                                          <li>Hold an annual orientation session for members of the Strategic Budget Initiatives
                                             Review Group prior to SBI proposal submission review and ranking process.
                                          </li>
                                          
                                       </ul>
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Budget &amp; Financial Advisory Group 2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Name</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Representative Group</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Mail Code</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Ext.</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Kristen Abel</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Faculty</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3-2</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>2403</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Sherri Dixon</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Budget Office</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>DTC-3</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3306</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Sue Fagan </p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Budget Office</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>DTC-3</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3309</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Elisha Gonzalez-Bonnewitz</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Professional</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>DTC-2</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3120</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Jeff Hogan</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Professional</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>4-14</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>5564</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Keith Houck</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Co-Chair</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>DTC-3</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3465</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Susan Ledlow </p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Administrator</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>DTC-4</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3415</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Bonnie Oliver</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Faculty</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3-29</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>2214</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>David Sutton</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Administrator</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>3-2</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>2753</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>2 Career Staff positions are currently vacant </p>
                                                </div>
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <p><strong>BUDGET STATEMENT FOR 2012-13</strong></p>
                                       
                                       <p>The Budget Statement for 2012-2013 was prepared under the direction of Jean Marie
                                          Führman, co-chair of the Budget and Financial Advisory Group, to determine whether
                                          the College’s budget for 2012-13 reflected the Budget Planning Principles adopted
                                          by the College Planning Council. The Budget Statement was approved by the College
                                          Planning Council as being representative of the college’s budget allocation process.
                                          Please click on the following link to view the Budget Statement: <a href="CPCO/documents/Budget_Statement_2012-13.pdf">2012 - 2013 Budget Statement</a></p>
                                       
                                       <p><strong>BUDGET PLANNING PRINCIPLES FOR 2013-14</strong></p>
                                       
                                       <p>This year the Budget and Financial Advisory Group has moved from value-centered principles
                                          to design-centered principles. By using design-centered principles, we hope to align
                                          our resources with Valencia's vision, values, mission, and strategic and organizational
                                          plans. Since the budget principles are a living document, both the process and content
                                          will continue to be a part of Valencia's ongoing institutional effectiveness cycle.
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Enchance the quality of studnet experience while minimizing the financial impact </li>
                                          
                                          <li>Strtive to retain and develop quality employees </li>
                                          
                                          <li>Strive for a robust quality of service and support </li>
                                          
                                          <li>Continue to trust those closest to the work to apply principles of effective stewardship
                                             in the use of college resources
                                          </li>
                                          
                                          <li>Create a multi-year financial strategy that strikes a balance between making the best
                                             use of our resources and managing our fund balance*
                                          </li>
                                          
                                          <li>Make the budget process collaborative and transparent at all levels of the college</li>
                                          
                                          <li>Invest in strategic initiatives that are focused on improving student success</li>
                                          
                                          <li>Maintain resources to maximize sustainability</li>
                                          
                                          <li>Seek out and invest in innovative thinking   </li>
                                          
                                       </ul>
                                       
                                       <blockquote>
                                          
                                          <p>*The fund balance is an accumulation of excess revenues over expenditures that is
                                             available for use by the college.&nbsp; We are required by Florida Statute 1011.84(3) to
                                             maintain a minimum unallocated fund balance of at least 5% of total operating funds
                                             available.
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <p>Revised 10/25/12 by the Budget &amp; Financial Advisory Group</p>                        
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>College Planning Committee</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The College Planning Committee is a representative group that  makes recommendations
                                          about the content of the strategic plan to the President. The President, in    turn,
                                          makes final recommendations to the District Board of Trustees. The    Committee includes
                                          a 10-member Steering Committee, described    below, and 20 additional members: 9 senior
                                          staff not serving as council co-chairs,    2 deans; 3 additional faculty, 3 additional
                                          professional staff, and 3 additional  career service staff. 
                                       </p>
                                       
                                       <p>The Committee  reviews  and approves the planning    process and its work products,
                                          notes any additional information or consultation    needed as the plan is developed,
                                          and makes needed adjustments to ensure a clear, consistent, and logical strategic
                                          planning document. 
                                       </p>
                                       
                                       <p>The Planning Steering Committee is a sub-group of the College    Planning Committee
                                          composed of the President , six governing council co-chairs,    one Trustee, one 
                                          representative of Career Service staff, and one representative of the professional
                                          staff. 
                                       </p>
                                       
                                       <p>The College Planning Committee membership during 2006-08 included: Joe Battista, Amy
                                          Bosely, Tom    Byrnes, Julie Corderman, Suzette Dohany, Kurt Ewen, Fitzroy Farquharson,
                                          Geraldine Gallagher,    Jared Graber, Thomas Greene,  Keith Houck, Debi Jakubcin,
                                          Brenda Jones, Susan Kelley, Sue    Maffei, Michele McArdle, Kenneth Moses, Bill Mullowney,
                                          John Niss, Ruth Prather, Joyce Romano,    Sanford Shugart, Michael Shugg, Larry Slocum,
                                          Stan Stone, Linda Swaine, Chanda Torres, Kaye    Walter, Rose Watson, Bill White,
                                          Falecia Williams, Reneesa Williams,  Silvia  Zapico, and Marisa Zuniga.
                                       </p>
                                       
                                       <p>For more information, visit the <a href="../../../lci/StrategicPlan0813.html">Learning-Centered Initiative</a> site.
                                       </p>
                                       
                                       <p>View the Committee's <a href="minutes.html#committee">meeting agendas and minutes</a>.
                                       </p>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Plan for Planning</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong>Approved by College Planning Council</strong> - May 25, 2006<br>
                                          <strong>Submitted to the President for Approval</strong> - June 5, 2006
                                       </p>
                                       
                                       
                                       <p><em>The dates in this Plan were adjusted by the College Planning Council during  2006-08
                                             and the final version is in the Strategic Plan for 2008-15<a href="documents/AppendixD-StratPlan.pdf">Appendix D.</a></em></p>
                                       
                                       <p><strong>Plan for Planning Committee Members:</strong> Linda Anthon, Fiona Baxter, Fitzroy Farquharson, Jean Marie Fuhrman,  Linda Swaine,
                                          David Rogers
                                       </p>
                                       
                                       <h3>1. Design principles for the Strategic Planning Process</h3>
                                       
                                       <p>(These are numbered for ease of reference, but are not listed in any priority order.)</p>
                                       
                                       <ol>
                                          
                                          <li>The  planning process and the plan that it yields will be learning-centered,  will
                                             be grounded in the College's history of excellence, innovation,  and community, and
                                             will support the quality and aspiration that  bequeath the College with its distinctive
                                             place in higher education.
                                          </li>
                                          
                                          <li>The  process will be strategic by impacting the results the college aims to  provide
                                             to society and to students as they progress in their programs  of learning.
                                          </li>
                                          
                                          <li>The planning process will be  collaborative by operating within our shared governance
                                             structure that  ensures broad-based participation and by providing a means for  stakeholder
                                             groups to be heard and to influence the plan. 
                                          </li>
                                          
                                          <li>The  process will build trust through effective communication and  negotiation, by
                                             making it safe to identify and challenge assumptions,  and by supporting agreements
                                             on shared values and the making of mutual  commitments that are the basis for the
                                             strategic plan, and that are  honored as the plan is implemented.
                                          </li>
                                          
                                          <li>The process  will be meaningful in that it will help the College to establish a  vision
                                             of the future that shapes, defines, and gives meaning to its  strategic purpose, and
                                             in that it will help to shape strategic  decisions, some of which are identified in
                                             advance.
                                          </li>
                                          
                                          <li>The  process will be data-driven, using qualitative and quantitative data,  routinely
                                             reviewed as the plan is implemented, with the aim of  continuous improvement.
                                          </li>
                                          
                                          <li>The plan will include  formative and summative evaluation components that evaluate
                                             the  planning process itself, as well as the implementation of the plan,  using agreed
                                             upon performance indicators.
                                          </li>
                                          
                                          <li>The  process will have a clear cycle of activities, with a beginning and an  end,
                                             and timed and structured to coordinate well with SACS  accreditation requirements.
                                          </li>
                                          
                                          <li>The process will be as  simple as possible while yielding a viable plan, avoiding
                                             the trap of  imposing more order than the College can tolerate, and integrating  planning
                                             into permanent governing structures and collegewide meetings,  rather than creating
                                             a separate set of activities removed from the  governance and life of the College.
                                          </li>
                                          
                                          <li>The process  will support the integration of fiscal, learning, and facilities plans
                                             with the strategic plan of the college, through careful timing and by  clearly connecting
                                             each of these plans to the College's revised Vision,  Mission, and Values.
                                          </li>
                                          
                                          <li>The strategic plan will be  useful to and therefore used by councils, campuses and
                                             departments as  they prepare their plans, and will encourage a future orientation
                                             to  their work.
                                          </li>
                                          
                                          <li>The process, its language, its products, and the results of the plan will be communicated
                                             to all employees internally.
                                          </li>
                                          
                                          <li>The plan will be expressed clearly, with language that is understood by stakeholders
                                             and with clear means of measuring progress.
                                          </li>
                                          
                                          <li>The process will be truly comprehensive, and will have clearly assigned roles for
                                             individuals and groups, including students.
                                          </li>
                                          
                                       </ol>
                                       
                                       
                                       <h3>2. Name the work products of the planning process</h3>
                                       
                                       <ul>
                                          
                                          <li> needs assessment/situational analysis/environmental scan, providing a  common understanding
                                             of the present and the anticipated future,  including information about our competitors
                                             and clearly defined gaps in  results at the mega, macro, and micro levels, as defined
                                             by Roger  Kaufman's planning model 
                                          </li>
                                          
                                          <li>reviewed/revised mission (the role we will play), vision, and values statements</li>
                                          
                                          <li>a  set of college strategies (the ways in which we will play our role and  get results),
                                             goals (what results we want to accomplish within our  role), measurable outcomes objectives
                                             (how much will we change specific  results at the mega, macro, and micro levels as
                                             used in Kaufman's  model), and related activities to achieve the objectives within
                                             an  agreed upon timeframe
                                          </li>
                                          
                                          <li>an evaluation plan, including  indicators/measures of institutional effectiveness,
                                             revised as needed,  and a means of assessing the extent to which College decisions
                                             are  consistent with the plan
                                          </li>
                                          
                                          <li>a recommended assignment of responsibilities for objectives to the governing councils</li>
                                          
                                          <li>list of major decisions to be impacted by the plan, which could include decisions
                                             such as:
                                          </li>
                                          
                                          <ul>
                                             
                                             <li>academic program plans for new campus(es) and evolution of programs on existing campuses</li>
                                             
                                             <li>the goals in the College enrollment plan</li>
                                             
                                             <li>Future Valencia Foundation fund raising goals</li>
                                             
                                             <li>Focus of the Quality Enhancement Plan for SACS in 2010-2012</li>
                                             
                                             <li>Professional development multi-year plan</li>
                                             
                                             <li>Multi-year financial plan and annual budgets</li>
                                             
                                             <li>Major external funding requests with collegewide impact, such as Title III and Title
                                                V
                                             </li>
                                             
                                             <li>Community relations priorities and programs</li>
                                             
                                             <li>Efforts designed to support student learning and to maintain academic excellence</li>
                                             
                                             <li>Strategic facilities plans</li>
                                             
                                             <li>Final proposed planning document for submission to the president and trustees </li>
                                             
                                          </ul>
                                          
                                          
                                       </ul>
                                       
                                       
                                       <h3>3. Establish first planning cycles and project future cycles.</h3>
                                       
                                       <ul>
                                          
                                          <li>The  following sample cycle provides for three, six-year planning cycles,  with a
                                             mid-point review/update/adjustment at the end of the third year  of each cycle. Importantly,
                                             this is planned to complement the SACS  accreditation process in 2010-12 and 2020-22,
                                             and to enable the  institution to devote adequate time and energy to both strategic
                                             planning and to developing the Quality Enhancement Plan required by  SACS. 
                                          </li>
                                          
                                          <li>
                                             <strong>2006-07</strong> - Complete remaining priority items under current Strategic Learning Plan.
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><strong>2008-2015 Planning Cycle</strong></p>
                                       
                                       <ul>
                                          
                                          Needs Assessment/Situational Analysis/environmental scan/- completed by November 30,
                                          2006
                                          
                                          <li>Review/revise mission, vision, and values statements - completed by February 28, 2007</li>
                                          
                                          <li>Proposed strategies, goals and measurable objectives - completed by April 30, 2007</li>
                                          
                                          <li>(The  strategies and goals will be set by working through a joint meeting of  the
                                             governing councils, with a means for collegewide input and  feedback. The objectives
                                             for each goal will be proposed by each Council  to which goals are assigned.)
                                          </li>
                                          
                                          <li>Proposed activities to achieve the objectives - completed by August 31, 2007</li>
                                          
                                          <li>Complete strategic planning document finalized - completed by October 31, 2007</li>
                                          
                                          <li>New plan for 2008-2015 presented to the Trustees for action - Nov - Dec 2007</li>
                                          
                                          <li>Evaluation of the planning process - April 30, 2007</li>
                                          
                                          <li>Annual  evaluation of progress on plan - submitted in Spring of each year,  covering
                                             the past calendar year, and impacting the budget process for  the following year.
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><strong>SACS 2010-12</strong></p>
                                       
                                       <ul>
                                          
                                          <li>SACS  Quality Enhancement Plan - Must develop in 2010-11, should be selected  based
                                             on the work on which we want to focus at this stage of the  2008-2015 plan.
                                          </li>
                                          
                                          <li>Mid-point evaluation and update of 2008-15 plan - submitted Spring 2011</li>
                                          
                                          <li>SACS Team Site Visit to Valencia - Spring 2012</li>
                                          
                                       </ul>
                                       
                                       <p><strong>2014-2019 Planning Cycle</strong></p>
                                       
                                       <ul>
                                          
                                          
                                          <li>Needs Assessment/Situational Analysis/Environmental Scan for 2014 - 2019- completed
                                             by November 30, 2012
                                          </li>
                                          
                                          <li>Review/revision of mission, vision, and values statements - completed by February
                                             28, 2013
                                          </li>
                                          
                                          <li>Proposed strategies, goals and measurable objectives - completed by April 30, 2013</li>
                                          
                                          <li>Proposed activities to achieve the objectives - completed by August 31, 2013</li>
                                          
                                          <li>Complete strategic planning document finalized - completed by October 31, 2013</li>
                                          
                                          <li>New plan for 2014-2019 presented to the Trustees for action - Nov-Dec 2013</li>
                                          
                                          <li>Annual  evaluation of progress on 2014-19 plan - submitted in Spring of each  year,
                                             and impacting the budget process for the following year.
                                          </li>
                                          
                                          <li>Mid-point evaluation and update of plan - submitted Spring 2017</li>
                                          
                                       </ul>
                                       
                                       <p><strong>2020-2025 Planning cycle</strong></p>
                                       
                                       <ul>
                                          
                                          <li>Needs Assessment/Situational Analysis/Environmental Scan for 2020-2025 - completed
                                             by November 30, 2018
                                          </li>
                                          
                                          <li>Review/revision of mission, vision, and values statements - completed by February
                                             28, 2019
                                          </li>
                                          
                                          <li>Proposed strategies, goals and measurable objectives - completed by April 30, 2019</li>
                                          
                                          <li>Proposed activities to achieve the objectives - completed by August 31, 2019</li>
                                          
                                          <li>Complete strategic planning document finalized - completed by October 31, 2019</li>
                                          
                                          <li>New plan for 2020-25 presented to the Trustees for action - Nov - Dec 2017</li>
                                          
                                       </ul>
                                       
                                       <p><strong>SACS - 2020-22</strong></p>
                                       
                                       <ul>
                                          
                                          <li>SACS Quality Enhancement Plan - Must develop in 2020-21</li>
                                          
                                          <li>Annual  evaluation of progress on plan - submitted in Spring of each year and  impacting
                                             the budget process for the following year.
                                          </li>
                                          
                                          <li>SACS Team Site Visit to Valencia - 2022</li>
                                          
                                          <li>Mid-point evaluation and update of strategic plan - submitted Spring 2023</li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>4.  Set up an organizational chart of committees and/or task forces to  design and
                                          carry out each phase, including projected Small Meetings and  Big Meetings, and working
                                          closely with existing Councils and within the  governance structure.
                                       </h3>
                                       
                                       <div>
                                          
                                          <p>We will begin with the structure for 2006-07. </p>
                                          
                                          <p>The CPC will work collaboratively with others to develop and recommend planning processes.</p>
                                          
                                          <p>A  Planning Committee, membership to be determined, will act on the  recommendations
                                             that come from those processes, making final  recommendations to the President and
                                             Trustees about content/priorities  in the strategic plan.
                                          </p>
                                          
                                          <p>The President, as our chief planner, is being consulted about the structure and membership
                                             of the Planning Committee.
                                          </p>
                                          
                                          <p>We  propose to recruit/name the following five task forces. We will seek to  involve
                                             CPC alumni in these teams and in the planning work.
                                          </p>
                                          
                                       </div>
                                       
                                       <p><strong>Task Force #1 - Need Assessment/Situational analysis/Environmental Scan</strong></p>
                                       
                                       <ul>
                                          
                                          <li>Data gathering will take place June - mid-September 2006.</li>
                                          
                                          <li>Data analysis will take place during late September - early November 2006.</li>
                                          
                                       </ul>
                                       
                                       <div>
                                          
                                          <p>We  would ask Thomas Greene, Rhonda Glover, Joan Tiller and Fiona Baxter to  propose
                                             the data to be gathered (for CPC approval) and to suggest the  optimum size for the
                                             task force.
                                          </p>
                                          
                                          <p>We would then ask the  Faculty Council and the professional and career service staff
                                             councils  for volunteers, issue a collegewide call for volunteers, and invite CPC
                                             alumni to participate. The task force would coordinate the data  gathering activities,
                                             analyze and summarize the findings (including a  needs analysis that specifies results
                                             gaps), and conduct and issue an  analysis of our situation (which may include a report
                                             on strengths,  weaknesses, opportunities, and threats), completing their work by 
                                             November 30, 2006.
                                          </p>
                                          
                                          <p>We would ask the team to propose by  the July 2006 CPC meeting a process of inviting
                                             large scale  consideration of the data, resulting in a situational analysis report,
                                             which may include a SWOT analysis or some alternative assessment, to  the College.
                                             This process would be acted on at the July CPC meeting,  and recommended to the Planning
                                             Committee.
                                          </p>
                                          
                                       </div>
                                       
                                       <p><strong>Task Force #2 - Mission, Vision, and Values</strong></p>
                                       
                                       <div>
                                          
                                          <p>In  Fall 2006, the CPC will recruit/name a Mission, Vision and Values Task  Force.
                                             The task force will review the processes used to create the  current statements, and&nbsp;
                                             propose a process to invite broad input into  review and revision, as needed, of these
                                             statements. 
                                          </p>
                                          
                                          <p>It  is anticipated that the process will work within our current structures  and collegewide
                                             meeting opportunities. For example, work to review and  recommend changes to the Visions,
                                             Values, and Mission statements could  be conducted as part of the October 31, 2006
                                             Learning Day.
                                          </p>
                                          
                                          <p>The  task force will work with the CPC to carry out the process by February  28, 2007,
                                             with recommendations made to the Planning Committee at that  time.
                                          </p>
                                          
                                       </div>
                                       
                                       <p><strong>Task Force #3 - Strategies, Goals and Objectives</strong></p>
                                       
                                       <div>
                                          
                                          <p>In  January 2007, the CPC will recruit/name a Strategies, Goals and  Objectives Task
                                             Force. The task force will review the processes used to  create the strategies and
                                             goals in the current SLP, and propose a  process to invite broad input into decisions
                                             to be made about  strategies, goals and objectives for 2008-15. 
                                          </p>
                                          
                                          <p>It is  anticipated that the process will work within our current structures  and collegewide
                                             meeting opportunities. For example, the governing  councils could be asked to work
                                             jointly, seeking input and reaction  from the College, in crafting new strategies
                                             and goals. And each  Council assigned new goals would be asked to develop measurable
                                             objectives and a plan of action to achieve the objectives, all of which  would tie
                                             into the annual budgeting process and the multi-year  financial plan.
                                          </p>
                                          
                                          <p>The work would take place during the  Summer and Fall of 2007. Resulting recommendations
                                             for new strategies,  goals, and objectives would be made to the Planning Committee
                                             by  October 31, 2007, and the 2008-09 budget (and the budgets in subsequent  years)
                                             would reflect the priorities in the plan. 
                                          </p>
                                          
                                       </div>
                                       
                                       <p><strong>Task Force #4 - Communications</strong></p>
                                       
                                       <div>
                                          
                                          <p>In  July 2006, the CPC will recruit/name a Communications Task Force that  will&nbsp; have
                                             the responsibility of communicating with the College about  the planning process,
                                             opportunities to participate, and the products of  the process. This task force will
                                             play a pivotal role in ensuring that  the plan is based on the best thinking of the
                                             College and that the  planning process is open and builds trust.
                                          </p>
                                          
                                       </div>
                                       
                                       <p><strong>Task Force #5 - Evaluation</strong></p>
                                       
                                       <div>
                                          
                                          <p>In  September 2006, the CPC will recruit/name an Evaluation Task Force that  will
                                             have the responsibility of evaluating the planning process, using  the design principles
                                             established, and seeking feedback from all those  involved. The Task Force will also
                                             make recommendations about the  process of evaluation of the results achieved throughout
                                             the  implementation of the plan, working through existing councils and  meetings to
                                             the greatest extent possible.
                                          </p>
                                          
                                       </div>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Strategic Goal Teams 2009-10</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>In 2009-10, the College Planning Council convened a Strategic Goal Teams for each
                                          of the four goals in the College Strategic Plan: 1) Build Pathways, 2) Learning Assured,
                                          3) Invest in Each Other, and 4) Partner with the Community. Each goal team will monitor
                                          progress toward a goal, collect and interpret data, monitor related strategic issues,
                                          make recommendations to the annual Big Meeting, and prepare an annual report on progress
                                          toward the goal. 
                                       </p>
                                       
                                       <p> The first meeting of the four strategic goals teams was held November 10, 2009, on
                                          West Campus, in Building 8, the Special Events Center, from 1:15 - 4:15 p.m. If you
                                          have questions about our work, are interested in getting involved, or have comments
                                          to offer, please contact either of our co-chairs, Joan Tiller or Jean Marie Führman.
                                       </p>
                                       
                                       <ul>
                                          
                                          <li><a href="BIG/documents/CPCStrategicGoalTeamsDec14.docx">List of Goal Team Members</a></li>
                                          
                                          <li><a href="documents/StrategicPlanBrochure_000.pdf">Strategic Planning Brochure</a></li>
                                          
                                          <li>
                                             <a href="documents/StratPlanBOOKMARKED_000.pdf">Strategic Plan - Complete Narrative</a> 
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>&nbsp;  </h3>
                                       
                                       <h3><strong>President Shugart's Goal Essays </strong></h3>
                                       
                                       <ul>
                                          
                                          <li><a href="BIG/documents/CPCGoalOneEssayOct2009.docx">Goal One Essay: Build Pathways </a></li>
                                          
                                          <li><a href="BIG/documents/Goal2EssayLearningAssuredMarch14FINAL.docx">Goal Two Essay: Learning Assured</a></li>
                                          
                                          <li><a href="BIG/documents/Goal3EssayInvestinEachOtherMarch14.docx">Goal Three Essay: Invest in Each Other </a></li>
                                          
                                          <li><a href="BIG/documents/GoalFourEssayMarch152010.docx">Goal Four Essay: Partner with the Community </a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Strategic Goal Team Materials</h3>
                                       
                                       
                                       <p><strong>Goal Team Meeting Schedules</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="BIG/documents/CPCGoalTeamSchedule-GroupMeetings1stRoundDec2.docx">1st Round Goal Team Meeting Schedule</a></li>
                                          
                                          <li>
                                             <a href="BIG/documents/CPCGoalTeamSchedule-GroupMeetings2ndRoundJan28.docx">2nd Round Goal Team Meeting Schedule</a>  
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>Goal Team Collective Work Products</strong></p>
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="BIG/documents/CPCFirstGoalTeamMeetingNov10FlipChartNotes.docx">November 10, 2009 - Flip Chart Notes</a> 
                                          </li>
                                          
                                          <li><a href="BIG/documents/JointGoalTeamMeetingTableCommentsonEssay1andEncouragingDialogueNov102009.docx">November 10, 2009 - Table Comments on Goal One Essay</a></li>
                                          
                                          <li><a href="BIG/documents/JointGoalTeamMeetingTableCommentsonRolesPrinciplesNov10.docx">November 10, 2009 - Team Comments on Meeting Plans, Roles, Responsibilities and Principles</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <h3><strong>Goal One <strong>Team</strong> Materials</strong></h3>
                                       
                                       <blockquote>
                                          
                                          <p><strong>1st Goal Team 1 Meeting Attachments- January 21, 2010</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam1AgendaFirstSoloMeetingJan212010.doc">CPC Goal Team 1 Agenda First Solo Meeting Jan 21 2010</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1Jan21AttachAStrategicGoalTeamsNov24.docx">CPC Goal Team 1 Jan 21 Attach A Strategic Goal Teams Nov 24</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalteam1Jan21AttachBChargeUPDATEDNov102009.docx">CPC Goal team 1 Jan 21 Attach B Charge UPDATED Nov 10 2009</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1Jan21AttachCNov10GoalTeamFlipChartNotes.docx">CPC Goal Team 1 Jan 21 Attach C Nov 10 Goal Team Flip Chart Notes</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1Jan21AttachDNov10GoalTeamCommentsonRolesPrinciples.docx">CPC Goal Team 1 Jan 21 Attach D Nov 10 Goal Team Comments on Roles Principles</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1AttachEJan21StrategicIssuesListfrom2008-13Plan.docx">CPC Goal Team 1 Attach E Jan 21 Strategic Issues List from 2008-13 Plan</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeam1Jan21AttachFInitiativesListDevelopedbySrStaffDec1.xls">CPC Goal Team 1 Jan 21 Attach F Initiatives List Developed by Sr Staff Dec 1</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>Minutes &amp; Notes - January 21, 2010 Meeting </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam1MinutesFirstSoloMeetingJan212010.doc">CPC Goal Team 1 Minutes First Solo Meeting Jan 21 2010</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachANov10FlipChartNotes.docx">CPC Goal Team 1 Minutes Jan 21 Attach A Nov10 Flip Chart Notes</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachBNov10RolesandPrinciples.docx">CPC Goal Team 1 Minutes Jan 21 Attach B Nov 10 Roles and Principles</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachCOutcomes.docx">CPC Goal Team 1 Minutes Jan 21 Attach C Outcomes</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachDStrategicIssuesIdentified.docx">CPC Goal Team 1 Minutes Jan 21 Attach D Strategic Issues Identified</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeam1MinutesJan21AttachEInitiativesListModified.xls">CPC Goal Team 1 Minutes Jan 21 Attach E Initiatives List Modified</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachFStrategicIssuesBrieftemplate.docx">CPC Goal Team 1 Minutes Jan 21 Attach F Strategic Issues Brief template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam1MinutesJan21AttachGInitiativereporttemplate.docx">CPC Goal Team 1 Minutes Jan 21 Attach G Initiative report template</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>2nd Goal Team 1 Meeting Attachments - February 11, 2010 </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate.docx">CPC Goal Team Initiative report template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamOutcomesReport.docx">CPC Goal Team Outcomes Report</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>Minutes &amp; Notes - February 11, 2010 </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam1MinutesSecondSoloMeetingFeb112010.doc">CPC Goal Team 1 Minutes Second Solo Meeting Feb 11 2010</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>3rd Goal Team 1 Meeting Attachments - March 1, 2010</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="file:///S%7C/Institutional%20Adv%20&amp;%20ResDev-Common/CPC%20-%20COLLEGE%20PLANNING%20COUNCIL/STRATEGIC%20PLANNING%20GOALS%20TEAMS/Goal%20Team%201/3rd%20Meeting%20March%201%202010/CPC%20Goal%20Team%201%20Agenda%20Third%20Solo%20Meeting%20March%201%202010.doc">CPC Goal Team 1 Agenda Third Solo Meeting March 1 2010</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <h3><strong>Goal Two Team Materials</strong></h3>
                                       
                                       <blockquote>
                                          
                                          <p><strong>1st Goal Team 2 Meeting Attachments - December 11, 2009</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam2AgendaFirstSoloMeetingDec11.doc">CPC Goal Team 2 Agenda First Solo Meeting Dec 11</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                             
                                             <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                             
                                             <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>Minutes &amp; Notes - December 11, 2009 Meeting</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam2MinutesFirstSoloMeetingDec112009.doc">CPC Goal Team 2 Minutes First Solo Meeting Dec 11 2009</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam2Dec11MinutesAttachAStrategicIssuesListfrom2008-13Plan.docx">CPC Goal Team 2 Dec 11 Minutes Attach A Strategic Issues List from 2008-13 Plan</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam2MinutesDec11AttachBInitiativesListwithGoal2teamedits.xlsx">CPC Goal Team 2 Minutes Dec 11 Attach B Initiatives List with Goal 2 team edits</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam2MinutesDec11AttachCStrategicIssuesBrieftemplate.docx">CPC Goal Team 2 Minutes Dec 11 Attach C Strategic Issues Brief template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeam2Dec11MinutesAttachDInitiativereporttemplate.docx">CPC Goal Team 2 Dec 11 Minutes Attach D Initiative report template</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>2nd Goal Team 2 Meeting Attachments - February 9, 2010 </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_000.docx">CPC Goal Team Initiative report template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_000.docx">CPC Goal Team Outcomes Report</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_000.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                             
                                             <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_000.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>Minutes &amp; Notes - February 9, 2010</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam2ThirdSoloMeetingAttachAMinutesofFeb92010.doc">CPC Goal Team 2 Third Solo Meeting Attach A Minutes of Feb 9 2010</a></li>
                                             
                                          </ul>
                                          
                                          <p><strong>3rd Goal Team 2 Meeting Attachments - February 23, 2010</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam2AgendaThirdSoloMeetingFeb232010.doc">CPC Goal Team 2 Agenda Third Solo Meeting Feb 23 2010</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <h3><strong>Goal Three  <strong>Team</strong> Materials</strong></h3>
                                       
                                       <blockquote>
                                          
                                          <p><em><strong>Subgroup 3.1</strong></em></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1st Subgroup Goal Team 3.1 Meeting Attachments - December 7, 2009</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan_000.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009_000.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24_000.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan_000.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - December 7, 2009 Meeting</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/StrategicGoal3Minutes12-7-09update2revised.doc">Strategic Goal 3 Minutes 12-7-09 update (2) revised</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>2nd Subgroup Goal Team 3.1 Meeting Attachments - February 10, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_001.docx">CPC Goal Team Initiative report template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_001.docx">CPC Goal Team Outcomes Report</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_001.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_001.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Minutes &amp; Notes - February 10, 2010</strong></p>
                                             
                                             
                                          </blockquote>
                                          
                                          
                                          <p><em><strong>Subgroup 3.2</strong></em></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1st Subgroup Goal Team 3.2 Meeting Attachments - December 16, 2009</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan_001.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009_001.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24_001.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan_001.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativesListDevelopedbySrStaffDec10.xlsx">CPC Goal Team Initiatives List Developed by Sr Staff Dec 10</a></li>
                                                
                                                <li><a href="BIG/documents/StrategicMeetingNotes121609.docx">Strategic Meeting Notes 121609</a></li>
                                                
                                                <li><a href="BIG/documents/RolesandPrinciples121609.docx">Roles and Principles 121609</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Minutes &amp; Notes - December 16, 2009 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/StrategicPlanGoal3.2MtgMinutes12.16.09_000.docx">Strategic Plan Goal 3.2 Mtg Minutes 12.16.09</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>2nd Subgroup Goal Team 3.2 Meeting Attachments - January 25, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_002.docx">CPC Goal Team Initiative report template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_002.docx">CPC Goal Team Outcomes Report</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_002.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_002.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Minutes &amp; Notes - January 25, 2010 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeam3-2Minutes012510.docx">CPC Goal Team 3-2 Minutes 012510</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate-11.docx">CPC Goal Team Strategic Issues Brief template - 11</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate-12.docx">CPC Goal Team Strategic Issues Brief template - 12</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          
                                          <p><em><strong>Subgroup 3.3</strong></em></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1st Subgroup Goal Team 3.3 Meeting Attachments - December 2, 2009</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan_002.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009_002.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24_002.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan_002.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - December 2, 2009</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeam33FirstSoloMeetingMinutesDec220091.docx">CPC Goal Team 3 3 First Solo Meeting Minutes Dec 2 20091</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>2nd Subgroup Goal Team 3.3 Meeting Attachments - January 27, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeam3.3AgendaSecondSoloMeetingJan262010.doc">CPC Goal Team 3.3 Agenda Second Solo Meeting Jan 26 2010</a></li>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaSecondSoloMeetingJanorFeb2010.doc">CPC Goal Team Agenda Second Solo Meeting Jan or Feb 2010</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_003.docx">CPC Goal Team Initiative report template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_003.docx">CPC Goal Team Outcomes Report</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_003.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_003.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - January 27, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeam3.3SecondSoloMeetingMinutesJan262010.doc">CPC Goal Team 3.3 Second Solo Meeting Minutes Jan 26 2010</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       
                                       <h3><strong>Goal Four <strong>Team</strong> Materials</strong></h3>
                                       
                                       <blockquote>
                                          
                                          <p><em><strong>Subgroup 4.1 &amp; 4.2</strong></em></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1st Subgroup Goal Team 4.1 &amp; 4.2 Meeting Attachments - December 9, 2009 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan_003.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009_003.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24_003.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan_003.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - December 9, 2009 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/GoalTeam4MeetingNoteDec92009.doc">Goal Team 4 Meeting Note Dec 9 2009</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>2nd Subgroup Goal Team 4.1 &amp; 4.2 Meeting Attachments - January 28, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaSecondSoloMeetingJan.doc">CPC Goal Team Agenda Second Solo Meeting Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_004.docx">CPC Goal Team Initiative report template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_004.docx">CPC Goal Team Outcomes Report</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_004.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_004.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - January 28, 2010</strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/GoalTeam4MeetingNotesJan282010.doc">Goal Team 4 Meeting Notes Jan 28 2010</a></li>
                                                
                                                <li><a href="documents/StrategicIssue8Brief-EdAmes.doc">Strategic Issue 8 Brief - Ed Ames</a></li>
                                                
                                             </ul>
                                             
                                             
                                          </blockquote>
                                          
                                          <p><em><strong>Subgroup 4.3 &amp; 4.4</strong></em></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1st Subgroup Goal Team 4.3 &amp; 4.4 Meeting Attachments - December 4, 2009 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="documents/2008-IndicatorsOfEngagement.pdf">2008-IndicatorsOfEngagement</a></li>
                                                
                                                <li><a href="documents/CPCGoalTeamAgendaFirstSoloMeetingDecorJan_004.doc">CPC Goal Team Agenda First Solo Meeting Dec or Jan</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicGoalTeamsNov24_004.docx">CPC Strategic Goal Teams Nov 24</a></li>
                                                
                                                <li><a href="BIG/documents/CPCStrategicIssuesListfrom2008-13Plan_004.docx">CPC Strategic Issues List from 2008-13 Plan</a></li>
                                                
                                                <li><a href="documents/Furcorubriccopy.doc">Furco rubric copy</a></li>
                                                
                                                <li><a href="documents/ServiceLearningatValenciaCommunityCollegeDraftProposalv2.doc">Service Learning at Valencia Community College Draft Proposal v2</a></li>
                                                
                                                
                                                <li><a href="BIG/documents/ServiceLearningprogramsinFL.docx">Service Learning programs in FL</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalteamschargeUPDATEDNov102009_004.docx">CPC Goal teams charge UPDATED Nov 10 2009</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - December 4, 2009 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/GoaltTeam4-3and4-4Minutes120409.docx">Goalt Team 4-3 and 4-4 Minutes 120409</a></li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>2nd Subgroup Goal Team 4.3 &amp; 4.4 Meeting Attachments - January 29, 2010 </strong></p>
                                             
                                             <ul>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamInitiativereporttemplate_005.docx">CPC Goal Team Initiative report template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReport_005.docx">CPC Goal Team Outcomes Report</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamOutcomesReportingSampleTemplate_005.docx">CPC Goal Team Outcomes Reporting Sample Template</a></li>
                                                
                                                <li><a href="BIG/documents/CPCGoalTeamStrategicIssuesBrieftemplate_005.docx">CPC Goal Team Strategic Issues Brief template</a></li>
                                                
                                             </ul>
                                             
                                             <p><strong>Minutes &amp; Notes - January 29, 2010 </strong></p>
                                             
                                             <ul>
                                                
                                                <li>Coming Soon </li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       
                                       <h3><strong>Charge of the 2009-10 Strategic Goal Teams </strong></h3>
                                       
                                       
                                       <p>Each goal team will monitor progress toward a goal, collect and interpret data, monitor
                                          related strategic issues, make recommendations to the annual Big Meeting, and prepare
                                          an annual report on progress toward the goal.
                                       </p>
                                       
                                       <p>Each goal team will maintain the College’s “to do” list of the initiatives and projects
                                          that are intentionally aligned with each goal. This list of a handful of major projects
                                          for each goal (not to be confused with the more exhaustive list that can be culled
                                          from all unit plans) will help us to maintain our focus on deliverables. This will
                                          also help us get past the vagueness that some may feel over what constitutes a “goal”
                                          vs. an “objective” vs. an “outcome.”
                                       </p>
                                       
                                       
                                       <p><strong><u>Composition of the Goal Teams</u></strong></p>
                                       
                                       <p>The goals teams, which will be recruited college-wide, will include volunteers from
                                          faculty, career service, professional and administrative staff. The Council will seek
                                          to have representation from each campus on each goal team. Also, care will be taken
                                          to ensure that those who have responsibility for the work of the goal are represented
                                          on each goal team.
                                       </p>
                                       
                                       
                                       <p><strong><u>Guiding Principles </u></strong></p>
                                       
                                       <p>The teams will utilize principles to guide their work, and will ask one member of
                                          the team to call the attention of the team to the extent to which the work is being
                                          conducted in keeping with &nbsp;those principles. The teams will develop the principles
                                          at a first, joint meeting, set for November 10. That meeting will include Dr. Shugart
                                          who will assist in launching the work of the teams.
                                       </p>
                                       
                                       
                                       <p><strong><u>Responsibilities of the Goal Teams</u></strong></p>
                                       
                                       <p>The teams will have the following responsibilities, designed to be carried out in
                                          four or five meetings between November 2009 and June 1010, with support and assistance
                                          provided by the co-chairs of the College Planning Council. The first meeting was 
                                          a joint meeting of all four goal teams, set for 1:15 p.m. – 4:15 p.m., November 10,
                                          2009.
                                       </p>
                                       
                                       <p><u>By December 18, 2009 (this should be achieved in one meeting, on November 10)</u>:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Review and discuss the goal assigned to the team and its place within the strategic
                                             plan.
                                          </li>
                                          
                                          <li>Recommend ways to encourage college-wide discussion and dialog on the goal essay developed
                                             by the President that will provide perspective on the goal, with the aims of furthering
                                             understanding of the meaning of the goal and supporting its continued evolution based
                                             on what we learn from our assessment of progress each year.
                                          </li>
                                          
                                          <li>Identify a few key strategic issues that relate to each goal, including relevant issues
                                             named in the Strategic Plan, and devise a means of monitoring those issues and updating
                                             the college regarding potential impact on the College and our ability to carry out
                                             the goal.
                                          </li>
                                          
                                          <li>Develop principles to guide the work of the goal teams.</li>
                                          
                                       </ul>
                                       
                                       <p><u>By February 5, 2010 (this work is anticipated to require two meetings)</u>:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Consider and recommend the changes that might be observed and documented that would
                                             indicate that the College was making progress toward achieving the goal.
                                          </li>
                                          
                                          <li>Identify, obtain, and study appropriate baseline data that will be useful as indicators
                                             of progress toward achieving the goal, using 2008-09 as the baseline year. (Data may
                                             be obtained from the office of Institutional Research, the Data Task Force of the
                                             College Planning Council, the College Data Team, and/or other sources.)
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><u>By March 3, 2010 (this work is anticipated to require one meeting)</u>:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Review the original objectives developed for each goal, interpret baseline data and
                                             any progress data from 2009-10, review updates on strategic issues, draft a list of
                                             major projects and initiatives that are intentionally aligned with the goal, and make
                                             any recommendations regarding the goal that the committee believes may be advisable
                                             for consideration at the Big Meeting on March 19, 2010. (Team members will participate
                                             in the Big Meeting.)
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><u>By June 30, 2010 (this work is anticipated to require one meeting)</u>:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Prepare an annual report on progress toward the goal, with a focus on the list of
                                             major projects and initiatives that are intentionally aligned with the goal, including
                                             recommendations on how the College might use what is learned to improve its effectiveness.
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <h3>Strategic Goal Teams 2011-12</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>In 2009-10, the College Planning Council convened a <strong>Strategic Goal Teams</strong> for each of the four goals in the College Strategic Plan: 1) Build Pathways, 2) Learning
                                          Assured, 3) Invest in Each Other, and 4) Partner with the Community. Each goal team
                                          will monitor progress toward a goal, collect and interpret data, monitor related strategic
                                          issues, make recommendations to the annual Big Meeting, and prepare an annual report
                                          on progress toward the goal. 
                                       </p>
                                       
                                       
                                       <h3> Summaries of Progress on Goals 2009-10 </h3>
                                       
                                       <p>College Planning Council  2009-10 Co-chairs Amy Bosley and Susan Kelley drafted a
                                          two-page summary of the findings of each 09-10 goal teams, presented in these four
                                          documents. The first page of each summary lists the goal and its objectives, and states
                                          the long-term aims set by the team. The second page provides a summary of progress
                                          in 2009-10. 
                                       </p>
                                       
                                       
                                       <p><strong>Strategic Goal Team One: Build Pathways</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalOneSummaryOctober2010.docx">CPC Goal One Summary October 2010</a></li>
                                          
                                          <li><a href="documents/StrategicGoals1BoardPresentationNotesAdded21311.ppt">Strategic Goals One Board Presentation</a></li>
                                          
                                       </ul>
                                       
                                       <p><strong>Strategic Goal Team Two: Learning Assured</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTwoSummaryOctober2010.docx">CPC Goal Two Summary October 2010</a></li>
                                          
                                          <li><a href="documents/StrategicGoal2BoardPresentationApril16AMABKEedits.ppt">Strategic Goals Two Board Presentation</a></li>
                                          
                                       </ul>
                                       
                                       <p><strong>Strategic Goal Team Three: Invest in Each Other</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalThreeSummaryOctober2010.docx">CPC Goal Three Summary October 2010</a></li>
                                          
                                          <li><a href="documents/StrategicGoal3BoardPresentationMay15.pptx">Strategic Goals Three Board Presentation</a></li>
                                          
                                       </ul>
                                       
                                       <p><strong>Strategic Goal Team Four: Partner with the Community</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalFourSummaryOctober2010.docx">CPC Goal Four Summary October 2010</a></li>
                                          
                                          <li><a href="documents/StrategicGoal4BoardPresentationJune_20_AM_draft.ppt">Strategic Goals Four Board Presentation</a></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><a href="#top">TOP</a> 
                                       </p>
                                       
                                       <h3>Combined Strategic Goal Team Meeting October 22, 2010 </h3>
                                       
                                       <p>College Planning Council  2010-11 Co-chairs Ed Frame and Susan Kelley convened the
                                          2010-11 Strategic Goal Teams in a combined meeting on October 22, 2010. Documents
                                          distrubuted to the new goal team members are listed below.
                                       </p>
                                       
                                       
                                       <p><strong>Team Rosters</strong></p>
                                       
                                       <ul>
                                          
                                          <li> <a href="documents/CombinedStrategicGoalTeamRosterNov152010.docx">Combined Strategic Goal Team Roster Nov 15 2010 </a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>Agenda &amp; Documents</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/JointGoalTeamMeetingAgendaOct2220101_000.docx">Joint Goal Team Meeting Agenda Oct 22 2010</a></li>
                                          
                                          <li><a href="documents/GoalTeamPrinciples09-10_000.docx">Goal Team Principles 09-10</a></li>
                                          
                                          <li><a href="documents/GoalTeamsNotesonTaxonomyOct222010.docx">Goal Teams Notes on Taxonomy Oct 22 2010</a></li>
                                          
                                          <li><a href="documents/GoalTeam1TableNotesonEightTaxonomyQuestions.docx">Goal Team 1 Table Notes on Eight Taxonomy Questions</a></li>
                                          
                                          <li><a href="documents/GoalTeam1OrganizationalSession.docx">Goal Team 1 Organizational Session</a></li>
                                          
                                          <li><a href="documents/GoalTeam2TableNotesonEightTaxonomyQuestions.docx">Goal Team 2 Table Notes on Eight Taxonomy Questions</a></li>
                                          
                                          <li><a href="documents/GoalTeam2OrganizationalSession.docx">Goal Team 2 Organizational Session</a></li>
                                          
                                          <li><a href="documents/GoalTeam3TableNotesonEightTaxonomyQuestions.docx">Goal Team 3 Table Notes on Eight Taxonomy Questions</a></li>
                                          
                                          <li><a href="documents/GoalTeam3OrganizationalSession.docx">Goal Team 3 Organizational Session</a></li>
                                          
                                          <li><a href="documents/GoalTeam4TableNotesonEightTaxonomyQuestions.docx">Goal Team 4 Table Notes on Eight Taxonomy Questions</a></li>
                                          
                                          <li><a href="documents/GoalTeam4OrganizationalSession.docx">Goal Team 4 Organizational Session</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>Team Meetings &amp; Instructions </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CombinedStrategicGoalTeamMeetingCalendar-Round1.docx"> Combined Strategic Goal Team Meeting Calendar - Round 1</a></li>
                                          
                                          <li> <a href="documents/WEAVEONLINEAccessInstructionsforGoalTeamsNov2010.docx">Weave Online Goal Report Instructions </a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       <h3>Strategic Goal Team One: Build Pathways</h3>
                                       
                                       <p><strong>Baseline Data:</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/Baselinedata1.1MarketShare.pdf">Baseline data 1.1 Market Share</a></li>
                                          
                                          <li><a href="documents/BaselineData1.1HighSchools.xlsx">Baseline Data 1.1 High Schools</a></li>
                                          
                                          <li><a href="documents/BaselineData1-1College-going.pptx">Baseline Data 1.1 College-going</a></li>
                                          
                                          <li><a href="documents/BaselineData1-1SeminoleCollege-going.xlsx">Baseline data 1.1 Seminole College-going</a></li>
                                          
                                          <li><a href="documents/BaselineData1.2.pdf">Baseline Data 1.2</a></li>
                                          
                                          <li><a href="documents/BaselineData1-2GatewayGaps.pptx">Baseline Data 1.2 Gateway Gaps</a></li>
                                          
                                          <li><a href="documents/BaselineData1.3withdrawals.xlsx">Baseline Data 1.3 withdrawals</a></li>
                                          
                                          <li><a href="documents/BaselineData1.3Completion.docx">Baseline Data 1.3 Completion</a></li>
                                          
                                          <li><a href="documents/BaselineData1-3GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 1.3 Gateway Gap Model FTIC Students 20110131</a></li>
                                          
                                          <li><a href="documents/BaselineData1-3GatewayGapAnalysisbyCourse.pptx">Baseline Data 1.3 Gateway Gap Analysis by Course</a></li>
                                          
                                          <li><a href="documents/BaselineData1-3StrategicIndicatorsReport.docx">Baseline Data 1.3 Strategic Indicators Report</a></li>
                                          
                                          <li><a href="documents/BaselineData1-3DFWITop10Courses.pptx">Baseline Data 1.3 DFWI Top 10 Courses</a></li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BaselineData1.4EconDev.doc">Baseline Data 1.4 Econ Dev</a></li>
                                          
                                          <li><a href="documents/BaselineData1.5Scholarships.docx">Baseline Data 1.5 Scholarships</a></li>
                                          
                                          <li><a href="documents/Baseline1-5FinancialAid2008.pdf">Baseline Data 1.5 Financial Aid 2008</a></li>
                                          
                                          <li><a href="documents/Baselinedata1-5DirectConnectEnrollment2007-102.xlsx">Baseline data 1-5 Direct Connect Enrollment 2007-10 2</a></li>
                                          
                                          <li><a href="documents/BaselineData1-5AlternativeDelivery.pptx">Baseline Data 1.5 Alternative Delivery</a></li>
                                          
                                          <li><a href="documents/BaselineData1.5Facilities.docx">Baseline Data 1.5 Facilities</a></li>
                                          
                                          <li><a href="documents/BaselineData1-5DirectConnectSTEMdata.pptx">Baseline Data 1.5 Direct Connect STEM data</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>1st Goal Team 1 Meeting</strong></p>
                                       
                                       <p>Date: December 1, 2010 – 1:30PM to 3:30PM</p>
                                       
                                       <p>Location: CJI 143</p>
                                       
                                       <p><strong>Agenda &amp; Documents</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam1AgendaFirstSoloMeetingDec12010.docx"> CPC Goal Team 1 Agenda First Solo Meeting Dec 1 2010</a></li>
                                          
                                          <li><a href="documents/WeaveGoal1Nov182010.pdf">Weave Goal 1 Nov 18 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>2nd Goal Team 1 Meeting</strong></p>
                                       
                                       <p>Date: February 7, 2011 - 9:00AM to 12:00PM</p>
                                       
                                       <p>Location: CJI 148</p>
                                       
                                       <p><strong>Agenda &amp; Documents </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam1AgendaSecondSoloMeetingFeb72011.doc">CPC Goal Team 1 Agenda Second Solo Meeting Feb 7 2011</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam1MinutesFirstSoloMeetingDec12010.docx">CPC Goal Team 1 Minutes First Solo Meeting Dec 1 2010</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam1ReferenceforIssuesInitiativesFeb72011.docx">CPC Goal Team 1 Reference for Issues Initiatives Feb 7 2011</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>3rd  Goal Team 1 Meeting</strong></p>
                                       
                                       <p>Date: April 11, 2011 - 2:00PM to 4:30PM</p>
                                       
                                       <p>Location: CJI 148</p>
                                       
                                       <p><strong>Agenda &amp; Documents </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam1AgendaSoloMeetingApril2011.doc">CPC Goal Team 1 Agenda Solo Meeting April 2011</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam1MinutesSoloMeetingFeb72011.docx">CPC Goal Team 1 Minutes Solo Meeting Feb 7 2011</a></li>
                                          
                                       </ul>
                                       
                                       <p><a href="#top">TOP</a> 
                                       </p>
                                       
                                       
                                       <h3>Strategic Goal Team Two: Learning Assured </h3>
                                       
                                       <p><strong>Baseline Data:</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BaselineData2-1and2-2.docx">Baseline Data 2-1 and 2-2</a></li>
                                          
                                          <li><a href="documents/BaselineData2-3.pdf">Baseline Data 2-3</a></li>
                                          
                                          <li><a href="documents/BaselineData2-4.pdf">Baseline Data 2-4</a></li>
                                          
                                          <li><a href="documents/BaselineData2-415CollegeCredits.xlsx">Baseline Data 2-4 15 College Credits</a></li>
                                          
                                       </ul>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BaselineData2-5GatewayGapModelFTICStudents20110131.xlsx">Baseline Data 2-5 Gateway Gap Model FTIC Students 20110131</a></li>
                                          
                                          <li><a href="documents/BaselineData2-5GatewayGapAnalysisbyCourse.pptx">Baseline Data 2-5 Gateway Gap Analysis by Course</a></li>
                                          
                                          <li><a href="documents/Baseline2-5-StrategicIndicatorsReport.docx">Baseline 2-5 - Strategic Indicators Report</a></li>
                                          
                                       </ul>
                                       
                                       <p><strong>1st Goal Team 2 Meeting</strong></p>
                                       
                                       <p>Date: November 19, 2010 - 2:00 p.m. to 4:00 p.m.</p>
                                       
                                       <p>Location: CJI 150</p>
                                       
                                       <p><strong>Agenda &amp; Documents</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam2AgendaFirstSoloMeetingNov192010.doc"> CPC Goal Team 2 Agenda First Solo Meeting Nov 19 2010</a></li>
                                          
                                          <li><a href="documents/WeavePlansGoal2Nov122010.pdf">Weave Plans Goal 2 Nov 12 2010</a></li>
                                          
                                          <li><a href="documents/Goal2AttachmentNov19-ProgramLearningOutcomeAssessmentPlanTemplate.docx">Goal 2 Attachment Nov 19 - Program Learning Outcome Assessment Plan Template</a></li>
                                          
                                          <li><a href="documents/Goal2AttachmentNov19IEComponentGoal.doc">Goal 2 Attachment Nov 19 IE Component Goal</a></li>
                                          
                                          <li><a href="documents/Goal2AttachmentNov19AssessingCollegeLevelWritinginENC1101.doc">Goal 2 Attachment Nov 19 Assessing College Level Writing ENC1101</a></li>
                                          
                                          <li><a href="documents/Goal2AttachmentNov19AcademicPrograms.doc">Goal 2 Attachment Nov 19 Academic Programs</a></li>
                                          
                                          <li><a href="documents/Goal2AttachmentNov19LETRubricforWrittenCommunication.pdf">Goal 2 Attachment Nov 19 LET Rubric for Written Communication</a></li>
                                          
                                          <li><a href="documents/Goal2MinutesOctober222010.docx">Goal 2 Minutes October 22 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>2nd Goal Team 2 Meeting</strong></p>
                                       
                                       <p>Date: February 3, 2011 - 1:30PM to 4:30PM </p>
                                       
                                       <p>Location: EC 3-113 </p>
                                       
                                       <p><strong>Agenda &amp; Documents </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam2AgendaSecondSoloMeetingFeb32011.doc">CPC Goal Team 2 Agenda Second Solo Meeting Feb 3 2011</a></li>
                                          
                                          <li><a href="documents/CPCGoal2AgendaFeb32011AttachmentB.docx">CPC Goal 2 Agenda Feb 3 2011 Attachment B</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam2MinutesFirstSoloMeetingNov192010.doc">CPC Goal Team 2 Minutes First Solo Meeting Nov 19 2010</a></li>
                                          
                                          <li><a href="documents/WeavePlansGoal2Nov122010_000.pdf">Weave Plans Goal 2 Nov 12 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>3rd Goal Team 2 Meeting</strong></p>
                                       
                                       <p>Date: April 15, 2011 - 
                                          
                                          
                                          2:00PM to 4:00PM
                                       </p>
                                       
                                       <p>Location: CJI 140 </p>
                                       
                                       <p><strong>Agenda &amp; Documents </strong></p>
                                       
                                       <p><a href="#top">TOP</a> 
                                       </p>
                                       
                                       
                                       <h3>Strategic Goal Team Three: Invest in Each Other </h3>
                                       
                                       <p><strong>Baseline Data: </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/BaselineData3.2-StaffOrgDevPlan2008-09.pdf">Baseline Data 3.2 - StaffOrg Dev Plan 2008-09</a></li>
                                          
                                          <li><a href="documents/BaselineData3.2-LeadershipValenciaOfferings2008-09.pdf">Baseline Data 3.2 - Leadership Valencia Offerings 2008-09</a></li>
                                          
                                          <li><a href="documents/Baseline3.3-EmployeeSurvey.docx">Baseline 3.3 - Employee Survey</a></li>
                                          
                                          <li><a href="documents/Baseline3.3-CignaWellnessAggregateData.docx">Baseline 3.3 - Cigna Wellness Aggregate Data</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong><em>Subgroup 3.1 </em></strong></p>
                                       
                                       <blockquote>
                                          
                                          <p><strong>1st Goal Team 3.1 Meeting</strong></p>
                                          
                                          <p>Date: November 10, 2010 - 9:00 a.m. to 11:00 a.m.</p>
                                          
                                          <p>Location: EC 3-113 &amp; <em>GoToMeeting</em> 
                                          </p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam31AgendaFirstSoloMeetingNov102010.doc">CPC Goal Team 3 1 Agenda First Solo Meeting Nov 10 2010</a></li>
                                             
                                             <li><a href="documents/WeaveGoal3OutcomeandObjective1Nov9report.pdf">Weave Goal 3 Outcome and Objective 1 Nov 9 report</a></li>
                                             
                                             <li><a href="documents/SharedGovernanceDocuments.docx">Shared Governance Documents</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>2nd Goal Team 3.1 Meeting (Subcommittee)</strong></p>
                                          
                                          <p>Date: March 21, 2011 - 
                                             
                                             
                                             2:00PM to 4:00PM
                                          </p>
                                          
                                          <p>Location: 
                                             
                                             
                                             WC SSB-171J
                                          </p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam3-1AgendaThirdSoloMeetingMarch212011.doc">CPC Goal Team 3-1 Agenda Third Solo Meeting March 21 2011</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <blockquote>
                                          
                                          <p><strong>3rd Goal Team 3.1 Meeting</strong></p>
                                          
                                          <p>Date: April 14, 2011 - 
                                             
                                             
                                             3:00PM to 4:30PM
                                          </p>
                                          
                                          <p>Location: <em>GoToMeeting</em></p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          
                                          
                                       </blockquote>
                                       
                                       <p><strong><em>Subgroup 3.2 </em></strong></p>
                                       
                                       <blockquote>
                                          
                                          <p><strong>1st Goal Team 3.2 Meeting</strong></p>
                                          
                                          <p>Date: November 15, 2010 - 2:00 p.m. to 5:00 p.m.</p>
                                          
                                          <p>Location: CJI 219 </p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam32AgendaFirstSoloMeetingNov152010.doc"> CPC Goal Team 3 2 Agenda First Solo Meeting Nov 15 2010</a></li>
                                             
                                             <li><a href="documents/WeaveGoal3Outcome2andObjective2Nov10.pdf">Weave Goal 3 Outcome 2 and Objective 2 Nov 10</a></li>
                                             
                                          </ul>
                                          
                                          
                                          <p><strong>2nd Goal Team 3.2 Meeting</strong></p>
                                          
                                          <p>Date: January 24, 2011 - 2:00 p.m. to 5:00 p.m.</p>
                                          
                                          <p>Location: CJI 219</p>
                                          
                                          <p><strong>Agenda &amp; Documents </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam3-2AgendaSoloMeetingJan242011.docx">CPC Goal Team 3-2 Agenda Solo Meeting Jan 24 2011</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeam3-2MinutesfromNov152010.doc">CPC Goal Team 3-2 Minutes from Nov 15 2010</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>3rd Goal Team 3.2 Meeting</strong></p>
                                          
                                          <p>Date: March 22, 2011 - 
                                             
                                             
                                             2:00PM to 5:00PM
                                          </p>
                                          
                                          <p>Location: CJI 219</p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>Agenda &amp; Documents </strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam3-2AgendaThirdSoloMeetingMar222011.doc">CPC Goal Team 3-2 Agenda Third Solo Meeting Mar 22 2011</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <p><strong><em>Subgroup 3.3 </em></strong></p>
                                       
                                       <blockquote>
                                          
                                          <p><strong>1st Goal Team 3.3 Meeting</strong></p>
                                          
                                          <p>Date: December 1, 2010 - 9:00 a.m. to 10:30 a.m. </p>
                                          
                                          <p>Location: CJI 143 </p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam33AgendaFirstSoloMeetingDec120101.doc"> CPC Goal Team 3 3 Agenda First Solo Meeting Dec 1 20101</a></li>
                                             
                                             <li><a href="documents/Goal3.3WeavePlans.pdf">Goal 3.3 Weave Plans</a></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       
                                       <blockquote>
                                          
                                          <p><strong>2nd Goal Team 3.3 Meeting</strong></p>
                                          
                                          <p>Date: January 28, 2011 - 
                                             
                                             
                                             10:00AM to 12:00PM
                                          </p>
                                          
                                          <p>Location: CJI 143 </p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam33AgendaSecondMeetingJan282011.docx">CPC Goal Team 3 3 Agenda Second Meeting Jan 28 2011</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeam33MinutesFirstMeetingDec12010.doc">CPC Goal Team 3 3 Minutes First Meeting Dec 1 2010</a></li>
                                             
                                          </ul>
                                          
                                          
                                          <p> <strong>3rd Goal Team 3.3 Meeting</strong></p>
                                          
                                          <p>Date: April 4, 2011 - 
                                             
                                             
                                             2:30PM to 4:30PM
                                          </p>
                                          
                                          <p>Location: CJI 148</p>
                                          
                                          <p><strong>Agenda &amp; Documents</strong></p>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/CPCGoalTeam3-3AgendaThirdSoloMeetingApril420111.doc">CPC Goal Team 3-3 Agenda Third Solo Meeting April 4 20111</a></li>
                                             
                                             <li><a href="documents/CPCGoalTeam3-3MinutesSecondSoloMeetingJan282011.doc">CPC Goal Team 3-3 Minutes Second Solo Meeting Jan 28 2011</a></li>
                                             
                                          </ul>
                                          
                                          
                                       </blockquote>
                                       
                                       <p><a href="#top">TOP</a></p>
                                       
                                       <h3>Strategic Goal Team Four: Parther with the Community </h3>
                                       
                                       <p><strong>Baseline Data:</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/Baselinedata4.1FundsRaised.xls">Baseline data 4.1 Funds Raised</a></li>
                                          
                                          <li><a href="documents/Baselinedata4.2AlumniGiving.docx">Baseline data 4.2 Alumni Giving</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>1st Goal Team 4 Meeting</strong></p>
                                       
                                       <p>Date: December 3, 2010 - 1:00PM to 4:00PM </p>
                                       
                                       <p>Room: CJI 143 </p>
                                       
                                       <p><strong>Agenda &amp; Documents</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam4AgendaFirstSoloMeetingDec32010_000.doc"> CPC Goal Team 4 Agenda First Solo Meeting Dec 3 2010</a></li>
                                          
                                          <li><a href="documents/Goal4WeaveReportNov2010.pdf">Goal 4 Weave Report Nov 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>2nd Goal Team 4 Meeting</strong></p>
                                       
                                       <p>Date: January 26, 2011 - 9:00AM to 12:00PM</p>
                                       
                                       <p>Location: CJI 148</p>
                                       
                                       <p><strong>Agenda &amp; Documents </strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam4AgendaSecondSoloMeetingJan262011.doc">CPC Goal Team 4 Agenda Second Solo Meeting Jan 26 2011</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam4MinutesofFirstSoloMeetingDec12010.doc">CPC Goal Team 4 Minutes of First Solo Meeting Dec 1 2010</a></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><strong>3rd Goal Team 4 Meeting</strong></p>
                                       
                                       <p>Date: March 1, 2011 - 9:00AM to 12:00PM</p>
                                       
                                       <p>Location: CJI 109</p>
                                       
                                       <p><strong>Agenda &amp; Documents</strong></p>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CPCGoalTeam4AgendaThirdSoloMeetingMarch12011.doc">CPC Goal Team 4 Agenda Third Solo Meeting March 1 2011</a></li>
                                          
                                          <li><a href="documents/CPCGoalTeam4MinutesSecondMeetingJan262011.doc">CPC Goal Team 4 Minutes Second Meeting Jan 26 2011    </a></li>
                                          
                                       </ul>
                                       
                                       <p><a href="#top">TOP</a></p>
                                       
                                       
                                       
                                       <h3>
                                          <strong>Goal Teams </strong><strong>Roles and Responsibilities</strong>
                                          
                                       </h3>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Role</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Responsibility</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Convener</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Schedules Goal Team Meetings with members (help with room reservations is available
                                                      through CPC staff); prepares agenda
                                                   </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Recorder</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Records minutes of meetings and distributes to group and CPC staff</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Reporter</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Reports group progress, findings, etc. to CPC as needed</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Seekers</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Actively seeks information to answer group questions or needs (could be two or three
                                                      people in this role)
                                                   </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Questioners</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Encourages group to explore multiple scenarios and ideas when addressing data sets
                                                      and assumptions(could be two or three people in this role)
                                                   </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Principles Monitor</p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p>Monitors the group’s adherence to design principles and helps the group through tough
                                                      conversations by focusing on principles
                                                   </p>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>                        
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>College Planning Council</h3>
                              
                              <div>
                                 
                                 
                                 <h3>College Planning Council</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The <strong>College Planning Council (CPC)</strong> is one of Valencia’s four governing councils. Among the Council’s responsibilities
                                          are designing and conducting 1) a collaborative strategic planning process, and 2)
                                          a collaborative budgeting process that links the annual budget to the strategic plan.&nbsp;
                                          The Strategic Plan for 2008-13 was approved by the District Board of Trustees on June
                                          17, 2008. On September 9, 2011, the Board approved extending the <a href="documents/StrategicPlan2008-15RevisedSep92011.pdf">Strategic Plan</a> until 2015. You can access the summary Strategic Plan brochure <a href="../documents/StrategicPlanBrochure.pdf.html">here</a>. 
                                       </p>
                                       
                                       <p>The Council works closely with and consults the Senior Leadership Team as it designs
                                          and conducts these processes. The CPC created Task Forces to carry out the phases
                                          of the planning process, and the charges, agendas, and minutes of those task forces
                                          may be accessed through the strategic planning web page. Volunteers were recruited
                                          collegewide for the various task forces. The task forces include: Vision, Values,
                                          and Mission; Data and Situational/Needs Analysis; Communications; Evaluation; and
                                          Strategies, Goals, and Objectives.
                                       </p>
                                       
                                       <p> CPC is committed to a collaborative process with many opportunities for involvement
                                          of those within the college and in the community that we serve. Throughout the planning
                                          cycle, we will schedule big and small group meetings, community meetings, consultations
                                          with key constituencies, and discussions by the governing councils and the senior
                                          leadership team, all of which combine to enable broad based participation in the various
                                          phases of creating, updating, and evaluating the Strategic Plan.
                                       </p>
                                       
                                       <p>During the 2012-13 period, the Council is scheduled to meet on September 27, October
                                          25, November 29, January 24, February 28, March 28, and April 25. 
                                       </p>
                                       
                                       <p>To request additional information or for questions about the College Planning Council,
                                          please contact its co-chairs: <a href="mailto:kewen@valenciacollege.edu">Kurt Ewen</a>, <a href="mailto:jfuhrman@valenciacollege.edu">Jean Marie Führman</a>, or <a href="mailto:sledlow@valenciacollege.edu">Susan Ledlow</a>. 
                                       </p>
                                       
                                       <h3>2012-13 Council Membership</h3>
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th"> 
                                                   <p>Member</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th"> 
                                                   <p>Title</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th"> 
                                                   <p>Represented</p>
                                                   
                                                   <p>Group</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th"> 
                                                   <p>Contact</p>
                                                   
                                                   <p>Information</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th"> 
                                                   <p>End of</p>
                                                   
                                                   <p>Term</p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Susan Ledlow</strong></p>
                                                   
                                                   <p><strong>(Chair)</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>VP Academic Affairs &amp; Planning</strong></div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>VP Academic Affairs &amp; Planning</strong></div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Downtown Center, DTC-4, x3423</strong></div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>N/A</strong></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Kurt Ewen</strong></p>
                                                   
                                                   <p><strong>(Co-Chair)</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>AVP Assessment and Institutional Effectiveness</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Institutional Research</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-12, x1611 </strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Jean Marie Führman</strong></p>
                                                   
                                                   <p><strong>(Co-Chair)</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Reading</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Faculty</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Winter Park Campus, 5-3, x6865</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Vivian Calderon</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Administrative Assistant</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Career Staff</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Osceola Campus, 6-3, x4862</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Donna Deitrick</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Staff Assistant I</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Career Staff</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-1, x5473</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Regina Falconer</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Biology</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Faculty</strong></p>
                                                   
                                                   <p><strong>East Campus</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>East Campus, 3-23, x2025</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Damion Hammock</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Mathematics</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Faculty</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Winter Park Campus, 5-3, x6917</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Deidre Holmes DuBois</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Speech</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Faculty Council,</strong></p>
                                                   
                                                   <p><strong>Vice President</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Osceola Campus, 6-3, x4891 </strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Keith Houck</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>VP Operations &amp; Finance</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Chief Financial Officer</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Downtown Center, DTC-3, x3300</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Dale Husbands</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Dean, Business Information Technology</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Academic Deans</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Osceola Campus, 6-8, x4841</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Stacey Johnson</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Campus President</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>East &amp; Winter Park Campus President</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>East, 3-1, x2822/Winter Park, 5-1, x6805</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Amy Kleeman</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>AVP College Transition</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Student Affairs Representative</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-25, x1238</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Kevin Matis</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Network Server Specialist</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professional Staff</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-12, x1775</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Rob McCaffrey</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Graphics Technology</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Faculty Council,</strong></p>
                                                   
                                                   <p><strong>President</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>East Campus, 3-2, x2784</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>John McFarland</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, ESL</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Faculty</strong></p>
                                                   
                                                   <p><strong>Osceola Campus</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Osceola Campus, 6-2, x4125</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Kathleen Plinske</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Campus President</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Osceola &amp; Lake Nona</strong></p>
                                                   
                                                   <p><strong>Campus President</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Osceola Campus, 6-2, x4100</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Jerry Reed</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Programmer Analyst</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professional Staff </strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-12, x5583</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2013</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Joyce Romano</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>VP Student Affairs</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>VP Student Affairs</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Downtown Center, DTC-4, x3402</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Deborah Simko</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Professor, Nursing</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Faculty</strong></p>
                                                   
                                                   <p><strong>West Campus</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-14, x1739</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>David Sutton</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Dean, Humanities &amp; Foreign Language</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Academic Deans</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>East Campus, 3-2, x2753</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>04/2014</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Bill White</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Chief Information Officer</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Chief Information Officer</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-12, x1185</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Falecia Williams</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Campus President</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>West Campus</strong></p>
                                                   
                                                   <p><strong>Campus President</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>West Campus, 4-1, x1235</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>N/A</strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <p><a href="#top">TOP</a></p>
                                       
                                       <h3>College Planning Council Staff</h3>
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   
                                                   <p>Member</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   
                                                   <p>Title</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   
                                                   <p>Represented</p>
                                                   
                                                   <p>Group</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   
                                                   <p>Contact</p>
                                                   
                                                   <p>Information</p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   
                                                   <p>End of</p>
                                                   
                                                   <p>Term</p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Alys Arceneaux</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Research Analyst</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Institutional Research</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>West Campus, 4-12, x1611</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A</p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Sherri Dixon</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>AVP Budget &amp; Auxiliary Services</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Financial Services</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Downtown Center, DTC-3, x3306</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Sonya Joseph</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>AVP Student Affairs</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Student Affairs </p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Osceola Campus, 6-8, x4997</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Jackie Lasch</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>AVP Financial Services</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Financial Services</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Downtown Center, DTC-3, x3302</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Helene Loiselle</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>AVP Facilities &amp; Sustainability</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Facilities</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>West Campus, 4-21, x1707</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A </p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="th">
                                                   <p>Noelia Maldonado</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Executive Assistant</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Strategic Planning</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>Downtown Center,  DTC-4, x3487</p>
                                                </div>
                                                
                                                <div data-old-tag="th">
                                                   <p>N/A</p>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>            
                                       
                                       
                                       
                                       <h3><strong>College Planning Council Charge</strong></h3>
                                       
                                       <ul>
                                          
                                          <li> Coordinate the development and revision of the Strategic Learning Plan</li>
                                          
                                          <li>Coordinate the development of the annual budget</li>
                                          
                                          <li>Recommend multi-year budget and staffing plans</li>
                                          
                                          <li>Oversee the measurement of institutional effectiveness</li>
                                          
                                          <li>Function as Steering Committee for institutional accreditation by the Southern Association
                                             of Colleges and Schools (SACS)
                                          </li>
                                          
                                          <li>Oversee the College's Diversity Plan</li>
                                          
                                       </ul>            
                                       
                                       
                                       <h3><strong>Design Principles for the Strategic Planning Process</strong></h3>
                                       
                                       <p><br>
                                          The College Planning Council established the following principles to guide the strategic
                                          planning work:<br>
                                          (These are numbered for ease of reference, but are not listed in any priority order.)
                                       </p>
                                       
                                       <ol>
                                          
                                          <li>The planning process and the plan that it yields will be learning-centered, will be
                                             grounded in the College’s history of excellence, innovation, and community, and will
                                             support the quality and aspiration that bequeath the College with its distinctive
                                             place in higher education.<br>
                                             
                                          </li>
                                          
                                          <li>The process will be strategic by impacting the results the college aims to provide
                                             to society and to students as they progress in their programs of learning.<br>
                                             
                                          </li>
                                          
                                          <li>The planning process will be collaborative by operating within our shared governance
                                             structure that ensures broad-based participation and by providing a means for stakeholder
                                             groups to be heard and to influence the plan. <br>
                                             
                                          </li>
                                          
                                          <li>The process will build trust through effective communication and negotiation, by making
                                             it safe to identify and challenge assumptions, and by supporting agreements on shared
                                             values and the making of mutual commitments that are the basis for the strategic plan,
                                             and that are honored as the plan is implemented.<br>
                                             
                                          </li>
                                          
                                          <li>The process will be meaningful in that it will help the College to establish a vision
                                             of the future that shapes, defines, and gives meaning to its strategic purpose, and
                                             in that it will help to shape strategic decisions, some of which are identified in
                                             advance.<br>
                                             
                                          </li>
                                          
                                          <li>The process will be data-driven, using qualitative and quantitative data, routinely
                                             reviewed as the plan is implemented, with the aim of continuous improvement.<br>
                                             
                                          </li>
                                          
                                          <li>The plan will include formative and summative evaluation components that evaluate
                                             the planning process itself, as well as the implementation of the plan, using agreed
                                             upon performance indicators.<br>
                                             
                                          </li>
                                          
                                          <li>The process will have a clear cycle of activities, with a beginning and an end, and
                                             timed and structured to coordinate well with SACS accreditation requirements.<br>
                                             
                                          </li>
                                          
                                          <li>The process will be as simple as possible while yielding a viable plan, avoiding the
                                             trap of imposing more order than the College can tolerate, and integrating planning
                                             into permanent governing structures and collegewide meetings, rather than creating
                                             a separate set of activities removed from the governance and life of the College.<br>
                                             
                                          </li>
                                          
                                          <li>The process will support the integration of fiscal, learning, and facilities plans
                                             with the strategic plan of the college, through careful timing and by clearly connecting
                                             each of these plans to the College’s revised Vision, Mission, and Values.<br>
                                             
                                          </li>
                                          
                                          <li>The strategic plan will be useful to and therefore used by councils, campuses and
                                             departments as they prepare their plans, and will encourage a future orientation to
                                             their work.<br>
                                             
                                          </li>
                                          
                                          <li>The process, its language, its products, and the results of the plan will be communicated
                                             to all employees internally.<br>
                                             
                                          </li>
                                          
                                          <li>The plan will be expressed clearly, with language that is understood by stakeholders
                                             and with clear means of measuring progress.<br>
                                             
                                          </li>
                                          
                                          <li>The process will be truly comprehensive, and will have clearly assigned roles for
                                             individuals and groups, including students.
                                          </li>
                                          
                                       </ol>
                                       
                                       
                                       <h3><strong>Work Products of the Planning Process</strong></h3>
                                       
                                       <p>The College Planning Council has named the following as the products of the planning
                                          process:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>needs assessment/situational analysis/environmental scan, providing a common understanding
                                             of the present and the anticipated future, including information about our competitors
                                             and clearly defined gaps in results at the mega, macro, and micro levels, as defined
                                             by Roger Kaufman’s planning model
                                          </li>
                                          
                                          <li>reviewed/revised mission (the role we will play), vision, and values statements</li>
                                          
                                          <li>a set of college strategies (the ways in which we will play our role and get results),
                                             goals (what results we want to accomplish within our role), measurable outcomes objectives
                                             (how much will we change specific results at the mega, macro, and micro levels as
                                             used in Kaufman’s model), and related activities to achieve the objectives within
                                             an agreed upon timeframe
                                          </li>
                                          
                                          <li>an evaluation plan, including indicators/measures of institutional effectiveness,
                                             revised as needed, and a means of assessing the extent to which College decisions
                                             are consistent with the plan
                                          </li>
                                          
                                          <li>a recommended assignment of responsibilities for objectives to the governing councils</li>
                                          
                                          <li>list of major decisions to be impacted by the plan, which could include decisions
                                             such as:<br>
                                             
                                             <ul>
                                                
                                                <li>academic program plans for new campus(es) and evolution of programs on existing campuses</li>
                                                
                                                <li>the goals in the College enrollment plan</li>
                                                
                                                <li>Future Valencia Foundation fund raising goals</li>
                                                
                                                <li>Focus of the Quality Enhancement Plan for SACS in 2010-2012</li>
                                                
                                                <li>Professional development multi-year plan</li>
                                                
                                                <li>Multi-year financial plan and annual budgets</li>
                                                
                                                <li>Major external funding requests with collegewide impact, such as Title III and Title
                                                   V
                                                </li>
                                                
                                                <li>Community relations priorities and programs</li>
                                                
                                                <li>Efforts designed to support student learning and to maintain academic excellence</li>
                                                
                                                <li>Strategic facilities plans</li>
                                                
                                                <li>Final proposed planning document for submission to the president and trustee</li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <p>For more information, visit the <a href="../../../lci/StrategicPlan0813.html">Learning-Centered Initiative</a> site.
                                       </p>
                                       
                                       <p>For more information about the College Budget, which is the fiscal expression of our
                                          plans, visit the <a href="../../../about/budget/index.html">Budget Process</a> site.
                                       </p>
                                       
                                       <p>View the Council's <a href="../minutes.html#council">meeting agendas and minutes</a>.
                                       </p>
                                       
                                       <p><a href="#top">TOP</a></p>
                                       
                                       <h2>&nbsp;</h2>
                                       
                                       <h3>Organizational Overview</h3>
                                       
                                       <a href="documents/org_chart.ppt" title="View the Organizational Chart for Strategic Planning in PowerPoint format"><img alt="" border="0" height="301" src="strat_plan_org.png" width="582"></a>
                                       
                                       <p>The College Planning Council will work closely with and consult the Senior Leadership
                                          Team as it designs and conducts the planning process. The CPC will charge Task Forces
                                          with designing and carrying out the phases of the process, ensuring that decisions
                                          are made in a collaborative fashion, using Big and Small Group meetings, consulting
                                          with key constituencies, and working through the governing councils and the Senior
                                          Leadership Team.
                                       </p> 
                                       
                                       
                                       <p>The Planning Steering Committee will be composed of the President (1), the council
                                          co-chairs (6), a Trustee (1), and a representative of Career Service and Professional
                                          staff (2). The Steering Committee will make final recommenda-
                                          tions regarding strategy and the content of the plan.
                                       </p>
                                       
                                       <p>The full Committee will include 20 additional members: 9 senior staff not serving
                                          as co-chairs, 2 deans;, 3 additional faculty, 3 additional professional staff, and
                                          3 additional career service staff. This group of 30 will review work products, note
                                          any additional information or consultation needed, and make needed adjustments  to
                                          ensure a clear, consistent, and logical document.
                                       </p>
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <h3>Enrollment Planning</h3>
                              
                              <div>
                                 
                                 
                                 <h3>Enrollment Planning</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Enrollment Planning</h2>
                                       
                                       <p><strong>TABLE 
                                             OF CONTENTS</strong></p>
                                       
                                       <p>I.&nbsp;&nbsp; 
                                          &nbsp;&nbsp;&nbsp; <a href="#Summary">Executive Summary</a></p>
                                       
                                       <p>II.&nbsp;&nbsp; 
                                          &nbsp;&nbsp; <a href="#Relationship"> Relationship to Strategic 
                                             Planning Process</a></p>
                                       
                                       <p>III.&nbsp; 
                                          &nbsp;&nbsp; <a href="#Issues">Vital Issues</a></p>
                                       
                                       <p>IV.&nbsp;&nbsp;&nbsp; <a href="#Response">Response to Vital Issues</a></p>
                                       
                                       <p>V. 
                                          &nbsp; &nbsp; <a href="#Scheduling">Precision Schedule Development</a></p>
                                       
                                       <p>VI.&nbsp;&nbsp;&nbsp; <a href="#Scanning">Environmental Scan/Data</a></p>
                                       
                                       <p>VII. 
                                          &nbsp; <a href="#Analysis">Data Analysis</a></p>
                                       
                                       <p>VIII.&nbsp; <a href="#Goals">Goals</a></p>
                                       
                                       <p>IX.&nbsp; 
                                          &nbsp; <a href="#Indicators">Key Performance Indicators</a></p>
                                       
                                       <p>X.&nbsp;&nbsp; 
                                          &nbsp; <a href="#Strategies">Strategies</a></p>
                                       
                                       <p>XI.&nbsp; 
                                          &nbsp;&nbsp; <a href="#Accountability">Accountability - Stewardship</a></p>
                                       
                                       <p>XII. 
                                          &nbsp;&nbsp; <a href="#LearningPlan">Enrollment Management Learning 
                                             Plan</a></p>
                                       
                                       <p>XIII. 
                                          &nbsp; <a href="#ResearchAgenda">Enrollment Management Research 
                                             Agenda</a></p>
                                       
                                       
                                       
                                       <p><font color="#990000"><strong><span>I. 
                                                   &nbsp; EXECUTIVE SUMMARY </span></strong></font></p>
                                       
                                       
                                       <p>At 
                                          Valencia College we believe that enrollment growth can 
                                          be strategically influenced by internal actions when these actions 
                                          are tightly coupled to our strategic enrollment management plan. 
                                          The strategic enrollment management plan allows us to intentionally 
                                          use marketing, scheduling, staffing, budgeting, etc. in a strategic 
                                          manner to enable us to meet our enrollment targets.&nbsp; 
                                       </p>
                                       
                                       <p>The 
                                          Strategic Enrollment Planning process enables Valencia faculty and 
                                          staff to create action plans that turn our vision and concepts for 
                                          the future into reality. &nbsp;Through the planning process we can: 
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>make clearer 
                                             choices about growth. 
                                          </li>
                                          
                                          <li>define clear 
                                             lines of responsibility for different aspects of the growth process. 
                                          </li>
                                          
                                          <li>have a more 
                                             precise and clear budgeting process that "triggers" in real time. 
                                          </li>
                                          
                                          <li>connect the 
                                             budget, scheduling, staffing, and marketing processes. 
                                          </li>
                                          
                                          <li>have clear 
                                             objectives for each "scheduler". 
                                          </li>
                                          
                                          <li>develop long-range 
                                             planning for programs, space, etc. 
                                          </li>
                                          
                                          <li>develop multi-year 
                                             financial models. 
                                          </li>
                                          
                                          <li>move to a 
                                             precision scheduling model. 
                                          </li>
                                          
                                          <li>put all resources 
                                             to work. 
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><strong><span>II.&nbsp; <font>RELATIONSHIP TO STRATEGIC PLANNING PROCESS</font></span></strong></p>
                                       
                                       
                                       <p><font>The 
                                             following Guiding Principles for Enrollment Planning have been established 
                                             at Valencia College. These Guiding Principles are fully 
                                             aligned with the Valencia College Mission and Values <a href="../../../about-valencia/vision.html">Vision, Mission, Values and Statutory Purpose </a>, and the <a href="documents/StrategicPlanBrochure.pdf">Strategic
                                                Learning Plan</a>. </font></p>
                                       
                                       <p><font>The 
                                             Guiding Principles for Enrollment Planning at Valencia CommunityCollege 
                                             are:</font></p>
                                       
                                       <blockquote>&nbsp; </blockquote>
                                       
                                       <ul>
                                          
                                          <li><font>Accountability. </font></li>
                                          
                                          <li><font>Accessibility 
                                                (understanding that students want flexibility, many options, ability 
                                                to manage their cash with good financial stewardship). </font></li>
                                          
                                          <li><font>Inclusion. </font></li>
                                          
                                          <li><font>Dignity 
                                                and respect for all. </font></li>
                                          
                                          <li><font>Passion 
                                                for Excellence and Quality. </font></li>
                                          
                                          <li><font>Financial 
                                                Stewardship. </font></li>
                                          
                                          <li><font>Resource 
                                                Stewardship. </font></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><font>At 
                                             Valencia College , we believe that Enrollment Planning 
                                             is most effective when it includes the following best practices: </font></p>
                                       
                                       
                                       <ul>
                                          
                                          <li><font>Thoughtful 
                                                planning that is data driven (based on community needs, market 
                                                analysis,) and based on Valencia 's strategic priorities. </font></li>
                                          
                                          <li><font>Evidence 
                                                based on measurable outcomes. </font></li>
                                          
                                          <li><font>Student 
                                                and learning centered. </font></li>
                                          
                                          <li><font>Fully 
                                                uses all providers and modes of delivery. </font></li>
                                          
                                          <li><font>Maintains 
                                                standards of excellence. </font></li>
                                          
                                          <li><font>Monitors 
                                                and maintains satisfaction level of students (including timely 
                                                graduation, courses offered on-demand and through multiple delivery 
                                                modes). </font></li>
                                          
                                          <li><font>All 
                                                units are coordinated/connected to provide best service. </font></li>
                                          
                                          <li><font>All 
                                                employees are developed and understand their role in the process. </font></li>
                                          
                                          <li><font>Is 
                                                cost effective. </font></li>
                                          
                                          <li><font>Rewards 
                                                excellence in operation and results. </font></li>
                                          
                                          <li><font>Students 
                                                agree on the established principles. </font></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><strong><span>III.&nbsp; 
                                                VITAL ISSUES</span></strong></p>
                                       
                                       <p>The 
                                          purpose of Valencia's Strategic Enrollment Plan is to improve Student 
                                          Learning Performance by:
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p>A.&nbsp; 
                                             Reducing Chaos for students by implementing and 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp; constantly improving precision scheduling.</p>
                                          
                                          <p>B.&nbsp; 
                                             Increasing Investment in Student Learning by improving 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp; Valencia's financial performance.&nbsp; </p>
                                          
                                       </blockquote>
                                       
                                       <p>This 
                                          may be conceptualized as illustrated in the following charts.
                                       </p>
                                       
                                       <blockquote>
                                          
                                          
                                          <p><a href="EnrollmentManagementCharts.htm">Enrollment 
                                                Management Charts</a></p>
                                          
                                       </blockquote>
                                       
                                       
                                       
                                       <p><strong><span>IV.&nbsp; 
                                                RESPONSE TO VITAL ISSUES</span></strong></p>
                                       
                                       <p>When 
                                          looking at the vital issues identified above the following questions 
                                          must be asked:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>How can we 
                                             begin to produce and implement precision scheduling so that complexity 
                                             and unpredictability is reduced for students.
                                          </li>
                                          
                                          <li>How can we 
                                             increase the investment in Student Learning?
                                          </li>
                                          
                                          <li>How can we 
                                             improve Valencia's financial performance?
                                          </li>
                                          
                                       </ul>
                                       
                                       <p>This 
                                          (What? Answers to questions, response to issues?) can be accomplished 
                                          using several strategies.
                                       </p>
                                       
                                       <p>First, 
                                          we must look at the schedule as an operational plan.&nbsp; <font>The 
                                             schedule generates revenue which in turn is used for discretionary 
                                             resources. The schedule should be built based upon the best estimate 
                                             of student need to fulfill their educational plans. This estimate 
                                             of student need is used to assure that learning support (courses, 
                                             textbooks, software, technology labs, etc.) is aligned with student 
                                             need in a timely fashion. All stakeholders in the college have access 
                                             to the same enrollment and tuition estimates. All resources are 
                                             aligned with our strategic needs.</font></p>
                                       
                                       <p><font>Second, 
                                             we must connect the schedule and budget directly to the college-wide 
                                             learning plan. This would include sharing our best thinking with 
                                             students to impact their progress and success, creating new and 
                                             expanded systems that support better advising and counseling interactions, 
                                             expanding and strengthening the role of faculty as advisors and 
                                             mentors, and removing the challenges that exist in our processes 
                                             so that a student' s college experience becomes more predictable 
                                             and less stressed. </font></p>
                                       
                                       <p>The 
                                          objectives of our Precision Scheduling Process are to:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Fulfill the 
                                             enrollment plan.
                                          </li>
                                          
                                          <li>Meet student 
                                             needs.
                                          </li>
                                          
                                          <li>Minimize 
                                             the number of classes canceled and added.
                                          </li>
                                          
                                          <li>Guarantee 
                                             the course schedule.
                                          </li>
                                          
                                          <li>Plan on an 
                                             annual and multi-year basis.
                                          </li>
                                          
                                       </ul>
                                       
                                       <p>The 
                                          predicted and anticipated outcomes of the Precision Scheduling Process 
                                          are:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Mutually 
                                             agreed upon budgets for staffing both adjunct and full-time faculty.
                                          </li>
                                          
                                          <li>Planned and 
                                             precise resource management.
                                          </li>
                                          
                                          <li>Effective 
                                             and efficient staffing.
                                          </li>
                                          
                                          <li>Student average 
                                             course loads increased.
                                          </li>
                                          
                                          <li>Improved 
                                             services to students.
                                          </li>
                                          
                                       </ul>
                                       
                                       <p>The 
                                          long term impact of enrollment planning and precision scheduling 
                                          for Valencia College should be:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Earlier counseling 
                                             and advising for students.
                                          </li>
                                          
                                          <li>The ability 
                                             of students to plan beyond one term.
                                          </li>
                                          
                                          <li>A strong 
                                             basis for faculty involvement in advising
                                          </li>
                                          
                                          <li>Program continuity.</li>
                                          
                                          <li>Less complexity 
                                             and unpredictability equals more learning.
                                          </li>
                                          
                                          <li>More students 
                                             achieve their stated goals.
                                          </li>
                                          
                                          <li>Increased 
                                             number of graduates.
                                          </li>
                                          
                                          <li>Increased 
                                             amount of time and effort for administrators to spend on other 
                                             leadership and management responsibilities.&nbsp;&nbsp;
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><strong><span>V.&nbsp; 
                                                PRECISION SCHEDULE DEVELOPMENT </span></strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>1.&nbsp; 
                                             Begin with enrollment planning at each level of the organization
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>2.&nbsp; 
                                             Develop schedule to meet established enrollment goals
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>3.&nbsp; 
                                             Reaffirm recent enrollment trends of prospective new returning 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp; students</p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>4.&nbsp; 
                                             Analyze strategic growth in specific program areas
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>5.&nbsp; 
                                             Estimate enrollments for each course; number of sections needed
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>6.&nbsp; 
                                             Review schedule based on Student Preferences Term Schedule 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp; Checklist</p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>7.&nbsp; 
                                             Compile by course, departments, division, college
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>8.&nbsp; 
                                             Review and adjust in cross department teams, including student 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp; services;  apply patterns that are student friendly</p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>9.&nbsp; 
                                             Confirm total student demand and college commitment
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>10.&nbsp; 
                                             Build a revenue model
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; a.&nbsp; Estimate revenue from enrollment above
                                          </p>
                                          
                                          <p>&nbsp; 
                                             &nbsp;&nbsp; &nbsp; b.&nbsp; Modify for residency, exemptions, 
                                             etc.
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; c.&nbsp; Create reserve/response fund as appropriate
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p>11.&nbsp; 
                                             Build cost model
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; a.&nbsp; Estimate in every discipline and department the 
                                             in-load,&nbsp;&nbsp;&nbsp;
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             overload,&nbsp; adjunct distribution to the schedule
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; b.&nbsp; Adjust for retirements, new-hires, etc.
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; c.&nbsp; Adjust for reassignments &nbsp; 
                                          </p>
                                          
                                          <p>&nbsp; 
                                             &nbsp;&nbsp;&nbsp;&nbsp; d.&nbsp; Build a faculty assignment database 
                                             for the year and assign 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             costs to  each assignment
                                          </p>
                                          
                                          <p> 12.&nbsp; Build a schedule management system</p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; a.&nbsp; Identify "reserve" sections that will 
                                             not be initially published 
                                          </p>
                                          
                                          <p>&nbsp; 
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (about 
                                             3-5&nbsp; of most populous courses)
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; b.&nbsp; Determine the decision process and timing for 
                                             opening these 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             sections in  response to demand
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; c.&nbsp; In early stages, agree on decision rules for canceling 
                                             sections
                                          </p>
                                          
                                          <p>&nbsp;&nbsp;&nbsp;&nbsp; 
                                             &nbsp; d.&nbsp; Create accountability reports for every department 
                                             to track 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             effectiveness of scheduling 
                                             (cancellations, additions, 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             class size, student drop/add rates, faculty 
                                             feedback on first 
                                          </p>
                                          
                                          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                             week, student services feedback)
                                          </p>
                                          
                                          
                                       </blockquote>
                                       
                                       
                                       <p><strong><span>VI.&nbsp; 
                                                Environmental Scan/Data</span></strong></p>
                                       
                                       <p><font>The 
                                             mission of Valencia College is as follows: </font></p>
                                       
                                       <p><font><em>Valencia 
                                                Community College provides outcomes-oriented, quality learning opportunities 
                                                by: </em></font></p>
                                       
                                       <ul>
                                          
                                          <li><font><em>Achieving, 
                                                   measuring, and applying the results of learning. </em></font></li>
                                          
                                          <li><font><em>Emphasizing 
                                                   critical and creative thinking, effective communication, collaboration, 
                                                   and workplace skills. </em></font></li>
                                          
                                          <li><font> <em>Maintaining an open-minded, nurturing, and collaborative environment. </em></font></li>
                                          
                                          <li><font><em>Reaching 
                                                   out to potential students and providing affordable, accessible 
                                                   learning opportunities. </em></font></li>
                                          
                                          <li><font><em>Fostering 
                                                   enthusiasm for lifelong learning. </em></font></li>
                                          
                                          <li><font><em>Motivating 
                                                   learners to define and achieve their goals. </em></font></li>
                                          
                                          <li><font><em>Respecting 
                                                   uniqueness and appreciating diversity. </em></font></li>
                                          
                                          <li><font><em>Encouraging 
                                                   faculty and staff to continue professional growth. </em></font></li>
                                          
                                          <li><em>Partnering 
                                                with businesses, industries, public agencies, civic groups, and 
                                                educational institutions that support learning and promote the 
                                                economic development of Central Florida. </em></li>
                                          
                                       </ul>
                                       
                                       <p><font>In 
                                             order to develop a strategic enrollment plan that meets the goals 
                                             outlined in our mission statement, we must first understand the 
                                             changes that are occurring in our world and for the students we 
                                             serve. There are many variables that have changed the higher education 
                                             landscape during the past fifteen years. Some of the variables that 
                                             changed are: </font></p>
                                       
                                       <p><strong>Changing 
                                             demographics and uneven population growth</strong>. Students seeking 
                                          access to higher --education are more diverse than previous generations 
                                          of students in terms of many characteristics. A higher proportion 
                                          of students: 
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>are from 
                                             minority racial or ethnic backgrounds, 
                                          </li>
                                          
                                          <li>are from 
                                             geographically dispersed areas of the region and world, 
                                          </li>
                                          
                                          <li>have learning 
                                             and physical challenges, 
                                          </li>
                                          
                                          <li>come from 
                                             lower socioeconomic levels, 
                                          </li>
                                          
                                          <li>demonstrate 
                                             limited academic preparation to do college work, 
                                          </li>
                                          
                                          <li>come from 
                                             homes where English is not the first language, 
                                          </li>
                                          
                                          <li>work part-time 
                                             or full-time, 
                                          </li>
                                          
                                          <li>have families 
                                             to support. 
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><strong>Rise 
                                             in need for college education and workforce training - <br>
                                             </strong>Most jobs today demand some college education. 
                                       </p>
                                       
                                       
                                       <p><strong>Globalization</strong> - Increasingly, technology makes it possible for potential students 
                                          from around the world to seek to enroll at Valencia for on site 
                                          or distance learning opportunities. The curricula in which students 
                                          enroll must reflect global concerns, particularly as employers look 
                                          for employees with an appreciation of international and inter-cultural 
                                          issues. 
                                       </p>
                                       
                                       
                                       <p><strong>New 
                                             market forces -&nbsp; </strong>Private, for profit universities, 
                                          and corporate universities are eagerly seeking students whose main 
                                          option to attend college in the past was the "traditional" college. 
                                       </p>
                                       
                                       <p><strong>Increasing 
                                             expectations from students and parents</strong> -&nbsp;Students 
                                          expect colleges to be student centered and responsive to their needs.&nbsp; 
                                          They expect good and convenient service when they want the service, 
                                          in the manner they want it and personalized to meet their particular 
                                          needs.&nbsp; That is why distance education programs and on-line 
                                          degrees programs have grown dramatically during the past ten years. 
                                          This need will continue to grow as students continue to request 
                                          "college on demand," classes when they want them and in the learning 
                                          modality in which they want to learn. &nbsp;Students, and their 
                                          parents, also want academic and co-curricular programs that are 
                                          relevant and timely to them.&nbsp; 
                                       </p>
                                       
                                       
                                       <p><strong>Federal, 
                                             state, and local accountability - </strong>Governments and businesses 
                                          want students to reach their educational objectives in a timely 
                                          manner and with knowledge and skills relevant to today's market 
                                          needs. 
                                       </p>
                                       
                                       
                                       <p><strong>Changing 
                                             funding models - </strong>Federal and state funding streams will 
                                          increasingly be tied to performance, directly through fund set aside 
                                          for that purpose, or less directly as legislative bodies weigh performance 
                                          in allocating funds to the higher education sector. At the same 
                                          time, the College will compete for increasingly scarce resources 
                                          for its operational and capital needs. Performance in terms of meeting 
                                          our mission by serving students will be essential if we are to meet 
                                          our organization's fiscal needs. 
                                       </p>
                                       
                                       
                                       <p><strong>Changing 
                                             enrollment patterns </strong>-&nbsp;There are growing numbers of 
                                          students who want access to college who are different from the "traditional" 
                                          college student of the past.&nbsp; There are more non-traditional 
                                          students, more re-entry, more adult learners, and more career-oriented 
                                          students<strong>.&nbsp;&nbsp;[</strong>Environmental scanning data 
                                          that addressed these issues was collected and analyzed in the Development 
                                          of the Valencia College Enrollment Plan. The Chief Learning 
                                          Officer, Provosts, Deans, faculty leaders, assistant vice presidents, 
                                          institutional research staff, and others were involved in the analysis.
                                       </p>
                                       
                                       <p><font><strong><em>Enrollment 
                                                   Data </em></strong>- Current enrollment conditions were examined. 
                                             Market conditions were examined to determine where new programs 
                                             need to be started and/or old programs expanded, where market penetration 
                                             needs to be adjusted, and where the student profile needs to be 
                                             adjusted.</font></p>
                                       
                                       <p><font><strong><em>Course 
                                                   offerings </em> ( </strong>capacity, scheduling, duplication, wait 
                                             lists) - Course offering information was analyzed to identify unused 
                                             capacity (opportunities for growth), to identify unmet demand (where 
                                             we need to invest resources), and to pinpoint areas where resources 
                                             need to be recovered (programs that have reached a plateau and no 
                                             longer meet community/student needs).</font></p>
                                       
                                       <p><font><strong><em>Retention 
                                                   Information </em></strong>- Retention information was analyzed to 
                                             determine where and why students were dropping out and/or stopping 
                                             out. </font></p>
                                       
                                       <div>
                                          
                                          <p><font><strong><em>Market 
                                                      surveys </em></strong>- Market surveys were analyzed to determine 
                                                unmet student and community needs. </font></p>
                                          
                                          <p><font><strong><em>Financial 
                                                      aid and scholarships </em>- </strong>Annual reports were analyzed 
                                                for distribution of types of aid and characteristics of students 
                                                on aid to identify areas to leverage financial aid programs to 
                                                targeted groups in order to increase access and enrollment. </font></p>
                                          
                                          <p><font><strong><em>Financial 
                                                      Information </em></strong>(income streams and expenditures) - 
                                                Financial data were data was analyzed to determine both revenue 
                                                and expenditures needed to support enrollment growth. The budget 
                                                was developed so that it meets the best estimate of student needs. </font></p>
                                          
                                          <p><font><strong><em>Dual 
                                                      Enrollment </em>- </strong>Trends in enrollment by high school 
                                                were analyzed to identify areas for expansion of enrollments at 
                                                the high school campuses and on Valencia campuses. </font></p>
                                          
                                          <p><strong>Data 
                                                and Situational/Needs Analysis - </strong>A task force of the 
                                             College Planning Council analyzed over 60 sets of data about the 
                                             College and the community in September - October 2006, and shared 
                                             seven key points that will shape the College's future based on 
                                             their review. The key points are: 
                                          </p>
                                          
                                          <p>1. 
                                             &nbsp;&nbsp;Demand for higher education will grow in Central Florida 
                                             due to continuing population increases, changes in the employment 
                                             market, and workforce vacancies as baby boomers retire. While 
                                             high school graduates will increase, the proportion earning a 
                                             standard diploma will decline. University admissions limits will 
                                             increase the number of students starting at community colleges. 
                                          </p>
                                          
                                          <p>2. 
                                             &nbsp;&nbsp;The students in our future will be increasingly diverse 
                                             in background and needs . Younger students will prefer non-traditional 
                                             methods of learning. Prospective students over age 44 (a group 
                                             that will increase at a higher rate than will the younger population) 
                                             will be interested in career changes and growth, weighing the 
                                             investment of their time in education as a cost. 
                                          </p>
                                          
                                          <p>3. 
                                             &nbsp;&nbsp;Educational options available to students will continue 
                                             to evolve. Private institutions will increase in number and enrollments, 
                                             and financial aid policies and availability will make it possible 
                                             for students to attend higher cost private institutions if they 
                                             choose. 
                                          </p>
                                          
                                          <p>4. 
                                             &nbsp;&nbsp;Working to improve learning results, and to document 
                                             those improvements, will continue to challenge the College and 
                                             our students. The College can expect increased community interest 
                                             in how it can partner to increase high school graduation rates 
                                             and improve college readiness. As students move on to their first 
                                             experiences at Valencia , a large number will struggle, and we 
                                             will need more information about why this is the case. As learning 
                                             technologies evolve, the College will need more information about 
                                             which students can be successful with different learning modalities, 
                                             such as web-based courses. 
                                          </p>
                                          
                                          <p>5. 
                                             &nbsp;&nbsp;The community's needs and related expectations are 
                                             changing. More employees will be required who readily learn and 
                                             adapt to new technologies, who work effectively and serve people 
                                             from other cultures, and who contribute to solving societal and 
                                             global problems. Employment in the biological sciences, health 
                                             care, high-technology fields, business, construction, hospitality, 
                                             and teaching will experience highest local demand. Cries for public 
                                             accountability will intensify, adding to the need to collect, 
                                             analyze and report to the public, and to improve our assessment 
                                             of learning. 
                                          </p>
                                          
                                          <p>6. 
                                             &nbsp;&nbsp;Valencia 's costs of doing business will continue 
                                             to rise, and so must our revenues.&nbsp; Our 
                                             needs will exceed available State funds, meaning that we must 
                                             continue to seek alternative revenue through gifts, grants, and 
                                             revenue-generating activities. Significant investments in land, 
                                             new facilities, renovations, and technologies will be required 
                                             in a marketplace in which scarcity of many resources will drive 
                                             up costs. Competition for qualified personnel and the need for 
                                             the development of new leaders will intensify as baby boomers 
                                             retire. 
                                          </p>
                                          
                                          <p> 7. &nbsp;&nbsp;Defining community (which is, in fact, our middle 
                                             name) is increasingly complicated and increasingly important. 
                                             Just as the members of our immediate families are less likely 
                                             to all live in one geographic location, it is increasingly difficult 
                                             to pinpoint geographically the "community" that we serve or could 
                                             serve, both due to technology opening up distance learning options, 
                                             and due to growing numbers of people world-wide looking to Orlando 
                                             as a place to come for higher education. Valencia will be expected 
                                             to contribute to solving problems, both natural and human-made, 
                                             that have no geographic boundaries, such as worldwide health crises, 
                                             disaster recovery, or the prevention of violence. What we do here 
                                             and now as we plan for 2008-13 will make a difference to our community, 
                                             no matter how broadly or narrowly we define it. 
                                          </p>
                                          
                                          
                                          
                                          <p><strong><span>VII.&nbsp; 
                                                   DATA ANALYSIS</span></strong></p>
                                          
                                          <p><font>All 
                                                decisions concerning enrollment are made after careful data analysis. 
                                                Five important sets of data analysis occur: Trend data analysis, 
                                                Schedule analysis, Enrollment data analysis, Financial analysis, 
                                                and Budge process analysis. </font></p>
                                          
                                          
                                          <p><strong><font>A. 
                                                   &nbsp;&nbsp;Trend Data Analysis: </font></strong><font>Valencia 
                                                's analysis of trend data begins with two very important questions: </font></p>
                                          
                                          <p><font>What 
                                                are the business and industry needs for the community Valencia 
                                                serves? </font></p>
                                          
                                          <p><font>How 
                                                are we responding to business and industry needs? </font></p>
                                          
                                          
                                          <p><font>In 
                                                order to answer these questions, provosts, deans, and program 
                                                directors review the following data: </font></p>
                                          
                                       </div>
                                       
                                       <blockquote>
                                          
                                          <div>
                                             
                                             <p>•&nbsp; <font>U.S. Occupational Employment for 
                                                   Occupations Requiring </font></p>
                                             
                                             <p>&nbsp;&nbsp;&nbsp; <font>Postsecondary 
                                                   Training or Associate's Degree Annual Projections </font></p>
                                             
                                             <p><font> &nbsp;&nbsp;&nbsp; (currently 2004 to 2014)</font></p>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <p>•&nbsp; <font>Region 12 Occupational Employment 
                                                   Projections </font></p>
                                             
                                          </div>
                                          
                                          <div>•&nbsp; <font>Targeted Occupations List - Number 
                                                of Annual Openings in Region </font>
                                             
                                          </div>
                                          
                                          <div>•&nbsp; <font>Job Growth By Occupational Group </font>
                                             
                                          </div>
                                          
                                          <div>•&nbsp; <font>Occupation Linked to Program (Dept 
                                                of Ed) </font>
                                             
                                          </div>
                                          
                                          <div>•&nbsp; <font>Wage Earning for Occupation: </font>
                                             
                                          </div>
                                          
                                          <blockquote>
                                             
                                             <div><font>Targeted 
                                                   Occupation - Mean Wage - $11.39/Hour </font></div>
                                             
                                             <div><font>High 
                                                   Skill/High Wage - Mean Wage - $17.86/Hour </font></div>
                                             
                                             <div><font>State 
                                                   Approved AS Programs not Offered at Valencia </font></div>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       <div>
                                          
                                          <p><strong><font>B. 
                                                   &nbsp;&nbsp;Schedule Analysis from the previous year (sections, 
                                                   canceled, additions): </font></strong><font>As 
                                                preparation for scheduling, class sections are analyzed for trends 
                                                in offerings at various times of days and in different modalities. 
                                                The projected schedule is then established to meet the needs of 
                                                students . </font></p>
                                          
                                          
                                          <p><strong><font>C. 
                                                   &nbsp;&nbsp;Enrollment Data Analysis: </font></strong><font>After 
                                                reviewing the data and determining the enrollment health of each 
                                                program, recommendations are made for improving existing program, 
                                                as well as, discontinuation of programs no longer needed in the 
                                                community. The following issues are considered in the program 
                                                review. </font></p>
                                          
                                          
                                       </div>
                                       
                                       <blockquote>
                                          
                                          <div>
                                             <font>1. 
                                                &nbsp; Program Outcomes </font>
                                             
                                             <div>
                                                
                                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font>Factors to Assess Program Outcomes:</font></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <div>
                                                   
                                                   <div><font>Enrollment/Student 
                                                         Interest</font></div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   
                                                   <div><font>Completion</font></div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   
                                                   <div><font>Placement 
                                                         Rate</font></div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   
                                                   <div><font>Licensure 
                                                         Rates (if apply)</font></div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div>
                                                   
                                                   <div><font>Earnings 
                                                         of Graduates/Completers </font></div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><font>2. 
                                                &nbsp; Complete Health &amp; Viability Assessment </font></p>
                                          
                                          
                                          <div><font>3. 
                                                &nbsp; Identify and Indicate Program Growth </font></div>
                                          
                                          
                                          <div><font>4. 
                                                &nbsp; Findings and Recommendations: </font></div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <div><font>Curriculum 
                                                      Modifications &amp; New Programs</font></div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div><font>Strategies 
                                                      to Enhance Enrollment &amp; Capacity</font></div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <div><font>Potential 
                                                      Articulation Agreements</font></div>
                                                
                                             </li>
                                             
                                             <li><font>Online 
                                                   Course Development </font></li>
                                             
                                          </ul>
                                          
                                       </blockquote>
                                       
                                       <p><strong><font>D. 
                                                &nbsp;&nbsp;Financial Analysis </font></strong><font>: </font></p>
                                       
                                       <p><font>Revenue 
                                             Model </font></p>
                                       
                                       <ul>
                                          
                                          <li><font>Estimate 
                                                revenue from enrollment </font></li>
                                          
                                          <li><font>Modify 
                                                for residency, exemptions, etc. </font></li>
                                          
                                          <li><font>Create 
                                                reserve/response fund as appropriate </font></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><font>Cost 
                                             Model </font></p>
                                       
                                       <ul>
                                          
                                          <li><font><font>Estimate 
                                                   in every discipline and department the in-load, overload, adjunct 
                                                   distribution to the schedule</font></font></li>
                                          
                                          <li><font><font>Adjust 
                                                   for retirements, new-hires, etc. </font></font></li>
                                          
                                          <li><font><font>Adjust 
                                                   for reassignments </font></font></li>
                                          
                                          <li><font><font>Build 
                                                   a faculty assignment database for the year and assign costs to 
                                                   each&nbsp;assignment </font></font></li>
                                          
                                       </ul>
                                       
                                       <p><font><strong>E. 
                                                &nbsp;&nbsp;Business Process Analysis for Budgeting </strong></font></p>
                                       
                                       
                                       <p><u><font>How 
                                                does Budgeting support or fulfill the Precision Schedule? </font></u><font>Budget's 
                                             primary responsibility is to coordinate the budget process in a 
                                             manner that encourages good healthy communications and results in 
                                             the best all location of the resources necessary to support the 
                                             enrollment plan and generate the necessary funds. </font></p>
                                       
                                       
                                       <p><u><font>Beginning 
                                                of the process: What is the first step in the process? </font></u><font>Using 
                                             broad assumptions for our enrollment goals and expense patterns, 
                                             we will develop a preliminary budget that will give us some idea 
                                             of the resources available and the known costs. </font></p>
                                       
                                       
                                       <p><u><font>End: 
                                                When is the process complete? </font></u><font>The 
                                             process is complete when a more refined budget is prepared after 
                                             input from each area as to staffing needs, marketing costs, and 
                                             other expenses necessary to successfully obtain the desired enrollment. </font></p>
                                       
                                       
                                       <p><u><font>Actors: 
                                                Who are the people who actually touch the process? </font></u><font>Every 
                                             area </font></p>
                                       
                                       <p><u><font>Stakeholders: 
                                                Who are the people affected by the process? </font></u><font>Every 
                                             area of the college will be involved in this process. </font></p>
                                       
                                       
                                       <p><u><font>Process: 
                                                What are the action steps in the process? </font></u><font>With 
                                             input from the President and the senior staff, assumptions will 
                                             be developed in order to generate a preliminary budget. This information 
                                             will be used by the other groups to establish budgeting parameters. 
                                             As specific staffing and resource allocation plans are developed 
                                             by the various groups, a more refined budget will be established. 
                                             Buy in that the budgeted resources are adequate to accomplish the 
                                             enrollment objectives is necessary. </font></p>
                                       
                                       
                                       <p><u><font>Where 
                                                is the Pain? What are the challenges to this process? </font></u><font>Moving 
                                             from the perceived entitlements of base plus budgeting to more precision 
                                             allocation of resources based on our enrollment objectives. </font></p>
                                       
                                       
                                       <p><u><font>Wish 
                                                List/Action Items: What would make the pain go away? </font></u><font>Everyone 
                                             obtaining a better understanding of the interrelationships of each 
                                             unit (marketing, admissions, deans, grounds, etc.) in the fulfillment 
                                             of the college's objectives. This along with all of us developing 
                                             a willingness to share and even give up resources in order for everyone 
                                             to be more successful. </font></p>
                                       
                                       
                                       
                                       <p><strong><span>VIII.&nbsp; 
                                                GOALS</span></strong></p>
                                       
                                       <p><font>Over 
                                             the last several years enrollment has flattened. This flattening 
                                             of enrollment is due to several major factors. These factors include 
                                             but are not limited to: </font></p>
                                       
                                       <ul>
                                          
                                          <li><font>purposefully 
                                                lowering enrollment in order to align enrollment with finances </font></li>
                                          
                                          <li><font>several 
                                                bad hurricane seasons </font></li>
                                          
                                          <li><font>a 
                                                rebounding economy and low unemployment </font></li>
                                          
                                          <li><font>not 
                                                meeting student demands for alternative delivery of courses </font></li>
                                          
                                          <li><font>Start 
                                                Right initiatives </font></li>
                                          
                                          <li><font>decline 
                                                in dual-enrollment classes </font></li>
                                          
                                       </ul>
                                       
                                       
                                       <p><font>In 
                                             order to begin growing enrollment and revenue again, a new Strategic 
                                             Enrollment Management System of planning has been initiated. Valencia 
                                             Community College plans to grow enrollment strategically through 
                                             precision scheduling based on both financial modeling and continuous 
                                             enrollment analysis. </font></p>
                                       
                                       
                                       <p><font>For 
                                             the coming academic year, course enrollment patterns and student 
                                             marketing surveys indicate that there is a demand for distance and 
                                             flex-scheduled courses exceeding our supply. While graduation rates 
                                             in our traditional AA (transfer) degree program remains constant, 
                                             there is a considerable increase in the graduation rates for the 
                                             Technical Certificate and AS degree programs. </font></p>
                                       
                                       <p><font><strong><em>Goal 
                                                   1: Enrollment Targets </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will increase student enrollment by 5-6% from Fall 2006 to Fall 
                                             2007. </font></p>
                                       
                                       
                                       <p><font><strong><em>Goal 
                                                   2: Program Mix </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will expand programs identified with available capacity and room 
                                             to grow, as well as, expand programs where there has been a need 
                                             identified in the annual occupational openings data. </font></p>
                                       
                                       
                                       <p><font><strong><em>Goal 
                                                   3: Program Delivery </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will increase the number of alternative delivery classes courses 
                                             and flex-start classes by 15% from Fall 2006 to Fall 2007. </font></p>
                                       
                                       
                                       <p><font><strong><em>Goal 
                                                   4: Income Targets </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will increase income by to meet budget needs. </font></p>
                                       
                                       
                                       <p><font><strong><em>Goal 
                                                   5: Services </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will provide all necessary services to accommodate the increase 
                                             in enrollment envisioned by this plan. </font></p>
                                       
                                       
                                       <p><font><strong><em>Goal 
                                                   6: Stewardship/management </em></strong></font></p>
                                       
                                       <p><font>Valencia 
                                             will provide the stewardship and management necessary to meet the 
                                             enrollment challenges that will result from this plan. </font></p>
                                       
                                       
                                       
                                       <p><strong><span>IX.&nbsp; 
                                                KEY PERFORMANCE INDICATORS</span></strong></p>
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><font><strong>Institutional 
                                                            Positioning </strong></font></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Summer 
                                                         2006 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Summer 
                                                         2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Fall 
                                                         2006 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Fall 
                                                         2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Spring 
                                                         2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Spring 
                                                         2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Tuition 
                                                      and Fees 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Profile 
                                                      of entering students (Demographics to include race/ethnicity, 
                                                      gender, age) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Retention 
                                                      Rates 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <h2>Capacity 
                                                      and Demand 
                                                   </h2>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Available 
                                                      capacity to handle enrollment growth 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Projected 
                                                      changes to operating and fixed costs with enrollment changes 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Top 
                                                      10 competitors 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Education 
                                                      and General Expenditures (avg cost to educate student) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Cost 
                                                      analysis of Direct/Indirect costs based on FTE 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Distribution 
                                                      of admitted and enrolled students by income and/or need level 
                                                      (financial aid - % in each demographic or sub-group) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Student 
                                                      demand as measured through application to enrollment yield 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Financial 
                                                         Aid Policies </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Award 
                                                      "gaps" between student need and actual awards 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Percentage 
                                                      of need met/aid awarded in each year of the student's career 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Yield 
                                                      rates by need level and academic ability level 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Yield 
                                                      rates by key enrollment shaping goals (academic program, geographical 
                                                      area, ethnicity, talent, students) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Financial 
                                                      aid by academic ability 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Family 
                                                      income distribution in your primary student markets to assess 
                                                      "ability to pay" 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Application 
                                                      to completion of Financial Aid 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Student 
                                                         Marketing, Recruitment, and Retention </strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Marketing 
                                                      and recruitment activities 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Advertising 
                                                      cost/student by applicant, applied/enrolled, new students 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Conversion 
                                                      rates for each stage of your recruitment funnel-inquiry to 
                                                      application, application to assessment, assessment to orientation, 
                                                      orientation to registration, registration to payment--and 
                                                      comparison with national data 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Institutional 
                                                      image perception study for each relevant prospective student 
                                                      market 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <h2>Applicant/Enrolled </h2>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Yield 
                                                      (enrolled) rates for each subpopulation (Demographics to include 
                                                      race/ethnicity, gender, age) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Retention 
                                                      and graduation rates of subpopulations 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Per 
                                                      student financial aid levels for sub- populations 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Average 
                                                      hours taken per students by sub-population 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <h2>Educational 
                                                      Capacity and Demand 
                                                   </h2>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Current 
                                                      and projected capacities for each course, academic program, 
                                                      and classroom 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Current 
                                                      and projected student demands for each course, academic program, 
                                                      and classroom 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Lost 
                                                      net revenues for each over demand and under enrolled situation 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Average 
                                                      cost to deliver each program per student 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <h2>Student 
                                                      Performance 
                                                   </h2>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Baseline </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2006-2007 </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Performance </strong></p>
                                                   
                                                   <p><strong>Academic 
                                                         Yr 2007-2008 </strong></p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Retention 
                                                      and financial impact analysis (fall to fall - population by 
                                                      sub-group) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Retention 
                                                      and financial impact analysis (fall to spring - population 
                                                      by sub-group) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Graduation 
                                                      rate data and financial impact 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p>Retention 
                                                      of 3-peats (measure of 100% fee) 
                                                   </p>
                                                </div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <p><strong><span>X.&nbsp; 
                                                STRATEGIES</span></strong></p>
                                       
                                       <p><font>Strategies 
                                             have been identified and developed in the following areas to accomplish 
                                             the desired increase in FTE for the designated academic year: </font></p>
                                       
                                       
                                       <ul>
                                          
                                          <li><font>Program 
                                                Mix and Class Capacity Strategies</font></li>
                                          
                                          <li><font>Marketing 
                                                and Recruitment StrategiesRetention 
                                                Strategies</font></li>
                                          
                                          <li><font>Policy 
                                                and Procedure Strategies</font></li>
                                          
                                          <li><font>Financial 
                                                Strategies</font></li>
                                          
                                          <li><font>Financial 
                                                Aid Strategies</font></li>
                                          
                                          <li><font>Dual 
                                                Enrollment Strategies</font></li>
                                          
                                          <li><font>Campus 
                                                Strategies </font></li>
                                          
                                       </ul>
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><font>East 
                                                   Campus Enrollment Management Plan </font></p>
                                             
                                             <p><font>Osceola 
                                                   Campus Enrollment Management Plan </font></p>
                                             
                                             <p><font>West 
                                                   Campus Enrollment Management Plan </font></p>
                                             
                                             <p><font>Winter 
                                                   Park Campus Enrollment Management Plan </font></p>
                                             
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       
                                       <p><strong>XI.&nbsp; <font>ACCOUNTABILITY- STEWARDSHIP</font></strong></p>
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font><strong>Process </strong></font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font><strong>Steward(s) </strong></font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Enrollment 
                                                         Planning Work Team (EPWT) </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Faculty 
                                                         Council President, Provosts, Director of IR, IAC Chairperson, 
                                                         VP Administrative Services, VP for Institutional Advancement, 
                                                         VP for Student Affairs, VP for Human Resources and Diversity, 
                                                         CLO </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Financial 
                                                         Analysis, Revenue, and Budget Planning </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>VP 
                                                         Administrative Services </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Precision 
                                                         Scheduling and Staffing of Schedule </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Provosts 
                                                         and Deans </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Marketing 
                                                         Plan </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>VP 
                                                         for Institutional Advancement, Assistant VP Marketing &amp; 
                                                         Media Relations </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Financial 
                                                         Aid Plan </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Asst. 
                                                         VP, College Transition </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Registration 
                                                         Management </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>AVP 
                                                         Admissions &amp; Records, AVP Student Affairs </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Payment 
                                                         Management </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>AVP 
                                                         for Financial Services </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Enrollment 
                                                         Funnel </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>VP 
                                                         for Student Affairs </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>College 
                                                         Transition Planning </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>AVP 
                                                         College Transition </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Precision 
                                                         Budgeting for Staffing </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>AVP 
                                                         Budget &amp; Logistical Services </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Course/Program 
                                                         Completion Performance </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Dean 
                                                         of Workforce Development, Provosts, Deans, Director of IR, 
                                                         Director of Assessment, AVP Curriculum and Articulation </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Program 
                                                         Review Trends and Program Development </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Dean 
                                                         of Workforce Development, Provosts, Deans, Director of IR, 
                                                         Director of Assessment, AVP Curriculum and Articulation </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Employment 
                                                         Trends </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Dean 
                                                         of Workforce Development, Director of IR, Director of Assessment, 
                                                         AVP Curriculum and Articulation </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Enrollment 
                                                         Trends </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Director 
                                                         of IR, VP for Student Affairs, Deans, Provosts </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Enrollment 
                                                         Analysis </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Deans, 
                                                         Provosts, AVPs, CLO </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Student 
                                                         Profile Trends </font></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><font>Director 
                                                         of IR, VP for Student Affairs, Director of Assessment </font></p>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">Assessment</div>
                                                
                                                <div data-old-tag="td">Collegewide Assessment Team </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <p><font>A 
                                             Schedule for the Enrollment Planning Processes has been established:</font></p>
                                       
                                       <p><a href="documents/EnrollmentCalendar.docx.html">Enrollment 
                                             Calendar</a></p>
                                       
                                       
                                       
                                       <p><strong><span>XII.&nbsp; 
                                                ENROLLMENT MANAGEMENT LEARNING PLAN&nbsp; </span></strong></p>
                                       
                                       <p><font>An 
                                             Enrollment Management Learning Plan will be established annually 
                                             that: </font></p>
                                       
                                       <ul>
                                          
                                          <li><font>identifies 
                                                areas of best practices that we need to study and that will support 
                                                our plan. </font></li>
                                          
                                          <li><font>establishes 
                                                a professional development plan that will support learning of 
                                                best practices. </font></li>
                                          
                                          <li><font>identifies 
                                                appropriate development activities and budgeting priorities for 
                                                those development activities. </font></li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><strong><span>XIII.&nbsp; 
                                                ENROLLMENT MANAGEMENT RESEARCH AGENDA</span></strong></p>
                                       
                                       <p>Enrollment 
                                          management relies on research. Research should provide strategic 
                                          information on College demographics, market trends and student behavior. 
                                          In turn, the College can more effectively engage in data-driven 
                                          decision-making to support student learning.<br>
                                          <br>
                                          The following questions can help guide the research agenda for the 
                                          College moving forward:<br>
                                          <br>
                                          1.&nbsp; What are we doing, how do we measure it, and how well are 
                                          we doing it?<br>
                                          2.&nbsp; Whom do we want to recruit and does the College sufficiently 
                                          target 
                                       </p>
                                       
                                       <p> &nbsp;&nbsp;&nbsp;&nbsp; market its&nbsp;recruitment activities?<br>
                                          3.&nbsp; Which academic programs attract students?<br>
                                          4.&nbsp; Which delivery modes are most effective and for which students?<br>
                                          5.&nbsp; Does advising and counseling, orientation and student life 
                                          programs 
                                       </p>
                                       
                                       <p> &nbsp;&nbsp;&nbsp;&nbsp; help students adjust to the College?<br>
                                          6.&nbsp; How do the activities, programs and services support student 
                                          learning?<br>
                                          <br>
                                          Other research areas for consideration:<br>
                                          <br>
                                          DEMOGRAPHIC TRENDS:<br>
                                          Local and national forecasts of higher education enrollment trends<br>
                                          <br>
                                          STUDENT TRENDS:<br>
                                          First generation college students, challenges and opportunities<br>
                                          Diversity in higher education (what does this mean to Valencia?)<br>
                                          <br>
                                          ACADEMIC PROGRAMS:<br>
                                          Course information, demand analysis, closed section tracking etc.<br>
                                          Full- and part-time enrollment mix<br>
                                          Learning communities<br>
                                          <br>
                                          RETENTION TRENDS:<br>
                                          Graduation and transfer rates<br>
                                          Probation/dismissal rates<br>
                                          Success and failure rate by program and division<br>
                                          Degree and program completion patterns<br>
                                          Remedial programs (individual metrics?)<br>
                                          Honors programs (individual metrics?)<br>
                                          Mentoring programs (does the college have them, if so, individual 
                                          metrics?)<br>
                                          Early alert assessment and monitoring system (individual metrics?)<br>
                                          <br>
                                          FACULTY TRENDS:<br>
                                          Professional development &amp; training (in enrollment management 
                                          practices) for both full-time and adjunct faculty<br>
                                          Impending retirements<br>
                                          <br>
                                          MARKETING/ADVERTISING TRENDS:<br>
                                          Advertising (what is our target market watching?)<br>
                                          Direct mail<br>
                                          Personal contact<br>
                                          <br>
                                          TECHNOLOGY:<br>
                                          Distance learning and use of technology<br>
                                          Online web advising<br>
                                          Online communication management module (Banner)<br>
                                          Student portals<br>
                                          Web design for Enrollment Management Program at Valencia<br>
                                          <br>
                                          MARKET RESEARCH:<br>
                                          Job market trends and changing market demands<br>
                                          Economic development trends<br>
                                          Partnerships with business and industry and other local employers<br>
                                          <br>
                                          COMPETITION TRENDS:<br>
                                          Proprietary institutions<br>
                                          Four-year colleges and universities<br>
                                          Other community colleges<br>
                                          <br>
                                          POLICY TRENDS:<br>
                                          Financial aid/scholarships<br>
                                          Need based vs. merit<br>
                                          Tying funding to assessment of student performance<br>
                                          Admission (i.e. residency, etc.)<br>
                                          Homeland security<br>
                                          Policies and procedures (how well do these support student learning?)
                                       </p>
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/Archives.pcf">©</a>
      </div>
   </body>
</html>