<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/educational-commitment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>Impact on Education</strong></h2>
                        
                        
                        <p>For the past 50 years, Valencia has steadily broken barriers to opportunity. And,
                           along the way, we’ve rejected orthodox educational methods, opting instead for bold
                           experiments that stemmed from one core belief: That our students can learn anything
                           under the right conditions.
                        </p>
                        
                        
                        <p>As we look toward the coming years, we have a strong foundation to build upon. Our
                           part-time faculty participate enthusiastically in development opportunities. Our career
                           and workforce education programs boast a 98% placement rate, so that our A.S. graduates
                           leave the college with a job. And the number of students taking online classes has
                           grown dramatically.
                        </p>
                        
                        
                        <p>Today, thanks to Direct Connect, our groundbreaking partnership with UCF, the number
                           of transfer students entering UCF as juniors now outnumbers the UCF freshman class
                           each year. There is no more productive transfer partnership in the country—and that
                           promise of a guaranteed transfer has provided a purpose to our students, who now can
                           see a clear path to their educational goals. Even as first-year students sitting in
                           a composition class at Valencia, they can now envision a journey that concludes with
                           commencement from UCF.
                        </p>
                        
                        <p>For the college to transform the student experience, we need to collaborate across
                           the education ecosystem, and include our Pre-K-Secondary partners too.
                        </p>
                        
                        
                        <p><strong>Initiative 1. Education Ecosystems</strong></p>
                        
                        <p>If Valencia is going to impact education, we need to look beyond our campuses and
                           at the larger educational ecosystem of Central Florida. From the moment a student
                           enters pre-kindergarten to the day she graduates from UCF, she is a product of our
                           community. And the educational performance of this community is judged not on just
                           one college or high school or university, but the performance of the whole system.
                           To get results that will benefit students and the community as a whole, we need seamless
                           pathways that guide students—and data that will guide the decisions of administrators
                           and faculty. Toward that end, we have set two goals that will move toward a seamless
                           educational ecosystem.
                        </p>
                        
                        
                        <p><strong>Initiative 2. Direct Connect 2.0</strong></p>
                        
                        <p>Since its inception in 2006, DirectConnect to UCF has received national attention
                           and become a model for the rest of the country to emulate. The program has achieved
                           great things for the students and this community: In the 2014-15 graduating class
                           at UCF, 30% of the graduates of color were Valencia grads. Taken together, Valencia
                           graduates made up almost 25% of the UCF graduating class during that year.
                        </p>
                        
                        
                        <p>Over the past five years, the number of Hispanics who earned a bachelor’s degree at
                           UCF—and graduated from one of the DirectConnect partner colleges—jumped 134%. The
                           number of black students who went through DirectConnect to earn bachelor’s degrees
                           at UCF during that five years nearly doubled. If we want to change the outlook for
                           families of color in Central Florida, this is the most practical way to do it—through
                           education.
                        </p>
                        
                        <p><strong>Initiative 3. Online Learning</strong></p>
                        
                        <p>At Valencia, we pride ourselves on our many excellent professors and instructors and
                           our nationally recognized Teaching/Learning Academy. For decades, Valencia has worked
                           to make the classroom experience top-notch, so that our students can excel. And while
                           we relish the time for students and professors to interact face to face, we also recognize
                           that a growing number of students choose to take courses online. Over the past 10
                           years, the percentage of Valencia students taking an online course has jumped from
                           4.9% in 2004-05 to 24.6% in 2014-15.
                        </p>
                        
                        
                        <p>However, our internal data shows that students in online classes are not as successful
                           as students who learn in a traditional classroom, face-to-face with a professor. In
                           2007, Valencia data showed that 59.5% of our students taking online classes earned
                           a passing grade, while 72.7% of students who took face-to-face classes passed. Although
                           we’ve narrowed that gap—in 2015, 71.2% of students taking online classes at Valencia
                           passed, while 76.1% of those in face-to-face classes passed—we are committed to giving
                           the online student the same chance for success as that experienced by the student
                           who drives to campus every day.
                        </p>
                        
                        
                        <p><strong>Initiative 4: New Student Experience</strong></p>
                        
                        <p>At Valencia, we believe that if students “start right,” they can succeed. And over
                           the years, we’ve made great strides in helping them to start right and get on the
                           right path. The college took an initial step in requiring a New Student Experience
                           (NSE) course that provides an extended orientation to college, integrates student
                           success skills, requires career and academic advising and teaches the student to develop
                           an individualized education plan. Our goal is to ensure that all first-time college
                           students and transfer students will have a plan to graduate. 
                        </p>
                        
                        
                        <p>We are already witnessing good preliminary results. One early success: Students who
                           take NSE appear to be more likely to sign up for classes the next semester—and less
                           likely to drop out—than those who didn’t take the course. In spring 2015, before NSE
                           was launched, Valencia’s “persistence rate”—or the percentage of students taking classes
                           in fall who returned to college during the spring semester—was 76.9%. The next year,
                           after we launched NSE, the percentage of students returning for spring classes had
                           jumped to 82.9%.
                        </p>
                        
                        
                        <p><strong>Initiative 5: Part-Time Faculty Engagement</strong></p>
                        
                        <p>Just as Valencia has invested in our Teaching/Learning Academy to support new professors,
                           counselors, and librarians as they improve their professional practices and students’
                           learning, we plan to invest in the college’s part-time faculty members as well.
                        </p>
                        
                        
                        <p>Part-time faculty members make up a substantial part of the college’s workforce—and
                           investing in them will pay dividends for our students. During the 2015-2016 fiscal
                           year, Valencia employed 1,157 part-time faculty, who taught 6,190 sections—45.9% of
                           all the classes taught.
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/educational-commitment.pcf">©</a>
      </div>
   </body>
</html>