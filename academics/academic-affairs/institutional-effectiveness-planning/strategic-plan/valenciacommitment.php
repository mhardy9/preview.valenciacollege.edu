<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Planning at Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/valenciacommitment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Strategic Planning at Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/">Strategic Plan</a></li>
               <li>Strategic Planning at Valencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2><strong>Purpose of This Plan</strong></h2>
                        
                        
                        <p>As we prepare to celebrate Valencia’s 50th anniversary in 2017, Valencia is proud
                           to complete our Impact Plan for 2016-2022. Looking back over Valencia’s storied history,
                           we are building upon a foundation of excellent planning that was established by the
                           college’s early leaders, faculty and staff. Now, as then, Valencia faculty and staff
                           have worked collaboratively in the process of creating this thoughtful impact plan—as
                           a map to guide us for the next seven years.
                        </p>
                        
                        
                        <p>Building on the deep work conducted as a result of the previous plan (see Appendix
                           on Pillars), Valencia College commits to serving our community by Making a Positive
                           Impact on Education; Transforming Our Regional Workforce and Economy; and Forging
                           a Better Quality of Life.
                        </p>
                        
                        
                        <p>Deeply rooted in the college’s experience, these commitments project our core work
                           from 2016-2022, calling us to action to improve our students’ lives and the life of
                           the community that we serve. Although Valencia has served the community through its
                           service to students, the college is embarking on a community partnership that we have
                           not yet experienced. As we execute this Strategic Plan, Central Florida will begin
                           to see Valencia as an essential partner in addressing and solving the region’s problems
                           and goals.
                        </p>
                        
                        
                        <p>The three commitments were developed during 2014-16 through a collaborative process
                           involving faculty, staff, students, and our community. Just as important as drawing
                           up the plan, however, is adjusting the plan based on what we learn. Because the impact
                           plan is designed to evolve, each year there will be changes in goals and the objectives
                           we use to reach those goals, but it is important to note that despite the evolution
                           of the plan from year to year, our mission and our vision will not change.
                        </p>
                        
                        
                        <p>Design Principles </p>
                        
                        <p>Facilitators and team members will apply the following design principles during the
                           review process: 
                        </p>
                        
                        <ul>
                           
                           <li>To build on the college’s foundation (Mission, Vision, Values)</li>
                           
                           <li>To trust those closest to the work </li>
                           
                           <li>To ensure the process is kept simple</li>
                           
                           <li>To maintain an open collaborative process</li>
                           
                           <li>To guarantee the process will be kept transparent </li>
                           
                           <li>To actively communicate the status of the process to the college community</li>
                           
                           <li>To heavily involve and encourage faculty and staff input</li>
                           
                           <li>To strive for consensus among participants</li>
                           
                           <li>To work at a strategic not operational level</li>
                           
                           <li>To continue to build on work currently done at the college</li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <p><strong>Steering Committee</strong></p>
                        
                        <p>The Steering Committee is responsible for the oversight of the college-wide strategic
                           planning process, and act as liaison between campus and college-wide planning. 
                        </p>
                        
                        
                        <p><strong><u>Steering Committee Responsibilities:</u></strong></p>
                        
                        <ul>
                           
                           <li>Develop a specific timeline within the strategic planning calendar</li>
                           
                           <li>Attend campus based meetings to share information from work teams </li>
                           
                           <li>Compile information from campus meeting to bring back to the steering committee</li>
                           
                           <li>Make recommendations to the Executive Council on new strategic initiatives</li>
                           
                           <li>Review final reports </li>
                           
                           <li>Learn the new strategic planning software</li>
                           
                           <li>Share information from the steering committee to campus leadership teams</li>
                           
                        </ul>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        <p><strong>Work Teams</strong><strong><u>
                                 </u></strong>
                           
                        </p>
                        
                        <p>Work teams will assess the current status of significant college-wide work, collect
                           and interpret data, monitor related strategic issues, make recommendations on how
                           the work should progress between 3 to 5 years, and prepare a report that will contribute
                           to the development of the college’s 5yr Strategic Plan. 
                        </p>
                        
                        <p><strong><u>Composition of the Work Teams</u></strong></p>
                        
                        <p>Members of the review teams will be identified by the <strong><em>Strategic Plan Steering Committee</em></strong> and <strong><em>Executive Council</em></strong>. These members will include faculty, career service, professional, administrative,
                           and executive staff who have direct responsibility for the work at each of the campuses
                           or college-wide offices. &nbsp;
                        </p>
                        
                        <p><strong><u>Responsibilities of the Work Teams</u></strong></p>
                        
                        <p>The teams will answer the following questions during the review process: </p>
                        
                        <ul>
                           
                           <li>What are we doing well? </li>
                           
                           <li>Where are the gaps, and in what ways can we improve?</li>
                           
                           <li>How do we partner with the community?</li>
                           
                           <li>Where do we want to go with this work in the next five years? </li>
                           
                           <li>What is currently happening at the college to help us get there?</li>
                           
                           <li>What measures can we use to help us identify progress towards the strategic initiative?</li>
                           
                           <li>List major activities that will assist in moving the work forward</li>
                           
                        </ul>
                        
                        <p><strong><u>Work Team Facilitators </u></strong></p>
                        
                        <p>Each team will have a two designated Facilitators who are directly tied to the work.
                           The Facilitators will be responsible for the following:
                        </p>
                        
                        <ul>
                           
                           <li>Lead the review process of each work team</li>
                           
                           <li>Schedule work team meetings</li>
                           
                           <li>Lead outcomes and assessment discussion</li>
                           
                           <li>Collect data and information</li>
                           
                           <li>Participate in training session after Academic Assembly</li>
                           
                           <li>Learn the new planning software </li>
                           
                           <li>Draft final strategic initiatives work report </li>
                           
                        </ul>            
                        <p><strong><u>Work Product: </u></strong></p>
                        
                        <p>Final Report</p>
                        
                        <ul>
                           
                           <li><strong>Elements:</strong></li>
                           
                           <ul>
                              
                              <li>History Overview</li>
                              
                              <li>Where we are</li>
                              
                              <li>Data for decision making</li>
                              
                              <li>Data analysis </li>
                              
                              <li>Where we want to go</li>
                              
                              <li>Draft outcomes / objectives (timeline)</li>
                              
                              <li>Draft assessment measures</li>
                              
                           </ul>
                           
                           <p><a href="#top">TOP</a></p>
                           
                        </ul>                      
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/16AMN002-strategic-plan-brochure.pdf"><img alt="The Power to Serve: Valencia's Five-Year Impact Plan" height="75" src="valencia_impact_banner_sm.jpg" width="245"></a>
                        
                        
                        
                        <h3>Contact Information:</h3>
                        
                        
                        
                        <p><strong>Susan Ledlow</strong><br>
                           Sponsor <br>
                           VP, Academic Affairs &amp; Planning <br>
                           P: 407-582-3423 <br>
                           E: <a href="mailto:sledlow@valenciacollege.edu" rel="noreferrer">sledlow@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Karen M. Borglum</strong><br>
                           Facilitator <br>
                           AVP, Curriculum &amp; Assessment <br>
                           P: 407-582-3455 <br>
                           E: <a href="mailto:kborglum@valenciacollege.edu" rel="noreferrer">kborglum@valenciacollege.edu</a></p>
                        
                        
                        <p><strong>Noelia Maldonado Rodriguez</strong><br>
                           Co-Facilitator <br>
                           Coordinator, Institutional Advancement <br>
                           P: 407-582-3487 <br>
                           E: <a href="mailto:nmaldonado5@valenciacollege.edu" rel="noreferrer">nmaldonado5@valenciacollege.edu</a></p>
                        
                        
                        
                        
                        <h3><a href="http://wp.valenciacollege.edu/strategic-planning/">Discussion Blog</a></h3>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <ul>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-other-nse-related-goals/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On Other NSE-Related Goals</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 
                                 
                                 
                                 
                                 
                                 <a href="http://wp.valenciacollege.edu/strategic-planning/2015/11/11/nse-strategic-planning-team-feedback-requested-on-lifemap-2-0mep/" target="_blank">NSE-Strategic Planning Team-Feedback Requested On LifeMap 2.0/MEP</a> <br>
                                 
                                 
                                 
                                 
                                 
                                 <span>Nov 11, 2015</span><br>
                                 
                                 
                              </li>
                              
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/strategic-plan/valenciacommitment.pcf">©</a>
      </div>
   </body>
</html>