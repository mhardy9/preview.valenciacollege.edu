<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meet the Staff  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Effectiveness &amp; Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li>Meet the Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Meet the Staff</h2>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/daryl-davis.jpg" alt="Daryl Davis" class="img-circle"><br>
                           <strong>Daryl Davis<br>
                              <em>Director</em></strong><br>
                           <a href="mailto:djdavis@valenciacollege.edu">djdavis@valenciacollege.edu</a></p>
                        
                        <p>Prior to this positon, Daryl served the college for a number of years as a research
                           analyst, were he successfully collaborated with Valencia staff, faculty members and
                           administrators to develop a wide range of reports and data analysis, used throughout
                           the college to make data-informed decision on some of the college’s biggest initiatives,
                           including the Aspen Prize, Achieving the Dream, DEI and the QEP Core Team . While
                           working in this role, Daryl played a lead role in collaborating with the Office of
                           Information Technology to launch the college into a new era of reporting by rolling
                           out the use of tools like SAS Visual Analytics. 
                        </p>
                        
                        <p>Daryl attended the University of Central Florida, receiving a Bachelor’s of Science
                           in Business Management, with a focus on Hospitality Management. He also attended the
                           University of Phoenix, earning a Master’s of Science in Computer Information Systems.
                           His personal interests include exercise and basketball, in which he was inducted into
                           the UCF Athletic Hall of Fame in 2008, where he was a 4 year letterman in basketball.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/donna-kosloski.jpg" alt="Donna Kosloski" class="img-circle"><br>
                           <strong>Donna Kosloski<br>
                              <em>Institutional Research Analyst</em></strong><br>
                           <a href="mailto:dkosloski@valenciacollege.edu">dkosloski@valenciacollege.edu </a></p>
                        
                        <p>Donna Kosloski has 14 years of Institutional Research experience and is responsible
                           for conducting analyses using SQL, SAS, and Excel to provide comprehensive studies
                           which include visually displaying meaningful information accompanied by relevant data
                           tables. Donna's primary responsibilities include registration and enrollment planning
                           (Credit Registration Report), distance learning studies, and Direct Connect studies.
                           Donna also consults with individuals to assist in creating research projects and developing
                           the best approach to formulate a data study. Donna chairs the Professional Staff Leadership
                           Team and facilitates the PDP Tool. Donna also created and continues to facilitate
                           the IR and You workshop, SOTL2273, which raises the awareness and understanding of
                           Institutional Research and partners with faculty in developing their research projects
                           through the Teaching and Learning Academy. Donna also takes the lead in learning new
                           technologies, develops training manuals, and teaches technical training workshops.
                        </p>
                        
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/selina-li.jpg" alt="Selina Li" class="img-circle"><br>
                           <strong>Selina Li<br>
                              <em>Institutional Research Analyst</em></strong><br>
                           <a href="mailto:aarceneaux@valenciacollege.edu">sli6@valenciacollege.edu</a></p>
                        
                        <p> Selina joined Valencia in 2016, serving as State and Federal Reports’ Coordinator.
                           She collects, analyses, and reports college data to assist in State and Federal reports,
                           accreditation requirements, accountability and internal decision making. Selina has
                           14 years data analysis experience, holds Master of Science in Computer Science and
                           Master of Art in Statistics. 
                        </p>
                        
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/craig-simpson.jpg" alt="Craig Simpson" class="img-circle"><br>
                           <strong>Craig Simpson<br>
                              <em>Institutional Research Analyst</em></strong><br>
                           <a href="mailto:csimpson25@valenciacollege.edu">csimpson25@valenciacollege.edu</a></p>
                        
                        <p> Prior to Craig joining Valencia in 2015, he served as a Report’s Coordinator and
                           Sr. Decision Support Analyst for another FCS Institution. Craig has 10 years of Institutional
                           Research experience and utilizes SAS, SQL and other tools to extract and visualize
                           data. He holds a Master’s of Science in Management Information Systems and Bachelors
                           of Science in Psychology.
                        </p>
                        
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/leann-dubois.jpg" alt="Leann DuBois" class="img-circle"><br>
                           <strong>Leann DuBois<br>
                              <em>Institutional Research Analyst</em></strong><br>
                           <a href="mailto:ldubois4@valenciacollege.edu">ldubois4@valenciacollege.edu</a></p>
                        
                        <p> Selina joined Valencia in 2016, serving as State and Federal Reports’ Coordinator.
                           She collects, analyses, and reports college data to assist in State and Federal reports,
                           accreditation requirements, accountability and internal decision making. Selina has
                           14 years data analysis experience, holds Master of Science in Computer Science and
                           Master of Art in Statistics. 
                        </p>
                        
                        
                        
                        <hr>
                        
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/cissy-reindahl.jpg" alt="Cissy Reindahl" class="img-circle"><br>
                           <strong>Cissy Reindahl<br>
                              <em>Data Analyst</em></strong><br>
                           <a href="mailto:creindahl1@valenciacollege.edu">creindahl1@valenciacollege.edu</a></p>
                        
                        <p>Cissy joined Valencia in 2012 as part of the Faculty and Instructional Development
                           team. She currently manages the IR office, oversees the IR Work Request process, assists
                           IR Research Analysts in the final preparation and distribution of strategic reports,
                           and manages the IR website. Cissy holds a Master of Business Administration degree
                           from Oklahoma City University. 
                        </p>
                        
                        
                        <hr>
                        
                        
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/eleanor-carter.jpg" alt="Eleanor L. McCoy-Carter" class="img-circle"><br>
                           <strong>Eleanor L. McCoy-Carter<br>
                              <em>Institutional Research Analyst</em></strong><br>
                           <a href="mailto:emccoycarter@valenciacollege.edu">emccoycarter@valenciacollege.edu</a></p>
                        
                        <p>Eleanor L. McCoy-Carter is a native Floridian and military brat who considers Southern
                           California to be her second home. She is a former Valencia REACH participant who completed
                           her senior year of High School at Maynard Evans High of Orlando where she graduated
                           with honors. She subsequently earned a Bachelor's of Science degree in Business Administration
                           with an Industrial Engineering minor from Florida A&amp;M University (Tallahassee, FL)
                           and later a Master’s of Science Degree in Predictive Analytics from Northwestern University
                           (Chicago, IL). While attending FAMU, she was honored to be Miss Truth Hall, President
                           of SBI Forum Profiles, Women's Tennis Team Captain, Scholar Athlete of the Year, and
                           2nd VP of the Presidential Scholar's Association. In 2012, she was employed as the
                           Degrees At Work AmeriCorps VISTA for the Louisville, KY chamber of commerce (Greater
                           Louisville, Inc.) and has also since been actively involved with her national alumni
                           association. Through both those efforts, she has continuously helped to support and
                           promote educational initiatives throughout the communities in which she has lived.
                           In 2013, she received the FAMU Distinguished Alumni Award for Community Service for
                           her active involvement in more than a dozen community and church organizations over
                           the course of more than a decade.
                        </p>
                        
                        <p>Eleanor's career has mainly spanned the corporate sector with such companies as Dun
                           &amp; Bradstreet Plan Services, the Home Shopping Network and AAA Auto Club South where
                           her primary responsibilities involved Database Marketing, Marketing Research and Vendor
                           Risk Management. She is now very excited to be a part of the Valencia family.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/institutional-effectiveness-planning/kari-schlieper.jpg" alt="Kari Schlieper" class="img-circle"><br>
                           <strong>Kari Schlieper<br>
                              <em>Kari Schlieper, Staff Assistant II</em></strong><br>
                           <a href="mailto:kschlieper@valenciacollege.edu" rel="noreferrer">kschlieper@valenciacollege.edu</a></p>
                        
                        <p>Kari joined Valencia in 2017 as a staff assistant in Institutional Research. She performs
                           a wide variety of administrative duties including travel, budget and updating strategic
                           reports.
                        </p>
                        
                     </div>
                     
                     
                     <hr>
                     
                     
                     
                     
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>