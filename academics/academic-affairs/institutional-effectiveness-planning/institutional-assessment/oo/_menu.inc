<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-window-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Institutional Assessment</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<a href="../index.php"><h3>Home</h3></a><li><a href="../Calendar.php">Calendar &amp; Tools</a></li>
<li><a href="../saicc/FacultyWorkshops.php">Workshops</a></li>
<li><a href="../fg/index.php">Focus Groups</a></li>
<li><a href="../saicc/SurveySupport.php">Survey Support</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../loa/index.php"><h3>Learning Outcomes Assessment (LOA) </h3></a><li><a href="../LOA/assessment_plans.php">Assessment Plans</a></li>
<li><a href="../loa/outcomes_GenEd.php">General Education</a></li>
<li><a href="../loa/qep.php">Quality Enhancement Plan (QEP)</a></li>
<li><a href="index.php">Tools</a></li>
<li><a href="../loa/Resources.php">Resources</a></li>
<li><a href="../loa/Examples.php">Examples</a></li>
<li><a href="../loa/forms_reports.php">Forms, Reports, &amp; More</a></li>
<li><a href="../loa/program_outcomes.php">Degree and Career Program Outcomes</a></li>
<li><a href="../dt/index.php">Data Team</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../saicc/index.php"><h3>Student Feedback Survey</h3></a><li><a href="../saicc/SAICalendar.php">Calendar &amp; Tools</a></li>
<li><a href="../saicc/SAIReports.php">Reports</a></li>
<li><a href="../saicc/ResourcesforUsers.php">Resources</a></li>
<li><a href="../saicc/LoginHere.php">Sign In Here</a></li>
<li><a href="../saicc/SFIFrequentlyAskedQuestions.php">Frequently Asked Questions</a></li>
<li><a href="../surveys/ccsse.php">CCSSE Survey</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../surveys/index.php"><h3>Planning</h3></a><li><a href="../surveys/online-learning.php">Online Learning</a></li>
<li><a href="../surveys/direct-connect.php">DirectConnect 2.0</a></li>
<li><a href="../surveys/new-student-experience.php">New Student Experience</a></li>
<li><a href="../surveys/faculty-engagement.php">Part-time Faculty Engagement</a></li>
<li><a href="../sam/index.php">State Assessment Meeting</a></li>
<li><a href="../AspenPrizeApplication.php">Aspen Prize</a></li>
<li><a href="../MeetTheTeam.php">Meet the Team</a></li>
</ul></div>
</div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
