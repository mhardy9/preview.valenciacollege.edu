<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/oo/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Oo</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3><em>Online Organizer</em></h3>
                        
                        <blockquote>
                           
                           <blockquote>
                              
                              <blockquote>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             
                                             <p><em>Click Here to Log In </em></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><a href="https://ban-apex-prod.valenciacollege.edu:8070/apex/f?p=OOLOA:LOGIN" target="_blank"> </a>The Online Organizer supports the annual cycle of learning outcomes assessment activities
                                    and improves our ability to understand and use learning outcomes data over time. This
                                    Online Organizer will help you to document your assessment over time and share that
                                    information with others. It also allows us to document our work in ways that we are
                                    able to share with others, such as state and regional accreditors.
                                 </p>
                                 
                              </blockquote>
                              
                           </blockquote>
                           
                        </blockquote>            
                        
                        <p>The Organizer has been designed using an interface that may be familiar to those who
                           have used the leave system or other online reporting tools at Valencia. It developed
                           out of the needs expressed by faculty and staff leading Program Learning Outcomes
                           Assessment (PLOA) planning and other college-wide assessment activities. They wanted
                           a dynamic system – accessible over time and web-based. Among the features the Organizer
                           includes a way to enter results over time, a learning outcomes assessment plan timeline
                           tool, a way to map outcomes to courses offered and a place to upload supporting documents.
                           Your work in the Online Organizer focuses on learning outcomes at the level of the
                           program or area being assessed (not course-level outcomes). The assessment plans and
                           results reported here should be for the program, not limited to feedback on a specific
                           course. What is it you want to students to know or able to do as they leave your program
                           or discipline? Here in the Online Organizer you can pose those questions specific
                           to your field, test your theories in collaboration with your colleagues, document
                           and share your results – and then take action to improve the outcomes for faculty,
                           staff, and students. 
                        </p>
                        
                        
                        <p>If you have questions or need assistance, please contact Laura Blasi, Director, Institutional
                           Assessment (lblasi@valenciacollege.edu)
                        </p>
                        
                        <h4>training materials for online organizer</h4>
                        
                        <p><a href="../loa/documents/1of2-OO-ARReportforLOLMeeting2-3-2015-NM.pdf" target="_blank">Online Organizer -AR Report for LOL Meeting rev. 02/06/2015</a> 
                        </p>
                        
                        <p><a href="../loa/documents/2of2-OOandResultsReportforLOLMeeting02-03-15-NM.pdf" target="_blank">Online Organizer -Results Report for LOL Meeting rev. 02/03/2015</a></p>
                        
                        <p><a href="../loa/documents/OnlineOrganizerHandoutLoginandOutcomesSpecTCs3pg1-27-2015.pdf" target="_blank">Online Organizer Handout Login and Outcomes Spec TCs pg. 1 rev. 01/27/2015</a> 
                        </p>
                        
                        <p><a href="../loa/documents/OOEntryHandout-Template-1-27-2015.pdf" target="_blank">Online Organizer Handout - Template rev. 01/27/2015 </a></p>
                        
                        <p><a href="documents/PrintingInstructionsinOnlineOrganizer.pdf" target="_blank">Printing Instructions in Online Organizer</a></p>
                        
                        <p><a href="documents/PrintingInstructions-ProgramListsinOnlineOrganizer-Administrators.pdf" target="_blank">Printing Instructions for Program Lists -Administrators</a></p>
                        
                        <p><a href="documents/RollingDataintheOnlineOrganizer.pdf" target="_blank">Rolling Data in the Online Organizer</a></p>
                        
                        
                        
                        
                        <p>. </p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/oo/index.pcf">©</a>
      </div>
   </body>
</html>