<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/ccsse.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/">Surveys</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>The Community College Survey of Student Engagement (CCSSE)</h2>
                        
                        <p>Interactive workbook of CCSSE 2015 and 2017 data available here:<br><a href="https://public.tableau.com/profile/nichole.jackson#!/vizhome/DiscussionandDecisionDatasetCCSSE2015and2017/Sample" target="_blank">
                              Discussion and Decision Dataset--CCSSE</a><br>
                           <br>
                           <a href="documents/Overview-of-how-to-use-the-data-available-online.pdf" target="_blank">(Overview of how to use the data available online).</a></p>
                        
                        <p>The CCSSE provides information about effective educational practice in community colleges
                           and assists institutions in using that information to strengthen student learning
                           and persistence. Student engagement is defined as the amount of time and energy students
                           invest in meaningful educational practices. Please contact our office for more information
                           and customized reports using the CCSSE data.
                        </p>
                        
                        <p>Learn more about CCSSE: <a href="http://www.ccsse.org/" target="_blank">http://www.ccsse.org</a></p>
                        
                        <p>Please email the Institutional Assessment Office if you have any questions about the
                           CCSSE: <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a>. 
                        </p>
                        
                        
                        <p>CCSSE 2017 results are in. Select reports from the items below.</p>
                        
                        <h3>2017 Key Findings for Valencia College Report</h3>
                        
                        <p>The Key Findings report provides an entry point for reviewing  results from your administration
                           of the 2017 Community College Survey of  Student Engagement (CCSSE). The report provides
                           college-specific data in an  easy-to-share format including benchmark comparisons
                           between the college,  top-performing colleges, and the CCSSE cohort. It also highlights
                           aspects of  highest and lowest student engagement at the college, as well as results
                           from  five CCSSE special-focus items. Select faculty survey data are also highlighted.
                           Note that this report only shows the base sample of 1,375  students, some of the respondents
                           did not complete enough of the questions to  be included in the cohort. This report
                           does not include the oversample, which is the additional students we invited to  take
                           the survey beyond the base sample that was randomly determined by CCSSE.  The total
                           base sample and oversample results include 3,863 students from  Valencia college.
                           
                        </p>
                        
                        <p><strong><a href="documents/CCSSE-2017-Exec-Sum-Base-Sample-1407-8-7-2017.pdf" target="_blank">Key Findings Report -  Executive Summary 2017</a></strong><br>
                           (N=1,375 base sample) compared to the CCSSE Cohort
                        </p>
                        
                        
                        
                        <h3>CCSSE 2017 Cohort Overview</h3>
                        
                        <p> Colleges participating in CCSSE 2017 received a refreshed survey  instrument. Most
                           of the items on the survey did not change at all, and the  majority of those items
                           that were revised underwent only minor adjustments to  wording or response categories.
                           Items that were no longer providing relevant  data (e.g., outdated technology items)
                           were eliminated, and the updated  instrument includes several high-impact practices
                           items that were not  previously on the core survey. The refreshed survey also includes
                           items about  library and active military/veteran services, as well as new demographic
                           items  about active military/veteran and college athlete status. CCSSE 2017 utilizes
                           a  single-year cohort (2017 CCSSE participant colleges only) in all of its data  analyses,
                           including the computation of benchmark scores. This cohort is  referred to as the
                           2017 CCSSE Cohort. The 2017 CCSSE Cohort includes 297  institutions from 40 states
                           and one Canadian province. One hundred thirty-five  are classified as small (&amp;lt;4,500),
                           74 as medium (4,500-7,999), 63 as large  (8,000-14,999), and 25 as extra-large institutions
                           (15,000+) credit students.  The 2018 CCSSE cohort will include participant colleges
                           from 2017 and 2018, and  in 2019 CCSSE cohorts will return to the Center's customary
                           three-year cohort  model.
                        </p>
                        
                        
                        <h3>Additional Reports for Valencia College</h3>
                        
                        <ul>
                           
                           <li>    <a href="documents/Valencia-College-Benchmarks.pdf" target="_blank">Valencia College Benchmarks</a>
                              
                           </li>
                           
                           <li><a href="documents/Valencia-College-Frequencies.pdf" target="_blank">Valencia College Frequencies</a></li>
                           
                           <li><a href="documents/Valencia-College-Benchmark-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Valencia College Benchmark by Enrollment (part-time and  full-time)</a></li>
                           
                           <li><a href="documents/Valencia-College-Frequencies-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Valencia College Frequencies by Enrollment (part-time and  full-time)</a></li>
                           
                           <li><a href="documents/Academic-Advising-and-Planning.pdf" target="_blank">Academic Advising and Planning</a></li>
                           
                           <li><a href="documents/Academic-Advising-and-Planning-by-Enrollment-part-time-and-full-time.pdf" target="_blank">Academic Advising and Planning by Enrollment (part-time and  full-time)</a></li>
                           
                           <li>
                              <a href="documents/Academic-Advising-and-Planning.pdf" target="_blank"></a><a href="documents/Achieving-the-Dream-AtD-Benchmarks.pdf" target="_blank">Achieving the Dream (AtD) Benchmarks</a>
                              
                           </li>
                           
                           <li><a href="documents/Achieving-the-Dream-AtD-Frequencies.pdf" target="_blank">Achieving the Dream (AtD) Frequencies</a></li>
                           
                           <li><a href="documents/Achieving-the-Dream-(AtD)-Benchmarks-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Achieving the Dream (AtD) Benchmarks by Enrollment (part-time  and full-time)</a></li>
                           
                           <li><a href="documents/Achieving-the-Dream-(AtD)-Frequencies-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Achieving the Dream (AtD) Frequencies by Enrollment  (part-time and full-time)</a></li>
                           
                           <li><a href="documents/Hispanic-Serving-Institutions-HSI-Benchmarks.pdf" target="_blank">Hispanic Serving Institutions (HSI) Benchmarks</a></li>
                           
                           <li><a href="documents/Hispanic-Serving-Institutions-HSI-Frequencies.pdf" target="_blank">Hispanic Serving Institutions (HSI) Frequencies</a></li>
                           
                           <li><a href="documents/Hispanic-Serving-Institutions-(HSI)-Benchmarks-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Hispanic Serving Institutions (HSI) Benchmarks by Enrollment  (part-time and full-time)</a></li>
                           
                           <li><a href="documents/Hispanic-Serving-Institutions-(HSI)-Frequencies-by-Enrollment-(part-time-and-full-time).pdf" target="_blank">Hispanic Serving  Institutions (HSI) Frequencies by Enrollment (part-time and full-time)</a></li>
                           
                           
                           <li> <a href="documents/Top-Performing-Ex-Large-Colleges-Benchmarks.pdf" target="_blank">Top-Performing Ex-Large Colleges Benchmarks</a>
                              
                           </li>
                           
                           <li><a href="documents/Top-Performing-Ex-Large-Colleges-Frequencies.pdf" target="_blank">Top-Performing Ex-Large Colleges  Frequencies</a></li>
                           
                           <li><a href="documents/Top-Performing-Ex-Large-Colleges-Benchmarks-by-Enrollment-part-time-and-full-time.pdf" target="_blank">Top-Performing Ex-Large Colleges Benchmarks  by Enrollment (part-time and full-time)</a></li>
                           
                           <li><a href="documents/Top-Performing-Ex-Large-Colleges-Frequencies-by-Enrollment-part-time-and-full-time.pdf" target="_blank">Top-Performing Ex-Large Colleges  Frequencies by Enrollment (part-time and full-time)</a></li>
                           
                           <li><a href="documents/Top-Performing-Ex-Large-Colleges-Academic-Advising-and-Planning-Frequencies.pdf" target="_blank">Top-Performing Ex-Large Colleges Academic  Advising and Planning Frequencies</a></li>
                           
                           <li><a href="documents/Top-Performing-Ex-Large-Colleges-Academic-Advising-and-Planning-Frequencis-by-Enrollment-pt-and-ft.pdf" target="_blank">Top-Performing Ex-Large Colleges Academic  Advising and Planning Frequencies by Enrollment
                                 (part-time and full-time)</a></li>
                           
                        </ul>
                        
                        
                        <h3>Discussion and Decision Guides</h3>
                        
                        <ul>
                           
                           <li><a href="documents/Discussion-and-Decision-Data-for-Writing.pdf" target="_blank">Discussion and Decision Guide for Writing</a></li>
                           
                        </ul>
                        
                        
                        <h3>Faculty Survey (CCFSSE) Report 2017</h3>
                        
                        <p>The Community College Faculty Survey of Student Engagement (CCFSSE)  elicits information
                           from faculty about their teaching practices, the ways they  spend their professional
                           time, both in and out of class, and their perceptions  regarding students' educational
                           experiences. CCFSSE reports enable  participating institutions to view faculty perceptions
                           of student engagement  alongside student responses. However, the side-by-side tables,
                           while  illustrative, are not entirely equivalent: That is, CCSSE asks students to
                           report  about their experiences across the period of the current academic year, while
                           faculty are asked to report their perceptions of their students' engagement  experiences
                           in a specific selected course. Nonetheless, the comparisons of  student and faculty
                           responses provide a useful prompt for campus discussions,  particularly in those areas
                           where students and faculty seem to be reporting  divergent perceptions of the same
                           experience. The 2017 CCFSSE Cohort includes  86 institutions from 28 states. The 2017
                           CCFSSE was completed by 9,577 faculty  members.
                        </p>
                        
                        <p><a href="documents/CCFSSE-and-CCSSE-2017-(N=551)-Frequencies-Executive-Summary-2017.pdf" target="_blank">Comparative CCFSSE and  CCSSE 2017 (N=551) Frequencies - Executive Summary 2017</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Survey Items Used </div>
                                 
                                 <div data-old-tag="th">Frequently Asked Questions</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/2017-CCSSE-Refresh-Items-Used.pdf" target="_blank">2017 CCSSE Refresh Items Used</a></li>
                                       
                                       <li><a href="documents/2017-CCSSE-Special-Focus-Items.pdf" target="_blank">2017 CCSSE Special Focus Items</a></li>
                                       
                                       <li><a href="documents/2017-CCFSSE-Refresh-Items-Used.pdf" target="_blank">2017 CCFSSE Refresh Items Used</a></li>
                                       
                                       <li><a href="documents/CCSSEOriginalSurveyQuestionsusedfrom2005-2012_000.pdf" target="_blank">2005-2015 CCSSE Items Used </a></li>
                                       
                                       <li><a href="documents/CCSSE_Promising_Practices.pdf" target="_blank">2015 CCSSE  Special Focus Items</a></li>
                                       
                                       <li><a href="documents/2015-CCFSSE-Items-used.pdf" target="_blank">2015 CCFSSE Items Used</a></li>
                                       
                                       <li><a href="documents/CCSSE_2013_Special_Focus_Items2013.pdf" target="_blank">2013 CCSSE Special Focus Items</a></li>
                                       
                                       <li><a href="documents/Student_Report_Information_Sheet.pdf" target="_blank">CCSSE Student Report Information Sheet</a></li>
                                       
                                       <li><a href="documents/Program_Code_Sheet.pdf" target="_blank">CCSSE Program Code Sheet</a></li>
                                       
                                       <li><a href="documents/IRRequestTemplate.xlsx" target="_blank">CCSSE Sample  Template</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Benchmarks-overview-2011-2015.pdf" target="_blank">Benchmarks overview 2011-15</a></li>
                                       
                                       <li><a href="documents/Benchmarks-calculated-2017.pdf" target="_blank">Benchmarks calculated 2017</a></li>
                                       
                                       <li><a href="documents/CCSSE_Student_FAQs.pdf" target="_blank">CCSSE Frequently Asked Questions</a></li>
                                       
                                       <li><a href="documents/CCFSSE_FAQs.pdf" target="_blank">CCFSSE Frequently Asked Questions</a></li>
                                       
                                       <li><a href="documents/Memo_to_Students.docx" target="_blank">E-mail  to Students</a></li>
                                       
                                       <li><a href="documents/Memo_to_Faculty.docx" target="_blank">E-mail  to Faculty</a></li>
                                       
                                       <li><a href="documents/Letter-to-Faculty-Scheduling.docx" target="_blank">Letter to Faculty - Scheduling</a></li>
                                       
                                       <li>
                                          <a href="documents/Thank_You.docx" target="_blank">Letter to Faculty - Thank You</a> 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3><strong>Archives</strong></h3>
                        
                        <h3>Valencia Summaries and Key Findings</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2015-CCSSE-Key-Findings-Executive-Summary.pdf" target="_blank">2015 CCSSE Key Findings - Executive Summary</a></li>
                           
                           <li><a href="documents/CCSSE2013Executivesummary.pdf" target="_blank">2013 CCSSE Key Findings - Executive Summary</a></li>
                           
                        </ul>
                        
                        <h3>Comparison Reports</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2015-CCSSE-Promising-Practices-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Promising Practices  by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/2015-CCSSE-Benchmarks-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Benchmarks by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/2015-CCSSE-Frequencies-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Frequencies by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/2015-CCFSSE-(N=431)-and-CCSSE-Valencia-Frequencies.pdf" target="_blank">2015 CCFSSE (N=431) and CCSSE Valencia Frequencies</a></li>
                           
                           <li><a href="documents/CCSSE2013FrequenciesComparisonReportFTandLessThanFT.pdf" target="_blank">2013 CCSSE Frequencies by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/CCFSSE2013StudentandFacultyNationalCohortFrequencies_000.pdf" target="_blank">2013 CCFSSE and CCSSE National Frequencies </a></li>
                           
                        </ul>
                        
                        <h3>Specialized Reports</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2015-CCSSE-Achieving-the-Dream-(ATD)-Benchmarks-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Achieving the Dream (AtD) Benchmarks by Enrollment (less than FT and FT)
                                 </a></li>
                           
                           <li><a href="documents/2015-CCSSE-Achieving-the-Dream-(ATD)-Frequencies-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Achieving the Dream (AtD) Frequencies by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/2015-CCSSE-Hispanic-Serving-Institutions-(HSI)-Benchmarks-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Hispanic-Serving Institutions (HSI) Benchmarks by Enrollment (less than
                                 FT and FT)</a></li>
                           
                           <li><a href="documents/2015-CCSSE-Hispanic-Serving-Institutions-(HSI)-Frequencies-by-Enrollment-(less-than-FT-and-FT).pdf" target="_blank">2015 CCSSE Hispanic-Serving Institutions (HSI) Frequencies by Enrollment (less than
                                 FT and FT)</a></li>
                           
                           <li><a href="documents/CCSSE2013BenchmarksComparisonReport-ATD-FTandLessThan-frequencies.pdf" target="_blank">2013 CCSSE Achieving the Dream ( AtD)  Frequencies by Enrollment (less than FT and
                                 FT)</a></li>
                           
                           <li><a href="documents/CCSSE2013BenchmarksComparisonReport-FloridaConsortium-FTandLessThan-frequencies.pdf" target="_blank">2013 CCSSE  Florida Consortium Frequencies by Enrollment (less than FT and FT)</a></li>
                           
                           <li><a href="documents/CCSSE2013BenchmarksComparisonReport-HispanicServing-FTandLessThan-frequencies.pdf" target="_blank">2013 CCSSE  Hispanic Serving Institutions (HSI) Frequencies by Enrollment (less than
                                 FT and FT) </a></li>
                           
                        </ul>
                        
                        <h3>CCSSE- Benchmarks- 2004, 2007, 2009, 2011, 2013, and 2015</h3>
                        
                        <ul>
                           
                           <li><a href="documents/2015-Benchmarks-Valencia.pdf" target="_blank">2015  Benchmarks- Valencia </a></li>
                           
                           <li><a href="documents/2013Benchmarks-Valencia.pdf" target="_blank">2013 Benchmarks- Valencia</a></li>
                           
                           <li><a href="documents/2011Benchmarks-Valencia.pdf" target="_blank">2011 Benchmarks- Valencia</a></li>
                           
                           <li><a href="documents/2009Benchmarks-Valencia.pdf" target="_blank">2009 Benchmarks- Valencia</a></li>
                           
                           <li><a href="documents/2007Benchmarks-Valencia.pdf" target="_blank">2007 Benchmarks- Valencia</a></li>
                           
                           <li><a href="documents/2004Benchmarks-Valencia.pdf" target="_blank">2004 Benchmarks- Valencia</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Articles</h3>
                        
                        <ul>
                           
                           <li><a href="documents/CCSSE-2017-Grove-Article-9-18-17.pdf" target="_blank">Grove Article  9-18-17</a></li>
                           
                           <li><a href="documents/CCSSE-2017-Grove-Article-1-26-17.pdf" target="_blank">Grove Article 1-26-17</a></li>
                           
                           <li><a href="documents/CCSSE-2017-Grove-Article-1-10-17.pdf" target="_blank">Grove Article 1-10-17</a></li>
                           
                           <li><a href="documents/CCSSE-Student-Success-Grove-Article-1-29-15.pdf" target="_blank">Grove Article 1-29-15</a></li>
                           
                           <li><a href="http://www.ccsse.org/docs/matter_of_degrees.pdf" target="_blank">A Matter of Degrees: Promising Practices for Community College Student Success (CCSSE
                                 2012 National Report)</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/ccsse.pcf">©</a>
      </div>
   </body>
</html>