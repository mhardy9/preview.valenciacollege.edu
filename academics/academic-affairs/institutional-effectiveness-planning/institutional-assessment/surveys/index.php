<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Surveys</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1">
                           
                           
                           <h2>Surveys</h2>
                           
                           <p>Valencia's<a href="../../strategic-plan/Educational-Commitment.html" target="_blank"> Strategic Planning</a> Process has been committed to further developing several key areas of the college
                              as well as the college's Education Ecosystem. 
                           </p>
                           
                           
                           
                           <h3>Online Learning</h3>
                           
                           
                           <h4><strong>EDUCAUSE ECAR Survey 2012 </strong></h4>
                           
                           <ul>
                              
                              <li>
                                 <a href="#ECAR">The EDUCAUSE Center for Applied Research (ECAR) survey</a> 
                              </li>
                              
                           </ul>
                           
                           <p>The 2012 online student study explored technology ownership, use, and perceptions
                              of technology among undergraduate students.
                           </p>
                           
                           <ul>
                              
                              <li>About ECAR <a href="http://www.educause.edu/ECAR/ECARHome/AboutECAR/94">http://www.educause.edu/ECAR/ECARHome/AboutECAR/94</a>
                                 
                              </li>
                              
                              <li>2012 Survey Information<a href="http://www.educause.edu/ECAR/Reference/StudentStudy"> http://www.educause.edu/ECAR/Reference/StudentStudy</a>
                                 
                              </li>
                              
                              <li>Valencia ECAR Info-Graphic <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/Surveys/documents/ECAR_Info_Graphic_Valencia_11-15-2012.pdf" target="_blank">http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/ECAR_Info_Graphic_Valencia_11-15-2012.pdf</a>
                                 
                              </li>
                              
                              <li>ECAR 2012 Report <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/Surveys/documents/ECAR2012ReportERS1208.pdf" target="_blank">http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/ECAR2012ReportERS1208.pdf</a>
                                 
                              </li>
                              
                           </ul>
                           
                           
                           
                           <hr>
                           
                           <h3>DirectConnect 2.0</h3>
                           
                           <p><strong>Development Education  Research 2016-17</strong></p>
                           
                           <ul>
                              
                              <li><a href="/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/Dev-Ed-Math-and-English-research-recommendations.pdf" target="_blank">Survey and Focus Groups 2016 for English and  Math</a></li>
                              
                           </ul>
                           
                           <p><em>Aligns  with Goal 2. Establish curricular alignment in specific programs, courses,
                                 and  career pathways. Objective: Develop advising recommendations based upon  curriculum
                                 alignment.</em></p>
                           
                           <p><strong>Title V East Advising  and Transfer Research 2016</strong></p>
                           
                           <ul>
                              
                              <li><a href="/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/Title-V-East-Advising-and-Transfer-Focus-Group-Data.pdf" target="_blank">Focus Groups 2016 about faculty advising and the  transfer process</a></li>
                              
                           </ul>
                           
                           <p><em>Aligns  with Goal 3. Prepare faculty and staff to advise students and families in
                                 the  transfer process. Objective: Clarify a common understanding of the way students
                                 experience the transfer process.</em></p>
                           
                           
                           
                           <hr>
                           
                           <h3>New Student Experience</h3>
                           	
                           <p><strong>NSE Updates November  2017</strong></p>
                           
                           <ul>
                              
                              <li>Persistence rates, success rates, key academic  threshholds, and grad rates as evidence
                                 of the impact of the <a href="/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/NSE-Updates-2017.pdf" target="_blank">New Student  Experience, from fall 2014-spring 2017.</a></li>
                              
                           </ul>
                           
                           <p><em>Aligns  with Vision for NSE: Students have personal connections within and beyond
                                 curricular and co-curricular experiences that lead to a successful completion of 
                                 18 college-level credits.</em><strong></strong></p>
                           
                           <p><strong>NSE Updates December  2016</strong></p>
                           
                           <ul>
                              
                              <li>Enrollment, persistence rates, success rates,  gpa, and CCSSE results as evidence
                                 of the impact of the <a href="/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/NSE-Updates-2016.pdf" target="_blank">New Student Experience,  from fall 2014-fall 2016</a>.
                              </li>
                              
                           </ul>
                           
                           <p><em>Aligns  with Vision for NSE: Students have personal connections within and beyond
                                 curricular and co-curricular experiences that lead to a successful completion of 
                                 18 college-level credits.</em></p>
                           
                           
                           
                           
                           <hr>
                           
                           <h3>Part-time Faculty Engagement</h3>
                           
                           <p><strong>Community College Faculty Survey of Student Engagement (CCFSSE) 2017</strong></p>
                           	
                           <p>		
                              <ul>
                                 			  
                                 <li><a href="/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/documents/CCFSSE-2017-by-Part-Time-and-Full-time-Faculty.pdf" target="_blank">CCFSSE 2017 by Part-Time and Full-time Faculty</a></li>
                                 			  
                              </ul>
                              
                           </p>
                           	
                           <hr>
                           	   
                           <h3>Important Links</h3>
                           
                           <p>        
                              
                              		<a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"> Learning Assessment Data Definitions</a></p>
                           	
                           	
                           	
                           	
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/index.pcf">©</a>
      </div>
   </body>
</html>