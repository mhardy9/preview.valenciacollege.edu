<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/calendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h3>Valencia's Institutional Assessment Calendar</h3>
                        
                        <h3>2017-2018 Academic Year</h3>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Annual-Cycle-2017-2018.pdf" target="_blank">Updated as of 8/2/2017</a>. Please contact Darren Smith <a href="mailto:dsmith335@valenciacollege.edu" rel="noreferrer">dsmith335@valenciacollege.edu</a> for the most recent version.
                        </p>
                        
                        <h3>Learning Outcomes Leaders (LOL) Meeting with Deans</h3>
                        
                        <ul>
                           
                           <li>Friday Sept. 22, 9 am-noon School of Public Safety (SPS) Auditorium</li>
                           
                           <li>Friday Feb. 9, 9 am-noon, Osceola Campus Auditorium</li>
                           
                        </ul>
                        
                        <h3>Assessment Day – all faculty and deans</h3>
                        
                        <ul>
                           
                           <li>Thursday May 3, 9 am - noon A.S. programs (location TBA; check with deans)</li>
                           
                           <li>Friday May 4, 9 am - noon General Education and other programs (library, etc.) Lake
                              Nona Campus
                           </li>
                           
                        </ul>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Contact List for Tools Used at the College Related to Learning Assessment</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Course Information Management (Curriculum)-- software supporting course outlines and
                                       the online catalog.
                                    </p>
                                    
                                    <p>Contact: Glenn Ricci <a href="mailto:gricci2@valenciacollege.edu">gricci2@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>AccuSQL (Learning Support)-- tracking software for student use of learning support
                                       services.
                                    </p>
                                    
                                    <p>Contact: Wes Sondermann <a href="mailto:wsondermann@valenciacollege.edu">wsondermann@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Blackboard (Office of Information Technology) – learning management system.</p>
                                    
                                    <p>Contact: <a href="mailto:onlinehelp@valenciacollege.edu">onlinehelp@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>CourseEval (Institutional Assessment)-- student feedback software used to launch surveys
                                       and provide supervisor and faculty level access to reports.
                                    </p>
                                    
                                    <p>Contact: <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>EAC Visual – Reporting tool for tests and rubrics being piloted by New Student Experience
                                       (NSE) faculty and faculty in Nursing.
                                    </p>
                                    
                                    <p>Contact: <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Libguides (Library)-- resource webpages that librarians will help you create to inform
                                       faculty or students.
                                    </p>
                                    
                                    <p>Contact: Courtney Moore <a href="mailto:cmoore71@valenciacollege.edu">cmoore71@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Navigator (Internship and Workforce)-- workforce information tool available to all
                                       faculty and students.
                                    </p>
                                    
                                    <p>Contact: Debra Sembrano <a href="mailto:dsembrano@valenciacollege.edu">dsembrano@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>OneDrive (Information Technology)-- online storage available to all faculty and staff
                                       under Atlas password protection for file sharing.
                                    </p>
                                    
                                    <p>Contact: <a href="mailto:vnguyen30@valenciacollege.edu">vnguyen30@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Qualtrics (Institutional Assessment)-- online tool available for faculty and staff
                                       to use for assessment and surveys.
                                    </p>
                                    
                                    <p>Contact: Nichole Jackson <a href="mailto:njackson18@valenciacollege.edu">njackson18@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Tableau (Institutional Research, Institutional Assessment, and Human Resources)--
                                       data visualization software used to prepare report that can be viewed interactively
                                       using Tableau Reader.
                                    </p>
                                    
                                    <p>Contact: Cissy Reindahl <a href="mailto:creindahl1@valenciacollege.edu">creindahl1@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Xitracs (Curriculum and Assessment)--NEW planning tool for the strategic plan and
                                       program outcomes assessment. Contact: Noelia Maldonado Rodriguez <a href="mailto:nmaldonado5@valenciacollege.edu">nmaldonado5@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        <a title="Glossary" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank"><img title="Click to View Glossary" src="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/glossary.jpg" alt="Glossary" width="245" height="60" border="0"></a></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/calendar.pcf">©</a>
      </div>
   </body>
</html>