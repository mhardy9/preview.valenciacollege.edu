<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h3><em>Resources</em></h3>
                        
                        <div>
                           
                           <h3>Critical Thinking</h3>
                           
                           <div>
                              
                              <h2>The Critical Thinking Outcome in General Education: An Overview</h2>
                              
                              <p>The critical thinking outcome adopted by Valencia College faculty members (December
                                 2017) states: students will be able to effectively analyze, evaluate, synthesize,
                                 and apply information and ideas from diverse sources and disciplines. In a review
                                 of the assessment work led by faculty members since 2002 three indicators emerged
                                 in common across the disciplines, asking that students address: 1) use of evidence;
                                 2) bias; and 3) context. These three indicators are defined in this way by faculty
                                 members in the Gen Ed Checklist for assessing student artifacts:
                              </p>
                              
                              <p>Across all of the State colleges it has been specified that "each general education
                                 core course option must contain high-level academic and critical thinking skills and
                                 common compencies that students must demonstrate to successfully complete the course".
                                 (<strong>House Bill 7135 Florida Statute 1007.25 </strong>General education courses, common prerequisites, other degree requirements.)
                              </p>
                              
                              <h3><strong>Video - The Intellectual Standards: </strong></h3>
                              
                              <h3><strong>An Introduction by Gary Meegan</strong></h3>
                              
                              <p>This is a two-minute tutorial on The Intellectual Standards in critical thinking.
                                 The information in the video is based on the work of Richard Paul and Linda Elder.
                                 For an in depth look at the Intellectual Standards and The Elements of Thought go
                                 to: <a href="http://www.TheElementsofThought.org">www.TheElementsofThought.org</a>. (Published on Apr 2, 2014 / 3.32 minutes.) <a href="https://www.youtube.com/watch?v=gcxhalSk73k">https://www.youtube.com/watch?v=gcxhalSk73k</a></p>
                              
                              <h3><strong><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CT-ElderandPaulExcerpt7-10-2015.pdf" target="_blank">Intellectual Standards for Students at Valencia</a></strong></h3>
                              
                              <p>This handout outlines the “intellectual standards” for analytic thinking and it includes
                                 a one-page related set of guiding questions for students to use when reading an article,
                                 essay, or chapter in your class.&nbsp; One or both of these pages may be useful in your
                                 course as we seek to create a shared understanding related to what students are learning
                                 and the ways we are discussing the “Critical Thinking” outcome that spans General
                                 Education. (Note that the video above can also be shown to students). This is excerpted
                                 from the book which is available through the related Faculty Development courses or
                                 can be purchased online.&nbsp; Paul, R. &amp; Elder, L. (2010). <em>The thinker’s guide to analytic thinking</em>. Dillon Beach, CA: Foundation for Critical Thinking Press.&nbsp; http://www.criticalthinking.org/store/products/analytic-thinking-2nd-edition/171
                              </p>
                              
                              <h3>&nbsp;</h3>
                              
                              <h3>Researching Regarding ways to teach the skills needed for critical thinking</h3>
                              
                              <ul>
                                 
                                 <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Howcollegeaffectsstudents-CriticalThinkingembeddedpreferred-final_000.pdf.html">Evidence in the research literature</a> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Howcollegeaffectsstudents-CriticalThinkingembeddedpreferred-final_000.pdf.html">regarding the positive impact of embedding critical thinking skills across disciplines.</a> These passages were excerpted for discussion from…. Pascarella, E. T., &amp;Terenzini,
                                    RT. (2005).How College Affects Students, Volume 2, A Third. Decade of Research. San
                                    Francisco, CA: Jossey-Bass.
                                 </li>
                                 
                                 <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Instructionalinterventionsaffectingcriticalthinkingmetaanalysisabrami-rer081_000.pdf.html">A research-based overview of interventions shown to impact critical thinking skills</a>. Abrami PC, Bernard RM, Borokhovski E, Wadem A, Surkes M A, Tamim R, Zhang D. 2008.
                                    Instructional interventions affecting critical thinking skills and dispositions: A
                                    stage 1 meta-analysis. Review of Educational Research. 78:1102-1134.
                                 </li>
                                 
                              </ul>
                              
                              <h3>Critical Thinking &amp; Gen Ed + Multiple Choice Question &amp; Essay Question Workshops 10/10/2014</h3>
                              
                              <p><strong>Dr. Steven Downing's Presentation </strong></p>
                              
                              <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/MCQPresentationItem_Writing_ValenciaDowning101014_000.pptx">Creative Effective Multiple-Choice Items / Designing Items to Test Critical Thinking</a></p>
                              
                              <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/MCQSamplesHARDCOPY_smd_BLUE_YELLOWgroups_000.pptx">Multiple-Choice Questions Sample</a></p>
                              
                              <p><strong>Dr. Laura Blasi's Presentation </strong></p>
                              
                              <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Presentation-EssayQuestionsWorkshop-Blasi10-9-2014_000.pdf">Essay Question Workshop</a></p>
                              
                              <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Handout1-Blue-AQuickGuidetoBetterTestsfromBEST1-6-2011_000.pdf">Handout 1</a>; <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Handout2-ThesaurusofVerbsandMLOCriteria_000.pdf">Handout 2</a>; <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Handout3-Pink-ConstructingEssayExams-StudyGuidesOverview_000.pdf">Handout 3</a>; <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Handout4-Yellow-Quiz-ChoosingbetweenObjectiveandSubjectiveTestItems_000.pdf">Handout 4</a></p>
                              
                              <p><strong>Drs. Karen Borglum and Laura Blasi's Presentation</strong></p>
                              
                              <div>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/CriticalThinkingandGEWorktoDate10-9-2014FINALupdated2.pdf" target="_blank">General Education - Critical Thinking and Assessment</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Ethical Responsibility</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CopyoftheDIT-2.pdf" target="_blank">Copy of the Defining Issues Test (DIT-2)</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/BebeauDefiningIssuesTestandFourComponentModel20027431201.pdf" target="_blank">The Definings Issues Test and the Four Component Model: contributions to professional
                                       education</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/BaileyDoestheDefiningIssuesTestMeasureEthical201160106632.pdf" target="_blank">Does the Defining Issues Test Measure Ethical Judgement Ability or Political Position?</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/NarvaezBockMoralSchemasDTI2002.pdf" target="_blank">Moral Schemas and Tacit Judgement or How the Defining Issues Test is Supported by
                                       Cognitive Science</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GeneralEducation-AssessmentEthicalReasoningDIT-2-Overview.pdf" target="_blank">General Education – Possible Assessment Related to Ethical Reasoning </a><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GeneralEducation-AssessmentEthicalReasoningDIT-2-Overview.pdf">Defining Issues Test of ethical judgment (DIT/DIT-2)</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Information Literacy</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>Threshold Achievement Test for Information Literacy (TATIL)</h2>
                                 
                                 <h3>Campus Leaders:</h3>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">East</div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p><a href="mailto:cmoore71@valenciacollege.edu">Courtney Moore</a></p>
                                             
                                             <p><a href="mailto:ddalrymple1@valenciacollege.edu">Diane Dalrymple</a></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Winter Park</div>
                                          
                                          <div data-old-tag="td"><a href="mailto:bmittag@valenciacollege.edu">Ben Mittag</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Osceola and Lake Nona</div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p><a href="mailto:kbest7@valenciacollege.edu">Karene Best</a></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">West</div>
                                          
                                          <div data-old-tag="td"><a href="mailto:rseguin@valenciacollege.edu">Regina Seguin</a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>TATIL FAQs</h3>
                                 
                                 <p><strong>What is the Threshold Achievement Test for Information Literacy (TATIL)?</strong></p>
                                 
                                 <p>The TATIL is a 4-module assessment tool that measures the information literacy abilities
                                    of students. It is specifically designed to address the threshold concepts introduced
                                    in the Association of College and Research Libraries' (ACRL) <em>Framework for Information Literacy</em>.
                                 </p>
                                 
                                 <p><strong>TATIL Modules:</strong></p>
                                 
                                 <p>Evaluating Process and Authority</p>
                                 
                                 <p>Strategic Searching</p>
                                 
                                 <p>Research and Scholarship</p>
                                 
                                 <p>The Value of Information</p>
                                 
                                 <p>For more information, visit: <a href="https://thresholdachievement.com/" target="_blank">https://thresholdachievement.com/</a></p>
                                 
                                 <p><strong>Where can I find more information about Valencia's Information Literacy Assessment
                                       Pilot?</strong></p>
                                 
                                 <p>Please visit: <a href="https://valenciacollege.libapps.com/libguides/admin_c.php?g=386673" target="_blank">https://valenciacollege.libapps.com/libguides/admin_c.php?g=386673</a></p>
                                 
                                 <p><strong>What should I do if I am interested in participating?</strong></p>
                                 
                                 <p>Please visit: <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_eQViKzmn5k8LW4J" target="_blank">http://valenciacollege.qualtrics.com/SE/?SID=SV_eQViKzmn5k8LW4J</a></p>
                                 
                                 <p>Or contact your campus leader:</p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">East</div>
                                          
                                          <div data-old-tag="td">Diane Dalrymple
                                             
                                             <p><a href="mailto:ddalrymple1@valenciacollege.edu">ddalrymple1@valenciacollege.edu</a></p>
                                             
                                             <p>407-582-2887</p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">Courtney Moore
                                             
                                             <p><a href="mailto:cmoore71@valenciacollege.edu">cmoore71@valenciacollege.edu</a></p>
                                             
                                             <p>407-582-2883</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Winter Park</div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Ben Mittag</p>
                                             
                                             <p><a href="mailto:bmittag@valenciacollege.edu">bmittag@valenciacollege.edu</a></p>
                                             
                                             <p>407-582-6832</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Osceola and Lake Nona</div>
                                          
                                          <div data-old-tag="td">Karene Best
                                             
                                             <p><a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a></p>
                                             
                                             <p>407-582-4315</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">West</div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Regina Seguin</p>
                                             
                                             <p><a href="mailto:rseguin@valenciacollege.edu">rseguin@valenciacollege.edu</a></p>
                                             
                                             <p>407-582-1361</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><strong>Where will students take the test?</strong></p>
                                 
                                 <p>Faculty participating in the pilot may book a computer room by contacting a librarian.
                                    A librarian will also be present to proctor the test. In addition, students can be
                                    given a link to take the test off campus at their convenience. This option allows
                                    students the flexibility to break the testing period up into smaller sections.
                                 </p>
                                 
                                 <p><strong>How soon will I get the results?</strong></p>
                                 
                                 <p>Test results for the Fall should be available in early Spring 2016.</p>
                                 
                                 <p><strong>When is the test offered?</strong></p>
                                 
                                 <p>The 4 modules are being released at different times throughout the 2015-16 year. The
                                    release time frame is as follows:
                                 </p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Evaluating Process &amp; Authority</div>
                                          
                                          <div data-old-tag="td">Now Available</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Strategic Searching</div>
                                          
                                          <div data-old-tag="td">Now Available</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Research &amp; Scholarship</div>
                                          
                                          <div data-old-tag="td">Upcoming</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">The Value of Information</div>
                                          
                                          <div data-old-tag="td">Upcoming</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p><strong>What are the benefits of participating in this pilot?</strong></p>
                                 
                                 <p>Often times, instructors get to see the final product of a paper or speech but not
                                    the process the student took to complete the assignment. The TATIL will allow us to
                                    peek behind the curtain and gain new insight into how our students find, use, and
                                    interact with information. As a result, instructors will have guidance on how to teach
                                    to students' unique information needs.
                                 </p>
                                 
                                 <p>Additionally, the TATIL can help refine assignments by highlighting areas in the research
                                    process where students may struggle. Instructors would then have more guidance about
                                    the type of assignments, sources, and documentation to include for coursework.
                                 </p>
                                 
                                 <p><strong>Do students have to take the test in one sitting?</strong></p>
                                 
                                 <p>While completing the test in one sitting is preferable, students may reopen an uncompleted
                                    test.
                                 </p>
                                 
                                 <p><strong>What if a student misses the test date? Will there be a makeup day?</strong></p>
                                 
                                 <p>Each module will be administered over a short period of time. Arrangements can be
                                    made for students who might have missed a class session to either test with another
                                    class or get an individual student key to take the test on their own.
                                 </p>
                                 
                                 <p><strong>What is ACRL's <em>Framework for Information Literacy</em>?</strong></p>
                                 
                                 <p>ACRL's <em>Framework</em> outlines 6 foundational concepts that information literate students should be able
                                    to understand and demonstrate. The <em>Framework</em> is a product of collaboration among librarians, faculty, information professionals,
                                    and other affiliated parties in order to address the complexities of the current information
                                    ecosystem.
                                 </p>
                                 
                                 <p>The six concepts are as follows:</p>
                                 
                                 <p>Authority is Constructed and Contextual</p>
                                 
                                 <p>Information Creation as a Process</p>
                                 
                                 <p>Information has Value</p>
                                 
                                 <p>Research as Inquiry</p>
                                 
                                 <p>Scholarship as Conversation</p>
                                 
                                 <p>Searching as Strategic Exploration</p>
                                 
                                 <p>For more information, visit: <a href="http://www.ala.org/acrl/standards/ilframework" target="_blank">http://www.ala.org/acrl/standards/ilframework</a></p>
                                 
                                 <p><strong>Will the TATIL be offered every year?</strong></p>
                                 
                                 <p>Future offerings of the TATIL are dependent upon our pilot results of the test.</p>
                                 
                                 <p><strong>What do questions on the TATIL look like?</strong></p>
                                 
                                 <p>The TATIL is composed of multiple choice questions that highlight the critical thinking
                                    process and skills needed in information literacy.
                                 </p>
                                 
                                 <p><strong>Will results for individual students be available?</strong></p>
                                 
                                 <p>No. The TATIL will report on student results as a cohort.</p>
                                 
                                 <p><strong>Can online students participate in the pilot?</strong></p>
                                 
                                 <p>Yes. Please contact a member of the pilot's committee for further information.</p>
                                 
                                 <p><strong>What is Project Standardized Assessment of Information Literacy Skills (SAILS)?</strong></p>
                                 
                                 <p>The Project for Standardized Assessment of Information Literacy Skills (SAILS) is
                                    a Kent State University initiative to develop an instrument for standardized assessment
                                    of information literacy skills. From the outset, they envisioned a standardized tool
                                    that is valid and reliable; contains items not specific to a particular institution
                                    or library but rather assesses at an institutional level; is easily administered;
                                    and provides for both external and internal benchmarking. With such a tool, we will
                                    have the ability to measure information literacy skills, gather national data, provide
                                    norms, and compare information literacy measures with other indicators of student
                                    achievement.
                                 </p>
                                 
                                 <p>They applied systematic instructional design to develop the test questions, using
                                    item response theory (IRT) as the measurement model, and thus created a bank of test
                                    questions of varying difficulty levels to measure information literacy. The test items
                                    are based on the ACRL Information Literacy Competency Standards for Higher Education.
                                    Use of these standards maximizes the ability of the instrument to be used by a wide
                                    variety of academic institutions for internal and external benchmarking.
                                 </p>
                                 
                                 <p>Beginning in 2001, Project SAILS brought together a team of experts in librarianship,
                                    test design and measurement, data analysis, and programming, with a three-year research
                                    and development phase involving more than 80 higher education institutions in the
                                    U.S. and Canada culminated in 2006 in the production version of the SAILS test of
                                    information literacy skill.
                                 </p>
                                 
                                 <p>For more information, visit: <a href="https://www.projectsails.org/AboutSAILS" target="_blank">https://www.projectsails.org/AboutSAILS</a></p>
                                 
                                 <h3>Library</h3>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/LibrarianInvolvementinPLOA.pptx.html" target="_blank">PLOA with the Library</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ResearchLog-Fall2014.docx.html" target="_blank">Research Log</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/RubricMay2013.docx.html" target="_blank">Rubric</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ILRubricFall08.pdf.html" target="_blank">Rubric for Assessing Information Literacy</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Interpersonal</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GeneralEducation-AssessmentreInterpersonalIMPACCT-Overview.pdf" target="_blank">Related to Interpersonal Communication: The Interactive Media Package for the Assessment
                                       of Communication and Critical Thinking (IMPACCT)</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/IMPACCTStudentProfile_p1.pdf" target="_blank">IMPACCT Student Profile</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Spitzberg2011IMPACCT-assessmentofcommunication-59214466.pdf" target="_blank">The Interactive Media Package for Assessment of Communication and Critical Thinking
                                       (IMPACCT): Testing a Programmative Online Communication Competence Assessment System</a></p>
                                 
                                 <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SpeechRubric13-14.pdf">Interpersonal Communications Obs Rubric 10-11-2013 for Speech and SLS </a></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Resources.pcf">©</a>
      </div>
   </body>
</html>