<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/forms_reports.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h3><em><a href="#Forms">Forms</a>, <a href="#Reports">Reports</a>, and <a href="#Tools">Tools</a></em></h3>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/AssessmentDaySurvey-VIA-5yearanalysis72916.pdf" target="_blank">Assessment Day Survey - VIA - 5 Year Analysis</a></p>
                        
                        <p>Assessment Day 2015 Handouts</p>
                        
                        <h3>All Programs and Disciplines</h3>
                        
                        <ul>
                           
                           <li>Assessment Day 2015: <a href="http://wp.valenciacollege.edu/thegrove/assessment-day-2015-a-culture-of-evidence/" target="_blank">A Culture of Evidence</a> by Nichole Jackson
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DiscussionTopicsHandoutforPLOALeaders-FINAL.pdf" target="_blank">Optional Discussion Topics Handout for PLOA Leaders Final March 2015</a>&nbsp;
                           </li>
                           
                        </ul>
                        
                        <h3><strong>General Education</strong></h3>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SlidesReGenEdAssessment5-7-2015toshare.ppt" target="_blank">PowerPoint presentation for General Education Assessment 2015 </a></li>
                           
                        </ul>
                        
                        <h3>Leading Change – Strategies for Growing Academic Initiatives for Student Success:
                           A Visit with Abby Parcell
                        </h3>
                        
                        <p>On Thursday May 7th Abby Parcell &amp;lt; <a href="http://www.mdcinc.org/about-us/our-people/abby-parcell">http://www.mdcinc.org/about-us/our-people/abby-parcell</a> &amp;gt; from MDC joined directors and staff from a range of academic initiatives (like
                           LinC) at 9 am on the West campus to share some of the challenges faced, lessons learned,
                           and effective practices in the development of meaningful, life-changing efforts that
                           directly impact the lives of our students. She shared what she has learned from her
                           work in AtD assisting community colleges in order to scale up effective practices.
                           Participants learned strategies for understanding impact and scaling-up effective
                           practices related to initiatives across the college – beyond the NSE – that directly
                           impact student success. The materials below were used in her session.
                        </p>
                        
                        <ul>
                           
                           <li>
                              
                              <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DraftInviteParticipantList-AbbyParcellMay7final.docx" target="_blank">Participant List </a></p>
                              
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Coburn_RethinkingScale_2003_Berkeley.pdf" target="_blank">Rethinking Scale: Moving Beyond Numbers to Deep and Lasting Change. by Cynthia E.
                                 Coburn. Berkeley, 2003 </a></li>
                           
                        </ul>
                        
                        <p>Assessment Day 2014 Handouts</p>
                        
                        <h3>All Programs and Disciplines</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/OnlineOrganizerReportingResults-CreatingPlansMay2014byOutcomesameformRTF.rtf" target="_blank">Online Organizer Reporting Results - Creating Plans May 2014 by Outcome same form
                                 RTF</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/OnlineOrganizerReportingResults-CreatingPlansMay2014byOutcomeWORD.docx" target="_blank">Online Organizer Reporting Results - Creating Plans May 2014 by Outcome</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DiscussionTopicsHandoutforPLOALeaders-FINAL_000.pdf" target="_blank">Optional Discussion Topics Handout for PLOA Leaders - FINAL April 2014</a></li>
                           
                        </ul>
                        
                        <h3>General Education</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GenEdOutcomesMap--2014-2015April2014_000.pdf" target="_blank">Gen Ed Outcomes Map -- 2014-2015 April 2014 </a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GenEdOutcomesMap--2014-2015April2014sampleslides.pptx">Gen Ed Outcomes Map -- 2014-2015 April 2014 sample slides</a></li>
                           
                        </ul>
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>Assessment Day 2013 Handouts</h2>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/EndofCyclePLOAPTemplateAY2012-2013forMay2013v3.docx" target="_blank">End of Term Template</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CopyofAssessmentDay2013MailingList-5-1-2013UPDATED-JK.xlsx" target="_blank">PLOA contact list w rooms Assessment Day 2013 (5-1-2013 UPDATED)</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DiscussionTopicsHandoutforPLOALeaders-April182012-2013FINAL.pdf" target="_blank">Discussion Topics Handout for PLOA Leaders</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/ProgramLearningOutcomesAssessmentRubric.docx" target="_blank">Optional Rubric (Draft- April 19, 2013)</a></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Announcements re <u><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/IECompensationPlanBOTAnnouncementfromMay2013.pdf" target="_blank">IE – Compensation – Plan 2013-2014</a> </u><br> with the BOT from Assessment Day, May 2013
                           </li>
                           
                        </ul>
                        
                        <p>Other Resources</p>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/IErecommendation2013-2014dhd.pdf" target="_blank"><strong>IE Faculty Recommendation</strong></a> February 2014
                           </li>
                           
                        </ul>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/OnlineOrganizerHelpfulHandoutMarch2014OutcomesSpecializationsTCs.pdf">Online Organizer Helpful Handout - Outcomes Specializations TCs</a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/TemplateforOnlineOrganizerPlanandReportTab-3-2014bothintroandoutcomes.pdf">Template for Online Organizer Plan and Report Tab - Intro and Outcomes </a></p>
                        
                        <p>Forms</p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/2010-11ProgramLearningOutcomesAssessmentPlanTemplate.docx" target="_blank">PLOAP Start Template AY 10-11 from October 2011 </a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/PLOAPTemplate.docx" target="_blank">PLOAP End Template AY 11-12 from May 2012</a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/ProgramLearningOutcomesAssessmentPlanTemplateSTARTValenciaCollegev7.docx" target="_blank">PLOAP Start Template AY 12-13 From August 2012</a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/EndofCyclePLOAPTemplateAY2012-2013forMay2013v4.docx" target="_blank">PLOAP End Template AY 12-13 from April 2013</a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/CopyofBlankOutcomesAlignmentWorkbook-savewithyourprogramname-datehere.xlsx" target="_blank">2012-2013 Program Alignment Workbook Template</a></p>
                        
                        <p><a id="Reports" name="Reports"></a>Reports
                        </p>
                        
                        <h2><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ProgramLearningOutomes-ProgressandPromise2-3-2013FINALPAPER-.pdf" target="_blank">Program Learning Outcome Assessment: Progress &amp; Promises 2013 </a></h2>
                        
                        <p>This report provides an overview of the annual assessment cycle at Valencia College;
                           background on its development; and an inventory of the methods used by faculty and
                           staff members.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Workforce1-ByProgram07-08final.pdf" target="_blank">Workforce 1- By Program 07-08 </a></li>
                           
                           <li><span><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Workforce2-OverallReport09-10.pdf" target="_blank">Workforce 2- Overall Report 09-10 </a></span></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPFastFactsReport.pdf" target="_blank">Program Learning Outcomes Assessment Plans Assessment Pattern Handout</a></li>
                           
                        </ul>
                        
                        <p>SACS Reaffirmation Reports Submitted 3/18/2013 Related to Assessment</p>
                        
                        <p>The institution identifies expected outcomes, assesses the extent to which it achieves
                           these outcomes, and provides evidence of improvement based on analysis of the results
                           in each of the following areas:
                        </p>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SACS3.3.1.1.pdf" target="_blank"> 3.3.1.1- educational programs, to include student learning outcomes </a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SACS3.3.1.3.pdf" target="_blank">3.3.1.3- educational support services</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SACS3.5.1.pdf" target="_blank">3.5.1- general education</a></li>
                           
                        </ul>
                        
                        <p><a id="Tools" name="Tools"></a>Tools
                        </p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ThesaurusofVerbsandMLOCriteria.pdf" target="_blank">Verbs for Writing Outcomes: Thesaurus and Criteria</a></p>
                        
                        <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/LOWorksheet.doc" target="_blank">Program Learning Outcomes Assessment Worksheet</a></p>
                        
                        <p><strong>Rubric for Assessing the Quality of Academic Program Learning Outcomes</strong></p>
                        
                        <p><a href="http://www.wascsenior.org/findit/files/forms/Program_Learning_Outcomes_Rubric_4_08.pdf" target="_blank">http://www.wascsenior.org/findit/files/forms/Program_Learning_Outcomes_Rubric_4_08.pdf</a></p>
                        
                        <p><strong>VALUE: Valid Assessment of Learning in Undergraduate Education Rubrics</strong></p>
                        
                        <p><a href="http://www.aacu.org/value/rubrics" target="_blank">http://www.aacu.org/value/rubrics</a></p>
                        
                        <p><strong>Small Sample Size Calculator</strong><br> University of North Carolina Greensboro
                        </p>
                        
                        <p><a href="http://assessment.uncg.edu/academic/materials/ss.html">http://assessment.uncg.edu/academic/materials/ss.html</a></p>
                        
                        <p><strong>Rubric for Assessing the Use of Portfolios for Assessing Program Learning Outcomes</strong></p>
                        
                        <p><a href="http://wascsenior.org/findit/files/forms/Portfolio_Rubric_4_08.pdf" target="_blank">http://wascsenior.org/findit/files/forms/Portfolio_Rubric_4_08.pdf</a></p>
                        
                        <p><strong>Rubric for Assessing the Use of Capstone Experiences to Assess Program Learning Outcomes</strong></p>
                        
                        <p><a href="http://www.wascsenior.org/findit/files/forms/Capstone_Rubric_4_08.pdf" target="_blank">http://www.wascsenior.org/findit/files/forms/Capstone_Rubric_4_08.pdf</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/forms_reports.pcf">©</a>
      </div>
   </body>
</html>