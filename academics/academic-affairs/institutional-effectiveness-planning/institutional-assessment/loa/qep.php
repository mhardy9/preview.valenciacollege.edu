<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/qep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Quality Enhancement Plan (QEP)</h2>
                        
                        
                        <div>
                           
                           <h3>Our QEP</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p><a href="documents/NSE-Updates-December-2016.pptx" target="_blank"><strong>NSE Updates - December 2016</strong></a></p>
                                 
                                 
                                 <p> For information about the development of the Quality Enhancement Plan (QEP), please
                                    see: <a href="../../../../about/our-next-big-idea/index.html" target="_blank">http://preview.valenciacollege.edu/ournextbigidea/</a></p>
                                 <br>                  
                                 
                                 <p>For information about the New Student Experience (NSE), please see: <a href="../../../new-student-experience/index.html" target="_blank">http://preview.valenciacollege.edu/academic-affairs/new-student-experience/</a></p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Assessment Instruments</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The QEP assessment includes learning artifact assessment  using common rubrics developed
                                    by the faculty, Student Feedback on Instruction (SFI)  surveys, and student engagement
                                    as reported on the Community College Survey of  Student Engagement (CCSSE).      
                                    
                                    
                                 </p>
                                 
                                 <h3> Learning Artifact  Assessment                        </h3>
                                 
                                 <p>  NSE faculty assess common assignments in the SLS 1122 New  Student Experience course.
                                    The Academic Blueprint (originally the My Education  Plan or MEP Assignment) rubric
                                    was developed during summer 2014 and updated in  summer 2015 and summer 2017. The
                                    Final Story Project rubric was developed  during summer 2014 and updated in summer
                                    2015 and summer 2017. Rubrics are  developed and updated on a regular basis. In fall
                                    2016 a new rubric for  Interpersonal Communication was incorporated into the assessment
                                    plan  associated with the Personal Connection Co-Curricular Reflection for onsite
                                    students and the Career Interview for online students. The rubric and  associated
                                    assignments and learning activities were developed in collaboration  with the Interpersonal
                                    Communication faculty.                        
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/2017-Co-Curricular-Rubric-Onsite.pdf" target="_blank">2017 Co-Curricular Rubric - Onsite</a></li>
                                    
                                    <li><a href="documents/2017-Career-Interview-Rubric-Online.pdf" target="_blank">2017 Career Interview Rubric - Online</a></li>
                                    
                                    <li><a href="documents/2017-Academic-Blueprint-Rubric.pdf" target="_blank">2017 Academic Blueprint Rubric</a></li>
                                    
                                    <li><a href="documents/2017-Final-Story-Project-Rubric.pdf" target="_blank">2017 Final Story Project Rubric</a></li>
                                    
                                    <li><a href="documents/2016-Personal-Connection-Co-curricular-Rubric-Onsite.pdf" target="_blank">  2016 Personal  Connection Co-curricular Rubric - Onsite</a></li>
                                    
                                    <li><a href="documents/2016-Career-Interview-Rubric-Online.pdf" target="_blank">2016 Career  Interview Rubric - Online</a></li>
                                    
                                    <li><a href="documents/2015-Academic-Blueprint-Rubric-.pdf" target="_blank">2015 Academic  Blueprint Rubric</a></li>
                                    
                                    <li><a href="documents/2015-Final-Story-Project-Rubric.pdf" target="_blank">2015 Final Story  Project Rubric</a></li>
                                    
                                    <li><a href="documents/2014-Final-Story-Project-Rubric.pdf" target="_blank">2014 Final Story  Project Rubric</a></li>
                                    
                                    <li><a href="documents/2014-MEP-Rubric-AA-degree-assignment.pdf" target="_blank">2014 MEP Rubric -  AA degree assignment</a></li>
                                    
                                    <li><a href="documents/2014-MEP-Rubric-AS-degree-assignment.pdf" target="_blank">2014 MEP Rubric -  AS degree assignment                        
                                          </a></li>
                                    
                                 </ul>
                                 
                                 <h3>Community College  Survey of Student Engagement (CCSSE)</h3>
                                 
                                 <p>When the New Student Experience (NSE) was being developed it  was intended to draw
                                    on student success and engagement strategies that have  been shown to be effective
                                    for community college students across the country as  documented over time by the
                                    CCSSE. Specifically, among the research questions  associated with the NSE are explicit
                                    questions meant to help us better  understand our students' needs and their perceptions
                                    of learning and  college-readiness and to measure their engagement of Valencia as
                                    a place for learning.  Questions in the CCSSE target these areas of interest, to include
                                    the NSE  outcomes (or the 6Ps) for the NSE. For  example, “How much does this college
                                    emphasize-- Providing the support you need  to help you succeed at this college?"
                                    is one CCSSE question that will  provide feedback from NSE students about whether
                                    or not the NSE course is  meeting the outcome of Place - Students will demonstrate
                                    awareness of college  support systems.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/2017-CCSSE-Refresh-Items-Used.pdf" target="_blank">2017 CCSSE  Refresh Items Used</a></li>
                                    
                                    <li><a href="documents/2015-CCSSE-Items-Used.pdf" target="_blank">2015 CCSSE Items  Used</a></li>
                                    
                                 </ul>
                                 
                                 
                                 <h3>Student Feedback on  Instruction (SFI)</h3>
                                 
                                 <p>Student feedback is an integral part of the design of the  NSE program, to include
                                    the NSE course, as we consider that starting in Fall  2015, the NSE Course will be
                                    a required, degree-earning, General Education  course for all First Time in College
                                    Students (FTIC) and those students new to  Valencia who have fewer than 15-credit
                                    hours. The college adopted a set of  questions from the CCSSE into a Qualtrics Survey
                                    in spring 2014. The survey was  sent to all NSE students registered in the pilot (21
                                    sections of the NSE  Course) to provide them the opportunity to share feedback on
                                    their experience  in NSE. Their feedback was used to develop a set of recommendations
                                    about the  curricular and co-curricular aspects of the NSE as they related to the
                                    students' overall perceptions of the learning environment, their needs as  students,
                                    and their college readiness/success skills. Additionally, questions  from the CCSSE
                                    have been embedded into the Student Feedback on Instruction (SFI)  for NSE Students
                                    since fall 2014.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/Course-Evaluation-Question-Items-for-SFI-college-wide-since-spring-2015.pdf" target="_blank">Course Evaluation  Question - Items for SFI college-wide since spring 2015</a></li>
                                    
                                 </ul>
                                 
                                 <h3>Archives</h3>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/NSEDataCollected_Spring2015.pdf" target="_blank">NSE Data Collected -Spring 2015</a></li>
                                    
                                    <li>
                                       <a href="documents/NSEDataCollected.pdf" target="_blank">NSE Data Collected - Fall 2014</a>                    
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Assessment Methods</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The NSE Assessment Team met in November 2016 and confirmed  that learning outcomes
                                    assessment had evolved to a point where the rubrics (in  conjunction with the SFI)
                                    were sufficient in assessing for the General  Education outcomes and the 6Ps. To ensure
                                    the integrity of the data with the  common rubrics as a primary source for assessment,
                                    LOBP 3334 Using Rubrics to  Create Dialog was adapted to a specialized NSE version
                                    focused on uniform and  consistent use of the rubrics. The workshop was offered four
                                    times during  spring 2017 with over half the full-time and part-time NSE faculty attending
                                    for professional development credit. 
                                 </p>
                                 
                                 
                                 <h3>Archives</h3>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/NSE-Assessment-Overview-Summer-2015.pptx" target="_blank">NSE Assessment Overview - Summer 2015</a></li>
                                    
                                    <li><a href="documents/NSEStudentLearningArtifactSampleandOversample--Fall2015.pdf" target="_blank">NSE Sampling Plan - Fall 2015</a></li>
                                    
                                    <li><a href="documents/NSEStudentLearningArtifactSample--Spring2015.pdf" target="_blank">NSE Sampling Plan - Spring 2015</a></li>
                                    
                                    <li><a href="documents/NSEStudentLearningArtifactSample--Fall2014.pdf" target="_blank">NSE Sampling Plan - Fall 2014 </a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Results</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="documents/NSE-results-LOL-Meeting-January-20-2017.pptx" target="_blank">NSE results - LOL Meeting January 20, 2017</a></p>   
                                 
                                 
                                 <h3>Archives</h3>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/NSE1415AnnualInternalReport73015.pdf" target="_blank">NSE report 2014-15</a></li>
                                    
                                    <li><a href="documents/NSE_Fall_2014_Report_REVISED_3_2_15.pdf" target="_blank">NSE report 2014</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/qep.pcf">©</a>
      </div>
   </body>
</html>