<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/outcomes_gened.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>General Education</h2>
                        
                        <p>Valencia college faculty members from across seven disciplines assess the eight learning
                           outcomes that are at the heart of the General Education program at Valencia College.
                           The intersection is shown on our <a href="#MapGenEd"> General Education outcomes map</a> and <a href="#GenEdStudentLO"> the outcomes have also been defined</a> by faculty members. More information is available on the <a href="../../../../college-curriculum-committee/GeneralEducation.html" target="_blank">General Education website</a>.
                        </p>
                        
                        <ul>
                           
                           <li><a href="documents/Gen-Ed-Checklist-CT-W-IL-CHU-Sept-2017-Info-Lit-Criteria.pdf" target="_blank">This is the updated version of the General Education Checklist for 2017-2018 with
                                 the revised indicators for Information Literacy</a></li>
                           
                        </ul>
                        
                        
                        <p> Since 2013 faculty members have collaborated on the development of a shared Checklist
                           for assessing student artifacts. The indicators were defined and the tool tested in
                           2014 then faculty piloted and adopted a version of the checklist which allowed for
                           more nuanced discussion  2016. Sampling <a href="documents/OverviewoftheHistoryandNextStepsforGenEdArtifactSamplingRevisedLLJandLB8-16-2016.pdf" target="_blank">methods</a> have also evolved over time and there are now<a href="documents/RequestingyourSampleofStudentsforGenEdAssessmentLOLs-Updated7-5-2016LB.pdf" target="_blank"> steps</a> that the Learning Outcomes Leaders take working with the Institutional Research office
                           in order to request the data they need. 
                        </p>
                        
                        
                        <p>In 2016 faculty members began a conversation that is still unfolding related to their
                           assessment of writing in<a href="http://catalog.valenciacollege.edu/degrees/associateinarts/gordonrule/" target="_blank"> Gordon Rule</a> courses.&nbsp; Prior<a href="GeneralEducationPlanningDocuments.html"> information</a>  related to learning outcomes assessment is still available. Please click on the
                           links below for documents from Learning Outcomes Leaders related to their work-in-progress
                           assessing General Education.
                        </p>            
                        
                        <ul>
                           
                           <li><a href="documents/2016-17-General-Education-Program-Assessment-Summary.pdf" target="_blank">2016-17 General Education Program Assessment Summary (2pg.)</a></li>
                           
                           <li><a href="documents/2016-17-General-Education-Program-Assessment-Results-and-Recommendations.pdf" target="_blank">2016-17 General Education Program Assessment (full report)</a></li>
                           
                        </ul>
                        
                        <p>(Click on these links to visit the other sections below.)</p>            
                        
                        <h3><strong>The General Education Disciplines </strong></h3>
                        
                        <ul>
                           
                           <li><strong><a href="#GenEdDisciplines">New Student Experience (NSE)</a></strong></li>
                           
                           <li><strong><a href="#Englishlink">English (Comp I &amp; II)</a></strong></li>
                           
                           <li><strong><a href="#HumanitiesLink">Humanities</a></strong></li>
                           
                           <li><strong><a href="#Mathematicslink">Mathematics</a></strong></li>
                           
                           <li><strong><a href="#Sciencelink">Science</a></strong></li>
                           
                           <li><strong><a href="#SocialScienceslink">Social Sciences</a></strong></li>
                           
                           <li><strong><a href="#Speechlink">Speech</a></strong></li>
                           
                        </ul>            
                        
                        
                        
                        
                        <h2>
                           <a name="MapGenEd" id="MapGenEd"></a>Our Map of General Education Based on Approved Assessment Plans
                        </h2>
                        
                        <p>(Please Click Image to Enlarge) </p>
                        
                        <h2><a href="documents/Assessment-of-Student-Learning-Outcomes-in-Gen-Ed-MAP-NJ-8-21-17.pdf" target="_blank"><img alt="The Assessment of Student Learning Outcomes in General Education 2016-2017" border="0" height="456" src="Assessment-of-Student-Learning-Outcomes-in-Gen-Ed-MAP-2017-18.JPG" width="770"></a></h2>
                        <br>
                        
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        <h2>
                           <strong><a name="GenEdStudentLO" id="GenEdStudentLO"></a>Valencia General Education Student Learning Outcomes</strong> 
                        </h2>
                        
                        <p>The general education program at Valencia is an integral part of the A.A. Degree program
                           and is designed to contribute to the student's educational growth by providing a basic
                           liberal arts education. A student who completes the general education program should
                           have achieved the following outcomes:
                        </p>
                        
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>CULTURAL &amp; HISTORICAL UNDERSTANDING: </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <strong> </strong>
                                       
                                       <p>Demonstrate understanding of the diverse traditions of the world, and an individual's
                                          place in it.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong><strong>QUANTITATIVE AND SCIENTIFIC REASONING:</strong> </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <p>Use processes, procedures, data, or evidence to solve problems and make effective
                                          decisions.
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong><strong>COMMUNICATION SKILLS:</strong> </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <p>Engage in effective interpersonal, oral, and written communication.</p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong><strong>ETHICAL RESPONSIBILITY:</strong> </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <p>Demonstrate awareness of personal responsibility in one's civic, social, and academic
                                          life.
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong><strong>INFORMATION LITERACY:</strong></strong></div>
                                    
                                    <div data-old-tag="td">
                                       <p>Locate, evaluate, and effectively use information from diverse sources.</p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong><strong>CRITICAL THINKING:</strong> </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <p>Effectively analyze, evaluate, synthesize, and apply information and ideas from diverse
                                          sources and disciplines.
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h2>
                           <a name="GenEdStudentLOCategories" id="GenEdStudentLOCategories"></a>Florida Community College General Education Student Learning Outcome Categories 
                        </h2>
                        
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>COMMUNICATION:</div>
                                    </div>
                                    
                                    <div data-old-tag="td"> Read, write, speak and listen effectively. </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>CRITICAL THINKING: </div>
                                    </div>
                                    
                                    <div data-old-tag="td"> Reflect, analyze, synthesize and apply. </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>SCIENTIFIC AND QUANTITATIVE REASONING: </div>
                                    </div>
                                    
                                    <div data-old-tag="td"> Understand and apply mathematical and scientific principles and methods. </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>INFORMATION LITERACY: </div>
                                    </div>
                                    
                                    <div data-old-tag="td"> Find, evaluate, organize and use information. </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>GLOBAL SOCIO-CULTURAL RESPONSIBILITY: </div>
                                    </div>
                                    
                                    <div data-old-tag="td"> Actively participate as an informed and responsible citizen in social, cultural,
                                       global and environmental matters.
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3>
                           <a name="GenEdDisciplines" id="GenEdDisciplines"></a>Valencia General Education Disciplines
                        </h3>
                        
                        <div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong><a name="NSEAnchor" id="NSEAnchor"></a> NEW STUDENT EXPERIENCE</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/2016NSEAssessmentDayPP.pptx"><strong>NSE Assessment Day 2016 Presentation</strong></a></li>
                                             
                                             <li>
                                                <a href="documents/AssessmentDay-CompetentCommunicatorCharacteristics.pdf"><strong>NSE Assessment 2016- Competent Comminicator Characteristics</strong></a> 
                                             </li>
                                             
                                             <li><a href="documents/NSEAssessmentDay20155715-Final.pptx" target="_blank"><strong>NSE Assessment Day 2015 Presentation</strong></a></li>
                                             
                                          </ul>
                                          
                                          <ul>
                                             
                                             <li><a href="documents/NSEAssessmentSession1CriticalThinking.pptx" target="_blank"><strong>NSE Assessment Critical Thinking</strong></a></li>
                                             
                                             <li>
                                                <a href="documents/NSEAssessmentSession1CriticalThinking.pdf" target="_blank"><strong>NSE Assessment Critical Thinking</strong></a> 
                                             </li>
                                             
                                             <li><a href="documents/NSEAssessmentSession2CommunicationsSkills.pptx" target="_blank"><strong>NSE Assessment Communications Skills</strong></a></li>
                                             
                                             <li><strong><a href="documents/NSEAssessmentSession2CommunicationsSkills.pdf" target="_blank">NSE Assessment Communications Skills</a></strong></li>
                                             
                                          </ul>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong><a name="Englishlink" id="Englishlink"></a>ENGLISH COMP I &amp; II</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <ul>
                                          
                                          <li><strong><a href="documents/Assessment-Plan-for-2016-Academic-Year-Comp-I-and-II.pdf" target="_blank">English Comp I &amp; II Assessment Plan for 2016 Academic Year</a> </strong></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong><a name="HumanitiesLink" id="HumanitiesLink"></a>HUMANITIES</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="documents/HumanitiesAssessmentDay2016.pdf" target="_blank"><strong>Humanities Assessment Day 2016</strong></a> 
                                          </li>
                                          
                                       </ul>                    
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          <a name="Mathematicslink" id="Mathematicslink"></a><strong>MATHEMATICS</strong>
                                          
                                       </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <ul>
                                          
                                          <li><a href="documents/Spring2016MathAssessmentResults6-8-16.pdf" target="_blank"><strong>Math Assessment Day 2016 Presentation</strong></a></li>
                                          
                                          <li><strong><a href="documents/Math-Algebra-Assessment-Day-2017-Presentation.pdf" target="_blank">Math Algebra Assessment Day 2017 Presentation</a></strong></li>
                                          
                                          <li><strong><a href="documents/Math-STA-2023-Assessment-Day-2017-Presentation.pdf" target="_blank">Math STA-2023 Assessment Day 2017 Presentation</a></strong></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong><a name="Sciencelink" id="Sciencelink"></a>SCIENCE</strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <ul>
                                          
                                          <li><a href="documents/GenEdAssessmentDay-2016SlidesScience.pdf" target="_blank"><strong>Science Assessment Day 2016 Presentation </strong></a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong><a name="SocialScienceslink" id="SocialScienceslink"></a>SOCIAL SCIENCES </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="documents/AssessmentDay16.pdf" target="_blank"><strong>Social Sciences Assessment Day 2016 Presentation Materials</strong></a> 
                                          </li>
                                          
                                          <li><a href="documents/AssessmentDay2015.pptx" target="_blank"><strong>Social Sciences Assessment Day 2015 Presentation Materials</strong></a></li>
                                          
                                          <li><a href="documents/AssessmentDayAgendaMay82015.docx" target="_blank"><strong>Social Sciences Assessment Day 2015 Agenda </strong></a></li>
                                          
                                          <li><a href="documents/GenEdSocialScience-AlternatingOutcomes4-2-15.pdf" target="_blank"><strong>Email clarifying that the Critical Thinking and the Ethical Reasoning Outcomes alternate
                                                   each year for Social Science  4-2-15</strong></a></li>
                                          
                                          <li><strong><a href="documents/Social-Science-Assessment-Day-2017-Presentation.pdf" target="_blank">Social Science Assessment Day 2017 Presentation</a></strong></li>
                                          
                                       </ul>                    
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          <a name="Speechlink" id="Speechlink"></a><strong>SPEECH</strong>
                                          
                                       </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <ul>
                                          
                                          <li>                      <a href="documents/AssessmentDayPresentation.pptx"><strong>Speech Assessment Day 2016 Presentation</strong></a>                    
                                          </li>
                                          
                                          <li>                      <a href="documents/AssessmentDayAgenda2016.docx"><strong>Speech Assessment Day 2016 Agenda</strong></a>                    
                                          </li>
                                          
                                          <li>                      <a href="documents/AssessmentDayPresentation-Speech_Interpersonaltan5-12-2016.pdf"><strong>Speech Assessment Day Presentation - Speech - Interpersonal Tan 5-12-2016</strong></a>                    
                                          </li>
                                          
                                          <li>                      <strong><a href="documents/SpeechCommunicationAssessmentDay_LastUpdated_May72015.pptx" target="_blank">Speech Assessment Day 2015 Presentation</a></strong>                    
                                          </li>
                                          
                                          <li>                      <strong><a href="documents/AssessmentDayAgenda_May8_West_11_217_9am.docx" target="_blank">Speech Assessment Day 2015 Agenda </a> </strong>                    
                                          </li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3><strong><a name="FloridaResourcesLink" id="FloridaResourcesLink"></a>Outcomes Resources Florida - State College System </strong></h3>            
                        
                        <ul>
                           
                           <li>
                              <a href="#GenEdStudentLOCategories"><strong>Florida Community College General Education Student Learning Outcome Categories </strong></a> 
                           </li>
                           
                           <li><a href="http://www.fldoe.org/articulation/pdf/GE-SteeringFacultyCommitteeFinalRecommendations.pdf"><strong>General Education Core Course Options</strong></a></li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/outcomes_gened.pcf">©</a>
      </div>
   </body>
</html>