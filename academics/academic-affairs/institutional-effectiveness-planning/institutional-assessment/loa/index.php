<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Loa</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				<a id="content" name="content"></a>
                     				
                     <h2>Learning Outcomes Assessment</h2>
                     				
                     <blockquote>
                        					
                        <p><em>"Knowing what happens at the program or department level in student learning outcomes
                              assessment from the faculty and staff working in these programs and departments is
                              essential..." Kuh, 2011</em></p>
                        				
                     </blockquote>
                     
                     				
                     <p>Valencia College is committed to learning outcomes assessment for the purpose of understanding
                        and strengthening teaching and learning.&nbsp;The Office of Institutional Assessment helps
                        to facilitate the conversation and to support the development and implementation of
                        the strategies needed to reach those goals. Look to this section of the Website for
                        related forms and reports.&nbsp; The tabs at the left also provide access to the program
                        learning outcomes for General Education as well as for the Associate in Arts (AA)
                        transfer programs, Associate in Science (AS) degree, and related programs.
                     </p>
                     
                     
                     				<a href="http://valenciacollege.xitracs.net/portal.htm" target="_blank">
                        <div class="button-outline">Xitracs Portal</div></a>
                     
                     				
                     <table class="table table ">
                        
                        					
                        <tr>
                           						
                           <th scope="col" rowspan="2">General Education Learning Outcomes</th>
                           						
                           <th scope="col" colspan="3" align="center">Communications</th>
                           						
                           <th scope="col" rowspan="2">Humanities Assignment with Checklist</th>
                           						
                           <th scope="col" rowspan="2">Mathematics Exam</th>
                           						
                           <th scope="col" rowspan="2">Science Exam</th>
                           						
                           <th scope="col" rowspan="2">Social Science Exam</th>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="col">NSE Assignments with Rubric</th>
                           						
                           <th scope="col">English Assignment with Checklist Randomized Sample</th>
                           						
                           <th scope="col">Speec Assignment with Checklist + Self-Assessment</th>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="col" align="right"><em>Sample Group</em></th>
                           						
                           <th scope="col"><em>All Students</em></th>
                           						
                           <th scope="col"><em>Comp I &amp; II</em></th>
                           						
                           <th scope="col"><em>All Students</em></th>
                           						
                           <th scope="col"><em>Randomized Sample</em></th>
                           						
                           <th scope="col"><em>Randomized Sample</em></th>
                           						
                           <th scope="col"><em>All Students</em></th>
                           						
                           <th scope="col"><em>All Students</em></th>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Quantitative Reasoning</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X All Gen Ed. Math Classes</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Scientific Reasoning</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X All Gen Ed. Science Classes</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Written Communication</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Oral Communication</th>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Interpersonal Communication</th>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Ethical Responsibility</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Cultural &amp; Historical    Understanding</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X All Gen Ed. Humanities Classes</td>
                           						
                           <td></td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           					
                        </tr>
                        					
                        <tr>
                           						
                           <th scope="row">Information Literacy</th>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td>&nbsp;</td>
                           						
                           <td align="center" valign="middle">X</td>
                           					
                        </tr>
                        					
                        <tr>
                           <td colspan="8" bgcolor="#FDB913" align="center"><strong>Critical Thinking</strong></td>
                        </tr>
                        				
                     </table>
                     				
                     <div class="row">
                        					
                        <div class="col-md-6">
                           						
                           <h3>Important Information</h3>
                           						
                           <ul>
                              							
                              <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf">LOA Glossary</a></li>
                              							
                              <li><a href="documents/LOLListLaura_MakeChanges4.pdf" target="_blank" class="icon_for_pdf">List of Learning Outcomes Assessment Leaders- Current as of 04/20/2017</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/WorkPlanLOLssenttodeans9-2014--edited1-2015v3KBLB.pdf" target="_blank">Learning Outcome Leaders Work Plan </a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/EssentialCompetenciesofaValenciaEducator-onepage1-20-2015.pdf" target="_blank">Essential Competencies of a Valencia Educator pg.1 rev. 01/20/2015</a></li>
                           </ul>
                           					
                        </div>
                        
                        					
                        <div class="col-md-6">
                           						
                           <h3>Archives</h3>
                           						
                           <ul>
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/AssessmentDay2016-ReportingandPlanningTemplatefor2016-2017v3.docx">Assessment Day 2016-Reporting and Planning Template for 2016-2017</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/ProposedAdjustmentofASOutcomesOnlyv7--1-18-2017.pdf" target="_blank">Proposed Adjustment of AS Outcomes Only v.7 1-18-2017</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/AssessmentPlanFlowChart.pdf" target="_blank">Approval and Improvement Process Flow Chart</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/FINALagendaFeb6-LOLmeeting.pdf" target="_blank">Agenda for Learning Outcomes Leaders Meeting 02/06/2015</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/LOLFeb-6-2015PPT.pptx" target="_blank">Learning Outcomes Leaders Meeting 02/06/2015 Presentation</a></li>
                              							
                              <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/documents/ProgramLearningOutcomesAssessmentPlanningIAChandoutBlasi7-16-2-13v2.pdf" target="_blank">Next Steps for Deans 2013-14</a><br> Discussed with deans in the Instructional Affairs Committee (IAC) in July 2013 and
                                 shared with campus presidents.
                              </li>
                           </ul>
                           					
                        </div>
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/index.pcf">©</a>
      </div>
   </body>
</html>