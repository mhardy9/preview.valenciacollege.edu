<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Examples.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h3><em>Examples</em></h3>
                        
                        <div>
                           
                           <h3>Rubric Library</h3>
                           
                           <div>
                              
                              <h2>Examples of Rubrics Being Used in Program Learning Outcomes Assessment</h2>
                              
                              <p>This Rubrics Library offers the tools being developed by faculty and staff as part
                                 of their Program Learning Outcomes Plans and process. We received these from program
                                 assessment plan leaders after Assessment Day in May 2012.
                              </p>
                              
                              <p>All materials here have been reviewed by the leaders. We expect to have approximately
                                 32 samples of rubrics, checklists, and other examples. This library will grow throughout
                                 the 2013 fall semester.
                              </p>
                              
                              <p>Please contact us directly with any questions.</p>
                              
                              <p>Examples of Nationally-Developed Rubrics are available through <a href="http://www.aacu.org/value/rubrics/index_p.cfm" target="_blank">The Association of American Colleges and Universities</a></p>
                              
                              <h2>General Education- 2005: Draft Rubrics <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/TVCADrafts.pdf" target="_blank">Critical Thinking </a><br> General Education- 2007: Final Rubrics <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/TVCAFinals.pdf" target="_blank">Communication and Critical Thinking</a></h2>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">&nbsp;</div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td"><strong>Example rubrics by Academic Program/Discipline and Contact</strong></div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Accounting Technology, Contact: <a href="mailto:cbattiste@valenciacollege.edu">Cecil Battiste</a></p>
                                          
                                          <p>The faculty members working within the Accounting Technology program area have developed
                                             a plan focused on students' evaluation of financial information using a budget assignment
                                             with an analytic rubric. Their next steps will be to revise the assessment and also
                                             begin using the "Monopoly Project" to evaluate their first learning outcome. One interesting
                                             finding from their work - the quantitative skills of students assessed are adequate,
                                             they would like to strengthen the students' qualitative skills - improving the way
                                             they write reports and memos based on their analyses. They would like their graduates
                                             to be able to serve as business advisors with strong communication skills. Please
                                             contact Cecil Battiste for more information (or to be directed to the most current
                                             group leader if there has been a change.)
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/AccountingRubric.pdf" target="_blank">Accounting Rubric </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Baking and Pastry Management,&nbsp;Contact: <a href="mailto:kbourgoin@valenciacollege.edu">Ken Bourgoin</a></p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Baking and Pastry Management program area have
                                          developed a plan focused on the overall performance of students on specific assignments,
                                          exams, and activities as documented through a portfolio process. They will continue
                                          to use this approach and work to check on students' understanding of the criteria
                                          earlier in the term. A major finding arose from their work as they learned that 75%
                                          of the work was done accurately with 25% sent back for correction (and 10% from that
                                          group receiving a penalty towards the final grade. Please contact Pierre Pilloud for
                                          more information (or to be directed to the most current group leader if there has
                                          been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CulinaryRubric.pdf" target="_blank">Culinary Rubric </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Criminal Justice Technology,&nbsp;Contact: <a href="mailto:jmurphy60@valenciacollege.edu">Brian Murphy</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Criminal Justice Technology program area have
                                          developed a plan focused on a student essay as part of a case study involving a burglary
                                          scenario. This plan will be implemented in Fall 2012 (in CJE 2600) using a grading
                                          checklist. Please contact James McDonald for more information (or to be directed to
                                          the most current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CriminalJusticeAssesment.pdf" target="_blank">Criminal Justice Assessment </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Dance Performance (AA Pre-Major),&nbsp;Contact: <a href="mailto:ssalapa@valenciacollege.edu">Suzanne Salapa</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Dance Performance (AA Pre-Major) program area
                                          have developed a plan focused on the evaluation of student performance in dance using
                                          a structured rubric and resulting in a video of the performance. As a result of this
                                          year's program assessment the learning outcomes were reduced from four to three -
                                          as the fourth (the fitness principle) was difficult to measure. An additional class
                                          is being added (Modern Dance III) to strengthen the support of student performance.
                                          Their assessment tool has been streamlined with feedback from each person using it.
                                          Please contact Suzanne Salapa for more information (or to be directed to the most
                                          current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DanceRubric.pdf" target="_blank">Dance Rubric </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Digital Media ,&nbsp;Contact: <a href="mailto:rmccaffrey@valenciacollege.edu">Rob McCaffrey</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Digital Media for Video and Motion Graphics
                                          program assess student video projects, in which students are asked to apply the skills
                                          taught throughout the program. Students were challenged in terms of their audio collection
                                          skills and their ability to tell stories. The Digital Story faculty will work more
                                          closely with the Sound Media instructor to better prepare the students. The instructors,
                                          as subject matter experts, are going to create the upcoming year's assessment. They
                                          are looking into adding courses in audio and lighting - while also adding motion graphics
                                          projects. Please contact Matt Messenger for more information (or to be directed to
                                          the most current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ProgramAssessmentSheet-DigitalMedia.pdf" target="_blank">Digital Media Assessment</a></p>
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ProgramLevelAssessment-_Designcompellingmotiongraphics.pdf" target="_blank">Motion Graphics Assessment</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Graphics Technology, Contact: <a href="mailto:akern@valenciacollege.edu">Amanda Kern</a></div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Graphics Technology program area have developed
                                          a plan that draws on their student portfolios assessed using a rubric by experts in
                                          the field. The artifacts collected will be used for a range of learning outcomes -
                                          the work spans process samples, resumes, promotional materials and other pieces. As
                                          a result of the most recent assessment workshops will be offered to help students
                                          learn how to digitize their work earlier in the program. Please contact Kristy Pennino
                                          for more information (or to be directed to the most current group leader if there
                                          has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/TVCAEvaluation_InteractiveForm_GRA.pdf" target="_blank">Evaluation</a></p>
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/TVCAStudentSelf_Evaluation_GRA.pdf" target="_top">Self- Evaluation</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">History, Contact: <a href="mailto:ccreasman@valenciacollege.edu">Carl Creasman</a></div>
                                       
                                       <div data-old-tag="td">The faculty members working within the History program area have developed a plan
                                          focused on an assessment of the student understanding of historical time period and
                                          the role of diverse subgroups. They provide criteria for faculty to assess, they do
                                          not require a specific assignment; 80% of students we rated as "good" and found to
                                          satisfactorily meet the expectation for this outcome. Please contact Carl Creasman
                                          for more information (or to be directed to the most current group leader if there
                                          has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/HistoryCheckList.pdf" target="_blank">History Checklist </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Landscape and Horticulture Technology,&nbsp;Contact: <a href="mailto:jgarces3@valenciacollege.edu">Javier Garces </a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Landscape and Horticulture Technology program
                                          area have developed a plan using student writing demonstrating their writing skills
                                          paired with related research into career opportunities in their profession. Please
                                          contact Javier Garces for more information (or to be directed to the most current
                                          group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/HorticultureRubric.pdf" target="_blank">Horticulture Rubric </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Nursing, RN,&nbsp;Contact: <a href="mailto:akovalsky@valenciacollege.edu">Anita Kovalsky </a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Nursing, RN program area have developed a plan
                                          using the results of a simulation administering an intervention for a designated group
                                          of patients. Faculty members plan to review the standardized achievement exam results
                                          and National Board reports in reinforce the skills associated with safe clinical decision-making.
                                          Please contact Anita Kovalsky for more information (or to be directed to the most
                                          current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/DataforRubric-Nursing-Majorfindingsfromlastyear2011-2012andrelatedchange.pdf" target="_blank">Nursing Rubric </a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Office Administration,&nbsp;Contact: <a href="mailto:mhoward@valenciacollege.edu">Marie Howard</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Office Administration program area have developed
                                          a plan focused on the results of student performance (production tests) on real work
                                          activities typically used in office settings (for example creating business letters.)
                                          Please contact Betty Wanielista for more information (or to be directed to the most
                                          current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ChecklistAcademicReportupdated.pdf" target="_blank">Report Checklist</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Checklistletterupdated.pdf" target="_blank">Letter Checklist</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Checklistmemoupdated.pdf" target="_blank">Memo Checklist</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/Checklisttableupdated.pdf" target="_blank">Table Checklist </a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Paralegal Studies, Contact: <a href="mailto:wtoscano@valenciacollege.edu">Wendy Toscano</a></div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>The faculty members working within the Paralegal Studies program have developed a
                                             plan which assesses student performance through a review of students’ portfolios.&nbsp;
                                             Paralegal Studies students in Legal Research &amp; Theory III produce a portfolio of their
                                             practical skill application assignments completed throughout the program.&nbsp; Attending
                                             the most recent assessment review were four industry members and three paralegal faculty,
                                             along with a College facilitator.&nbsp; Each person reviewed two randomly selected portfolios.&nbsp;
                                             Each reviewer completed a checklist indicating whether they believed that program
                                             outcomes were met.&nbsp; Reviewers had an opportunity to discuss the portfolios among themselves
                                             and to make recommendations for the program.&nbsp; Please contact Wendy Toscano for more
                                             information.
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/AssessmentChecklist-ParalegalStudies.docx">Paralegal Studies Checklist</a></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">&nbsp;</div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>Student Life Skills,&nbsp;Contact: <a href="mailto:jlee50@valenciacollege.edu">Jenny Lee</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">The faculty members working within the Student Life Skills program area have developed
                                          a plan drawing on student artifacts (Learning Reflections) from the Study Skills section
                                          of the Course Portfolio. Students achieved approximately 40% of the learning objectives,
                                          and as part of the next steps they are pursuing faculty development geared at teaching
                                          strategies for the SLS 1122 Learning Portfolio and updated materials for new and returning
                                          faculty members. Please contact Chip Turner for more information (or to be directed
                                          to the most current group leader if there has been a change.)
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ACADEMICPLANNINGPortfolioRubric.pdf" target="_blank">Academic Planning Rubric</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/CAREEREXPLORATIONPortfoliRubric.pdf" target="_blank">Career Exploration Rubric</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/SELFDISCOVERYPortfolioRubric.pdf" target="_blank">Self Discovery Rubric</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/STUDYSKILLSPortfolioRubric.pdf" target="_blank">Study Skills Rubric</a><br> <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PortfolioGradingRubric.pdf" target="_blank">Portfolio Grading Rubric </a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Examples of Program Assessment and Improvement Plans</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h3><a href="#Valencia">Examples from Valencia</a></h3>
                                 
                                 <h3><a href="#Other">Examples from Other Colleges</a></h3>
                                 
                                 <h3><a href="#related">Related Reports</a></h3>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th"><a id="Valencia" name="Valencia"></a>Examples from Valencia College
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th"><strong>Academic Program/Discipline</strong></div>
                                          
                                          <div data-old-tag="th"><strong>Plan Summary</strong></div>
                                          
                                          <div data-old-tag="th"><strong>Assessment Plan </strong></div>
                                          
                                          <div data-old-tag="th"><strong>2012-2013 Improvement Plan </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dance Performance (AA Pre-Major)<br> Contact: <a href="mailto:ssalapa@valenciacollege.edu">Suzanne Salapa </a></div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Dance Performance (AA Pre-Major) program area
                                             have developed a plan focused on the evaluation of student performance in dance using
                                             a structured rubric and resulting in a video of the performance. As a result of this
                                             year's program assessment the learning outcomes were reduced from four to three -
                                             as the fourth (the fitness principle) was difficult to measure. An additional class
                                             is being added (Modern Dance III) to strengthen the support of student performance.
                                             Their assessment tool has been streamlined with feedback from each person using it.
                                             Please contact Suzanne Salapa for more information (or to be directed to the most
                                             current group leader if there has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/PLOA/documents/2011-12_ProgramLearningOutcomeAssessmentPlan_Dance.pdf.html" target="_blank">Dance Plan</a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPDanceSalapa.pdf" target="_blank">Dance Improvement Plan </a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dental Hygiene<br> Contact: <a href="mailto:psandy@valenciacollege.edu">Pam Sandy </a></div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Dental Hygiene program area have developed
                                             a plan focused on several program learning outcomes drawing on case documentation
                                             papers, daily clinical treatment decisions, and oral presentations. While the faculty
                                             developed a rubric they also used the Dental Hygiene Clinic manual as part of the
                                             assessment; data from 2010 and 2011 were compared as part of this process. Over time
                                             there was a slight decline in oral presentations and an upward trend in the written
                                             work of students from term to term. The results of the assessment have caused them
                                             to look into ways of better evaluating evidence-based decision-making using a performance
                                             assessment. Please contact Pam Sandy for more information (or to be directed to the
                                             most current group leader if there has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/2011-12_ProgramLearningOutcomeAssessmentPlan_DentalHygiene.pdf" target="_blank">Dental Hygiene Plan </a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPDentalHygeineSandy.pdf" target="_blank">Dental Hygiene Improvement Plan </a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Emergency Medical Services Technology<br> Contact: <a href="mailto:abrody@valenciacollege.edu">Andrea Brody</a></div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Emergency Medical Services Technology program
                                             area have developed a plan focused on student communication skills - with patients,
                                             family members, physicians, and healthcare team members. Physicians participated in
                                             the evaluation of students, which was conducted according to a national standard of
                                             critical care intervention. The faculty plan to track student progress in the program
                                             by expanding the evaluation from one term to two terms. Please contact Andrea Brody
                                             for more information (or to be directed to the most current group leader if there
                                             has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/2011-12_ProgramLearningOutcomeAssessmentPlan_EMS.pdf" target="_blank">EMS Plan </a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPEMSBrody.pdf" target="_blank">EMS Improvement Plan </a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Film Production Technology<br> Contact:<a href="mailto:rclemente@valenciacollege.edu"> Ralph Clemente</a></div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Film Production Technology program area have
                                             developed an assessment of students' performance as they work on a film crew applying
                                             the skills they have learned throughout the program. The student evaluation includes
                                             feedback from professionals they work alongside on the feature film. A stronger emphasis
                                             on student skills with equipment may be brought into the curriculum. The faculty use
                                             feedback from an Advisory board and industry professionals as they continue to develop
                                             the assessment. Please contact Ralph Clemente for more information (or to be directed
                                             to the most current group leader if there has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/PLOA/documents/2011-12_ProgramLearningOutcomeAssessmentPlan_FilmProductionTechnology.pdf.html" target="_blank">Film Production Plan</a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPFilmClemente.pdf" target="_blank">Film Production Improvement Plan</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Graphics Technology<br> Contact: <a href="mailto:kpennino@valenciacollege.edu">Kristy Pennino</a> or <a href="mailto:mcurtiss@valenciacollege.edu">Meg Curtiss </a></div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Graphics Technology program area have developed
                                             a plan that draws on their student portfolios assessed using a rubric by experts in
                                             the field. The artifacts collected will be used for a range of learning outcomes -
                                             the work spans process samples, resumes, promotional materials and other pieces. As
                                             a result of the most recent assessment workshops will be offered to help students
                                             learn how to digitize their work earlier in the program. Please contact Kristy Pennino
                                             or Meg Curtiss for more information (or to be directed to the most current group leader
                                             if there has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/PLOA/documents/2011-12_ProgramLearningOutcomeAssessmentPlan_Graphics.pdf.html" target="_blank">Graphics Tech Plan</a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPGraphicsTechnologyCurtiss.pdf" target="_blank">Graphics Tech Improvement Plan</a></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Nursing, RN<br> Contact: <a href="mailto:akovalsky@valenciacollege.edu">Anita Kovalsky</a></p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">The faculty members working within the Nursing, RN program area have developed a plan
                                             using the results of a simulation administering an intervention for a designated group
                                             of patients. Faculty members plan to review the standardized achievement exam results
                                             and National Board reports to reinforce the skills associated with safe clinical decision-making.
                                             Please contact Anita Kovalsky for more information (or to be directed to the most
                                             current group leader if there has been a change.)
                                          </div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/2011-12_ProgramLearningOutcomesAssessmentPlan_Nursing.pdf" target="_blank">Nursing Plan </a></div>
                                          
                                          <div data-old-tag="td"><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/PLOAPNursingGomez.pdf" target="_blank">Nursing Improvement Plan </a></div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3><a id="Other" name="Other"></a>Examples from Other Colleges
                                 </h3>
                                 
                                 <p>College Department and Program - Nationally Recognized Resources<br> <a href="http://www.learningoutcomeassessment.org/CollegesUniversityPrograms.html" target="_blank">http://www.learningoutcomeassessment.org/CollegesUniversityPrograms.html</a></p>
                                 
                                 <p>Examples of Good Assessment Practice: Institution List<br> <a href="http://learningoutcomeassessment.org/CaseStudiesInstitutions.html" target="_blank">http://learningoutcomeassessment.org/CaseStudiesInstitutions.html</a></p>
                                 
                                 <p>Down and In: Assessment Practices at the Program Level (June 2011) Researchers from
                                    the National Institute for Learning Outcomes Assessment (NILOA) surveyed program heads
                                    in the two and four-year sectors to gain a more complete picture of assessment activity
                                    at the program or department level.<br> <a href="http://www.learningoutcomesassessment.org/documents/NILOAsurveyreport2011.pdf" target="_blank">http://www.learningoutcomesassessment.org/documents/NILOAsurveyreport2011.pdf</a></p>
                                 
                                 <h3><strong><a id="related" name="related"></a>Related Reports </strong></h3>
                                 
                                 <p>Engaging Departments in Assessing Student Learning: Overcoming Common Obstacles<br> By Jo Michelle Beld, St. Olaf College <br> PeerReview - Winter 2010, Vol. 12, No. 1 <br> <a href="http://www.aacu.org/peerreview/pr-wi10/pr-wi10_Beld.cfm" target="_blank">http://www.aacu.org/peerreview/pr-wi10/pr-wi10_Beld.cfm</a></p>
                                 
                                 <p>Levels of Assessment<br> By Ross Miller and Andrea Leskes, AAC&amp;U<br> <a href="http://www.aacu.org/pdf/LevelsOfAssessment.pdf" target="_blank">http://www.aacu.org/pdf/LevelsOfAssessment.pdf</a></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Examples.pcf">©</a>
      </div>
   </body>
</html>