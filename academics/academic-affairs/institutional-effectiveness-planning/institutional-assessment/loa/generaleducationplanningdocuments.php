<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/generaleducationplanningdocuments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/">Loa</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Prior Draft Recommendations and Other Planning Documents</h2>
                        
                        <p>General Education Analysis <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Copyof273GENEDANALYSIS.xlsx" target="_blank">(2013)</a></p>
                        
                        <p>General Education Faculty Committee Draft Recommendations <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/GeneralEducationFacultyCommitteeInitialDraftRecommendations.docx" target="_blank">(2012)</a></p>
                        
                        <p>Themes from General Education Discussions <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/ThemesfromtheGenEdDiscussions.doc" target="_blank">(2012) </a></p>
                        
                        <p>General Education Map <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/GenEdMap.pptx" target="_blank">(2010)</a></p>
                        
                        <p>General Education LC Presentation <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GenEd-LC10-21-08.ppt" target="_blank">(2008)</a> 
                        </p>
                        
                        <p>Principles and Procedures <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/PrinciplesandProcedures.docx" target="_blank">(2007)</a></p>
                        
                        <p>General Education Faculty Vote<a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GenEd-FacultyVote.pdf" target="_blank"> (2007)</a></p>
                        
                        <p>General Education- Timeline <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/documents/GeneralEducationTimeline.ppt" target="_blank">(2007)</a> 
                        </p>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        
                        
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/generaleducationplanningdocuments.pcf">©</a>
      </div>
   </body>
</html>