<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Effectiveness &amp; Planning | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/meettheteam.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Effectiveness &amp; Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Institutional Effectiveness &amp; Planning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Meet the Team</h2>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong> Laura Blasi, Ph.D., Director, Institutional Assessment </strong></p>
                                    
                                    <p><a href="mailto:lblasi@valenciacollege.edu">lblasi@valenciacollege.edu</a></p>
                                    
                                    <p>Dr. Blasi has twenty years of experience in higher education, including five years
                                       as a full-time faculty member teaching assessment and research methods to graduate
                                       students at the University of Central Florida (UCF). She began her work in higher
                                       education developing and managing programs within the American Association of Colleges
                                       and Universities (AAC&amp;U) in Washington, DC, focused on issues that impact equity in
                                       education. With a master’s degree in English from Georgetown University, she later
                                       earned her doctoral degree in Educational Leadership and Policy Studies from the University
                                       of Virginia. Her experience as a program evaluator includes work with the U.S. Department
                                       of Education, NASA, and other federal agencies. Through her work in assessment and
                                       institutional research, she has supported the ten-year SACS review processes at two
                                       different institutions. Laura assists with analysis and reporting from national survey
                                       data;  the design and implementation of surveys focused on learning; and she works
                                       to guide and facilitate the annual cycle of program learning outcomes assessment college-wide
                                       at Valencia. Her own research has focused on the use of assessment and research methods
                                       to improve teaching and learning.<br>
                                       <br>See her related National Institute for Learning Outcomes Assessment (NILOA) essay:
                                       How Assessment and Institutional Research Staff Can Help Faculty with Student Learning
                                       Outcomes Assessment.
                                    </p>
                                    
                                    <p><a href="https://illinois.edu/blog/view/915/64343?displayOrder=desc&amp;displayType=none&amp;displayColumn=created&amp;displayCount=1">https://illinois.edu/blog/view/915/64343?displayOrder=desc&amp;displayType=none&amp;displayColumn=created&amp;displayCount=1</a> 
                                    </p>                  
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Nichole Jackson, Assistant Director, Learning Assessment </strong></p>
                                    
                                    <p><a href="mailto:njackson18@valenciacollege.edu">njackson18@valenciacollege.edu</a></p>
                                    
                                    <p>Nichole has worked in higher education over the past 8 years with a focus on outcomes-based
                                       practices and assessment methods that inform curriculum, especially rubric design
                                       to support curricular alignment and scaffolding learning activities that culminate
                                       in summative assessments. Her pedagogical experience includes years in the classroom
                                       as a full-time Humanities Instructor at Valencia College. Prior to that she also taught
                                       Quantitative Reasoning at Rollins College and worked with their TRIO grant programs
                                       teaching rising first-generation college students through Upward Bound. Nichole earned
                                       a Bachelor's in Mathematics and a Master's in Liberal Studies, both from Rollins College.
                                       She worked for several years as a faculty member supporting the Developmental Education
                                       Initiative at Valencia College focusing on college success skills. Nichole participated
                                       in the development of Valencia’s QEP in the Big Meetings, reading discussion groups,
                                       and the co-curricular task force. Her work as co-chair of the Internationalizing the
                                       Curriculum Committee was integral to Valencia’s recent award of a two-year USIFL grant
                                       for Internationalizing the Curriculum. Nichole’s evaluations incorporate mixed methods
                                       of quantitative and qualitative analysis to support continuous process improvement.
                                       Her own research interests include classical and postmodern philosophies of education
                                       and justice, as well as cross-cultural learning.
                                    </p>                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Kim Adams-Long, Staff Assistant, Institutional Assessment</strong><br>
                                       <a href="mailto:kimadams@valenciacollege.edu">kimadams@valenciacollege.edu</a>                  
                                    </p>
                                    
                                    <p>Kim began work as a Staff Assistant at Valencia College  in 1997, working in Planning
                                       and Educational Services for six years. After  that, she moved into the position of
                                       Executive Assistant for the Chief Learning  Officer, finishing up her 15.5 year tenure
                                       at Valencia as the Administrative  Assistant to the Assistant Vice President for Curriculum
                                       and Articulation.  During her time at Valencia, Kim gained experience and expertise
                                       working  through the 2003 SACS reaccreditation process; handling multiple budgets
                                       for  the CLO; setting up meetings, appointments, travel; minutes; website updates,
                                       as well as the creation of multiple websites for the office of Curriculum and  Articulation
                                       (Curriculum Committee, Articulation, Course Substitution, and  others); served as
                                       the point of contact for the State Course Numbering System  and as the Curriculum
                                       Assistant for the Curriculum Committee. After retiring  from full-time employment
                                       at the end of 2012, Kim proceeded to return in  several roles, filling in once again
                                       in her previous role as Administrative  Assistant for Curriculum and Articulation,
                                       as well as temporary assignments in  Study Abroad and Global Experiences, and Teaching
                                       and Learning. Kim also works  as a Sign Language Interpreter part-time at Valencia,
                                       dividing her time between  interpreting and working in the office of Institutional
                                       Assessment. Finally,  she worked in the temporary, eight-month position as Coordinator
                                       for Deaf and  Hard-of-Hearing Interpretation from November, 2014 to July, 2015, dividing
                                       her  time in that full-time position between interpreting in Valencia College  classes
                                       and handling the schedule for all the staff interpreters and caption  providers at
                                       Valencia.
                                    </p>
                                    
                                    <p>After obtaining her Associate of Arts degree at Valencia  College, Kim is currently
                                       enrolled at Piedmont International University in a  fully online program, with the
                                       hope of graduating with a Bachelor of Science  degree in Interpreting.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/meettheteam.pcf">©</a>
      </div>
   </body>
</html>