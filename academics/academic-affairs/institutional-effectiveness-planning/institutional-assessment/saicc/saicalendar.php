<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/saicalendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h3>CoursEval Schedule of Events, STUDENTS, Spring 2018</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Part of Term</th>
                                 
                                 <th scope="col">Classes Begin</th>
                                 
                                 <th scope="col">Student Assessments Begin</th>
                                 
                                 <th scope="col">Withdrawal Deadline</th>
                                 
                                 <th scope="col">Student Assessments End</th>
                                 
                                 <th scope="col">Classes End</th>
                                 
                                 <th scope="col">Grades Due</th>
                                 
                                 <th scope="col">Faculty Reports Open</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">First 8 weeks<br> Term A, H1
                                 </th>
                                 
                                 <td>1/8</td>
                                 
                                 <td><strong>1/31</strong></td>
                                 
                                 <td>2/16</td>
                                 
                                 <td><strong>2/21</strong></td>
                                 
                                 <td>2/28</td>
                                 
                                 <td>4/30</td>
                                 
                                 <td>5/1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">First 10 weeks<br> TWJ
                                 </th>
                                 
                                 <td>1/8</td>
                                 
                                 <td><strong>2/21</strong></td>
                                 
                                 <td>2/23</td>
                                 
                                 <td><strong>3/21</strong></td>
                                 
                                 <td>3/25</td>
                                 
                                 <td>4/30</td>
                                 
                                 <td>5/1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">FULL TERM</th>
                                 
                                 <td><strong>1/8</strong></td>
                                 
                                 <td><strong>3/28</strong></td>
                                 
                                 <td><strong>3/30</strong></td>
                                 
                                 <td><strong>4/18</strong></td>
                                 
                                 <td><strong>4/22</strong></td>
                                 
                                 <td><strong>4/30</strong></td>
                                 
                                 <td><strong>5/1</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Middle 8 Weeks LSC</th>
                                 
                                 <td>1/22</td>
                                 
                                 <td><strong>2/21</strong></td>
                                 
                                 <td>3/2</td>
                                 
                                 <td><strong>3/21</strong></td>
                                 
                                 <td>3/25</td>
                                 
                                 <td>4/30</td>
                                 
                                 <td>5/1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Second 8 weeks<br> Term B, H2
                                 </th>
                                 
                                 <td>3/1</td>
                                 
                                 <td><strong>3/28</strong></td>
                                 
                                 <td>4/13</td>
                                 
                                 <td><strong>4/18</strong></td>
                                 
                                 <td>4/29</td>
                                 
                                 <td>4/30</td>
                                 
                                 <td>5/1</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Second 10 weeks<br> TWK
                                 </th>
                                 
                                 <td>2/12</td>
                                 
                                 <td><strong>3/28</strong></td>
                                 
                                 <td>4/6</td>
                                 
                                 <td><strong>4/18</strong></td>
                                 
                                 <td>4/29</td>
                                 
                                 <td>4/30</td>
                                 
                                 <td>5/1</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>Download the CoursEval Schedules:</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Fall</th>
                                 
                                 <th scope="col">Spring</th>
                                 
                                 <th scope="col">Summer</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEval-Schedule-of-Events-Fall-2017.pdf" target="_blank">2017</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEval-Schedule-of-Events-Spring-2018.pdf" target="_blank">2018</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEval-Schedule-of-Events-Summer-2018.pdf" target="_blank">2018</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsFall2016.pdf" target="_blank">2016</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSpring2017.pdf" target="_blank">2017</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSummer2017.pdf" target="_blank">2017</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsFall2015.pdf" target="_blank"> 2015</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSpring2016.pdf" target="_blank">2016</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSummer2016.pdf" target="_blank"> 2016</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsFall14_008.pdf" target="_blank"> 2014</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSpring2015.pdf" target="_blank">2015 </a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSummer2015_001.pdf" target="_blank">2015</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsFall13.pdf"> 2013</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSpring14.pdf"> 2014</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSummer14.pdf" target="_blank"> 2014 </a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsFall12.pdf" target="_blank"> 2012</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSpring13-facultystudents_000.pdf" target="_blank">2013</a></td>
                                 
                                 <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalScheduleofEventsSummer13-facultystudents.pdf" target="_blank"> 2013</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/saicalendar.pcf">©</a>
      </div>
   </body>
</html>