<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/resourcesforusers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> 
                                    <h3><em>Student Feedback on Instruction (CoursEval)</em></h3>
                                    
                                    <p><br>
                                       <span>If you would like to skip directly to the area that pertains to you, please click
                                          the link below</span>:
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>
                                          
                                          <p><strong><a href="#Deans">Deans</a></strong></p>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <p><a href="#Instructors">Instructors</a> 
                                          </p>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <p><a href="#Staff">Administrative and Staff Assistants</a> 
                                          </p>
                                          
                                       </li>
                                       
                                       <li>                  
                                          
                                          <p><strong><a href="#Students">Students</a> </strong></p>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><a name="Deans" id="Deans"></a> Deans 
                                    </p>
                                    
                                    <p>Deans can use the following tools to help ensure that the student's feedback is appropriately
                                       utilized. 
                                    </p>
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p><strong>Initial View:</strong></p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>Department Administrator Landing Page <em>(updated info coming soon) </em></p>
                                                
                                                <p><a href="documents/Howtoreadatermnumber-graphic.docx" target="_blank">Interpreting Survey Titles </a></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Reporting Tools:</strong></div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p><a href="documents/SurveyIntelligenceReportInstructions.pdf" target="_blank">Survey Intelligence Report Instructions </a></p>
                                                
                                                <p><a href="documents/SFIHandoutforDeans-RunningReportsupdated2016-04.pdf" target="_blank">SFI Handout for Deans - Running Reports</a> 
                                                </p>
                                                
                                                <p><a href="documents/SFI20-20.pptx" target="_blank">SFI 20/20</a> 
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Reviewers Lists:</strong></div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p><a href="documents/HowtoPullFacultyNamesandCoursesforPermissions-SFI-SASInstructions7-28-2016.pdf" target="_blank">How to Pull Faculty Names and Courses for Permissions - SFI - SAS Instructions</a></p>
                                                
                                                
                                                <p><a href="documents/PermissionsfromDeansforSFIReviewers11-24-2014.xlsx" target="_blank">Permissions from Deans for SFI Reviewers 11-24-2014 xlsx</a> 
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Current Pilot Questions:</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/CoursEvalPilotQuestions-Spring2015-ver.103.06.15.pdf" target="_blank">2014-2015 CoursEval Pilot Questions </a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <p><a href="#Top">Back to top</a></p>
                                    
                                    
                                    <h2>
                                       <a name="Instructors" id="Instructors"></a>Instructors
                                    </h2>
                                    
                                    <p>Instructors can use the following tools to better understand the functions and features
                                       of CoursEval. 
                                    </p>
                                    
                                    <p>Read about the CoursEval Summer Launch in <a href="http://thegrove.valenciacollege.edu/valencia-launches-new-student-assessment-of-instruction-tool/" target="_blank">the Grove</a></p>
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Initial View: </strong></div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>Faculty Landing Page <em>(updated info coming soon)</em></p>
                                                
                                                <p><a href="http://academicmanagement.com/helpcenter/FacPortal/FacPortal.pdf" target="_blank">Faculty User Guide</a></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Reporting Tools: </strong></div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>Evaluation and Detailed Report Overview <em>(updated info coming soon)</em></p>
                                                
                                                <p><a href="documents/SurveyIntelligenceReportInstructions.pdf" target="_blank">Survey Intelligence Report Instructions </a></p>
                                                
                                                <p><a href="documents/SFI20-20.pptx" target="_blank">SFI 20/20</a></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Blackboard:</strong></div>
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/CoursEvalBlackBoardSet-UpInstructions.pdf" target="_blank">Four Quick Steps for Adding a CoursEval Link in BlackBoard</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Current Pilot Questions:</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/CoursEvalPilotQuestions-Spring2015-ver.103.06.15.pdf" target="_blank">2014-2015 CoursEval Pilot Questions </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>SFI Overview in Spanish</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/EvaluaciondeCursoforEAPstudentstoreadpriortotakingtheSFIonline9-28-2015-.pdf" target="_blank">Evaluación de Curso por el Estudiante</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">
                                                <span><strong>NEW!!</strong></span><strong> Faculty Development Training Materials </strong>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">
                                                <p>Presentation:</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div>
                                                   
                                                   <ul>
                                                      
                                                      <li><a href="../documents/SFIFacultyTrainingApril2015.pptx" target="_blank">SFI Faculty Training for Spring 2015 </a></li>
                                                      
                                                      <li><a href="documents/SFIFacultyTraining9-15-2014_001.pptx" target="_blank">SFI Faculty Training for Fall 2014 </a></li>
                                                      
                                                   </ul>
                                                   
                                                </div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="th">
                                                <p>Handouts:</p>
                                             </div>
                                             
                                             <div data-old-tag="td"> 
                                                <ul>
                                                   
                                                   <li><a href="documents/ToolsforMidtermEvaluations9-10-2012.docx" target="_blank">Tools for Midterm Evaluations</a></li>
                                                   
                                                   <li><a href="documents/ExampleofaMid-termEvaluationForm.doc" target="_blank">Example of a Midterm Evaluation Form </a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <ul>
                                                   
                                                   <li><a href="documents/SFIReportOptionsPageinCoursEvalHandoutinColoronepage.docx" target="_blank">SFI Report Options Page In CoursEval</a></li>
                                                   
                                                   <li><a href="documents/SurveyIntelligenceinMyCoursEval.pdf" target="_blank">Survey Intelligence in MyCoursEval</a></li>
                                                   
                                                   <li><a href="documents/SampleComparisonReport.pdf" target="_blank">Sample Comparison Report </a></li>
                                                   
                                                </ul>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <p><a href="#Top">Back to top</a></p>
                                    
                                    
                                    
                                    <p><a name="Staff" id="Staff"></a>Administrative Assistants
                                    </p>
                                    
                                    <p>In the CoursEval system, Administrative and Staff Assistants will be sent a .csv <a href="documents/ClassClimateInstructionsforHowtoChangeyourAcountPassword.pdf"><u></u></a>(open in Excel) to verify the courses for their department. Please verify that all
                                       of your courses are listed and correct. Once the courses are verified, please notify
                                       Institutional Assessment that this step has been completed. In the event that any
                                       courses are missing, please contact Institutional Assessment as quickly as possible
                                       for assistance.
                                    </p>
                                    
                                    
                                    
                                    <p><a href="#Top">Back to top</a></p>
                                    
                                    
                                    
                                    <h2>
                                       <a name="Students" id="Students"></a>Students
                                    </h2>
                                    
                                    <p>All students are asked to complete a CoursEval Survey for each class they take. Students
                                       should receive an email approximately 3 weeks from the end of the term. This email
                                       will include instructions on how to login to the CoursEval system and will also list
                                       all of the courses the student is enrolled in. The student's responses are completely
                                       anonymous. Instructors will receive a report containing all of their student's responses
                                       after grades have been submitted. Instructors will never know which students made
                                       which comments. 
                                    </p>
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Initial View: </strong></div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>Student Landing Page <em>(updated info coming soon)</em></p>
                                                
                                                <p>Student User Guide <em>(updated info coming soon)</em></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Anonymity and Privacy:</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/CoursEval_Anonymity.pdf" target="_blank">Anonymity and Privacy FAQ</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Survey Submission:</strong></div>
                                             
                                             <div data-old-tag="td">Survey Submission for Students <em>(updated info coming soon)</em>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Send Proof to Instructor:</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/SendProofofCompletedSurvey.pdf" target="_blank">How to Send Proof of a Completed Survey to your Instructor </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>Revise Survey Responses:</strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/EditSurveyResponses.pdf" target="_blank">How to Edit Responses submitted on SFI </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>SFI Overview in Spanish </strong></div>
                                             
                                             <div data-old-tag="td"><a href="documents/EvaluaciondeCursoforEAPstudentstoreadpriortotakingtheSFIonline9-28-2015-.pdf" target="_blank">Evaluación de Curso por el Estudiante</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>              
                                    
                                    
                                    <p><a href="#Top">Back to top</a></p>
                                    
                                    
                                    
                                    <p> <a href="https://ce1.connectedu.net/etw/ets/et.asp?nxappid=1N2&amp;nxmid=start" target="_blank">Admin -ATLAS  </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/resourcesforusers.pcf">©</a>
      </div>
   </body>
</html>