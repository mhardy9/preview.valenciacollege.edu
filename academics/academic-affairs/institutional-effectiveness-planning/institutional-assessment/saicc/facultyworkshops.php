<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/facultyworkshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h3><em>Workshops</em></h3>
                        
                        <p>Valencia faculty and staff learn how to register for assessment workshops here:
                           <a href="http://thegrove.valenciacollege.edu/registering-for-faculty-development-courses-visit-the-valencia-edge/">http://thegrove.valenciacollege.edu/registering-for-faculty-development-courses-visit-the-valencia-edge</a>
                           
                        </p>
                        
                        <hr>
                        
                        
                        <p>The Office of Institutional Assessment offers workshops for faculty and staff in assessment
                           methods and tools, research methods and protocol, and survey design and data use.
                           We are able to develop customized versions of some workshops specific to your program
                           or content area. 
                        </p>
                        
                        <p>Visit the Faculty Development website for more information: <a href="../../../../faculty/development/index.html">valenciacollege.edu/faculty/development</a>
                           
                        </p>
                        
                        <p>Please email the Institutional Assessment Office if you are interested in scheduling
                           a workshop: <a href="mailto:lblasi@valenciacollege.edu">lblasi@valenciacollege.edu</a></p>
                        
                        
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Courses Offered 2016-17 </div>
                                 
                                 <div data-old-tag="th">Day/Time </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">LOBP 3334: Using Rubrics to Create Dialogue - 3 Hours </div>
                                 
                                 <div data-old-tag="td">10/20/17 Osceola Campus 9:00am-12:00pm<br>
                                    4/5/2018 West Campus 1:00pm-4:00pm 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">     Survey Design and Development Using Qualtrics Online - 2 Hours</div>
                                 
                                 <div data-old-tag="td"> 9/20/17 Osceola Campus 9:00am-11:00am<br>
                                    10/5/17 West Campus 9:00am-11:00am Room 6-118 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Survey Data Analysis and Reporting Using Qualtrics Online - 1 Hour</div>
                                 
                                 <div data-old-tag="td">9/20/17 Osceola Campus 11:00am-12:00pm<br>
                                    10/5/17 West Campus 11:00am-12:00pm Room 6-118 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Research at Valencia and Institutional Review Board (IRB) Review  - 1 Hour</div>
                                 
                                 <div data-old-tag="td">9/20/17 Osceola Campus 11:00am-12:00pm </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>        
                        
                        <hr> 
                        <h3>Course Descriptions</h3>
                        
                        <p><strong>Research at Valencia and Institutional Review Board (IRB) Review</strong> <strong><img alt="New Icon" height="29" src="NewIcon.gif" width="31"></strong></p>
                        
                        <p>
                           Interested in conducting research at Valencia? Want to learn more about studies being
                           conducted and how they were reviewed by the IRB? This session will introduce Valencia
                           faculty and staff members to the Institutional Review Board (IRB) process. The Board
                           reviews research in compliance with federal regulations required of all colleges and
                           universities. The session will allow us to review its purpose, the process, and the
                           forms, while also discussing some best practices in the design of your own study.
                           This course is helpful for those advising or collaborating with researchers at the
                           college; those developing their own research studies (for example, those in graduate
                           school.) Participants will review the IRB purpose, the process, and the forms, while
                           also discussing some best practices in the design of their own studies. 
                           
                           
                           
                           
                           
                           
                        </p>  
                        
                        <p><strong>Survey Design and  Development Using Qualtrics Online</strong> <strong><img alt="New Icon" height="29" src="NewIcon.gif" width="31"></strong></p>
                        
                        <p>This course has been designed for staff members across the college who would like
                           to develop their own questionnaires and design them for paper or online administration
                           (using the Qualtrics survey tool.)  We will explore the most effective practices for
                           survey development drawn from research in the field and principles from the Tailored
                           Design Method (Dillman, 2007.) We will also discuss ways of understanding and communicating
                           your findings through written reports and presentations. The participants should be
                           able to identify and implement effective strategies for developing and administering
                           surveys using the Tailored Design Method (Dillman, 2007.) The participants should
                           be able to design, test, and refine a questionnaire employing effective design and
                           strategies for improving response rate. The participants should be able to analyze
                           their results in Excel and create a report format or presentation to convey key findings
                           and raise related questions for specific audiences.  
                        </p>  
                        
                        
                        <p><strong>Survey Data Analysis and Reporting Using Qualtrics Online</strong> <strong><img alt="New Icon" height="29" src="NewIcon.gif" width="31"></strong></p>
                        
                        <p>This course has been designed for staff members across the college who already develop
                           their own questionnaires online using the Qualtrics survey tool and want to learn
                           strategies for analyzing and reporting results. This course follows the Survey Design
                           and Development workshop that is also available and provides a helpful introduction
                           to the online tools. Participants will learn how to set up surveys for easier reporting,
                           access and use reporting tools, and they will discover ways of automatically generating
                           and sharing reports. 
                        </p>                         
                        
                        
                        
                        
                        <p><strong>LOBP3331:&nbsp; <img alt="New Icon" height="29" src="NewIcon.gif" width="31"> <br>Developing Meaningful Assessment Plans: Program and Discipline</strong></p>
                        
                        <p>Focused on a series of interactive activities, this course will help participants
                           develop essential skills for the program learning outcomes development process.&nbsp; Participants
                           will work with examples and exercises to develop or contribute to the development
                           of program learning outcomes plans, the collection and analysis of data across courses,
                           and the subsequent creation of program improvement plans. In this interactive working
                           session, faculty members, or groups of faculty members, attending from a specific
                           discipline or program, can develop and enhance their specific assessment plans or
                           improvement plans
                        </p>
                        
                        
                        <p><strong>LOBP3332:&nbsp;<br>
                              </strong><strong>Models and Strategies that Work – 2 PD Hours (often scheduled by request)</strong></p>
                        
                        <p>As a showcase of current and emerging best practices at Valencia, this course will
                           offer examples of models and strategies for learning outcomes assessment drawn from
                           diverse academic disciplines. The session will feature speakers from programs across
                           the college who will share the process they experienced over time along with breakthroughs
                           and lessons learned. 
                        </p>
                        
                        
                        
                        
                        <p><strong>LOBP 3334:&nbsp; <br>
                              </strong><strong>Using Rubrics to Create Dialogue </strong> <strong>– 3 PD Hours </strong></p>
                        
                        <p>This course introduces faculty to the collaborative result of intentionally using
                           rubrics for assessment at the program level. The learning occurs at the cross-section
                           between assessment and curricular design, both of which require dialogue among colleagues.
                           Through the process of developing a rubric participants learn the importance of identifying
                           measurable outcomes. While engaging in discussions of potential actionable results,
                           participants practice making use of assessment of student learning for curricular
                           improvements. 
                        </p>
                        
                        
                        
                        <p><strong>ASMT2910:&nbsp; Assess Higher Order Thinking:<br>
                              </strong><strong>Multiple Choice Question Development – 3 PD Hours</strong></p>
                        
                        <p>Faculty members attending will be able to develop, test, and revise multiple choice
                           questions drawing on hands-on activities using examples from Valencia College faculty.
                           Issues of validity including reliability will be included in the conversation.
                        </p>
                        
                        
                        
                        <p><strong>ASMT3220: Implementing Rubrics for Program Assessment – 3 PD Hours (by request)</strong></p>
                        
                        <p>This course has been designed as an interactive working session for faculty implementing
                           rubrics for the review of student work across course sections / courses.&nbsp; Faculty
                           members participating from the same programs will be invited to use their own student
                           work.&nbsp; The session will cover sampling, inter-rater reliability, norming of review
                           teams, and the consistent application of criteria to student artifacts and assignments
                           for the assessment of program learning outcomes.
                        </p>
                        
                        
                        
                        
                        <p><strong>SOTL3270:&nbsp; IRB Requirements and Your Research – 4 PD Hours</strong></p>
                        
                        <p>Meeting one-hour face-to-face and three hours online, this course will provide an
                           overview of the Institutional Review Board (IRB) for all faculty members who are planning
                           to conduct research studies at Valencia College. The online portion of the course
                           will result in nationally-recognized certification from the National Institute of
                           Health (NIH) on protecting human subjects with the expectation it is completed by
                           the 26th of the following month. The online training is self-paced and it is recognized
                           by most institutions of higher education for three years after the date of completion.
                        </p>
                        
                        
                        
                        <p><strong>SOTL3271: Principles of Good Practice</strong></p>
                        
                        
                        <hr>
                        
                        <p><a href="documents/IRBValenciaTemplateVIAWorkingCopyTLA2-24-2015.pptx" target="_blank">Principles of Good Practice in Research at Valencia College</a></p>
                        
                        <p><a href="documents/StandardsforScholarship-Destination-Blasi5-15-2013.pdf" target="_blank">Standards for Scholarship</a></p>
                        
                        <p><a href="documents/StandardsforScholarshipCompanionArticle-AlessioSOTLPBL-highlighted.pdf" target="_blank">Student Perceptions About and Performance in Problem-Based Learning</a> 
                        </p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        
                        
                        <a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"><img alt="Glossary" border="0" height="60" src="glossary.jpg" title="Click to View Glossary" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/facultyworkshops.pcf">©</a>
      </div>
   </body>
</html>