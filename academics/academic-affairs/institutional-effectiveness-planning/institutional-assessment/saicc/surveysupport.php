<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/surveysupport.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3><em>Survey Support</em></h3>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/ToregisterandcreateyourQualtricsaccount.pdf" target="_blank">Valencia faculty and staff learn how to set up your Qualtrics account here </a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                      
                        
                        <hr>
                        
                        
                        <p>The Office of Institutional Assessment provides guidance and support for faculty and
                           staff members seeking to create, send, and interpret surveys. Qualtrics is an online
                           tool available to support survey development, distribution, and reporting.&nbsp; 
                        </p>
                        
                        <p>Get acquainted with Qualtrics: <a href="https://www.qualtrics.com/support/" target="_blank">https://www.qualtrics.com/support/</a> 
                        </p>
                        
                        
                        <p>Please email the Institutional Assessment Office if you need help developing your
                           surveys, using Qualtrics, or learning more from your results: <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a>.
                        </p>
                        
                        <hr> 
                        
                        
                        
                        <h3>
                           
                           
                           <img alt="Image of Qualitrics survey and what it helps you do. Design user friendly surveys for stream-lined data collection. Distribute assessments to individual contact. Analyze and filter data for meaning. Publish reports with visualizations of the data. " height="250" src="LowPriority-Addsurveyimage-SurveySupport-forfullpagerevisionNJ.JPG" width="520">
                           
                           
                        </h3>
                        
                        
                        <p> <u><a href="http://thegrove.valenciacollege.edu/qualtrics-upgrade-in-progress-more-than-20-new-features-will-be-available/" target="_blank">Qualtrics features and support available 4/19/2016</a></u> 
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        
                        
                        <a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"><img alt="Glossary" border="0" height="60" src="glossary.jpg" title="Click to View Glossary" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/surveysupport.pcf">©</a>
      </div>
   </body>
</html>