<ul>
<li><a href="../index.php">Institutional Assessment</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<a href="../index.php"><h3>Home</h3></a><li><a href="../Calendar.php">Calendar &amp; Tools</a></li>
<li><a href="FacultyWorkshops.php">Workshops</a></li>
<li><a href="../fg/index.php">Focus Groups</a></li>
<li><a href="SurveySupport.php">Survey Support</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../loa/index.php"><h3>Learning Outcomes Assessment (LOA) </h3></a><li><a href="../LOA/assessment_plans.php">Assessment Plans</a></li>
<li><a href="../loa/outcomes_GenEd.php">General Education</a></li>
<li><a href="../loa/qep.php">Quality Enhancement Plan (QEP)</a></li>
<li><a href="../oo/index.php">Tools</a></li>
<li><a href="../loa/Resources.php">Resources</a></li>
<li><a href="../loa/Examples.php">Examples</a></li>
<li><a href="../loa/forms_reports.php">Forms, Reports, &amp; More</a></li>
<li><a href="../loa/program_outcomes.php">Degree and Career Program Outcomes</a></li>
<li><a href="../dt/index.php">Data Team</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="index.php"><h3>Student Feedback Survey</h3></a><li><a href="SAICalendar.php">Calendar &amp; Tools</a></li>
<li><a href="SAIReports.php">Reports</a></li>
<li><a href="ResourcesforUsers.php">Resources</a></li>
<li><a href="LoginHere.php">Sign In Here</a></li>
<li><a href="SFIFrequentlyAskedQuestions.php">Frequently Asked Questions</a></li>
<li><a href="../surveys/ccsse.php">CCSSE Survey</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../surveys/index.php"><h3>Planning</h3></a><li><a href="../surveys/online-learning.php">Online Learning</a></li>
<li><a href="../surveys/direct-connect.php">DirectConnect 2.0</a></li>
<li><a href="../surveys/new-student-experience.php">New Student Experience</a></li>
<li><a href="../surveys/faculty-engagement.php">Part-time Faculty Engagement</a></li>
<li><a href="../sam/index.php">State Assessment Meeting</a></li>
<li><a href="../AspenPrizeApplication.php">Aspen Prize</a></li>
<li><a href="../MeetTheTeam.php">Meet the Team</a></li>
</ul></div>
</div>
</li>
</ul>