<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/saireports.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> 
                                    <h3><em><a name="Top" id="Top"></a>CoursEval Reports</em></h3>
                                    
                                    <p>CoursEval at Valencia is used by faculty members to improve the teaching and learning
                                       experience.
                                    </p>
                                    
                                    <p>On this Webpage you will find reports to help you to understand the use and impact
                                       of the SFI - whether you are a student, a faculty member, a dean,or one of the Valencia
                                       staff members working with the survey. 
                                    </p>
                                    
                                    <p>We are also providing face-to-face trainings through the <a href="documents/edge-schedule-SummerFall2012.pdf" target="_blank">EDGE</a> and <a href="../../../../faculty/development/index.html" target="_blank">Faculty Development</a>. 
                                    </p>
                                    
                                    
                                    <h2>CoursEval Returns by Campus and Discipline</h2>
                                    
                                    <hr>
                                    
                                    <h3><em>Reports</em></h3>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/201720-The-Presidents-Dashboard-SFI-Response-Rates-Report-All-Enrollment.pdf" target="_blank">Spring 2017 Returns by Campus and Discipline</a></li>
                                       
                                       <li>
                                          <a href="documents/201710ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Fall 2016 Returns by Campus and Discipline</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/201630ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Summer 2016 Returns by Campus and Discipline</a> 
                                       </li>
                                       
                                       <li><a href="documents/201620ThePresidentsDashboardSFIResponseRatesReportAllEnrollmentv2.pdf" target="_blank">Spring 2016 Returns by Campus and Discipline</a></li>
                                       
                                       <li>
                                          <a href="documents/201610ThePresidentsDashboardSFIResponseRatesReportAllEnrollmentv2.pdf" target="_blank">Fall 2015 Returns by Campus and Discipline</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/201530ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Summer 2015 Returns by Campus and Discipline</a> 
                                       </li>
                                       
                                       <li><a href="documents/201520ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Spring 2015 Returns by Campus and Discipline </a></li>
                                       
                                       <li><a href="documents/201510ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Fall 2014 Returns by Campus and Discipline</a></li>
                                       
                                       <li><a href="documents/201430_ThePresidentsDashboardSFIResponseRatesReportAllEnrollment_000.pdf" target="_blank">Summer 2014 Returns by Campus and Discipline</a></li>
                                       
                                       <li><a href="documents/201420_ThePresidentsDashboardSFIResponseRatesReportAllEnrollment.pdf" target="_blank">Spring 2014 Returns by Campus and Discipline</a></li>
                                       
                                       <li><a href="documents/President_Dashboard_Student_Feedback_on_Instruction-Fall2013.pdf" target="_blank">Fall 2013 Returns by Campus and Discipline </a></li>
                                       
                                       <li><a href="documents/FinalResponseReportAllCampusesbyDivisionSummer2013.pdf" target="_blank">Summer 2013 Returns by Campus and Discipline</a></li>
                                       
                                       <li>
                                          <a href="documents/FinalResponseReportAllCampusesbyDivisionSpring2013-revised.pdf" target="_blank">Spring 2013 CoursEval Returns By Campus and Discipline</a> 
                                       </li>
                                       
                                       <li><a href="documents/StudentAssessmentofInstruction-resultsoverview-Blasi6-25-2012.pdf" target="_blank">Survey of Students- 2012 Results Part 1- Summary</a></li>
                                       
                                       <li><a href="documents/SAISurvey-StudentCommentsReportJKLB6-29-2012.pdf" target="_blank">Survey of Students- 2012 Results Part 2- Qualitative</a></li>
                                       
                                       <li><a href="documents/ResponseRatesforStudentAssessmentofInstructionSurvey-CollegeSummary.pdf" target="_blank">Summer 2012 </a></li>
                                       
                                       <li><a href="documents/201310_CompleteSAIResponseRate_CampusbyDivision_12-18-12_000.pdf" target="_blank">Fall 2012</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/saireports.pcf">©</a>
      </div>
   </body>
</html>