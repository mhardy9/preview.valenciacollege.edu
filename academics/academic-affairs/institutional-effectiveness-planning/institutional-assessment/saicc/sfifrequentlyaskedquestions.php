<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/sfifrequentlyaskedquestions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/">Saicc</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> 
                                    <h3><em>Frequently Asked Questions</em></h3>
                                    
                                    
                                    
                                    
                                    
                                    <div>
                                       
                                       
                                       <h3><strong>Why am I being asked to evaluate my professor before the end of the semester?</strong></h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p> A. The faculty decided to pilot a new schedule for obtaining student feedback that
                                                allows them to receive feedback from students who may withdraw before the withdrawal
                                                deadline and separates the process from final exams. This schedule will be evaluated
                                                at a later date using feedback from both the student population and faculty members.
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <h3><strong>Are the surveys anonymous?</strong></h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <p>A. Yes, survey responses are anonymous. Professors and deans will not see any responses
                                                until after grades are submitted. Even then, no student names are associated with
                                                comments. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       <h3> <strong>Why can't I log in?</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p> A. There are two possible reasons why you are having trouble logging in to the system.
                                                
                                             </p>
                                             
                                             <p>1. We have found that for some students, the last time they changed their Atlas passwords,
                                                they did not correctly sync across all of Valencia’s systems. The only way to force
                                                a reset is to change your Atlas password again. I understand that this can be an inconvenience,
                                                and apologize for that. If you remember your previous Atlas password, you might be
                                                successful using that to log in. 
                                             </p>
                                             
                                             <p>2. If you are receiving a "Not a valid CoursEval user" error, then you have not been
                                                added to the CoursEval system. New users are added at different times during the semester
                                                just before surveys launch. Please refer to our calendar for survey availablility
                                                dates. You will receive an email notification when your available surveys are open
                                                and will have access at that time. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <h3> <strong>Oops! I filled out the survey wrong—can you reset it for me? I can't see my completed
                                             surveys.</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. Students can edit their survey responses until the surveys close. To edit your
                                                responses, click on <strong>SURVEYS</strong> toward the upper left corner of the homepage and choose <strong>COMPLETED SURVEYS</strong>. All of your completed surveys will be displayed. You can bring the most recent completion
                                                to the top of the list by selecting the downward arrow to the right of<strong> Date/Time Completed</strong>. Select <strong>Edit Survey Answers</strong> in the <strong>Date Closed</strong> column to edit your responses.
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       <h3> <strong>I didn’t take a survey, and the deadline has passed. Can you re-open it for me?</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. Unfortunately, once a survey is closed, we are unable to re-open it. The faculty
                                                and deans value your feedback and hope you will be able to participate in future semesters.
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       <h3> <strong>How do I send proof that I completed the survey to my professor?</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A: To send proof of completion to your instructor, follow these steps: </p>
                                             
                                             <p>1. Log into CoursEval: <a href="http://tiny.cc/CoursEval_Student" target="_blank">http://tiny.cc/CoursEval_Student</a></p>
                                             
                                             <p>2. Select the<strong> Surveys</strong> link toward the top left corner of the homepage and then choose <strong>Completed Surveys</strong>.
                                             </p>
                                             
                                             <p>*Note: If you are on the mobile site, please click on the <strong>Full Site</strong> link to be able to access your Completed Surveys. 
                                             </p>
                                             
                                             <p>3. On the Completed Surveys page, make sure the most recent surveys appear by clicking
                                                on the downward arrow to the right of <strong>Date/Time Completed</strong>.
                                             </p>
                                             
                                             <p>4. In the <strong>Date/Time Completed</strong> column, click on the <strong>Send Proof</strong> link for your completed survey. An email will be sent to your professor letting them
                                                know that you have completed the course survey. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <h3> <strong>When I click the “send proof” link, what does my professor see?</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. The send proof option sends an email to the instructor informing them that you
                                                have successfully submitted your survey. This is what the message will say and it
                                                does not include your survey responses: 
                                             </p>
                                             
                                             <p>The participant shown below has recently completed an assigned evaluation.<br>
                                                <br>
                                                Participant............: Your Name <br>
                                                Date/Time Submitted....: The date and time submitted <br>
                                                <br>
                                                Survey.................: Survey Name <br>
                                                Course Number..........: Course Number <br>
                                                Course Name............: Course Name <br>
                                                Course Section.........: Course Section 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3><a href="#"><strong>Why are students being surveyed before the end of the term or why are the surveys
                                                being administered earlier than before?</strong></a></h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. The decision to open the surveys before the end of term and at various time periods
                                                during the semester was made by faculty, who are piloting these changes as a result
                                                of research showing it is best to separate these surveys from final exams. Also, faculty
                                                hope to capture feedback from students who may withdraw. This schedule will be evaluated
                                                at a later date using feedback from both the students and faculty. A copy of the SFI
                                                administration dates can be found here: <a href="http://tiny.cc/SFI-calendar">http://tiny.cc/SFI_calendar</a> 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>  
                                       
                                       
                                       <h3> <strong>Can students access the SFI through the tab/link in our class Blackboard site too?</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. The survey software (CoursEval) does not currently allow for a single sign on with
                                                Blackboard. The company has been working on this issue, and we hope to have that feature
                                                available soon. In the meantime, students can access the survey portal by clicking
                                                the link embedded in all of our e-mail correspondence. Additionally, the "Student
                                                Feedback on Instruction (SFI) - CoursEval" link within Blackboard will direct students
                                                to the CoursEval portal.
                                             </p>
                                             
                                          </div>  
                                          
                                       </div>  
                                       
                                       
                                       <h3><a href="#"><strong>Why aren’t all of the classes I’m teaching appearing on the list of courses to take
                                                the SFI?</strong></a></h3>
                                       
                                       <div>
                                          
                                          <div>  
                                             
                                             <p>A. The faculty decided to try a new schedule for administering the SFI to students.&nbsp;
                                                This schedule will be reviewed at a later date and will include feedback from both
                                                the student population and faculty members.&nbsp; SFI schedule of administration dates
                                                can be found at: <a href="http://tiny.cc/SFI-calendar">http://tiny.cc/SFI_calendar</a></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3> <strong>This list of courses/students doesn’t look right: Something is missing or something
                                             is showing that shouldn’t be.</strong>
                                          
                                       </h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. Your roster of students taking the SFI should reflect everyone who was enrolled
                                                (those who stayed in your class and those who withdrew). The decision to make the
                                                survey available to students who withdrew was a faculty-made decision. If any courses
                                                are missing, or students appear to be incorrect or omitted from the list and they
                                                are currently enrolled, please contact our office for assistance. Please contact Deidre
                                                Holmes DuBois at <a href="mailto:dholmesdubois@valenciacollege.edu">dholmesdubois@valenciacollege.edu</a> with questions related to the decisions made by the faculty. Please see her related
                                                governance update from Fall 2013 here: <a href="http://tiny.cc/DHolmesDuboisArticle">http://tiny.cc/DHolmesDuboisArticle</a> 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3>
                                          <a href="#"> <strong>What should I do if I cannot see the correct survey results using the reporting tools?</strong></a>
                                          
                                       </h3>
                                       
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. First check your filters to ensure they are not preventing you from seeing the
                                                data you need.&nbsp; The filters can be found beneath the menu: 
                                             </p>
                                             
                                             
                                             <p>If after clearing all of your filters you are still not seeing the courses you need,
                                                please contact Institutional Assessment at <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a>. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3><a href="#"><strong>Is there a unique URL (hyperlink) for my class that I can send to my students?</strong></a></h3>
                                       
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. CoursEval is a portal system, much like Atlas, which students and faculty must
                                                log in to; therefore, we cannot generate unique URLs for each course. The URL for
                                                the portal is <a href="http://tiny.cc/CoursEval_Student">http://tiny.cc/CoursEval_Student</a> and users should log in with their regular Valencia credentials.
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3><a href="#"><strong>Can I add my own questions to the surveys for my class?</strong></a></h3>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>A. Faculty cannot add questions to their own individual surveys in CoursEval. However,
                                                faculty members are able to use the Qualtrics online survey tool, which allows them
                                                to create and administer customized surveys to Valencia College students.&nbsp; <a href="http://tiny.cc/Valencia_Qualtrics">http://tiny.cc/Valencia_Qualtrics</a> 
                                             </p>
                                             
                                             <p><strong>Current CoursEval Pilot Questions:</strong> <a href="documents/CoursEvalPilotQuestions-Spring2015-ver.103.06.15.pdf" target="_blank">http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/documents/CoursEvalPilotQuestions-Spring2015-ver.103.06.15.pdf</a></p>
                                             
                                             <p><strong>Learn more about the recent accomplishments of the Faculty subcommittee here:</strong><strong> </strong><a href="http://wp.valenciacollege.edu/thegrove/studentfeedbackofinstruction/">http://wp.valenciacollege.edu/thegrove/studentfeedbackofinstruction/</a> 
                                             </p>
                                             
                                             <p><strong>Access a script for discussing the SFI with your students here: </strong><a href="http://tiny.cc/SFI-Script">http://tiny.cc/SFI-Script</a> 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <h3>
                                          <a href="#"><strong>Additional questions?</strong></a>
                                          
                                       </h3>
                                       
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <p>Please contact the Office of Institutional Assessment at<strong> <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a>.</strong></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/sfifrequentlyaskedquestions.pcf">©</a>
      </div>
   </body>
</html>