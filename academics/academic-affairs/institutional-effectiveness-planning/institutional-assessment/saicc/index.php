<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Saicc</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Survey Feedback Survey</h2>
                        
                        
                        
                        <h3> Your CourseEval account requires your Atlas username and password. Faculty and students
                           can log in <a href="https://p1.courseval.net/etw/ets/et.asp?nxappid=1N2&amp;nxmid=start" target="_blank">here</a>. This link will redirect you to Valencia's Atlas portal so you can log in. Please
                           contact OIT if you have trouble logging in: <a href="https://valenciacollege.zendesk.com/hc/en-us/requests/new">Information Technology Help Desk Request </a>
                           
                        </h3>
                        
                        
                        <hr>
                        
                        
                        <p>Each term students taking courses are asked to complete the Student Feedback on Instruction
                           survey to let us know more about their experiences. It is used at Valencia by faculty
                           members to improve the teaching and learning experience. On this Web page you will
                           find materials to help you to use CoursEval - whether you are a student, a faculty
                           member, a dean,or one of the Valencia staff members working with the survey. 
                        </p>
                        
                        <hr>
                        
                        
                        
                        
                        <p><strong><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/SAICC/documents/CoursEvalPilotQuestions-Spring2015-ver.103.06.15.pdf" target="_blank">CoursEval - Questions Asked (2016-2017)</a></strong></p>                      
                        
                        
                        <p><strong>The survey is described further in the <a href="http://thegrove.valenciacollege.edu/valencia-launches-new-student-assessment-of-instruction-tool/" target="_blank">Grove</a>.</strong></p>
                        
                        <hr>     
                        
                        
                        <h2>The Student Feedback on Instruction (SFI) Survey (online through CoursEval)</h2>
                        
                        <p><strong>The following graphic illustrates the cycle of the CoursEval Survey. </strong></p>
                        
                        
                        
                        <h2>Frequently Asked Questions are available <a href="SFIFrequentlyAskedQuestions.html" target="_blank">here</a>. 
                        </h2>
                        
                        <p>We are located in the District Office and can be reached through <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu.</a></p>
                        
                        
                        <p><a href="#Top">TOP</a></p>
                        
                        
                        
                        
                        
                        <p><strong>Admin Tools </strong></p>
                        
                        <ul>
                           
                           <li><a href="https://ce1.connectedu.net/etw/ets/et.asp?nxappid=1N2&amp;nxmid=start" target="_blank">Admin -ATLAS</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/SAICC/documents/TinyURLs.pdf" target="_blank">Web Links</a></li>
                           
                           <li><a href="https://p1.courseval.net/etw/ets/et.asp?nxappid=1N1&amp;nxmid=start&amp;cktr=&amp;ticket=ST-855387-UGPQfLgmLHUGbLd0YGg3-ptl5-cas-prod.valenciacollege" target="_blank">Manager Portal</a></li>
                           
                        </ul>
                        
                        
                        <p><a href="#Top">TOP</a></p>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        
                        
                        <a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"><img alt="Glossary" border="0" height="60" src="glossary.jpg" title="Click to View Glossary" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/index.pcf">©</a>
      </div>
   </body>
</html>