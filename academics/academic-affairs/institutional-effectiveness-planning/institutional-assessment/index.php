<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	 
                        <div class="box_style_1">
                           
                           	
                           <h2>Welcome to Valencia’s Institutional Assessment (VIA) Office:</h2>
                           
                           <h3>Supporting Faculty and Staff Members Assessing What Matters Most</h3>
                           
                           
                           <p><em>“Improvement—if it is to happen—will occur where faculty and staff have the most leverage
                                 to change how they approach teaching and learning.”<br>
                                 </em>--Ewell, Paulson, and Kinzie, 2011
                           </p>
                           
                           
                           <hr>     
                           <p>Valencia's Institutional Assessment (VIA) office is responsible for the support of
                              faculty and staff members who are defining, gathering, analyzing and learning from
                              data related to student learning outcomes. We document and report the college-wide
                              results of the assessment of program learning outcomes that is being led by deans,
                              directors, coordinators, and other staff and faculty members.
                           </p>
                           
                           <ul>
                              
                              <li><a href="documents/ForFacultyMembers-InstitutionalAssessmentOverview7-27-2016.pdf" target="_blank">Overview for Faculty Members</a></li>
                              
                              <li>
                                 <a href="documents/AnnualReport-VIA-LauraBlasi-8-30-2016-final.pdf" target="_blank">Valencia  Assessment Annual Report 2015-2016</a>  
                              </li>
                              
                           </ul>          
                           
                           <p>We are located in the District Office and are available to meet throughout the week
                              on each campus by appointment. We work in partnership with the Office of Faculty Development
                              and the Office of Institutional Research. Program assessment at Valencia is part of
                              a cyclical process that includes culminating events that are coordinated by the office
                              – such as Assessment Day. We collaborate with faculty and staff as we:     
                           </p>
                           
                           <ul>
                              
                              <li>Assist with the development of surveys and the analysis and report of results</li>
                              
                              <li>Provide support for those who want to use the Qualtrics survey tool</li>
                              
                              <li>Collaborate on assessment projects that use qualitative methods</li>
                              
                              <li>Develop and coordinate focus groups for program assessment</li>
                              
                              <li>Participate in program-wide planning sessions and retreats to assure the reliable
                                 use of research methods as well as to support the integration of assessment strategies
                                 into the long-term vision.
                              </li>
                              
                           </ul>
                           
                           <p>We also are responsible for several activities as we:</p>
                           
                           <ul>
                              
                              <li>Administer national surveys - sharing data and related reports</li>
                              
                              <li>Oversee the administration of the Student Feedback on Instruction (SFI)</li>
                              
                              <li>Coordinate the State Assessment Meeting conference </li>
                              
                              <li>Oversee the Institutional Review Board (IRB)</li>
                              
                           </ul>
                           
                           <p>We are available for conversation regarding projects and proposals in their planning
                              stages or in progress, we are also involved with a range of on-going projects at Valencia
                              College such as the Carnegie Statway project (focused on student learning in the areas
                              of statistics, data analysis and quantitative reasoning) while contributing to discussions
                              held within other nationally-funded projects such as Achieving the Dream (AtD.) 
                           </p>
                           
                           
                           
                           <p>Laura Blasi, Ph.D., Director, Institutional Assessment<br>
                              E-mail: <a href="mailto:lblasi@valenciacollege.edu">lblasi@valenciacollege.edu</a><br>
                              Phone: <a href="tel:407-582-3486">407-582-3486</a></p>
                           
                           <p><strong><a href="https://valenciacollege.edu/via">www.valenciacollege.edu/via</a></strong></p>
                           
                           <p><a href="documents/VIAOfficeOverviewHandout8-4-2016.pdf" target="_blank">Valencia Institutional Assessment- Overview</a> 
                           </p>
                           
                           
                           
                        </div> 
                        
                     </div>  
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.pcf">©</a>
      </div>
   </body>
</html>