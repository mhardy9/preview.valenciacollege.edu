<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/AspenPrizeApplication.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Institutional Assessment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <p>In December 2011 the Aspen Institute College Excellence Program announced to the press
                           that “Valencia College in Orlando, Florida, is the nation’s top community college”
                           as they also honored four ‘finalists with distinction’ from an original pool of over
                           1,000.&nbsp; Valencia received a $600,000 prize to support its programs while each “finalist
                           with distinction” will receive $100,000. The Finalists with Distinction are: Lake
                           Area Technical Institute (Watertown, SD); Miami Dade College (Miami, FL); Walla Walla
                           Community College (Walla Walla, WA); and West Kentucky Community and Technical College
                           (Paducah, KY).
                        </p>
                        
                        <p>The announcement followed a rigorous, year-long effort by the Aspen Institute, as
                           described in their press release, “to assemble and review an unprecedented collection
                           of data on community colleges and the critical elements of student success: student
                           learning, degree completion and transfer, equity and employment/earnings after college…&nbsp;
                           Nearly half of Valencia’s students are underrepresented minorities — African American,
                           Hispanic/Latino or Native American — and many are low-income. Yet, more than 50 percent
                           graduate or transfer within three years of entering college, compared to under 40
                           percent for community colleges nationally.” 
                        </p>
                        
                        <p>Dr. Sandy Shugart, President of Valencia College shared the news with faculty, staff,
                           and administrators through a 2 minute video: <a href="http://www.youtube.com/watch?v=GuQFw8NAbjo" target="_blank">http://www.youtube.com/watch?v=GuQFw8NAbjo</a>. Carol Traynor in Valencia’s office for Marketing &amp; Strategic Communication, described
                           the award in a press release:<a href="http://news.valenciacollege.edu/academic-issues/valencia-named-top-community-college-in-nation/" target="_blank"> http://news.valenciacollege.edu/academic-issues/valencia-named-top-community-college-in-nation/</a>.&nbsp; More details regarding this honor and progress related to student completion in
                           community colleges can be found in the online articles listed below from the Aspen
                           Institute, The Chronicle of Higher Education, Inside Higher Ed, and The Community
                           College Times.
                        </p>
                        
                        <p>   First Annual Aspen Prize Caps Year-long Effort to Recognize Excellence in Nation’s
                           1,200 Community Colleges, Which Serve Nearly Half of All Undergraduates Nationally
                           <a href="http://www.aspeninstitute.org/news/2011/12/12/valencia-college-wins-aspen-prize" target="_blank">(press release) </a></p>
                        
                        <p>Completion Comes First <a href="http://www.communitycollegetimes.com/Pages/Campus-Issues/Valencia-College-receives-Aspen-Prize.aspx" target="_blank">(Dec.13, 2011) </a></p>
                        
                        <p>Valencia College REceives Aspen Prize <a href="http://www.communitycollegetimes.com/Pages/Campus-Issues/Valencia-College-receives-Aspen-Prize.aspx" target="_blank">(Dec. 12, 2011) </a></p>
                        
                        <p>Valencia College Wins First Aspen Prize for Community College Excellence <a href="http://chronicle.com/article/Valencia-College-Wins-First/130091/" target="_blank">(Dec. 12, 2011)</a> 
                        </p>
                        
                        <h3>Aspen Prize Application *** Please note: the materials used in the Aspen application
                           are no longer current**
                        </h3>
                        
                        <p>Please contact us for updated information: <a href="../../../contact/index.html" target="_blank">http://preview.valenciacollege.edu/contact/</a></p>
                        
                        <p><a href="http://click.icptrack.com/icp/relay.php?r=64018022&amp;msgid=808877&amp;act=SU3H&amp;c=337460&amp;destination=http%3A//www.aspeninstitute.org/sites/default/files/content/docs/ccprize/AspenCCPrizeOverview.pdf">Five Page Overview of the Aspen Prize</a></p>
                        
                        <p><a href="http://click.icptrack.com/icp/relay.php?r=64018022&amp;msgid=808877&amp;act=SU3H&amp;c=337460&amp;destination=http%3A//www.aspeninstitute.org/sites/default/files/content/docs/ccprize/Aspen_Prize_Round2_cover_letter.pdf">The Original Letter That Went Out With the Hard Copy Packet</a></p>
                        
                        
                        <p>Aspen Prize for Community College Excellence<br>
                           Round 2 Application
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Institutional Information And Profile</strong></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Please complete the following contact information, and provide enrollment data for
                           your institution in the attached “Enrollment Profile” template. 
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Institution name:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Valencia College </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Designated contact person/Title:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Dr. Joyce Romano, Vice President for Student Affairs </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Contact telephone:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-582-3402</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Contact email:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="mailto:jromano@valenciacollege.edu">jromano@valenciacollege.edu</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Institution address:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>190 S. Orange Ave</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>City, State, Zip:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Orlando, Florida 32801-3204</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Website:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>www.valenciacollege.edu</p>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>President’s name:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Dr. Sanford Shugart</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>President’s Email:</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="mailto:sshugart@valenciacollege.edu">sshugart@valenciacollege.edu</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Number of years current president has held the position:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>11Years</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>President’s Assistant:</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Barbara Halstead</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Assistant’s Email:</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>bhalstead@valenciacollege.edu</p>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Institutional Mission: </strong><strong>In approximately 100 words, describe your mission, the populations you serve, and
                                          the programs you offer. </strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia College, located in Orange and Osceola Counties in Central Florida, is a
                                       public, comprehensive community college that continually identifies and addresses
                                       the changing learning needs of the communities it serves. Valencia provides opportunities
                                       for academic, technical and life-long learning in a collaborative culture dedicated
                                       to inquiry, results and excellence.
                                    </p>
                                    
                                    
                                    <p>Valencia provides: associate degree programs that prepare learners to succeed in university
                                       studies; courses and services that provide learners with the right start in their
                                       college careers; and associate degree, certificate, bachelors, and continuing professional
                                       education programs that prepare learners for entering and progressing in the workforce.
                                       In 2010-11, approximately 65,000 students took courses at seven campus or center locations.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="#top">TOP</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Faculty composition</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <p>Number of Faculty</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Percentage of All Faculty</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Full-Time&nbsp; Faculty </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>392</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>24%</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Part-Time Faculty </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1262</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>76%</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>External Partners</strong>: On no more than one page total, please list external entities (including individual
                           and consortia from K-12, business, non-profit, research, four-year colleges or other
                           sectors) with which your community college is engaged in partnerships that are important
                           to the student outcomes your institution has achieved. Provide a brief explanation
                           (no more than 50 words for each) of the role these partnerships have played at your
                           institution:
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Partner Entity</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Key Contact</strong></p>
                                    
                                    <p><strong>Name/Title and Email</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Description of Partnership</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Orange County Public Schools (OCPS)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Barbara Jenkins</p>
                                    
                                    <p>Deputy Superintendent</p>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>K12 Partnership – We have a formal Interinstitution Articulation Agreement with OCPS
                                       that names a coordinating group that meets at least annually and delineates our partnership
                                       in dual enrollment, career pathways, entry assessment, preparation for college, scholarships,
                                       faculty and staff development, and college transition programs with middle school
                                       and high school students. 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>School District of Osceola County (SDOC)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>&nbsp;Pamela Tapley </p>
                                    
                                    <p>Assistant Superintendent of Secondary Curriculum and Instruction</p>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>K12 Partnership – We have a formal Interinstitution Articulation Agreement with SDOC
                                       that names a coordinating group that meets at least annually and delineates our partnership
                                       in dual enrollment, career pathways, entry assessment, preparation for college, scholarships,
                                       faculty and staff development and college transition programs with middle school and
                                       high school students.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>University of Central Florida, Direct Connect</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Tony Waldrop</p>
                                    
                                    <p>Provost and Vice President</p>
                                    
                                    <p>Academic Affairs</p>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Direct Connect is a unique 2+2 partnership that guarantees admission of all Valencia
                                       graduates into the University of Central Florida as juniors. Valencia and UCF staff
                                       has built pathways for early advising, transcript sharing, and early admission to
                                       ensure a smooth transition to the university. UCF also provides its bachelors programs
                                       on two Valencia campuses. Shared buildings, libraries and joint philanthropy are other
                                       features of this unique partnership which currently 60,000 Valencia students have
                                       joined. &nbsp;
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Business and Industry Representatives:&nbsp; Program Advisory Committees</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Falecia D. Williams </p>
                                    
                                    <p>West Campus President</p>
                                    
                                    <p><a href="mailto:fawilliams@valenciacollege.edu">fawilliams@valenciacollege.edu</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Advisory Committees comprised of local business and industry representatives, faculty,
                                       and staff serve as a cooperative and collaborative entity for examining workforce
                                       trends, curricular content, and student learning outcomes.&nbsp; Advisory Committees are
                                       a significant partnership for engaging the interdependence economic development and
                                       academic training of a quality workforce.&nbsp; 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Business and Industry Representatives:&nbsp; Employers of Students Interns</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Falecia D. Williams</p>
                                    
                                    <p>West Campus President</p>
                                    
                                    <p><a href="mailto:fawilliams@valenciacollege.edu">fawilliams@valenciacollege.edu</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Employers provide onsite workplace learning experiences to Associate in Arts, Associate
                                       in Science, and Associate in Applied Science students pursuing a degree within the
                                       designated industry sector.&nbsp; They work collaboratively with faculty supervisors to
                                       establish and evaluate learning and performance objectives that &nbsp;support the program
                                       outcomes of the degree in which the student is enrolled.&nbsp; Students are also provided
                                       a formal opportunity to network with potential employers.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Business and Industry Representatives:&nbsp; Affiliation Agreements</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Falecia D. Williams</p>
                                    
                                    <p>West Campus President</p>
                                    
                                    <p><a href="mailto:fawilliams@valenciacollege.edu">fawilliams@valenciacollege.edu</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Corporations and businesses provide supplemental funding, clinical sites, and use
                                       of their facilities and resources via signed affiliation agreements.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>The State of Florida has a statewide articulation agreement between community colleges
                                       and the eleven State universities that guarantees any student who graduates with an
                                       A.A. Degree will be accepted to one of the eleven state universities.&nbsp; In addition,
                                       Valencia has partnered with private institutions to expand student options for seamless
                                       transition. Some of the agreements are created with an Articulated Pre-Major. They
                                       are designed for students to transfer to a particular public or private university
                                       as a junior to complete a four-year bachelor degree in a specific major.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Barry University</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Al McCullough, Exec. Interim Dir.<br>
                                       ACE Enrollment Management<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2+2 partnership</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Belhaven College</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>&nbsp;Alexis Fields, Campus Dir.</p>
                                    
                                    
                                    <p>Dr. Richard Harris, AVP for Adult Studies</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2+2 partnership</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Capella Universtiy</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Ricardo Grant</p>
                                    
                                    <p>Account Manager</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2+2 partnership</p>
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Section 1. Completion Outcomes </strong></p>
                                    
                                    <p><strong>(no more than 3 pages of written responses total for this section)</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <ol>
                           
                           <li>Please use the attached templates to provide data on student progression, transfer,
                              and completion (including those who begin in developmental education).
                           </li>
                           
                           <li>In 500 or fewer words, summarize the specific programs or factors that you believe
                              have contributed to success in student completion, improvements over time in student
                              completion, or specific achievements demonstrated in your completion data. 
                           </li>
                           
                        </ol>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia has been on a journey to become a <u>more learning-centered college</u> since 1994. We have <u>advanced our strategic planning through alignment with national initiatives</u> to improve student success (i.e. student persistence, course &amp; degree completion).&nbsp;
                                       A broad, deep, and continuing conversation has resulted to transform our organizational
                                       culture and improve student completion of courses and degrees. Through the <u>ACE/Kellogg Project (1995</u>), Valencia began engaging faculty/staff in new conversations about learning, common
                                       purpose, and educational issues. A culture of programmatic development emerged to
                                       improve student learning and completion.<u></u></p>
                                    
                                    <p>One such institutional effort was the <u>College Prep Task Force</u> through which procedural, curricular and learning support changes were made to improve
                                       students’ completion of developmental education. This reform work continued in the
                                       <u>College Level Achievement Initiative</u>.&nbsp; Faculty reviewed and aligned the curriculum in conjunction with targeted high school
                                       teachers through a summer faculty development program, Destinations (which has continued
                                       annually). Completion of the developmental education sequence in two years in reading,
                                       writing and mathematics improved from the 30-40’s% in 1991 to over 70% in reading
                                       and writing and nearly 60% in math (2008).
                                    </p>
                                    
                                    <p>Valencia’s study of student learning and success through <u>Title III programs created the institutional momentum</u> and design of <u>LifeMap</u> (our developmental advising model), <u>Teaching and Learning Academy</u> (faculty induction process), <u>core competencies of a Valencia graduate</u> (Think, Value, Communicate and Act), <u>engaged teaching practice</u> (through learning strategies, inclusive classrooms, action research, “connection
                                       and direction”). 
                                    </p>
                                    
                                    <p>As a <u>Vanguard Learning College</u> (League for Innovation), Valencia adopted the two fundamental learning-centered questions;
                                       “How will this impact student learning? and How will we know?” These questions established
                                       a <u>community of learning leaders and the now ingrained practice of collaboration focused
                                          on student success</u>. 
                                    </p>
                                    
                                    <p>Valencia developed a proposal for the <u>Achieving the Dream (AtD) initiative</u> which focused on closing gaps in student achievement through data-driven decision
                                       making (2004). In a collaborative planning process, we developed a “model of innovation”
                                       and identified strategies that were “ripe, effective, and scalable”:&nbsp; <u>Supplemental Learning (SL), Student Success Skills (SLS), and Learning Communities
                                          (LinC)</u>.&nbsp; Faculty and staff were invited&nbsp; to pilot, review data, and make plans for expansion.
                                       Valencia was awarded the <u>first annual Leah Meyer Austin Award</u> for our progress in “closing the gaps” in six “front door” courses (2009). 
                                    </p>
                                    
                                    <p>This led the College Learning Council to adopt the <u>Foundations of Excellence self-study</u> in order to continue the focus on the “front door” (2007-08).&nbsp; The self-study led
                                       us to explore nine foundational dimensions (Philosophy, Organization, Learning, Campus
                                       Culture, Transitions, All Students, Diversity, Roles and Purposes, and Improvement)
                                       to facilitate conversations, study, and recommendations for a coherent first-year
                                       experience. 
                                    </p>
                                    
                                    <p>The <u>Developmental Education Initiative</u> (2009) continues scaling up SL and LinC strategies and also building an accelerated,
                                       fully realigned developmental education program.&nbsp; A fully integrated Developmental
                                       Education Learning Community will lead entering students to complete 21 college level
                                       credits in their first year (2011-12).
                                    </p>
                                    
                                    <p>Together these efforts illustrate a “long obedience in the same direction” towards
                                       improved student completion.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <ol>
                           
                           <li>For your completion data, please briefly describe:</li>
                           
                        </ol>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>How the data are used on your campus, and specifically by whom. </li>
                              
                              <li>What program or other changes you have made in response to these outcomes data, the
                                 scale of the changes (how many and which students are impacted), and plans to implement
                                 and sustain those changes.
                              </li>
                              
                              <li>How you assess the effectiveness of those changes.&nbsp;&nbsp;&nbsp; </li>
                              
                           </ul>
                           
                        </ul>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia’s learning-centered history includes engaging in large scale, data-supported,
                                       institutional efforts aimed at improved student learning, completion and success.&nbsp;
                                       Four such efforts include the annual <em><u>Program Viability, Growth &amp; Success</u> </em>meetings&nbsp; in which faculty, staff and administrators gather on a yearly basis to consider
                                       career and technical programs in light of established targets for program growth and
                                       student success (more on this work will follow in section 2); the development, implementation
                                       and annual review of&nbsp; <em><u>Valencia’s Strategic Plan</u></em>; our efforts to <u>close achievement gaps</u> through <em>the Achieving the Dream</em> and <em>Developmental Education Initiatives; </em>and our work with the <em><u>Foundations of Excellence</u> </em>process.
                                    </p>
                                    
                                    <p><strong><em>The Strategic Plan</em></strong> 
                                    </p>
                                    
                                    <p><a href="../strategic-plan/index.html">Valencia’s Strategic Plan</a> for 2008-2013 has four Goals: 1) Build Pathways, 2) Learning Assured, 3) Invest in
                                       Each Other, and 4) Partner with the Community.&nbsp; Progress on each Goal, and the targeted
                                       Objectives for each, is evaluated on a yearly basis with the help of nearly 200 faculty,
                                       staff and administrators engaged primarily in two activities:&nbsp; 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>The College Planning Council convenes<a href="../strategic-plan/strategicplanning2010-11.html"> Strategic Goal Teams</a>  &nbsp;for each of the four goals in the Strategic Plan. &nbsp;Each goal team monitors progress
                                          toward the goal, collects and interprets data, monitors related strategic issues,
                                          makes recommendations to the annual <a href="../strategic-plan/BIG/index.html">Big Meeting</a>, and prepares an annual report on progress toward the goal<br>
                                          .
                                       </li>
                                       
                                       <li>
                                          <a href="../strategic-plan/BIG/index.html">The Big Meeting</a>  is an important component both of the College’s Institutional Effectiveness effort
                                          and of the annual evaluation of progress toward the goals and objectives in our Strategic
                                          Plan. The meeting provides the opportunity to identify and make recommendations to
                                          the President about any missing pieces of the work to be done to achieve the strategic
                                          goals and objectives, to make recommendations regarding how the work might best be
                                          sequenced and coordinated, and to recommend any changes the group may deem advisable
                                          to the 2008-13 strategic goals and objectives and/or the evaluation plan.&nbsp; <br>
                                          
                                       </li>
                                       
                                       <li>Annual presentations are made to the District Board of Trustees<a href="documents/StrategicPlanGoalReports.pdf.html">Strategic Plan Goal Reports</a> on the progress made on each goal.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><a href="#top">TOP</a></p>
                                    
                                    <p><strong><em>Achieving the Dream </em></strong><strong>and <em>The Development Education Initiative </em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                    </p>
                                    
                                    <p><u><a href="http://achievingthedream.org/about-us">Achieving the Dream (AtD)</a></u>&nbsp;is a multiyear national initiative to help more community college students succeed.
                                       The initiative is particularly concerned about student groups that have traditionally
                                       faced significant barriers to success, including students of color and low-income
                                       students. AtD works on multiple fronts, including efforts at community colleges and
                                       in research, public engagement and public policy. It emphasizes the use of data to
                                       drive change. Building of the work of AtD, the<a href="http://achievingthedream.org/resources/initiatives/developmental-education-initiative"> <u>Developmental Education Initiative (DEI)</u></a> &nbsp;is a national effort to increase the number of students who complete their developmental
                                       education courses successfully and move on to college-level courses.&nbsp; DEI at Valencia
                                       will expand the reach of the AtD strategies to an ever great number of students.
                                    </p>
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>Added Strategy</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Participating Students</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Student Baseline</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Students That Will Be Served</p>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>2009-2010</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>2010-2011</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>2011-2012</p>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>Increase the number of SL leaders in classroom and disciplines</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Developmental Education students in math, reading, and writing</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>5,584 students in 2007-2008</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>8,525 actual</p>
                                                
                                                <p>320 sections</p>
                                                
                                                <p>(6,000 original)</p>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>9,508</p>
                                                
                                                <p>Actual</p>
                                                
                                                <p>410 sections </p>
                                                
                                                <p>(7,000 target)</p>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>8,300 target</p>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>Increase the number of LinC sections (both breadth and depth)</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Developmental Education students in math, reading, and writing</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>722 students in 2007-2008</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>894 actual</p>
                                                
                                                <p>40 sections</p>
                                                
                                                <p>(850 original)</p>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>1,088 </p>
                                                
                                                <p>Actual</p>
                                                
                                                <p>50 sections</p>
                                                
                                                <p>(1,000 target)</p>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>1,300 target</p>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>Expand the number of students in the Bridges Program </p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>Low socioeconomic, at risk graduates of high school</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>250 new students every summer; they are eligible for 3 years</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                
                                                <p>278 actual</p>
                                                
                                                <p>(300</p>
                                                
                                                <p>target for Summer 2010)</p>
                                                
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>350 target</p>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <p>400 target</p>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <p>Central to our work on both initiatives has been the creation of a College Data Team.
                                       The Data Team collects and structures college-wide reflection on student success rates,
                                       persistence from fall to spring and fall to fall, time to degree and time to complete
                                       prep mandates. For both DEI and AtD strategies, we monitor the achievement gaps between
                                       ethnic groups in six target courses: MAT0012C (Pre-Algebra), MAT0024C (Beginning Algebra),
                                       MAT1033C (Intermediate Algebra), ENC1101 (English Composition I), POS2041 (US Government)
                                       and MAC1105 (College Algebra).&nbsp; We also track success rates and achievement gaps in
                                       Developmental Reading I (REA 007C), Developmental Reading II (REA 0017C), Developmental
                                       Writing I (ENC 0015C), and Developmental Writing II (0025C). &nbsp;<a href="documents/DEI2011AnnualReportingValencia.pdf.html">&nbsp;</a><a href="documents/DEI2011AnnualReportingValencia.pdf.html">Development Education Report</a></p>
                                    
                                    <p>In addition to traditional forms of data collection and analysis, we use qualitative
                                       methods to study the impact of our strategies. For example, we conducted a semester
                                       long study of our Student Success course. We documented faculty and student responses
                                       to the content of the course, and then analyzed those responses for specific themes.
                                       Based on these results, changes were made to the curriculum of Student Success and
                                       to an important feature of the course, the Student Success portfolio.&nbsp; 
                                    </p>
                                    
                                    
                                    <p><strong><em>Foundations of Excellence</em></strong></p>
                                    
                                    <p>At the heart of the Foundations of Excellence (FoE) process is the work done by the
                                       nine dimension teams – one team for each of the nine foundational dimensions (Philosophy,
                                       Organization, Learning, Campus Culture, Transitions, All Students, Diversity, Roles
                                       and Purposes, Improvement).&nbsp; Each dimension has a series of performance indicators
                                       on which the dimension team members must evaluate the College using institutional
                                       data, publications, the student FoE survey, the faculty and staff FoE survey, and
                                       focus group data.&nbsp; This data, combined with reflection and discussion allows the dimension
                                       teams to evaluate the College’s performance, assign the College a grade, and make
                                       recommendations for improvement.&nbsp; The result of this process allowed each dimension
                                       team to produce a report.&nbsp;&nbsp; 
                                    </p>
                                    
                                    <p>An analysis of the results by the 180 person taskforce revealed the existence of six
                                       global themes – themes that transcend the conclusions of any one team.&nbsp; The six global
                                       themes include the following: New Students Transitioning Into Valencia, Coordinated
                                       New Student Experience, Learning, Data Collection and Dissemination, Faculty and Professional
                                       Development, and Communication.&nbsp; Work is well underway on the implementation of the
                                       taskforce recommendations -<a href="documents/FoEFinalReport-ApprovedbyCLC5-7-09.pdf.html"> Foundations of Excellence Report.</a></p>
                                    
                                    <p>As result of this work A New Student Experience Committee has been established to
                                       promote the creation of a coordinated New Student Experience.&nbsp; Part of this work has
                                       been focused on the student experience in the<a href="../institutional-research/reporting/strategic-indicators/documents/SIR1213OnlineStudentOverview20130614.pdf"> top 10 highest enrolled courses</a> for new students at Valencia.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Section 2. Labor Market Outcomes </strong></p>
                                    
                                    <p><strong>(no more than 3 pages of written responses total for this section)</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <ol>
                           
                           <li>Please provide data on the earnings and employment outcomes of students who have completed,
                              at your institution, an associate’s degree or certificate of one year or greater length,
                              including:
                           </li>
                           
                           <ol>
                              
                              <li>
                                 <em>Absolute</em> student earnings and employment for those who have completed, at your institution,
                                 an associate’s degree or certificate/program of one year or greater length (i.e.,
                                 percent of graduates employed within one year of completion and average annual/annualized
                                 salaries), and
                              </li>
                              
                              <li>
                                 <em>Improvements</em> in earnings and employment resulting from completing, at your institution, an associate’s
                                 degree or certificate/program of one year or greater length (i.e., increases in earnings
                                 and employment measured from the time prior to enrollment to the time after completion).
                              </li>
                              
                           </ol>
                           
                        </ol>
                        
                        
                        <ol>
                           
                           <li>In 500 or fewer words, describe any data or information you collect regarding the
                              match between regional labor market demand and the degrees/credentials produced by
                              your institution. &nbsp;Cite the source of your information, indicate how frequently the
                              information is collected, and describe how the information is used to improve curricula
                              or practice. Also, specify if students are placed into employment, and whether data
                              on employee performance are collected from employers. 
                           </li>
                           
                        </ol>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The primary source utilized by Valencia to verify the appropriateness of degrees,
                                       certificates, and credentials earned through its academic programs with regard to
                                       the regional labor market demand is the <u>Regional Targeted Occupations List</u>.&nbsp; The List is derived from recommendations made by the Florida Workforce Estimating
                                       Conference as authorized under F.S.§445.011. The conference meets semi-annually to
                                       establish a Statewide Occupational Demand List and provides recommendations to Workforce
                                       Florida, Inc. for establishing Regional Targeted Occupations Lists. Workforce Florida,
                                       Inc. approves the Lists.
                                    </p>
                                    
                                    
                                    <p>The <u>Workforce Estimating Conference</u> develops Florida’s official information set for use by the state planning and budgeting
                                       offices to ascertain the personnel needs of current, new, and emerging industries.&nbsp;
                                       The conference applies quantitative and qualitative research methods to create short-term
                                       and long-term forecasts of employment demand for jobs by occupation and industry,
                                       entry and average wage forecasts among those occupations, and estimates of the supply
                                       of trained and qualified individuals available or potentially available for employment
                                       in those occupations. Emphasis is placed upon occupations and industries which yield
                                       short-term and long-term employment with <u>High-Skills/High-Wage</u> levels.&nbsp; Data is generated through surveys conducted as part of Florida’s internet-based
                                       job matching and labor market information system as well as local surveys conducted
                                       by economic development agencies and others. Based upon its review and analysis of
                                       such survey data, the conference makes recommendations semi-annually to Workforce
                                       Florida, Inc. on additions and deletions to the lists of locally targeted occupations
                                       and industries.&nbsp; This work of the conference results in the <u>annual publication of Regional Targeted Occupations Lists by Workforce Florida, Inc</u>.
                                    </p>
                                    
                                    
                                    <p>Each year <u>Valencia Deans and Faculty complete a comparative analysis of its degrees and certificate
                                          programs and the Regional Targeted Occupations List</u>.&nbsp; Florida’s curriculum frameworks for A.S. and technical certificate programs include
                                       a designation of correlated occupations.&nbsp; For each degree and certificate program,
                                       the college compares the correlated occupations to the occupations identified on the
                                       Regional Targeted Occupations List and assigns an institutionally derived indicator
                                       for each program’s level of compatibility with regional workforce demands. Programs
                                       that have a correlated occupation appearing on the Regional Targeted Occupations List
                                       with a High Skill/High Wage indicator are given the highest rating.&nbsp; Programs that
                                       do not have a correlated occupation appearing on the Regional Targeted Occupations
                                       List are reviewed more deeply to project short-term and long-term program viability.
                                    </p>
                                    
                                    
                                    <p><u>Valencia Deans and Faculty review the viability of A.S. and technical certificate
                                          programs annually as part of its institutional model for continuous improvement</u>.&nbsp; The process includes a detailed review of student learning outcomes, curriculum,
                                       and program performance measures such as completion, placement, and transfer to the
                                       baccalaureate.&nbsp; Data obtained from Florida Education and Training Placement Information
                                       Program is used to gauge the level employment of students who complete certificates
                                       and degrees.&nbsp; Based upon performance, each program completes an action plan that guides
                                       practice and curriculum for the purpose of improving the student learning experience.
                                    </p>
                                    
                                    
                                    <p>Valencia Workforce Services provides online job listings but not formal placement
                                       services or formal collection of employee performance data.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <ol>
                           
                           <li>In 500 or fewer words, please describe:</li>
                           
                        </ol>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>How the labor market data discussed in response to questions 1 and 3 above are used
                                 on your campus (to inform programming, instruction, budgeting, planning, etc.), and
                                 specifically by whom.
                              </li>
                              
                              <li>What program or other changes you have made in response to these outcome data.</li>
                              
                              <li>How you assess the effectiveness of those changes and how you plan to scale and sustain
                                 any changes.
                              </li>
                              
                              <li>Whether there is anything particular about the labor market in your region that might
                                 inform or contextualize reviewers’ understanding of the employment and earnings outcomes
                                 you provide.&nbsp;&nbsp; 
                              </li>
                              
                           </ul>
                           
                        </ul>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia utilizes the <u>Regional Targeted Occupations List</u> and other labor market data sources to evaluate program performance and inform models
                                       of enhancement.&nbsp; <u>Labor Market Statistics (LMS), </u>from the State of Florida Agency for Workforce Innovation, and the<u> Analyst</u>, provided by Economic Modeling Specialists, Inc. (EMSI), are additional data sources
                                       regularly used in such analysis.&nbsp; The <u>Labor Market Statistics</u> includes a collection of economic, demographic, and labor market data related to
                                       employment and wages, economic indicators, labor force, and population. The <u>Analyst</u> is a web-based tool that comprises local employment data, compatibility analyses
                                       for local training programs, and re-employment scenarios.&nbsp; Annually, Valencia examines
                                       and compares this collect of data sources to inform our work in determining program
                                       viability and modes for continuous program improvement. 
                                    </p>
                                    
                                    
                                    <p>Valencia holds a <u>Program Viability and Success Forum</u> annually as well as specialized periodic reviews of all A.S. degrees and certificates
                                       based upon the <a href="http://www.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/baccalaureate/Engineering/documents/2.5%20Five-Year%20Program%20Review%20Schedule.pdf">Five-Year Program Review Schedule</a>.&nbsp; Such efforts<strong> </strong>examine and determine program effectiveness, efficiency, and methods of improvement.&nbsp;
                                       Evaluation teams include internal staff such as senior administrators, deans, program
                                       directors, faculty, and advisors as well as external representatives with background
                                       and expertise in the respective curricular areas.&nbsp; Review criteria include faculty
                                       profiles, full-time to part-time ratios, enrollment data, student yield, program stability,
                                       placement rates, scheduling alternatives, advising, articulation agreements, learning
                                       resources support, Advisory Committee efforts and program learning outcomes. Specific
                                       recommendations for improvement are linked to a <u>Plan of Action</u>.&nbsp; 
                                    </p>
                                    
                                    
                                    <p>The <u>Plan of Action</u><u> </u>is reviewed periodically until the recommendations for improvement have been implemented
                                       and evaluated. Comprehensive documentation of this process is provided through an
                                       annually published handbook titled <a href="../sacs-regional-accreditation/documents/The%20State%20of%20Workforce%20Education%20at%20Valencia.pdf.html">The State of Workforce Education at Valencia</a>, and <a href="http://www.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/baccalaureate/Engineering/documents/2.5%20Program%20Review%20Reports.pdf">Program Review Reports</a> are published every five years. Examples demonstrate systemic programmatic review
                                       for the purpose of continuous improvement in accomplishing the institutional mission:&nbsp;
                                       <a href="http://www.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/baccalaureate/Engineering/documents/1%20Part%20A%20Summary%20of%202009-10%20Program%20Success%20Strategies.pdf">Summary of 2009-10 Program Success Strategies</a>; five-year A.S. program review (<a href="http://www.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/baccalaureate/Engineering/documents/2.5%20Business%20Administration%20Program%20Review.pdf">Business Administration</a>); selected A.S. program performance dashboards (<a href="http://www.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/baccalaureate/Engineering/documents/2.5%20Graphics%20Technology%20Program%20Review.pdf">Graphics Technology</a>).
                                    </p>
                                    
                                    
                                    <p>The cycle provides for planning, assessment, and evaluation for continuous improvement
                                       of programs and addresses state, national and professional accountability measures,
                                       as well as regional accreditation core requirements and comprehensive standards based
                                       on the <em>Principles of Accreditation, Foundations for Quality Enhancement</em>, Commission on Colleges, Southern Association of Colleges and Schools (SACS). Each
                                       plan is approved by the Campus President and obtains additional feedback through the
                                       college governance process.&nbsp; Recommended changes are prioritized for funding through
                                       the budget planning process to ensure appropriate scale and sustainability.&nbsp; Outcomes
                                       from these systemic program reviews have yielded changes in program content and sequencing,
                                       staffing levels, faculty development, and learning support services.&nbsp; The effectiveness
                                       of programmatic changes are assessed and evaluated during the next Program Viability
                                       and Success Forum as well as the five-year program review. Successful changes are
                                       documented for best practices.&nbsp; 
                                    </p>
                                    
                                    
                                    <p>While Central Florida is best known for its tourism industry, it is also the center
                                       of the simulation industry; a world leader in the science of photonics and the field
                                       of financial software; and an emerging hub for digital media technologies and bioscience
                                       research. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Section 3. Learning Outcomes </strong></p>
                                    
                                    <p><strong>(no more than 3 pages of written responses total for this section)</strong><strong> </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <ol>
                           
                           <li>Please provide any data you collect regarding how student learning is measured in
                              college-level courses, vocational certificate programs, or in general education courses,
                              indicating the source of data (i.e., assessment tests, licensure exams, surveys),
                              including:
                           </li>
                           
                           <ol>
                              
                              <li>
                                 <em>Absolute</em> student learning outcomes (i.e., number and percent of students passing licensure
                                 or other standardized tests), and
                              </li>
                              
                              <li>
                                 <em>Improvements</em> in learning while students are enrolled (i.e., pre/post tests or repeated assessments).
                              </li>
                              
                           </ol>
                           
                        </ol>
                        
                        
                        <ol>
                           
                           <li>In 500 or fewer words, describe:</li>
                           
                        </ol>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>How frequently the assessments are conducted, what percentage of students are included
                                 in the assessment(s), and if the data apply only to a specific population (transfer
                                 track, developmental students, degree-seeking only, etc.).
                              </li>
                              
                              <li>Your assessment of the reliability of the assessments used to measure learning outcomes
                                 (include description of the assessment(s) and the actual instrument if possible).
                              </li>
                              
                           </ul>
                           
                        </ul>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia learning-centered work includes strong <u>faculty development in support of the assessment of learning in the classroom</u> (<a href="../../../faculty/development/coursesResources/tla_lifemap_resources_counseling_lcf.html">Outcomes Based Practice</a>).&nbsp; For some years, assessment efforts focused on the shared (interdisciplinary) contribution
                                       to the 4 <u>Competencies of Valencia Graduate identified by faculty (Think, Value, Communicate
                                          and Act)</u>.&nbsp; While these competencies had broad appeal and are now deeply rooted in the institutional
                                       culture, we discovered their broad applicability created difficulties for measurability/assess-ability.
                                       Since 2007, we have engaged in a culture shift in our assessment efforts by working
                                       to develop and approve <u>Program Learning Outcomes</u> for each program of study.&nbsp; Formally published in our College Catalog as a promise
                                       to our students, the outcomes serve as a starting point for ensuring the alignment
                                       of all courses and co-curricular activities with program outcomes.
                                    </p>
                                    
                                    
                                    <p><u>Plans for the assessment of program outcomes are designed, approved, and implemented
                                          by faculty</u> with the support of Valencia’s College Learning Council, the Learning Assessment
                                       Committee, the Offices of Institutional Assessment, Institutional Research and Faculty
                                       Development. The development of yearly assessment plans is guided by a common<a href="documents/ProgramLearningOutcomesAssessmentPlanTemplate.pdf.html"> <u>Program Learning Outcomes Assessment Template</u></a>  and an <u><a href="documents/ProgramOutcomeAssessmentPlan-ProcessFlowChart.pdf.html">approval process flow-chart</a></u> <a href="documents/ProgramOutcomeAssessmentPlan-ProcessFlowChart.pdf.html">t</a>o ensure broad –based program faculty involvement in the process.&nbsp; The assessment
                                       plans rely heavily on embedded assessments and the collection of student artifacts
                                       at points in a student’s progress toward the completion of a degree or certificate.&nbsp;
                                       The nature of embedded assessment, especially in larger programs like General Education,
                                       require sustained faculty engagement with each other to balance program-wide consistency
                                       for assessment purposes with academic freedom in individual classrooms. <u>Student artifacts are collected based on random sampling of students within the targeted
                                          courses</u> – the samples are stratified based on Campus, mode of instructional delivery, and
                                       the contract status of the instructor. &nbsp;Institutionally, we consider assessment activities
                                       at the program level as primarily a tool to promote faculty dialog about the learning
                                       goals they have for students.&nbsp; To date, the majority of program level assessment activities
                                       have yielded agreements concerning common expectations (rubrics, etc.) and improvements
                                       to the assessment process. &nbsp;The “validity and reliability” of the assessment results
                                       is reflected in the ability of our faculty to build consensus about collective action
                                       to improve student learning. We are one or two cycles away from meaningful / actionable
                                       evidence of student learning at the program level in the majority of programs.
                                    </p>
                                    
                                    
                                    <p>Faculty efforts in this area are recognized by our District Board of Trustees of a
                                       <u>Faculty Compensation Plan</u> that includes an institutional effectiveness component:
                                    </p>
                                    
                                    
                                    <p><em>By May 13, 2012,<a href="documents/AcademicPrograms.pdf.html"> all academic programs</a> will have implemented an assessment plan that has been approved by the Learning Assessment
                                          Committee (LAC)….In order for the faculty, collectively, to be eligible to receive
                                          the Institutional Effectiveness (IE) component that is in addition to their normal
                                          salaries in Fall 2012, 90% of all of the academic programs must have developed faculty
                                          approved improvement plans.&nbsp; These improvement plans will be based on the learning
                                          assessment data compiled from each academic program’s assessment plan.</em></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>3.&nbsp;&nbsp;&nbsp; In 500 or fewer words, describe</p>
                        
                        <ul>
                           
                           <ul>
                              
                              <li>How the learning outcomes data you provide are used on your campus (to inform programming,
                                 instruction, budgeting, planning, etc.), and specifically by whom.
                              </li>
                              
                              <li>What programmatic or other changes you have made in response to these data.</li>
                              
                              <li>How you assess the effectiveness of those changes, and how you plan to scale and sustain
                                 those changes. 
                              </li>
                              
                           </ul>
                           
                        </ul>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><u>Learning Outcomes Assessment data is used by program faculty to improve learning and
                                          curricular alignment.</u> An example is from a report recently produced by <u>English Faculty after assessing Information Literacy in English Composition</u> paper on Assessment Day 2011 (Papers were randomly selected from all sections of
                                       ENC1101).&nbsp; <u>Assessment Day</u> is an annual event for faculty to gather with their colleagues across campuses to
                                       assess student artifacts and discuss ways of improving either the assessment process
                                       or student learning. 
                                    </p>
                                    
                                    <p>&nbsp;<em>As a result of the findings from the assessment of student artifacts, the thirty-three
                                          faculty participants voted to work on improving instruction of the Program Learning
                                          Outcomes for General Education under the Information Literacy heading. Specifically,
                                          faculty agreed to address two objectives during the upcoming year: To teach students
                                          to properly integrate source materials into an essay and to document sources within
                                          the essay. An official vote to approve the upcoming 2011-2012 improvement plan will
                                          be sent to the eligible ENC1101 voter list to obtain consensus among at least 2/3
                                          of the English faculty. </em></p>
                                    
                                    <p><em>The improvement plan will include having each campus English Coordinator work with
                                          their respective faculty to develop a plan to address the two objectives and a campus-based
                                          improvement plan to increase students’ ability to properly integrate source materials
                                          into an essay and document sources within the essay. The four campus coordinators
                                          will work together toward an overall improvement plan for the college’s English department.
                                          During Assessment Day 2012, the English faculty will conduct the same assessment work
                                          as completed during the May 6, 2011 meeting and compare the results of the 2012 student
                                          artifacts assessment data with that of the 2011 data to determine if any improvements
                                          to the student work have been demonstrated.</em> 
                                    </p>
                                    
                                    <p>Work on the articulation and assessment of Program Learning Outcomes has moved Valencia
                                       to create <u>much stronger structural alignment</u> between the Offices of Institutional Assessment, Institutional Research, Faculty
                                       Development, and the Teaching / Learning Academy – all of these departments now report
                                       to the AVP for Assessment &amp; Institutional Effectiveness within Academic Affairs. &nbsp;Valencia’s
                                       experience with faculty development and the assessment of learning outcomes at the
                                       classroom, course and program levels has situated us as leader within the State College
                                       System in the State of Florida.&nbsp; For three years in a row <u>Valencia has sponsored a State Assessment Meeting </u>for the 28 Colleges in the State System – in June 2011 160 Faculty, Program Chairs
                                       and Administrators from 22 of the 28 State Colleges were in attendance.&nbsp; The center
                                       piece of the meeting for the past three years has been an intensive<a href="documents/StateAssMtgPLOs.pdf.html"> 5 hour workshop</a> &nbsp;on the articulation and assessment of program learning outcomes.
                                    </p>
                                    
                                    <p>Program wide assessment activities have also revealed <u>challenges with helping adjunct faculty to participate in the process</u>.&nbsp; As a result of these concerns, a college-wide professional staff member has been
                                       hired in Faculty Development to focus on the needs and engagement of Valencia adjunct
                                       faculty.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <p><strong>Section 4. Closing Statement </strong></p>
                        
                        <p>In one page or less, please provide a statement explaining why your community college
                           has achieved excellent student outcomes, is positioned to continue improving such
                           outcomes in the future, and should win the Aspen Prize for Community College Excellence.
                        </p>
                        
                        <p>Three deep habits in Valencia’s culture both explain the improvement results thus
                           far and position us for continued progression and student success. First, an important
                           part of the sustained efforts toward improving student learning at Valencia has been
                           the crystallizing of several key ideas that serve as fulcrums for change, signifiers
                           for emerging organizational culture, and rallying points for action. The process of
                           moving from promising innovation to large-scale pilot, to sustained solution, that
                           is, the process of institutionalizing the work, depends heavily on a <u>community of practice shaped by powerful common ideas</u>.&nbsp; While these ideas aren’t unique to Valencia, they are authentically ours in the
                           sense that they are organic to our work, having rooted themselves in the discourse
                           of campus conversations, planning, development, and day to day activity. For example,
                           “<u>Anyone can learn anything under the right conditions</u>.”&nbsp; This idea marks a change in belief about our students.&nbsp; Most of the culture of
                           education is built on a long-standing myth that talent for learning is relatively
                           scarce and that many, perhaps the majority of our population, just aren’t “college
                           material.”&nbsp;&nbsp; This idea shifts the focus from the deficiencies of the learner to the
                           conditions of learning.&nbsp; Our task as a college is to partner with the learner, who
                           controls many but not all of these conditions, to create the very best conditions
                           for her to succeed. More on <a href="documents/BigIdeaspdfversion.pdf.html">“</a><a href="documents/BigIdeaspdfversion.pdf.html">Valencia’s Big Ideas</a><a href="documents/BigIdeaspdfversion.pdf.html">”</a>. &nbsp;
                        </p>
                        
                        <p>Second, the creation of <u>authentic and meaningful Strategic Plans</u> (<a href="../../../lci/essays/plan.pdf"> Strategic Learning Plan 2001-2004</a>) (<a href="../strategic-plan/documents/StratPlanBOOKMARKED_000.pdf">Strategic Plan 2008-1013</a>) has named the most important areas of focus and outcomes. The Goals are intentionally
                           short expressions that capture the spirit and the possibility for participation by
                           faculty and staff across the college. “<u>Start Right</u>” became short hand for a host of program and procedural changes that improved students’
                           experience at the front door.&nbsp; “<u>Learning First</u>” was a galvanizing expression of priorities to put into practice the questions we
                           adopted as a learning-centered institution – How will this improve learning? How will
                           we know?&nbsp; Currently, “<u>Build Pathways</u>” is igniting the evolution of our thinking that “access” and “student success” are
                           insufficient concepts and that true pathways&nbsp; require a&nbsp; more comprehensive and personally
                           compelling vision to propel students “to, through and beyond” Valencia.&nbsp; “<u>Learning Assured</u>” specifies a commitment to learning beyond “hope” and specifies in its objectives
                           where we want to ensure that student learning takes place. Our budget development
                           process is intentionally designed to align with our strategic priorities, and we present
                           an annual report to our Board of Trustees that specifically names how the recommended
                           budget is an expression of our plans and priorities.&nbsp; 
                        </p>
                        
                        <p>Finally, Valencia has developed “<u>tools of collaboration</u>” through which we continuously engage students, faculty and staff from across the
                           college in our learning-centered work.&nbsp; One such tool we simply refer to as “ <u>Big Meeting</u>”, in which we engage 125-175 people in a full day conversation to share evidence,
                           reflect on its meaning and set priorities for the next phase of our work. The “<u>innovation funnel</u>” is a model that describes the progression of new ideas from initial trial to broader
                           student involvement and finally to full scale adoption, with evidence to support the
                           progression and investment in those ideas that are most promising in terms of improvement
                           for students.&nbsp; New systems and solutions begin with a collaborative process to specify
                           the <u>design principles</u> for the solution, thus ensuring that the final product reflects the values and outcomes
                           we seek. We practice a deliberate <u><a href="../../../governance/index.html">shared governance system</a></u> 
                        </p>
                        
                        <p>The Aspen Prize would be an honor for Valencia to recognize our enduring commitment
                           to student learning and success, the progressive improvement we have demonstrated
                           thus far and our impatience to do better for and with our students.&nbsp; As a large, multi-campus,
                           urban community college, we have all of the challenges that face similar colleges:&nbsp;
                           we are under resourced in funding and space, the majority of our students arrive unprepared
                           for college level work, we have a large organization dispersed geographically and
                           a majority of part-time faculty and staff – yet we have demonstrated that with thoughtful
                           leadership, planning, engagement across the college and continuous commitment to improvement,
                           we can “move the needle” for students goal achievement. The specifics of the next
                           phases of our work are in our <a href="../strategic-plan/index.html">Strategic Plan</a>.The Aspen Prize would signal an important investment in our journey and would enable
                           us to reach the next level of student achievement. 
                        </p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3>Valencia Data Templates </h3>
                        
                        <p><a href="ValenciaDataTemplates-AspenRound2Application-Final.xlsx">Valencia Data Templates - Aspen Round 2 Application - Final</a></p>
                        
                        
                        <h3>Resource Documents </h3>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Cardiovascular_Technology.pdf.html">PGVS_2010_Scorecard_Cardiovascular_Technology</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Radiography.pdf.html">PGVS_2010_Scorecard_Radiography</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Nursing_RN.pdf.html">PGVS_2010_Summary_Health_Nursing_RN</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Nursing_RN.pdf.html">PGVS_2010_Scorecard_Nursing_RN</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Emergency_Medical_Services_Technology.pdf.html">PGVS_2010_Summary_Emergency_Medical_Services_Technology</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Diagnostic_Medical_Sonography.pdf.html">PGVS_2010_Summary_Health_Diagnostic_Medical_Sonography</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Dental_Hygiene.pdf.html">PGVS_2010_Summary_Health_Dental_Hygiene</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Diagnostic_Medical_Sonography.pdf.html">PGVS_2010_Scorecard_Diagnostic_Medical_Sonography</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Respiratory_Care.pdf.html">PGVS_2010_Scorecard_Respiratory_Care</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Law_Enforcement_Officer.pdf.html">PGVS_2010_Scorecard_Law_Enforcement_Officer</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Correctional_Officer.pdf.html">PGVS_2010_Scorecard_Correctional_Officer</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_RespiratoryCare.pdf.html">PGVS_2010_Summary_Health_Respiratory Care</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Radiography.pdf.html">PGVS_2010_Summary_Health_Radiography</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Emergency_Medical_Services_Technology.pdf.html">PGVS_2010_Summary_Health_Emergency_Medical_Services_Technology</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Dental_Hygiene.pdf.html">PGVS_2010_Summary_Health_Dental_Hygiene</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Dental_Hygiene.pdf.html">PGVS_2010_Scorecard_Dental_Hygiene</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Emergency_Medical_Services_Technology.pdf.html">PGVS_2010_Summary_Health_Emergency_Medical_Services_Technology</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_CJI_Correctional_Officer.pdf.html">PGVS_2010_Summary_CJI_Correctional_Officer</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_CJI_Law_Enforement_Officer.pdf.html">PGVS_2010_Summary_CJI_Law_Enforement_Officer</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Educator_Preparation_Institute.pdf.html">PGVS_2010_Summary_Educator_Preparation_Institute</a></p>
                        
                        <p><a href="documents/PGVS_2010_Scorecard_Educator_Preparation_Institute.pdf.html">PGVS_2010_Scorecard_Educator_Preparation_Institute</a></p>
                        
                        <p><a href="documents/ResultsandImprovementPlan_CompI.doc.html">Results and Improvement Plan_Comp I</a></p>
                        
                        <p><a href="documents/Communicaterubrics.pdf.html">Communicate rubrics</a></p>
                        
                        <p><a href="documents/HUMRubric.pdf.html">HUMRubric</a></p>
                        
                        <p><a href="documents/RubricforWrittenCommunication.pdf.html">Rubric for Written Communication</a></p>
                        
                        <p><a href="AssessingScientificReasoning.pptx">Assessing Scientific Reasoning</a></p>
                        
                        <p><a href="documents/ParalegalStudiesCapstonePortfolioAssignment1.doc.html">Paralegal Studies Capstone Portfolio Assignment1</a></p>
                        
                        <p><a href="documents/Form_Letter_Rubric.pdf.html">Form_Letter_Rubric</a></p>
                        
                        <p><a href="documents/Librarians.pdf.html">Librarians</a></p>
                        
                        <p><a href="HUMAssessment.rtf">HUMAssessment</a></p>
                        
                        <p><a href="documents/PGVS_2010_Summary_Health_Cardiovascular_Technology.pdf.html">PGVS_2010_Summary_Health_Cardiovascular_Technology</a></p>
                        
                        <p><a href="ProgramLearningOutcomesProgramAssessmentCapstone-DentalHygiene.docx">Program Learning Outcomes Program Assessment Capstone - Dental Hygiene</a></p>
                        
                        <p><a href="documents/Evaluation-ModelBuildingconstruction.pdf.html">Evaluation-Model Building construction</a></p>
                        
                        <p><a href="documents/politicalscience-InformationLiteracy.pdf.html">political science - Information Literacy</a></p>
                        
                        <p><a href="HistoryWritingPrompt-Checklist.docx">History Writing Prompt - Checklist</a></p>
                        
                        <p><a href="ITCapstoneProjectRubric.docx">IT Capstone Project Rubric</a></p>
                        
                        <p><a href="documents/TheaterTechRubrics.pdf.html">Theater Tech Rubrics</a></p>
                        
                        <p><a href="documents/AssessingScientificReasoning.pdf.html">Assessing Scientific Reasoning</a></p>
                        
                        <p><a href="documents/ResultsandImprovementPlan_CompI.pdf.html">Results and Improvement Plan_Comp I</a></p>
                        
                        <p><a href="documents/ProgramLearningOutcomesProgramAssessmentCapstone-DentalHygiene.pdf.html">Program Learning Outcomes Program Assessment Capstone - Dental Hygiene</a></p>
                        
                        <p><a href="documents/HistoryWritingPrompt-Checklist.pdf.html">History Writing Prompt - Checklist</a></p>
                        
                        <p><a href="documents/IT.pdf.html">IT</a></p>
                        
                        <p><a href="documents/ITCapstoneGradeSummary.pdf.html">ITCapstoneGradeSummary</a></p>
                        
                        <p><a href="documents/ParalegalStudiesCapstonePortfolioAssignment.pdf.html">Paralegal Studies Capstone Portfolio Assignment</a></p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/AspenPrizeApplication.pcf">©</a>
      </div>
   </body>
</html>