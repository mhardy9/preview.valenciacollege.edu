<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/fg/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Fg</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2><em>Focus Groups</em></h2>
                        
                        <p>Download the focus group protocol here<br>
                           <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/focus-group-protocol.pdf" target="_blank">Focus Group Protocol</a><br>
                           <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/focus-group-protocol.docx" target="_blank">Focus Group Protocol</a><br><br>
                           <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/5-steps-for-campus-based-focus-groups.pdf" target="_blank">(5 steps for campus based focus groups)</a></p>
                        
                        <hr>
                        
                        <p>Valencia College has a team of faculty and staff members who have been professionally
                           trained to facilitate focus groups.&nbsp;We are able to develop focus group questions with
                           you, identifying the sample, running the sessions, and reporting out the results.
                           
                        </p>
                        
                        <p> History of and best practices for focus groups: Download the presentation from the
                           Southeastern Evaluations Association webinar “<a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/CapturingStudentVoicesWebinarLBlasiNJacksonSEA4.9.2015.pdf" target="_blank">Capturing Student Voices, Teaching Them to Listen: Effective Strategies for Conducting
                              Focus Groups on Campus</a>". 
                        </p>
                        
                        <p>Please email the Institutional Assessment Office if you need help developing your
                           research questions, focus group procedures, or analyzing your results: <a href="mailto:participate@valenciacollege.edu">participate@valenciacollege.edu</a></p>        
                        
                        <hr>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Focus Group Guide </div>
                                 
                                 <div data-old-tag="th">Final Report </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/StudentFacilitatedConversations-FacilitatorGuideNJackson2.20.2015.pdf" target="_blank">New Student Experience Focus Group Guide- Spring 2015 </a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/FocusGroupGuideLeadershipAcademy6-1-2012BlasiandTorresFinal.pdf" target="_blank">Leadership Academy Focus Group Guide </a></div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/FocusGroupGuideLeadershipAcademy6-1-2012Report-finalv4.pdf" target="_blank">Leadership Academy Report </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/FocusGroupGuideREACH7-10-2012ShephardandBlasiv1.pdf" target="_blank">REACH Focus Group Guide </a></div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/REACHFocusGroupFINDINGSHandout7-23-2012.pdf" target="_blank">REACH Report </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/StudentSuccessandthe3PrepMandate.pdf" target="_blank">Student Success and the 3-Prep Mandate Report </a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/LifeMapStudentHandbookFocusGroupGuide.pdf" target="_blank">LifeMap Student Handbook Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/TitleV-TransferandAdvisingFocusGroupGuide.pdf" target="_blank">Title V – Transfer and Advising Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/SupplementalLearningFocusGroupGuide.pdf" target="_blank">Supplemental Learning Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/CommunityInclusionCampaignFocusGroupGuide.pdf" target="_blank">Community Inclusion Campaign Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/StudentDecisionMakingFocusGroupGuide.pdf" target="_blank">Student Decision-Making Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/CareerServicesFocusGroupGuide.pdf" target="_blank">Career Services Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/NewStudentExpereinceFocusGroupGuide.pdf" target="_blank">New Student Experience Focus Group Guide</a></div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>        
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/FG/documents/TrainingOutlineandScenariosNJ92716.pdf" target="_blank">Training Materials 9/27/16</a></p>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        
                        
                        <a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"><img alt="Glossary" border="0" height="60" src="glossary.jpg" title="Click to View Glossary" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/fg/index.pcf">©</a>
      </div>
   </body>
</html>