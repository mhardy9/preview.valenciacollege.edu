<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/programs-electronic-presentations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/">2016</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <span>
                           <h2>State Assessment Meeting<br>
                              <em>June 16 &amp; 17, 2016</em><br>
                              Orlando, FL
                           </h2>
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <h3><em><strong>Please <a href="documents/SAM2016Program.pdf" target="_blank">click here</a> to download the Program Schedule </strong></em></h3>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <h6><u>Presentations </u></h6>
                                          
                                          <h3>
                                             <em>Within each session below select the <strong><img alt="Handouts Icon" border="0" height="40" src="Handouts.png" width="40"></strong>icon to download session handouts.</em><br>
                                             
                                          </h3>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>  
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Day One: Thursday, June 16, 2016 </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Session A - Building 8, Special Events Center </strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Session B - Building 11, Room 115 </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Welcome</strong></p>
                                             
                                             <p>Dr. Laura Blasi</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Opening Keynote</strong></p>
                                             
                                             <p><strong>Elevating and Understanding Ethical Reasoning Skills through Undergraduate Research
                                                   Activities: </strong></p>
                                             
                                             <p><strong>What We are Learning When We Assess</strong></p>
                                             
                                             <p>Dr. Julio Turrens</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Intensive Workshop on Developing and Assessing Program Learning Outcomes</strong></p>
                                             
                                             <p>Wendi Dew and Dr. Laura Blasi</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Next Steps from Florida's Participation in the Pilot of the Threshold Achievement
                                                   Test for Information Literacy (TATIL)</strong></p>
                                             
                                             <p><strong> Aligned with ACRL Frameworks</strong></p>
                                             
                                             <p>Diane Dalrymple and Carolyn Radcliff</p>
                                             
                                             <p>Valencia College and Carrick Enterprises</p>
                                             
                                             <blockquote>
                                                
                                                <blockquote>
                                                   
                                                   <blockquote>
                                                      
                                                      <em><strong><em>
                                                               </em></strong></em>
                                                      
                                                   </blockquote>
                                                   <em><strong><em>
                                                            </em></strong></em>
                                                   
                                                </blockquote>
                                                <em><strong><em>
                                                         </em></strong></em>
                                                
                                             </blockquote>
                                             <em><strong><em>
                                                      </em></strong></em>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Using AAC&amp;U VALUE Rubrics to Enhance the General Education Assessment Model at St.
                                                   Petersburg College</strong></p>
                                             
                                             <p>Maggie Tymms, David Monroe, and Dr. Erika Johnson-Lewis</p>
                                             
                                             <p>St. Petersburg College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             <p><strong>Using the Case Study Method to Teach and Assess Ethical Reasoning Skills in the Context
                                                   of Undergraduate Research Activities</strong></p>
                                             
                                             <p>Dr. Julio Turrens and Dr. Laura Blasi</p>
                                             
                                             <p>The Council of Undergraduate Research (CUR) and Valencia College</p>
                                             
                                             
                                             
                                          </div>                    
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>The Gen Ed Assessment Journey: Where Pedagogy Meets Accountability</strong></p>
                                             
                                             <p>Susan Taylor and Jennifer Page</p>
                                             
                                             <p>North Florida Community College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Innovations in the Assessment of Learning Specific to Communications: Oral Citations
                                                   Rubric and a Pre-Post Assessment</strong></p>
                                             
                                             <p>April Raneri, Tina Tan, and Liza Schellpfeffer</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Assessing Student Success Initiatives: Hand-on Steps for Using Video Case Studies</strong></p>
                                             
                                             <p>Nichole Jackson and Jenny Lee</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             <blockquote>
                                                
                                                <blockquote>
                                                   
                                                   <blockquote>
                                                      
                                                      
                                                   </blockquote>
                                                   
                                                </blockquote>
                                                
                                             </blockquote>
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>The New Advisor Experience Training Program: Leveraging Collaborative Effort</strong></p>
                                             
                                             <p>Ed Holmes and Evelyn Lora-Santos</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Day Two: Friday, June 17, 2016 </strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Session A - Building 8, Special Events Center </strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Session B - Building 11, Room 115 </strong></div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div><strong>Session C - Building 11, Room 129</strong></div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Developing an Online Course Evaluation Model that Results in Response Rates Above
                                                   50%</strong></p>
                                             
                                             <p>Joe Boyd, Jamie Ferrazano, and Maggie Tymms</p>
                                             
                                             <p>St. Petersburg College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>To MARS and Beyond - Opening the Gateway! / Math at the Root of Success - Indian River
                                                   State College's QEP</strong></p>
                                             
                                             <p>Dr. Bobbi Parrino Cook</p>
                                             
                                             <p>Indian River State College</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Using a Peer Review Process to Ensure Quality in Online Learning at Daytona State
                                                   College</strong></p>
                                             
                                             <p>Dr. Karla Moore and Dr. Andrea Gibson</p>
                                             
                                             <p>Daytona State College</p>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             <p><strong>The "Read to Succeed" QEP: Using Data for Continuous Improvement at Seminole State
                                                   College</strong></p>
                                             
                                             <p>Carissa Baker</p>
                                             
                                             <p>Seminole State College</p>
                                             
                                             
                                          </div>                    
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             <p><strong>Applying Behavioral Insights to Improve Student Outcomes: An Iterative Approach to
                                                   Learning</strong></p>
                                             
                                             <p>Josh Martin and Dani Grodsky</p>
                                             
                                             <p>ideas42</p>
                                             
                                          </div>                    
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             <p><strong>Strategies for Applying Behavioral Economics to Assess and Strengthen Student Skills</strong></p>
                                             
                                             <p>Josh Martin and Dani Grodsky</p>
                                             
                                             <p>ideas42</p>
                                             
                                             
                                             
                                          </div>                    
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong>Revisions to Developmental Education in English: Learning from Outcomes Assessment</strong></p>
                                          
                                          <p>Elizabeth Barnes and Dana Davidson</p>
                                          
                                          <p>Daytona State College</p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Show, Don't Tell: Ways to Work with Data in Tableau</strong></p>
                                             
                                             <p>Derek Noce and Colleagues</p>
                                             
                                             <p>DataBrains, Inc.</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>Assessing Student Success Initiatives: Hand-on Steps for Using Video Case Studies</strong></p>
                                             
                                             <p>Nichole Jackson and Jenny Lee</p>
                                             
                                             <p>Valencia College</p>
                                             
                                             <blockquote>
                                                
                                                <blockquote>
                                                   
                                                   
                                                </blockquote>
                                                
                                             </blockquote>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             <p><strong>"Watching Assessment" - A Glance Backward while Looking Towards Tomorrow in a Closing
                                                   Conversation with Pat Hutchings</strong></p>
                                             
                                             <p>Dr. Pat Hutchings</p>
                                             
                                             <p>National Institute for Learning Outcomes Assessment (NILOA)</p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                           <p><a href="#top"> TOP</a></p></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/programs-electronic-presentations.pcf">©</a>
      </div>
   </body>
</html>