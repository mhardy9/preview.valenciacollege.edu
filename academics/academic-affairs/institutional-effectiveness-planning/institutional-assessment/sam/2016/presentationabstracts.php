<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/presentationabstracts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/">2016</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em>June 16 &amp; 17, 2016</em><br>
                           Orlando, FL
                        </h2>
                        
                        
                        <h3><em><strong> </strong></em></h3>
                        
                        <p>Final presentations will be posted after the meeting. </p>
                        
                        <p>Click <a href="../index.html">here</a> for details about this gathering. 
                        </p>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>Presentations Abstracts<br>
                                          
                                       </h3>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Applying Behavioral Insights to Improve Student Outcomes: An Iterative Approach to
                                          Learning</strong></p>
                                    
                                    <p>Josh Martin, Vice President; and Dani Grodsky, Associate, ideas42 </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>What explains student behavior, and what are the implications for schools seeking
                                       to assess how well they’re doing at shaping it? Students enroll in college with intention
                                       to graduate and do so with manageable or no debt. Yet their actions are often inconsistent
                                       with this goal, for example, selecting sub-optimal course loads and loan packages
                                       or neglecting other support structures. The behavioral science lens helps to explain
                                       these inconsistencies, assess the influence of current structures and suggest design
                                       solutions, ranging from small tweaks to entirely new systems. In this talk, we will
                                       uncover the interdependence of behavioral science and rigorous evaluation methods
                                       and explore the ideas42 methodology through real-world applications in postsecondary
                                       education.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Assessing Student Success Initiatives: Hand-on Steps for Using Video Case Studies</strong></p>
                                    
                                    <p>Nichole Jackson, Assistant Director, Learning Assessment; and Jenny Lee, Professor,
                                       New Student Experience, Valencia College 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This session provides attendees with the opportunity to see why video data is an efficient
                                       and effective tool that may be missing from their current assessments. Presenters
                                       will outline the steps for using video data to capture student perspectives.&nbsp; Participants
                                       will be guided through this process and begin working on their own outline for how
                                       to use video data and how to apply new collaborative review strategies at their institution.
                                       In this interactive session, attendees will watch brief video clips of student interviews
                                       and assess them from different perspectives. In the end, attendees will leave with
                                       a plan for ways to take video from public relations (promoting a program) to analysis
                                       and understanding (being reflective and critically engaging the voices of students
                                       and instructors).&nbsp; 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Developing an Online Course Evaluation Model that Results in Response Rates Above
                                          50%</strong></p>
                                    
                                    <p>Joe Boyd, Assessment Coordinator, Academic Effectiveness and Assessment; Jamie Ferrazano,
                                       Executive Director, Academic Technology; and Maggie Tymms, Assessment Director, Academic
                                       Effectiveness and Assessment, St. Petersburg College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>St. Petersburg College recently enhanced the electronic delivery method used to administer
                                       course evaluations, the Student Survey of Instruction.&nbsp; This topic describes the implementation
                                       of a widget and overlay video, displayed during the survey period until the survey
                                       is submitted. Although prior improvement methods have been successful, SPC’s recent
                                       comprehensive approach has been most effective, resulting in response rates that exceed
                                       50%.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Elevating and Understanding Ethical Reasoning Skills through Undergraduate Research
                                          Activities: What We are Learning when We Assess </strong></p>
                                    
                                    <p><strong>*Opening Keynote* </strong></p>
                                    
                                    <p>Julio Turrens, Ph.D., The Council of Undergraduate Research (CUR)</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The phrase “Responsible Conduct of Research” (RCR) encompasses a set of basic rules
                                       as well as federal regulations expected to be followed in research.&nbsp; These principles
                                       are to be followed not only by scientists and other professionals but also by students
                                       involved in undergraduate research experiences.&nbsp; Some of the principles learned during
                                       training can be extrapolated to other aspects of academic life, and contribute to
                                       making students more mature and aware of their responsibilities.&nbsp; This is particularly
                                       important since some college-age students do not have a clear idea of what constitutes
                                       academic misconduct.&nbsp; 
                                    </p>
                                    In this presentation we will discuss: a) the historical background that led to developing
                                    training areas in RCR; b) how do we adapt some of these topics to better train undergraduates
                                    at our institution; c) how undergraduates perceive the relevance of this training;
                                    and d) some current ideas for assessment of RCR training and misconduct among scientists.&nbsp;&nbsp;
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Innovations in the Assessment of Learning Specific to Communications: Oral Citations
                                          Rubric and a Pre-Post Assessment</strong></p>
                                    
                                    <p>April Raneri, Tina Tan, and Liza Schellpfeffer, Professors, Speech, Valencia College</p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Intensive Workshop on Developing and Assessing Program Learning Outcomes (2hr session)</strong></p>
                                    
                                    <p>Wendi Dew, Assistant Vice President, Teaching and Learning; and Laura Blasi, Ph.D.,
                                       Director, Institutional Assessment, Valencia College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Successful program learning outcomes assessment requires partnerships and a plan that
                                       can be implemented across programs. Workshop participants will be able to: 1. Write
                                       learning outcome statements and performance indicators; 2. Distinguish among program
                                       assessment methods and instruments; and 3. Involve colleagues in program assessment
                                       planning and implementation. Presenters will discuss the lessons learned over four
                                       years of planning at Valencia College. This session is relevant to both faculty members
                                       and administrators regardless of their leadership roles.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Next Steps from Florida’s Participation in the Pilot of the Threshold Achievement
                                          Test for Information Literacy (TATIL) Aligned with ACRL Frameworks</strong></p>
                                    
                                    <p>Diane Dalrymple, Librarian, Valencia College; and Carolyn Radcliff, Information Literacy
                                       Librarian for Carrick Enterprises
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>As one information literacy assessment option, Valencia Libraries participated in
                                       the Threshold Achievement Test for Information Literacy (TATIL) pilot designed to
                                       measure the competency level of college students regarding the Association of College
                                       &amp; Research Libraries (ACRL) threshold concepts.&nbsp; Participants will learn about the
                                       Valencia Libraries process and consider through poster posts whether standardized
                                       testing is a viable option for their institution.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Revisions to Developmental Education in English: Learning from Outcomes Assessment</strong></p>
                                    
                                    <p>Elizabeth Barnes, Professor of English and Chair, Academic Support; and Dana Davidson,
                                       Coordinator, Academic Support Center, Daytona State College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>In response to legislatively mandated changes to Developmental Education, Daytona
                                       State College is providing viable alternatives for students needing academic intervention
                                       and support in Introduction to Composition (ENC1101). Students at the College are
                                       able to take accelerated 7 ½-week developmental courses in reading (REA0017) and writing
                                       (ENC0025) before they take ENC1101. They can also choose the English Studio (ENC0055L),
                                       a one-hour per week, contextualized workshop style course. Assessment and accountability
                                       measures include a writing evaluation during the first week of class and perceptions
                                       of student learning surveys. This session will include insights gathered from the
                                       implementation of these options, and we will take a look at what we are learning from
                                       and how we are improving our learning outcomes assessment.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Show, Don’t Tell: Ways to Work with Data in Tableau (2hr session)</strong></p>
                                    
                                    <p>Derek Noce, Practice Director – Analytics; and Colleagues, DataBrains, Inc.</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This session provides data management and analytics best practices, a walk-through
                                       of Assessment analytical dashboards, and hands-on Tableau training. Based on our decades
                                       of experience, DataBrains will share lessons and recommendations regarding analytics
                                       leadership, program structure, data definition, user adoption, data integration, database
                                       development, optimized data preparation, and other key insights for analytics. Assessment
                                       use cases and common challenges will be discussed, leading to a series of Tableau
                                       dashboards which DataBrains has designed in response. Additionally, attendees will
                                       have the opportunity for a 30-minute, hands-on training session designed for beginners
                                       plus lessons and tips for more advanced Tableau users. DataBrains is a Jacksonville-based
                                       data analytics consultancy, and is a preferred alliance partner with Tableau, a leader
                                       in data analytics applications. DataBrains sponsors and facilitates Tableau User Groups
                                       in Jacksonville, Orlando, and Tampa.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Strategies for Applying Behavioral Economics to Assess and Strengthen Student Support
                                          (2hr session)</strong></p>
                                    
                                    <p>Josh Martin, Vice President; and Dani Grodsky, Associate, ideas42</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Students receive hundreds of communications from their school, starting before their
                                       first day of class and continuing throughout and beyond their tenure. Many include
                                       critical information about important actions and deadlines but very often do not trigger
                                       the desired reactions. As a follow-up to the pre-lunch talk, this workshop will give
                                       you tools to use behavioral science to improve the effectiveness of your student-facing
                                       communications and, critically, assess whether you’ve been successful in doing so.&nbsp;
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>The Gen Ed Assessment Journey: Where Pedagogy Meets Accountability</strong></p>
                                    
                                    <p>Susan Taylor, Ed.S., Coordinator of Institutional Effectiveness, Research, and Accreditation;
                                       and Jennifer Page, Director, Curriculum and Instruction, North Florida Community College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The Cheshire Cat tells Alice that she will get "somewhere" if she walks long enough!
                                       Finding the intersection of pedagogy and accountability in the assessment and reporting
                                       of student learning outcomes is challenging, especially for those new to assessment.
                                       Facilitators share their current professional development strategies and engagement
                                       processes, highlighting strengths and weaknesses, encouraging participants to engage
                                       in lively and interactive discussions of best practices.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>The New Advisor Experience Training Program: Leveraging Collaborative Effort</strong></p>
                                    
                                    <p>Ed Holmes, Director of Advising, West Campus; and Evelyn Lora Santos, Director of
                                       Advising, East Campus, Valencia College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Leading over 20 staff and faculty in the development, delivery, and assessment of
                                       an online hybrid advisor training program, the presenters provide a consistent model
                                       of collaborative effort across the college when onboarding new advisors and faculty
                                       in their roles as academic advisors. The presenters will also discuss ways that Title
                                       III Grant made this possible and how campus-specific projects were leveraged into
                                       a college-wide effort, supporting Valencia’s Quality Enhancement Plan (QEP). 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>The “Read to Succeed” QEP: Using Data for Continuous Improvement at Seminole State
                                          College</strong></p>
                                    
                                    <p>Carissa Baker, Director, Read to Succeed QEP and Professor of English, Seminole State
                                       College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Seminole State College’s Quality Enhancement Plan, Read to Succeed, focuses on improving
                                       reading comprehension and creating a culture of reading on campus. After three years
                                       of conducting this research project, we have learned a lot! This presentation will
                                       detail how we assess the QEP and how reviewing the data has led to improvement in
                                       our plan. These ideas can be applied to other programs, as the key concept is using
                                       data analysis for continuous improvement on course, program, and institutional levels.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>To MARS and Beyond – Opening the Gateway! / Math at the Root of Success – IRSC’s QEP</strong></p>
                                    
                                    <p>Bobbi Parrino Cook, Ed.D., Director QEP and Professor, Math, Indian River State College</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>IRSC’s QEP is “Improving Student Learning and Success in Gateway Math.” The plan has
                                       grown from one gateway course with two deliveries to two gateway courses with five
                                       deliveries. This is an Action Research project with many tweaks and changes. During
                                       this session current results of the plan will be shared along with an explanation
                                       of the processes, assessments, tweaks, successes, failures and the future.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Using a Peer Review Process to Ensure Quality in Online Learning at Daytona State
                                          College</strong></p>
                                    
                                    <p>Karla Moore, Ph.D., Dean, Academic Assessment and Planning; and Andrea Gibson, Ph.D.,
                                       Chair, Online Studies, Daytona State College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This session describes how Daytona State College launched an institutional initiative
                                       of online class assessment using a peer review process and incorporated this initiative
                                       into the overall institutional effectiveness of the College.&nbsp; A brief overview of
                                       the peer review process along with a detailed look at the results and how they affected
                                       online class quality and overall improvement of institutional assessment will be presented.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Using AAC&amp;U VALUE Rubrics to Enhance the General Education Assessment Model</strong></p>
                                    
                                    <p>Maggie Tymms, Assessment Director, Academic Effectiveness and Assessment; David Monroe,
                                       Academic Chair, Applied Ethics Institute; and Erika Johnson-Lewis, Professor, Humanities
                                       and Fine Arts, St. Petersburg College
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>St. Petersburg College recently enhanced its General Education Assessment Model by
                                       aligning Association of American Colleges and Universities (AAC&amp;U) VALUE Rubrics to
                                       institutional general education outcomes. This topic describes the processes of identifying
                                       VALUE Rubrics, and alignment to the outcomes’ course assessments and student competencies.
                                       The comprehensive assessment model discussed will include ideas for assessing Ethics,
                                       Critical Thinking, and Global Socio-Cultural Responsibility.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Using the Case Study Method to Teach and Assess Ethical Reasoning Skills in the Context
                                          Undergraduate Research Activities (2hr session)</strong></p>
                                    
                                    <p>Julio Turrens, Ph.D., The Council of Undergraduate Research (CUR); and Laura Blasi,
                                       Ph.D., Director, Institutional Assessment, Valencia College
                                    </p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>"Watching Assessment" - A Glance Backward while Looking Towards Tomorrow in a Closing
                                          Conversation with Pat Hutchings.</strong></p>
                                    
                                    <p>Pat Hutchings, Ph.D., Senior Scholar, National Institutute for Learning Outcomes Assessment
                                       (NILOA) 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Author of the influential article related to this topic (co-authored with Ted Marchese)
                                       published in 1990, she will be joining us by Skype to reflect on where we have been,
                                       while we discuss where we want assessment to go across Florida’s state colleges. Dr.
                                       Hutchings is currently a Senior Scholar at the National Institute for Learning Outcomes
                                       Assessment (NILOA) and Consulting Scholar for The Carnegie Foundation for the Advancement
                                       of Teaching.&nbsp; An electronic copy of the original article from 1990 will be provided
                                       in advance.<strong></strong></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                  
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/presentationabstracts.pcf">©</a>
      </div>
   </body>
</html>