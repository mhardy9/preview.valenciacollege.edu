<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/featured-presenters.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/">2016</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em><strong>June 2016</strong></em>
                           
                        </h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <h2><strong>Featured Speakers</strong></h2>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h4>&nbsp; </h4>
                        
                        <h2><strong>Opening Keynote Address:</strong></h2>
                        
                        <h3><em>Elevating and Understanding Ethical Reasoning Skills through Undergraduate Research
                              Activities: What We are Learning when We Assess</em></h3>            
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Dr. Julio Turrens</strong></p>
                                    
                                    <p>The Council of Undergraduate Research (CUR) </p>
                                    
                                    <p><a href="http://www.cur.org/about_cur/" target="_blank">http://www.cur.org/about_cur/</a></p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>            
                        
                        <h2>
                           <strong>Plenary Session:</strong> 
                        </h2>
                        
                        <h3><em>Applying  Insights about Behavior to Improve Student Performance: An Evidence Based
                              Approach </em></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Josh Martin</strong></p>
                                    
                                    <p>ideas42</p>
                                    
                                    <p><a href="http://www.ideas42.org/about-us/">http://www.ideas42.org/about-us/</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <p>Josh Martin&nbsp;is a Vice President at ideas42, currently responsible for managing behavioral
                                       economics intervention projects in the fields of financial aid reform and international
                                       cash transfer programs. Prior to joining ideas42,&nbsp;Josh&nbsp;was a policy advisor in Côte
                                       d’Ivoire’s Ministry of Planning and Development, having previously held posts at Cordoba
                                       Initiative and Princeton University’s Empirical Studies of Conflict program in addition
                                       to consulting roles at the World Bank, USAID, the National Democratic Institute, and
                                       others. With an MPP from the Harvard Kennedy School of Government,&nbsp;Josh’s policy research
                                       has included works on microfinance, conflict dynamics and governance in developing
                                       countries.&nbsp;Josh&nbsp;speaks fluent Arabic and French, and barely enough Farsi to order
                                       his favorite kebabs. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Dani Grodsky</strong></p>
                                    
                                    <p>ideas42</p>
                                    
                                    <p><a href="http://www.ideas42.org/about-us/">http://www.ideas42.org/about-us/</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Dani Grodsky is an Associate at Ideas42. Currently, her focus is on improving student
                                       outcomes throughout postsecondary education by applying a behavioral lens. She recently
                                       graduated from Brown University, where she majored in Cognitive Neuroscience with
                                       a smaller focus on Economics. She also has experience in science communication and
                                       event planning, writing for the Brown Medical School magazine and TED.com, and helping
                                       to organize the annual TEDxProvidence conference. Her quirkiest job was working at
                                       a bridal shop featured on reality television, and if she had an alter ego she would
                                       be a makeup artist for runway shows.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>            
                        <h2>Plenary Session: </h2>
                        
                        <h3>
                           <em>Show, Don’t Tell: Ways to Work with Interactive Learning Assessment Data in Tableau
                              (Using CCSSE Results, Course Evaluation Data, and More)</em><br>
                           
                        </h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Derek Noce</strong></p>
                                    
                                    <p>DataBrains</p>
                                    
                                    <p><a href="http://www.databrains.com" target="_blank">databrains.com  </a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <p>Derek leverages his decades of business and market intelligence experience to define
                                       and deliver actionable analytics programs. As our Tableau practice director, Derek
                                       provides thought leadership and structure to our team, our clients, and, via the user
                                       groups he facilitates, Florida’s Tableau community. After many years of insightful
                                       retail/consumer sector analysis for institutional investment firms, Derek honed an
                                       efficient, results-focused approach. He has incorporated Tableau into his capabilities
                                       and is dedicated to enabling our clients to make better decisions with their data
                                       and visualizations. Derek has worked with a wide variety of clients, including a major
                                       investment management firm, a real estate investment trust, a regional airline, a
                                       leading travel services provider, a consumer goods company, a restaurant technology
                                       company, a state university, and a major real estate development.
                                    </p>
                                    
                                    
                                    <p><strong>Click here for your free extended trial of Tableau: </strong></p>
                                    
                                    <p><a href="http://get.tableau.com/tpep/ValenciaCollege.html" target="_blank">http://get.tableau.com/tpep/ValenciaCollege.html</a></p>      
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Michael Docteroff</strong></p>
                                    
                                    <p>DataBrains</p>
                                    
                                    <p><a href="http://www.databrains.com" target="_blank">databrains.com</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> Michael brings over 27 years of experience as Lead Architect for a data warehouse
                                    and business intelligence platform solutions to the DataBrains team. Having built
                                    enterprise data architecture that supports legacy reporting needs, Michael has demonstrated
                                    his flexibility to support ongoing change in Enterprise-wide business systems design,
                                    data warehousing, and CRM to leverage existing data and technologies for timely and
                                    cost-effective centralized reporting for renowned clientele such as Extended Stay
                                    America, La Quinta Hotels, and Gartner’s Subscription Services, Order Management,
                                    and Fulfillment systems. 
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2016/featured-presenters.pcf">©</a>
      </div>
   </body>
</html>