<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2011featuredspeakers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em><strong>June 2011</strong></em>
                           
                        </h2>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Keynote Speakers</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Steven M. Sheeley</p>
                                    
                                    <p>Vice President, Commission on Colleges</p>
                                    
                                    <p>Southern Association of Colleges and Schools </p>
                                    
                                    <p><a href="documents/sheeleybio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Concurrent Session Speakers </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Nick Bekas</p>
                                    
                                    <p>Director, Developmental Education Initiative</p>
                                    
                                    <p>Valenca College  </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dawn Broschard </p>
                                    
                                    <p>Director, Institutional Effectiveness</p>
                                    
                                    <p>Broward College  </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Kathy Bucklew</p>
                                    
                                    <p>Registrar</p>
                                    
                                    <p>Polk State College </p>
                                    
                                    <p><a href="documents/KathyBucklew.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Adam Denny </p>
                                    
                                    <p>Academic Assessment Officer </p>
                                    
                                    <p>Hillsborough Community College </p>
                                    
                                    <p><a href="documents/AdamDenny.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Wendi Dew</p>
                                    
                                    <p>Academic Coordinator, Faculty Development</p>
                                    
                                    <p>Valencia College </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Kurt Ewen</p>
                                    
                                    <p>Assistant Vice President, Assessment and Institutional Effectiveness </p>
                                    
                                    <p>Valencia College</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Deborah Hefferin </p>
                                    
                                    <p>Assistant Professor in Fine Arts and Communication</p>
                                    
                                    <p>Broward College  </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Teresa Hodge </p>
                                    
                                    <p>Assistant Professor in Mathematics </p>
                                    
                                    <p>Broward College </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>John Hughes</p>
                                    
                                    <p>Associate Vice Chancellor for Evaluation</p>
                                    
                                    <p>Division of Florida Colleges </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Mark Morgan</p>
                                    
                                    <p>Director, Institutional Research</p>
                                    
                                    <p>Seminole State College</p>
                                    
                                    <p><a href="documents/AboutMarkMorgan.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Joyce Walsh-Portillo</p>
                                    
                                    <p>Associate Professor in Business Administration</p>
                                    
                                    <p>Broward College   </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2011featuredspeakers.pcf">©</a>
      </div>
   </body>
</html>