<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/presentationabstracts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/">2014</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em>June 19-20, 2014</em><br>
                           Orlando, FL
                        </h2>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>Presentations Abstracts<br>
                                          
                                       </h3>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       
                                       <p><strong><a name="OpeningSession" id="OpeningSession"></a>Using Big Data to Improve Student Learning - Laura Malcolm, Vice President of Product,
                                             Civitas Learning;&nbsp; Kurt Ewen, Assistant Vice President, Institutional Effectiveness
                                             and Planning; and Brian Macon, Professor of Mathematics, Valencia College</strong></p>
                                       
                                       
                                       <p>As students pursue their educations, they tell their stories in the data they create
                                          and interact with along the way. That information — captured in higher education databases
                                          — is an untapped resource that has the power to transform students’ journeys by improving
                                          their learning, academic outcomes, and overall likelihood of success.&nbsp; But what is
                                          the best way to capture this data and turn it into actionable insights? Valencia College
                                          and Civitas Learning have partnered to implement a "data, science, people" approach
                                          to delivering the right information, to the right people, at the right time, and in
                                          the right way in order to help more students be successful. This session will discuss
                                          predictive analytics applications and demonstrate how faculty and staff at Valencia
                                          College are starting to use insights to innovate learning and iterate toward improvements
                                          in online student success. 
                                       </p>
                                       
                                       <p>Speakers will discuss the following aspects of the "data, science, people" approach
                                          to inform decisions – big and small – across the student life cycle: 1. How institutional
                                          data from a broad array of sources (SIS, LMS, CRM, etc.) is captured in order to provide
                                          a historical view and to see predictions of future performance, 2. How data science
                                          insights are generated to enable faculty and staff to identify the most powerful predictors
                                          of student success and risk, and 3. Challenges and opportunities on the journey to
                                          using data to impact student outcomes. How insight can be turned into action by pushing
                                          student-level predictive analytics to the frontlines?
                                       </p>
                                       
                                       
                                       <p><strong><a name="WendyandLaura" id="WendyandLaura"></a>Intensive Workshop on&nbsp; Developing and Assessing Program Learning Outcomes&nbsp; *Special
                                             Events Center, Main Room* </strong><strong>(2hr session)- Wendi Dew,&nbsp; Director of Faculty Development; and Laura Blasi, Ph.D.,
                                             Director, Institutional Assessment, Valencia College</strong></p>
                                       
                                       <p>Successful program learning outcomes assessment requires partnerships and a plan that
                                          can be implemented across programs. Workshop participants will be able to: 1. Write
                                          learning outcome statements and performance indicators; 2. Distinguish among program
                                          assessment methods and instruments; and 3.Involve colleagues in program assessment
                                          planning and implementation. Presenters will discuss the lessons learned over four
                                          years of planning at Valencia College. This session is relevant to both faculty members
                                          and administrators regardless of their leadership roles 
                                       </p>
                                       
                                       
                                       <p><strong><a name="JohnFrederick" id="JohnFrederick"></a>Part I: Community College Survey of Student Engagement (CCSSE): Alternative Uses (2hr
                                             session) - John Frederick, Ph.D., Director of Learning Outcomes Assessment;&nbsp; and Archie
                                             Cubarrubia, Ed.D., Director of Planning and Policy Analysis, Miami Dade College</strong></p>
                                       
                                       <p>&nbsp; Learn how to use CCSSE in alternative ways. Participants will explore how CCSSE
                                          and other indirect measures can be used to close-the-loop and complement general education
                                          outcomes attainment. Additionally, this session addresses the use of CCSSE data in
                                          policy and planning conversations, such as discussions regarding Aspirations to Achievement:
                                          Men of Color and Community Colleges.
                                       </p>
                                       
                                       <p><strong><a name="BarikaBarboza" id="BarikaBarboza"></a>Part II: Practical Applications for Using Assessment Results to Improve Student Learning
                                             - Barika Barboza, Senior Trainer; and Yahemn Baeza Dager, Senior Assessment and Planning
                                             Associate, Miami Dade College</strong> 
                                       </p>
                                       
                                       <p>Assessment can seem like a daunting task. How do you use the results? Using a step-by-step
                                          approach the participants will be guided through the processes of using assessment
                                          results (including CCSSE data) to improve learning and teaching.
                                       </p>
                                       
                                       <p><strong><a name="KurtEwen" id="KurtEwen"></a>Strategies to Improve the Use of Data in Decision Making (2hr session) - Kurt Ewen,
                                             Assistant Vice President, Institutional Effectiveness and Planning, Valencia College.</strong></p>
                                       
                                       <p>The data-driven movement in higher education often assumes that both the meaning of
                                          the data we collect is self-evident and that the path forward using data is equally
                                          clear. This assumption is not generally true and is particularly untrue of data collected
                                          from the majority of learning outcomes assessment. This session will consider a series
                                          of strategies for collaboratively collecting and analyzing data that can help improve
                                          decision making and action for the benefit of students.
                                       </p>
                                       
                                       <p><strong><a name="CarolWeideman" id="CarolWeideman"></a>Building a Gateway for Non-STEM Students: Learning Assessment and the Developmental
                                             Student - Carol Weideman, Ph.D., Professor of Mathematics; and Sandy Cohen, Professor
                                             of Mathematics, St. Petersburg College</strong> 
                                       </p>
                                       
                                       <p>MAT1990 Exploration of Mathematics and Quantitative Reasoning was developed for non-STEM
                                          students as an alternative to MAT1033.&nbsp; This course provides the foundation for understanding
                                          concepts necessary for success in subsequent statistics and liberal arts math courses
                                          including algebra, set theory, geometry, probability, and statistics.&nbsp; Critical thinking
                                          skills, communicating mathematically and appropriate use of technology are incorporated.
                                       </p>
                                       
                                       <p><strong><a name="AndreaReese" id="AndreaReese"></a>Using Assessment to Ensure Quality in Online Learning - Andrea Reese, Ph.D., Chair
                                             of Online Studies; and Karla Moore, Ph.D., Academic Assessment and Planning Dean,
                                             Daytona State College</strong></p>
                                       
                                       <p>This session describes how Daytona State College is incorporating online assessment
                                          into the overall institutional effectiveness process of the college and the steps
                                          taken to develop a plan and process that will focus on the quality of online course
                                          design with faculty support and buy-in.&nbsp; Participants will have an opportunity to
                                          share and discuss the good, the bad, and the ugly of online quality assessment.
                                       </p>
                                       
                                       <p><strong><a name="LauraBlasi" id="LauraBlasi"></a>Developing a Communications Strategy to Support Faculty Assessing Student Learning
                                             - Laura Blasi, Ph.D., Director of Institutional Assessment, Valencia College </strong></p>
                                       
                                       <p>Institutional researchers can provide support for faculty members as they seek to
                                          improve the attainment of student learning outcomes through assessment. Sometimes
                                          a few dedicated faculty members drive the process, but increased faculty support is
                                          needed to cultivate a culture of assessment on campus. In this session, we will discuss
                                          the results from a qualitative survey of Institutional Research professionals and
                                          leaders in Academic Affairs - from this emerged some key recommendations and strategies
                                          that you can share on your own campus. 
                                       </p>
                                       
                                       <p><strong><a name="TammyMuhs" id="TammyMuhs"></a>Tracking Change and Documenting Impact: Learning Assessments Shared Across Partner
                                             Institutions - Tammy Muhs, Ph.D., Assistant Chair &amp; Lecturer, Mathematics Department,
                                             University of Central Florida; and Maryke Lee, Ph.D., Dean, Mathematics, Valencia
                                             College</strong></p>
                                       
                                       <p>Participants will learn about an assessment collaboration between the mathematics
                                          departments at Valencia College and the University of Central Florida.&nbsp; The common
                                          assessments and preliminary results will be shared.
                                       </p>
                                       
                                       <p><strong><a name="CarlaRossiter" id="CarlaRossiter"></a>Race to the Top: Improving Post-college Performance Metrics - Carla Rossiter, Assessment
                                             Director, College of Education, St. Petersburg College</strong></p>
                                       
                                       <p>As part of the Race to the Top initiative, the Florida Department of Education provides
                                          an Annual Report Card for each teacher preparation program in the state.&nbsp; The report
                                          card focuses on six post college performance metrics.&nbsp; This session will detail how
                                          one Teacher Preparation College organized faculty and assessment professionals to
                                          revise curriculum and measurably improve Student Success and annual program "report
                                          card" metrics.
                                       </p>
                                       
                                       <p><strong><a name="RobertaVandermast" id="RobertaVandermast"></a>Authentic Online Assessment: What’s in It for Your Students? -&nbsp; Roberta Vandermast,
                                             Professor Emeritus and Senior Teaching Fellow Valencia College</strong></p>
                                       
                                       <p>Authentic assessment is more than a buzz word.&nbsp; It's a way to use assessment to engage
                                          students in solving real world problems, engage professional standards, and practice
                                          critical thinking -- But it requires more work from both teachers and students.&nbsp; Participants
                                          will work from their own experiences to an understanding of what authentic assessment
                                          is and what it takes to use it effectively in the classroom.&nbsp; Along the way, information
                                          from educational research will underscore key points. 
                                       </p>
                                       
                                       <p><strong><a name="PeterUsinger" id="PeterUsinger"></a>Tracking Pathways to Student Success - Peter Usinger, Director of Institutional Research,
                                             Effectiveness, and Planning, Polk State College</strong></p>
                                       
                                       <p>Using the Motivated Strategies for Learning Questionnaire, a free assessment tool,
                                          Polk State College shows how it applies the analysis of longitudinal data to track
                                          student success.&nbsp; The presenter will provide insights into the interaction between
                                          student motivation and learning strategies across disciplines and delivery methods.
                                          &nbsp;Particular shortages in higher order learning skills will be discussed and how findings
                                          can be used to improve student orientation and curriculum content.
                                       </p>
                                       
                                       <p><strong><a name="KimberlyHardy" id="KimberlyHardy"></a>Engaging Key Stakeholders in Assessing Student Learning Outcomes in Collegiate Life
                                             and Co-Curricular Experiences - Kimberly Hardy, Ph.D., Executive Dean of Student Success
                                             and Learning Engagement; and Lynne Crosby, Ph.D., Associate Vice President, Institutional
                                             Effectiveness &amp;&nbsp; Accreditation, Florida State College at Jacksonville</strong></p>
                                       
                                       <p>Sharing ideas and models is vital to enhancing the impact of institutional effectiveness
                                          efforts.&nbsp; Roundtable participants will discuss ways to promote meaningful and active
                                          engagement of key stakeholders in institutional effectiveness efforts in student development
                                          units, particularly related to collegiate life and co-curricular experiences. 
                                       </p>
                                       
                                       <p><strong><a name="GregLindeblom" id="GregLindeblom"></a>When the Accreditors Leave - Greg Lindeblom, Assistant Professor of Business Administration;&nbsp;
                                             Nilo Marin, Ph.D., Professor of Biological Sciences;&nbsp; Behnoush Memari, Ph.D., Assistant
                                             Professor of Physical Sciences; and Christopher Johnston, Ph.D., Assistant Professor
                                             of English, Broward College</strong></p>
                                       
                                       <p>Assessment of student learning has enormous value in education, but it is often driven
                                          by accreditor mandate or anticipation of accreditor’s response.&nbsp; After the accreditors
                                          leave, how do you maintain the vitality?&nbsp; How do you keep faculty and administrators
                                          engaged and supportive? In this workshop, we will investigate some methods for maintaining
                                          momentum by focusing on long-term goals and strategies.
                                       </p>
                                       
                                       <p><strong><a name="MarkMorgan" id="MarkMorgan"></a>Read to Succeed: Assessing Seminole State's QEP - Mark Morgan, Ed.D., Associate Vice
                                             President, Institutional Effectiveness, Seminole State College of Florida</strong></p>
                                       
                                       <p>Seminole State uses a comprehensive range of course-level and college-wide assessments
                                          for measuring reading comprehension and metacognitive awareness associated with its
                                          Quality Enhancement Plan (QEP). Learn about Seminole State’s approach, lessons learned,
                                          reactions from faculty, and results to date.
                                       </p>
                                       
                                       <p><strong><a name="LynneCrosby" id="LynneCrosby"></a>Program Assessment Practices at Institutions Across the Southeast - Lynne Crosby,
                                             Ph.D., Associate Vice President, Institutional Effectiveness and Accreditation; and
                                             Susan Schultz, DNP, CNE, Professor of Nursing and Institutional Effectiveness Faculty
                                             Co-Chair, Florida State College at Jacksonville</strong></p>
                                       
                                       <p>This session focuses on a recent survey of program assessment/general education assessment
                                          practices at Florida College System institutions and other southeastern institutions.&nbsp;
                                          The presenters will summarize the results of the frequency of assessment cycles, the
                                          review of program assessment plans and reports, guidelines, the impact of the program
                                          assessment/general education assessment process, and future changes that institutions
                                          may be considering for their program assessment process. 
                                       </p>
                                       
                                       <p><strong><a name="KatinaGothard" id="KatinaGothard"></a>General Education Assessment: One College’s Journey - Katina Gothard, Ph.D., Professor,
                                             Mathematics; and&nbsp; Jayne Gorham, Ed.D., Associate Vice President of Planning and Assessment,
                                             Eastern Florida State College</strong></p>
                                       
                                       <p>In this session, participants will learn how faculty at Eastern Florida State College
                                          designed, tested, and fully implemented a general education assessment framework within
                                          three years. Participants will be provided a list of resources that were used to design
                                          the framework, the discipline rubrics developed for each of the general education
                                          outcomes, and the surveys used to aid analysis of student performance data.
                                       </p>
                                       
                                       <p><strong><a name="KristinAbel" id="KristinAbel"></a>Creativity: Assessment in the Arts and Entertainment Fields - Kristin Abel, Professor
                                             of Entertainment Design &amp; Technology; Alan Gerber, Professor of Music; Troy Gifford,
                                             D.M.A., Professor of Music;&nbsp; Wendy Givoglu, Dean of Arts &amp; Entertainment; and Kristy
                                             Pennino, Program Chair &amp; Professor, Graphic and Interactive Design Program, Valencia
                                             College</strong></p>
                                       
                                       <p>Arts and Entertainment faculty members face distinct and inspiring challenges when
                                          seeking to assess program learning outcomes in their areas of expertise. The faculty
                                          members who gather in the session will be describing their emerging assessment processes
                                          which have developed over the past three years. They will share the challenges they
                                          have faced and how they have met those challenges. This conversation is being held
                                          in advance of the afternoon workshop on the use of AAC&amp;U's creativity rubric developed
                                          through project VALUE.
                                       </p>
                                       
                                       <p><strong><a name="LynneCrosby2" id="LynneCrosby2"></a>Assessing Student Learning Outcomes Beyond the Classroom - Lynne Crosby, Ph.D., Associate
                                             Vice President, Institutional Effectiveness &amp; Accreditation; and Kimberly Hardy, Ph.D.,
                                             Executive Dean of Student Success and Learning Engagement, Florida State College at
                                             Jacksonville</strong></p>
                                       
                                       <p>This session offers approaches to guide outcome assessment for student development
                                          units, with an emphasis on student learning.&nbsp; Attendees will participate in the identification
                                          of student learning outcomes in student development units and discuss relevant direct
                                          and indirect measures of assessing student learning in these co-curricular experiences.
                                       </p>
                                       
                                       <p><strong><a name="MarthaAmbrose" id="MarthaAmbrose"></a>Creating Faculty Assessment Leadership at Edison State College - Martha Ambrose, Learning
                                             Assessment Committee Chair; and Amy Trogan, Ph.D., Assessment Coordinator, Edison
                                             State College</strong> 
                                       </p>
                                       
                                       <p>Edison State College has implemented a new faculty leadership role in assessment:
                                          the Faculty Assessment Coordinator.&nbsp; Like many institutions of higher education, ESC
                                          has a Learning Assessment Committee that oversees academic assessment with rotating
                                          members; however, the link between the committee and departments needed to be stronger,
                                          with professional training for each faculty member.&nbsp; Thus, the faculty and administration
                                          created the Assessment Coordinator position for each department.
                                       </p>
                                       
                                       <p><strong><a name="AshleyFinleyWorkshop" id="AshleyFinleyWorkshop"></a>Workshop:&nbsp; Common Ground: Using Rubrics to Create Dialogue. - Ashley Finley, Ph.D.,
                                             Senior Director of Assessment and Research, The Association of American Colleges and
                                             Universities (AAC&amp;U)</strong></p>
                                       
                                       <p>Campuses nationally are increasingly integrating direct assessment of student learning
                                          into their assessment portfolios.&nbsp; A significant number of these campuses have worked
                                          with the AAC&amp;U VALUE rubrics to help guide these efforts.&nbsp; Essential to successful
                                          adoption and implementation of the rubrics, however, is engaging faculty and campus
                                          co-educators in critical discussions around the interpretation of the rubric, application
                                          of performance levels, and use of results.&nbsp; In this session, participants will engage
                                          in a condensed calibration exercise that is used to train faculty and staff on applying
                                          rubrics to samples of student work.&nbsp; The VALUE rubric for "creative thinking" will
                                          be used to score a sample of student work to illustrate the utility of engaging campus
                                          stakeholders, including students, in dialogue around articulation of learning outcomes
                                          and interdisciplinary approaches to assessing student learning.&nbsp; Campus examples of
                                          calibration, implementation of the rubrics, and the use of evidence from direct assessment
                                          to improve student learning will also be shared.&nbsp; 
                                       </p>
                                       
                                       <p><strong><a name="JudyJonesLiptrot" id="JudyJonesLiptrot"></a>Continuous Improvement. Is That Even Possible in a Demanding Environment? - Judy Jones-Liptrot,
                                             Ed.D., Assessment and Certification Center Manager at the Open Campus/Deerwood Center;
                                             and Richard Turner, Assessment and Certification Manager-Kent Campus , Florida State
                                             College at Jacksonville</strong></p>
                                       
                                       <p>What do your customers think of your service? Is continuous improvement possible when
                                          you are so busy with your daily tasks?&nbsp; In this session, Judy and Rich will preview
                                          their customer satisfaction survey used throughout the College as well as real world
                                          results.&nbsp; They will walk through how to survey for continuous improvement as well
                                          as effective student learning outcomes for non-academic units.
                                       </p>
                                       
                                       <p><strong><a name="ChristinaHardin" id="ChristinaHardin"></a>Reinventing the Process of Student-Learning Assessment in English Composition - Christina
                                             Hardin, Ph.D., Director, New Student Experience, Valencia College</strong></p>
                                       
                                       <p>Assessment work in English Composition does not have to be challenging and cumbersome.
                                          Faculty from Valencia College will discuss how they revamped and reinvented the assessment
                                          of student learning in English Composition 1 and 2, college-wide.&nbsp; Attendees will
                                          participate in this interactive session designed to help them identify obstacles to
                                          their assessment success and find ways to improve their processes.
                                       </p>
                                       
                                       <p><strong><a name="AshleyCaron" id="AshleyCaron"></a>Using Business Intelligence to Enhance the Program Viability Report Process - Ashley&nbsp;
                                             Caron, Baccalaureate Assessment &amp; Accreditation Coordinator; and Maggie Tymms, Assessment
                                             Director, St. Petersburg College</strong></p>
                                       
                                       <p>St. Petersburg College's Academic Program Viability Report (APVR), a yearly summative
                                          evaluation of each program's viability, provides key College stake holders a snapshot
                                          of relevant program specific information.&nbsp; In 2013, Institutional Research and Effectiveness
                                          developed dashboards within SPC's Pulse/Business Intelligence system which highlight
                                          program trends in five man measure areas.&nbsp; The benefits realized by streamlining the
                                          APVR data extraction process have exceeded our expectations.
                                       </p>
                                       
                                       <p><strong><a name="KatiePaschall" id="KatiePaschall"></a>Assessing Oral Communication: An Evolving Process- Katie Paschall, Ph.D., Assessment
                                             Coordinator for the Department of Speech Communication and Foreign Languages, Edison
                                             State College</strong></p>
                                       
                                       <p>Assessing oral communication in the Basic Speech Course has proved to be a lengthy,
                                          expensive, and challenging endeavor.&nbsp; The Department of Speech and Foreign Languages
                                          at Edison State College has embraced the challenge and developed an assessment process
                                          leading to greater satisfaction and mastery of SLOs. This session will discuss the
                                          evolving process of faculty commitment, training, results, and plans for continuous
                                          improvement.
                                       </p>
                                       
                                       <p><strong><a name="JesseCoraggio" id="JesseCoraggio"></a>Using Business Intelligence to Improve the College Experience- Jesse Coraggio, Ph.D.,
                                             Associate Vice President, Institutional Effectiveness, Research and Grants; and Dan
                                             Gardner, Ph.D., Director of Institutional Research, St. Petersburg College</strong></p>
                                       
                                       <p>This session will provide a review of the College Experience initiative at SPC and
                                          how the college’s Pulse BI system was used to support this initiative. The College
                                          Experience includes a Student Learning Plan, Expanded Out-of-Classroom Support, Early
                                          Alert and Student Coaching, Integrated Career Services, and New Student Orientation.
                                          This strategic initiative is intended to improve student success and help students
                                          finish what they start. During a ‘live’ demonstration of the web-based SPC Pulse BI
                                          system, the participants will be shown how student behavioral data from the Pulse
                                          BI system is used by college stakeholders monitor the success of the various elements
                                          of the College Experience Student Success Initiative 
                                       </p>
                                       
                                       <p><strong><a name="SusanSchultz2" id="SusanSchultz2"></a>Trends in Outcomes and Plans of Academic Programs - Susan Schultz, DNP, CNE, Professor
                                             of Nursing and Institutional Effectiveness Faculty Co-chair, Jametoria Burton, B.A.,
                                             M.L.I.S., Associate Director Program Development/General Education Assessment, Janice
                                             Amos, MA., Program Manager of Career Education/Public Safety Management, and Derrick
                                             Johnson, M.Ed., Student Success Advisor/Student Life Skills Instructor, Florida State
                                             College at Jacksonville</strong></p>
                                       
                                       <p>During this presentation, participants will learn how the Institutional Effectiveness
                                          Committee analyzed patterns and trends in their academic programs' assessment plans
                                          and reports.&nbsp; They will examine links between general education outcomes and programs
                                          outcomes, analyzes action plans and improvements achieved reported in one year, and
                                          discuss recommendations for college action based on the analysis of program assessment
                                          plans and reports.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/presentationabstracts.pcf">©</a>
      </div>
   </body>
</html>