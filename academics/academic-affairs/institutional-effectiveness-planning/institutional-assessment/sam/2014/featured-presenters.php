<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/featured-presenters.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/">2014</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em><strong>June 2014</strong></em>
                           
                        </h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <h2><strong>Featured Speaker </strong></h2>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <h2>Dr. Ashley Finley&nbsp;</h2>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Ashley Finley is the senior director of assessment and research at AAC&amp;U and national
                                       evaluator for the Bringing Theory to Practice (BTtoP) Project. Finley’s national work,
                                       at both the campus and national levels, focuses on developing best practices regarding
                                       program implementation, instrumentation, and mixed methods assessment. Her work combines
                                       assisting campuses with the implementation of assessment protocols and the promotion
                                       of best practices across the institution, including general education, academic departments,
                                       and the co-curriculum. She is the author of Making Progress: What We Know the Achievement
                                       of Liberal Education Outcomes, and Using the VALUE Rubrics for Improvement of Learning
                                       and Authentic Assessment, with Terrel Rhodes, and many other articles and book chapters
                                       on assessment and student learning. In her work with Bringing Theory to Practice,
                                       Ashley has worked with campuses to implement and assess programs focused on the intersectionality
                                       of emphases attendant to the whole student— their engagement in learning, civic development,
                                       and their psychosocial well-being. Before joining AAC&amp;U, she was an assistant professor
                                       of sociology at Dickinson College, where she taught courses in quantitative methods,
                                       social inequality, and gender in Latin America. As a faculty member she taught courses
                                       incorporating high-impact learning practices, such as learning communities and service
                                       learning.&nbsp; Finley received a BA from the University of Nebraska-Lincoln and an MA
                                       and PhD, both in sociology, from the University of Iowa.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Session - What Should Students Know and Be Able to Do?: Connecting Meaningful Assessment
                                          With Meaningful Questions</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Learning in the twenty-first century has been contextualized by a number of factors
                                       that have profoundly shaped (and reshaped) higher education. Just as everyday life
                                       has been dramatically altered through increasing levels of interconnectivity and application,
                                       so too has college level learning. To meet the demands of an expanding global world,
                                       colleges and universities increasingly need to consider the role of assessment to
                                       tell a story about student learning across the curriculum.&nbsp; In part, this means connecting
                                       authentic evidence of students’ learning and skill development (e.g. critical thinking,
                                       integrative learning, and civic responsibility) to the engaging practices that help
                                       to deepen their understanding. It also means gathering the right kind of evidence
                                       that is meaningful to faculty, campus co-educators and students and that can be thoughtfully
                                       used to facilitate evidence-based improvement of efforts.&nbsp; This discussion will focus
                                       on how direct assessment of student learning using rubrics can promote transparency
                                       across institutional learning outcomes and provide actionable evidence of what students
                                       can actually do. The goal of the session is to give participants insights into the
                                       kinds of questions that can not only help to guide assessment, but can also help to
                                       construct a campus narrative for student learning that matters to faculty, student
                                       affairs professionals, administrators, <u>and</u> students. 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Workshop - Common Ground: Using Rubrics to Create Dialogue, Collaboration and Meaningful
                                          Assessment</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campuses nationally are increasingly integrating direct assessment of student learning
                                       into their assessment portfolios.&nbsp; A significant number of these campuses have worked
                                       with the AAC&amp;U VALUE rubrics to help guide these efforts.&nbsp; Essential to successful
                                       adoption and implementation of the rubrics, however, is engaging faculty and campus
                                       co-educators in critical discussions around the interpretation of the rubric, application
                                       of performance levels, and use of results.&nbsp; In this session, participants will engage
                                       in a condensed calibration exercise that is used to train faculty and staff on applying
                                       rubrics to samples of student work.&nbsp; The VALUE rubric for “creative thinking” will
                                       be used to score a sample of student work to illustrate the utility of engaging campus
                                       stakeholders, including students, in dialogue around articulation of learning outcomes
                                       and interdisciplinary approaches to assessing student learning.&nbsp; Campus examples of
                                       calibration, implementation of the rubrics, and the use of evidence from direct assessment
                                       to improve student learning will also be shared.&nbsp; 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        <h2>&nbsp; </h2>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/featured-presenters.pcf">©</a>
      </div>
   </body>
</html>