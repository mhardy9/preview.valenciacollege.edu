<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/programs-electronic-presentations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/">2014</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <span>
                           <h2>State Assessment Meeting<br>
                              <em>June 19-20, 2014</em><br>
                              Orlando, FL
                           </h2>
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             <h3><em><strong>Please <a href="documents/SAM2014ProgramwAgenda.pdf" target="_blank">click here</a> to download the Program Schedule</strong></em></h3>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <h6><u>Presentations </u></h6>
                                          
                                          <h3>
                                             <em>Within each session below select the <strong><a href="documents/TrackingPathwaystoSuccess2014-SAMslides.pdf" target="_blank"><img alt="Handouts Icon" border="0" height="40" src="Handouts.png" width="40"></a></strong>icon to download session handouts.</em><br>
                                             
                                          </h3>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>  
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">
                                          <div>Session A - Building 8, Special Events Center </div>
                                       </div>
                                       
                                       <div data-old-tag="th">Session B - Building 11, Room 11-314</div>
                                       
                                       <div data-old-tag="th">Session C - Building 11 Room 11-317 </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">
                                          
                                          <p><strong>"Welcome"</strong></p>
                                          
                                          <p>Dr. Laura Blasi<br>
                                             Valencia College 
                                          </p>
                                          
                                          <p><strong>State Assessment Meeting Opening Session:<a href="PresentationAbstracts.html#OpeningSession"><br>
                                                   "Using Big Data to Improve Student Learning"</a> </strong></p>
                                          Laura Malcolm, Vice President of Product, Civitas Learning, Kurt Ewen, Assistant Vice
                                          President, Institutional Effectiveness and Planning; and Brian Macon, Professor of
                                          Mathematics, 
                                          Valencia College <strong><a href="documents/Opening2014SAM.pdf" target="_blank"><img alt="Handouts Icon" border="0" height="40" src="Handouts.png" width="40"></a><img alt="Handouts Icon" height="40" src="Handouts.png" width="40"></strong>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#WendyandLaura">"Intensive Workshop on Developing and Assessing Program Learning Outcomes"</a></strong></p>
                                          
                                          <p>Wendi Dew and Laura Blasi, Ph.D.,<br>
                                             Valencia College 
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#JohnFrederick">"Community College Survey of Student Engagement (CCSSE): Alternative Uses"</a></strong></p>
                                          
                                          <p><strong><a href="PresentationAbstracts.html#BarikaBarboza">"Practical Applications for Using Assessment Results to Improve Student Learning"</a></strong></p>
                                          
                                          <p>John Frederick, Ph.D., Archie Cubarrubia, Ed.D., Barika Barboza, and Yahemn Baeza
                                             Dager, Miami Dade College 
                                          </p>
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#KurtEwen">"Strategies to Improve the Use of Data in Decision Making"</a></strong></p>
                                          
                                          <p>Kurt Ewen, Assistant Vice President, Institutional Effectiveness and Planning, <br>
                                             Valencia College 
                                          </p>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#CarlaRossiter">"Race to the Top: Improving Postcollege Performance Metrics" </a></strong></p>
                                          
                                          <p>Carla Rossiter<br>
                                             St. Petersburg College 
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#CarolWeideman">"Building a Gateway for Non-STEM Students: Learning Assessment and the Developmental
                                                   Student"</a></strong></p>
                                          
                                          <p>Carol Weideman, Ph.D., and Sandy Cohen <br>
                                             St. Petersburg College 
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#AndreaReese">"Using Assessment to Ensure Quality in Online Learning"</a></strong></p>
                                          
                                          <p>Andrea Reese, Ph.D., and Karla Moore, Ph.D.<br>
                                             Daytona State College 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#LauraBlasi">"Developing a Communications Strategy to Support Faculty Assessing Student Learning
                                                   " </a></strong></p>
                                          
                                          <p>Laura Blasi , Ph.D.<br>
                                             Valencia College 
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#TammyMuhs">"Tracking Change and Documenting Impact: Learning Assessments Shared Accross Partner
                                                   Institutions" </a></strong></p>
                                          
                                          <p>Tammy Muhs, Ph.D., and Maryke Lee, Ph.D.<br>
                                             University of Central Florida and Valencia College 
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#RobertaVandermast">"Authentic Online Assessment: What's in it for Your Students?"</a></strong></p>
                                          
                                          <p>Roberta Vandermast <br>
                                             Valencia College
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#GregLindeblom">"When the Accreditors Leave" </a></strong></p>
                                          
                                          <p>Greg Lindeblom, Nilo Marin, Ph.D., Behnoush Memari, Ph.D., and Christopher Johnston,
                                             Ph.D., 
                                             Browward College
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#PeterUsinger">"Tracking Pathways to Student Success" </a></strong></p>
                                          
                                          <p>Peter Usinger <br>
                                             Polk State College
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#KimberlyHardy">"Engaging Key Stakeholders in Assessing Student Learning Outcomes in Collegiate Life
                                                   and Co-Curricular Experiences" </a></strong></p>
                                          
                                          <p>Kimberly Hardy, Ph.D., and Lynne Crosby Ph.D., 
                                             Florida State College at Jacksonville
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#MarkMorgan">"Read to Succeed: Assessing Seminole State's QEP"</a></strong></p>
                                          
                                          <p>Mark Morgan, Ed.D. <br>
                                             Seminole State College of Florida
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#LynneCrosby">"Program Assessment Practices at Institutions Across the Southeast" </a></strong></p>
                                          
                                          <p>Lynne Crosby, Ph.D., and Susan Schultz, DNP, CNE, 
                                             Florida State College at Jacksonville
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#KatinaGothard">"General Education Asessment: One College's Journey" </a></strong></p>
                                          
                                          <p>Katina Gothard, Ph.D., and Jayne Gorham, Ed.D., 
                                             Eastern Florida State College
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#KristinAbel">"Creativity: Assessment in the Arts and Entertainment Fields"</a></strong></p>
                                          
                                          <p>Kristin Abel, Alan Gerber, Troy Gifford, D.M.A., Wendy Givoglu, and Kristy Pennino
                                             <br>
                                             Valencia College
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#LynneCrosby2">"Assessing Student Learning Outcomes Beyond the Classroom"</a></strong></p>
                                          
                                          <p>Lynne Crosby, Ph.D., and Kimberly Hardy, Ph.D. <br>
                                             Florida State College at Jacksonville
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#MarthaAmbrose">"Creating Faculty Assessment Leadership at Edison State College" </a></strong></p>
                                          
                                          <p>Martha Ambrose and Amy Trogran, Ph.D.<br>
                                             Edison State College 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong>State Assessment Meeting Featured Speaker<a href="Featured-Presenters.html"><br>
                                                   "What Should Students Know and Be Able to Do?: Connecting Meaningful Assessment with
                                                   Meaningful Questions "</a> </strong></p>
                                          
                                          <div> 
                                             
                                             <p><strong>Ashley Finley, Ph.D., Senior Director of Assessment and Research, </strong></p>
                                             
                                             <p><strong>The Association of American Colleges and Universities (AAC&amp;U) </strong></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#JudyJonesLiptrot">"Continuous Improvement. Is that Even Possible in a Demanding Environment" </a></strong></p>
                                          
                                          <p>Judy Jones-Liptrot, Ed.D., and Richard Turner, 
                                             Florida State College at Jacksonville
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="Featured-Presenters.html" target="_blank">(Workshop) "Common Ground: Using Rubrics to Create Dialogue" </a></strong></p>
                                          
                                          <p>Ashley Finley, Ph.D., Senior Director of Assessment and Research, The Association
                                             of American Colleges and Universities (AAC&amp;U)
                                          </p>
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#ChristinaHardin">"Reinventing the Process of Student-Learning Assessment in English Composition" </a></strong></p>
                                          
                                          <p>Christina Hardin, Ph.D.<br>
                                             Valencia College 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#AshleyCaron">"Using Business Intelligence to Enhance the Program Viability Report Process" </a></strong></p>
                                          
                                          <p>Ashley Caron and Maggie Tymms<br>
                                             St. Petersburg College
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#KatiePaschall">"Assessing Oral Communication: An Evolving Process"</a></strong></p>
                                          
                                          <p>Katie Paschall, Ph.D.<br>
                                             Edison State College
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong>Thank you for making this event a success. But you can make it better. </strong></p>
                                          
                                          <p><strong>Click here <a href="https://jfe.qualtrics.com/form/SV_4SdsZjmkUceAyCV" target="_blank"><img alt="Survey Icon" border="0" height="96" src="SurveyIcon.png" width="79"></a> to complete our online survey. </strong></p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#SusanSchultz2">"Trends in Outcomes and Plans of Academic Programs" </a></strong></p>
                                          
                                          <p>Susan Schultz, DNP, CNE; Jametoria Burton, M.L.I.S.; Janice Amos, MA.; and Derrick
                                             Johnson, M.Ed., 
                                             Florida State College at Jacksonville
                                          </p>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><a href="PresentationAbstracts.html#JesseCoraggio">"Using Business Intelligence to Improve the College Experience " </a></strong></p>
                                          
                                          <p>Jesse Coraggio, Ph.D., and Dan Gardner, Ph.D., 
                                             St. Petersburg College
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              
                              
                           </div>
                           
                           <p><a href="#top"> TOP</a></p></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2014/programs-electronic-presentations.pcf">©</a>
      </div>
   </body>
</html>