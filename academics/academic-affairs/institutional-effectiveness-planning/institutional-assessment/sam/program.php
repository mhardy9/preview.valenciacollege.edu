<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/program.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <span>
                           <h2>State Assessment Meeting<br>
                              June 2010<br>
                              Orlando, FL
                           </h2>
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Schedule-at-a-Glance</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="documents/AgendafortheStateMeetings-June2010.pdf">Click here for the 2010 agenda</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Presentations - <br>
                                             Power Points Now Available! Click on the Session Title for a PDF of the Power Point
                                             Presentation 
                                          </h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <h2> <u>Learning Outcomes Assessment</u>
                                             
                                          </h2>
                                          
                                          <p><strong><a href="documents/EdHolmesPresent.pdf">Assessing Student Learning in Student Affairs</a></strong> <em>-<br> 
                                                Edward Holmes, Valencia College</em></p>
                                          
                                          <p><strong>Ramping Up: Mapping Institutional Student Learning Outcomes to Programs and Courses
                                                for Accreditation and Beyond</strong> - <em>Jane E. Scott, Lake Sumter Community College</em></p>
                                          
                                          <p><strong><a href="documents/FacultyFriendlyOnlineAssessmentSystem_RHumerick.pdf">How to Make it Easy for Faculty to Document Embedded Assessment Activities and Use
                                                   the Results for Program Improvement</a></strong><em> - Rosiland Humerick, St. Johns River Community College </em></p>
                                          
                                          <p><strong>Transforming Academic Culture at MDC: Faculty Perspectives on Faculty Engagement Strategies
                                                and Lessons Learned</strong> <em>- <br>
                                                Sean Madison, Miami Dade College </em></p>
                                          
                                          <p><strong><a href="documents/RubricsforStateAssessmentConf_PNellis.pdf">Rubric Development</a></strong> <em>-<br> 
                                                Pat Nellis, Miami Dade College</em></p>
                                          
                                          
                                          <h2><u>SACS Accreditation</u></h2>
                                          
                                          <p><strong>Responding Effectively to SACS 5th Year Interim Reports</strong> - <em><br>
                                                Diane Calhoun-French, Jefferson Community and Technical College</em></p>
                                          
                                          <p><strong><a href="documents/SheeleyDCFPresentationFLCCAssessmentConference2010.pdf">Responding Meaningfully to the <em>Principles of Accreditation</em></a> -<br> 
                                                </strong><em>Steve Sheeley &amp; Diane Calhoun-French, Southern Association of Colleges and School;
                                                Jefferson Community and Technical College</em></p>
                                          
                                          <p><strong><a href="documents/FLCCsQEP2010sheeley.pdf">Developing Quality Enhancement Plans</a> - </strong><em><br>
                                                Steve Sheeley, Southern Association of Colleges and School</em></p>
                                          
                                          <p><strong><a href="documents/Learning_about_learnng_in_the_FoE_process_SDohany.pdf">Lessons Learned about Learning in the Foundation of Excellence Process</a></strong> –<br> 
                                             <em>Suzette Dohany &amp; Julie Alexander-Hamilton, Valencia College; The Policy Center on
                                                the First Year Experience </em></p>
                                          
                                          
                                          <h2><u>Data Collection and Interpretation</u></h2>
                                          
                                          <p><strong>Making Effective Use of CCSSE Data</strong> -<br> 
                                             <em>J
                                                eff Cornett, Valencia College - <strong>CANCELLED</strong></em></p>
                                          
                                          <p><strong><a href="documents/StateAssessmentMeeting-June2010-Survey_KEwen.pdf">Survey as a Tool for IE Data Collection</a></strong> - <em><br>
                                                Kurt Ewen, Valencia College</em></p>
                                          
                                          <p><strong><a href="documents/ClosingtheCommunicationGap_Holder_Ruengart.pdf">Closing the Communication Gap: Using Assessment Data to Enhance Student Learning and
                                                   Institutional Effectiveness</a></strong> -<br>
                                             <em>Julia Ruengert &amp; John Holder, Pensacola Junior College</em></p>
                                          
                                          <p><strong><a href="documents/stateassessmtg_firstlook_RBrown.pdf">A First Look at General Education Program Learning Outcomes Assessment Data</a></strong><em> -<br> 
                                                Roberta Brown, Valencia College</em></p>
                                          
                                          
                                          <h2><u>Career &amp; Technical Faculty Seminar</u></h2>
                                          
                                          <p><strong><a href="documents/StateAssMtgPLOs_Brown_Clarke_Dew.pdf">Developing and Assessing Program Learning Outcomes in Career and Technical Programs</a> -<br> 
                                                </strong><em> Helen Clark, Wendi Dew, and Roberta Brown, Valencia College</em></p>
                                          
                                          <h3>Facilitated  Discussions</h3>
                                          
                                          <p><a href="documents/PeterEwellarticle.pdf">Assessment, Accountability, and Improvement: Revisiting the Tension</a> 
                                          </p>
                                          
                                          <p><a href="documents/Ewell-StateAssessmentMeeting.pdf"><strong>Session Power Point Presentation</strong></a> 
                                          </p>
                                          
                                          <p>Article by: Peter T. Ewell </p>
                                          
                                          
                                          <p><a href="documents/janewellmanarticle.pdf">Connecting the Dots Between Learning and Resources</a></p>
                                          
                                          <p>Article by: Jane V. Wellman </p>
                                          
                                          
                                          <p><a href="documents/Banta.Griffin.Flateby.Kahnaritle.pdf">Three Promising Alternatives for Assessing College Students' Knowledge and Skills</a></p>
                                          
                                          <p>Article by: Trudy W. Banta, Merilee Griffin, Teresa L. Flateby, and Susan Kahn</p>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="#top">TOP</a></p></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/program.pcf">©</a>
      </div>
   </body>
</html>