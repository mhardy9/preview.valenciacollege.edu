<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2013-programs-electronic-presentations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <span>
                           <h2>State Assessment Meeting<br>
                              <em>June 20-21, 2013</em><br>
                              Orlando, FL
                           </h2>
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Schedule-at-a-Glance</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="documents/SAMAgenda_Final.pdf" target="_blank">State Assessment Meeting Agenda 2013 </a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Presentations <br>
                                             
                                          </h3>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="documents/Opening2013SAM.pptx">"Welcome"</a></p>
                           
                           <p>Dr. Laura Blasi<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="http://prezi.com/hukyksme6qbn/?utm_campaign=share&amp;utm_medium=copy&amp;rc=ex0share" target="_blank">"State Assessment Meeting  Opening Session:<br>
                                 The Purpose of Assessment is to Improve Learning
                                 "</a></p>
                           
                           <p>Kurt Ewen<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/Showing_the_Elephant_The_Door_State_Assessment_Meeting_2013.pdf" target="_blank">"Showing the Elephant the Door: Using Results"</a></p>
                           
                           <p>Ms. Barbara June Rodriguez and Dr. John Frederick<br>
                              Miami Dade College 
                           </p>
                           
                           
                           <p>"Intensive Workshop on Developing and Assessing Program Learning Outcomes" <br>
                              <a href="documents/WendiHandouts-Combined.pdf" target="_blank">Handouts</a> <br>
                              (Please contact us for the presentation)
                              
                           </p>
                           
                           <p>Ms. Wendi Dew and Dr. Laura Blasi<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/HowBITransformedtheCultureatSPCV2.pptx" target="_blank">"How Business Intelligence Transformed the Culture at St. Petersburg College"</a></p>
                           
                           <p>Dr. Jesse Coraggio and Dr. Dan Gardiner<br>
                              St. Petersburg College 
                           </p>
                           
                           
                           <p><a href="documents/OSD_StoryLearningAssessmentconference2013.pptx" target="_blank">"Division Action Plans: Putting Student Learning First"</a></p>
                           
                           <p>Dr. Deborah Larew<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/StateAssessmentMeeting2013-GenEdAssessmentModel.pptx" target="_blank">"A Model for Assessing General Education Utilizing an Online Institutional Assessment"</a> 
                           </p>
                           
                           <p>Ms. Maggie Tymms and Ms. Ashely Caron<br>
                              St. Petersburg College
                              
                           </p>
                           
                           
                           <p><a href="documents/ThePhoenixProject2.0.pptx" target="_blank">"The Phoenix Project: Helping Academically Distressed Students Succeed"</a></p>
                           
                           <p>Dr. Julie Corderman<br>
                              Valencia College
                           </p>
                           
                           
                           <p><a href="file:///http%7C//prezi.com/ew8uxjjcewa8/?utm_campaign=share&amp;utm_medium=copy&amp;rc=ex0share%20" target="_blank">"Discussion: Changing Institutional Culture to Promote Assessment of HIgher Learning
                                 (NILOA Reading)"</a><br>
                              <a href="documents/NILOAKurtEwenReadingSessionoccasionalpaperseventeen.pdf" target="_blank">Recommended Reading</a></p>
                           
                           <p>Kurt Ewen<br>
                              Valencia College
                           </p>
                           
                           
                           <p><a href="documents/UsingDatatoImproveStudentLearning.ppt">"Strategies to Improve the Use of Data in Decision Making"</a></p>
                           
                           <p>Kurt Ewen<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/FinalSelfAssessment-presentationselfassessment6-13-2013.pptx">"Student Engagement Through Self-Assessment: Research-based Strategies- A Panel Discussion"</a></p>
                           
                           <p>Dr. Laura Blasi, Ms. Donna Colwell, Ms. Kathleen Marquis, Mr. Russell Takashima<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="http://www.vuvox.com/collage/detail/06bbdb8723" target="_blank">"Work in Progress: Learning Outcomes Assessment Within the Library"</a></p>
                           
                           <p>Ms. Diane Dalrylmple, Ms. Regina Seguin, and Ms. Beth King<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/SpeechAssessmentDay2013.pptx" target="_blank">"Work in Progress: Learing Outcomes Assessment Within Speech"</a></p>
                           
                           <p>Ms. Tina Tan and Ms. Liza Schellpfeffer<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="http://prezi.com/bbfz1eu8lafv/?utm_campaign=share&amp;utm_medium=copy" target="_blank">"Keeping the Assessment Fires Burning: The QEP Beyond the Five-Year Impact Report"</a></p>
                           
                           <p>Dr. Alex Perez and Ms. Alissa Sustarsic<br>
                              Lake-Sumter State College 
                           </p>
                           
                           
                           <p><a href="documents/Styles.ppt" target="_blank">"Work in Progress: Learning Outcomes Assessment Within Humanities"</a></p>
                           
                           <p>Ms. Karen Styles<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/MappingPLOsToCurriculum.pptx" target="_blank">"Mapping Program Learning Outcomes to Curriculum"</a></p>
                           
                           <p>Mr. Andy Ray<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/StateAssessmentMeeting-fox.pptx" target="_blank">"Supporting Faculty Assessment of Student Learning Online: Using Quality Matters to
                                 Sterngthen Online Teaching and Learning" </a></p>
                           
                           <p>Ms. Erin O'Brien and Mr. Charles Fox<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/DecisionMaking-Boileau.pptx" target="_blank">"Valencia College Student Affairs Major Decision Survey"</a></p>
                           
                           <p>Ms. Danielle Boileau<br>
                              Valencia College
                              
                           </p>
                           
                           
                           <p><a href="documents/usingactionreserach.pptx" target="_blank">"Using Action Research to Advance Planning and Learning"</a></p>
                           
                           <p>Dr. Lisa Armour and Ms. Wendi Dew<br>
                              Santa Fe State College and Valencia College 
                           </p>
                           
                           
                           <p><a href="documents/Psychologysgenedourcome.pptx" target="_blank">"Psychology's General Education Outcome: Improving Throught Assessment" </a></p>
                           
                           <p>Dr. Barbara Van Horn<br>
                              Indian River State College
                              
                           </p>
                           
                           
                           <p><a href="documents/TrackingPathwaystoSuccess2013-Usinger.pptx" target="_blank">"Tracking Pathways to Success"</a></p>
                           
                           <p>Mr. Peter Usinger<br>
                              Polk State College
                              
                           </p>
                           
                           
                           <p><a href="documents/GetReadyforCollege-MathPresentationforStateAssessmentMeetingFINAL.pptx" target="_blank">"Opportunites and innovations: Assessing Developmental Education Going Forward- Exploring
                                 an Example from a MOOC"</a><br>
                              <a href="documents/DJenkins_RethinkingDevEdasOn-ramp_FLAssessmentMeeting_June2013.pptx" target="_blank">"Opportunites and innovations: Assessing Developmental Education Going Forward- Exploring
                                 an Example from a MOOC- St. Petersburg College "</a>            
                           </p>
                           
                           <p>Dr. Karen Borglum, Dr. Davis Jenkins, Dr. Carol Weideman<br>
                              Valencia College, Columbia University, St. Petesburg College 
                           </p>
                           
                           
                           <p><a href="http://prezi.com/nbuuzdodvzry/?utm_campaign=share&amp;utm_medium=copy" target="_blank">"Work in Progress: Learning Outcomes Assessment within English and English for Academic
                                 Purposes"</a></p>
                           
                           <p>Dr. Christina Hardin and Ms. Marcelle Cohen<br>
                              Valencia College
                              
                           </p>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <p><a href="#top"> TOP</a></p></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2013-programs-electronic-presentations.pcf">©</a>
      </div>
   </body>
</html>