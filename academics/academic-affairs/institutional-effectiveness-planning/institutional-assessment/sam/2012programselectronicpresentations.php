<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2012programselectronicpresentations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <span>
                           <h2>State Assessment Meeting<br>
                              <em>June 2012</em><br>
                              Orlando, FL
                           </h2>
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Schedule-at-a-Glance</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="documents/StateAssessmentMeetingAgenda2012Final06042012JWOOD_000.pdf">Click here for the 2012 agenda</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Presentations - <br>
                                             Power Points Now Available! Click on the Session Title for a PDF of the Power Point
                                             Presentation 
                                          </h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><a href="documents/AssessmentMeetingHandout_Perez.pdf">"Transforming Faculty through Implementation, Assessment and Continuous Improvement
                                                of a Developmental Education QEP" </a><br>
                                             <br>
                                             Alexander M. Perez, Ed.D., QEP Program Manager for Developmental Studies<br>
                                             Lake-Sumter Community College 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/HB7135Webinar_042012.pdf">"An Open Discussion of House Bill 7135 and It's Implications for Learning Asse4ssment
                                                   and Reaffirmation"<br>
                                                   </a><a href="documents/Borglum2.pdf">Supplemental Document                      </a></p>
                                             
                                             <p>Karen Borglum, Ed.D., AVP Curriculum &amp; Articulation<br>
                                                Valencia College
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/TrackingPathwaystoSuccess-Usinger.pdf">"Pathways to Success: The Role of Individual Learning Success Factors" </a></p>
                                             
                                             <p>Peter A. Usinger, Director of Institutional Research, Effectiveness, and Planning<br>
                                                Polk State College
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/StudentWithdrawalRevisited-Usinger.pdf">"Student Withdrawal Revisited: An Exit-Survey Approach" </a></p>
                                             
                                             <p>Peter A. Usinger, Director of Institutional Research, Effectiveness, and Planning<br>
                                                Polk State College 
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/CultivatingAwareness-CrosbySleap.pdf">"Cultivating Institutional Awareness and Monitoring of Substantive Change Activities"
                                                   </a></p>
                                             
                                             <p>Lynne S. Crosby, Ph.D., Director of Institutional Effectiveness and Accreditation
                                                &amp; <br>
                                                Naomi Sleap, Project Coordinator, The Office of Institutional Effectiveness and Accreditation<br>
                                                Florida State College at Jacksonville
                                             </p>
                                             
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/InformationLiteracyAcrosstheCurriculum-Seguin.pdf">"Information Literacy Across the Curriculum: A Case Study"</a></p>
                                             
                                             <p>Regina Seguin, Librarian<br>
                                                Valencia College 
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             <p><a href="documents/BacktoBasicsSACSPresFINAL-Staley.pdf">"Back to Basics: Evolving Program Student Learning Outcomes from Florida Curriculum
                                                   Frameworks" </a></p>
                                             
                                             <p>Michael Staley, Dean, School of Engineering, Design and Construction<br>
                                                Seminole State College
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="#top">TOP</a></p></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/2012programselectronicpresentations.pcf">©</a>
      </div>
   </body>
</html>