<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional AssessmentState Assessment Meeting | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/speakers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional AssessmentState Assessment Meeting</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/">Sam</a></li>
               <li>Institutional AssessmentState Assessment Meeting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>State Assessment Meeting<br>
                           <em><strong>June 2010 </strong></em>
                           
                        </h2>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Keynote Speakers</div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Diane Calhoun-French</p>
                                    
                                    <p>Provost and Vice President for Academic and Student Affairs</p>
                                    
                                    <p>Jefferson Community &amp; Technical College</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Teresa (Terri) Flateby</p>
                                    
                                    <p>T.L. Flateby &amp; Associates</p>
                                    
                                    <p>Assessment Consulting Firm </p>
                                    
                                    <p><a href="documents/TeresaFlatebybio.pdf">Biographical Summary</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Dr. Steven M. Sheeley</p>
                                    
                                    <p>Vice President, Commission on Colleges</p>
                                    
                                    <p>Southern Association of Colleges and Schools </p>
                                    
                                    <p><a href="documents/sheeleybio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Concurrent Session Speakers </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Roberta Brown</p>
                                    
                                    <p>Assistant Director, Learning Assessment</p>
                                    
                                    <p>Valencia College</p>
                                    
                                    <p><a href="documents/RobertaBrownbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Helen Clarke</p>
                                    
                                    <p>Director, Teaching/Learning Academy</p>
                                    
                                    <p>Valencia College</p>
                                    
                                    <p><a href="documents/HelenClarkbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Jeff Cornett</p>
                                    
                                    <p> Managing Director, Institutional Research </p>
                                    
                                    <p>Valencia College </p>
                                    
                                    <p><a href="documents/JeffCornettbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Wendi Dew</p>
                                    
                                    <p>Academic Coordinator, Faculty Development</p>
                                    
                                    <p>Valencia College </p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Kurt Ewen</p>
                                    
                                    <p>Assistant Vice President, Assessment &amp; Institutional Effectiveness</p>
                                    
                                    <p>Valencia College</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Edward Holmes</p>
                                    
                                    <p>Counselor</p>
                                    
                                    <p>Valencia College</p>
                                    
                                    <p><a href="documents/EdwardHolmesbio.pdf">Biographical Summary </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Rosiland Humerick</p>
                                    
                                    <p> Vice President, Research and Institutional Effectiveness</p>
                                    
                                    <p> St. Johns River Community College</p>
                                    
                                    <p><a href="documents/RosalindHumerickbio.pdf">Biographical Summary </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Sean Madison</p>
                                    
                                    <p>Director of Learning Outcomes Assessment, Institutional Effectiveness</p>
                                    
                                    <p>Miami Dade College</p>
                                    
                                    <p><a href="documents/SeanMadisonbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Pat Nellis </p>
                                    
                                    <p>District Director, College Training and Development</p>
                                    
                                    <p>Miami Dade College </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Julia Ruengert</p>
                                    
                                    <p> Director, Center for Teaching and Learning </p>
                                    
                                    <p>Pensacola Junior College  </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Jane Scott</p>
                                    
                                    <p>Manager, Student Learning Outcomes &amp; Project Manager, eLumen</p>
                                    
                                    <p>Lake-Sumter Community College </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Panelists</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    
                                    <p>John Frederick</p>
                                    
                                    <p> Assistant Professor of Speech Communication </p>
                                    
                                    <p>Miami Dade College</p>
                                    
                                    <p><a href="documents/JohnFrederickbio.pdf">Biographical Summary</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> 
                                    
                                    <p>Isabel Rodriguez-Dehmer</p>
                                    
                                    <p>Senior Associate Professor of Reading</p>
                                    
                                    <p>Miami Dade College</p>
                                    
                                    <p><a href="documents/IsabelRodriguezbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Sandra Schultz </p>
                                    
                                    <p> Professor of Biology, Health, and Wellness </p>
                                    
                                    <p>Miami Dade College</p>
                                    
                                    <p><a href="documents/SandraSchultzbio.pdf">Biographical Summary</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/speakers.pcf">©</a>
      </div>
   </body>
</html>