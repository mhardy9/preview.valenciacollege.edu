
<ul>
<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/index.php">Institutional Assessment</a>
<ul>
		<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/Calendar.php">Calendar &amp; Tools</a></li>
		<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/FacultyWorkshops.php">Workshops</a></li>
		<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/fg/index.php">Focus Groups</a></li>
		<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/SurveySupport.php">Survey Support</a></li>
 </ul>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/MeetTheTeam.php">Meet the Team</a></li>	
	<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/index.php">Learning Outcomes Assessment (LOA)</a>
<ul>
		<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/LOA/assessment_plans.php">Assessment Plans</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/outcomes_GenEd.php">General Education</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/qep.php">Quality Enhancement Plan (QEP)</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/oo/index.php">Tools</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Resources.php">Resources</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/Examples.php">Examples</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/forms_reports.php">Forms, Reports, &amp; More</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/program_outcomes.php">Degree and Career Program Outcomes</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/dt/index.php">Data Team</a></li>
 </ul>
	
	<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/index.php">Student Feedback Survey</a>
<ul>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/SAICalendar.php">Calendar &amp; Tools</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/SAIReports.php">Reports</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/ResourcesforUsers.php">Resources</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/LoginHere.php">Sign In Here</a></li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/saicc/SFIFrequentlyAskedQuestions.php">Frequently Asked Questions</a></li>
<li><a href="ccsse.php">CCSSE Survey</a></li>
 </ul>	
		
<li class="submenu"><a class="show-submenu" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/surveys/index.php">Planning</a>
<ul>

	
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/sam/index.php">State Assessment Meeting</a></li>
 </ul>			
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/AspenPrizeApplication.php">Aspen Prize</a></li>
		
</ul>

