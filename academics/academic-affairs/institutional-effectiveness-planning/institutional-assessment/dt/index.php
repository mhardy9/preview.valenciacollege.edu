<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Assessment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/dt/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/">Institutional Assessment</a></li>
               <li>Dt</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h3><em>Data Team Gatherings at Valencia College and Related Principles</em></h3>
                        
                        <p>Data Team members support the effective use of data consistent with a collaborative
                           culture dedicated to inquiry, results, and excellence.<strong> </strong>Building on the lessons learned about data collection and processing from our Achieving
                           the Dream (AtD) efforts, Data Team members enable faculty and staff across the college
                           to make data-supported decisions concerning student learning, student success, and
                           student engagement.<strong> </strong>Along with other data-related activities, the Data Team helps faculty and student
                           affairs professionals to collect, process, and report data associated with the college’s
                           learning outcomes assessment efforts. 
                        </p>
                        
                        
                        <p>Currently we have Data Teams organized to support college-wide initiatives (such as
                           the New Student Experience – NSE), grant-funded initiatives (such as the LSAMP program
                           funded by the National Science Foundation – NSF), and for program evaluations (also
                           known as Academic Initiative Reviews.) Attached you will find PowerPoint template
                           text to use / adapt as you are organizing a Data Team at Valencia and other resources.
                           The data teams are supported by Valencia’s Institutional Assessment (VIA) office and
                           the Office of Institutional Research (IR.)
                        </p>             
                        
                        <hr>             
                        <p><strong>Data Team Principles</strong></p>
                        
                        <ul>
                           
                           <li>Create knowledge essential to future decision making regarding program expansion and/or
                              development<strong></strong>
                              
                           </li>
                           
                           <li>Design and select research questions that will effectively guide our work<strong></strong>
                              
                           </li>
                           
                           <li>Consider diverse methods of data collection/generation, including both quantitative
                              and qualitative methods<strong></strong>
                              
                           </li>
                           
                           <li>Collaborate with each other, seeking to understand diverse perspectives<strong></strong>
                              
                           </li>
                           
                           <li>Endeavour to identify and overcome bias based on our current involvement in this work,
                              seeking to guide this research objectively<strong></strong>
                              
                           </li>
                           
                           <li>Consider formative and summative assessment <strong></strong>
                              
                           </li>
                           
                           <li>Determine appropriate audiences for sharing our work&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6-4-2014<strong></strong>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/DataTeamSamplePPTSlidestoAdapt5-11-2015Blasi.pptx" target="_blank">Data Team Basics at Valencia</a> (template text in slides to adapt)
                        </p>
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/DraftGuidelinesforReportingAssessmentDataatValencia.7-1-15.docx" target="_blank">Draft Guidelines for Reporting Assessment Data 7-2-2015</a></p>
                        
                        <p>This is a working document that has developed out of lessons learned when reporting
                           for the New student Experience (NSE). These guidelines are expected to grow with the
                           College Data Team once it gathers in the fall. These guidelines are focused on improving
                           the ways we report learning assessment data. They are not a set of directions for
                           the Institutional Research (IR) office and this document does not lay out a college-wide
                           framework for presenting data.
                        </p>
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/Draft-ProjectManagementTool-DataTeam-forchairs-midtermcheck-in4-20-2015.docx" target="_blank">Project Management Tool for Data Team Chairs</a> (mid-term check-in - audit of process) 
                        </p>
                        
                        <p><a href="http://www.jff.org/publications/calculating-cost-return-investments-student-success" target="_blank">Example Cost Effectiveness Resource</a> (<a href="http://www.jff.org/publications/calculating-cost-return-investments-student-success">http://www.jff.org/publications/calculating-cost-return-investments-student-success</a> )
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">                   
                                    <h3><strong>Sound Practices in Data Sharing and Use</strong></h3>
                                    
                                    <p>The use of data at Valencia College complies with FERPA regulations (<a href="http://www.valenciacollege.edu/FERPA" target="_blank">www.valenciacollege.edu/FERPA</a>) as well as regulations related to Institutional Review Board review (<a href="http://www.valenciacollege.edu/IRB" target="_blank">www.valenciacollege.edu/IRB</a>). &nbsp;
                                    </p>                   
                                    <p>User access is restricted for data files that include student identification (such
                                       as IDs.) When requested by authorized external users (such as the federal government)
                                       files are sent using data encryption and password protection.
                                    </p>                   
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>             
                        
                        
                        <p><strong>Archives:</strong></p>
                        
                        <p><strong>Members</strong></p>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><font color="#000000">Name</font></div>
                                    
                                    <div data-old-tag="td"><font color="#000000">Representing</font></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Alys Arceneaux</div>
                                    
                                    <div data-old-tag="td">
                                       <div>Institutional Research </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Leonard Bass </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Learning Support </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Nicholas Bekas </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Academic Affairs, West </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Laura Blasi </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Instiutional Assessment </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Karen Borglum </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Curriculum and Articulation </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Mike Bosley </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Executive Dean, Lake Nona </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Lucy Boudet </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Marketing and Strategic Communications </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Robyn Brighton </div>
                                    
                                    <div data-old-tag="td">
                                       <div>LinC</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Roberta Carew </p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Faculty, Mathematics </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Tanisha Carter </p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>College Transitions Programs</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Kristeen Christian</div>
                                    
                                    <div data-old-tag="td">
                                       <div>Resource Development </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Daryl Davis </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Institutional Research</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Kurt Ewen</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Assessment &amp; Institutional Effectiveness </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Michelle Foster </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Academic Affairs, East </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Al Groccia </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Faculty, Mathematics </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Jonathan Hernandez </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Academic Affiars</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Stacey Johnson </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Campus President, East and Winter Park </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Sonya Joseph</div>
                                    
                                    <div data-old-tag="td">
                                       <div>Student Affairs</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Donna Kosloski </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Institutional Research </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Maryke Lee </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Academic Affairs, Mathematics </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Michele McArdle </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Executive Dean, Winter Park </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Boris Nguyen </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Faculty, Mathematics </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Cathy Penfold Navarro</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Student Success &amp; Transfer Readiness</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Kathleen Plinske</div>
                                    
                                    <div data-old-tag="td">
                                       <div>Campus President, Osceola</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Karen Reilly </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Learning Support </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Joyce Romano </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Student Affairs</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Larry Rosen </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Institutional Research </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Upasana Santra </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Faculty, Mathematics </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Landon Shephard </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Learning Support </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>David Sutton </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Academic Affairs, Humanities, and Foreign Language </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Russell Takashima </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Faculty, Mathematics </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Falecia Williams </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Campus President, West </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>             
                        <p><strong>Past Meetings &amp; Minutes</strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3><font color="white"><strong>Date</strong></font></h3>
                                 </div>
                                 
                                 <div data-old-tag="td"><font color="white">Agendas</font></div>
                                 
                                 <div data-old-tag="td">
                                    <h3><font color="white"><strong>Minutes</strong></font></h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>July 7, 2010 </div>
                                 </div>
                                 
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/20100707DataTeamMeetingNotes.pdf" target="_blank"> Minutes</a> 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">January 25, 2010</div>
                                 
                                 
                                 <div data-old-tag="td">
                                    <a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/DataTeamMeetingNotes20100125.pdf" target="_blank">Minutes</a> 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">August 31, 2011 </div>
                                 
                                 
                                 <div data-old-tag="td">Minutes</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">September 28, 2011 </div>
                                 
                                 
                                 <div data-old-tag="td">Minutes</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">October 26, 2011 </div>
                                 
                                 
                                 <div data-old-tag="td">Minutes</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">March 28, 2011 </div>
                                 
                                 
                                 <div data-old-tag="td">Minutes</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">July 9, 2012 </div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/DataTeamAgenda7-9-2012v1.pdf" target="_blank">Agenda</a></div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/DataTeamMinutes7-9-2012draftsent7-22-2012v2.pdf" target="_blank">Minutes</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">August 29, 2012 </div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/CDTAgenda8-29-12.pdf" target="_blank">Agenda</a></div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/CDTMinutes8-29-12.pdf" target="_blank">Minutes</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">September 26, 2012 </div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/CDTAgenda9-26-12Final.pdf" target="_blank">Agenda</a></div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/CDTMinutes9-26-12.pdf" target="_blank">Minutes</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">November 28, 2012 </div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/CDTAgenda11-28-12.pdf" target="_blank">Agenda</a></div>
                                 
                                 <div data-old-tag="td">Minutes</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                          
                        <p><strong>Documents &amp; Presentations</strong></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/AcademicAssemblyPosterswithtables.pdf" target="_blank">Academic Assembly Posters 2009-2010 </a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/FromDatatoMeaningfulInformation-FirstYearExperienceConference2009.pdf" target="_blank">From Data to Meaningful Information - First Year Experience Conference 2009 </a></p>
                        
                        <p><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-assessment/DT/documents/FromStrategytoInstitutionalization-ConnectionsConference2009.pdf" target="_blank">From Strategy to Institutionalization - Connections Conference 2009 </a></p>
                        
                        <p>Note: If you are interested in using any of the data/presentations listed above, please
                           contact <a href="mailto:rbrown75@valenciacollege.edu">Roberta Brown.</a></p>                          
                        <h2>
                           
                           
                        </h2>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>IMPORTANT LINKS</h3>
                        
                        
                        <a href="../documents/Learning%20Assessment%20Data%20Definitions-rev6-18-15.pdf" target="_blank" title="Glossary"><img alt="Glossary" border="0" height="60" src="glossary.jpg" title="Click to View Glossary" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-assessment/dt/index.pcf">©</a>
      </div>
   </body>
</html>