<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Southern Association of Colleges and Schools Commission on Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/approval-process.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Southern Association of Colleges and Schools Commission on Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/">Sacs Regional Accreditation</a></li>
               <li>Southern Association of Colleges and Schools Commission on Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>SACS COC Procedures</h2>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/SACSCOCSubstantiveChangePolicyProcedure1.pdf">SACSCOC Substantive Change Policy - Procedure 1</a> 
                           </li>
                           
                           <li>
                              <a href="documents/SACSCOCSubstantiveChangePolicyProcedure2.pdf">SACSCOC Substantive Change Policy - Procedure 2</a> 
                           </li>
                           
                           <li><a href="documents/SACSCOCSubstantiveChangePolicyProcedure3.pdf">SACSCOC Substantive Change Policy - Procedure 3 </a></li>
                           
                        </ul>            
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>Valencia's Approval Process Table </h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Types of Change</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Procedure</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Prior Notification Required</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Prior Approval Required</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Time Frame<br>
                                          for Contacting COC</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Time Frame for Contacting Valencia's Liaison</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>SACSCOC</strong></p>
                                    
                                    <p><strong>Documentation</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th"> 
                                    <p><strong>Primary Departments</strong></p>
                                    
                                    <p><strong>Responsible for monitoring and / or documentation </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Initiating coursework or programs at a different level than currently approved</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Application for Level<br>
                                       Change<br>
                                       Due dates: April 15 or<br>
                                       September 15
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Expanding at current degree level <em>(significant departure from current programs)</em></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Initiating a branch campus (See definition of “branch campus” on p. 3 of this document.)<strong></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Initiating a certificate program at employer’s request and on short notice</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…using existing approved courses</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                    
                                    <p><strong>Note: </strong>Notification to Collegewide Committee required
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…at a new off-campus site (previously approved program)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Modified prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…that is a significant departure from previously approved programs</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Approval required prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Modified prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Initiating other certificate programs</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…using existing approved courses</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                    
                                    <p><strong>Note: </strong>Notification to Collegewide Committee required
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…at a new off-campus site (previously approved program)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…that is a significant departure from previously approved programs</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Altering significantly the educational mission of the institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Contact Commission Staff<br>
                                       (<em>Also see page 16, item 9</em>)
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Initiating joint or dual degrees with another institution: <u><a href="documents/SACSCOCAgreementsInvolvingDualandJointAwards.pdf">(See: “AgreementsInvolving JointandDualAcademic Awards.”)</a></u> </strong></p>
                                    
                                    <p><strong>        Joint Programs:</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…with another SACSCOC accredited institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Copy of signed agreement and contact information for each institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>… with an institution not accredited by SACSCOC</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Dual Programs</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>No</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Copy of signed agreement and contact information for each institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Initiating off-campus sites (including Early College High School and dual enrollment
                                          programs offered at the high school)</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Student can obtain<br>
                                       50% or more credits toward program
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Student can obtain 25-<br>
                                       49% of credit<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Letter of notification</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Student can obtain<br>
                                       24% or less
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Expanding program offerings at previously approved off-campus sites</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Adding programs that are significantly different from current programs <u>at</u> <u>the site</u></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                    
                                    <p><strong>Note: </strong>Notification to Collegewide Committee required
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Adding programs that are NOT significantly different from current programs <u>at the site</u></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                    
                                    <p><strong>Note: </strong>Notification to Collegewide Committee required
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Altering significantly the length of a program</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Career and Workforce Education / Campus President
                                       / Campus Dean of Academic Affairs
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Initiating distance learning…</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Offering 50% or more of a program <u>forthefirst</u> <u>time</u></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Offering 25-49%</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>No</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Letter of notification</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Offering 24% or less</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                    
                                    <p><strong>Note: </strong>Notification to Collegewide Committee required
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Initiating programs or courses offered through contractual agreement or consortium</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Letter of notification and copy of signed agreement</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Entering into a contract with an entity not certified to participate in USDOE Title
                                          IV programs</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…if the entity provides<br>
                                       25% or more of an educational program offered by the COC accredited institution
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…if the entity provides less than 25% of an educational program offered by the COC
                                       accredited institution
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Copy of the signed agreement</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Curriculum and Articulation</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Initiating a merger/consolidation with another institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See SACSCOC policy <span><strong><u><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf">“Mergers,</a></u></strong><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf"><br>
                                             <strong><u>Consolidations</u> <u>andChangeof</u> <u>Ownership:</u> <u>Reviewand</u> <u>Approval.”</u></strong></a></span></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus<br>
                                       Due dates: April 15 or<br>
                                       September 15
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Changing governance, ownership, control, or legal status of an institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See SACSCOC policy <strong><u><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf">“Mergers,</a></u></strong><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf"><br>
                                          <strong><u>Consolidations</u> <u>andChangeof</u> <u>Ownership:</u> <u>Reviewand</u> <u>Approval.”</u></strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus<br>
                                       Due dates: April 15 or<br>
                                       September 15
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Relocating a main or branch campus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Moving an off-campus instructional site (serving the same geographic area)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision to move
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Letter of notification with new address and starting date</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Changing from clock hours to credit&nbsp; hours</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Justify reasons for change, indicate calculation of equivalency, and other pertinent
                                       information
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Altering significantly the length of a program</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Career and Workforce Education / Campus President
                                       / Campus Dean of Academic Affairs
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Initiating degree completion programs</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>NA</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Career and Workforce Education / Campus President
                                       / Campus Dean of Academic Affairs
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Closing a program, approved off-campus site, branch campus, or institution</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Institution to teach out its own students</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>3</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision to close
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See Valencia’s Program Planning and Development Manual</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Description of teach-out plan included with letter of notification</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Career and Workforce Education</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>…Institution contracts with another institution to teach-out students (Teach-out Agreement)</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>3</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision to close
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Immediately following<br>
                                       decision to close
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Description of teach-out plan, copy of signed teach-out agreement detailing terms
                                       included with notification
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / AVP, Career and Workforce Education</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Acquiring any program or site from another institution</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See SACSCOC policy <strong><u><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf">“Mergers,</a></u></strong><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf"><strong><u><br>
                                                Consolidations andChangeof Ownership: Reviewand Approval.”</u></strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning / Campus President / Campus Dean of Academic Affairs</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Adding a permanent location at a site where the institution is conducting a teach-out
                                       for students from another institution that is closing
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>See SACSCOC policy <strong><u><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf">“Mergers,</a></u></strong><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf"><strong><u><br>
                                                Consolidations andChangeof Ownership: Reviewand Approval.”</u></strong></a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Yes</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6 months</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>8 months prior to implementation</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Prospectus</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>VP, Academic Affairs and Planning</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/approval-process.pcf">©</a>
      </div>
   </body>
</html>