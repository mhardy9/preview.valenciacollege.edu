<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Southern Association of Colleges and Schools Commission on Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Southern Association of Colleges and Schools Commission on Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/">Sacs Regional Accreditation</a></li>
               <li>Southern Association of Colleges and Schools Commission on Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <p>Valencia College is accredited by the Commission on Colleges of the Southern Association
                           of Colleges and Schools to award Associate and Bachelor’s degrees. Contact the Commission
                           on Colleges at 1866 Southern Lane, Decatur, Georgia 30033-4097 or call 404-679-4500
                           for questions about the accreditation of Valencia College. 
                        </p>
                        
                        
                        <p>The Commission’s address and contact numbers are provided to enable interested constituents
                           to (1) learn about the accreditation status of the institution, (2) file a third-party
                           comment at the time of the institution’s decennial review, or (3) file a complaint
                           against the institution for alleged non-compliance with a standard or requirement.
                           General inquiries about the college, such as admission requirements, financial aid,
                           and educational programs should be addressed directly with the appropriate department
                           at Valencia College. 
                        </p>
                        
                        
                        <p>The Office of Curriculum &amp; Assessment provides leadership, support, and resources
                           for Valencia’s regional accreditation with SACSCOC. These processes assist the College
                           in maintaining SACSCOC accreditation, promoting the achievement of its mission and
                           goals, and fostering continuous improvement of programs and services. For any questions
                           related Valencia College and its accreditation with SACSCOC, please contact <strong>Karen Borglum, AVP for Curriculum &amp; Assessment.</strong></p>
                        
                        
                        
                        <p><strong>SACSCOC Collegewide Oversight Committee:</strong></p>
                        
                        <p><strong>Dr. Karen M. Borglum </strong></p>
                        
                        <p><strong>SACSCOC Liaison </strong></p>
                        
                        <p>AVP for Curriculum &amp; Assessment</p>
                        
                        <p><strong>Dr. Michael Bosley</strong></p>
                        
                        <p>Executive Dean, Lake Nona Campus</p>
                        
                        <p><strong>Dr. Nicholas Bekas</strong></p>
                        
                        <p>Campus Dean, West Campus</p>
                        
                        <p><strong>Ms. Michelle Foster</strong></p>
                        
                        <p>Campus Dean, East Campus</p>
                        
                        <p><strong>Mr. Daniel Diehl</strong></p>
                        
                        <p>Asst Dir, Central FL Fire Inst, Fire Science SPS </p>
                        
                        <p><strong>Ms. Erica Reese </strong></p>
                        
                        <p>Director, Standardized Testing</p>
                        
                        <p><strong>Ms. Noelia Maldonado Rodriguez</strong></p>
                        
                        <p>Planning Support Specialist </p>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3> Contact Information </h3>
                        
                        <p><strong><a href="../../../contact/Search_Detail2.cfm-ID=kborglum.html">Dr. Karen Borglum</a></strong></p>
                        
                        <p><strong>SACSCOC Institutional Liaison</strong></p>
                        
                        <p>Assistant Vice President</p>
                        
                        <p>Curriculum and Assessment</p>
                        
                        <p>Valencia College</p>
                        
                        <p>1768 Park Center Drive</p>
                        
                        <p>Orlando, FL 32835</p>
                        
                        <p>Mail code: DO-30</p>
                        
                        <p>(office) 407-582-3455; 
                           (fax) 407-582-3424
                        </p>
                        
                        <p><a href="mailto:kborglum@valenciacollege.edu">kborglum@valenciacollege.edu</a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/default.pcf">©</a>
      </div>
   </body>
</html>