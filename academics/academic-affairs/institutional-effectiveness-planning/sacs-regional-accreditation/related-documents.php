<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Southern Association of Colleges and Schools Commission on Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/related-documents.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Southern Association of Colleges and Schools Commission on Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/">Sacs Regional Accreditation</a></li>
               <li>Southern Association of Colleges and Schools Commission on Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Related Documents</h2>
                        
                        <p><strong>SACS COC </strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/SACSCOCSubstantiveChangePolicyasof01-2015.pdf">Substantive Change Policy</a></li>
                           
                           <li><a href="documents/SACSCOCProspectusSubstantiveChangeRevised07-2014.pdf">Substantive Change Prospectus</a></li>
                           
                           <li><a href="documents/SACSCOCAgreementsInvolvingDualandJointAwards.pdf">Agreements Involving Dual and Joint Awards</a></li>
                           
                           <li><a href="documents/SACSCOCApplicationforMemberInstitutions.pdf">Application for Member Institutions</a></li>
                           
                           <li><a href="documents/SACSCOCFacultyRosterForm.pdf">Faculty Roster Form</a></li>
                           
                           <li><a href="documents/SACSCOCInstructionsforReportingtheQualificationsofFullTimeandPartTimeFaculty.pdf">Instructions for Reporting the Qualifications of Full Time and Part Time Faculty</a></li>
                           
                           <li><a href="documents/SACSCOCMERGERSCONSOLIDATIONSCHANGEOFOWNERSHIPACQUISITIONS.pdf">Mergers, Consolidations, Change of Ownership, Acquisitions, and Change of Governance,
                                 Control, Form or Legal Status</a></li>
                           
                           <li><a href="documents/SACSCOC2012PrinciplesOfAcreditation.pdf">Principles of Accreditation, <em>Revised 2012</em></a></li>
                           
                           <li><a href="documents/SACSCOCReaffirmationOfAccreditationandSubssequentReports.pdf">Reaffirmation of Accreditation and Subsequent Reports</a></li>
                           
                           <li>
                              <a href="documents/SACSCOCSubstantiveChangePolicyProcedure1.pdf">SACSCOC Substantive Change Policy - Procedure 1</a> 
                           </li>
                           
                           <li>
                              <a href="documents/SACSCOCSubstantiveChangePolicyProcedure2.pdf">SACSCOC Substantive Change Policy - Procedure 2</a> 
                           </li>
                           
                           <li><a href="documents/SACSCOCSubstantiveChangePolicyProcedure3.pdf">SACSCOC Substantive Change Policy - Procedure 3 </a></li>
                           
                        </ul>
                        
                        
                        <p><strong>Valencia College</strong></p>
                        
                        <ul>
                           
                           <li><a href="documents/ValenciaCollegePolicy6Hx28401-Accreditation.pdf">Valencia College Policy 6Hx28401 - Accreditation</a></li>
                           
                        </ul>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3> Contact Information </h3>
                        
                        <p><strong><a href="../../../contact/Search_Detail2.cfm-ID=kborglum.html">Dr. Karen Borglum</a></strong></p>
                        
                        <p><strong>SACSCOC Institutional Liaison</strong></p>
                        
                        <p>Assistant Vice President</p>
                        
                        <p>Curriculum and Assessment</p>
                        
                        <p>Valencia College</p>
                        
                        <p>1768 Park Center Drive</p>
                        
                        <p>Orlando, FL 32835</p>
                        
                        <p>Mail code: DO-30</p>
                        
                        <p>(office) 407-582-3455; 
                           (fax) 407-582-3424
                        </p>
                        
                        <p><a href="mailto:kborglum@valenciacollege.edu">kborglum@valenciacollege.edu</a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/related-documents.pcf">©</a>
      </div>
   </body>
</html>