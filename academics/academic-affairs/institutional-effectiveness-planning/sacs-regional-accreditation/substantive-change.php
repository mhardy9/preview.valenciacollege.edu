<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Southern Association of Colleges and Schools Commission on Colleges | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/substantive-change.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Southern Association of Colleges and Schools Commission on Colleges</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/">Sacs Regional Accreditation</a></li>
               <li>Southern Association of Colleges and Schools Commission on Colleges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Substantive Change</h2>
                        
                        <p>The Southern Association of Colleges and Schools Commission on Colleges (SACSCOC)
                           defines substantive change as a significant modification or expansion of the nature
                           and scope of an accredited institution. Under federal regulations, substantive change
                           includes:
                        </p>
                        
                        <ul>
                           
                           <li>Any change in the established mission or objectives of the institution</li>
                           
                           <li>Any change in legal status, form of control, or ownership of the institution</li>
                           
                           <li>The addition of courses or programs that represent a significant departure, either
                              in content or method of delivery, from those that were offered when the institution
                              was last evaluated
                           </li>
                           
                           <li>The addition of courses or programs of study at a degree or credential level different
                              from that which is included in the institution’s current accreditation or reaffirmation.
                           </li>
                           
                           <li>A change from clock hours to credit hours</li>
                           
                           <li>A substantial increase in the number of clock or credit hours awarded for successful
                              completion of a program
                           </li>
                           
                           <li>The establishment of an additional location geographically apart from the main campus
                              at which the institution offers at least 50% of an educational program.
                           </li>
                           
                           <li>The establishment of a branch campus</li>
                           
                           <li>Closing a program, off-campus site, branch campus or institution</li>
                           
                           <li>Entering into a collaborative academic arrangement that includes only the initiation
                              of a dual or joint academic program with another institution
                           </li>
                           
                           <li>Acquiring another institution or a program or location of another institution</li>
                           
                           <li>Adding a permanent location at a site where the institution is conducting a teach-out
                              program for a closed institution
                           </li>
                           
                           <li>Entering into a contract by which an entity not eligible for Title IV funding offers
                              25% or more of one or more of the accredited institution’s programs
                           </li>
                           
                        </ul>
                        
                        
                        <p>Valencia Substantive Change Flow Chart </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>•Any change in the established mission or objectives of the institution </p>
                                    
                                    <p>•Entering a contract with an institution or organization not eligible for Title IV
                                       funds 
                                    </p>
                                    
                                    <p>•The establishment of an additional location geographically apart from the main campus
                                       at which the institution offers at least 50% of an educational program (degree, diploma
                                       or certificate, but not PSAV) 
                                    </p>
                                    
                                    <p>•The addition of new courses or programs of study at a degree or credential level
                                       different from that which is included in the institution’s current accreditation or
                                       reaffirmation 
                                    </p>
                                    
                                    <p>•A change from clock hours to credit hours </p>
                                    
                                    <p>•A substantial increase in the number of clock or credit hours awarded for successful
                                       completion of a program 
                                    </p>
                                    
                                    <p>•Any change in legal status, form of control, or ownership of the institution </p>
                                    
                                    <p>•The addition of courses or programs that represent a significant departure, either
                                       in content or method of delivery, from those that were offered when the institution
                                       was last evaluated. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Significant departure: </strong>a program that is not closely related to previously approved programs at the institution
                                       or site or for the mode of delivery in question. To determine whether a new program
                                       is a “significant departure,” it is helpful to consider the following questions:
                                    </p>                  
                                    
                                    <ul>
                                       
                                       <li>•What previously approved programs does the institution offer that are closely related
                                          to the new program and how are they related? 
                                       </li>
                                       
                                    </ul>                  
                                    <blockquote>
                                       
                                       <p>•Will significant additional equipment or facilities be needed? </p>
                                       
                                       <p>•Will significant additional financial resources be needed? </p>
                                       
                                       <p>•Will a significant number of new courses will be required? </p>
                                       
                                       <p>•Will a significant number of new faculty members will be required? </p>
                                       
                                       <p>•Will significant additional library/learning resources be needed? </p>
                                       
                                    </blockquote>                  
                                    
                                    <p>•<strong>Note</strong>: For additional questions about "significant," please contact the SACSCOC Liaison.
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>•Determination (notification, modified porspectus, prospectus) and recommendations
                                       from appropriate academic department, academic/discipline dean, and/or administrator
                                    </p>
                                    
                                    <ol>
                                       
                                       <li>
                                          <strong>Notification</strong>: a letter from an institution’s chief executive officer, or his/her designated representative,
                                          to SACSCOC President summarizing a proposed change, providing the intended implementation
                                          date, and listing the complete physical address if the change involves the initiation
                                          of an off-campus site or branch campus. The policy and procedures for reporting and
                                          review of institutional substantive change are outlined in the document “Substantive
                                          Change for Accredited Institutions of the Commission on Colleges.
                                       </li>
                                       
                                       <li>                      <strong>Modified prospectus: </strong>a prospectus submitted in lieu of a full prospectus for certain designated substantive
                                          changes. When a modified prospectus is acceptable, the Commission specifies requested
                                          information from the institution.
                                       </li>
                                       
                                    </ol>                  
                                    
                                    <p>•Recommendations from appropriate committee or council (i.e. Executive Council, Learning
                                       Council, Faculty Council, Facilities Offices, Curriculum Committee, General Counsel,
                                       etc.) to appropriate campus SACSCOC representative 
                                    </p>
                                    
                                    <p>•Notification to Valencia's SACSCOC laison from appropriate department, academic/discipline
                                       dean, and other administrator. Include all recommendations from each area. Note: Valencia's
                                       liaison to be included in subsequent meetings/communications related to the substantive
                                       change 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>•Documentation submitted to Valencia's SACSCOC liaison for review (i.e. notification,
                                       modified prospectus, prospectus) and final recommendations. <strong>Note</strong>: For collegewide initiatives, please send to SACSCOC lisaison; for campus-based initiatives,
                                       please send to appropriate campus SACSCOC representative 
                                    </p>
                                    
                                    <p>•Feedback submitted to appropriate department for final edits if necessary </p>
                                    
                                    <p>•Final report submitted to Valencia's Vice President of Academic Affairs and President
                                       for approval 
                                    </p>
                                    
                                    <p>•<strong>Important: </strong>SACSCOC submission deadlines for Board of Trustees are April 8 for June meetings and
                                       September 15 for December meetings 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>•Final report submitted to SACSCOC </p>
                                    
                                    <p>•Determinations by SACSCOC are as follows: </p>
                                    
                                    <ul>
                                       
                                       <li>
                                          <strong>Notification: </strong>The institution sends a letter to Dr. Wheelan to inform the Commission of a change.
                                          SACSCOC sends a letter in return that says, “We accept notification of…” (there may
                                          also be a request for additional information). 
                                       </li>
                                       
                                       <li>
                                          <strong>Approval: </strong>The institution sends a cover letter with a prospectus or modified prospectus. The
                                          documentation is reviewed, and the Commission sends a response that says, “It was
                                          the decision of the Board to approve the [site/program] and include it in the scope
                                          of the current accreditation.” 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3> Contact Information </h3>
                        
                        <p><strong><a href="../../../contact/Search_Detail2.cfm-ID=kborglum.html">Dr. Karen Borglum</a></strong></p>
                        
                        <p><strong>SACSCOC Institutional Liaison</strong></p>
                        
                        <p>Assistant Vice President</p>
                        
                        <p>Curriculum and Assessment</p>
                        
                        <p>Valencia College</p>
                        
                        <p>1768 Park Center Drive</p>
                        
                        <p>Orlando, FL 32835</p>
                        
                        <p>Mail code: DO-30</p>
                        
                        <p>(office) 407-582-3455; 
                           (fax) 407-582-3424
                        </p>
                        
                        <p><a href="mailto:kborglum@valenciacollege.edu">kborglum@valenciacollege.edu</a></p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/sacs-regional-accreditation/substantive-change.pcf">©</a>
      </div>
   </body>
</html>