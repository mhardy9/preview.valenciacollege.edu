<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Glossary  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/glossary.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li>Glossary </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Glossary/Definitions </h2>
                        
                        <h3>(IR Reporting Terminology)</h3>
                        <strong>ACCOUNTABILITY REPORT</strong> – a process where data-based measures are used to provide information on institutional
                        performance. Accountability Reports, published by the Florida Community Colleges and
                        Technical Center Management and Information System (CCTCMIS), contain fall cohort
                        benchmark measures calculated from data submitted by Florida State Colleges. These
                        reports include prior year high school graduate enrollments, student retention and
                        success, performance indicators for degree graduates who transfer to state universities
                        by college prep and non-college prep, and vocational program placement. 
                        
                        <p><strong>AGE</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Mean Age </strong>– the age obtained by computing the arithmetic average of all the Valencia students'
                              ages. 
                           </li>
                           
                           <li>
                              <strong>Median Age</strong> – the 50th percentile, obtained by creating a list of the ages of all the students
                              in numerical order. The total number of students is divided by 2. The resulting quantity
                              (rounded up to the next integer value if necessary) will be called position X. Starting
                              from either end of the list and counting up or down to X, the middle age is found,
                              corresponding to position X. That is the MEDIAN. For example, if there were 7 students
                              and the list of their ages looked like this: 17 17 18 18 18 19 19, the median would
                              be 18 (the second of the three 18s, in the fourth, or middle, position). 
                           </li>
                           
                        </ul>
                        
                        <p><strong>ALTERNATIVE DELIVERY </strong>– all modes of instruction other than traditional course delivery and may include
                           web-enhanced, hybrid, online, and computer-assisted. The intent of alternative delivery
                           courses is to provide flexibility to students who are attempting to balance work,
                           family and college, and still reach their career and academic goals. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Hybrid/Blended Course </strong>– a course that blends online and face-to-face delivery. A certain percentage of course
                              instruction is delivered via electronic means and a certain percentage of instruction
                              is conducted face-to-face<em>. (Some hours are spent in the classroom with the rest being completed online.)</em> 
                           </li>
                           
                           <li>
                              <strong>Online Course </strong>- a course where all of the content is delivered online using the college approved
                              Course Management System. <em>(All class meetings and coursework occur online. Proctored testing may be required.)</em> 
                           </li>
                           
                           <li>
                              <strong>Web Enhanced/Facilitated </strong>– a course which may use web-based technology to enhance a face-to-face course. Uses
                              a Course Management System (CMS) or web pages to post the syllabus and assignments.
                              <em>(All meetings are face-to-face.)</em>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>APPLICATION and ADMISSION</strong> – the process of completing and submitting an application to attend Valencia. Specific
                           criteria must be met before an applicant is admitted to Valencia (see page 20 &amp; 21
                           of Valencia Catalog). 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Applied Student</strong> – one who completed Valencia’s application. 
                           </li>
                           
                           <li>
                              <strong>Admitted Student </strong>– a student whose application has been reviewed and accepted but may not have already
                              registered for classes at Valencia. 
                           </li>
                           
                           <li>
                              <strong>Active Student </strong>– a student who has enrolled in credit courses at Valencia within the past 24 months.
                              
                           </li>
                           
                           <li>
                              <strong>Enrolled Student</strong> – a student who registered for one or more courses.
                           </li>
                           
                        </ul>
                        
                        <p><strong>ARTICULATION</strong> – a State Board of Education rule that establishes provisions to facilitate the smooth
                           transition of students through the various levels of Florida’s educational system.
                           
                        </p>
                        
                        <p><strong>AtD (Achieving the Dream: Community Colleges Count)</strong> – is a multiyear national initiative to help more community college students succeed.
                           The initiative is particularly concerned about student groups that traditionally have
                           faced significant barriers to success, including students of color and low-income
                           students. Achieving the Dream is working to help more students earn certificates or
                           degrees that open the door to better jobs, further education, and greater opportunity.
                           Valencia’ participation in AtD ended in 2009 the knowledge gained and the criteria
                           used during the AtD process is often used for Valencia’s internal reporting. 
                        </p>
                        
                        <p><strong>CAMPUS LOCATIONS and OFFERINGS </strong>(*Associate in Applied Science (AAS) and Associate in Science (AS) Degree programs
                           are composed of general and specialized courses. General education courses are taught
                           at all college locations. Due to the need for equipment, some specialized courses
                           may need to be taken at a specific campus.)<strong> </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Criminal Justice Institute (CJI)</strong> <strong>and Fire Rescue Institute (FRI)</strong> – located at 8600 Valencia College Lane, Orlando, Florida. Career Certificate Programs
                              in Criminal Justice and Fire Rescue, and Fire college credit programs are offered
                              at this location. 
                           </li>
                           
                           <li>
                              <strong>District Office (DO) </strong>– located at Park Place at MetroWest, 1768 Park Center Drive, Orlando, FL . College
                              Administration, Valencia Foundation and Human Resources are located at the DO. 
                           </li>
                           
                           <li>
                              <strong>East Campus – </strong>located at 701 North Econlockhatchee Trail, Orlando, Florida. This campus houses the
                              Performing Arts Center/Black Box Theater and Alumni Association Office. In addition,
                              the following programs are offered on East Campus: AA, and AS* Degree Programs, and
                              Certificate Programs, and Honors Program. Students who apply and maintain their home
                              records on this campus are considered East Campus enrollees. 
                           </li>
                           
                           <li>
                              <strong>Lake Nona Campus </strong>– 12500 Narcoossee Road, Bldg. 400, Orlando, Florida. This Campus houses the Collegiate
                              Academy which includes credit courses in Math, Science, Communications and Social
                              Sciences. This location also offers noncredit Foreign Language courses and is located
                              near Orlando’s Medical City.
                           </li>
                           
                           <li>
                              <strong>Osceola Campus – </strong>located at 1800 Denn John Lane, Kissimmee, Florida. The following programs are offered
                              on Osceola Campus: AA, and AS* Degree Programs, Certificate Programs, Corporate and
                              Continuing Education, and Honors Program. 
                           </li>
                           
                           <li>
                              <strong>Continuing Education at West Campus</strong> – 1800 S Kirkman Road, Building 10, Orlando, FL 32811. This Center houses Valencia
                              Enterprises which includes Corporate Training and Continuing Education, Center for
                              Global Languages, Learning Scenarios, and Performance Consulting. 
                           </li>
                           
                           <li>
                              <strong>West Campus – </strong>located at 1800 South Kirkman Road, Orlando, Florida. The following programs are offered
                              on West Campus: AA, and AS* Degree Programs, Certificate Programs, and Honors Program.
                              Beginning Fall 2011, Bachelor’s Degrees are offered in Electrical and Computer Engineering
                              Technology (B.S.E.C.E.T Degree), and Radiologic and Imaging Sciences (A.S. to B.S.
                              Degree). Students who apply and maintain their home records on this campus are considered
                              West Campus enrollees. 
                           </li>
                           
                           <li>
                              <strong>Winter Park Campus – </strong>located 850 West Morse Boulevard, Winter Park, Florida. The following programs are
                              offered on Winter Park Campus: Honors Program, Corporate and Continuing Education,
                              AA Degree Programs, Certificate Programs, Teacher Recertification, Weekend College,
                              and Career Assessment &amp; Employability Skills. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>CAREER PATHWAYS (formerly Tech Prep) - </strong>Students begin Career Pathways in high school focusing on higher academics and technical
                           skills, then link their high school studies with at least two years at an Orange or
                           Osceola county technical school or Valencia College. This can lead to a two-year certificate,
                           associate’s degree, or a four-year college degree. After taking designated technical
                           courses at their high schools, Career Pathways students participate in an assessment
                           and are eligible to receive credit when they enroll at Valencia. By earning college
                           credit while in high school, students make a smooth transition between high school
                           and college by not duplicating coursework and save time and tuition/book money. 
                        </p>
                        
                        <p><strong>COURSE CLASSIFICATION</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Advanced &amp; Professional (A &amp; P) </strong>– college <u>credit</u> courses and instructional programs designed to provide the first two years of course
                              work for transfer to an upper-division institution. 
                           </li>
                           
                           <li>
                              <strong>Continuing Workforce Education (CWE) </strong>– <u>noncredit </u>courses designed for students who are already employed when they enroll at Valencia
                              and whose primary objective is to enhance specific vocational abilities. These courses
                              <u>are not</u> funded by the State. CWE courses <u>do not earn college credit or postsecondary adult vocational credit. </u>CWE courses are offered at Valencia’s Criminal Justice Institute and Valencia Enterprises.
                              
                           </li>
                           
                           <li>
                              <strong>Developmental Education (formerly College Preparatory)</strong> – courses intended to bridge the gap between secondary school and college for students
                              with specifically identified deficiencies. The courses are credit earning but <u>may not be applied</u> toward a degree or certificate. These courses are designed to meet the academic needs
                              of educationally disadvantaged students. 
                           </li>
                           
                           <li>
                              <strong>Educator Preparation Institute (EPI)</strong> – Educator Preparation Institute prepares students with a bachelor’s degree in a
                              discipline other than education to become classroom teachers in Florida. EPI courses
                              provide institutional credit, <u>are not</u> transferable to an upper-division institution, and <u>do not</u> count toward any degree. 
                           </li>
                           
                           <li>
                              <strong>Lifelong Learning </strong>– <u>noncredit</u> courses that are not in the workforce arena. These courses <u>are not</u> funded by the State, do not belong to a program, and have an ICS code of 13300 (CLAST
                              preparatory courses, study skills, and senior citizen physical education courses are
                              lifelong learning courses). These courses are designed to review and improve a student's
                              competencies. Students <u>do not</u> earn college credit for these courses. 
                           </li>
                           
                           <li>
                              <strong>Lifelong Learning (College Credit LLL)</strong> – a student enrolled in a non-repeatable college credit course in which the student
                              previously received a grade of A, B, or C. Grade forgiveness may not be used after
                              the second enrollment. <u>Enrollments are not funded</u> by the State of Florida. 
                           </li>
                           
                           <li>
                              <strong>Post Secondary Adult Vocational (PSAV) </strong>– certificate career education, job preparatory courses and programs through which
                              a student receives an adult vocational certificate upon completion of instruction.
                              Students <u>earn vocational credit</u> but <u>not college credit </u>for completion of these courses. PSAV credit does not apply to college credit programs.
                              
                           </li>
                           
                           <li>
                              <strong>Post Secondary Vocational (PSV) </strong>– college credit courses that are part of an Associate in Science Degree (AS), Vocational
                              Credit Certificate (PSVC), or an Advanced Technical Certificate (ATC). Select Postsecondary
                              Vocational courses may be used to satisfy elective credit for an Associate of Arts
                              Degree.
                           </li>
                           
                        </ul>
                        
                        <p><strong>CERT</strong><strong>IFICATE-SEEKING STUDENT</strong> – a student who fulfilled Valencia’s application and admission requirements and is
                           pursuing programs to prepare him/her for immediate entry into a career in the workforce.
                           Valencia offers an Advanced Technical Certificate (ATC), Career Certificates, and
                           Technical Certificates.<strong><u></u></strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Advanced Technical Certificate (ATC</strong>) – an extension of a specific AS degree program that consists of at least nine (9)
                              but less than 45 credits of college-level course work. Students who have already received
                              an AS degree and are seeking a specialized program of study to supplement their associate
                              degree may seek an ATC. Students receive a certificate upon completion of the program.
                           </li>
                           
                           <li>
                              <strong>Post Secondary Adult Vocational Certificate (PSAV) – </strong>programs that prepare students for careers directly in the workforce. The programs
                              require prescribed technical credit courses and basic skills proficiency achieved
                              through college-preparatory courses. The only Career Certificate program offered at
                              Valencia is offered through the Criminal Justice Institute. 
                           </li>
                           
                           <li>
                              <strong>Technical Vocational Certificate (PSVC) </strong>– programs that prepare students for immediate entry into a career in the workforce.
                              Technical Certificate programs require prescribed technical courses, and do require
                              general education courses, for a minimum of 12 college credits. These credits are
                              applicable towards a related  AS degree if the student meets degree-seeking requirements.
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>COHORT </strong>– a group followed through time, whose <a href="http://www.businessdictionary.com/definition/member.html">members</a> <a href="http://www.businessdictionary.com/definition/share.html">share</a> a significant <a href="http://www.businessdictionary.com/definition/experience.html">experience</a> or have one or more similar <a href="http://www.businessdictionary.com/definition/characteristic.html">characteristics</a> (e. g. ALL Valencia FTIC (first time in college) students, ages 20 through 29, who
                           began Valencia during Fall 2009). 
                        </p>
                        
                        <p><strong>DAY or EVENING STUDENT </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Day Student </strong>– a student enrolled in courses that meet during the day (prior to 5:00 p.m.). Counts
                              are unduplicated during the day; however, students may also be counted as an evening
                              student if they are also enrolled in night courses. 
                           </li>
                           
                           <li>
                              <strong>Evening Student </strong>– a student enrolled in courses that meet during evening hours (after 5:00 p.m.).
                              Counts are unduplicated for the evenings; however, students may also be counted as
                              day students if they are also enrolled in day courses. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>DEGREE-SEEKING STUDENT </strong>–<strong> </strong>a student who fulfilled Valencia’s application and admission requirements and is pursuing
                           an <u>associate</u> degree program (AA or AS) or Bachelor’s degree at Valencia. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Associate in Applied Science (AAS) </strong>– a two-year technical degree program that indicates a student has trained in a particular
                              field and is prepared for employment (degree designated for job entry). <strong></strong>
                              
                           </li>
                           
                           <li>
                              <strong>Associate in Arts (AA) </strong>– a two-year degree program designed to provide the freshman and sophomore levels
                              of education to students intending to transfer to an upper-division institution. 
                           </li>
                           
                           <li>
                              <strong>Associate in Science (AS)</strong> – a two-year technical degree program that contains 15-18 credit hours of transferable
                              general education courses. This program is designed to prepare students for employment
                              in a vocational technical career without subsequent upper-division training. 
                           </li>
                           
                           <li>
                              <strong>Bachelor’s Degree – </strong>Valencia’s bachelor's degree programs require about four years of study, but differ
                              from standard baccalaureate programs. Here, students must first complete an associate
                              degree before they can transfer into the bachelor's degree programs.
                           </li>
                           
                        </ul>
                        
                        <p><strong>DEI (Developmental Education Initiative)</strong> – is a national effort to increase the number of students who complete their developmental
                           education courses successfully and move on to college-level courses. Valencia’ participation
                           in DEI will end in the summer of 2012; the knowledge gained and the criteria used
                           during the DEI process is often used for Valencia’s internal reporting. 
                        </p>
                        
                        <p><strong>DIRECT CONNECT</strong> – an agreement with UCF (University of Central Florida) in which Valencia students
                           receive guaranteed admission to a bachelor’s degree program, increased opportunity
                           to complete a bachelor’s degree on a Valencia campus, and preferential admission to
                           select bachelor degree programs. 
                        </p>
                        
                        <p><strong>DUAL ENROLLMENT </strong>– an acceleration mechanism by which high school students may simultaneously earn
                           credit toward high school completion and a college degree. Dual enrollment students
                           pay no tuition. Dual enrolled students may attend courses on a Valencia campus, on
                           a high school campus, or online. 
                        </p>
                        
                        <p><strong>ENTRY LEVEL TESTING and PLACEMENT </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>CPT (Computerized Placement Test) </strong>– the primary entry testing used for placement in English, reading, and mathematics.
                              Subtests include CPT Reading Skills (R), CPT Sentence Skills (W), CPT Arithmetic (M),
                              and CPT Elementary Algebra (A). CPT College Level Math (I) is optional. Appropriate
                              ACT or SAT scores may exempt a student from taking the CPT. <strong><u></u></strong>
                              
                           </li>
                           
                           <li>
                              <strong>PERT (Postsecondary Education Readiness Test) </strong>– PERT assesses English, reading, and mathematics skills. Students are exempt from
                              taking the PERT, unless needed as a prerequisite for a specific course, or they provide
                              written documentation for one of Valencia’s approved exceptions (page 52 &amp; 53 of 2011/2012
                              catalog).<strong> </strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>ETHNICITY / RACE </strong>– a self-reported classification on Valencia’s application. Students are able to indicate
                           their ethnicity (Hispanic/Latino OR Not Hispanic/Latino) and may choose from one of
                           the following race categories: American Indian/Alaskan Native, Asian, Black or African
                           American, Native Hawaiian or Other Pacific Islander, or White.
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>Depending on the specific report, Valenica College internally reports Ethnicity/Race
                              data into either four or six categories. Four categories include African American,
                              Caucasian, Hispanic, and Other. Six categories include African American, Asian/Pacific
                              Islander, Caucasian, Hispanic, Native American, and Other. (Other category includes
                              all who did not indicate an Ethnicity/Race category).
                           </li>
                           
                        </ul>
                        
                        <p><strong>FETPIP (Florida Education and Training Placement Information Program) </strong>– this program obtains follow-up data by matching the social security numbers of former
                           students with information housed in various state and federal agencies. The 1989 Florida
                           Legislature established this program as the primary resource for all public education
                           agencies for the collection of graduate and leaver follow-up information. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Placement </strong>– graduates of an <u>AA degree program are considered placed</u> if they are continuing their education in an upper-division institution or serving
                              in the military. Graduates of <u>AS degree programs are placed</u> if they are employed in a related field, are continuing their education, or are serving
                              in the military. <u>Certificate completers are placed</u> if they are employed in a related field or serving in the military. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>FOE (Foundations of Excellence)</strong> – a one-year self-study, with guidance from the <em>Policy Center on the First Year of College</em>, to develop a formal Start Right or New Student experience . Valencia participated
                           in FOE during 2008-2009; the knowledge gained and the criteria used during the FOE
                           process are often used for Valencia’s internal reporting. 
                        </p>
                        
                        <p><strong>FTE (Full-Time Equivalent)</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Credit FTE </strong>– equals <u>weekly course credit hours</u> multiplied by <u>course total enrollments</u> divided by 30; one course credit hour is defined as 50 minutes of instruction per
                              week. This is a method of funding defined by the State Department of Education. 
                           </li>
                           
                           <li>
                              <strong>Noncredit FTE</strong> – equals <u>term total course contact hours</u> multiplied by <u>total course enrollments</u> divided by 900; one contact hour is defined as 60 minutes of instruction. This is
                              a method of funding defined by the State Department of Education. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>FTIC (First Time in College) Student – </strong>a new Valencia student who has never attended any college prior to enrolling at Valencia.
                           
                        </p>
                        
                        <p><strong>FTAV (First Time at Valencia) Student – </strong>a new Valencia student who has attended another institution prior to enrolling at
                           Valencia. 
                        </p>
                        
                        <p><strong>FULL-TIME/PART-TIME STUDENT</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Full-Time Student </strong>– a student enrolled in 12 or more credit hours in Fall, Spring, or Summer terms.
                              <strong></strong>
                              
                           </li>
                           
                           <li>
                              <strong>Part-Time Student </strong>– a student enrolled in fewer than 12 credit hours in Fall, Spring, or Summer terms.
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>GATEWAY COURSES </strong>– high-enrolled courses that are taken by students early in their education at Valencia;
                           six courses were targeted by the AtD (Achieving the Dream) Initiative. These courses
                           included MAT0018C (Developmental Math I), MAT0028C (Developmental Math II), MAT1033
                           (Elementary Algebra), MAC1105 (College Algebra), ENC1101 (English Composition I),
                           and POS2041 (U. S. Government I). 
                        </p>
                        
                        <p><strong>GPA (Grade Point Average)</strong> – obtained by dividing quality points earned (based on grades) by credits. GPAs range
                           from 0.0 to 4.0. The following GPA types are calculated at Valencia: Institutional
                           GPA (all course work at Valencia), Overall GPA (all course work, including transfer
                           and college preparatory), Term GPA (all course work attempted and earned for a given
                           term), and Transfer GPA (all course work from other institutions). For certain reports,
                           the Institutional Research Office also groups GPAs by course, department, college
                           credit, college prep, etc. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Quality Points</strong> – the value, ranging from 0 to 4, for grades from A to F for all courses completed,
                              used in determining a grade point average (GPA). <strong><u></u></strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>GRADES</strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Success grades and rates – </strong>A, B, or C. (For some courses, a D constitutes a successful completion of the course;
                              <u>for Valencia data reporting</u>, a D constitutes an unsuccessful completion of the course.) 
                           </li>
                           
                           <li>
                              <strong>Non Success grades and rates – </strong>D, F, I (Incomplete), W (Withdrawal), WP (Withdrawal, Passing), or WF (Withdrawal,
                              Failing).
                           </li>
                           
                           <li>
                              <strong>Withdrawal grades and rates – </strong>W (Withdrawal), WP (Withdrawal Passing), or WF (Withdrawal Failing).
                           </li>
                           
                           <ul class="list-style-1">
                              
                              <li>Prior to Fall 2010, Valencia issued grades of WP (Withdrawal Passing) and WF (Withdrawal
                                 Failing). A WP is not calculated in the student’s GPA; while a WF is calculated as
                                 an F with 0 quality points.
                              </li>
                              
                           </ul>
                           
                        </ul>
                        
                        <p><strong>INTERNATIONAL STUDENT</strong> – a student who enters the United States on a nonimmigrant visa. Immigrants, refugees,
                           and asylees <u>ARE NOT</u> international students. At Valencia, International students are referred to as <strong>SEVIS</strong> and <strong>non-SEVIS</strong> students depending on Department of Homeland Security reporting requirements.<strong><u> </u></strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>SEVIS (Student and Exchange Visitor Information System) </strong>– is a web-accessible database used by the <a href="http://en.wikipedia.org/wiki/Department_of_Homeland_Security">Department of Homeland Security</a> to collect, track, and monitor information regarding exchange visitors, international
                              students and scholars who enter the <a href="http://en.wikipedia.org/wiki/United_States">United States</a> on F, M or J <a href="http://en.wikipedia.org/wiki/Visa_(document)">visa</a> types. SEVIS is managed by the Student and Exchange Visitor Program (SEVP) within
                              <a href="http://en.wikipedia.org/wiki/U.S._Immigration_and_Customs_Enforcement">U. S. Immigration and Customs Enforcement</a>. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>LEVEL (Discipline) </strong>– a Banner term used to categorize courses and programs—credit, continuing workforce
                           education, educator preparation institute, or post secondary adult vocational.
                        </p>
                        
                        <p><strong>LinC</strong><strong> (Learning in Community)</strong> – a program of linking two or more courses into one integrated course in which professors
                           from different disciplines tie their courses together around a common theme. 
                        </p>
                        
                        <p><strong>MODES OF DELIVERY – </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Face-to-Face/Onsite</strong> – a course where all content is delivered in a classroom setting on campus. 
                           </li>
                           
                           <li>
                              <strong>Online</strong> – a course where all the content is delivered online using the college approved Course
                              Management System. <em>(All class meetings and coursework occur online. Proctored testing may be required.)</em> 
                           </li>
                           
                           <li>
                              <strong>Web Enhanced/Facilitated </strong>– a course which may use web-based technology to enhance a face-to-face course. The
                              course content uses a Course Management System (CMS) or web pages to post the syllabus
                              and assignments. <em>(All meetings are face-to-face.)</em>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>MSA (OR Orlando MSA) </strong>- Orlando <strong><u>M</u></strong>etropolitan <strong><u>S</u></strong>tatistical <strong><u>A</u></strong>rea includes Orange, Osceola, Seminole, and Lake counties. Florida is divided into
                           twenty MSAs. Metropolitan and micropolitan statistical areas (metro and micro areas)
                           are geographic entities defined by the <a href="http://www.whitehouse.gov/omb/inforeg/statpolicy.html">U. S. Office of Management and Budget (OMB)</a> for use by Federal statistical agencies in collecting, tabulating, and publishing
                           Federal statistics.
                        </p>
                        
                        <p><strong>NEW STUDENT</strong> – According to Valencia’s FOE classification, a new student is one who has earned
                           less than 15 college-credit hours at Valencia.
                        </p>
                        
                        <p><strong>PERSONNEL CLASSIFICATION <u></u></strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Administrative/Executive/Management (EAM) </strong>- a personnel category defined by the Florida Accounting Manual which includes persons
                              whose assignments are planning, organizing, and managing the institution or a subdivision
                              within the institution. Officers holding such titles as President, Vice President,
                              Provost, or Department Deans are reported here. 
                           </li>
                           
                           <li>
                              <strong>Faculty </strong>– a personnel category defined by the Florida College System that includes persons
                              whose assignments are for the purpose of conducting instruction. 
                           </li>
                           
                           <li>
                              <strong>Noninstructional</strong><strong> Professional</strong> – a personnel category defined by the Florida College System which includes staff
                              members with assignments requiring professional training and are not reported under
                              administrative or faculty. Such titles as Counselor, Coordinator, and Director are
                              reported here. 
                           </li>
                           
                           <li>
                              <strong>Secretarial/Clerical </strong>– a personnel category defined by the Florida College System that includes persons
                              whose assignments are associated with clerical activities or is specifically of a
                              secretarial nature. 
                           </li>
                           
                           <li>
                              <strong>Service/Management</strong> – a personnel category defined by the Florida College System which includes persons
                              whose assignments require the performance of duties which provide comfort, convenience,
                              and hygiene to personnel and students or the upkeep and care of buildings, facilities,
                              or grounds. Examples include custodians, groundskeepers, and security personnel. 
                           </li>
                           
                           <li>
                              <strong>Teaching/Laboratory Assistants</strong> – a personnel category defined by the Florida College System that include persons
                              whose assignments require the development of teaching materials (syllabi and visual
                              aids) and the supervision of laboratories. 
                           </li>
                           
                           <li>
                              <strong>Technical/Paraprofessional/Skilled Craft </strong>– a personnel category defined by the Florida College System that includes persons
                              whose assignments require special manual or technical skills and a thorough knowledge
                              of the processes involved in specific trades. Such positions as computer operator,
                              graphic artist, electricians, carpenters, and typesetters are included here. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>RESIDENT COUNTY </strong>– a self-reported county classification completed by students when submitting Valencia’s
                           application. Students with Florida mailing address can choose from the drop-down menu
                           of Florida counties; students with non-Florida mailing addresses should select “Out
                           of State” under county.
                        </p>
                        
                        <p>For internal reporting purposes, Valencia generally provides Service District (Orange
                           &amp; Osceola counties), Seminole county, and Other data. (Other category includes Other
                           In-state counties, Out of State, or Not Available data.) 
                        </p>
                        
                        <p><strong>SUPPLEMENTAL LEARNING </strong>– classes that are supported by small group study sessions lead by Supplemental Learning
                           Leaders (former students). These study sessions are regularly scheduled, casual sessions,
                           in which students in the class compare notes, discuss assignments, and develop organizational
                           tools and study skills. 
                        </p>
                        
                        <p><strong>TERM </strong>- the academic period for which classes meet. Fall, Spring, and Summer Full terms
                           are approximately 14 weeks each; through Flex Start scheduling, courses are also taught
                           in five, six, eight, and ten week time periods. 
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Fall Term</strong> – August to December. 
                           </li>
                           
                           <li>
                              <strong>Spring Term</strong> – January to May. 
                           </li>
                           
                           <li>
                              <strong>Summer Term</strong> – May to August. 
                           </li>
                           
                           <li>
                              <strong>Flex Start </strong>– Flex Start courses shorten the number of weeks a student spends in class (from 14
                              to 10 weeks or less), but not the number of hours.
                           </li>
                           
                        </ul>
                        
                        <p><strong>VOCATIONAL CREDIT</strong> – a type of credit assigned to courses or course equivalent learning that is part
                           of an organized and specified vocational degree or certificate program. 
                        </p>
                        
                        <p><strong>YEAR </strong></p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              <strong>Academic Year </strong>– the sequence of three terms—Fall, Spring, and Summer. 
                           </li>
                           
                           <li>
                              <strong>Calendar Year </strong>– January through December. 
                           </li>
                           
                           <li>
                              <strong>Financial Aid Year </strong>– October 1 through September 30 (Federal Fiscal Year). 
                           </li>
                           
                           <li>
                              <strong>Fiscal Year </strong>– July 1 through June 30. 
                           </li>
                           
                           <li>
                              <strong>Foundation Year </strong>– April 1 through March 31. 
                           </li>
                           
                           <li>
                              <strong>Reporting Year</strong> – the sequence of three terms—Summer, Fall, and Spring. State of Florida reporting
                              falls in this category. 
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/glossary.pcf">©</a>
      </div>
   </body>
</html>