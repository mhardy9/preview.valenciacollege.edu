<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Research  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li>Institutional Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Mission</h2>
                        
                        <p>The mission of the Office of Institutional Research is to contribute data, information,
                           and analysis to Valencia's culture of inquiry and evidence in support of learning
                           assessment, decision-making, strategic planning, continuous improvement, and mandatory
                           reporting.&nbsp;
                        </p>
                        
                        <hr>
                        
                        <p><span style="text-decoration: underline;"><strong>Quick Links</strong></span></p>
                        
                        <h4>Enrollment</h4>
                        
                        <ul>
                           
                           <li>&nbsp;<a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/EOT-FTE-Term-201720.pdf">FTE</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/High-School-Feedback-Valencia-District-Fall-2016.pdf" target="_blank">High School Feedback</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/Reporting/Strategic-Indicators/MarketShare.html">High School Market Share (Interactive)</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/High-School-Feedback-Valencia-District-Fall-2016.pdf" target="_blank">High School Feedback</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/Intl-Student-Char-SPRING-201820-24JAN2018.pdf" target="_blank">International Students</a></li>
                           
                        </ul>
                        
                        <h4>&nbsp;</h4>
                        
                        <h4>Facts</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/StatisticalHistory2016.pdf">Statistical Fact Book</a></li>
                           
                           <li><a href="/academics/about-valencia/documents/valencia-college-facts.pdf">Facts Sheet</a></li>
                           
                        </ul>
                        
                        <h4>&nbsp;</h4>
                        
                        <h4>Strategic Indicators</h4>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/SIROnlineOverview2015-2016.pdf">Online Student Overview</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/Reporting/Strategic-Indicators/documents/1314StudentDemographics.pdf">Student Demographics</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/Reporting/Strategic-Indicators/StrategicIndicators.html">(interactive)</a></li>
                           
                        </ul>
                        
                        <h4>&nbsp;</h4>
                        
                        <h4>Internal Reports and Presentations</h4>
                        
                        <ul>
                           
                           <li><a href="https://mailvalenciacc.sharepoint.com/sites/IR/default.aspx">IR SharePoint Site</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.html">Discussion and Decision Dataset CCSSE 2015-2017</a>&nbsp;
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h2>&nbsp;Sources of Information&nbsp;</h2>
                        
                        <p>Commonly needed information is published on the IR web site in well-formatted reports
                           easily accessible by all:
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li><strong>Specialized Research Studies</strong>:&nbsp; Various studies are performed on an irregular basis to support grants and other
                              ad-hoc research needs.&nbsp; These include research briefs, survey results, decision-support
                              models, and links to other internal and external research sites.
                           </li>
                           
                        </ul>
                        
                        <p>If the information needed is not already published on this site, users have a variety
                           of options for meeting specialized information needs:
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              
                              <p><strong>State and Federal Agencies</strong>:&nbsp; The Institutional Research Department routinely provides data as requested by state
                                 and federal agencies.&nbsp; Valencia also participates in a variety of voluntary research
                                 and benchmarking surveys.&nbsp; Many of these external studies publish their own data summaries
                                 and research reports.&nbsp; IR can advise and assist in obtaining and understanding this
                                 information.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><strong><u><a href="/academics/oit/dw/index.html">SAS Warehouse Library</a></u></strong>:&nbsp; Hundreds of standard reports are available for users to run as needed against a
                                 SAS data warehouse that is updated daily.&nbsp; Many of these reports are designed as templates
                                 to allow you to select just the specific information you need.&nbsp; The Information Technology
                                 Department offers training and technical assistance in setting you up as a SAS warehouse
                                 user.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><strong><u><a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank">IR Work Request</a></u></strong>:&nbsp; Use this form to specify custom information requirements.&nbsp; A member of IR staff
                                 will be assigned to work with you as a consultant and retrieve the information needed.&nbsp;
                                 Output can be delivered as lists, tables, graphs, or data files.&nbsp; IR can also assist
                                 in converting your request into a SAS Warehouse report.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <h4>Internal Consulting</h4>
                        
                        <p>IR staff frequently consult with users on complex, ongoing research projects and routinely
                           participate as valued members of various committees throughout Valencia.&nbsp; Other sources
                           of information and research advice include:
                        </p>
                        
                        <ul class="list-style-1">
                           
                           <li>
                              
                              <p><strong><u><a href="/employees/organizations-committees/institutional-review-board/index.html">Institutional Review Board (IRB)</a></u></strong>:&nbsp; Research involving human subjects must comply with federal laws and regulations
                                 that protect the rights and welfare of individuals being studied.&nbsp; All researchers
                                 within Valencia must be trained on IRB procedures.&nbsp; Some routine academic research
                                 is exempt from IRB oversight, but it is always best to consult with the IRB to ensure
                                 compliance and protect all involved.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><strong>Valencia Data Team:</strong>&nbsp; The data team meets regularly and provides consultation on the design, execution,
                                 and publication of complex research projects.&nbsp; Organized as a sub-committee of Valencia’s
                                 College Learning Council, its focus is on large research grants, learning assessment,
                                 and the new student experience.&nbsp; The data team can also advise on other types of research
                                 projects as requested by those with complex research issues.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h4>Contact Us</h4>
                        
                        <p>The Office of Institutional Research welcomes your suggestions, comments, and requests.
                           Feel free to contact our staff for assistance.
                        </p>
                        
                        <p><strong>Daryl J. Davis</strong><br> <a href="mailto:djdavis@valenciacollege.edu">djdavis@valenciacollege.edu</a><br> Director, Institutional Research <br> 407-582-1255
                        </p>
                        
                        <p><strong>Donna J. Kosloski</strong><br> <a href="mailto:dkosloski@valenciacollege.edu">dkosloski@valenciacollege.edu</a><br> Institutional Research Analyst<br> 407-582-1345
                        </p>
                        
                        <p><strong>Selina Li</strong> <br> <a href="mailto:aarceneaux@valenciacollege.edu">sli6@valenciacollege.edu</a> <br> Institutional Research Analyst<br> 407-582-1611
                        </p>
                        
                        <p><strong>Craig L. Simpson</strong> <br> <a href="mailto:lrosen@valenciacollege.edu">csimpson25@valenciacollege.edu </a><br> Institutional Research Analyst<br> 407-582-1339
                        </p>
                        
                        <p><strong>Leann DuBois</strong><br> <a href="mailto:ldubois4@valenciacollege.edu">ldubois4@valenciacollege.edu </a><br> Institutional Research Analyst, Title V <br> 407-582-1071
                        </p>
                        
                        <p><strong>Cissy Reindahl</strong> <br> <a href="mailto:creindahl1@valenciacollege.edu">creindahl1@valenciacollege.edu</a><br> Data Analyst <br> 407-582-1612
                        </p>
                        
                        <p><strong>Eleanor L. McCoy-Carter</strong><br> <a href="mailto:emccoycarter@valenciacollege.edu">emccoycarter@valenciacollege.edu</a><br> Institutional Research Analyst, Title V<br> 407-582-1639
                        </p>
                        
                        <p><strong>Dr. Christos Giannoulis</strong><br><a href="mailto:cgiannoulis@valenciacollege.edu">cgiannoulis@valenciacollege.edu</a><br><span>Institutional Research Analyst</span><br><span>407-582-5258</span></p>
                        
                        <p><span><strong>Kari Schlieper</strong><br><a href="mailto:kschlieper@valenciacollege.edu">kschlieper@valenciacollege.edu</a><br>Staff Assistant II<br><span>407-582-1042</span></span></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.pcf">©</a>
      </div>
   </body>
</html>