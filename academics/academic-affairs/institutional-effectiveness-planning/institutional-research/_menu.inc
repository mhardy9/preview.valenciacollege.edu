<ul>
                    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.php">Institutional Research</a></li>
                    <li><a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank">Data Request Form</a></li>
                    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/glossary.php">Glossary</a></li>
                  <li class="submenu">
<a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/index.php" class="show-submenu">Reporting<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                    <ul>
                        <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/index.php">Internal Reports</a></li>
                        <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/enrollment.php">Enrollment</a></li>
                        <li>
<a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.php">External Reports</a>
                        </li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/index.php">Strategic Indicators</a></li>

                    </ul>
                </li>
                    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/links.php">Links</a></li>
                 </ul>
    
