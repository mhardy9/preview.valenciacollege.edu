<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Strategic Indicator  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li>Strategic Indicators</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        
                        
                        <h2>Reporting</h2>
                        
                        <h3>Strategic Indicator Reports</h3>
                        
                        <p><em>What is a Strategic Indicator Report?</em></p>
                        
                        <p>Valencia's Strategic Indicator Reports provide an overview of key areas of importance
                           to Valencia—student demographics, student progression, online students, etc.&nbsp; The
                           individual reports incorporate State of Florida accountability measures as well as
                           Valencia-generated data to present a succinct overview of self-defined student performance
                           and student success indicators.&nbsp; 
                        </p>
                        
                        <p>Additional Strategic Indicator Reports will be added in the future and current reports
                           will be updated annually.&nbsp; &nbsp;&nbsp;
                        </p>
                        
                        <hr>
                        
                        <h4>SIR Online Student Overview</h4>
                        
                        <p>The<a href="documents/SIROnlineOverview2015-2016.pdf"> <strong>2015-2016 Online Student Overview</strong></a><strong> </strong>Strategic Indicator Report highlights characteristics of students who enroll in online
                           courses and students who enroll in all modalities during the 2003-2004 through 2015-2016
                           academic year.&nbsp; This report includes the following:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>FTE (Full-time equivalent) for academic years 2003-2004 through 2015-2016
                              
                              <ul class="list_style_1">
                                 
                                 <li>Online, hybrid, and face-to-face (collegewide) </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Student characteristics data for all modalities and online for Fall terms 2003 through
                              2016
                              
                              <ul class="list_style_1">
                                 
                                 <li> Headcount, gender, ethnicity, age range and average credit hours attempted</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Glossary of Institutional Research terms </li>
                           
                        </ul>
                        
                        
                        <hr>
                        
                        
                        <h4>Student Demographics</h4>
                        
                        <p>The<a href="documents/1314StudentDemographics.pdf"> 2013-2014 Student Demographics</a> Strategic Indicator Report (SIR) provides the following information in graphical
                           and tabular format: 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Headcount
                              (Unduplicated) by Reporting Year 2003 through 2013 (Summer, Fall, Spring)  
                           </li>
                           
                           <li>Headcount (Unduplicated) Fall Term
                              
                              <ul class="list_style_1">
                                 
                                 <li>by gender, race/ethnicity, age, and student classification (Fall Term 2003 - 2013)
                                    
                                 </li>
                                 
                                 <li>by direct connect to UCF (Fall Term 2007 - 2013) </li>
                                 
                                 <li> by degree intent and full-time and part-time student status (Fall 2003 - 2014) </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>FTE (Full-Time Equivalent)
                              by Reporting Year 2006 through 2014 (Summer, Fall, Spring)
                              
                              <ul class="list_style_1">
                                 
                                 <li>New Student Share of Total Credit FTE (less than 15 hours completed at Valencia) &nbsp;
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Three-year College Preparatory Success Rate by Ethnicity (Mathematics, Writing, Reading)
                              (Fall 2005 - Fall 2013) 
                           </li>
                           
                        </ul>
                        
                        <p>A Glossary of terminology used in Valencia's Student Demographics SIR  is available<a href="documents/Glossary20120420.doc"> here</a>. 
                        </p>
                        
                        <hr>
                        
                        <h4>Student Progression</h4>
                        
                        <p>The <a href="StrategicIndicators.php">Strategic Indicator Report (SIR)</a> provides interactive data following  the path students often take to complete their
                           degree at Valencia.&nbsp; This path frequently begins with completing the necessary Developmental
                           Education courses as students proceed to Graduation.&nbsp; 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Developmental Education - Successful Program Completion within 3 Full Academic Years
                              
                           </li>
                           
                           <li>Developmental Education - Collegewide Success (A, B, C) and Withdrawal (W, WP, WF)</li>
                           
                           <li>Front Door High Enrollment Courses</li>
                           
                           <li> Persistence - Degree-seeking, First Time at Valencia (FTAV) Cohort by Enrollment
                              Category
                           </li>
                           
                           <li>Persistence Rates - Degree-seeking, First Time in College (FTIC) by Gender and Ethnicity
                              
                           </li>
                           
                           <li>Progression - Degree-seeking, Fall First Time in College (FTIC) Tracked over Time</li>
                           
                           <li>Graduation Rates - Degree-seeking, First Time in College (FTIC) over 5 Years</li>
                           
                           <li>Completion Rates and Time to Graduation - Degree-seeking, First Time in College (FTIC)
                              
                           </li>
                           
                        </ul>
                        
                        <p>A <strong>Glossary</strong> of terminology used in Valencia's Student Progression SIR is available <a href="documents/Glossary20120420.doc">here</a>. 
                        </p>
                        
                        <hr>
                        
                        <h4>Other Reports</h4>
                        
                        <p>
                           
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/bridges-student-characteristics.php">Bridges Student Characteristics</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/market-share.php">Market Share Table</a></li>
                           
                           <li>
                              <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/student-characteristics.php">Student Characteristics</a> 
                           </li>
                           
                           <li>
                              <a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/student-indicators.php">Student Indicators</a> 
                           </li>
                           
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/index.pcf">©</a>
      </div>
   </body>
</html>