<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Characteristics  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/student-characteristics.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/">Strategic Indicators</a></li>
               <li>Student Characteristics </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        
                        
                        <h2>Student Characteristics</h2>
                        
                        <p><strong>Click on the tabs below to navigate</strong></p>
                        
                        <div class="tableauPlaceholder" style="width: 1200px; height: 1350px;">
                           
                           <noscript><a href="http://preview.valenciacollege.edu/ir"><img alt=" " src="https://public.tableau.com/static/images/St/StudentCharacteristics_0/Summary/1_rss.png" style="border: none"></a></noscript>
                           <object class="tableauViz" width="975" height="1385" style="display:none;">
                              <param name="host_url" value="https%3A%2F%2Fpublic.tableau.com%2F"> 
                              <param name="site_root" value="">
                              
                              <param name="name" value="StudentCharacteristics_0/Summary">
                              
                              <param name="tabs" value="yes">
                              
                              <param name="toolbar" value="yes">
                              
                              <param name="static_image" value="https://public.tableau.com/static/images/St/StudentCharacteristics_0/Summary/1.png"> 
                              <param name="animate_transition" value="yes">
                              
                              <param name="display_static_image" value="yes">
                              
                              <param name="display_spinner" value="yes">
                              
                              <param name="display_overlay" value="yes">
                              
                              <param name="display_count" value="yes">
                              
                              <param name="showVizHome" value="no">
                              
                              <param name="showTabs" value="y">
                              
                              <param name="bootstrapWhenNotified" value="true"></object>
                           
                           
                        </div>
                        
                     </div>
                  </div>
                  
                  
                  
                  
                  
                  <main role="main">
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </main>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/student-characteristics.pcf">©</a>
      </div>
   </body>
</html>