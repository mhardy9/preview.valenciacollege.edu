<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>External Reports  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li>External</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        
                        
                        <h2>Reporting</h2>
                        
                        <h3>External Reports</h3>
                        
                        <p>The Office of Institutional Research creates standardized reports that are published
                           on a regular basis. These reports are readily available and provide data that is frequently
                           requested by internal and external sources.  Should you need data that is not available
                           on the IR website, please complete and submit an<a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank"> IR Work Request form</a>. 
                        </p>
                        
                        <hr>
                        
                        <h4>CCSSE (Community College Survey of Student Engagement)</h4>
                        
                        <p>The Office of Institutional Research partners with the Valencia's Assessment Offce
                           to provide external student surveys. One survey, the CCSSE, is completed by students
                           on a regular schedule. To learn more about CCSSE, please <a href="http://www.ccsse.org/aboutccsse">click here</a>. 
                        </p>
                        
                        <p>For more information on external surveys provided by the Office of Assessment and
                           Institutional Effectiveness, please <a href="../../../institutional-assessment/surveys/index.html">review their website</a>.
                        </p>
                        
                        <hr>
                        
                        <h4>IPEDS Reporting (Integrated Postsecondary Education Data System)</h4>
                        
                        
                        <p>IPEDS is the Integrated Postsecondary Education Data System. It is a system of interrelated
                           surveys conducted annually by the U.S. Department’s National Center for Education
                           Statistics (NCES). IPEDS gathers information from every college, university, and technical
                           and vocational institution that participates in the federal student financial aid
                           programs. The Higher Education Act of 1965, as amended, requires that institutions
                           that participate in federal student aid programs report data on enrollments, program
                           completions, graduation rates, faculty and staff, finances, institutional prices,
                           and student financial aid. These data are made available to students and parents through
                           the <a href="http://nces.ed.gov/collegenavigator/">College Navigator</a> college search Web site and to researchers and others through the <a href="http://nces.ed.gov/ipeds/datacenter">IPEDS Data Center</a>.
                        </p>
                        
                        <p><strong>Institutional Student General Information </strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              
                              <p><a href="documents/IPEDS_Institutional_Characteristics_2015-16Dataold.pdf">IPEDS Institutional Characteristics Survey  </a>collects data about institution's mission, admissions, student services, and student
                                 charges.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Enrollment, Retention </strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="documents/Completed12-monthEnrollmentSurvey.pdf">IPEDS 12-Month Enrollment Survey </a>collects unduplicated student enrollment counts and instructional activity data for
                              an entire 12-month period. Using the instructional activity data reported, a full-time
                              equivalent (FTE) student enrollment at the undergraduate and graduate level is estimated.
                              
                           </li>
                           
                           <li>
                              <a href="documents/CompletedFallEnrollmentSurvey.pdf">IPEDS Fall Enrollment Survey</a> collects student enrollment counts by level of student, enrollment status, gender,
                              and
                              race/ethnicity. In addition, first-time student retention rates and the student-to-faculty
                              ratio are collected. Every other
                              year data on residence of first-time undergraduates is required and in opposite years,
                              enrollment by student age is
                              required to be reported.
                           </li>
                           
                           <li>
                              <a href="documents/CompletedFallEnrollmentSurvey.pdf">IPEDS Retention Rates</a> The Fall Enrollment Overview collects student enrollment counts by level of student,
                              enrollment status, gender, and race/ethnicity. Retention Rates for full-time, first-time
                              Bachelor's students are included in this cohort. Florida 4-year colleges do not have
                              a full-time Bachelor-seeking cohort. The Florida College System is a 2+2 system.
                           </li>
                           
                           <li>
                              <a href="documents/CompletedGRS.pdf">IPEDS Transfer Rates </a> are included in the IPEDS Graduation Rates Overview (Section III).
                           </li>
                           
                        </ul>
                        
                        <p><strong>Financial Aid </strong></p>            
                        <ul class="list_style_1">
                           
                           <li>
                              
                              <p><a href="documents/CompletedFASurvey.pdf">IPEDS Student Financial Aid Survey </a>collects information about financial aid provided to various groups of undergraduate
                                 students at Valencia College.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Graduation</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="documents/CompletedGRS.pdf">IPEDS Graduation Rates Survey  </a>collects data on the cohort of first-time, full-time, degree/certificate-seeking undergraduates
                              and tracks them for 150% of the normal time of their program to see how many complete.
                           </li>
                           
                           <li>
                              <a href="documents/CompletedCompletionsSurvey.pdf">IPEDS Completions Survey</a> collects the number of degrees and certificates awarded by field of study, level
                              of award, race/ethnicity, and gender.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Institutional Resources</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="documents/CompletedHRSurvey.pdf">IPEDS Human Resources Survey </a> collects important information about
                              Valencia's staff.
                           </li>
                           
                           <li>
                              <a href="documents/CompletedFinanceSurvey.pdf">IPEDS Finance Survey </a>collects basic financial information from items associated with the
                              institution's General Purpose Financial Statements.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Library Survey</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="documents/CompletedLibrarySurvey.pdf">Academic Libraries Survey</a> collects data on Library expenses, collections,  services, and staff; completed January
                              28, 2013.
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.pcf">©</a>
      </div>
   </body>
</html>