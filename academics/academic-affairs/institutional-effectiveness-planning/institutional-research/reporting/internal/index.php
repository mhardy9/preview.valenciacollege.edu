<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internal Reports  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li>Internal</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Reporting</h2>
                        
                        <h3>Internal Reports</h3>
                        
                        <p>The Office of Institutional Research creates standardized reports that are published
                           on a regular basis. These reports are readily available and provide data that is frequently
                           requested by internal and external sources. Should you need data that is not available
                           on the IR website, please complete and submit an<a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank"> IR Work Request form</a>.
                        </p>
                        
                        <hr>
                        
                        <h4>Fact Books (Annual)</h4>
                        
                        <p>The <strong>Statistical History</strong>, through displays of selected data, presents a quantitative overview of Valencia
                           College as well as selected dimensions of its service area (Orange and Osceola counties).
                           The broad topics detailed in this document include information about the College's
                           student body, personnel, and financial resources. In general, most data are descriptive
                           in nature and sometimes include comparative data from previous years. In addition
                           to collegewide data displays, several types of student data are presented by campus.
                           Readers are invited to consult the glossary for definitions and clarification of unfamiliar
                           terms. This edition marks the twenty-sixth year of publication of Valencia's Statistical
                           History Fact Book.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/Statistical-History-Fact-Book-2016-17.pdf" target="_blank">2016/2017 Statistical History Fact Book</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/StatisticalHistory2016.pdf">2015/2016 Statistical History Fact Book</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Completed2014-15StatHistoryFactBook.pdf">2014/2015 Statistical History Fact Book</a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/CompletedStatisticalHistoryReport2014updated20140424.pdf">2013/2014 Statistical History Fact Book </a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Stat-Hist-2013-FINAL_000.pdf">2012/2013 Statistical History Fact Book </a></li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2012StatHistFinal.pdf">2011/2012 Statistical History Fact Book -</a> Updated 10/02/2012
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2011StatHistFinal20120120.pdf">2010/2011 Statistical History Fact Book</a> - Revised 01/20/2012
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/StatHistoryFinalCorrected20110407.pdf">2009/2010 Statistical History Fact Book</a> - Revised 04/07/2011
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/08-09SHFinal-4.pdf">2008/2009 Statistical History Fact Book</a> - Revised 10/13/2009
                           </li>
                           
                           <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/SH0708Final-IRPrint-REV20090609.pdf">2007/2008 Statistical History Fact Book </a>- Revised 06/09/2009
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h4>Fact Sheets (Annual)</h4>
                        
                        <p><em>Just the Facts</em> presents frequently-asked questions in an easy-to-use, one-page summary for Valencia
                           College. The following information is included in J<em>ust the Facts</em>: Valencia name changes and dates, service area, accreditation, library holdings,
                           annual enrollment by category, personnel by category, budget and tuition information,
                           Valencia sites, spedific student characteristics for Fall of previous year, and the
                           names of the members of the Board of Trustees, Valencia President, Vice Presidents,
                           and Campus Presidents.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “ACADEMICYEAR”" href="#">ACADEMIC YEAR</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “TERMS”" href="#">TERMS</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2013/2014</td>
                                    
                                    <td>Fall 2013 - Spring 2014 - Summer 2014</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012/2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master12-13_000.pdf">Fall 2012 - Spring 2013 - Summer 2013</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011/2012</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master11-12_000.pdf">Fall 2011 - Spring 2012 - Summer 2012</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2010/2011</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master101-REV-3_000.pdf">Fall 2010 - Spring 2011 - Summer 2011</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2009/2010</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master0910REV20101119_000.pdf">Fall 2009 - Spring 2010 - Summer 2010</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2008/2009</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master0809-REV_000.pdf">Fall 2008 - Spring 2009 - Summer 2009</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2007/2008</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Fact_Sheet_Master0708_000.pdf">Fall 2007 - Spring 2008 - Summer 2008</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <hr>
                           
                           <p>Campus-specific data for Fall 2014 End of Term in numeric format is available by clicking
                              the links below.
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <ul class="list_style_1">
                              
                              <li>East Campus &amp; Winter Park Campus</li>
                              
                              <li>Osceola Campus &amp; Lake Nona Campus</li>
                              
                              <li>West Campus</li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/index.pcf">©</a>
      </div>
   </body>
</html>