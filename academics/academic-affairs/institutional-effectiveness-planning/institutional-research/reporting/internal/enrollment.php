<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Enrollment  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/enrollment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/">Internal</a></li>
               <li>Enrollment </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Reporting</h2>
                        
                        <h3>Enrollment</h3>
                        
                        <h4>Credit Registration Report by Report Year (End of Term)</h4>
                        
                        <p>Day-to-day and previous year comparisons on headcount, FTE, and semester hours produced
                           on a regular schedule during the registration period.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “REPORTINGYEAR”" href="#">REPORTING YEAR</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “SUMMMER”" href="#">SUMMER</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “FALL”" href="#">FALL</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “SPRING”" href="#">SPRING</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2014</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201330FINAL.pdf">Summer 2013 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201410Final.pdf">Fall 2013</a></td>
                                    
                                    <td><em> in SAS Visual Analytics</em></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201230FinalReport.pdf">Summer 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201310Final.pdf">Fall 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201320FINAL.pdf">Spring 2013 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201130FINAL.pdf">Summer 2011 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201210Final.pdf">Fall 2011</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201220Final.pdf">Spring 2012 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/FinalMirror201030-RevisedAug172010.pdf">Summer 2010 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201110FINAL.pdf">Fall 2010 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201120Spring2011FinalCreditRegistrationReport.pdf">Spring 2011 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2010</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200930FinalReport.pdf">Summer 2009</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201010Final.pdf">Fall 2009</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror201020UtilizationFullModelFINAL-Revisedv2.pdf">Spring 2010 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2009</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200830Final.pdf">Summer 2008 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200910-Final.pdf">Fall 2008 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200920Final.pdf">Spring 2009 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2008</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200730-Final.pdf">Summer 2007 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200810Final.pdf">Fall 2007 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Mirror200820-FinalModified.pdf">Spring 2008</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>FTE (Full-Time Equivalent Enrollment) by Report Year (End of Term)</h4>
                        
                        <p>FTE comparisons for current and previous year. Includes credit and noncredit FTE.
                           Also includes Reference Table matching State Instructional Discipline and Valencia
                           Course Prefix. Generated two times per term-Beginning of Term data is replaced when
                           End of Term data is available.
                        </p>
                        
                        <div class="table-responsive">&amp;lt;
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “REPORTINGYEAR1”" href="#">REPORTING YEAR</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “SUMMMER1”" href="#">SUMMER</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “FALL1”" href="#">FALL</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “SPRING1”" href="#">SPRING</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2017</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201630-Web.pdf">Summer 2016</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201710-Web.pdf">Fall 2016</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOT-FTE-Term-201720.pdf">Spring 2017</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201530.pdf">Summer 2015 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201610.pdf">Fall 2015</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201620-Web.pdf">Spring 2016</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201430Summer2014EOTFTE.pdf">Summer 2014</a></td>
                                    
                                    <td><a href="http://preview.valenciacollege.edu/academic-affairs/institutional-effectiveness-planning/institutional-research/Reporting/internal/documents/201510Fall2014EOTFTECOMPLETE..pdf">Fall 2014</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm201520Complete.pdf">Spring 2015</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201330Summer2013EOTFTEComp22JAN2014.pdf">Summer 2013</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201410Fall2013EOTFTEComp09JAN2014-backup.pdf">Fall 2013</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EOTFTETerm20142030APR14-Final_000.pdf">Spring 2014</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201230Summer2012FTEEnrlBulBinder1.pdf">Summer 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201310Fall2012EOTFTECompFINAL.pdf">Fall 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201320EOTFTECompFINAL.pdf">Spring 2013</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <div>2012</div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201130EOTSummerEnrlBulletinPrelimFINAL.pdf">Summer 2011</a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/20121EOTFTECompPrelimFINAL.pdf">Fall 2011 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201220EOTFTECompPrelimFINAL.pdf">Spring 2012 </a></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <div>2011</div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201030FTEComparisonFINAL.pdf">Summer 2010</a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201110Fall2010FTEComparisonFINAL-REV20110124.pdf">Fall 2010 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201120EB20110610EOTFINAL.pdf">Spring 2011 </a></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <div>2010</div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200930EOTPrelimEB.pdf">Summer 2009</a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/EB201010es07Jan2010-REV-FINAL.pdf">Fall 2009</a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201020EOTEnrlBul-FINAL.pdf">Spring 2010 </a></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <div>2009</div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200830EOTEnrlBulletin.pdf">Summer 2008 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200910EOTEnrlBul-Final.pdf">Fall 2008 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200920BOTEnrlBul-final.pdf">Spring 2009 </a></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <div>2008</div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200730Summer2007EBFINAL-1.pdf">Summer 2007 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200810Fall2007EOTEB-FINAL.pdf">Fall 2007 </a></div>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <div><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/200820EOTFinalEnrlBulletin.pdf">Spring 2008</a></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>High School Feedback Report - Valencia College Service District (Fall Term Only )</h4>
                        
                        <p>Executive summary and number of students from Orange and Osceola county public high
                           schools who attended Valencia during Fall Term in the following categories:&nbsp; dual
                           enrolled, PYHSG, honors, scholarships awarded, dean's &amp; president's list, mandated
                           into College Prep, Tech Prep, retention during Spring Term, attended, and graduated
                           from Valencia. Individual reports produced for Orange and Osceola County in addition
                           to a combined Service District Report.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “ACADEMICYEAR”" href="#">ACADEMIC YEAR</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “FALL1”" href="#">FALL</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2016/2017</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/High-School-Feedback-Valencia-District-Fall-2016.pdf" target="_blank">Fall 2016</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015/2016</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201610HSFeedback-District.pdf">Fall 2015</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014/2015</td>
                                    
                                    <td>Fall 2014</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013/2014</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201410HSFeedback-DistrictFINAL_001.pdf">Fall 2013 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012/2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201310HSFeedback-DistrictFINAL.pdf">Fall 2012 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011/2012</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/ServiceDistrictHSFeedbackFall2011.pdf">Fall 2011 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2010/2011</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/DistrictHSFeedback201110.pdf">Fall 2010 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2009/2010</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/DISTRICTHSFeedback-FINAL.pdf">Fall 2009</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2008/2009</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/ValenciaDistrict2008HSFeedbackRpt.pdf">Fall 2008 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2007/2008</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/ServiceDistrict2007HSFeedback-Final.pdf">Fall 2007 </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>High School Market Share (Fall Term Only)</h4>
                        
                        <p>Provides the number of public High School graduates from Valenica's Service District,
                           Orange and Osceola counties, the number enrolled at Valencia, and the percent enrolled
                           at Valencia for 3 years.&nbsp; Also provides the&nbsp; number and percent change for Fall compared
                           to same period one year ago.&nbsp;&nbsp;High School graduate data is provide by DOE School Diploma
                           &amp; Certificate reports; enrolled graduate data comes from Valencia's Banner data.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “ACADEMICYEAR”" href="#">ACADEMIC YEAR</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “FALL1”" href="#">FALL</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2016/2017</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/Strategic-Indicators/MarketShare.html"><em>(interactive)</em></a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015/2016</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/MarketShare2015.pdf">Fall 2015 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014/2015</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/MarketShare2014.pdf">Fall 2014</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013/2014</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/MarketShare2013.pdf">Fall 2013 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012/2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/MarketShare2012.pdf">Fall 2012 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011/2012</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Marketshare2011.pdf">Fall 2011</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2010/2011</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2010Market_Share-Rev.pdf">Fall 2010 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2009/2010</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2009Market_Share-Rev.pdf">Fall 2009</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2008/2009</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2008Market_Share-DA.pdf">Fall 2008 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2007/2008</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/2007Market_Share-ES.pdf">Fall 2007 </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <h4>International Student Enrollment</h4>
                        
                        <p>&nbsp;</p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “REPORTINGYEAR1”" href="#">REPORTING YEAR</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “SUMMMER1”" href="#">SUMMER</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “FALL1”" href="#">FALL</a></th>
                                    
                                    <th scope="col" width="50"><a title="Sort on “SPRING1”" href="#">SPRING</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2018</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/Intl-Student-Char-SUMMER-201730-23MAY2017.pdf">Summer 2017</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/Intl-Student-Char-FALL-201810-18SEP2017.pdf" target="_blank">Fall 2017</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/documents/Intl-Student-Char-SPRING-201820-24JAN2018.pdf" target="_blank">Spring 2018</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2017</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201630_23MAY2016.pdf">Summer 2016 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_FALL_201710_07SEP2016.pdf">Fall 2016 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SPRING_201720_13JAN2017.pdf">Spring 2017</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2016</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201530_20MAY2015.pdf">Summer 2015</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_FALL_201610_15SEP2015.pdf">Fall 2015</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SPRING_201620_20JAN2016.pdf">Spring 2016 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2015</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201430_18JUN2014_000.pdf">Summer 2014</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_FALL_201510_03SEP2014.pdf">Fall 2014</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SPRING_201520_30JAN2015.pdf">Spring 2015 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2014</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201330_29MAY2013.pdf">Summer 2013</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_FALL_201410_08JAN2014.pdf">Fall 2013</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SPRING_201420_17FEB2014.pdf">Spring 2014</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2013</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201230_22MAY2012.pdf">Summer 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_FALL_201310_08OCT2012.pdf">Fall 2012 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SPRING_201320_01FEB2013.pdf">Spring 2013 </a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2012</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Char_SUMMER_201130_17MAY2011.pdf">Summer 2011</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201210Fall201122SEP2011.pdf">Fall 2011 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/201220Spring201218JAN2012.pdf">Spring 2012</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2011</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_SUMMER_201030_17SEP2010.pdf">Summer 2010 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_FALL_201110_20SEP2010.pdf">Fall 2010 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_SPRING_201120_19JAN2011.pdf">Spring 2011</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>2010</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_SUMMER_200930_22MAR2010.pdf">Summer 2009</a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_FALL_201010_05NOV09.pdf">Fall 2009 </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/Intl_Student_Chars_SPRING_201020_05MAR10.pdf">Spring 2010 </a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        <hr>
                        
                        <h4>Student Characteristics Report by Academic Year (End of Term)</h4>
                        
                        <p>Characteristics of students broken out by various categories:&nbsp; FTAV, FTIC, Admission
                           Status, Mandated Status, Ethnicity, Age, # Credit Hours, Resident County &amp; High School
                           for current term and same timeframe previous year.&nbsp;Report is produced for Beginning
                           of Term and End of Term.
                        </p>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “ACADEMICYEAR”" href="#">ACADEMIC YEAR</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “FALL1”" href="#">FALL-SPRING-SUMMER</a></th>
                                    
                                    <th scope="col" width="100">&nbsp;</th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>2010 - 2016</td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/Strategic-Indicators/StudentCharacteristics.html"> Student Characteristics (<em>Interactive</em>) </a></td>
                                    
                                    <td><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/documents/StudentCharacteristicsFall2016BOT.pdf"> Student Characteristics (<em>pdf</em>)</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/enrollment.pcf">©</a>
      </div>
   </body>
</html>