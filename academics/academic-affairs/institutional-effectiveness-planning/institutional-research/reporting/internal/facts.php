<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Institutional Research | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/facts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/">Reporting</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/">Internal</a></li>
               <li>Institutional Research</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        <h2>Facts</h2>
                        
                        <div>
                           
                           <div><a href="#">Fact Books <strong>(Annual)</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>The <strong>Statistical History</strong>, through displays of selected data, presents a quantitative overview of Valencia
                                       College as well as selected dimensions of its service area (Orange and Osceola counties).
                                       The broad topics detailed in this document include information about the College's
                                       student body, personnel, and financial resources. In general, most data are descriptive
                                       in nature and sometimes include comparative data from previous years. In addition
                                       to collegewide data displays, several types of student data are presented by campus.
                                       Readers are invited to consult the glossary for definitions and clarification of unfamiliar
                                       terms. This edition marks the twenty-sixth year of publication of Valencia's Statistical
                                       History Fact Book.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/StatisticalHistory2016.pdf">2015/2016 Statistical History Fact Book</a></li>
                                       
                                       <li><a href="documents/Completed2014-15StatHistoryFactBook.pdf">2014/2015 Statistical History Fact Book</a></li>
                                       
                                       <li><a href="documents/CompletedStatisticalHistoryReport2014updated20140424.pdf">2013/2014 Statistical History Fact Book </a></li>
                                       
                                       <li><a href="documents/Stat-Hist-2013-FINAL_000.pdf">2012/2013 Statistical History Fact Book </a></li>
                                       
                                       <li>
                                          <a href="documents/2012StatHistFinal.pdf">2011/2012 Statistical History Fact Book -</a> Updated 10/02/2012 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/2011StatHistFinal20120120.pdf">2010/2011 Statistical History Fact Book</a> - Revised 01/20/2012 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/StatHistoryFinalCorrected20110407.pdf">2009/2010 Statistical History Fact Book</a> - Revised 04/07/2011 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/08-09SHFinal-4.pdf">2008/2009 Statistical History Fact Book</a> - Revised 10/13/2009 
                                       </li>
                                       
                                       <li>
                                          <a href="documents/SH0708Final-IRPrint-REV20090609.pdf">2007/2008 Statistical History Fact Book </a>- Revised 06/09/2009
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div><a href="#">Fact Sheets <strong>(Annual)</strong></a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><em>Just the Facts</em> presents frequently-asked questions in an easy-to-use, one-page summary for Valencia
                                    College. The following information is included in J<em>ust the Facts</em>: Valencia name changes and dates, service area, accreditation, library holdings,
                                    annual enrollment by category, personnel by category, budget and tuition information,
                                    Valencia sites, spedific student characteristics for Fall of previous year, and the
                                    names of the members of the Board of Trustees,  Valencia President, Vice Presidents,
                                    and Campus Presidents. 
                                 </p>
                                 
                                 
                                 <div>
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Academic Year </strong></div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><strong>Terms</strong></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2013/2014</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div>Fall 2013 - Spring 2014 - Summer 2014 </div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2012/2013</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master12-13_000.pdf">Fall 2012 - Spring 2013 - Summer 2013</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2011/2012</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master11-12_000.pdf">Fall 2011 - Spring 2012 - Summer 2012</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2010/2011</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master101-REV-3_000.pdf">Fall 2010 - Spring 2011 - Summer 2011</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2009/2010</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master0910REV20101119_000.pdf">Fall 2009 - Spring 2010 - Summer 2010</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2008/2009</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master0809-REV_000.pdf">Fall 2008 - Spring 2009 - Summer 2009</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <div>2007/2008</div>
                                             </div>
                                             
                                             <div data-old-tag="td">
                                                <div><a href="documents/Fact_Sheet_Master0708_000.pdf">Fall 2007 - Spring 2008 - Summer 2008</a></div>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <p> Campus-specific data for Fall 2014 End of Term in numeric format is available by
                                    clicking the links below. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>East Campus &amp; Winter Park Campus</li>
                                    
                                    <li>Osceola Campus &amp; Lake Nona Campus</li>
                                    
                                    <li>West Campus  </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>              
                        
                        
                        
                        
                        <p><em>Updated 07/31/2014 </em></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/facts.pcf">©</a>
      </div>
   </body>
</html>