<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner"> <div id="logo">
<a href="/index.php"> <img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true" /></a>
</div>
</div>
<nav class="col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a> <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
<div class="site-menu">
<div id="site_menu"><img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College" data-retina="true" /></div>
<a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a>
<ul>
    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.php">Institutional Research</a></li>
    <li><a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank">Data Request Form</a></li>
    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/glossary.php">Glossary</a></li>
                          <li class="submenu">
<a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/index.php" class="show-submenu">Reporting<i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                            <ul>
                                <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/index.php">Internal Reports</a></li>
                                <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/enrollment.php">Enrollment</a></li>
                                <li>
<a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.php">External Reports</a>
                                </li>
<li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/index.php">Strategic Indicators</a></li>

                            </ul>
                        </li>
                            <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/links.php">Links</a></li>
                         </ul>
    </div>
     
</nav>
</div>
</div>
 
</div>
