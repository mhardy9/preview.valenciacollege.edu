<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Reports  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Research</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li>Reporting</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        
                        <div class="row">
                           
                           <div class="main-title">
                              
                              <h2>
                                 Reporting ...
                                 
                              </h2>
                              
                              <p>
                                 
                              </p>
                              
                              <p>A library of commonly used reports, both internal and external, is updated each term
                                 and annually when appropriate.&nbsp;Please browse through this site to find well-formatted
                                 summaries of current data and historical trends. Should you require a report that
                                 is not listed, please complete a <a href="http://net4.valenciacollege.edu/forms/academic-affairs/institutional-effectiveness-planning/institutional-research/DataInfoApp.cfm" target="_blank">IR Work Request form</a>. 
                              </p>
                              
                              
                              
                           </div>
                           
                           
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/index.html"> <i class="far fa-desktop"></i>
                                 
                                 <h3>
                                    Strategic Indicator Reports
                                    
                                 </h3>
                                 
                                 <p>
                                    Provide an overview of key areas of importance to Valencia—student demographics etc.
                                    
                                 </p>
                                 </a>
                              
                           </div>
                           
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/internal/index.html"> <i class="far fa-desktop"></i>
                                 
                                 <h3>
                                    Internal Reports
                                    
                                 </h3>
                                 
                                 <p>
                                    The Office of Institutional Research creates standardized reports that are published
                                    on a regular basis.
                                    
                                 </p>
                                 </a>
                              
                           </div>
                           
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/external/index.html"> <i class="far fa-desktop"></i>
                                 
                                 <h3>
                                    External Reporting
                                    
                                 </h3>
                                 
                                 <p>
                                    The Office of Institutional Research routinely provides data as requested by state
                                    and federal agencies.&nbsp;
                                    
                                 </p>
                                 </a>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/index.pcf">©</a>
      </div>
   </body>
</html>