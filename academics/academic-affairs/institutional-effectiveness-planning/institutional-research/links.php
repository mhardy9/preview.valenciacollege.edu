<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links  | Valencia College</title>
      <meta name="Description" content="The Office of Institutional Effectiveness &amp;amp; Planning is organized to support the demands of meaningful institutional effectiveness planning and implementation efforts within Academic Affairs and throughout the college.">
      <meta name="Keywords" content="college, school, educational, institutional effectiveness &amp;amp; planning, institutional, effectiveness, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Institutional Effectiveness &amp; Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/">Institutional Effectiveness Planning</a></li>
               <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/">Institutional Research</a></li>
               <li>Links </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Links</h2>
                        
                        <h3>Education Resources</h3>
                        
                        <p><strong>Data, Research, Policy Studies, and Reports</strong></p>
                        
                        <h4>College and University Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.ucf.edu/">University of Central Florida</a></li>
                           
                        </ul>
                        
                        
                        <h4>State and Regional Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.florida-air.org">Florida
                                 Association for Institutional Research (FAIR) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.fldoe.org/schools/higher-ed/fl-college-system/colleges/index.stml">Florida College System</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.myafchome.org">Association of Florida Colleges</a>
                              
                           </li>
                           
                           <li><a href="http://www.sreb.org/">Southern Regional Education Board</a></li>
                           
                        </ul>
                        
                        
                        <h4>Federal and National Organizations' Resources</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.aacc.nche.edu/">American Association  of Community Colleges (AACC)</a></li>
                           
                           <li>
                              <a href="http://airweb.org/">Association for Institutional Research (AIR) </a> 
                           </li>
                           
                           <li><a href="http://ccrc.tc.columbia.edu">Community
                                 College Research Center</a></li>
                           
                           <li>
                              <a href="http://www.ecs.org/">Education Commission of
                                 the States</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://nces.ed.gov/ipeds/">Integrated Postsecondary Education System (IPEDS) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.ed.gov/NCES/">National Center for
                                 Education Statistics (NCES) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://ncrve.berkeley.edu">National Center
                                 for Research in Vocational Education </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://national-institute-on-postsecondary-education-libraries-and-lif.idilogic.aidpage.com/national-institute-on-postsecondary-education-libraries-and-lifelong-learning/">National
                                 Institute on Postsecondary Education, Libraries, and Lifelong
                                 Learning (PLLI) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://209.129.196.100/indev/ncrp/ncrp.htm">NCRP
                                 Survey Bank (maintained by Peralta CCD, Office of Institutional
                                 Development) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.scup.org/">Society for College and
                                 University Planning (SCUP) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.ed.gov/">U.S. Department of Education
                                 </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.gseis.ucla.edu/">UCLA
                                 Graduate School of Education &amp; Info. Studies -- Higher Education
                                 Research Institute </a>
                              
                           </li>
                           
                           <li><a href="http://www.firstgov.gov/">U.S. Government Portal
                                 </a></li>
                           
                        </ul>
                        
                        
                        <h4>Other Institutional Research Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.airweb.org/Resources/Pages/AIR%20Resources.aspx"><strong>AIR Internet Resources for Institutional Research </strong></a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Library Resources</h3>
                        
                        <p><strong>Library, Academic, and Political Sources of Data and Information</strong></p>
                        
                        <h4>State and Regional Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.linccweb.org" target="_top">
                                 Library Information Network for Community Colleges (LINCCWeb)
                                 </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.fcla.edu/" target="_top">Florida Center for Library Automation
                                 </a>
                              
                           </li>
                           
                           <li><a href="http://www.distancelearn.org/" target="_top">
                                 Florida Community College Distance Learning Consortium</a></li>
                           
                           <li><a href="http://www.geolib.org/" target="_top">
                                 FSU GeoLib Project</a></li>
                           
                        </ul>
                        
                        
                        
                        <h4>National Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           
                           <li><a href="http://www.ala.org/acrl/" target="_top">Association of College &amp; Research Libraries (ACRL)</a></li>
                           
                           <li><a href="http://www.eric.ed.gov/" target="_top">The Educational
                                 Resources Information Center (ERIC) Database</a></li>
                           
                           <li><a href="http://www.edweek.org/" target="_top">Education
                                 Week</a></li>
                           
                           <li><a href="http://www.ed.gov/" target="_top">US Department of Education Resources</a></li>
                           
                           <li><a href="http://www.gpoaccess.gov/databases.html" target="_top">Government Printing Office (GPO) Database
                                 </a></li>
                           
                           <li><a href="http://www.loc.gov/" target="_top">Library of
                                 Congress</a></li>
                           
                           <li><a href="http://national-institute-on-postsecondary-education-libraries-and-lif.idilogic.aidpage.com/national-institute-on-postsecondary-education-libraries-and-lifelong-learning/" target="_top">
                                 National Institute on Postsecondary Education, Libraries, and Lifelong Learning (PLLI)</a></li>
                           
                           <li><a href="http://www.vote-smart.org" target="_top">Project
                                 Vote Smart -- Political Resources, Research &amp; Statistics</a></li>
                           
                           <li><a href="http://www.rand.org/research_areas/education/">Rand
                                 Corporation -- Education Research Area</a></li>
                           
                           <li><a href="http://www.ed.gov/pubs/index.html" target="_top">U.S.
                                 Department of Education Publications</a></li>
                           
                           <li><a href="http://www.firstgov.gov/" target="_top">U.S.
                                 Government Portal</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Grant Resources</h3>
                        
                        <p><strong>Grant Resources for Educational Programs, Projects, and Research</strong></p>
                        
                        
                        <h4>U.S. Department of Education</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.ed.gov/index.jhtml" target="_top">United States
                                 Department of Education</a></li>
                           
                        </ul>
                        
                        
                        <h4>National Institutes of Health (NIH)</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://grants.nih.gov/grants/oer.htm" target="_top">NIH:
                                 Extramural Research and Research Training Programs</a></li>
                           
                           
                           <li><a href="http://grants.nih.gov/grants/funding/sbir.htm" target="_top">NIH Small Business Funding
                                 Opportunities</a></li>
                           
                        </ul>
                        
                        
                        <h4>National Science Foundation (NSF)</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.nsf.gov/" target="_top">NSF</a></li>
                           
                           <li><a href="http://www.nsf.gov/funding" target="_top">National
                                 Science Foundation Grant Funding</a></li>
                           
                        </ul>
                        
                        
                        <h4>Other Federal Government Funding Sources</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.neh.gov/index.html">National
                                 Endowment for the Humanities (NEH)</a></li>
                           
                           <li><a href="http://arts.gov/grants" target="_top">National Endowment for the
                                 Arts (NEA) Grants</a></li>
                           
                           <li><a href="http://www.epa.gov/epahome/finance.htm" target="_top">Environmental Protection Agency (EPA) -- Contracts, Grants and
                                 Financing</a></li>
                           
                           <li><a href="http://www.nasa.gov/audience/foreducators/postsecondary/grants/">National
                                 Aeronautics and Space Administration (NASA) -- Fellowship and Grants</a></li>
                           
                           <li><a href="http://science.energy.gov/grants/" target="_top">Department of Energy (DOE) Office of Science -- Grants and
                                 Contracts</a></li>
                           
                           <li><a href="http://www.usda.gov/" target="_top">U.S.
                                 Department of Agriculture (USDA)</a></li>
                           
                           <li><a href="http://www.whitehouse.gov/about/fellows">The
                                 White House Fellowships</a></li>
                           
                           <li><a href="http://fedworld.ntis.gov/" target="_top">Guide to Federal Agencies</a></li>
                           
                           <li><a href="http://www.firstgov.gov/" target="_top">U.S.
                                 Government Portal</a></li>
                           
                        </ul>
                        
                        
                        <h4>Foundation Centers -- Sources of Grants</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://foundationcenter.org/findfunders/">
                                 Private Foundations</a></li>
                           
                           <li><a href="http://foundationcenter.org/findfunders/">
                                 Corporate Grant Providers</a></li>
                           
                           <li><a href="http://foundationcenter.org/findfunders/">
                                 Public Charities Providing Grants</a></li>
                           
                           <li><a href="http://foundationcenter.org/findfunders/">
                                 Community Foundations</a></li>
                           
                        </ul>
                        
                        
                        <h4>Other Resources</h4>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.aacc.nche.edu/Pages/default.aspx" target="_top">American
                                 Association of Community Colleges</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Population Data</h3>
                        
                        <p><strong>Population Data </strong></p>
                        
                        
                        <h4>Regional and State Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.bebr.ufl.edu/">Florida
                                 Business Data: </a><a href="http://www.bebr.ufl.edu/">Bureau of Economic
                                 and Business Research </a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Federal Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.census.gov/">U.S. Bureau of the
                                 Census </a>
                              
                           </li>
                           
                           <li><a href="http://www.firstgov.gov/">U.S. Government
                                 Portal </a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Learning Innovations</h3>
                        
                        <p><strong>Teaching and Learning Innovations Resources</strong></p>
                        
                        <h4>Web-based Journals, Articles, and Abstracts to Review</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.aera.net/pubs/">American Education
                                 Research Association (AERA) Journals </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.educause.edu/apps/er/index.asp">EDUCAUSE
                                 Review -- Why IT Matters to Higher Education</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.ilt.columbia.edu/">Institute
                                 for Learning Technologies at Columbia University</a>
                              
                           </li>
                           
                           <li><a href="http://www.josseybass.com">Jossey-Bass Publishers
                                 </a></li>
                           
                        </ul>
                        
                        
                        <h4>Bibliographical Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="https://www.libraries.psu.edu/findingaids/5955.htm#adminInfo">American
                                 Center for the Study of Distance Education</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://gse.berkeley.edu/research">Center
                                 for Research on Education and Work, UC, Berkeley Graduate School
                                 of Education </a>
                              
                           </li>
                           
                           <li><a href="http://www.wiley.com/WileyCDA/Section/id-813253.html">New
                                 Directions for Teaching and Learning, Jossey-Bass, Inc. Journals
                                 </a></li>
                           
                        </ul>
                        
                        
                        <h4>Websites for College- and University-Based Teaching &amp; Learning Resources and/or Centers</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://crlt.indiana.edu/">Center for Research
                                 on Learning &amp; Technology -- Indiana University </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://cfe.unc.edu/teaching-and-learning/">Center
                                 for Teaching and Learning, University of North Carolina at Chapel
                                 Hill </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.wku.edu/teaching/">College
                                 Teaching Encyclopedia, Western Kentucky University Center for
                                 Teaching and Learning </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.cmu.edu/provost/teaching/index.html">Eberly
                                 Center for Teaching Excellence, Carnegie Mellon University</a>
                              
                           </li>
                           
                           <li>
                              <a href="http://learning.media.mit.edu/">The M.I.T. Epistemology
                                 and Learning Group </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://ncrve.berkeley.edu/">National Center
                                 for Research in Vocational Education at UC Berkeley </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.northwestern.edu/searle/">Searle Center
                                 for Teaching Excellence, Northwestern University </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://teaching.berkeley.edu/compendium/">Suggestions
                                 for Teaching with Excellence, A Berkeley Compendium </a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Cooperative and Collaborative Learning Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.virginia.edu/~trc/cooperat.htm">Cooperative
                                 Learning Bibliography, Teaching Resource Center, University
                                 of Virginia </a>
                              
                           </li>
                           
                           <li><a href="http://www1.umn.edu/ohr/teachlearn/tutorials/active/resources/index.html">Cooperative Learning
                                 Center, University of Minnesota </a></li>
                           
                        </ul>
                        
                        
                        <h4>Innovative Learning Resources by Content Area/Academic Discipline</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://forum.swarthmore.edu/index.js.html">The
                                 Math Forum, Swarthmore College </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://writingcenters.org/">International
                                 Writing Center Associations </a>
                              
                           </li>
                           
                           <li><a href="http://www.wku.edu/teaching/">Western
                                 Kentucky University Faculty Center for Excellence in Teaching
                                 </a></li>
                           
                        </ul>
                        
                        
                        <h4>Distance Learning &amp; Computer/Information  Technology Resources for Teaching</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://www.nea.org/home/34765.htm">Distance Education
                                 Resources</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Other Resources</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="http://teaching.berkeley.edu/compendium/">A
                                 Berkeley Compendium of Suggestions for Teaching with Excellence
                                 </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.aacc.nche.edu/">American Association
                                 of Community Colleges </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.clearinghouse.net/">Argus Clearinghouse
                                 -- Internet Resources by Content Area </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.gseis.ucla.edu/research/">Centers
                                 and Research Units of the UCLA Graduate School of Education
                                 &amp; Information Studies </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.educationindex.com/education_resources.html">The
                                 Education Index -- Web Site Resources by Subject Area </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://national-institute-on-postsecondary-education-libraries-and-lif.idilogic.aidpage.com/national-institute-on-postsecondary-education-libraries-and-lifelong-learning/">National
                                 Institute on Postsecondary Education, Libraries, and Lifelong
                                 Learning (PLLI) </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.ntlf.com/">National Teaching and
                                 Learning Forum </a>
                              
                           </li>
                           
                           <li>
                              <a href="http://www.newhorizons.com">New Horizons
                                 for Learning </a>
                              
                           </li>
                           
                           <li><a href="http://www.pedagonet.com/">PedagoNet -- Learning
                                 Resource Material Exchange </a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/links.pcf">©</a>
      </div>
   </body>
</html>