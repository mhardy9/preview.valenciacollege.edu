<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Affairs  | Valencia College</title>
      <meta name="Description" content="The Office of Academic Affairs and Planning is committed to ensuring the success of faculty, students and staff">
      <meta name="Keywords" content="college, valencia, academic affairs, students, staff, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Academic Affairs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li>Academic Affairs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     <div class="col-md-12">
                        
                        
                        <div role="main">
                           
                           <h2>Welcome to the Office of Academic Affairs and Planning at Valencia College</h2>
                           
                           <p>At Valencia College, the Office of Academic Affairs and Planning is committed to ensuring
                              the success of faculty, students and staff. Our mission is to provide strategic vision,
                              leadership and support for inclusive academic excellence and integrity to further
                              our core focus on exemplary teaching and learning, our  collaborative culture, and
                              service to the College and community.
                              
                           </p>
                           
                           <p>I invite you to explore our website to learn how this office supports and  promotes
                              the College's progress in academic programming, faculty development, and academic
                              affairs. I hope you will return often for updates on these  activities.
                              
                           </p>
                           
                           <p>
                              Sincerely,
                              
                           </p>
                           
                           <p>
                              <strong>Susan E. Ledlow</strong><br>
                              Vice President, Academic Affairs &amp; Planning
                           </p>
                           
                           <hr>
                           
                           <h3>About Us</h3>
                           
                           <p>The Division of Academic Affairs and Planning includes the Office of the  Vice President
                              for Academic Affairs and Planning and five departments that  report to the Vice President:
                              
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li><a href="/academics/academic-affairs/career-workforce-education/index.html">Career and Workforce Education</a></li>
                              
                              <li><a href="/academics/academic-affairs/curriculum-assessment/index.html">Curriculum and Assessment</a></li>
                              
                              <li>
                                 <a href="/faculty/development/index.html">Teaching and Learning</a> (formerly  Faculty and Instructional Development)
                              </li>
                              
                              <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/index.html">Institutional Research</a></li>
                              
                              <li><a href="/academics/academic-affairs/resource-development/default.html">Resource Development</a></li>
                              
                           </ul>
                           
                           <h4>Vision</h4>
                           
                           <p>The  Academic Affairs Leadership Team (AALT) works collaboratively across campuses
                              to continuously strengthen and support Valencia as a national leader of college excellence.
                           </p>
                           
                           <h4>Values</h4>
                           
                           <p>We value and support learning through:</p>
                           
                           <ul class="list-style-1">
                              
                              <li>Meaningful assessment</li>
                              
                              <li>Robust program design</li>
                              
                              <li>Curriculum development</li>
                              
                              <li>Faculty development</li>
                              
                              <li>Continuous improvement thru evidence based practices</li>
                              
                              <li>(People) Collaborative leadership and deliberative engagement</li>
                              
                              <li>Diversity through the purposeful understanding of points of view and the promotion
                                 of inclusive excellence
                              </li>
                              
                              <li>Access by working with our community partners to build purposeful pathways to and
                                 through the college and beyond to help learners achieve their goals
                              </li>
                              
                              <li>Community support through collaborative and responsive partnerships to build Economic
                                 and social growth, enrichment, and transformation (UCF DirectConnect, K-12)
                              </li>
                              
                              <li>Integrity by staying true to the college's core values, and ensuring compliance with
                                 state, regional and federal regulations. 
                              </li>
                              
                           </ul>
                           
                           <hr>
                           
                           <h4>Mission</h4>
                           
                           <p>The AALT members provide strategic leadership, guidance, and expertise in the areas
                              of:
                           </p>
                           
                           <ul class="list-style-1">
                              
                              <li>Accreditation (SACSCOC)</li>
                              
                              <li>Articulation</li>
                              
                              <li>Assessment</li>
                              
                              <li>Career and Workforce Education</li>
                              
                              <li>Curriculum</li>
                              
                              <li>Faculty Development</li>
                              
                              <li>Institutional Effectiveness</li>
                              
                              <li>Institutional Research</li>
                              
                              <li>Planning</li>
                              
                              <li>Resource Development</li>
                              
                           </ul>
                           
                           <hr>
                           
                           <h4>Upcoming Meetings</h4>
                           <a href="http://events.valenciacollege.edu/event/part-time_faculty_town_hall_meeting" target="_blank">Part-time Faculty Town Hall Meeting  at Winter Park Campus</a><br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                  </div>
                  
                  
                  
                  
                  
                  <main role="main">
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </main>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/index.pcf">©</a>
      </div>
   </body>
</html>