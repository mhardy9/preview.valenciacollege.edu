<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meet the Staff  | Valencia College</title>
      <meta name="Description" content="The Office of Curriculum and Assessment has the responsibility for the College Curriculum Committee, the College Catalog, Articulation agreements, and the Course Substitution Committee.">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/curriculum-assessment/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/curriculum-assessment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>The Office of Curriculum and Assessment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/curriculum-assessment/">Curriculum Assessment</a></li>
               <li>Meet the Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <p class="col-md-12"><a name="content" id="content"></a>
                        
                        
                        
                        
                     </p>
                     
                     <h2>Meet the Staff </h2>
                     
                     <h3>Office of Curriculum and Assessment</h3>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-karen-borglum-68x68.jpg" alt="Dr. Karen Borglum" class="img-circle"><br>
                        <strong>Dr. Karen Borglum<br>
                           <em>AVP, Office of Curriculum and Assessment</em></strong><br>
                        <a href="mailto:kborglum@valenciacollege.edu">kborglum@valenciacollege.edu</a></p>
                     
                     <p>Dr. Karen Borglum currently serves as the Assistant Vice  President of Curriculum
                        and Assessment at Valencia Community College.&nbsp; Prior  to this position, she was the
                        Dean of Communications at Valencia’s West Campus,  and she also served as the Director
                        of a Title III grant.&nbsp; 
                     </p>
                     
                     <p>Karen holds a Bachelor of Arts degree in Marketing, a  Masters of Arts degree in Communication,
                        and a Doctor of Education in  Curriculum and Instruction from the University of Central
                        Florida, in  Orlando.&nbsp; Her professional development activities have included selection
                        in the Management Development Program (MDP) at Harvard University, the 2002  Florida
                        Community College System Chancellor’s Leadership Program in Tampa,  Florida, the Leadership
                        Academy in Toronto, the Leadership Orlando  series.&nbsp; She has also served on the State
                        General Education Steering  Committee from 2012-2013.
                     </p>
                     
                     <p>Dr. Borglum has presented at state and national conferences  in the areas of curriculum,
                        instructional enhancement, and communities of  practice.&nbsp; As part of her professional
                        practice, Dr. Borglum has won a  Bronze certificate for School-to-Work Best Teaching
                        Practice, and designed a  national communities of practice group through the American
                        Association for  Higher Education.&nbsp; She served as a lead QEP evaluator and as an onsite
                        reaffirmation reviewer for SACS-COC, she has served on 3 on-site SACS-COC  review
                        teams, and she has also served, for two years, on the SACS-COC Annual  Meeting Planning
                        Committee.&nbsp; Additionally, she co-authored and article in The  Community College Journal
                        of Research and Practice entitled, “Academic and  Social Integration of Community
                        College Students:&nbsp; A case Study (August  2000),” and in 1996 she authored an article
                        entitled, “What About Academic  Standards?” for Community College Week and Community
                        College Times.
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-glenn-ricci-68x68.jpg" alt="Glenn Ricci" class="img-circle"><br>
                        <strong>Glenn Ricci<br>
                           <em>Curriculum Coordinator</em></strong><br>
                        <a href="mailto:gricci2@valenciacollege.edu">gricci2@valenciacollege.edu</a></p>
                     
                     <p>Glenn Ricci began at Valencia in July 2013 after years of college experience as Dean
                        of Arts &amp; Sciences,  Professor, Program Manager, and Online Faculty member.  He is
                        a graduate of the University of  Central Florida, Doctorate in Curriculum &amp; Instruction;
                        Carnegie Mellon University; University of Wyoming;  and Point Park College.  Glenn
                        holds Florida Teacher Certification and Blackboard Product Specialist  Certification.
                        He has taught online courses in BlackBoard, E-College, E-learning, and WebCT learning
                        management platforms and recently returned to Florida after teaching hybrid and online
                        courses at the  University of Arkansas. 
                     </p>
                     
                     <p>Over the years, Glenn has designed curriculum for numerous courses and programs in
                        various academic  areas of study. He has also served as chair of curriculum committees
                        in working with diverse faculty and  staff and has coordinated curriculum changes
                        with the Florida Statewide Course Numbering System.  He looks forward to sharing his
                        educational experiences and working with Valencia College staff as  Curriculum Coordinator
                        in the Office of Curriculum and Assessment.
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-noelia-maldonado-68x68.jpg" alt="Noelia Maldonado" class="img-circle"><br>
                        <strong>Noelia Maldonado<br>
                           <em>Planning Support Specialist</em></strong><br>
                        <a href="mailto:nmaldonado5@valenciacollege.edu">nmaldonado5@valenciacollege.edu</a></p>
                     
                     <p>Noelia joined Valencia in 2008 as an Executive Assistant within the Institutional
                        Advancement department. In the fall 2011, she moved under Academic Affairs &amp; Planning
                        where she primarily supports strategic planning at the college. Noelia also serves
                        as support staff of the new Governance and as a Grievance Committee Representative
                        for Career Staff. 
                     </p>
                     
                     <p>Noelia holds an A.A. in General Education and multiple certifications in Business
                        Management from Valencia College, as well as a B.A. in Psychology from University
                        of Central Florida. She also spends her spare time supporting cultural events and
                        activities in the local Hispanic community. 
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-krissy-brissett-68x68.jpg" alt="Krissy Brissett" class="img-circle"><br>
                        <strong>Krissy Brissett<br>
                           <em>Curriculum Support Specialist, Office of Curriculum and Assessment</em></strong><br>
                        <a href="mailto:kbrissett1@valenciacollege.edu">kbrissett1@valenciacollege.edu</a></p>
                     
                     <p>Krissy Brissett came onboard as the Administrative Assistant  for Curriculum and Assessment
                        in August 2012.&nbsp; Previously she held other  administrative positions at UCF and in
                        the US Navy.&nbsp; Krissy's duties as  Administrative Assistant allow her to work with
                        various faculty in many  departments.&nbsp; Her main duties involve proposals that come
                        to the College  Curriculum Committee, which is responsible for ensuring that all courses
                        and  programs have instructional integrity, address appropriate learning outcomes,
                        fit into a sequential framework that leads to students achieving the respective  competencies,
                        and meet the college's standards of excellence. Krissy serves as the West/District
                        Office Career Staff secretary.
                     </p>
                     
                     <p>Krissy is working on her Bachelor's degree at UCF.&nbsp; She  completed her Associate's
                        at Valencia College.&nbsp; She enjoys gardening and  traveling.&nbsp; She especially likes to
                        travel to her native Jamaica.
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <h3>Study Abroad and Global Experiences/Curriculum Initiatives</h3>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-robyn-brighton-68x68.jpg" alt="Robyn Brighton" class="img-circle"><br>
                        <strong>Robyn Brighton<br>
                           <em>Director, Sage and Curriculum Initiatives</em></strong><br>
                        <a href="mailto:rbrighton@valenciacollege.edu">rbrighton@valenciacollege.edu</a></p>
                     
                     <p>Robyn joined the Valencia family as a part-time Human Resources assistant in 2007
                        and, since then, has been a Senior Instructional Assistant in the East Communications
                        Center. In 2010, she became the first full-time college-wide LinC Coordinator and
                        more recently has begun serving the role of the Director, Curriculum Initiatives.
                        In addition to continuing her work with LinC, Robyn is now supporting the growth and
                        sustainability of our Service Learning initiative. Robyn has also been teaching Student
                        Success as a stand-alone course and LinC since 2009.
                     </p>
                     
                     <p>Robyn is a Florida native and UCF Alumni. She received her BS in Elementary Education
                        and MEd in Exceptional Education with a focus in Autistic disorders. When she's not
                        at work she enjoys spending time with her family and friends. 
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-kara-parsons-68x68.jpg" alt="Kara Parsons" class="img-circle"><br>
                        <strong>Kara Parsons<br>
                           <em>Manager, Study Abroad Programs, Study Abroad and Global Experiences</em></strong><br>
                        <a href="mailto:ametz4@valenciacollege.edu">kparsons8@valenciacollege.edu</a></p>
                     
                     <p>Kara joined Valencia in July 2014 as a Program Support Specialist in the SAGE Office,
                        working predominantly with faculty-led study abroad programs. Kara earned a Bachelor
                        of Arts in Organizational Communication and International Studies from University
                        of West Florida in 2012, and is currently pursuing a master’s degree in Educational
                        Leadership at University of Central Florida.
                     </p>
                     
                     <p>Kara’s background consists of student affairs and a variety of non-profit efforts,
                        including a year of volunteer service as an AmeriCorps VISTA. She studied abroad during
                        the fall of 2011 at University of Strathclyde in Glasgow, Scotland, and her experience
                        inspired a passion for increasing students’ access to international opportunities.
                     </p>
                     
                     
                     
                     
                     
                     <p><br>
                        <strong>Vannia Ruiz<br>
                           <em>Administrative Assistant, Study Abroad Programs, Study Abroad and Global Experiences</em></strong><br>
                        
                     </p>
                     
                     <p></p>
                     
                     
                     
                     
                     <p><br>
                        <strong>Leila Ayala Camacho<br>
                           <em>Administrative Assistant, Curriculum Initiatives</em></strong><br>
                        <a href="mailto:mfigueroa@valenciacollege.edu">layalacamacho@valenciacollege.edu</a></p>
                     
                     <p>Leila Ayala Camacho is the Administrative Assistant of Curriculum Initiatives at Valencia
                        College. While being a Valencia College student, Leila was part of Phi Theta Kappa,
                        Honor Society and the Honors Program on East Campus. Leila graduated from Valencia
                        College, and completed her Bachelor’s program at the University of Central Florida.
                        Leila became a Valencia College staff member at the Academic Success Center, East
                        Campus on 2012. Leila is planning to start a Master’s program in order to continue
                        her education path in the near future. 
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><br>
                        <strong>Ellen Ayala<br>
                           <em>Staff Assistant, Florida Consortium for International Education</em></strong><br>
                        <a href="mailto:eayala16@valenciacollege.edu">eayala16@valenciacollege.edu</a></p>
                     
                     <p>Ellen joined Valencia in 2014 as a Staff Assistant for FCIE, where she assists the
                        Director of Study Abroad and Global Experiences, Jennifer Robertson. Ellen received
                        her Bachelor's degree in 2013 from Marian University, Indianapolis, and she will finish
                        her Master’s Degree in May of 2015 at the University of Central Florida. She assists
                        with a variety of administrative tasks for the office, FCIE Memberships, Conference
                        details, and more. She has previous work experience is in customer service and Housing
                        and Residence Life.
                     </p>
                     
                     <p>Ellen moved to Florida from Indiana in 2013. She loves to cook and is very passionate
                        about international education. She hopes to plan her first international trip in the
                        summer of 2016! 
                     </p>
                     
                     
                     
                     <hr>
                     
                     <h3>Honors Program</h3>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-cheryl-robinson-68x68.jpg" alt="Cheryl Robinson" class="img-circle"><br>
                        <strong>Cheryl Robinson<br>
                           <em>Director, Honors Program</em></strong><br>
                        <a href="mailto:vburks@valenciacollege.edu">crobinson@valenciacollege.edu</a></p>
                     
                     <p>Cheryl has been with Valencia since 1997 when she joined the team as a psychology
                        and Student Success adjunct instructor. Later that fall, she added the responsibilities
                        of career and educational advising to her repertoire.
                     </p>
                     
                     <p>In August 2000, she transitioned positions to become collegewide director of assessment.
                        In this role, she coordinated all the standardized testing on all campuses and facilitated
                        proctoring for local students taking classes elsewhere. She also previously served
                        as the program coordinator for The Center for High-Tech Training for Individuals with
                        Disabilities on the East Campus.
                     </p>
                     
                     <p>Cheryl joined the Winter Park Campus in November 2004 as the interim dean of students
                        and accepted the position permanently in July 2005. In this capacity, she was responsible
                        for all areas of Student Affairs, including admissions, registration, advising and
                        career exploration. She also coordinated online advising collegewide and was one of
                        Valencia’s first Certified Digital Professors.
                     </p>
                     
                     <p>Cheryl has worked on the collegewide General Education Communications and Planning
                        Team, as well as the Developmental Education Task Force. She has also served on the
                        collegewide Curriculum Committee since 2007 and is the co-editor of the College catalog.She
                        has continued teaching and currently serves as an adjunct instructor for education.
                     </p>
                     
                     <p>Cheryl earned her bachelor’s degree from the University of South Florida in elementary
                        education and her Master of Social Work and Doctor of Education from the University
                        of Central Florida.
                     </p>
                     
                     
                     
                     <hr>
                     
                     
                     <p><img src="/_resources/img/academics/academic-affairs/curriculum-assessment/thumb-b-clyburn-68x68.jpg" alt="B. Clyburn" class="img-circle"><br>
                        <strong>B. Clyburn<br>
                           <em>Program Assistant, Honors</em></strong><br>
                        <a href="mailto:bclyburn@valenciacollege.edu">bclyburn@valenciacollege.edu</a></p>
                     
                     <p>B. Clyburn is the Honors Program Assistant for the Seneff Honors College college-wide
                        and she provides administrative support to faculty, staff, and students. B. Clyburn
                        has been at Valencia College since March 1996.
                     </p>
                     
                     
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/curriculum-assessment/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>