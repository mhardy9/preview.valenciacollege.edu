<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Career &amp; Workforce Education </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <p class="col-md-12"><a id="content" name="content"></a></p>
                     
                     <h2>Meet the Staff</h2>
                     
                     <h3>Career &amp; Workforce Education</h3>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-nasser-hedayat-68x68.jpg" alt="Dr. Nasser Hedayat"><br> <strong>Dr. Nasser Hedayat<br> <em>AVP, Career &amp; Workforce Education</em></strong><br> <strong>Office</strong>: District Office Room 310 <br> <strong>Mail Code</strong>: DO-32 <br> <strong>Phone</strong>: 407-582-3326<br> <a href="mailto:nhedayat@valenciacollege.edu">Contact Dr. Nasser Hedayat</a></p>
                     
                     <p>He holds a Master in Science degree in Electrical Engineering from Florida Institute
                        of Technology, and a Doctoral degree in Higher Education Leadership from NOVA Southeastern
                        University. He joined Valencia College in 1992 as a faculty member of the Electronics
                        Engineering Technology program and assumed the Program Chair responsibilities for
                        Engineering, Computer Engineering Technology, and Electronics Engineering Technology
                        programs. In 2007, Dr. Hedayat became the Division Dean of Architecture, Engineering,
                        and Technology at Valencia's West Campus and since July 2011 he has assumed the role
                        of Assistant Vice President for Career and Workforce Education at Valencia College.
                        Dr. Hedayat enjoys building relationships with both internal and external community
                        leaders; building on current strengths, creating pathways for growth, and many other
                        opportunities. He also enjoys golfing.
                     </p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-lesena-jones-68x68.jpg" alt="LeSena Jones"><br> <strong>Ms. LeSena Jones<br> <em>Manager of Career &amp; Workforce Education</em></strong><br> <strong>Office</strong>: District Office Room 308 <br> <strong>Mail Code</strong>: DDO-32 <br> <strong>Phone</strong>: 407-582-3344<br> <a href="mailto:lljones@valenciacollege.edu">Contact LeSena Jones</a></p>
                     
                     <p>LeSena started working at Valencia in 1992 and has held numerous roles in her tenure
                        at the college. She is currently the Manager for Career and Workforce Education.
                     </p>
                     
                     <p>LeSena is responsible for planning and implementing projects and activities that help
                        promote and support Valencia's Career and Workforce Education programs including:
                        the development of promotional materials for the career programs, overseeing the five-year
                        program review process for the college's AS degree and certificate programs, developing
                        articulation agreements that provide pathway opportunities into Valencia's career
                        programs, managing an innovative program for faculty and staff to gain industry knowledge
                        and experience, overseeing the process for awarding technical certificates to students,
                        and planning industry roundtables and forums to address workforce training and employment
                        needs.
                     </p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-michelle-terrell-68x68.jpg" alt="Michelle Terrell"><br> <strong>Ms. Michelle Terrell<br> <em>Director of Internships &amp; Workforce Services</em></strong><br> <strong>Office</strong>: District Office Room 307<br> <strong>Mail Code</strong>: DO-32 <br> <strong>Phone</strong>: 407-582-3328<br> <a href="mailto:mterrell8@valenciacollege.edu">Contact Michelle Terrell </a></p>
                     
                     <p>Michelle first worked for Valencia in various positions from 1999 – 2005 and returned
                        to the Career and Workforce Education team at Valencia in 2015. She is currently serving
                        as the Director of Internship and Workforce Services. Michelle has spent the past
                        23 years serving and assisting students in the areas of workforce education, college
                        and career planning, and graduate education.
                     </p>
                     
                     <p>Michelle holds a Master in Science degree in Counseling from Alabama State University,
                        a Master in Arts degree in Human Resources Management from Webster University, and
                        a Bachelor of Science degree in Psychology from Troy University.
                     </p>
                     
                     <p>In her spare time, Michelle enjoys traveling, reading, spending time with family and
                        volunteering.
                     </p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-vicki-fine-68x68.jpg" alt="Vicki Fine"><br> <strong>Ms. Vicki Fine<br> <em>Manager for Career Pathways</em></strong><br> <strong>Office</strong>: District Office Room 329<br> <strong>Mail Code</strong>: DO-337<br> <strong>Phone</strong>: 407-582-3485<br> <a href="mailto:vfine@valenciacollege.edu">Contact Vicki Fine </a></p>
                     
                     <p>Vicki joined Valencia College in 2012 as a coordinator in Conferencing and College
                        Events where she was actively involved with college-wide, department, and community
                        events as well as Valencia hosted conferences.&nbsp; Vicki has now joined the Career and
                        Workforce Education team as the Coordinator of Career Pathways.&nbsp; Vicki works with
                        the Career Pathways Consortium, which is Valencia College and Orange and Osceola County
                        school districts, to build the pathways from secondary to post-secondary education
                        for students participating in Career Pathways who are interested in attending Valencia
                        College.&nbsp; Career Pathways is part of the Perkins Grant.&nbsp;
                     </p>
                     
                     <p>Vicki brings her collaboration and organizational skills to this position along with
                        over ten years of experience serving faculty, staff, and students in higher education.
                        She holds a Master in Business Administration and Bachelor of Business Administration
                        from Georgia Southern University.&nbsp; Vicki also received her certification as a Professional
                        in Human Resources (PHR) in 2013 and was a graduate from the 2014 PIVOT 180 Leadership
                        Academy.
                     </p>
                     
                     <p>Vicki enjoys helping others develop professionally, reading on the beach, and spending
                        time with her family in Apopka, FL.
                     </p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-anjela-madison-68x68.jpg" alt="Anjela Madison"><br> <strong>Ms. Anjela Madison<br> <em>Perkins Grant Director</em></strong><br> <strong>Office</strong>: District Office Room 324<br> <strong>Mail Code</strong>: DO-32 <br> <strong>Phone</strong>: 407-582-3488<br> <a href="mailto:amadison1@valenciacollege.edu">Contact Anjela Madison</a></p>
                     
                     <p>In 1987, Anjela began her service to higher education in Pensacola, Florida. As she
                        transitioned to Central Florida, she continued that service by securing employment
                        with Valencia in 2001. She holds a bachelor's and master's degree from the University
                        of Central Florida.
                     </p>
                     
                     <p>She currently serves at the Perkins Grant Director.</p>
                     
                     <p>She enjoys spending time with family.</p>
                     
                     <p>&nbsp;</p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-cathy-campbell-68x68.jpg" alt="Cathy Campbell"><br> <strong> Ms. Cathy Campbell<br> <em>Career and Workforce Education&nbsp;Analyst</em></strong><br> <strong>Office</strong>: District Office Room 305B <br> <strong>Mail Code</strong>: DO-32<br> <strong>Phone</strong>: 407-582-3321<br> <a href="mailto:ccampbell@valenciacollege.edu">Contact Cathy Campbell</a></p>
                     
                     <p>Cathy has been with Career and Workforce Education since 1994. Cathy's primary responsibilities
                        include baccalaureate program development and work in Tableau for the Program Growth,
                        Viability and Success annual program reviews working in collaboration with Institutional
                        Research staff. She is responsible for employment demand studies related to potential
                        and existing programs at Valencia College. Designing presentations for various uses
                        collegewide and abroad makes her AS degree in Graphic Design from Valencia a valuable
                        asset.
                     </p>
                     
                     <p>Her creative efforts aren't limited to work alone. She enjoys sewing, gardening, crocheting,
                        knitting, pastel drawing, baking, food preservation, photography and even a bit of
                        research for friends and family.
                     </p>
                     
                     <hr>
                     
                     <p><img class="img-circle" src="/_resources/img/academics/academic-affairs/career-workforce-education/thumb-anissa-mohun-68x68.jpg" alt="Anissa Mohun"><br> <strong>Ms. Anissa Mohun<br> <em>Career and Workforce Education Specialist</em></strong><br> <strong>Office</strong>: District Office Room 312C <br> <strong>Mail Code</strong>: DO-32<br> <strong>Phone</strong>: 407-582-3158<br> <a href="mailto:amohun@valenciacollege.edu">Contact Anissa Mohun</a></p>
                     
                     <p>Anissa joined Career and Workforce Education in July 2016 after serving as an Administrative
                        Assistant for the Business, IT &amp; Public Services office on Valencia's East Campus.
                     </p>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>