<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/research-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Research Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Career and Workforce Research Services</h2>
                        
                        <p>Student data requests go directly through Institutional Research. Here our work is
                           to contribute employment data, information, and research to aid in the strengthening,
                           enhancing and improving the outcomes of Valencia's Career and Workforce Education
                           Programs to ensure they are meeting the needs and demands of our local workforce region.
                        </p>
                        
                        <h3>Documents available for Download</h3>
                        
                        <ul>
                           
                           <li><a href="/academics/academic-affairs/career-workforce-education/research-services/documents/2013-2014FinalTargetedOccupationsListRegion12.pdf" target="_blank">2013/2014 Final Targeted Occupations List</a></li>
                           
                           <li><a href="/academics/academic-affairs/career-workforce-education/research-services/documents/11-12ReportingYearPVGSPublication10.02.2012FINALsm.pdf" target="_blank">2011/2012 Program Viability Growth &amp; Success Handbook</a></li>
                           
                        </ul>
                        
                        <h3>Upcoming Presentations</h3>
                        <a href="http://net4.valenciacollege.edu/forms/adademic-affairs/career-workforce-education/research-services/work-request-form.cfm" target="_blank">Work Request Form</a><hr>
                        
                        <h3>Educational Resources</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.fldoe.org/workforce/dwdframe/">Curriculum Frameworks</a></li>
                           
                           <li><a href="http://nces.ed.gov/collegenavigator/">National Center for Education Statistics - College Navigator</a></li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Career Program Data</h3>
                        
                        <ul>
                           
                           <li><a href="http://preview.valenciacollege.edu/academic-affairs/career-workforce-education/research-services/documents/FETPIPCharts1112.pdf">FETPIP Earnings Chart </a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/academic-affairs/career-workforce-education/research-services/documents/FETPIP2011-12cubed.pdf">FETPIP Cubed Earnings Chart</a></li>
                           
                           <li><a href="http://smart-college-choices.com/">Florida Department of Education Student Outcomes Reference</a></li>
                           
                           <li><a href="http://www.fldoe.org/fetpip/">Florida Education and Training Placement Information Program (FETPIP)</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/academic-affairs/career-workforce-education/research-services/documents/FinalTwoPageESRSummary-4-14-14.docx">Florida Department of Education Economic Security Report Summary</a></li>
                           
                           <li><a href="http://www.beyondeducation.org/" target="_blank">Beyond Education</a></li>
                           
                           <li><a href="http://www.fldoe.org/fcs/pdf/FCS-EmploymentEarningsEducation.pdf" target="_blank">Florida Department of Education Employment Earnings</a></li>
                           
                           <li><a href="http://www.fldoe.org/fcs/xls/FCS-EmploymentEarningsEducation.xls" target="_blank">Florida Department of Education Employment Earnings</a></li>
                           
                        </ul>
                        
                        <p>In Atlas by clicking <strong>Five-year Program Review</strong> on the Faculty tab you can access the following pages.
                        </p>
                        
                        <ul>
                           
                           <li>Program Review</li>
                           
                           <li>Viability</li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Employment Information and Projections</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.floridajobs.org/labor-market-information/data-center/statistical-programs/employment-projections">Employment Projections</a></li>
                           
                           <li><a href="http://tol.labormarketinfo.com/">Targeted Occupations</a><br><small>Username and password are both <strong>guest</strong></small></li>
                           
                           <li><a href="http://www.whatpeopleareasking.com/index.shtm">What People are Asking</a></li>
                           
                           <li><a href="http://www.onetonline.org/">O*NET Online</a></li>
                           
                           <li><a href="http://www.mynextmove.org/">My Next Move</a></li>
                           
                           <li><a href="http://www.bls.gov/ooh/">Bureau of Labor Statistics Occupational Outlook Handbook (online)</a></li>
                           
                           <li><a href="https://www.employflorida.com/vosnet/Default.aspx">Employ Florida</a></li>
                           
                           <li><a href="https://www.employflorida.com/portals/veteran/Default.asp">Employ Florida Veterans</a></li>
                           
                           <li><a href="https://www.employflorida.com/vosnet/youth.aspx">Employ Florida Youth Services</a></li>
                           
                           <li><a href="https://www.employflorida.com/portals/silver/Default.asp">Employ Florida Silver Edition</a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/research-services/index.pcf">©</a>
      </div>
   </body>
</html>