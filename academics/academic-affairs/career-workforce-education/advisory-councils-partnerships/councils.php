<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/advisory-councils-partnerships/councils.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/advisory-councils-partnerships/">Advisory Councils Partnerships</a></li>
               <li>Career &amp; Workforce Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Advisory Councils</h2>
                        
                        <p>Valencia's advisory councils are designed to assist the college and its staff in the
                           long-range planning of programs, reviewing curriculum for relevance to the changing
                           needs of the community, and in  providing insight into the employment needs in Orange
                           and Osceola counties. Advisory Councils help the college to create cooperative relationships
                           with local businesses, professional organizations, government agencies, and corporations.
                           Valencia has 31 Advisory Councils with over 600 members from business and industry
                           representing over 325 different companies. These Advisory Councils play a crucial
                           role in assisting us in the long-range planning of career programs, curriculum review
                           and validation, providing insight into the employment needs and trends in Orange and
                           Osceola Counties, and helping to ensure that our Career and Workforce Education programs
                           remain relevant to the changing needs of the community.  The significance of the work
                           of Advisory Councils is in fully engaging the interdependence economic development
                           and academic training of a quality workforce.  With the expertise of academicians
                           and practitioners, Valencia strives to ensure that graduates will be capable of performing
                           technician-level skills effectively in an ever-changing job market.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://net4.valenciacollege.edu/forms/academic-affairs/career-workforce-education/advisory-councils-partnerships/advisory-council-membership-form.cfm" target="_blank">Advisory Council Membership Form</a>
                        <a href="councils.html">Current Advisory Councils</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/advisory-councils-partnerships/councils.pcf">©</a>
      </div>
   </body>
</html>