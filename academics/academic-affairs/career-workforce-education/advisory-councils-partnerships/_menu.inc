<ul>
              <li><a href="/academics/academic-affairs/career-workforce-education/index.php">Career &amp; Workforce Education</a></li>
                <li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
               <ul>
              <li><a href="/academics/academic-affairs/career-workforce-education/meet-the-staff.php">Meet the Staff</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/affairs.php"> Career &amp; Workforce Education Affairs</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/program-planning-development/index.php"> Program Planning &amp; Development</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/program-review/index.php">Program Review</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/research-services/index.php"> Career &amp; Workforce Research Services</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/advisory-councils-partnerships/index.php">Advisory Councils &amp; Partnerships</a></li>
              <li><a href="/academics/academic-affairs/career-workforce-education/perkins-management-compliance/index.php">Perkins Management &amp; Compliance</a></li>
              </ul>
</li>
              </ul>
