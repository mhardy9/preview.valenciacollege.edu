<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li>Career Workforce Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>About Us</h2>
                        
                        <p>The office of Career and Workforce Education works to:</p>
                        
                        <ul>
                           
                           <li>Improve the quality and impact of instruction in career and technical education programs.</li>
                           
                           <li>Prepare knowledgeable workforce for the 21st century workplace</li>
                           
                           <li>Support and strengthen collaborative work in the community</li>
                           
                           <li>Articulate short-term and long-term goals and objectives</li>
                           
                           <li>Provide statistical and descriptive information internally and externally</li>
                           
                           <li>Communicate goals, strategies and accomplishments</li>
                           
                           <li>Maintain active relationships with business, industry and education partners</li>
                           
                        </ul>
                        
                        <p>We use a sustainable model that takes commitment, recognizes and builds strengths
                           between business/industry, education systems, students and the community. We promote
                           partnerships to facilitate economic growth and strategic outcomes.
                        </p>
                        
                        <p>These partnerships are more than relationships providing opportunities in such areas
                           as:
                        </p>
                        
                        <ul>
                           
                           <li>Internships, Job Shadowing Experiences, and Focus on the Workplace opportunities for
                              career development for students and faculty
                           </li>
                           
                           <li>Career Pathways in secondary to postsecondary career and workforce education</li>
                           
                           <li>Advisory Council affiliations</li>
                           
                           <li>Employment and feedback on graduate skills</li>
                           
                           <li>Insight on future trends</li>
                           
                           <li>Customized Curriculum Development (DACUMs)</li>
                           
                           <li>Program Review and Assessment methods</li>
                           
                           <li>Equipment evaluation</li>
                           
                           <li>Scholarships and In-Kind donations</li>
                           
                        </ul>
                        
                        <p>From the start, Career and Workforce Education at Valencia defines what is desired
                           from our work. We identify key stakeholders in the economic ecosystem. We strive to
                           meet the needs of industry with great visions to promote great partnerships.
                        </p>
                        
                        <p>A great partner community will produce great outcomes. High student placement rates
                           and wages are just a part of the economic impact on our community. Please <a href="/academics/academic-affairs/career-workforce-education/meet-the-staff.php">contact us</a> for more information on how we can address your business and industry needs. We took
                           the community out of our name but not out of our work.
                        </p>
                        
                        <hr>
                        
                        <h3>Mission</h3>
                        
                        <p>The Career &amp; Workforce Education Office works cooperatively collegewide and in partnership
                           with business/industry and the community to develop and strengthen learning opportunities
                           for students that prepare them for a successful transition from college to the workplace
                           while addressing the economic development needs of our Central Florida community.
                           The office includes facets of assessment of workforce demands, program development
                           and review of performance and relevancy of programs to meet the workforce needs, work
                           based learning experiences for students and faculty/staff, placement services for
                           students and the articulation of programs.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/index.pcf">©</a>
      </div>
   </body>
</html>