<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/affairs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Career &amp; Workforce Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Career &amp; Workforce Education Affairs</h2>
                        
                        
                        <p>Our Office performs activities and functions that will  strengthen, enhance and improve
                           the outcomes of Career and Workforce Education  programs to ensure they are meeting
                           the needs and demands of our workforce  region. We respond to critical job needs emerging
                           in the workforce through  program modification, expansion and development.
                        </p>
                        
                        <h3>Program Growth and  Development</h3>
                        
                        <p>Direct the review of Regions' Targeted Occupational  Forecasting list to determine
                           the impact on Valencia's Career programs. Provide  placement and completion data,
                           occupational information and wage and earnings  data to be used for forecasting current
                           and future needs, program development  and expansion.
                        </p>
                        
                        <h4>Program Reviews and  Assessment</h4>
                        
                        <p> Manage and oversee the planning and development of five-year  program reviews, as
                           well as the annual program performance and assessment of  Valencia's Career and Workforce
                           Education programs.
                        </p>
                        
                        <h4>Partnerships with the  Community</h4>
                        
                        <p> Plan and coordinate Industry roundtables and forums with business  partners to discuss
                           workforce training and employment needs for program growth  and development. Provide
                           support for Valencia's Advisory Councils, which ensure  our Career and Workforce Education
                           programs remain current and relevant to  industry needs. Work with Business and Industry
                           partners to develop workplace  experiences for faculty and staff to gain industry
                           knowledge and experience,  and learn more about workforce needs and trends.
                        </p>
                        
                        <h4>Articulation and  Alternative Award of Credit</h4>
                        
                        <p> Develop and implement articulation agreements with  educational institutions that
                           provide opportunities for students to transition  into Career and Workforce Education
                           programs. Develop and oversee statewide  agreements for award of credit based on industry
                           certification or licensure.
                        </p>
                        
                        <h4>Technical Certificates</h4>
                        
                        <p> Manage and oversee the College process for issuing all  Technical Certificates awarded
                           to students each term.
                        </p>
                        
                        <h4>Promotion of Career  Programs</h4>
                        
                        <p> Develop collateral materials to promote and enhance Career  programs in collaboration
                           with Marketing and Media Relations and other  appropriate college staff. Work with
                           Catalog team in overseeing Career Program  information in College Catalog.&nbsp; Manage
                           and maintain College website for  all Career and Workforce Education programs.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/affairs.pcf">©</a>
      </div>
   </body>
</html>