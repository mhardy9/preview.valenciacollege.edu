<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/perkins-management-compliance/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Perkins Management Compliance</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Perkins Management &amp; Compliance</h2>
                        
                        <h3>Carl D. Perkins Career and Technical Education Act of 2006</h3>
                        
                        <p>The purpose of Perkins IV is to develop the academic and career and technical skills
                           of secondary and postsecondary students who elect to enroll in Career and Technical
                           Education programs. The goal is for all students to achieve challenging academic and
                           technical standards and be prepared for high-skill, high-wage, and/or high-demand
                           occupations in current or emerging professions.
                        </p>
                        
                        <h3>Major Themes in the Law</h3>
                        <a href="/academics/academic-affairs/career-workforce-education/perkins-management-compliance/images/perkins-chart.png"><img src="/academics/academic-affairs/career-workforce-education/perkins-management-compliance/perkins-chart.png" alt="" width="200" hspace="10" vspace="10"></a>
                        
                        <ul>
                           
                           <li>An integrated academic and career and technical education performance accountability
                              system that requires continuous student and program improvement at all levels.
                           </li>
                           
                           <li>Close association with business and industry</li>
                           
                           <li>Emphasis on preparation for postsecondary education AND employment.</li>
                           
                           <li>Focus on seamless transition from high school to postsecondary education.</li>
                           
                           <li>Increased emphasis on achievement of a degree, certificate or credential (including
                              licensure and industry certifications).
                           </li>
                           
                        </ul>
                        
                        <h3>Project and Services Supporting Career &amp; Workforce Education</h3>
                        
                        <h4>Personnel</h4>
                        
                        <ul>
                           
                           <li>Career Program Advisors - 18 positions</li>
                           
                           <li>Health Sciences - Advising &amp; Outreach Specialist</li>
                           
                           <li>Nursing Program Educational Specialist</li>
                           
                           <li>CTE Online Advising Support - 2 positions</li>
                           
                           <li>Transitions Coordinators - 2 positions</li>
                           
                           <li>Workplace Specialist</li>
                           
                           <li>Project Director</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                        <ul>
                           
                           <li>Career Pathways</li>
                           
                           <li>Perkins Director</li>
                           
                           <li>Perkins Records Specialist</li>
                           
                        </ul>
                        
                        <h4>Equipment</h4>
                        
                        <ul>
                           
                           <li>High Priority Program Equipment List (Over $1.2 million)</li>
                           
                        </ul>
                        
                        <h4>Transition Programs</h4>
                        
                        <ul>
                           
                           <li>Counselor Day</li>
                           
                           <li>Career Transition Camps for High School Students (East, Osceola, West)</li>
                           
                           <li>Career Carnivals</li>
                           
                           <li>Youth Academic Day for Special Populations</li>
                           
                           <li>Advising Workshops for Nursing &amp; Allied Health</li>
                           
                        </ul>
                        
                        <h4>Program Development &amp; Industry Certification</h4>
                        
                        <ul>
                           
                           <li>Program Improvement</li>
                           
                           <li>Recertification/Technical Assistance for High Priority Programs</li>
                           
                           <li>Business &amp; Industry Sector Forums</li>
                           
                        </ul>
                        
                        <h4>Faculty Development &amp; Assessment</h4>
                        
                        <ul>
                           
                           <li>Focus on the Workplace</li>
                           
                        </ul>
                        
                        <h4>Career Planning</h4>
                        
                        <ul>
                           
                           <li>Career Planning and Student Outreach</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/perkins-management-compliance/index.pcf">©</a>
      </div>
   </body>
</html>