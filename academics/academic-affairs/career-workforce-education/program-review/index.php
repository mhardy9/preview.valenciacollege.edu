<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/program-review/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Program Review</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Program Review</h2>
                        
                        <p>In general terms, program review is a regularly scheduled process designed to improve
                           the quality of curricular and co-curricular program areas and educational support
                           service units.
                        </p>
                        
                        <p>Depending on the area being reviewed, this process may involve staff, students, faculty,
                           alumni, community members, administrators, and external specialists in gathering information
                           about a unit, reviewing and analyzing the information collected, making judgments
                           about overall quality and recommendations for improvement, and following up to ensure
                           that the unit is fully supported in its efforts to address the outcomes of the review.
                        </p>
                        
                        <p>In order to ensure that program review at Valencia College is a reflection of our
                           learning-centered focus, the process that is described in this document is built upon
                           the following principles:
                        </p>
                        
                        <ul>
                           
                           <li>The review of curricular and co-curricular program areas, and educational support
                              service units, and recommendations for improvement, reflect the unit's connection
                              to the Mission, Vision, and Values of Valencia College.
                           </li>
                           
                           <li>The review process promotes an authentic connection / contribution to the College's
                              Strategic Plan.
                           </li>
                           
                           <li>The review process promotes program innovation and ownership by entrusting the process
                              to those most connected to the work.
                           </li>
                           
                           <li>The review process contributes to Valencia's long history of collaboration on important
                              matters and this history requires the involvement of staff and faculty representing
                              diverse internal constituencies.
                           </li>
                           
                           <li>The review process operates within an atmosphere of honest, objective, and candid
                              communication and feedback in order to ensure that the results are helpful to all
                              stakeholders.
                           </li>
                           
                           <li>The review process and recommendations for improvement focus on improving the results
                              / learning outcomes we want for our students.
                           </li>
                           
                        </ul>
                        
                        <hr>
                        
                        <h3>Important Documents</h3>
                        
                        <p>You can access the following forms in Atlas, by clicking <strong>Five-year Program Review (AS Degrees)</strong> on the Faculty tab.
                        </p>
                        
                        <ul>
                           
                           <li>Program Review 5 Year Schedule</li>
                           
                           <li>Program Review Instrument with Guidelines and Samples</li>
                           
                           <li>Program Review Instrument to Complete</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/program-review/index.pcf">©</a>
      </div>
   </body>
</html>