<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career &amp; Workforce Education  | Valencia College</title>
      <meta name="Description" content="We use a sustainable model that takes commitment, recognizes and builds strengths between business/industry, education systems, students and the community.">
      <meta name="Keywords" content="workforce education, career workforce, college, school, educational, curriculum, responsibility">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/career-workforce-education/program-planning-development/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/career-workforce-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Career &amp; Workforce Education</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/career-workforce-education/">Career Workforce Education</a></li>
               <li>Program Planning Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Program Planning &amp; Development for Degree and Certificate Programs</h2>
                        
                        <p>Valencia provides a continuous improvement plan to develop and modify B.S. Degrees,
                           A.S. Degrees and Certificate programs which includes the assessment and development
                           process for potential new programs, modification of existing programs, and the discontinuation
                           of programs so as to address and meet the needs of business/industry. The purpose
                           of this process is to respond to Business &amp; Industry Needs in order to:
                        </p>
                        
                        <ul>
                           
                           <li>Ensure programs address projected employment needs for Targeted Occupations (TOL)
                              and High Skill/High Wage Occupations (HSHW)
                           </li>
                           
                           <li>Confirm Alignment with Factors to Assess Business &amp; Industry Needs</li>
                           
                           <li>Provide Continuous Program Improvement to Ensure Program Relevancy to Business &amp; Industry
                              Needs
                           </li>
                           
                        </ul>
                        
                        <p>The planning and development process for programs is a collaborative process with
                           Faculty/Program Chairs, Deans, Campus Presidents, Vice Presidents, AVP for Career
                           &amp; Workforce Education, COO of Continuing Education, Business and Industry partners,
                           and others. The process includes various phases for continuous improvement, expansion
                           and growth:
                        </p>
                        
                        <h3>New A.S. Degree/Certificate Programs<br> <small>Program Planning, Design &amp; Development</small></h3>
                        
                        <ul>
                           
                           <li>Phase 1 - Exploration Process for Potential New Programs</li>
                           
                           <li>Phase 2 - Research Process for Potential New Programs</li>
                           
                           <li>Phase 3 - Design &amp; Development Process for New Programs</li>
                           
                        </ul>
                        
                        <h3>New Bachelors Programs<br> <small>Program Planning, Design &amp; Development</small></h3>
                        
                        <ul>
                           
                           <li>Phase 1 - Exploration Process for Potential New Programs</li>
                           
                           <li>Phase 2 - Research Process for Potential New Programs</li>
                           
                           <li>Phase 3 - Design &amp; Development Process for New Programs</li>
                           
                        </ul>
                        
                        <h3>Expansion of Degree and/or Certificate Programs</h3>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/career-workforce-education/program-planning-development/index.pcf">©</a>
      </div>
   </body>
</html>