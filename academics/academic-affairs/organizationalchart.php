<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Affairs &amp; Planning Organizational Charts  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/organizationalchart.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Academic Affairs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li>Academic Affairs &amp; Planning Organizational Charts </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Academic Affairs &amp; Planning Organizational Charts </h2>
                        
                        <p>
                           
                        </p>
                        
                        <ol class="list_style_1">
                           
                           <li><a href="documents/OrgChart-AALT6-6-16.pdf">Academic Affairs &amp; Planning Leadership Team (AALT) </a></li>
                           
                           <li><a href="documents/OrgChart-CWE6-6-16.pdf">Career &amp; Workforce Education</a></li>
                           
                           <li><a href="documents/OrgChart-CurrAssess6-6-16.pdf">Curriculum &amp; Assessment</a></li>
                           
                           <li><a href="documents/OrgChart-IR6-6-16.pdf">Institutional Research</a></li>
                           
                           <li><a href="documents/OrgChart-RD6-6-16.pdf">Resource Development </a></li>
                           
                           <li><a href="documents/OrgChart-TandL6-6-16.pdf">Teaching &amp; Learning </a></li>
                           
                           <li><a href="documents/OrgChart-TLA6-6-16.pdf">Teaching/Learning Academy </a></li>
                           
                           
                        </ol>
                        
                        
                        
                        <hr>
                        
                        
                        <h3>Academic Affairs Leadership Team (AALT)</h3>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Susan-Ledlow-thumb.png" alt="Dr. Susan Ledlow" class="img-circle"><br>
                           <strong>Dr. Susan Ledlow<br>
                              <em>Vice President of Academic Affairs &amp; Planning</em></strong></p>
                        
                        <p>Dr. Susan Ledlow is the Vice President of  <a href="index.html">Academic Affairs &amp; Planning</a>.
                        </p>
                        
                        <p>Dr. Ledlow oversees curriculum, articulation, assessment, career and workforce education,
                           faculty and instructional development, institutional research, resource development,
                           accreditation, strategic planning, and Valencia’s Teaching Learning Academy.<br>
                           <br>
                           Prior to joining Valencia, Dr. Ledlow was at Arizona State University (ASU) for 25
                           years, where she   held a variety of positions related to  faculty, professional,
                           and instructional development. She was also involved in the development of the curriculum
                           for ASU's new School of Sustainability, the first of its kind in the nation. 
                        </p>
                        
                        <p>Dr. Ledlow is a   graduate of Pensacola Junior College. She holds master's degrees
                           in anthropology and social psychology, and a Ph.D. in social  psychology. Her research
                           interests include social/resource dilemmas, quality of  life, and the psychology of
                           conservation and  sustainability. Her applied interests include group dynamics, team
                           development in the workplace and classroom, and the ethical use of social influence.
                           Her personal interests include reading, walking and playing  with her dog Lucy.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Kari-Makepeace-thumb.png" alt="Kari Makepeace" class="img-circle"><br>
                           <strong>Kari Makepeace<br>
                              <em>Coordinator of Academic Planning &amp; Support</em></strong></p>
                        
                        <p>Kari Makepeace is the Coordinator of Academic Planning &amp; Support.</p>
                        
                        <p>Kari holds a Master of  Business Administration degree in Management from Strayer
                           University. She  joined Valencia College in 2003 as a work study student in the Answer
                           Center.  Since that time she has worked in the  Office for Students with Disabilities,
                           in the Admissions &amp;  Records Department at East Campus, and as Executive Assistant
                           to the Vice President of Academic  Affairs &amp; Planning at the District Office. In September,
                           2014 Kari took over the  role of Coordinator of Academic Planning &amp; Support. This
                           includes coordination of the College Governance Councils, and working on special projects
                           for the VP of Academic Affairs &amp; Planning.
                        </p>
                        
                        <p> In her free  time, Kari enjoys spending time with her daughter, family, and friends.</p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Mary-Leppe-thumb.png" alt="Mary Leppe" class="img-circle"><br>
                           <strong>Mary Leppe<br>
                              <em>Senior Executive Assistant</em></strong></p>
                        
                        <p>Mary Leppe is the Senior Executive Assistant to Dr. Susan Ledlow in Academic Affairs
                           &amp; Planning.
                        </p>
                        
                        <p>Mary joined Valencia as an Administrative Assistant within in the Institutional Effectiveness
                           and Planning department in 2013 and took over the role of Executive Assistant to the
                           VP of Academic Affairs &amp; Planning in September, 2014. 
                        </p>
                        
                        <p>After moving from New York to Florida with her husband in 1985, Mary went to work
                           for the Walt Disney World Company where she worked for 25 years in various areas including,
                           Convention Sales, Special Events, Operations, Entertainment, and Catered Events.&nbsp;
                           In 2010 Mary went to work at the Central Florida Urban League and enjoyed her work
                           in the non-profit sector providing services such as youth development, economic services,
                           and workforce development for their clients.
                        </p>
                        
                        <p>Mary’s love for animals has kept her very busy for over 10 years by volunteering hundreds
                           of hours fostering and finding homes for countless orphaned canine and feline friends.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Nasser-Hedayat-thumb.png" alt="Dr. Nasser Hedayat" class="img-circle"><br>
                           <strong>Dr. Nasser Hedayat<br>
                              <em>Assistant Vice President of Career &amp; Workforce Education</em></strong></p>
                        
                        <p>Dr. Nasser Hedayat is the Assistant Vice President of <a href="career-workforce-education/index.html">Career &amp; Workforce Education</a>.
                        </p>
                        
                        <p> Dr. Hedayat holds a Master in Science degree in Electrical Engineering from Florida
                           Institute of Technology, and a Doctoral degree in Higher Education Leadership  from
                           NOVA Southeastern University. He joined Valencia College in 1992 as a  faculty member
                           of the Electronics Engineering Technology program and assumed  the Program Chair responsibilities
                           for Engineering, Computer Engineering  Technology, and Electronics Engineering Technology
                           programs. In 2007, Dr.  Hedayat became the Division Dean of Architecture, Engineering,
                           and Technology  at Valencia’s West Campus and since July 2011 he has assumed the role
                           of  Assistant Vice President for Career and Workforce Education at Valencia  College.
                        </p>
                        
                        <p> Dr. Hedayat enjoys building relationships with both internal and external community
                           leaders; building on current strengths, creating pathways for growth, and many  other
                           opportunities. He also enjoys golfing.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Susan-Ledlow-thumb.png" alt="Dr. Karen Borglum" class="img-circle"><br>
                           <strong>Dr. Karen Borglum<br>
                              <em>Assistant Vice President of Curriculum and Assessment</em></strong>
                           
                        </p>
                        
                        <p>Dr. Karen Borglum is the  Assistant Vice President of <a href="curriculum-assessment/index.html">Curriculum and Assessment</a>.
                        </p>
                        
                        <p> Prior  to this position, she was the Dean of Communications at Valencia’s West Campus,
                           and she also served as the Director of a Title III grant. Karen holds a  Bachelor
                           of Arts degree in Marketing, a Masters of Arts degree in  Communication, and a Doctor
                           of Education in Curriculum and Instruction from the  University of Central Florida,
                           in Orlando. Her professional development activities have included selection in the
                           Management Development Program (MDP) at Harvard University, the 2002 Florida  Community
                           College System Chancellor’s Leadership Program in Tampa, Florida, the  Leadership
                           Academy in Toronto, the Leadership Orlando series.
                        </p>
                        
                        <p> Dr.  Borglum has presented at state and national conferences in the areas of  curriculum,
                           instructional enhancement, and communities of practice. As part of her professional
                           practice, Dr.  Borglum has won a Bronze certificate for School-to-Work Best Teaching
                           Practice,  and designed a national communities of practice group through the American
                           Association for Higher Education. She  served as a lead QEP evaluator and as an onsite
                           reaffirmation reviewer for  SACS-COC, and she has also served, for two years, on the
                           SACS-COC Annual  Meeting Planning Committee. Additionally, she co-authored and article
                           in The Community College  Journal of Research and Practice entitled, “Academic and
                           Social Integration of  Community College Students: A case Study  (August 2000),” and
                           in 1996 she authored an article entitled, “What About  Academic Standards?” for Community
                           College Week and Community College Times.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Daryl-Davis-thumb.png" alt="Daryl Davis" class="img-circle"><br>
                           <strong>Daryl Davis<br>
                              <em>Director of Institutional Research</em></strong></p>
                        
                        <p>Daryl Davis is the Director of <a href="institutional-effectiveness-planning/institutional-research/index.html">Institutional Research</a>. 
                        </p>
                        
                        <p>Prior to this positon, he served the college for a number of years as a research analyst,
                           were he successfully collaborated with Valencia staff, faculty members and administrators
                           to develop a wide range of reports and data analysis, used throughout the college
                           to make data-informed decision on some of the college’s biggest initiatives, including
                           the Aspen Prize, Achieving the Dream, DEI and the QEP Core Team . While working in
                           this role, Daryl played a lead role in collaborating with the Office of Information
                           Technology to launch the college into a new era of reporting by rolling out the use
                           of tools like SAS Visual Analytics. 
                        </p>
                        
                        <p>Daryl attended the University of Central Florida, receiving a Bachelor’s of Science
                           in Business Management, with a focus on Hospitality Management. He also attended the
                           University of Phoenix, earning a Master’s of Science in Computer Information Systems.
                           His personal interests include exercise and basketball, in which he was inducted into
                           the UCF Athletic Hall of Fame in 2008, where he was a 4 year letterman in basketball.
                        </p>
                        
                        
                        <hr>
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Kristeen-Christian-thumb.png" alt="Kris Christian" class="img-circle"><br>
                           <strong>Kris Christian<br>
                              <em>Assistant Vice President of Resource Development</em>
                              </strong></p>
                        Kris Christian is the  Assistant Vice President of <a href="resource-development/index.html">Resource Development</a>.
                        
                        <p>A Wisconsin native, Kris joined the Valencia family in 2012 as Assistant Vice President
                           of the Resource Development Office. Kris has experience in both public and private
                           organizations. She is has been involved in all aspects of project development and
                           grant closeout. Kris has led teams of people throughout her career and knows that
                           the team process is the key component to development work. Kris has garnered millions
                           of dollars from federal and private funds and promotes the key institutional strategic
                           directions. 
                        </p>
                        
                        <p>  Kris attended Marian University in Fond du Lac, WI receiving a Bachelor’s of Arts
                           in Education with certification P-8 and the University of Wisconsin Milwaukee earning
                           a Master’s degree in Administrative Leadership and Supervision in Education. 
                        </p>
                        
                        <p>Kris is happy to be in Florida where the air is normally warm and the skies are blue.
                           Her hobbies include fishing, biking, eating healthy, spending time with family / friends
                           and most recently going to the beach! Kris is happy to be part of this innovative,
                           progressive College where work is student centered and those who work here know they
                           make a difference! 
                        </p>
                        
                        <hr>
                        
                        <p><img src="/_resources/img/academics/academic-affairs/AALT-Wendi-Dew-thumb.png" alt="Wendi Dew" class="img-circle"><br>
                           <strong>Wendi Dew<br>
                              <em>Assistant Vice President of Teaching and Learning</em>
                              </strong></p>
                        Wendi Dew is the Assistant Vice President for <a href="../faculty/development/index.html">Teaching and Learning</a>. 
                        
                        <p>Wendi is responsible for leading the college’s faculty and instructional development
                           programs for full-time and part-time faculty members, the broad strategy for online/hybrid
                           teaching and learning, and the development and implementation for strategic teaching
                           and learning initiatives.&nbsp; In this and previous roles at Valencia, she has been instrumental
                           the development and expansion of Valencia’s comprehensive, competency-based faculty
                           development programs; the advancement of outcomes-based practice and program learning
                           outcomes assessment; and implementation of quality improvement and excellence initiatives
                           for online teaching and learning. She also has extensive experience with the design
                           and implementation of student learning outcomes, curriculum development for all instructional
                           modalities, course and program assessment, and evidence-based improvement across two-year
                           general education and career technical education programs. 
                        </p>
                        
                        <p>As a tenured faculty member at Valencia for over ten years, her work and research
                           interests included grant-funded projects for the development of career-technical programs,
                           integration of learning technologies, and the use of high-fidelity simulation in allied
                           health programs. Additionally, she has consulted with colleges across the country
                           on issues related to faculty development, program learning outcomes assessment, strategic
                           planning, and the improvement of teaching and learning.
                           
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/organizationalchart.pcf">©</a>
      </div>
   </body>
</html>