<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resource Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/resource-development/grant-management.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/resource-development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Resource Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/resource-development/">Resource Development</a></li>
               <li>Resource Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Grant Management</h2>
                        
                        <p>The Resource Development Office offers support and guidance throughout the grant life
                           cycle. Directing a grant presents a unique set of opportunities and challenges. It's
                           important to have a complete understanding of the grant proposal, the award notice
                           and terms of the grant, as well as Valencia College policies and procedures. 
                        </p>
                        
                        <p>The Resource Development Office (RDO) staff and the Grants Finance department will
                           support the project director and will provide assistance, playing an integral role
                           in the project implementation from a compliance and financial perspective. 
                        </p>
                        
                        <p>The Grants Management Handbook is a guide providing user-friendly information to administrators,
                           project directors, and staff. Other resources are:
                        </p>
                        
                        <p><a href="https://atlas.valenciacollege.edu/">Project Director training</a> is now available through the EDGE. Please note that If you are a new project director,
                           you will be required to complete the training. 
                        </p>
                        
                        <p>The Valencia Policy Manual: <a href="../../about/general-counsel/policy/index.html">http://preview.valenciacollege.edu/generalcounsel/policy/</a></p>
                        
                        <p>A complete list of GL Codes: <a href="../../procurement/index.html">http://preview.valenciacollege.edu/procurement/</a></p>
                        
                        <p>Human Resources:  <a href="../../hr/index.html">http://preview.valenciacollege.edu/hr/</a>  
                        </p>
                        
                        <p>For more information about Grant Management, please contact Lashon Henderson at <a href="mailto:lhenderson22@valenciacollege.edu">lhenderson22@valenciacollege.edu</a> or call 407-582-2911.
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Contact Information</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administration</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:kchristian6@valenciacollege.edu">Kristeen Christian</a></div>
                                 
                                 <div data-old-tag="td">407-582-2909</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:cfrazier13@valenciacollege.edu">Channing Frazier</a></div>
                                 
                                 <div data-old-tag="td">407-582-2906</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administrative Assistant</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:mvelasquez17@valenciacollege.edu">Madeline Velasquez</a></div>
                                 
                                 <div data-old-tag="td">407-582-3342</div> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Proposal Development</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ayadav@valenciacollege.edu">Allie Yadav</a></div>
                                 
                                 <div data-old-tag="td">407-582-2902</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ktaylor3@valenciacollege.edu">Kimberly Taylor</a></div>
                                 
                                 <div data-old-tag="td">407-582-3320</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:nmallison@valenciacollege.edu">Nancy Mallison</a></div>
                                 
                                 <div data-old-tag="td">407-582-3330</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Management</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:lhenderson22@valenciacollege.edu">Lashon Henderson</a></div>
                                 
                                 <div data-old-tag="td">407-582-2911</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Location</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">District Office<br>
                                    PO Box 3028<br>
                                    Orlando, FL 32802-3028
                                 </div>
                                 
                                 <div data-old-tag="td">DO-34</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/resource-development/grant-management.pcf">©</a>
      </div>
   </body>
</html>