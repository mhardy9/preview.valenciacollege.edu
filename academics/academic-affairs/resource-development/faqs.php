<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resource Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/resource-development/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/resource-development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Resource Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/resource-development/">Resource Development</a></li>
               <li>Resource Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        
                        <h3>Grant Development</h3>
                        
                        
                        <div>
                           
                           
                           <h3>Is it necessary to work with the Resource Development (RDO) office in order to submit
                              a grant proposal?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Yes, the RDO is the point of contact for all College grant-seeking efforts, to evaluate
                                    grant ideas and funding opportunities, to assess the likelihood of success, to coordinate
                                    proposal development, and to ensure administrative staff approval for grants is obtained.
                                    In addition, RDO staff has specific expertise that will increase the competitiveness
                                    of grant proposals. Staff has experience composing federal, state and private foundation
                                    grant proposals, and brings as much enthusiasm to crafting a million dollar proposal
                                    as they do to a thousand dollar one. Finally, many college staff have remarked how
                                    working with the RDO made the process easier and resulted in a better finished product
                                    they felt confident submitting for funding.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>I have an idea for a grant, what do I do now?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Start by reviewing the <a href="documents/10302012processflowchart.pdf">Grant Process Diagram</a>. This will give you a good idea of Valencia’s process for developing and approving
                                    grants. Whether your idea is in response to a specific grant announcement or request
                                    for proposals (RFP) or whether it is a new idea still under development, complete
                                    the <a href="documents/PIW_083012final.pdf">Project Idea Worksheet</a> and email it to <a href="mailto:grantsadmin@valenciacollege.edu">grantsadmin@valenciacollege.edu.</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Why is the grants process so complex?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>A grant is an obligation and commitment by the College, so due diligence is needed
                                    to ensure that appropriate individuals are aware of and approve of a grant before
                                    it is developed and submitted to the funder. A number of issues are considered during
                                    the approval process, including the likelihood of funding, ability to charge indirect
                                    costs, alignment with the College’s strategic goals, availability of matching funds,
                                    and requirements to continue the project after the funding ends. (See the <u><a href="documents/10302012processflowchart.pdf">Grant Process Diagram</a></u><a href="GrantDev.html">)</a>. Following this process helps to ensure that you don’t spend a lot of time developing
                                    a grant proposal that is not internally approved or submitted. There are times when
                                    the grants process can be expedited; however, this is the exception rather than the
                                    rule.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>I just found out about a grant that is due in a few weeks. Can we submit a proposal?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Call the RDO immediately to discuss the grant opportunity. Some grants are simpler
                                    to develop and the process may be expedited. However, a large federal grant usually
                                    requires months of planning and development in order to produce a competitive proposal.
                                    Once again, approvals are needed.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Can I write my own grant?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Ultimately, the RDO staff will prepare and submit all applications on behalf of the
                                    College; however, as a content area expert, your assistance is needed to develop a
                                    competitive proposal.  After meeting with the RD staff and working through the approval
                                    process, the actual writing may be a blend of content developed by both parties. The
                                    RDO staff will seek guidance and final approval from appropriate individuals and will
                                    oversee all final edits to the documents and submission.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>My department has been asked to partner on a grant proposal submitted by another agency
                              and/or to provide a letter of support for another agency’s grant. Do I need to contact
                              the RDO?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Yes. All grant related items are processed through the RDO. If you’ve been asked to
                                    partner on a grant or provide a letter of support/commitment, please contact us immediately
                                    for assistance. Be prepared to answer questions regarding the level of involvement
                                    by individuals or the College.  Please do not commit the College to any activity without
                                    checking with your Supervisor/Dean. Working with you and your supervisor/dean, the
                                    RDO staff will process the partnership letter or proposal.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How long does it take to learn about whether a grant has been accepted for funding?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>It depends on the funder, but a federal grant can take as long as six months to learn
                                    about an award.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <p>Still have questions about Grant Development? Call the Resource Development Office
                           407-582- 2906.
                        </p>
                        
                        
                        
                        <h3>Grant Management</h3>
                        
                        
                        <div>
                           
                           
                           <h3>I've been named as project director for a new grant, now what?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Resource Development is committed to support each project director in their efforts
                                    to achieve the goals and objectives of their grant if at any time you have a question
                                    or problem implementing your project you should feel free to call upon us for assistance.
                                    Once an award notice from a funder is received by the College, an initial grants management
                                    meeting will be scheduled.  Members of the project staff, grant finance and Resource
                                    Development will be in attendance.  During this meeting the highlights of the grant-
                                    including the objectives, reporting and budget requirements, activity implementation,
                                    and College grant management protocol will be reviewed.  The goal of this first meeting
                                    is to clearly define the responsibilities and expectations (of both the funder and
                                    the College) associated with the management of the grant.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>What is the single most important thing I should do as a new project director?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Be familiar with your approved grant proposal.  Your proposal is your blueprint of
                                    what you should do, when it needs to be done, who is responsible for it, and what
                                    you should accomplish.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I know what is allowable under a grant?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Every grant program has a unique set of requirements and regulations.  The best place
                                    to get started is to look at your approved proposal.  If your proposal does not clearly
                                    state that you can do or purchase something, don’t assume it is allowable before you
                                    check with the Resource Development or Grants Accounting office.  Remember just because
                                    it’s a grant, it doesn’t mean that it is exempt from the College policies and procedures.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How should I respond to a funder asking for information about my grant?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Before you respond to any inquiry please contact Resource Development or Grants Accounting.
                                    We can often help “decode” what the funder wants and help to draft a response.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Is a grant budget different from a College budget?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In most cases the answer is yes.  Most grant budgets come with very specific regulations
                                    that may restrict the transfers between budget categories, prohibit the purchase of
                                    certain goods, or services, and limit the specific timeframe when the funds are available.
                                    In many cases, modifications to the originally approved budget requires prior approval
                                    from the funder.  This approval process (coordinated by the RDO) can be very detailed
                                    and may require several months to attain. Please always consult with Resource Development
                                    and Finance when you have restrictions that require prior approval.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>What are the reporting requirements related to my grant?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Because every grant program is different, funders will have different reporting requirements.
                                    However, the reporting process for all grants is coordinated and submitted by the
                                    College.  Grant accounting will process and submit all financial reports.  Program
                                    reporting is coordinated by the RDO.   Normally, project directors submit a draft
                                    report to Resource Development two weeks prior to the due date.  Resource Development
                                    will then submit the report for an internal review process which includes approval
                                    from the administrator(s) responsible for the grant, and if required, will then obtain
                                    the signature of the President.  The report is then submitted by Resource Development
                                    on behalf of the College.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <p>Still have questions about Grant Management? Call the Resource Development Office
                           at 407-582-2911.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Contact Information</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administration</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:kchristian6@valenciacollege.edu">Kristeen Christian</a></div>
                                 
                                 <div data-old-tag="td">407-582-2909</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:cfrazier13@valenciacollege.edu">Channing Frazier</a></div>
                                 
                                 <div data-old-tag="td">407-582-2906</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administrative Assistant</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:mvelasquez17@valenciacollege.edu">Madeline Velasquez</a></div>
                                 
                                 <div data-old-tag="td">407-582-3342</div> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Proposal Development</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ayadav@valenciacollege.edu">Allie Yadav</a></div>
                                 
                                 <div data-old-tag="td">407-582-2902</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ktaylor3@valenciacollege.edu">Kimberly Taylor</a></div>
                                 
                                 <div data-old-tag="td">407-582-3320</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:nmallison@valenciacollege.edu">Nancy Mallison</a></div>
                                 
                                 <div data-old-tag="td">407-582-3330</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Management</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:lhenderson22@valenciacollege.edu">Lashon Henderson</a></div>
                                 
                                 <div data-old-tag="td">407-582-2911</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Location</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">District Office<br>
                                    PO Box 3028<br>
                                    Orlando, FL 32802-3028
                                 </div>
                                 
                                 <div data-old-tag="td">DO-34</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/resource-development/faqs.pcf">©</a>
      </div>
   </body>
</html>