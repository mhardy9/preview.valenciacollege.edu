<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resource Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/academics/academic-affairs/resource-development/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/academics/academic-affairs/resource-development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Resource Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/academics/">Academics</a></li>
               <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
               <li><a href="/academics/academic-affairs/resource-development/">Resource Development</a></li>
               <li>Resource Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Meet the Staff</h2>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <strong>Kristeen Christian</strong><br>
                                    
                                    <p>A Wisconsin native, Kris joined the Valencia family in 2012 as Assistant Vice President
                                       of the Resource Development Office. Kris has experience in both public and private
                                       organizations. She is has been involved in all aspects of project development and
                                       grant closeout. Kris has led teams of people throughout her career and knows that
                                       the team process is the key component to development work. Kris has garnered millions
                                       of dollars from federal and private funds and promotes the key institutional strategic
                                       directions. 
                                    </p>
                                    
                                    <p>Kris attended Marian University in Fond du Lac, WI receiving a Bachelor's of Arts
                                       in Education with certification P-8 and the University of Wisconsin Milwaukee earning
                                       a Master's degree in Administrative Leadership and Supervision in Education. Kris
                                       is happy to be in Florida where the air is normally warm and the skies are blue. Her
                                       hobbies include fishing, biking, eating healthy, spending time with family / friends
                                       and most recently going to the beach! Kris is happy to be part of this innovative,
                                       progressive College where work is student centered and those who work here know they
                                       make a difference! 
                                    </p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Madeline Velasquez </strong></p>
                                    
                                    <p>Madeline Velasquez joined the Valencia College Resource Development Office in May
                                       2016. &nbsp;Prior to joining the college, Madeline worked in the Finance, Health, Defense
                                       &amp; Entertainment industries.&nbsp; She has over 20 years of demonstrated experience as an
                                       administrative assistant supporting executives and their teams in multiple industries.
                                       &nbsp;She has experience providing office management and coordinating special projects.&nbsp;
                                       
                                    </p>
                                    
                                    
                                    <p>After moving to Florida, Madeline joined Lockheed Martin in 2003 where she worked
                                       for 10 years in various areas including Corporate Program Performance and Supply Chain
                                       Management.&nbsp; In 2014, she went to work for the Walt Disney Company and enjoyed her
                                       work in the Enterprise Service Management division providing support and assistance
                                       in the implementation of successful IT projects.&nbsp; 
                                    </p>
                                    
                                    <p>As the administrative assistant for the Resource Development team, she primarily supports
                                       the Assistant Vice President of Resource Development in various capacities and helps
                                       ensure the day to day operations of the Resource Development Office<strong>. &nbsp;</strong>Madeline also works with the Resource Development team to provide support and assistance
                                       with coordinating submission of grant applications and managing funded grant projects.<strong>&nbsp; </strong></p>
                                    
                                    
                                    <p>A New York native Madeline moved from Brooklyn, New York, to Kissimmee, Florida in
                                       2002. In her spare time, Madeline enjoys spending time with her husband and daughters.
                                       Her hobbies include cooking, dancing, the arts and spending times with family/friends.&nbsp;
                                       Madeline is happy to be part of the Valencia family that is making a difference in
                                       both student lives and the community.&nbsp; She knows this first hand because both her
                                       daughters previously attended Valencia. 
                                    </p>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <strong>Allie Yadav</strong><br>
                                    
                                    <p>Allie Yadav joined the Resource Development Office and Valencia in March 2011. Prior
                                       to joining the college, Allie worked in nonprofit and local government. Allie manages
                                       all phases of grant project development, proposal writing, budget creation, and submission.
                                       Allie has experience submitting proposals to competitions held by the Department of
                                       Education, National Science Foundation, National Endowment for the Humanities, United
                                       States Department of Agriculture, and other federal, state, and private organizations.
                                       She works with faculty, staff, and administrators to craft grant ideas into viable,
                                       competitive proposals for funding. She researches upcoming grant opportunities in
                                       order to match funding to college initiatives, and she remains current on federal
                                       rules and college policy to ensure grant proposals are compliant with all regulations.
                                       Allie represents the college statewide at the Florida Council for Resource Development,
                                       as well as nationwide at the Council for Resource Development and as a member of the
                                       Federal Funding Taskforce. She has also completed Grantsmanship and grant management
                                       training.  
                                    </p>
                                    
                                    <p>Allie attended the University of Florida receiving a Bachelor of Arts in English,
                                       and Florida State University receiving a Master of Public Administration degree. She
                                       and her husband, Ryan, are the proud parents of Norman, their Great Dane. Allie is
                                       honored to be a member of the Valencia family, and is excited for the opportunity
                                       to assist her colleagues in developing their project dreams into grant-funded reality.
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <strong>Lashon Henderson</strong><br>
                                    
                                    <p>Lashon Henderson joined the Resource Development Office as the  Grants Administration
                                       and Development Manager in July 2012. In this role,  Lashon works with grant project
                                       directors to ensure that grant projects meet  all of the funding source requirements
                                       while working within college policy.  Lashon assists with grant kick-off meetings
                                       and helps establish timelines for  project implementation through closeout, including
                                       reporting. Lashon works in  collaboration with project directors to provide education
                                       and support on how to  successfully meet grant program objectives. Prior to joining
                                       Resource  Development team, Lashon worked with the United Negro College Fund (UNCF)
                                       as  the Development Director for Central/North Florida rising raise money for  scholarships
                                       for students through corporations, community businesses and  foundations. Lashon is
                                       a member of the National Grants Management Association  and the Florida Council for
                                       Resource Development.
                                    </p>
                                    
                                    <p> Lashon received a Bachelor of  Arts degree in Psychology from Florida State University
                                       and a Master of Arts  degree in Counseling from Webster University. Lashon has a son,
                                       William, age 7.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Channing Frazier</strong></p>
                                    
                                    
                                    <p>Channing joined the Resource Development Office and Valencia in April 2015. In his
                                       role, Channing collaborates with the Assistant Vice President of Resource Development
                                       as well as project directors in  grant managment,  process development, monitoring,
                                       and streamlining efforts that assist in the implementation of successful projects
                                       on  behalf of Valencia College.
                                    </p>
                                    
                                    <p>Channing obtained his Masters in Business Admnistration (MBA) from the Coggin College
                                       of Business as well as Bachelors of Science  in Business Marketing and Business Management.
                                       Additionally, Channing holds cerfications in project managment which he has had the
                                       opportunity to use while  working in various professional capacities  in public and
                                       private sectors. 
                                    </p>
                                    
                                    <p>When Channing is not working he enjoys spending time with his beautiful wife Hiliana,
                                       learning new skills and watching basketball. 
                                    </p>
                                    
                                 </div>                
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Nancy Mallison</strong></p>
                                    
                                    <p>Nancy Mallison joined the Valencia College Resource Development Team in February 2016
                                       as a Manager, Grants Development and Special Projects. Nancy provides pre-award services
                                       for all campuses, assisting in proposal development submitted to government and private
                                       agencies, along with building foundation partnerships at the community level. Nancy
                                       also prospects for opportunities within both public and private sectors, looking to
                                       provide appropriate funding sources to match the many career-based and student-centered
                                       aspirations of Valencia faculty and staff.
                                    </p>
                                    
                                    <p>Nancy brings pre- and post-award knowledge from her position in the Office of Research
                                       and Commercialization at the University of Central Florida. Her foundation building
                                       experience began at Quest, Inc., where she became skilled in proposal development
                                       and grant management working with a variety of agencies to serve individuals with
                                       developmental disabilities. She has additional proposal development expertise and
                                       post-award experience from Florida Virtual School. Nancy sees her position as the
                                       perfect combination of prior experience, finding ways to provide opportunities for
                                       the local communities served by Valencia College. 
                                    </p>
                                    
                                    <p>Originally from Michigan, Nancy graduated from Western Michigan University in Kalamazoo
                                       with a degree in English. Nancy has Developer strength, and experiences success when
                                       helping others succeed. In her spare time, Nancy enjoys traveling, baking, and working
                                       on her family tree.
                                    </p>                
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Kimberly Taylor </strong></p>
                                    
                                    <p>Kimberly is a Valencia College Alumni and joined the Resource Development Office in
                                       Spring 2016. Kimberly oversees the proposal development process and special projects
                                       for pre-award grant activities. Kimberly has successfully garnered wages from competitive
                                       federal funding sources including U.S. Department of Labor, Department of Education,
                                       Department of Justice, and other public and private funding sources. As a Florida
                                       native, Kimberly attended the University of Florida, where she earned aBachelor of
                                       Arts in History and a Master of Education in Administration and Policy. She is currently
                                       pursuing her Ph.D. in Community College Leadership at Old Dominion University in Norfolk,
                                       Virginia. As a member of professional grant affiliations including the Florida Council
                                       for Resource Development and Council for Advancement and Support of Education (CASE),
                                       Kimberly serves as a team captain for the CASE Federal Funding Task Force and advocates
                                       on behalf of two- community colleges, explaining the valuable role we play in our
                                       community. Along with remaining current on federal policy and available funding sources,
                                       Kimberly enjoys working closely with Valencia grant teams that have made a positive
                                       impact on the Valencia students and college community. 
                                    </p>
                                    
                                    <p>When Kimberly is not working, she enjoys the company of her friends and family.</p>                
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Contact Information</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administration</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:kchristian6@valenciacollege.edu">Kristeen Christian</a></div>
                                 
                                 <div data-old-tag="td">407-582-2909</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:cfrazier13@valenciacollege.edu">Channing Frazier</a></div>
                                 
                                 <div data-old-tag="td">407-582-2906</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Administrative Assistant</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:mvelasquez17@valenciacollege.edu">Madeline Velasquez</a></div>
                                 
                                 <div data-old-tag="td">407-582-3342</div> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Proposal Development</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ayadav@valenciacollege.edu">Allie Yadav</a></div>
                                 
                                 <div data-old-tag="td">407-582-2902</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:ktaylor3@valenciacollege.edu">Kimberly Taylor</a></div>
                                 
                                 <div data-old-tag="td">407-582-3320</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:nmallison@valenciacollege.edu">Nancy Mallison</a></div>
                                 
                                 <div data-old-tag="td">407-582-3330</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Grant Management</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="mailto:lhenderson22@valenciacollege.edu">Lashon Henderson</a></div>
                                 
                                 <div data-old-tag="td">407-582-2911</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Location</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">District Office<br>
                                    PO Box 3028<br>
                                    Orlando, FL 32802-3028
                                 </div>
                                 
                                 <div data-old-tag="td">DO-34</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/academics/academic-affairs/resource-development/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>