
<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Admissions | Valencia College</title>
      <!-- Favicons-->
      <link rel="shortcut icon" href="/_resources/img/favicon.ico" type="image/x-icon">
      <link rel="apple-touch-icon" type="image/x-icon" href="/_resources/img/apple-touch-icon-57x57-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/_resources/img/apple-touch-icon-72x72-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/_resources/img/apple-touch-icon-114x114-precomposed.png">
      <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/_resources/img/apple-touch-icon-144x144-precomposed.png">
      <!-- BASE CSS -->
      <link href="/_resources/css/base.css" rel="stylesheet">
      <!-- SPECIFIC CSS -->
<!--       <link href="/_resources/css/stories.css" rel="stylesheet"> -->
      <link href="/_resources/css/tabs.css" rel="stylesheet">
      <link rel="stylesheet" href="/_resources/css/oustyles.css" />
<!-- fontawesome loaded here as a deferred script to avoid delaying rendering -->

<!-- commented out for update ticket #44788 -->
<!--  <script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script> -->

<script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
          var page_url="https://preview.valenciacollege.edu/_staging/admissions-static-index.php";
        </script></head>
   <body>    
    <!--[if lte IE 10]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
  <![endif]-->      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><!-- ChromeNav Header ================================================== -->
<div class="header header-chrome desktop-only">
  <div class="container">
    <nav>
      <div class="row">
        <div class="col-xs-3" role="banner"> 
          <div id="logo">
            <a href="/"> <img src="/_resources/img/logo-vc.png" width="265" height="40" alt="Valencia College Logo" data-retina="true" /></a>
          </div>
        </div>
        <div class="chromeNav col-xs-6" role="navigation" aria-label="Main Navigation">
          <ul>
            <li class="submenu"><a href="javascript:void(0);" class="show-submenu">Students <span class="caret" aria-hidden="true"></span></a>
              <ul>
                <li><a href="/students/continuing-education/">Continuing Education</a> </li>
                <li><a href="/students/">Current Students</a></li>
                <li><a href="/students/future-students/">Future Students</a></li>
                <li><a href="/students/international/">International Students</a></li>
                <li><a href="/students/military-veterans.php">Military &amp; Veterans</a></li>
              </ul>
            </li>
            <li><a href="/faculty/">Faculty</a> </li>
            <li><a href="/employees/">Employees</a> </li>
            <li><a href="/FOUNDATION/alumni/">Alumni &amp; Foundation</a> </li>
          </ul>
        </div>
        <div class="chromeNav col-xs-3 pull-right" role="navigation" aria-label="Login Navigation and Search">
          <ul>
            <li><a href="#" data-toggle="modal" data-target="#login">Login <i class="fas fa-sort-down" aria-hidden="true"></i></a>
              <ul>
                <li class="fa-2x"><a href="https://atlas.valenciacollege.edu/"><i class="fal fa-sign-in-alt fa-fw icon-padding" aria-hidden="true">&nbsp;</i> Atlas</a></li>
                <li><a href="https://valenciaalumniconnect.com/"><i class="far fa-users fa-fw icon-padding" aria-hidden="true">&nbsp;</i> Alumni Connect</a></li>
                <li><a href="https://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&realm=mail.valenciacollege.edu&wa=wsignin1.0&wtrealm=urn:federation:MicrosoftOnline&wctx=bk%3D1367916313%26LoginOptions%3D3"><i class="valencia-font icon-o365 icon-padding" aria-hidden="true">&nbsp;</i> Office 365</a></li>
                <!--Link to current LMS; Blackboard or Canvas-->
                <li><a href="http://learn.valenciacollege.edu"><i class="far fa-graduation-cap fa-fw icon-padding" aria-hidden="true">&nbsp;</i>Valencia Online</a></li>
                <li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"><i class="far fa-envelope fa-fw icon-padding" aria-hidden="true">&nbsp;</i> Webmail</a></li>
              </ul>
            </li>
            <li role="search"><a href="https://search.valenciacollege.edu/search?site=preview&client=preview&proxystylesheet=preview&output=xml_no_dtd&proxyreload=1" id="search_bt"><i class="far fa-search" aria-hidden="true"></i><span>Search</span></a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</div></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><!-- Site Menu================================================== -->

          <ul>
            <li class="submenu"><a class="show-submenu"  href="/academics/"> Academics <span class="caret" aria-hidden="true"></span></a>
              <ul class="dropdown-menu-left">
                <li><a href="/academics/academic-affairs/">Academic Affairs</a></li>
                <li><a href="https://preview.valenciacollege.edu/continuing-education/accelerated-skills-training-programs/">Accelerated Skills Training</a></li>
                <li>
                  <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/associates/">Associate Degrees</a></div>
                </li>
                <li>
                  <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/bachelors/">Bachelor’s Degrees</a></div>
                </li>
                <li>
                  <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/certificates/">Certificate Programs</a></div>
                </li>
                <li>
                  <div><a href="https://preview.valenciacollege.edu/continuing-education/">Continuing Education</a></div>
                </li>
                <li>
                  <div><a href="#">Continuing Education Health Sciences</a></div>
                </li>
                <li>
                  <div><a href="/admissions/dual-enrollment/">Dual Enrollment</a></div>
                </li>
                <li>
                  <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/">Degree and Certificate Options</a></div>
                </li>
                <li>
                  <div><a href="https://projects.invisionapp.com/share/WAE88NJ8C%23/screens/268822537_17OIT003-CMS-Academics-2">View all Areas of Study</a>&nbsp;</div>
                </li>
              </ul>
            </li>
            <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);"> Admissions <span class="caret" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-6">
              <ul class="dropdown-menu-left">
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/continuing-education/"><span>Continuing Education</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/admissions/"><span>Domestic Students</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/international-students/"><span>International Students</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/continuing-education/programs/languages/"><span>Language Programs</span></a></div>
                    </li>
                    <li>
                      <div><a href="/students/offices-services/veterans-affairs/"><span>Military and Veterans</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/admissions/parents-of-future-students/"><span>Parents and Families</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/admissions/return-to-college/"><span>Return to College</span></a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/undocumented-students/"><span>Undocumented Students</span></a></div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- End menu-wrapper --></li>
            <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);"> Areas of Study <span class="caret" aria-hidden="true"></span></a>
              <div class="menu-wrapper c3">
                <div class="col-md-6">
                  <ul>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/general-studies/">General Studies</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/arts-and-entertainment/">Arts and Entertainment</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/business/">Business</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/communications/">Communications</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/education/">Education</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/engineering-and-technology/">Engineering and Technology</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/health-sciences/">Health Sciences and Nursing</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/hospitality-and-culinary/">Hospitality and Culinary</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/information-technology/">Information Technology</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/plant-science-and-agricultural-technology/">Plant Science and Agricultural Technology</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/public-safety-and-paralegal-studies/">Public Safety and Paralegal Studies</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/science-and-mathematics/">Science and Mathematics</a></li>
                    <li><a href="https://preview.valenciacollege.edu/students/future-students/programs/social-sciences/">Social Sciences<br /></a><hr /><a href="https://preview.valenciacollege.edu/students/future-students/programs/social-sciences/"><br /></a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <ul>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/advanced-technical-certificates/">Advanced Technical Certificates</a></div>
                    </li>
                    <li>
                      <div><a href="/students/offices-services/bridges-to-success/">Bridges to Success</a></div>
                    </li>
                    <li>
                      <div><a href="/students/offices-services/career-pathways/">Career Pathways</a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/degree-options/certificates/">Certificate Programs</a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/directconnect-to-ucf/">DirectConnect to UCF</a></div>
                    </li>
                    <li>
                      <div><a href="/faculty/education-preparation-institute/">Educator Preparation Institute</a></div>
                    </li>
                    <li>
                      <div><a href="https://preview.valenciacollege.edu/students/future-students/admissions/honors-program/">Honors Program</a></div>
                    </li>
                    <li>
                      <div><a href="/students/offices-services/career-pathways/">Tech Express to Valencia College</a></div>
                    </li>
                    <li>
                      <div><a href="http://preview.valenciacollege.edu/continuing-education/programs/english-as-a-second-language/">Intensive English Programs</a></div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- End menu-wrapper --></li>
            <li class="submenu"><a href="/about/"> About <span class="caret" aria-hidden="true"></span></a>
              <ul class="dropdown-menu-left">
                <li><a href="/about/about-valencia/">About Valencia</a></li>
                <li><a href="/about/about-us/trustees.php">Board of Trustees</a></li>
                <li><a href="#">Community Affairs</a></li>
                <li><a href="/employees/human-resources/">Employment</a></li>
                <li><a href="/about/">Facts</a></li>
                <li><a href="/about/about-us/history.php">History</a></li>
                <li><a href="/locations/">Locations</a></li>
                <li><a href="https://news.valenciacollege.edu/">News and Media</a></li>
                <li><a href="/about/">Office of the President</a></li>
                <li><a href="/students/offices-services/security/">Safety and Security</a></li>
                <li><a href="/about/sustainability/">Sustainability</a></li>
              </ul>
            </li>
            <li class="submenu"><a href="#">Campus Resources <span class="caret" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
              <ul class="dropdown-menu-right">
                  <li><a href="/academics/calendar">Academic Calendar</a></li>
                  <li><a href="/locations/">Campuses</a></li>
                  <li><a href="http://preview.valenciacollege.edu/students/future-students/career-coach/">Career Coach / Career Center</a></li>
                  <li><a href="/academics/courses/">Courses and Schedules</a></li>
                  <li><a href="/students/offices-services/locations-store/">Campus Stores (Bookstores)</a></li>
                  <li><a href="https://events.valenciacollege.edu">Events Calendar</a></li>
                  <li><a href="/students/offices-services/library/">Libraries</a></li>
                  <li><a href="#">Schools and Colleges</a></li>
                </ul>
              </div>
            </li>
            <!-- <li class="site-menu-outline"><a href="http://preview.valenciacollege.edu/students/future-students/visit-valencia/">Request Info</a></li> -->
            <li class="site-menu-outline apply"><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=">Apply</a></li>
          </ul>

<!-- End Site Menu Sub-nav  --></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg bg-academics">
         <div id="intro-txt">
            <h1>Admissions</h1>
            <nav>
               <div class="page-menu" id="page_menu">
                  <ul>
                     
                     <li><a href="/students/continuing-education"></a>Continuing Education
                     </li>
                     
                     <li><a href="#"></a>First-time in College
                     </li>
                     
                     <li><a href="/students/international"></a>International Students
                     </li>
                     
                  </ul>
               </div>
            </nav>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/_staging/">Testing</a></li>
               <li>Admissions</li>
            </ul>
         </div>
      </div>
      <main role="main">
         <div class="container margin-60">
            <div class="row">
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="wrapper_indent">
                        <h2>First-time in College Admission</h2>
                        
                        <p>Thank you for visiting the Admissions website of Valencia College! We are glad you
                           are here and we hope that you find the information on this website to be helpful as
                           you transition to one of the premiere learning-centered institutions in the United
                           States.
                        </p>
                        
                        <p class="p1"><span class="s1">You will be admitted to Valencia College as a degree-seeking student if you have a
                              standard high school diploma or a state-issued General Educational Development (GED)
                              diploma. To be admitted as a degree-seeking student you must have an admissions application
                              and official academic transcript(s) on file in the Admissions and Records Office.
                              If you do not have a high school diploma or GED, you may be admitted as a provisional
                              non-degree seeking student. </span></p>
                        
                        <p><img class="img-responsive img-fluid fullwidth" src="/_resources/img/prestige/art-admissions-00.jpg" alt="Applying for admission to Valencia is fun and easy!" width="1620" height="1080"></p>
                        
                        <h3>Admissions Deadlines</h3>
                        
                        <p>Remember, it takes time to process your admissions application. The earlier you apply,
                           the better! Please visit the online Important Dates &amp; Deadlines calendar for admissions
                           Application Priority Deadlines.
                        </p>
                        
                        <h3>Application Fees</h3>
                        
                        <p>There is a non-refundable application fee of $35.00 for Associate's degree level applicants
                           and for Bachelor's degree level applicants (you must have already completed an Associate's
                           degree in order to apply for admission to a Bachelor's degree program at Valencia).
                           Dual Enrollment students cannot apply online and should visit the Dual Enrollment
                           website for application instructions. Previous Valencia students who have not taken
                           classes here in the past two (2) years will need to reapply for admission and pay
                           the non-refundable $35.00 readmission fee.
                        </p>
                     </div>
                  </div>
               </div>
               <aside class="col-md-3">
                  <p>
                          
                     <!--BEGIN ACTION PALETTE-->
                     
                     <div class="box_style_announcement_sidebar">
                        
                        <div class="action-palette">
                           
                           <div>
                              
                              <div><a class="button-action home" href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=">Apply Now </a></div>
                              
                              <div><a class="button-action_outline home" href="http://net4.valenciacollege.edu/promos/internal/request-info.cfm">Request Information </a></div>
                              
                              <div><a class="button-action_outline home" href="#">Chat With Us </a></div>
                              <div><a class="button-action_outline home" href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia </a></div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <!--END ACTION PALETTE-->
                        <br><br><p>
                  
                  <div>
                     <h4 class="v-red cap"><strong>headline</strong></h4>
                     <p>Some intro paragraph to lead other audiences to seek out specific information about
                        their admission process.
                     </p>
                     <div>
                        <h5 class="val">Locations</h5><i class="far fa-map-marker fa-spacer"></i>Military and Veterans<br><i class="far fa-map-marker fa-spacer"></i>Parents and Families<br><i class="far fa-map-marker fa-spacer"></i>Return to College<br><i class="far fa-map-marker fa-spacer"></i>Undocumented Students<br><br></div>
                  </div>
                  
                  <div>
                     <h4 class="v-red cap"><strong>Contact us</strong></h4>
                     <p>Visit us on campus at the Answer Center.</p>
                     <div>
                        <h5 class="val">Locations</h5><i class="far fa-map-marker fa-spacer"></i>East Building 5, Room 211<br><i class="far fa-map-marker fa-spacer"></i>Lake Nona Building 1, Room 149<br><i class="far fa-map-marker fa-spacer"></i>Osceola Building 2, Room 150<br><i class="far fa-map-marker fa-spacer"></i>West SSB, Room 106<br><i class="far fa-map-marker fa-spacer"></i>Winter Park Building 1, Room 210<br><h5 class="val">Hours</h5><i class="far fa-calendar fa-spacer"></i>Mon. – Thurs. 8 a.m. – 6 p.m.<br><i class="far fa-calendar fa-spacer"></i>Friday 9 a.m. – 5 p.m.<br><br><i class="far fa-envelope fa-spacer"></i>enrollment@valenciacollege.edu<br><i class="far fa-phone fa-spacer"></i>407-582-1507<br></div>
                  </div>
               </aside>
            </div>
         </div>
            
         <div class="container-fluid container_gray_bg">
            <div class="container margin-30">
               <div class="row wrapper_indent">
                  <div class="col-md-2 v-red"></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">1</span><span class="step_to_do_text">Apply for admission and financial aid</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">2</span><span class="step_to_do_text">Create your ATLAS account</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">3</span><span class="step_to_do_text">register for classes &amp; pay tuition and fees</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">4</span><span class="step_to_do_text">register for classes &amp; pay tuition and fees</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">5</span><span class="step_to_do_text">get your students identification and parking decal</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
                  <div class="col-md-2"><span class="step_to_do_number">6</span><span class="step_to_do_text">buy your books and go to class</span><a href="#" target="" class="link-underline text-center mobile_only">learn more</a></div>
               </div>
               <div class="row desktop_only">
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">learn more</a></div>
               </div>
            </div>
         </div>
            
            
         <div class="container margin-60">
            <div class="row">
               <div class="col-md-6">
                  <div class="wrapper_indent">
                                
                     <h4 class="v-red cap">Degree Verification</h4>
                                
                     <p>DegreeVerify provides instant online verifications of college degrees or attendance
                        claimed by job applicants. The service is designed to simplify verification for employers,
                        background screening firms, executive search firms and employment agencies, who regularly
                        screen candidates.
                     </p>
                                <strong>Degree Verification Instructions</strong>
                                
                     <p>For Degree Verification go to the National Student Clearinghouse website and click
                        on the VERIFY button to get started.
                     </p>
                                
                     <p>Valencia College submits degree information to the Clearinghouse once per term. There
                        is a fee for Degree Verification; please see the National Student Clearinghouse website
                        for more information.
                     </p>
                              
                  </div>
               </div>
               <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;"><img class="img-responsive fullwidth" src="/_resources/images/landing/IMG_9913.jpg" alt="Students in Computer Lab"></div>
            </div>
         </div>
            
            
         <div class="container-fluid container_gray_bg">
            <div class="container margin-30">
               <div class="indent_title_in">
                  <h4 class="v-red cap">Florida residency</h4>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="wrapper_indent"><strong>Reminder</strong>
                                    
                        <p>Students who are eligible for Florida residency for tuition purposes pay a much lower
                           tuition rate. See the current rates.
                        </p>
                                    
                        <p>For students who fail to submit documentation and still register for classes, tuition
                           will be charged at the out-of-state rate.
                        </p>
                                    
                        <p>If your request for Florida Residency and the necessary documentation is not received
                           prior to the Proof of Florida Residency Deadline as listed in the Important Dates
                           and Deadlines calendar, your residency will be processed for the next available term.
                        </p>
                                    <strong>Degree Verification Instructions</strong>
                                    
                        <p>For Degree Verification go to the National Student Clearinghouse website and click
                           on the VERIFY button to get started.
                        </p>
                                    
                        <p>Valencia College submits degree information to the Clearinghouse once per term. There
                           is a fee for Degree Verification; please see the National Student Clearinghouse website
                           for more information.
                        </p>
                                  
                     </div>
                  </div>
                  <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">
                                
                     <p><strong>Dependent vs. Independent Students</strong></p>
                                
                     <p>The determination of dependent or independent student status is important because
                        it is the basis for whether the student has to submit his/her own documentation for
                        residency (as an independent) or his/her parent's or guardian's documentation of residency
                        (as a dependent). Parent means one or both of the parents of a student, any guardian
                        of a student or any person in a parental relationship to a student.
                     </p>
                                
                     <div class="row"><span class="date_stacked col-md-2">09 <span>MAR</span></span><span>Proof of Florida Residency Deadline</span><span><a class="button-outline_small" href="/" target="">submit residency</a></span></div>
                              
                  </div>
               </div>
            </div>
         </div>
            
                      
      </main><!-- Begin footer --><footer role="contentinfo">
<div class="container">
  <div class="row ">
    <div class="col-xs-8"><a href="#">CONSUMER INFO</a><span class="footer-link">|</span><a href="#">POLICIES</a><span class="footer-link">|</span><a href="#">PUBLIC RECORDS</a><span class="footer-link">|</span><a href="#">PRIVACY</a><span class="footer-link">|</span><a href="#">SSN USAGE</a><span class="footer-link">|</span><a href="#">SITE INDEX</a><br /></div>
    <div class="col-xs-4">
      <a href="http://www.facebook.com/ValenciaCollege" aria-label="Find us on Facebook"><i class="fab fa-facebook fa-3x fa-w-20 social-padding white"></i></a> <a href="http://www.twitter.com/ValenciaCollege" aria-label="Follow us on Twitter"><i class="fab fa-twitter fa-3x fa-w-20 social-padding"></i></a>
      <a
         href="https://www.instagram.com/valenciacollege" aria-label="Follow us on Instagram"><i class="fab fa-instagram fa-3x fa-w-20 social-padding"></i></a> <a href="http://www.linkedin.com/company/valencia-college" aria-label="Connect with us on LinkedIn"><i class="fab fa-linkedin fa-3x"></i></a> 
    </div>
    <!-- End row -->
    <div id="copy" class="row">
      <div class="col-md-7 col-sm-7">Valencia College provides equal opportunity for educational opportunities and employment to all. Contact the Office of Organizational Development and Human Resources for information.</div>
      <div class="col-md-1 col-sm-1 vertical_gray_line">&nbsp; <br /> &nbsp; <br /> &nbsp;</div>
      <div class="col-md-3 col-sm-4">P.O. Box 3028, Orlando, Florida 32802 <br /> 407-299-5000 <br /> <span id="directedit">©</span> 2017 - All rights reserved.</div>
    </div>
  </div></div>
<!-- End container --></footer><!-- End footer --><!-- Common scripts -->
    <script src="/_resources/js/jquery-1.11.2.min.js "></script>
    <script src="/_resources/js/common_scripts_min.js "></script>
    <script src="/_resources/js/functions.js "></script>
    <script src="/_resources/js/search.js"></script>
    <script src="/_resources/js/validate.js "></script>
<!--     <script src="/_resources/js/stories.js "></script> -->
    <script src="/_resources/js/tabs.js "></script>
    <script src="/_resources/js/direct-edit.js"></script>
  <script src="/_resources/js/ou.js"></script>
    <script>
    if (document.getElementById('tabs')) {
        new CBPFWTabs(document.getElementById('tabs'));
      }
    </script><!-- Insert google analytics here -->
<!--- 12/11/2018 - 12:50 PM EST -  This is a Test GA Code to test the functions of GA in OU - JJORDAN -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110983368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110983368-1');
</script>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/_staging/admissions-static-index.pcf">©</a>
      </div>
   <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/static-index.php">©</a></body>
</html>