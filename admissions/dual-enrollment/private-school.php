<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Private School Students | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Students attending an eligible private school within Orange or Osceola county are able to participate in Valencia's Dual Enrollment program. We encourage students to consult with their parents/legal guardians and guidance counselor to determine eligibility for the challenge of college level coursework.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/private-school.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Private School Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        	
                        <h2>Private School Students </h2>
                        	
                        <p>
                           		<strong>Summer 2018 Dates:</strong>
                           		<br>Application Opens: Monday, 2/26
                           		<br>Application &amp; Test Score Deadline: Friday, 4/6
                           	
                        </p>
                        
                        	
                        <p>
                           		<strong>Fall 2018 Dates:</strong>
                           		<br>Application Opens: Monday, 2/26
                           		<br>Application Deadline: Friday, 4/6
                           		<br>Test Score Deadline: Friday, 4/20
                           	
                        </p>
                        	
                        <p>Students attending an eligible private school within Orange or Osceola county are
                           able to participate in Valencia's Dual Enrollment program. We encourage students to
                           consult with their parents/legal guardians and guidance counselor to determine eligibility
                           for the challenge of college level coursework.
                        </p>
                        	
                        <p><a href="#TestScore">Test Score Requirements</a></p>
                        	
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Associate in Arts (A.A) Degree Requirements</a> 
                        </p>
                        	
                        <p><a href="FAQ-current-students.php" class="button_action_outline">Have a Question?<br> Visit Our Questions and Answers Page </a></p>
                        
                        	
                        <h3><strong>At A Glance (Criteria):</strong></h3>
                        	
                        <ul>
                           		
                           <li> Be a resident of Orange or Osceola County and enrolled in grades 6 – 12 at a public
                              school. 
                           </li>
                           		
                           <li>3.0 unweighted high school GPA for grades 6-11th; or 3.0 weighted GPA for 12th grade
                              at the time of application. Middle school students must  have a high school level
                              GPA.
                           </li>
                           		
                           <li>Demonstrate college readiness in reading, writing and math via the PERT, ACT or SAT
                              by the assessment scores deadline. We accept any combination  of scores.
                           </li>
                           	
                        </ul>
                        	
                        <p><a href="eligible-private-schools.php">Eligible Private Schools </a></p>
                        
                        	
                        <h4>Private School Student Checklist:</h4>
                        	
                        <ul>
                           		
                           <li>Resident of Orange or Osceola county</li>
                           		
                           <li>Currently attending a private school in Orange or Osceola county, grades 6-12 (<strong><em>Note: private school must have a current articulation agreement with Valencia College
                                    in order for students to participate</em></strong>)
                           </li>
                           		
                           <li>Complete a Dual Enrollment Application (Free)</li>
                           		
                           <li>Obtain parent permission (signature required on DE admissions application)</li>
                           		
                           <li>Meet with your guidance counselor to discuss Dual Enrollment requirements and to verify
                              your GPA is a  3.0 unweighted GPA for  grades 6-11th; or 3.0 weighted GPA for 12th
                              grade (guidance counselor signature required on DE admissions application). Middle
                              school students must  have a high school level GPA.
                           </li>
                           		
                           <li>Submit your application to the Dual  Enrollment representative at your school to be
                              submitted to the DE office  via email by the application deadline of the desired term
                              and receive a VID (Valencia ID number) assigned for the purpose of taking the PERT
                              exam 
                           </li>
                           		
                           <li>Take the PERT <strong>or</strong> submit your college-level test scores to an Assessment Center on any Valencia campus
                              before the test score deadline in order to be processed for the desired term of enrollment;
                              PERT can be taken three times at Valencia within a two-year period, with  a one-day
                              wait period between attempts. Your college-ready test scores for ACT/SAT must  be
                              original reports or sent electronically from College Board,  www.collegeboard.org
                              or act.org to Valencia. <strong>Please allow 5 weeks for these scores to be received and processed.</strong>
                              		
                           </li>
                           		
                           <li>Your college-ready test scores for ACT/SAT must be original reports or sent electronically
                              from College Board, www.collegeboard.org or act.org to an Assessment Center on any
                              Valencia campus before the test score deadline in order to be processed for the desired
                              term of enrollment; Please allow 5 weeks for the scores to be received and processed.
                           </li>
                           		
                           <li> If the student does not have eligible college ready  test scores on file with Valencia
                              College at the time the application is  submitted, the student will receive a weekly
                              email reminder to submit eligible  test scores. It is the student’s responsibility
                              to submit eligible  test scores. Once VC has received  test scores, the student will
                              be fully processed in 7 business days.
                           </li>
                           	
                        </ul>
                        
                        	
                        <h4>Accepted Student Checklist:</h4>
                        	
                        <ul>
                           		
                           <li>Set up your Atlas account once you receive the official, acceptance letter </li>
                           		
                           <li>Complete the mandatory Dual Enrollment New Student Online  Orientation via your Atlas
                              account; allow a minimum of 2 business days for  results to be posted and the hold
                              to be released from your account. You will not be able to register for courses  until
                              the online orientation is complete.
                           </li>
                           		
                           <li>Register for courses (via your Atlas account); <strong>you are not eligible</strong> for developmental courses, PE skills courses, flex start (<em>all courses must be full term/16 weeks</em>), or courses less than 3 credits
                           </li>
                           		
                           <li>Tuition and lab fees are waived for all DE approved courses. </li>
                           		
                           <li>Student is responsible for textbook affiliated costs related to licensing fees or
                              electronic media access
                           </li>
                           	
                        </ul>
                        
                        	
                        <h4><strong>Important Reminders:</strong></h4>
                        	
                        <ul>
                           		
                           <li>Must maintain a 3.0 high school GPA and a 2.0 Valencia College GPA in order to continue
                              participation in the DE program; Academic Probation, two "W"s/withdrawals, or Academic
                              Warning with less than a 2.0 will result in dismissal from further program participation
                              
                           </li>
                           		
                           <li>Permitted to register for up to 4 courses/13 credit hours for Fall and Spring terms;
                              2 courses/7 credit hours for Summer term
                           </li>
                           		
                           <li>Courses must be creditable towards successful completion of high school graduation
                              requirements
                           </li>
                           		
                           <li>As a Dual Enrollment student, a balance will remain on your account until the end
                              of the term. You are not responsible for the tuition balance and it will not impact
                              your registration status. 
                           </li>
                           	
                        </ul>
                        
                        	
                        <h3>Test Score Requirements</h3><a name="TestScore" id="TestScore"></a>
                        	
                        <p>All students are required to submit and have college-ready test scores on file at
                           Valencia College for reading, writing <em>and</em> math, by the deadline for the desired term of enrollment. <strong><em>Any combination of eligible scores (see below) will be accepted.</em></strong></p>
                        	
                        <table class="table table">
                           		
                           <caption>Eligible Test Scores</caption>
                           		
                           <tr>
                              			
                              <th scope="row">PERT</th>
                              			
                              <td>Reading:<br> 106
                              </td>
                              			
                              <td>Writing:<br> 103
                              </td>
                              			
                              <td>Math: 123 (College Algebra)<br> 114 (Intermediate Algebra)
                              </td>
                              		
                           </tr>
                           		
                           <tr>
                              			
                              <th scope="row">ACT</th>
                              			
                              <td>Reading:<br> 19
                              </td>
                              			
                              <td>English:<br> 17
                              </td>
                              			
                              <td>Math: 21 (College Algebra)<br> 19 (Intermediate Algebra) 
                              </td>
                              		
                           </tr>
                           		
                           <tr>
                              			
                              <th scope="row">
                                 <p>SAT<br> (Taken <em> prior</em> to March 1, 2016)
                                 </p>
                              </th>
                              			
                              <td colspan="2">Critical Reading: 440</td>
                              			
                              <td>Math: 500 (College Algebra)<br> 440 (Intermediate Algebra) 
                              </td>
                              		
                           </tr>
                           		
                           <tr>
                              			
                              <th scope="row">SAT<br> (Taken <em>on or after</em> March 1, 2016)
                              </th>
                              			
                              <td>Reading Test Score: 24</td>
                              			
                              <td>Writing Test Score: 25 </td>
                              			
                              <td>Math Test Score:<br> 26.5 (College Algebra)<br> 24 (Intermediate Algebra)
                              </td>
                              		
                           </tr>
                           	
                        </table>       
                        
                        
                        
                        
                        	  
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        
                        <div>
                           
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           
                           <div>
                              Nov<br>
                              <span>03</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           
                           <div>
                              Nov<br>
                              <span>10</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="finals_week">
                           
                           
                           <div>
                              Dec<br>
                              <span>11</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </aside>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/private-school.pcf">©</a>
      </div>
   </body>
</html>