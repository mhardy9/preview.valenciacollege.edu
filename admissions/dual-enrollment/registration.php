<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Course Registration Information | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Once a newly accepted student has completed the mandatory Dual Enrollment Online Orientation, they are eligible to register for classes. Dual Enrollment students are responsible for building their class schedule and coordinating with their middle or high school’s guidance counselor. Students are only eligible to register for Full Term classes and may take classes online or on campus.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/registration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Course Registration</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Course Registration Information</h2>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>IMPORTANT DATES:</h3>
                        
                     </div>
                     
                     <div class="col-md-9">
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><strong>SUMMER 2018 (Term B)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br></strong></td>
                                 
                                 <td><strong>FALL 2018&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p><strong> March 2</strong> - Registration Opens<br> <strong>June 19</strong> - Classes Begin<br> <strong>June 26</strong> - Drop/Refund Deadline (11:59pm)<br> <strong>May 25</strong> - Graduation Application Deadline<br> <strong>July 19</strong> - Withdraw Deadline "W" Grade (11:59pm)<br> <strong>July 31</strong> - Classes End
                                    </p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p><em>New DE students accepted for Summer 2018 are limited to a maximum of two of the following
                                          courses: SLS1122 - New Student Experience, ENC1101 - Freshman Composition I, ENC1102
                                          - Freshman Composition II, MAT1033C - Intermediate Algebra and MAC1105 - College Algebra.&nbsp;</em></p>
                                    
                                 </td>
                                 
                                 <td><strong> May 25</strong> - Registration Opens<br> <strong>August 27</strong> - Classes Begin<br> <strong>September 4</strong> - Drop/Refund Deadline (11:59pm)<br> <strong>September 14</strong> - Graduation Application Deadline<br> <strong>November 9</strong> - Withdraw Deadline "W" Grade (11:59pm)<br> <strong>December 9</strong> - Classes End
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <p>Once a newly accepted student has completed the mandatory Dual Enrollment Online Orientation,
                           they are eligible to register for classes. Dual Enrollment students are responsible
                           for building their class schedule and coordinating with their middle or high school’s
                           guidance counselor. Students are only eligible to register for Full Term classes for
                           Fall/Spring and may take online, hybrid or on campus classes.
                        </p>
                        
                        <p>Class registration is available through the student’s Atlas account under the Courses
                           tab. Each term, the college will ask you to provide emergency contact information
                           and accept a Student Enrollment Agreement. We recommend that student's use Firefox
                           or Chrome as their browser when registering for classes.
                        </p>
                        
                        <h3>Orientation</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Login to your Atlas Account</li>
                           
                           <li>Click on the Courses Tab</li>
                           
                           <li>Click My Courses/Blackboard</li>
                           
                           <li>Click Dual Enrollment Online Orientation <br> <em>Note: It will take up to 3 hours for this to appear once you have created a new Atlas
                                 account.</em></li>
                           
                           <li>Complete the LASSI, Career Review and New Student Orientation Quiz</li>
                           
                           <li>Wait 2 business days for the hold to be lifted</li>
                           
                           <li>You can now register for classes!</li>
                           
                        </ul>
                        
                        <h3>Registering</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Login to your Atlas Account</li>
                           
                           <li>Click on the Courses tab</li>
                           
                           <li>Click on Registration</li>
                           
                           <li>Click Register for Classes</li>
                           
                           <li>Select a Term</li>
                           
                           <li>Click Step 3: Register for Classes/Withdraw from Classes</li>
                           
                           <li>Enter the CRN number or click Class Search</li>
                           
                        </ul>
                        
                        <p><strong>Orange County Public School </strong>students using FSA, EOC and/or lower PERT score (97-113) are required to contact the
                           Dual Enrollment office via email at <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or phone at (407)582-1600 in order to add/remove courses to their Valencia schedule.
                        </p>
                        
                        <p>It is always helpful to do an <strong>Advanced Search</strong> so that you can choose specific parameters for the search, such as Full Term, which
                           should always be chosen for the Fall and Spring terms.
                        </p>
                        
                        <h3>Ineligible Courses</h3>
                        
                        <p>DE students are not permitted to register for the courses listed below. If registered,
                           the student will be dropped by the DE office.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Less than 3 credit hours</li>
                           
                           <li>Flex start courses during Fall (any course that does not begin 08/27/2018 and end
                              12/09/2018)
                           </li>
                           
                           <li>Developmental, remedial or physical education courses</li>
                           
                           <li>Advanced level major-specific courses <em>(Please contact the <a href="mailto:dualenrollment@valenciacollege.edu">DE office</a> if you have a question regarding the eligible course list.) </em></li>
                           
                        </ul>
                        
                        <h3>Economics</h3>
                        
                        <p>Economics taken at Valencia to satisfy high school graduation requirements:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>The <strong>Orange and Osceola County</strong> School District recently created a Financial Literacy on-line module to allow Dual
                              Enrollment students to register for and satisfy their High school Requirement. The
                              module is required along with Valencia's <strong>ECO 2013 </strong>(Macro) or <strong>ECO 2023 </strong>(Micro) courses to satisfy the Financial Literacy Graduation Requirement.
                              
                              <ul class="list_style_1">
                                 
                                 <li>Students will be required to meet with their Guidance counselor prior to enrolling
                                    in a DE Economics course.
                                 </li>
                                 
                                 <li>Students and parents will sign an agreement (at the high school) acknowledging that
                                    if they do not complete the module, they will not fulfill the graduation requirement.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><strong>Home Schoo</strong>l students, please contact the Dual Enrollment Office regarding Economics courses
                              offered through Valencia for high school graduation requirements.
                           </li>
                           
                        </ul>
                        
                        <h3>Adding/Removing Courses</h3>
                        
                        <p>Students are able to add courses to their Valencia schedule until the first day of
                           the term. Additionally, they are able to drop courses without penalties up until the
                           Drop/Refund deadline. Courses dropped <strong>after the Drop/Refund deadline</strong> are considered a <strong>Withdrawal</strong>.
                        </p>
                        
                        <h3>Registration for Courses Requiring Prerequisites</h3>
                        
                        <p><strong>Advanced Placement (AP) Exams:</strong> To use AP scores to satisfy a Valencia course prerequisite, a student must request
                           that their scores be sent to Valencia College through <a href="https://apstudent.collegeboard.org/home?navid=gh-aps">College Board</a>. Please allow 5 weeks for the scores to be processed.
                        </p>
                        
                        <p><strong>High School courses:</strong> To use high school course(s) as a prerequisite for a Valencia course, students must
                           have their guidance counselor email the DE office at dualenrollment@valenciacollege.edu
                           providing the class the student wishes to register for, student’s VID, and the completed
                           high school course and grade received or bring in a high school transcript in order
                           to be provided an override.
                        </p>
                        
                        <p>To view course prerequisites and descriptions, please visit the <a href="/academics/courses/">Courses</a> page.
                        </p>
                        
                        <h3>Account Balance</h3>
                        
                        <p>A balance will remain on the student’s account until the end of the term.<strong> Dual Enrollment students are not responsible for paying these fees.</strong> This balance will not cause the student to be dropped from their courses.
                        </p>
                        
                        <h3>Helpful Tips</h3>
                        
                        <ul>
                           
                           <li>Be sure to check your Atlas email frequently for important communication.</li>
                           
                           <li>You will need access to a computer and the internet to register for classes.</li>
                           
                           <li>Have your VID ready.</li>
                           
                           <li>For information on available courses, consult the <a href="http://net5.valenciacollege.edu/schedule/" target="_blank">Credit Class Schedule Search</a>. Pay close attention to campus locations and start and end dates or term length.
                           </li>
                           
                           <li>You may register for up to 13 credit hours/4 courses during fall and spring terms;
                              7 credit hours/2 courses during the summer.
                           </li>
                           
                           <li>DE students do not pay for courses.</li>
                           
                        </ul>
                        
                        <h3>Other Error Messages</h3>
                        
                        <p>If you are not able to register for the specific course section you have selected
                           and you know that everything mentioned above has been addressed and is in order, there
                           are several possibilities for receiving an error message, including the following:
                        </p>
                        
                        <ul>
                           
                           <li>If the section shows just one seat left, the class may actually be full; you can elect
                              to be placed on a waiting list if that option is available. When placed on a class
                              waiting list, you must check and read your Atlas emails regularly in order to select
                              the course if and when it becomes available.
                           </li>
                           
                           <li>The class may be reserved for a select group of students such as Bridges to Success
                              (special scholarship students) or Seneff Honors Program. Reserved sections can be
                              determined by clicking on the CRN number to the right of the checkbox (this will display
                              additional course information) or visiting the following link <a href="http://net5.valenciacollege.edu/schedule/">Credit Class Schedule.</a></li>
                           
                           <li>It may be a LinC class which requires that you register for another specified class
                              which is linked to the one you have selected. Check the details carefully, as these
                              classes are generally reserved for select Valencia programs.
                           </li>
                           
                           <li>It is recommended to use Firefox or Chrome browsers when accessing your Atlas Account
                              and/or Blackboard.
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" target="_blank" rel="noopener">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>Oct<br> <span>20</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a></div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>Nov<br> <span>03</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a></div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>Nov<br> <span>10</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a></div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>Dec<br> <span>11</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/registration.pcf">©</a>
      </div>
   </body>
</html>