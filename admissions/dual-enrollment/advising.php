<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advisors | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="The Dual Enrollment Advising Team helps serve students by making course recommendations, discussing graduation requirements, and answering DE questions. It is recommended that students meet with their high school guidance counselor to discuss unmet high school requirements before meeting with a DE advisor.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/advising.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Dual Enrollment</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Dual Enrollment Advisors</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Dual Enrollment Advisors</h2>
                        
                        <p>The Dual Enrollment Advising Team helps serve students by making course recommendations,
                           discussing graduation requirements, and answering DE questions. 
                        </p>
                        
                        <p>It is recommended that students meet with their high school guidance counselor to
                           discuss unmet high school requirements before meeting with a DE advisor.
                        </p>
                        
                        
                        <h3>West Campus <a name="West" id="West"></a>
                           
                        </h3>
                        <img src="images/rachel-bellamy.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Rachel Bellamy<br>
                           <strong>Position:</strong> Academic Advisor
                           
                        </p>
                        
                        <p>Rachel joined the Dual Enrollment team as an Academic Advisor on Valencia's West Campus.
                           Her advising philosophy is focused on meeting students where they are and providing
                           academic tools for success. She earned her B.S. in Social Work from Southeastern University
                           and has experience working in higher education as an Academic and Career Advisor.
                           The experience she has garnered, positions her to provide excellent developmental
                           advising practices. Rachel values serving students and utilizing every opportunity
                           to assist Dual Enrollment students to not only succeed, but excel. 
                        </p>
                        
                        <p><strong>A tip for DE students:</strong> Take a deep breath, you can do this! 
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Course selection</li>
                           
                           <li>One on one advising sessions </li>
                           
                           <li>Connecting students with academic resources </li>
                           
                           <li>Dual Enrollment questions or concerns</li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/EYmnHOj8hm">Schedule An Appointment</a></p>
                        
                        
                        <img src="images/lisandra-jimenez.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Lisandra Jimenez<br>
                           <strong>Position:</strong> Dual Enrollment Coordinator
                           
                        </p>
                        
                        <p> Lisandra has been with Valencia College since 2015 and is a current Dual Enrollment
                           Coordinator. She earned a B.A. in English and Spanish at Florida Atlantic University
                           and an M.A. in Education Leadership-Higher Education Track at the University of Central
                           Florida. In her role as a coordinator she promotes the Dual Enrollment program through
                           recruitment, information sessions, and collaboration, providing advising support,
                           coordinating the application process, and maintaining up to date records related to
                           the program. As a former Dual Enrollment student, she has firsthand knowledge about
                           the Dual Enrollment experience and is happy to give back to the program that helped
                           her jump start her educational journey. 
                        </p>
                        
                        <p><strong>A tip for DE Students:</strong> Don't be intimidated by the work load of college courses, ask for help! 
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong> 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Course selection</li>
                           
                           <li>Course registration issues</li>
                           
                           <li>Becoming a degree seeking student</li>
                           
                           <li>General college concerns or questions. </li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/EYmnHOj8hm">Schedule An Appointment</a></p>
                        
                        
                        <h3>Osceola <a name="Osceola" id="Osceola"></a>
                           
                        </h3>
                        <img src="images/lindsey-oxenrider.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Lindsey Oxenrider<br>
                           <strong>Position:</strong> Academic Advisor
                           
                        </p>
                        
                        <p>Lindsey has been with Valencia College since 2015. She is excited to have joined the
                           Dual Enrollment Department as an Academic Advisor on the Osceola Campus. She has been
                           a resident of Osceola County all of her life and looks forward to empowering students
                           to succeed in the Dual Enrollment program. She is a proud Valencia College alum and
                           earned a B.A. in Political Science from the University of Central Florida. Lindsey's
                           goal is to provide students with developmental advising that promotes purpose and
                           understanding of educational experiences. 
                        </p>
                        
                        <p><strong>A tip for DE students:</strong> Create an open line of communication with your professors; they are the first point
                           of contact regarding courses and personal concerns that impact academic success. 
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>One on One Advising</li>
                           
                           <li>Educational Goal Planning </li>
                           
                           <li>Registration Process </li>
                           
                           <li>Providing Academic Resources</li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/OJL9C877Cg">Schedule An Appointment</a></p>
                        
                        
                        <img src="images/jessica-jurado-arroyo.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Jessica Jurado Arroyo<br>
                           <strong>Position:</strong> Dual Enrollment Coordinator
                           
                        </p>
                        
                        <p>Jessica is committed to higher education and serving as the Dual Enrollment Coordinator
                           for the Valencia College, Osceola Campus. She earned a B.A. in Psychology at Florida
                           International University and a M.A. in Clinical Psychology at the University of Central
                           Florida. As a Coordinator for the Dual Enrollment program, Jessica works diligently
                           to make the student's transition into college a pleasant and empowering experience
                           through informative presentations, coordinating the application and registration process,
                           student advising, and organizational support. The tasks she enjoys the most are assisting
                           and supporting students throughout their dual enrollment journey. 
                        </p>
                        
                        <p><strong>A tip for DE Students:</strong> Valencia College offers a great deal of educational resources for all students; don't
                           be afraid to ask about them! 
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong> 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Course registration process</li>
                           
                           <li>Admission into the program</li>
                           
                           <li>Support for Spanish speaking students and families</li>
                           
                           <li>Provide individual and group informational sessions </li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/OJL9C877Cg">Schedule An Appointment</a></p>
                        
                        
                        <h3>East <a name="East" id="East"></a>
                           
                        </h3>
                        <img src="images/christina-rigel.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Christina Rigel<br>
                           <strong>Position:</strong> Academic Advisor
                           
                        </p>
                        
                        <p>Christina is excited about being a member of the Dual Enrollment team and serving
                           as an Academic Advisor for Valencia College, East Campus. Her goal is to inspire students
                           to create successful and innovative lives by providing strategic guidance in academics
                           and career planning. She obtained a B.A. in Psychology from Florida State University.
                           She strives to continue her role as a student advocate by providing developmental
                           advising. Christina promotes constant growth and supports students as they explore
                           the world of dual enrollment and beyond. 
                        </p>
                        
                        <p><strong>A tip for DE students:</strong> Know your worth. Many individuals don't see their own potential, but it's there,
                           trust yourself!
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>One-on-one advising sessions</li>
                           
                           <li>Course selection </li>
                           
                           <li>Enhance the academic experience </li>
                           
                           <li>Guide students towards pursuing their goals</li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/x1jlsLEZuz">Schedule An Appointment</a></p>
                        
                        
                        <img src="images/mildred-medina.jpg" class="pull-left img-responsive" alt="">
                        
                        <p>
                           <strong>Name:</strong> Mildred Medina<br>
                           <strong>Position:</strong> Dual Enrollment Coordinator
                           
                        </p>
                        
                        <p>Mildred was a former Dual Enrollment student at Valencia College and has a special
                           place in her heart for the program. She earned her B.A. in Sociology at the University
                           of Central Florida and her M.S. in Psychology at Kaplan University. Her professional
                           experience working in education and recruitment has set her up for success in her
                           current role. Mildred returned to Valencia's East Campus as a Dual Enrollment Coordinator;
                           she promotes the Dual Enrollment program through informational sessions, social media
                           marketing, and advising support. Her goal is to give back to the program by helping
                           students achieve their educational and professional goals. 
                        </p>
                        
                        <p><strong>A tip for DE Students:</strong> Be sure to familiarize yourself with your course syllabus during the first week of
                           classes. 
                        </p>
                        
                        <p><strong>Areas I can assist with:</strong> 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Course Registration</li>
                           
                           <li>Navigating Valencia resources</li>
                           
                           <li>New Student Orientation</li>
                           
                           <li>Dual Enrollment questions or concerns </li>
                           
                        </ul>
                        
                        <p><a href="https://checkappointments.com/appts/x1jlsLEZuz">Schedule An Appointment</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="documents/dual-enrollment-application.pdf" target="_blank" class="button-action_outline">Apply Now (PDF)</a>
                        
                        <p><a href="step-by-step-application-process.html">Application Process</a></p>
                        
                        
                        <p>Applications will not be accepted in-person from students/parents at any Valencia
                           College campus. 
                        </p>
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        
                        <div>
                           
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           
                           <div>
                              Nov<br>
                              <span>03</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           
                           <div>
                              Nov<br>
                              <span>10</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="finals_week">
                           
                           
                           <div>
                              Dec<br>
                              <span>11</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </aside>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/advising.pcf">©</a>
      </div>
   </body>
</html>