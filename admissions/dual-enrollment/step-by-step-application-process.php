<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Application Process | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/step-by-step-application-process.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Application Process</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        					
                        <h2>Spring 2018 Application Process </h2>
                        					
                        <p><strong>IMPORTANT Dates:</strong></p>
                        					
                        <ul class="list_style_1">
                           						
                           <li>
                              							<strong>Oct. 20</strong> - DE Applications Due (applications will not be accepted at any VC campus location)
                              
                           </li>
                           						
                           <li>
                              							<strong>Nov. 3</strong> - Test Scores Deadline (must be received by the Assessment Office) 
                           </li>
                           						
                           <li>
                              							<strong>Nov. 10</strong> - Course Registration Opens 
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>The Dual Enrollment Application</h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li>Save Application to Computer </li>
                           						
                           <li>Complete the DE Application (PLEASE TYPE)</li>
                           						
                           <li>Sign application and obtain parent signature</li>
                           						
                           <li> Meet with your guidance counselor to discuss Dual Enrollment requirements and to
                              verify your GPA is a 3.0 unweighted grades 6-11th; or 3.0 weighted GPA for 12th grade
                              at the time of application.
                           </li>
                           						
                           <li>
                              							<strong>If you are submitting PERT scores taken at your high school campus or elsewhere</strong>, and it was not indicated on your application, you must request to have your scores
                              retrieved from the state repository by the Valencia College Assessment Center staff.
                              Please go to the ‘Submit High School PERT Scores' link at valenciacollege.edu/dualenrollment
                              to request that your scores be retrieved and inputted in the system.
                           </li>
                           						
                           <li> <strong>If you have qualifying with ACT or SAT</strong> scores, it is your responsibility to request to have the official score report sent
                              electronically to Valencia College, from sat.collegeboard.org or actstudent.org within
                              the allotted time frame. Be advised that it may take 5 weeks for scores to delivered
                              <strong>and</strong> processed by the Valencia College Assessment Center; any scores that are not entered
                              by the designated deadline will not be accepted, No Exceptions. If you have your original
                              hard copy results, bring it to an Assessment Center at any Valencia campus to have
                              the scores entered, prior to the deadline above. No copies or screenshots can be accepted.
                              You will need your assigned Valencia ID number (VID, which is assigned by the Dual
                              Enrollment office when your admission application is submitted) and a state or federally
                              issued picture ID such as a driver’s license, military ID, or passport. 
                           </li>
                           						
                           <li> <strong>If you need to take the PERT test at Valencia</strong>, go to an Assessment Center at any Valencia campus. The PERT is offered on a walk
                              in basis, no appointments necessary. You will need to present your VID and state or
                              federally issued picture ID. Students  may take the PERT three times at Valencia.
                              Please note: If another attempt is  needed, you will be required to attend a retake
                              workshop and pay a $10 fee prior to the second attempt (<em>must allow at least 24 hours between initial test and retake</em>). For further information about Valencia Assessment Center testing locations, hours
                              of operation, college-ready scores, and retesting, visit the <a href="/students/assessments/">assessment website</a>.
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Submitting the Dual Enrollment Application</h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li>
                              							<strong><em>Applications will not be accepted at any Valencia College campus location.</em></strong> 
                           </li>
                           						
                           <li>
                              							<strong>Public school</strong> applications must be submitted through the designated school <a href="public-school.php">college coach/counselor</a> or other designated school dual enrollment representative by <strong>10/20</strong>. Check with your school to identify who will handle application submission. <strong></strong>
                              						
                           </li>
                           						
                           <li>
                              							<strong>Private and Charter School</strong> applications must be scanned to a pdf. Document and submitted to <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> by <strong>10/20</strong>.
                           </li>
                           						
                           <li> <strong>Home school </strong>students are required to submit additional documentation which includes: transcripts
                              with unweighted cumulative GPA and a Verification of Enrollment Letter (Provided by
                              the County's Home School Department). Applications and additional documentation must
                              be scanned to a pdf. document and submitted to <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> by <strong>10/20</strong>.
                           </li>
                           						
                           <li>Student will be issued a Valencia ID# (VID), 7 business days after the DE office receives
                              the application
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>Submitting Your Test Scores to Valencia College </h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li><strong><em>Scores must be received by the test score deadline 11/3</em></strong></li>
                           						
                           <li>Once you have received your VID number, the <a href="/students/assessments/pert/taking-the-exam.php">PERT Exam</a> can be taken at a Valencia College Testing Center. <em>A review is available at the bottom of VC's PERT exam website.</em> 
                           </li>
                           						
                           <li>If you have taken the PERT Exam at your high school, please indicate it on your application.
                              If you take it at your high school after application submission, please refer to the
                              ‘Submit High School PERT Scores' link at valenciacollege.edu/dualenrollment to request
                              that your scores be retrieved. 
                           </li>
                           						
                           <li>SAT and ACT scores must be submitted through <a href="https://www.collegeboard.org/">CollegeBoard</a> or <a href="http://www.act.org/">ACT.org</a>. To do this, please login into the applicable account and request that your score
                              report be sent to Valencia College. <strong><em>Please allow 5 weeks for the scores to be received and processed. </em></strong></li>
                           						
                           <li>If the student has eligible college ready test scores on file with Valencia College
                              at the time the application is submitted, the student will be fully processed in 10
                              business days.
                           </li>
                           						
                           <li>If the student does not have eligible college ready test scores on file with Valencia
                              College at the time the application is submitted, the student will receive an email
                              reminder on 10/6 and 10/25 to submit eligible test scores. Once VC has received test
                              scores, the student will be fully processed in 10 business days.
                           </li>
                           					
                        </ul>
                        
                        					
                        <h3>If accepted, when will I receive my acceptance letter?</h3>
                        					
                        <p>An official DE acceptance letter will be mailed to the address listed on the application.
                           All acceptance letters will be mailed by <strong>11/10</strong>. The letter will outline next steps or you can check our website for details at valenciacollege.edu/dual.
                           If you have not received your acceptance letter by 11/10 email <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>.
                        </p>
                        					
                        <h3>How do I check the status of my application?</h3>
                        					
                        <p>Contact your high school college counselor/coach to get updates on your application
                           status. The DE office partners with high school college counselors/coaches to keep
                           an up to date status on all applications. The college counselor/coach at your high
                           school is your first point of contact. 
                        </p>
                        					
                        <p>Private, Charter and Home School applications email <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></p>
                        
                        					
                        <h3>I received my acceptance letter, what next?</h3>
                        					
                        <ul class="list_style_1">
                           						
                           <li> Create your <a href="http://atlas.valenciacollege.edu">Atlas</a> account
                           </li>
                           						
                           <li>Complete the Dual Enrollment Online Orientation (Located within my MyCourses/Blackboard)
                              
                           </li>
                           						
                           <li> Wait 3 business days for the hold to be lifted </li>
                           						
                           <li> Registration for classes begins <strong>11/10</strong>. 
                           </li>
                           					
                        </ul>
                        
                        				
                     </div>
                     				
                     <aside class="col-md-3">
                        					
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        					
                        <div>
                           
                           						
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           						
                           <div data-old-tag="table">
                              							
                              <div data-old-tag="tbody">
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">407-582-1600</div>
                                    								
                                 </div>
                                 
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    								
                                 </div>
                                 
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    								
                                 </div>
                                 							
                              </div>
                              						
                           </div>
                           
                           						
                           <div data-old-tag="table">
                              							
                              <div data-old-tag="tbody">
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td"><strong>
                                          										East Campus 
                                          										</strong></div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">407-582-1600</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr"> 
                                    									
                                    <div data-old-tag="td">
                                       										<a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       									
                                    </div>
                                    
                                    
                                    								
                                 </div>
                                 
                                 
                                 
                                 
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td"><strong>
                                          										Osceola Campus 
                                          										</strong></div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">407-582-1600</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr"> 
                                    									
                                    <div data-old-tag="td">
                                       										<a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       									
                                    </div>
                                    
                                    
                                    								
                                 </div>
                                 
                                 
                                 
                                 
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td"><strong>
                                          										West Campus 
                                          										</strong></div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr">
                                    									
                                    <div data-old-tag="td">407-582-1600</div>
                                    								
                                 </div>
                                 								
                                 <div data-old-tag="tr"> 
                                    									
                                    <div data-old-tag="td">
                                       										<a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       									
                                    </div>
                                    
                                    
                                    								
                                 </div>
                                 
                                 
                                 
                                 
                                 							
                              </div>
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        					
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           						
                           <div>
                              							Oct<br>
                              							<span>20</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           						
                           <div>
                              							Nov<br>
                              							<span>03</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           						
                           <div>
                              							Nov<br>
                              							<span>10</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        
                        					
                        <div data-id="finals_week">
                           
                           						
                           <div>
                              							Dec<br>
                              							<span>11</span>
                              						
                           </div>
                           
                           						
                           <div>        
                              							<a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              						
                           </div>
                           
                           					
                        </div>
                        				
                     </aside>
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/step-by-step-application-process.pcf">©</a>
      </div>
   </body>
</html>