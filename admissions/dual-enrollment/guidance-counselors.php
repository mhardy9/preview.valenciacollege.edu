<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information for Guidance Counselors | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="All students are strongly encouraged to meet with their high school guidance counselors to determine the appropriate courses to meet their high school graduation requirements and count toward postsecondary academic goals. This page provides information for Guidance Counselors and other high school personnel to assist students with applying for Dual Enrollment programs at Valencia College.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/guidance-counselors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Information for Guidance Counselors</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     	  
                     <div class="col-md-9">
                        		  
                        <h2>Information for Guidance Counselors &amp; School Contacts</h2>
                        		  
                        <p>The Dual  Enrollment office is grateful to all guidance counselors, college transition
                           coaches and other school representatives for their continuous support of the  Dual
                           Enrollment program at Valencia College!
                        </p>
                        		  
                        <p>All students are strongly encouraged to meet with their high  school guidance counselors
                           to determine 
                           			  the appropriate courses to meet their  high school graduation requirements and
                           count toward postsecondary 
                           			  academic goals. This applies to students taking courses at the high school location
                           as  well as those 
                           			  enrolled at a Valencia campus.
                        </p>
                        		  
                        <p>It is the  responsibility of the high school guidance counselor to notify the Dual
                           Enrollment office if the student's high school cumulative grade point average (CGPA)
                           falls  below 3.0. In addition, students must adhere to Valencia's Academic Standards
                           as stated in the college catalog. Failure to do so will result in ineligibility  for
                           program participation.
                           		  
                        </p>
                        		  
                        <p>Below are  subsections as a high school representative that will be helpful for you
                           from  the time a student expresses interest in Dual Enrollment, through their  acceptance.
                        </p>
                        		  
                        <div>
                           
                           			  
                           <h3>Application</h3>
                           					  
                           <ul class="list_style_1">
                              						  
                              <li>Students must hold a 3.0 unweighted  cumulative high school GPA for grades 6-11 or
                                 3.0 weighted cumulative GPA for  high school seniors.
                              </li>
                              						  
                              <li>Students applying for first time  admission must complete the Dual Enrollment application;
                                 a new application is  required each term for any student that does not successfully
                                 complete the  admissions process the first time. Students do not need to reapply if
                                 the  student was previously accepted.
                              </li>
                              						  
                              <li>The application includes certification of  a student's eligibility (CGPA, college-level
                                 assessment scores), counselor  recommendation, and requisite signatures. Please encourage
                                 the student to type  on the fillable PDF prior to printing for required signatures.
                                 Typing the  application significantly reduces the number of discrepancies.
                              </li>
                              						  
                              <li>Applications and college-level test  scores (see Assessment/Testing section below)
                                 must be on file with the Dual  Enrollment department by the designated deadline for
                                 the desired term of  enrollment. The Dual Enrollment department will not accept applications
                                 after  the designated deadline each term. In addition, applications are not accepted
                                 at any Valencia campus and must be digitally provided by the deadline. Provide  your
                                 final application to your school CTC or Guidance Counselor to be digitally  sent to
                                 the Dual Enrollment office. 
                              </li>
                              						  
                              <li>Incomplete applications or applicants  with a high school CGPA below a 3.0 are not
                                 accepted.
                              </li>
                              						  
                              <li>Students cannot create an Atlas account  until their application is fully processed
                                 and entered into the database.
                              </li>
                              					  
                           </ul>
                           
                           
                           			  
                           <h3>Assessment/Testing</h3>
                           					  
                           <ul class="list_style_1">
                              						  
                              <li>All prospective students must submit  college-level test scores as part of the application
                                 requirements.
                              </li>
                              						  
                              <li>Acceptable tests include the SAT, ACT or  PERT exam; a combination of eligible scores
                                 will be accepted from the  aforementioned tests only.
                              </li>
                              						  
                              <li>The initial attempt to take the PERT exam  is available at no cost, at any Valencia
                                 campus, on a walk-in basis; prior to  taking the PERT exam the student must have a
                                 VID (Valencia Identification  Number is assigned by the Dual Enrollment office upon
                                 processing the students  application and then provided in email).
                              </li>
                              						  
                              <li>PERT retakes are available after the  student participates in a workshop and pays
                                 the $10 fee for each  section-requiring retake. A  student is allowed a total of three
                                 PERT attempts at Valencia in a two year  period. Valencia will only accept PERT scores
                                 from previously <em>enrolled</em> institutions within two years of test taken. Review is <em>required</em> for  second attempt and optional for third attempt. 
                              </li>
                              						  
                              <li>If the student has qualifying ACT or SAT  scores, it is their responsibility to request
                                 the official score is report sent  electronically to Valencia College, from sat.collegeboard.org
                                 or actstudent.org  within the allotted period. Be advised that it may take five weeks
                                 for scores  to delivered and processed by the Valencia College Assessment Center;
                                 any  scores that are not entered by the designated deadline will not be accepted.
                                 If  the student has original, hard copy results they can bring them to an  Assessment
                                 Center at any Valencia campus to have the scores entered, prior to  the test deadline.
                                 The Assessment department will not accept copies or  screenshots. 
                              </li>
                              						  
                              <li>The student will need their assigned  VIDÂ&nbsp; and a state or federally issued  picture
                                 ID such as a driver’s license, military ID, or passport to submit SAT /  ACT scores
                                 or to take the PERT exam. 
                              </li>
                              						  
                              <li>Please see the Assessment center <a href="/students/assessments/dual-enrollment.php">website</a> for  specific information regarding specific campus hours of operation, test  preparations,
                                 retake policy, etc.
                              </li>
                              						  
                              <li>If the student takes the PERT exam at the  high school or a location other than Valencia
                                 College after the initial  application is submitted, the student will need to inform
                                 the Assessment Center  staff (via email,&nbsp;<a href="mailto:assessment@valenciacollege.edu">assessment@valenciacollege.edu</a>). In the  email to Assessments, please provide the Full Name, Valencia Identification
                                 number (VID) or Social Security Number, and School Name / Test Site Location.  The
                                 student is responsible for contacting Assessments and confirming the scores  were
                                 uploaded from the repository prior to the test score deadline.
                              </li>
                              					  
                           </ul>
                           
                           
                           			  
                           <h3>New Student  Orientation/NSO</h3>
                           					  
                           <ul class="list_style_1">
                              						  
                              <li>All Dual Enrollment students must  complete the mandatory Dual Enrollment New Student
                                 Orientation online via their  Atlas account before they can register for courses;
                                 please allow two business  days for results to post and the hold to release from the
                                 students account.  Students will not be able to register for courses until the hold
                                 is released  and/or their specific registration date.
                              </li>
                              						  
                              <li>Students should contact the Dual  Enrollment department, via their Atlas email, if
                                 the New Student Orientation  course is not loaded into their Blackboard account. 
                              </li>
                              						  
                              <li>The on-campus orientation held throughout  the summer is highly encouraged for all
                                 newly accepted Dual Enrollment  students.
                              </li>
                              					  
                           </ul>
                           
                           
                           			  
                           <h3>Approved Courses</h3>
                           					  
                           <ul class="list_style_1">
                              						  
                              <li>Dual Enrollment students are eligible to  take three-credit or higher college-level
                                 courses. Students are eligible to  take a maximum of 13 credit hours in the Fall or
                                 Spring and seven in the  Summer.
                              </li>
                              						  
                              <li>Students are eligible for four classes  during the Fall and Spring semester and two
                                 during the Summer semester.  Students accepted for the Fall semester are not eligible
                                 to take Summer classes  until they have successfully completed the Fall and Spring
                                 term. If a student  is accepted for the Spring term they are eligible to take Summer
                                 classes the  consequent semester.
                              </li>
                              						  
                              <li>FLEX term courses (any course that is not  offered for the full Fall/Spring term)
                                 are not permitted for DE students.
                              </li>
                              						  
                              <li>As dictated by Florida statute, college  preparatory/developmental courses as well
                                 as most physical education courses  are not approved for coursework with Valencia
                                 College to count as high school  graduation requirements.
                              </li>
                              					  
                           </ul>
                           
                           
                           
                           			  
                           <h3>Textbooks</h3>
                           					  
                           <h4>Orange County Students:</h4>
                           					  
                           <p> Orange County Public School students must  obtain a textbook voucher from your school
                              guidance counselor/other designated  person. Textbooks can be picked up from a Valencia
                              College Campus Bookstore at  least two weeks prior to the start of term and must be
                              purchased within the  first two weeks of the start of the term; no vouchers accepted
                              after two weeks  from start of term. 
                           </p>
                           					  
                           <h4> Osceola County Students:</h4>
                           					  
                           <p> Osceola County Public School students,  check with your school guidance counselor
                              for specific details and procedures  on how to obtain textbooks from the Osceola District
                              Bookstore, as well as  bookstore hours. Any textbooks not available at the Osceola
                              District Bookstore  (ODB) will be purchased via a voucher that is issued by personnel
                              at the ODB.
                           </p>
                           					  
                           <h4> Private and Home School Students:</h4>
                           					  
                           <p> Private and Home School students are  responsible for their own textbook affiliated
                              costs for licensing fees or  electronic media access. You can get your textbooks at
                              your campus bookstore.  Contact the Valencia College bookstore for the textbooks required
                              for your  specific classes each semester.
                           </p>
                           					  
                           <h4> High School Campus Courses:</h4>
                           					  
                           <p> Textbooks for courses offered on a high  school campus are delivered directly to
                              the school site. All textbooks should  be returned to the high school campus once
                              the courses end. 
                           </p>
                           
                           
                           
                           			  
                           <h3>Registration</h3>
                           			  
                           <p>As a guidance counselor / school representative, please review  remaining graduation
                              requirements with students as you collaborate on each semester’s  schedule. All students
                              are encouraged to meet with a Dual Enrollment Academic  Advisor, however, your support
                              to help determine the appropriate courses to  meet their high school graduation requirements.
                           </p>
                           			  
                           <h4> Valencia Campus Courses: </h4>
                           			  
                           <ul class="list_style_1">
                              				  
                              <li>Students self-register via their Atlas  account; detailed instructions are provided
                                 during the on-campus orientation.  Additionally, detailed instructions can be located
                                 within the students Atlas  account, at an Atlas Help Desk or Atlas computer lab at
                                 any Valencia campus.
                              </li>
                              			  
                           </ul>
                           			  
                           <h4>High School Campus Courses: </h4>
                           			  
                           <ul class="list_style_1">
                              				  
                              <li>Dual Enrollment staff enroll students  taking courses at the high school site. A minimum
                                 of 15 eligible DE students  and no more than 25 students are required for a course
                                 offered at the high  school site. Eligible and interested students must request Dual
                                 Enrollment  courses through the high school guidance counselor during their normal
                                 high  school registration period. The class roster for each particular course should
                                 then provided to the Dual Enrollment office for eligibility verification and  registration
                                 completion.
                              </li>
                              			  
                           </ul>
                           
                           
                           		  
                        </div>
                        
                        
                        
                        
                        
                        	  
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        
                        <div>
                           
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           
                           <div>
                              Nov<br>
                              <span>03</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           
                           <div>
                              Nov<br>
                              <span>10</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="finals_week">
                           
                           
                           <div>
                              Dec<br>
                              <span>11</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </aside>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/guidance-counselors.pcf">©</a>
      </div>
   </body>
</html>