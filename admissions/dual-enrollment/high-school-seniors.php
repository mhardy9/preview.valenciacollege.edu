<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>High School Seniors | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Whether you are a newly accepted or current DE student, Dual Enrollment is happy to assist high school seniors with making the transition into degree-seeking students. With tools like the Degree Audit, it's easier than ever to know how close you are to earning your AA degree.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/high-school-seniors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>High School Seniors</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">          
                        					
                        <h2>High School Seniors</h2>
                        					
                        <p>Whether you are a newly accepted or current DE student, Dual Enrollment is happy to
                           assist high school seniors with making the transition into degree-seeking students.
                           With tools like the <a href="http://catalog.valenciacollege.edu/academicpoliciesprocedures/degreeaudit/">Degree Audit</a>, it's easier than ever to know how close you are to earning your AA degree. 
                        </p>
                        					
                        <h4>Are you looking to continue your education at Valencia College?</h4>
                        					
                        <p>Did you know Valencia College is recognized as one of the best colleges in the nation?
                           Not only will you receive the same education as a state university, but you'll do
                           it at half the cost. To continue your education at Valencia College, take the following
                           steps to ensure a seamless transition into a degree-seeking student: 
                        </p>
                        					
                        <ul>
                           						
                           <li>You don't need to reapply. You're already a Valencia College student.</li>
                           						
                           <li>Submit an official high school transcript to the Answer Center on any Valencia College
                              campus 
                           </li>
                           						
                           <li>Complete a Change of Program/Major Form (located within the <em>Student</em> tab in Atlas) and bring it to the Answer Center on any Valencia College campus 
                           </li>
                           						
                           <li>Apply for <a href="../finaid/">Financial Aid</a></li>
                           						
                           <li>Complete the <a href="../admissions-records/florida-residency/status.php">Florida Residency</a> form for the in-state tuition rate. Form located <a href="https://dynamicforms.ngwebsolutions.com/Login.aspx?ReturnUrl=/ShowForm.aspx%3FRequestedDynamicFormTemplate=741af59b-382d-4171-87a7-3dcfb2ab6840">here</a>. 
                           </li>
                           						
                           <li>Home School students are required to provide additional documents: a notarized <a href="../admissions-records/documents/HomeSchoolAffadavit8-2015.pdf">Home School Affidavit</a> verifying high school graduation. 
                           </li>
                           					
                        </ul>
                        					
                        <h4>Are you looking to transfer to a different college/university? </h4>
                        					
                        <ul>
                           						
                           <li>
                              							<a href="../admissions-records/officialtranscripts.php"><strong>Transcript requests</strong></a> can be completed via your Atlas account. Click on the Courses tab ► Select Registration
                              ►Transcript, Grades, Holds ► Request official transcripts; follow prompts to look
                              up specific school code(s) ► Select "as soon as possible" ► Pay. There is a $3.00
                              fee per transcript. 
                           </li>
                           						
                           <li>As of the Spring term, February 1st, a hold for the "Final HS Transcript w/Grad Date"
                              is placed on all senior accounts; this hold does not prevent students from requesting
                              a Valencia transcript. 
                           </li>
                           					
                        </ul>            
                        
                        					
                        <p><a href="potential-aa-graduates.php">Potential AA Graduates</a></p>
                        
                        				
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        
                        <div>
                           
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           
                           <div>
                              Nov<br>
                              <span>03</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           
                           <div>
                              Nov<br>
                              <span>10</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="finals_week">
                           
                           
                           <div>
                              Dec<br>
                              <span>11</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </aside>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/high-school-seniors.pcf">©</a>
      </div>
   </body>
</html>