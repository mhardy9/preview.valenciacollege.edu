<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Eligible Private Schools | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/eligible-private-schools.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Eligible Private Schools</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Eligible Private Schools</h2>
                        
                        <p>Below are private schools who have a current articulation agreement with Valencia
                           College Dual Enrollment for the 2017-2018 school year.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Avalon School</li>
                           
                           <li>Azalea Park Baptist School</li>
                           
                           <li>Beta Preparatory Collegiate Academy</li>
                           
                           <li>Calvary City Christian Academy</li>
                           
                           <li>Center Academy - East</li>
                           
                           <li>Center Academy - Maitland</li>
                           
                           <li>Central Christian Academy - Goldenrod</li>
                           
                           <li>Central Florida Christian Academy</li>
                           
                           <li>Central Florida Preparatory School - Gotha</li>
                           
                           <li>Central Pointe Christian Academy</li>
                           
                           <li>Champion Preparatory Academy</li>
                           
                           <li>Christian Victory Academy</li>
                           
                           <li>Circle Christian School</li>
                           
                           <li>City of Life Christian Academy</li>
                           
                           <li>Community Christian Learning</li>
                           
                           <li>Downey Christian School</li>
                           
                           <li>Eastland Christian School</li>
                           
                           <li>Edgewood Ranch Academy</li>
                           
                           <li>Faith Christian Academy</li>
                           
                           <li>Faith Harvest Christian Academy</li>
                           
                           <li>Family Christian Academy</li>
                           
                           <li>FMI S.T.E.M. Academy</li>
                           
                           <li>Foundation Academy</li>
                           
                           <li>Grace Christian Academy</li>
                           
                           <li>Heritage Christian School</li>
                           
                           <li>IBCK Educational Center</li>
                           
                           <li>Integrity Tabernacle Christian Academy</li>
                           
                           <li>International Community School</li>
                           
                           <li>Lake Rose Christian Academy</li>
                           
                           <li>Leaders Preparatory School</li>
                           
                           <li>Life Christian Academy</li>
                           
                           <li>LIGHT Christian Academy</li>
                           
                           <li>Lion of Judah Academy</li>
                           
                           <li>Muslim Academy of Greater Orlando</li>
                           
                           <li>North Kissimmee Christian School</li>
                           
                           <li>Orangewood Christian School</li>
                           
                           <li>Orlando Christian Preparatory School</li>
                           
                           <li>Osceola Christian Preparatory School</li>
                           
                           <li>Peaceforce Christian Academy</li>
                           
                           <li>PHA Preparatory School</li>
                           
                           <li>Potters House Academy</li>
                           
                           <li>Raising Knowledge Academy</li>
                           
                           <li>Saints Academy Private School</li>
                           
                           <li>SHIP Academy</li>
                           
                           <li>South Orlando Christian Academy</li>
                           
                           <li>Southland Christian School</li>
                           
                           <li>TDR Academy</li>
                           
                           <li>The Crenshaw School</li>
                           
                           <li>The First Academy</li>
                           
                           <li>The Paragon School</li>
                           
                           <li>The Vine Christian Academy</li>
                           
                           <li>Trinity Lutheran Academy</li>
                           
                           <li>Veritas Academy</li>
                           
                           <li>Victory Academy</li>
                           
                           <li>Woodruff Academy</li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" target="_blank" rel="noopener">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>Oct<br> <span>20</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a></div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>Nov<br> <span>03</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a></div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>Nov<br> <span>10</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a></div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>Dec<br> <span>11</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/eligible-private-schools.pcf">©</a>
      </div>
   </body>
</html>