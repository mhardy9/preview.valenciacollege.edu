<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Public School Students | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Students attending a public school in Orange or Osceola county are eligible to participate in Valencia's Dual Enrollment program. We encourage students to consult with their parents/legal guardians and guidance counselor to determine eligibility for the challenge of college level coursework.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/public-school.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Dual Enrollment</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Public School Students </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Public School Students</h2>
                        
                        <p><strong>Summer 2018 Dates:</strong> <br>Application Opens: Monday, 2/26 <br>Application &amp; Test Score Deadline: Friday, 4/6
                        </p>
                        
                        <p><strong>Fall 2018 Dates:</strong> <br>Application Opens: Monday, 2/26 <br>Application Deadline: Friday, 4/6 <br>Test Score Deadline: Friday, 4/20
                        </p>
                        
                        <p>Students attending a public school in Orange or Osceola county are eligible to participate
                           in Valencia's Dual Enrollment program. We encourage students to consult with their
                           parents/legal guardians and Dual Enrollment school representative to determine eligibility
                           for the challenge of college level coursework.
                        </p>
                        
                        <p><a href="#TestScore">Test Score Requirements</a></p>
                        
                        <p><a href="#CTC_Info" target="_top">College Coaches/Counselors (CTC) Information </a></p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Associate in Arts (A.A) Degree Requirements</a></p>
                        
                        <p><a class="button-action_outline" href="/admissions/dual-enrollment/FAQ-current-students.html">Have a Question?<br> Visit Our Frequently Asked Questions (FAQs) Page</a></p>
                        
                        <h3>At A Glance (Criteria):</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Be a resident of Orange/Osceola County and enrolled in grades 6 - 12 at a public school
                              at the time of application.
                           </li>
                           
                           <li>3.0 unweighted high school GPA for grades 6-11th; or 3.0 weighted GPA for 12th grade.
                              Middle school students must have a high school level GPA.
                           </li>
                           
                           <li>Demonstrate college readiness in reading, writing, and math via the PERT, ACT and/or
                              SAT by the test score deadline. We accept any combination of scores. <strong>Orange County Public/Charter School students (OCPS) ONLY:</strong> Demonstrate college readiness in reading and writing/english OR math by the test
                              score deadline. PERT, ACT, SAT, grade 10 FSA and/or Algebra 1 EOC scores will be accepted.
                              We accept any combination of scores. See Test Score Requirements section below for
                              more information.
                           </li>
                           
                        </ul>
                        
                        <h4>Public School Eligibility and Application Checklist:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Be a resident of Orange/Osceola county who is currently attending a public school
                              in Orange/Osceola county; grades 6-12 at the time of application.
                           </li>
                           
                           <li>Complete a free Dual Enrollment application upon parent/legal guardian’s approval.
                              You will only complete General Information and Parent/Guardian and Student Signatures
                              sections; you will <strong>not </strong>complete the Counselor Information section&nbsp;on the Dual Enrollment application.
                           </li>
                           
                           <li>Meet with your Dual Enrollment school representative to discuss DE requirements and
                              to verify your GPA. Your assigned DE school representative will complete the Counselor
                              Information section.
                           </li>
                           
                           <li>Submit your application to the Dual Enrollment school representative assigned to your
                              high school before the application deadline of the desired term of enrollment.
                           </li>
                           
                           <li>A Valencia ID number (VID) will be sent via email to each eligible applicant. Check
                              your email daily for this information and contact your assigned DE school representative
                              for any questions. Previous Dual Enrollment applicants will not receive this email
                              since the VID remains the same.
                           </li>
                           
                        </ul>
                        
                        <h4>Submitting Test Scores:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><strong>ACT:</strong> you are responsible for logging into your <a href="http://act.org/" target="_blank">ACT account</a> and requesting for your test scores to be sent to Valencia College. ACT scores are
                              not valid until received and processed by Valencia College and <em>this process takes five weeks</em>.
                           </li>
                           
                           <li><strong>SAT:</strong> you are responsible for logging into your <a href="http://collegeboard.org/" target="_blank">SAT account</a> and requesting for your test scores to be sent to Valencia College. SAT scores are
                              not valid until received and processed by Valencia College and <em>this process takes five weeks</em>.
                           </li>
                           
                           <li><strong>PERT exam taken at the high school before submitting DE application:</strong> you are responsible for notifying your assigned DE school representative; they will
                              indicate your PERT scores on the Counselor Information section of the DE application.
                           </li>
                           
                           <li><strong>PERT exam taken at the high school after submitting DE application:</strong> you are responsible for completing the <a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">PERT Scores Request Form</a> in order to have those scores retrieved from the PERT Repository and entered into
                              the Valencia system; <em>this process takes 24 hours</em>.
                           </li>
                           
                           <li><strong>Grade 10 FSA/Algebra 1 EOC (Orange County Public/Charter School students ONLY):</strong> you are responsible for notifying your assigned DE school representative; they will
                              indicate your FSA and/or EOC scores on the Counselor Information section of the DE
                              application.
                           </li>
                           
                        </ul>
                        
                        <p>If you have not taken any of the above mentioned exams, the PERT can be taken a maximum
                           of three times at Valencia within a two-year period. <strong>You must have a VID in order to take the PERT at Valencia.</strong> The first time taking the PERT will be free of cost; each re-take will have a cost
                           of $10 per section. It is your responsibility to submit eligible test scores by the
                           test score deadline.
                        </p>
                        
                        <h4>Accepted Student Checklist:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Set up your Atlas account once you receive the official Dual Enrollment acceptance
                              letter via USPS mail.
                           </li>
                           
                           <li>Complete the mandatory Dual Enrollment New Student Orientation via your Atlas account.
                              You will not be able to register for courses until the online orientation is complete.
                           </li>
                           
                           <li>Register for classes via your Atlas account. If you plan to take DE courses offered
                              at your high school, contact your DE school representative to register for these courses.
                              For all courses offered at a Valencia campus, you must register via your Atlas account.
                              Read the details about registering for Valencia courses on the <a href="/admissions/dual-enrollment/registration.php">Dual Enrollment Course Registration page.</a></li>
                           
                           <li>Tuition and lab fees are waived for all DE approved courses. <strong>You are not responsible any tuition balance.</strong></li>
                           
                           <li>You are responsible for picking up textbooks within the allotted two-week time frame
                              (2 weeks from the first day of the course), otherwise you may be held responsible
                              for the cost. For more information on textbooks, visit the <a href="/admissions/dual-enrollment/faqs.php">“What about textbooks?” question.</a></li>
                           
                        </ul>
                        
                        <h4><strong>Important Reminders:</strong></h4>
                        
                        <ul class="list_style_1">
                           
                           <li>You must maintain a 3.0 high school GPA and a minimum 2.0 Valencia College GPA in
                              order to continue participating in the DE program. Academic Probation (two "W"s/withdrawals
                              OR two consecutive terms with a Valencia College GPA below 2.0) will result in dismissal
                              from DE program.
                           </li>
                           
                           <li>You are eligible to register for up to 4 courses/13 credit hours for Fall and Spring
                              terms; 2 courses/7 credit hours for Summer term.
                           </li>
                           
                           <li>As a Dual Enrollment student, a balance will remain on your account until the end
                              of the term. You are not responsible for the tuition balance and it will not impact
                              your registration status.
                           </li>
                           
                           <li>Your DE school representative will help you select appropriate and approved courses
                              to ensure high school graduation requirements are met. After meeting with your DE
                              school representative, you can <a href="/admissions/dual-enrollment/advising.php">book an appointment</a> with a DE advisor to select Valencia courses that will meet your high school requirements.
                           </li>
                           
                        </ul>
                        <a id="CTC_Info" name="CTC_Info"></a>
                        
                        <h3>College Coaches/Counselors (CTC)</h3>
                        
                        <p><em>*Orange County Public/Charter school students are required to submit their application
                              to their CTC.</em></p>
                        
                        <table class="table table">
                           <caption>
                              
                              <h4><strong>Orange County Public/Charter Schools</strong></h4>
                              
                           </caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    
                                    <p>Linda Constantino</p>
                                    
                                    <p><a href="mailto:Linda.constantino@ocps.net">Linda.Constantino@ocps.net</a></p>
                                    
                                 </th>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>East River High School</li>
                                       
                                       <li>Timber Creek High School</li>
                                       
                                       <li>University High School</li>
                                       
                                       <li>Winter Park High School</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    
                                    <p>Renee Hudson</p>
                                    
                                    <p><a href="mailto:Renee.hudson@ocps.net%20">Renee.Hudson@ocps.net</a></p>
                                    
                                 </th>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Ocoee High School</li>
                                       
                                       <li>Olympia High School</li>
                                       
                                       <li>West Orange High School</li>
                                       
                                       <li>Windermere High School</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    
                                    <p>Latashia Joseph Edwards</p>
                                    
                                    <p><a href="mailto:Latashia.Edwards@ocps.net">Latashia.Edwards@ocps.net</a></p>
                                    
                                 </th>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Colonial High School</li>
                                       
                                       <li>Cypress Creek High School</li>
                                       
                                       <li>Lake Nona High School</li>
                                       
                                       <li>Oak Ridge High School</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    
                                    <p>Susan Robbins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    
                                    <p><a href="mailto:Sue.robbins@ocps.net">Susan.Robbins@ocps.net</a></p>
                                    
                                 </th>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Apopka High School<u></u></li>
                                       
                                       <li>Edgewater High School</li>
                                       
                                       <li>Evans High School</li>
                                       
                                       <li>Wekiva High School</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">
                                    
                                    <p>Nicole Gurley</p>
                                    
                                    <p><a href="mailto:Nicole.vaia@ocps.net%20">Nicole.Gurley@ocps.net </a></p>
                                    
                                 </th>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Boone High School<u></u></li>
                                       
                                       <li>Dr. Phillips High School</li>
                                       
                                       <li>Freedom High School</li>
                                       
                                       <li>Jones High School</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           <caption>
                              
                              <h4>Osceola County</h4>
                              
                           </caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th style="text-align: center;" colspan="2" scope="row">Osceola County will not submit DE applications to CTC; contact DE high school representative
                                    to submit application.
                                 </th>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>Test Score Requirements</h3>
                        <a id="TestScore" name="TestScore"></a>
                        
                        <p>All Dual Enrollment applicants are required to have college-ready test scores on file
                           at Valencia College by the deadline for the desired term of enrollment. <strong><em>Any combination of eligible scores (see below) will be accepted.</em></strong></p>
                        
                        <table class="table table">
                           <caption>Eligible Test Scores</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="row">PERT</th>
                                 
                                 <td>Reading: 106</td>
                                 
                                 <td>Writing: 103</td>
                                 
                                 <td>Math: 123 (College Algebra)<br> 114 (Intermediate Algebra)
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">SAT</th>
                                 
                                 <td>Reading: 24</td>
                                 
                                 <td>Writing: 25</td>
                                 
                                 <td>Math: 26.5 (College Algebra)<br> 24 (Intermediate Algebra)
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">ACT</th>
                                 
                                 <td>Reading: 19</td>
                                 
                                 <td>English: 17</td>
                                 
                                 <td>Math: 21 (College Algebra)<br> 19 (Intermediate Algebra)
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           <caption>Only Orange County Public/Charter Schools</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="row">Florida Standards Assessments (FSA)</th>
                                 
                                 <td>Grade 10 FSA: <strong>3 or higher</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">End Of Course (EOC)</th>
                                 
                                 <td>Algebra I EOC: <strong>3 or higher</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">PERT</th>
                                 
                                 <td>Math: <strong>96</strong> (Intro Statistical Reasoning or College Mathematics)
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <!-- <a href="documents/dual-enrollment-application.pdf" target="_blank" class="button-action_outline">Apply Now (PDF)</a>
<p><a href="step-by-step-application-process.html">Application Process</a></p>

<p>Applications will not be accepted in-person from students/parents at any Valencia College campus. </p> -->
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" target="_blank" rel="noopener">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>Oct<br> <span>20</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a></div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>Nov<br> <span>03</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a></div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>Nov<br> <span>10</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a></div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>Dec<br> <span>11</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/public-school.pcf">©</a>
      </div>
   </body>
</html>