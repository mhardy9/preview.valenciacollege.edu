<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>FAQS | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>FAQS</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <h3>What is dual enrollment?</h3>
                        
                        <p>Dual enrollment is an acceleration program that allows students to pursue an advanced
                           curriculum relevant to their individual college interests and simultaneously earn
                           credit toward high school graduation requirements.
                        </p>
                        
                        <h3>How much does it cost?</h3>
                        
                        <p>Application fees are waived. New legislation requires schools to pay, a discounted
                           tuition rate of $71.98, for all public and private dual enrolled students registered
                           for approved courses. Each school is invoiced at the end of the applicable term. Application
                           and tuition fees are waived for all homeschool dual enrollment students enrolled in
                           approved courses. Home and private school students are responsible for their own textbooks/affiliated
                           costs.
                        </p>
                        
                        <p>DE students do not pay for courses; a fee will appear on all student accounts when
                           you initially register for the term and will remain on your account until your public
                           or private school is invoiced at the end of each applicable term (fall, spring).
                        </p>
                        
                        <h3>What about textbooks?</h3>
                        
                        <p><u>Orange County Public School students:</u> Once you have completed you official registration, print your Student Detail Schedule
                           from Atlas. Present the schedule to your DE school representative to redeem a textbook
                           voucher. Once you have obtained a voucher, you must personally go to the campusâ€™
                           bookstore associated to the course. If you are taking courses on multiple campuses
                           or online, you must visit the specific campus bookstore(s). Online courses are assigned
                           to specific campus. You will have until the second week of classes at Valencia to
                           pick up your textbooks.
                        </p>
                        
                        <p><u>Osceola County Public School students:</u> Once you have completed your official registration, print your Student Detail Schedule
                           from Atlas. Present your printed detail schedule to you DE school representative for
                           approval. Once your schedule has been approved and stamped, you can visit the Osceola
                           County District bookstore for textbook pickup. Textbooks must be retrieved before
                           the second week of classes.
                        </p>
                        
                        <p><u>Homeschool and Private School students:</u> You are responsible for purchasing textbooks.
                        </p>
                        
                        <h3>Which Valencia courses should I take?</h3>
                        
                        <p>The purpose of dual enrollment is to prioritize meeting high school requirements while
                           gaining college credits, simultaneously. According to the Florida Department of Education,
                           the requirments for a high school diploma are: 4 english credits; 4 math credits;
                           3 science credits; 3 social science credits; 1 physical education credit; 8 elective
                           credits; 1 fine art, speech or practical art credit; 2 world language credits (in
                           the same language) and 1 online course.
                        </p>
                        
                        <h3>How can I find out which college courses can transfer back to my high school?</h3>
                        
                        <p>We encourage you to speak with your DE school representative to discuss unmet high
                           school requirements before meeting with a dual enrollment advisor. To further discuss
                           college courses and how they transfer to your high school, schedule an appointment
                           with a dual enrollment advisor by clicking on the "Advising Appointments" link on
                           the navigation panel.
                        </p>
                        
                        <h3>Can I satisfy all high school requirements at Valencia?</h3>
                        
                        <p>No. All high school requirements can be satisfied, with the exception of world history
                           and physical education.
                        </p>
                        
                        <h3>When and where are Valencia courses offered?</h3>
                        
                        <p>Courses are offered before, during, after school hours, and also during summer break.
                           You may take classes at any one of Valencia's 7 campuses and, in some cases, at the
                           high school. Ask your DE school representative if dual enrollment classes are offered
                           at your high school campus.
                        </p>
                        
                        <h3>Can I take online courses?</h3>
                        
                        <p>Yes, dual enrollment students are eligible to take online courses as well as hybrid
                           courses.
                        </p>
                        
                        <h3>How many high school credits are awarded for dual enrollment courses?</h3>
                        
                        <p>All dual enrollment courses are awarded 0.5 or 1.0 credit towards high school graduation
                           requirements. College credit is counted differently; each course will award at least
                           3 credits towards college graduation.&nbsp;
                        </p>
                        
                        <h3>Will credits earned in the Dual Enrollment program transfer to other colleges and
                           universities?
                        </h3>
                        
                        <p>Dual enrollment credits may be used toward a Valencia degree or transferred to any
                           public college or university in Florida. Students who plan to transfer to a private
                           college or to an institution in another state should contact the institution of interest
                           to ask about their dual enrollment course transfer policies.
                        </p>
                        
                        <h3>Can I take dual enrollment courses after my high school graduation date?</h3>
                        
                        <p>No. You are eligible to participate in the dual enrollment program until the end of
                           the spring semester of your high school senior year.&nbsp;
                        </p>
                        
                        <h3>If I continue at Valencia after graduating from high school, do I have to re-apply?</h3>
                        
                        <p>No, you will not have to apply for admission again. You will be required to submit
                           the following: FAFSA, Florida residency forms, change of major form and a final official
                           high school transcript by the appropriate deadlines in order to register for courses
                           as a degree-seeking student.
                        </p>
                        
                        <h3>Can dual enrollment courses count toward a Bright Futures Scholarship?</h3>
                        
                        <p>Yes. Talk with your high school counselor or contact Bright Futures regarding how
                           courses may be applied. The <a href="https://www.osfaffelp.org/bfiehs/fnbpcm02_CCTMain.aspx">Bright Futures Comprehensive Table (CCT)</a> lists all courses considered for state scholarships. Dual enrollment courses are
                           found by scrolling to the bottom of each subject area list of courses. For each course
                           the CCT displays the number of credits applied, its application to the different scholarship
                           levels, and if the course is identified as "core" for admissions purposes to the State
                           University System.
                        </p>
                        
                        <h3>What is the difference between dropping and withdrawing from a course?</h3>
                        
                        <p>If you DROP a course, before the Drop/Refund deadline, it is as if you never registered
                           for the course and it will not appear on your college transcript. If you remove a
                           course after the Drop/Refund deadline a "W" (WITHDRAWAL) will show on your transcript
                           as the grade for that course. DE students have a maximum of one withdrawal. DE students
                           who obtain two or more withdrawals will be dismissed from the dual enrollment program.
                        </p>
                        
                        <h3>Can a DE student pay their own tuition and fees in order to register for a course
                           that is not DE approved?
                        </h3>
                        
                        <p>No. Dual enrollment students must meet eligibility requirements to enroll in eligible
                           courses.
                        </p>
                        
                        <h3>What is a Dual Enrollment Articulation Agreement?</h3>
                        
                        <p>The Dual Enrollment Articulation Agreement between a school district and a postsecondary
                           institution establishes guidelines for implementing the program for eligible students.
                           Section 1007.271(21), F.S., mandates that a school district and the local Florida
                           College System institution enter into an agreement. The establishment of other articulation
                           agreements (e.g., school districts and state universities or private postsecondary
                           institutions, colleges and private secondary schools) are optional.
                        </p>
                        
                        <h3>Must Valencia College enter into a Dual Enrollment Articulation Agreement with a private
                           school?
                        </h3>
                        
                        <p>No. Florida law requires the Florida College System institution and school district
                           to enter into a Dual Enrollment Articulation Agreement. It is optional for the Florida
                           College System institution to enter into articulation agreements with private schools.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" target="_blank" rel="noopener">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 3, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>Oct<br> <span>20</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a></div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>Nov<br> <span>03</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a></div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>Nov<br> <span>10</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a></div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>Dec<br> <span>11</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/faqs.pcf">©</a>
      </div>
   </body>
</html>