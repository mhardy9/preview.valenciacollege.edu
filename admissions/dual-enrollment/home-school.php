<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Home School Students | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Homeschool students registered with the Orange or Osceola County Home School Office are eligible to participate in Valencia's Dual Enrollment program. We encourage students to consult with their parents/legal guardians to determine eligibility for the challenge of college level coursework.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/home-school.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Dual Enrollment</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>Home School Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Home School/Florida Virtual School (FLVS) Students</h2>
                        
                        <p><strong>Summer 2018 Dates:</strong> <br>Application Opens: Monday, 2/26 <br>Application &amp; Test Score Deadline: Friday, 4/6
                        </p>
                        
                        <p><strong>Fall 2018 Dates:</strong> <br>Application Opens: Monday, 2/26 <br>Application Deadline: Friday, 4/6 <br>Test Score Deadline: Friday, 4/20
                        </p>
                        
                        <p>Homeschool and Florida Virtual School (FLVS) students registered with Orange or Osceola
                           County Home Education program are eligible to participate in Valencia's Dual Enrollment
                           program. We encourage students to consult with their parents/legal guardians to determine
                           eligibility for the challenge of college level coursework.
                        </p>
                        
                        <p><a href="#TestScore">Test Score Requirements </a></p>
                        
                        <p><a href="http://catalog.valenciacollege.edu/degrees/associateinarts/courserequirements/">Associate in Arts (A.A) Degree Requirements</a></p>
                        
                        <p><a class="button-action_outline" href="/admissions/dual-enrollment/FAQ-current-students.html">Have a Question?<br>Visit Our Frequently Asked (FAQs) Page</a></p>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Dual Enrollment Information Sessions</h3>
                        
                        <p>DE Information Sessions for Home School and Florida Virtual School (FLVS) students
                           (grades 6-12) that are interested in participating in the Dual Enrollment program
                           for the Summer/Fall 2018 semesters will be held on the dates below; please RSVP.
                        </p>
                        
                        <ul>
                           
                           <li><strong>Monday, February 19th</strong> from 6pm-7pm at Valencia’s Osceola campus (Room: 4-105)
                           </li>
                           
                           <li><strong>Friday, February 23rd</strong> from 6pm-7pm at Valencia’s West campus (Room: 3-111)
                           </li>
                           
                        </ul>
                        
                        <p><a class="button-action_outline" href="http://vcdualenrollment.eventbrite.com" target="_blank">RSVP</a></p>
                        
                        <p>&nbsp;</p>
                        
                        <h3><strong>At A Glance (Criteria):</strong></h3>
                        
                        <ul>
                           
                           <li>Be a resident of Orange or Osceola County and enrolled in grades 6 – 12 in a home
                              school program&nbsp;at the time of application.
                           </li>
                           
                           <li>3.0 unweighted high school GPA for grades 6-11th; or 3.0 weighted GPA for 12th grade
                              at the time of application. Middle school students must have a high school level GPA.
                           </li>
                           
                           <li>Demonstrate college readiness in reading, writing and math via the PERT, ACT or SAT
                              by the assessment scores deadline. We accept any combination of scores.
                           </li>
                           
                        </ul>
                        
                        <h4>Home School School/FLVS Application Checklist:</h4>
                        
                        <ul>
                           
                           <li>Complete the DE application. The Counselor Information section must be completed by
                              the parent/legal guardian.
                           </li>
                           
                           <li>Submit the DE application package via email to <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or in person at any DE office by the application deadline of the desired term of
                              enrollment. The DE application package includes: the completed DE application, the
                              Letter of Verification of Home Education provided by Orange/Osceola County AND an
                              unofficial transcript with the student’s high school CGPA. <strong>Incomplete application packages will not be accepted.</strong></li>
                           
                           <li>Once application is processed, a Valencia ID number (VID) will be sent via email to
                              each eligible applicant; check your email daily for this. Previous Dual Enrollment
                              applicants will not receive this email since the VID remains the same.
                           </li>
                           
                        </ul>
                        
                        <h4>Submitting Test Scores:</h4>
                        
                        <ul>
                           
                           <li><strong>ACT:</strong> you are responsible for logging into your ACT account (insert link: act.org) and
                              requesting for your test scores to be sent to Valencia College. ACT scores are not
                              valid until received and processed by Valencia College and <em><u>this process takes five weeks</u></em>.
                           </li>
                           
                           <li><strong>SAT:</strong> you are responsible for logging into your SAT account (insert link: collegeboard.org)
                              and requesting for your test scores to be sent to Valencia College. SAT scores are
                              not valid until received and processed by Valencia College and <em><u>this process takes five weeks</u></em>.
                           </li>
                           
                           <li><strong>PERT exam taken at a high school <u>before</u> submitting DE application:</strong> you are responsible for notifying your parent/legal guardian; they will indicate
                              your PERT scores on the Counselor Information section of the DE application.
                           </li>
                           
                           <li><strong>PERT exam taken at a high school <u>after</u> submitting DE application:</strong> you are responsible for&nbsp; completing the <a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">PERT Scores Request Form</a> in order to have those scores retrieved from the PERT Repository and entered into
                              the Valencia system; <em><u>this process takes 24 hours</u></em>.
                           </li>
                           
                        </ul>
                        
                        <p>If you have not taken any of the above mentioned exams, the PERT can be taken a maximum
                           of three times at Valencia within a two-year period. <strong>You must have a VID in order to take the PERT at Valencia.</strong> The first time taking the PERT will be free of cost; each re-take will have a cost
                           of $10 per section. It is your responsibility to submit eligible test scores by the
                           test score deadline.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h4>Accepted Student Checklist:</h4>
                        
                        <ul>
                           
                           <li>Set up your Atlas account once you receive the official Dual Enrollment acceptance
                              letter via USPS mail.
                           </li>
                           
                           <li>Complete the mandatory Dual Enrollment New Student Orientation via your Atlas account.
                              You will not be able to register for courses until the online orientation is complete.
                           </li>
                           
                           <li>Register for Valencia courses via your Atlas account. Read the details about registering
                              for Valencia courses on the<a href="/dual/registration.cfm"> Dual Enrollment Course Registration page</a></li>
                           
                           <li>Tuition and lab fees are waived for all DE approved courses. <strong>You are not responsible any tuition balance.</strong></li>
                           
                           <li>Obtain textbooks for courses; you are responsible for purchasing textbooks.</li>
                           
                           <li>Complete and submit the Home School Articulation agreement that will be sent to your
                              Atlas email.
                           </li>
                           
                        </ul>
                        
                        <h4>Important Reminders:</h4>
                        
                        <ul>
                           
                           <li>You must maintain a 3.0 high school GPA and a minimum 2.0 Valencia College GPA in
                              order to continue participating in the DE program. Academic Probation (two "W"s/withdrawals
                              OR two consecutive terms with a Valencia College GPA below 2.0) will result in dismissal
                              from DE program.
                           </li>
                           
                           <li>You are eligible to register for up to 4 courses/13 credit hours for Fall and Spring
                              terms; 2 courses/7 credit hours for Summer term.
                           </li>
                           
                           <li>As a Dual Enrollment student, a balance will remain on your account until the end
                              of the term. You are not responsible for the tuition balance and it will not impact
                              your registration status.
                           </li>
                           
                           <li>Your parent/legal guardian is considered your counselor/home school program official
                              and is responsible for ensuring that you meet requirements for successful high school
                              graduation and providing documentation of coursework as requested. If you need additional
                              help selecting courses, you can <a href="/dual/Advising.cfm">book an appointment with a DE advisor</a> to select Valencia courses that will meet your high school requirements.
                           </li>
                           
                           <li>You will not be eligible for the DE program after your 19th birthday. (Consideration
                              for an exception to the age requirement must go through a review process which includes
                              submitting relevant documents and a meeting with the Director of Dual Enrollment.)
                           </li>
                           
                        </ul>
                        
                        <p>All students are required to submit and have college-ready test scores on file at
                           Valencia College for reading, writing <em>and</em> math, by the deadline for the desired term of enrollment. <strong><em>Any combination of eligible scores (see below) will be accepted.</em></strong></p>
                        
                        <h3>Test Score Requirements</h3>
                        <a id="TestScore" name="TestScore"></a>
                        
                        <p>All students are required to have college-ready test scores on file at Valencia by
                           the deadline for the desired term of enrollment. Any combination of eligible scores
                           (see below) will be accepted.
                        </p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th valign="top">
                                    
                                    <p align="center"><strong>PERT</strong></p>
                                    
                                 </th>
                                 
                                 <td valign="top">
                                    
                                    <p>Reading:&nbsp;<strong>106</strong><br> Writing:&nbsp;<strong>103</strong><br> Math:&nbsp;<strong>114 </strong>(Intermediate Algebra)<br> <strong>124 </strong>(College Algebra)
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th valign="top">
                                    
                                    <p align="center"><strong>SAT </strong></p>
                                    
                                 </th>
                                 
                                 <td valign="top">
                                    
                                    <p>Reading:&nbsp;<strong>24</strong><br> Writing: <strong>25</strong><br> Math: <strong>24 </strong>(Intermediate Algebra)<br> <strong>26.5 </strong>(College Algebra)
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th valign="top">
                                    
                                    <p align="center"><strong>ACT</strong></p>
                                    
                                 </th>
                                 
                                 <td valign="top">
                                    
                                    <p>Reading&nbsp;<strong>19</strong><br> English&nbsp;<strong>17</strong><br> Math&nbsp;<strong>19 </strong>(Intermediate Algebra)<br> <strong>21 </strong>(College Algebra)
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" target="_blank" rel="noopener">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>Oct<br> <span>20</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a></div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>Nov<br> <span>03</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a></div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>Nov<br> <span>10</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a></div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>Dec<br> <span>11</span></div>
                           
                           <div><a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/home-school.pcf">©</a>
      </div>
   </body>
</html>