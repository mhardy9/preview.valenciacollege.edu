<ul>
	<li><a href="index.php">Dual Enrollment</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<div class="menu-wrapper c3">
			<div class="col-md-4">
				<ul>

					<li><a href="advising.php">Advising Appointments</a></li>
					<li><a href="public-school.php">Public School Students</a></li>
					<li><a href="private-school.php">Private School Students</a></li>
					<li><a href="home-school.php">Home School Students</a></li>
					<li><a href="high-school-seniors.php">High School Seniors</a></li>
					<li><a href="registration.php">Course Registration</a></li>
					<li><a href="dual-enrollment-faculty.php">Faculty Information</a></li>
					<li><a href="guidance-counselors.php">Guidance Counselor Information</a></li>
					<li><a href="faqs.php">New Student FAQs</a></li>
					<li><a href="faq-current-students.php">DE Student FAQs</a></li>
					<li><a href="http://net4.valenciacollege.edu/forms/dual/contact.cfm" target="_blank">DE Updates</a></li>
				</ul>
			</div>
		</div>
	</li>
</ul>
