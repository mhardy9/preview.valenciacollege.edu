<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dual Enrollment  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Dual Enrollment</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li>Dual Enrollment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>About Dual Enrollment</h2>
                        
                        
                        <h3>Announcements</h3>
                        	
                        <p>
                           		<strong>Summer 2018 Dates:</strong>
                           		<br>Application Opens: Monday, 2/26
                           		<br>Application &amp; Test Score Deadline: Friday, 4/6
                           	
                        </p>
                        
                        	
                        <p>
                           		<strong>Fall 2018 Dates:</strong>
                           		<br>Application Opens: Monday, 2/26
                           		<br>Application Deadline: Friday, 4/6
                           		<br>Test Score Deadline: Friday, 4/20
                           	
                        </p>
                        
                        <h3>What is Dual Enrollment (DE)?</h3>
                        
                        <p>The Dual Enrollment Program enables qualified public, private and home education students
                           an opportunity to participate in an academic acceleration program. 
                        </p>
                        
                        <p>Dual Enrollment allows students to pursue an advanced curriculum that earns the student
                           both high school and college credit simultaneously.
                        </p>
                        
                        
                        <!-- <script src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/37835002/partner_id/2070621?autoembed=true&amp;entry_id=1_atn2xhxt&amp;playerId=kaltura_player_1509373310&amp;cache_st=1493911387&amp;width=400&amp;height=333&amp;flashvars[streamerType]=auto"></script> -->
                        
                        <iframe src="https://cdnapisec.kaltura.com/p/2070621/sp/207062100/embedIframeJs/uiconf_id/37835002/partner_id/2070621?iframeembed=true&amp;playerId=kaltura_player_1509373310&amp;entry_id=1_atn2xhxt&amp;flashvars[streamerType]=auto" width="560" height="395" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozAllowFullScreen="mozAllowFullScreen" frameborder="0"></iframe>
                        
                        
                        <h3>Benefits of Dual Enrollment </h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Get a head start on earning college credit </li>
                           
                           <li>Gain access to College resources and campus life</li>
                           
                           <li>Explore interesting subjects through a variety of course offerings</li>
                           
                           <li>Engage with different instructional methods available: Classroom, Online, or Hybrid
                              
                           </li>
                           
                           <li>Experience the college atmosphere </li>
                           
                           <li>Offset the cost of a college education</li>
                           
                           <li>Stand out among your peers</li>
                           
                        </ul>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/dual/contact.cfm">Receive Updates from Dual Enrollment, Sign Up Now!</a></p>
                        
                        
                        <h3>Student Testimonials</h3>
                        
                        <blockquote>
                           
                           <p>It was so fun to experience college life and make new friends. You will not regret
                              Dual Enrollment. 
                           </p>
                           
                           <p>— <strong>Tatiana - Senior (Osceola County)</strong></p>
                           
                        </blockquote>
                        
                        <blockquote>
                           
                           <p>I was scared I wasn’t ready to take college classes. But with the help of my professors
                              and advisor, I even got through my hardest math class. 
                           </p>
                           
                           <p>— <strong>Cameron - Junior (Orange County)</strong></p>
                           
                        </blockquote>
                        
                        <blockquote>
                           
                           <p>My first college class was Intro to Programming, where I learned about coding and
                              got to work on some cool assignments. 
                           </p>
                           
                           <p>— <strong>Justin - Freshman (Orange County)</strong></p>
                           
                        </blockquote>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <!--     <a href="documents/dual-enrollment-application.pdf" target="_blank" class="button-action_outline">Apply Now (PDF)</a>
    <p><a href="step-by-step-application-process.html">Application Process</a></p>
    <p>Applications will not be accepted in-person from students/parents at any Valencia College campus. </p> -->
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        <div>
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM
                                       <br>Friday: 9AM to 5PM
                                       <br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           <div>
                              Oct
                              <br>
                              <span>20</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a>
                              <br>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           <div>
                              Nov
                              <br>
                              <span>03</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a>
                              <br>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           <div>
                              Nov
                              <br>
                              <span>10</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a>
                              <br>
                              
                           </div>
                           
                        </div>
                        
                        <div data-id="finals_week">
                           
                           <div>
                              Dec
                              <br>
                              <span>11</span>
                              
                           </div>
                           
                           <div>
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a>
                              <br>
                              
                           </div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/index.pcf">©</a>
      </div>
   </body>
</html>