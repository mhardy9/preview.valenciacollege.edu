<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>FAQs for Current Students | Dual Enrollment | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/dual-enrollment/faq-current-students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/dual-enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Dual Enrollment</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/dual-enrollment/">Dual Enrollment</a></li>
               <li>FAQs for Current Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Frequently Asked Questions for Current Students</h2>
                        
                        
                        <h3>Can I satisfy all of my HS Requirements at Valencia?</h3>
                        
                        <p>Most high school requirements can be satisfied, with the exception of World History
                           and the Physical Education Requirement.
                        </p>
                        
                        
                        <h3>What about Economics?</h3>
                        
                        <p>The <strong>Orange County</strong> School District recently created a Financial Literacy on-line module to allow Dual
                           Enrollment students to register for and satisfy their High school Requirement. The
                           module is required along with Valencia's ECO 2013 or ECO b2023 courses to satisfy
                           the Financial Literacy Graduation Requirement. Students will be required to meet with
                           their Guidance counselor in order to enroll in a DE Economics course.
                        </p>
                        
                        <p>Students and parents will sign an agreement acknowledging that if they do not complete
                           the module, they will not fulfill the graduation requirement
                        </p>
                        
                        <p><strong>Osceola County</strong> students will need to complete Economics with their high school or FLVS in order
                           to satisfy their high school graduation requirements. 
                        </p>
                        
                        
                        <h3>Where and how do I get my  textbooks?</h3>
                        
                        <p><strong>Orange County Public School</strong> students must obtain a textbook voucher from your school guidance counselor/other
                           designated person.
                        </p>
                        
                        <p>Textbooks can be picked up from a Valencia College Campus Bookstore at least two weeks
                           prior to the start of term and must be purchased within the first two weeks of the
                           start of the term; no vouchers accepted after two weeks from start of term.
                        </p>
                        
                        <p><strong>Osceola County Public School </strong>students, check with your school guidance counselor for specific details and procedures
                           on how to obtain textbooks from the <a href="http://www.osceolaschools.net/cms/one.aspx?portalId=567190&amp;pageId=1721736">Osceola District Bookstore</a>, as well as bookstore hours. Any textbooks not available at the Osceola District
                           Bookstore will be purchased via a voucher that is issued by personnel at the ODB.
                        </p>
                        
                        <p><strong>Private and Home School </strong>students are responsible for their own textbook affiliated costs for licensing fees
                           or electronic media access. 
                        </p>
                        
                        <p>To view which textbooks are required for your class, please visit the <a href="http://www.valenciabookstores.com/">Campus Bookstore's web page</a>. 
                        </p>
                        
                        
                        <h3>What courses can should I take?</h3>
                        
                        <p> While courses offered at a high school campus are limited to certain basic subjects,
                           courses taken at Valencia should be creditable toward a high school diploma. Speak
                           with your high school guidance counselor about course selection to ensure that you
                           meet graduation requirements. 
                        </p>
                        
                        
                        <h3>How many classes/credits can I take?</h3>
                        
                        <p> You can register for up to 4 courses or 13 credit hours each Fall and Spring term;
                           and 2 courses or 7 credit hours for the Summer term. 
                        </p>
                        
                        <p>Payment cannot be received for increased coursework.</p>
                        
                        
                        <h3>Can I take an Online Course?</h3>
                        
                        <p> Yes, Dual Enrollment students are allowed to register for online courses. Onlne courses
                           are conducted through <a href="https://learn.valenciacollege.edu/">Blackboard</a>.
                        </p>
                        
                        
                        <h3>What is the difference between Add/Drop and Withdrawal?</h3>
                        
                        <p> <strong>Add/Drop</strong> allows students to add and remove courses from their schedule without penalty within
                           the add/drop period. <strong>Withdrawals</strong> remain on the student’s college transcript, and count against their withdrawal allotment
                           for Dual Enrollment participation. 
                        </p>
                        
                        
                        <h3>How many Withdrawals are DE students allowed??</h3>
                        
                        <p>Dual Enrollment Student are allowed <strong>one</strong> withdrawal to maintain eligibility.
                        </p>
                        
                        
                        <h3>Help! I have an a balance on my account</h3>
                        
                        <p>This is normal. A balance will remain on your account until the end of the term. Dual
                           Enrollment students are not responsible for paying these fees. This balance will not
                           cause you to be dropped from your courses. 
                        </p>
                        
                        
                        <h3>Where can I get my student ID and parking pass?</h3>
                        
                        <p>Student ID's and parking permits are issued by the <a href="/students/security/parking.php">Security Office</a>. 
                        </p>
                        
                        
                        <h3>I've registered for classes. Where can I view and print my schedule?</h3>
                        
                        <p>You can access your detailed class schedule by logging into the Atlas → Clicking the
                           <em>Courses</em> Tab → Registration → Student Detail Schedule. 
                        </p>
                        
                        
                        <h3>Who can help me choose classes to register for?</h3>
                        
                        <p>Dual Enrollment Academic Advisors and your school's guidance counselor can help guide
                           you through the course selection process. Dual Enrollment Advisors are available on
                           the West and Osceola Campuses M-F by appointment only. Please visit our <a href="advising.php">Advisors Page</a> to schedule an appointment. 
                        </p>
                        
                        
                        <h3>Do Dual Enrollment courses transfer to other colleges and universities?</h3>
                        
                        <p>Dual enrollment course credit transfers to any public college or university that offers
                           that course with the same prefix and number. If a student does not attend the same
                           college or university where they earned that dual enrollment credit, the application
                           of the college credit to general education, prerequisite, or degree programs may vary
                           at the receiving institutions.
                        </p>
                        
                        
                        <h3>I took an AP class at my high school, how can I apply that towards my AA?</h3>
                        
                        <p>AP Scores must be reported directly to Valencia via <a href="https://apstudent.collegeboard.org/home?navid=gh-aps">College Board</a> prior to registering for courses which require the completion of prior courses to
                           satisfy prerequisites. Please allow 5 weeks for the scores to be processed.
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="https://www.facebook.com/groups/valenciacollegedualenrollment" rel="noopener" target="_blank">Join our Facebook group</a></p>
                        
                        
                        <div>
                           
                           
                           <h3>CONTACT INFORMATION</h3>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8AM to 5PM<br>Friday: 9AM to 5PM<br>Lunch Hour: 12PM to 1PM
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 5 Room 211</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Suite 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Student Services Building (SSB), Suite 146</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1600</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <h3>UPCOMING EVENTS</h3>
                        
                        
                        
                        <div data-id="dual_enrollment_application_closes_spring_2018">
                           
                           
                           <div>
                              Oct<br>
                              <span>20</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/dual_enrollment_application_closes_spring_2018" target="_blank">Dual Enrollment Application Closes (Spring 2018)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="test_score_deadline_dual_enrollment">
                           
                           
                           <div>
                              Nov<br>
                              <span>03</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/test_score_deadline_dual_enrollment" target="_blank">Test Score Deadline (Dual Enrollment)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="fall_2017_withdrawal_deadline_w_grade">
                           
                           
                           <div>
                              Nov<br>
                              <span>10</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/fall_2017_withdrawal_deadline_w_grade" target="_blank">Fall 2017 Withdrawal Deadline ("W" Grade)</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="finals_week">
                           
                           
                           <div>
                              Dec<br>
                              <span>11</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/finals_week" target="_blank">Finals Week</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </aside>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/dual-enrollment/faq-current-students.pcf">©</a>
      </div>
   </body>
</html>