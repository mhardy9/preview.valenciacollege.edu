<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Enrollment  | Valencia College</title>
      <meta name="Description" content="Enrollment | Valencia Refund | Valencia College">
      <meta name="Keywords" content="college, school, educational, valencia, refund, enrollment">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/refund/enrollment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/refund/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Refund</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/refund/">Refund</a></li>
               <li>Enrollment </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h1>
                           <span>Your Refund.</span>  <span>Your Choice.</span>
                           
                        </h1>
                        
                        
                        <h3>ENROLLMENT INFORMATION</h3>
                        
                        
                        <p>Valencia encourages all students to have their refunds  deposited electronically,
                           but you are not required to enroll in order to  receive funds due to you. If you take
                           no action, you will receive refund  via a paper check.
                        </p>
                        
                        <p>Valencia has <a href="/documents/admissions/refund/Valencia-College-BBPay-Contract.pdf" target="_blank" title="View the Valencia-Blackboard Contract">partnered with Blackboard </a>in order to provide an efficient means of disbursing refunds.  Therefore, if you wish
                           to receive your refunds electronically by ACH transfer  to your existing account or
                           Money+ Card, a Money Network ® Enabled Prepaid  MasterCard, please enroll by following
                           these steps.
                        </p>
                        
                        <p><strong><u>Important Note</u></strong>:&nbsp; Check your Atlas email to make sure you have received a communication titled “Important
                           Message About Your Refund Choices”.&nbsp; If you do not have this email, contact the Business
                           Office at <a href="mailto:BusinessOffice@valenciacollege.edu">BusinessOffice@valenciacollege.edu</a> or call 407-582-1200//407-582-2387 during regular business hours BEFORE proceeding.
                           
                        </p>
                        
                        <ol>
                           
                           <li>Go to <a href="https://enroll.moneynetworkedu.com/" target="_blank">https://enroll.moneynetworkedu.com/</a>
                              
                           </li>
                           
                           <li>Click on “Enroll Now”.</li>
                           
                           <li>Read the important information, click “Next” if you want to continue with enrollment
                              process.
                           </li>
                           
                           <li>Login using your student ID number (VID), date of birth, and assigned Valencia Atlas
                              email (@mail.valenciacollege.edu).
                           </li>
                           
                           <li>Enter your security token sent to your Atlas email and click “Submit”. (Make sure
                              to check your Spam and Junk Folders.)&nbsp; <strong>The token will expire in 20 minutes</strong>.
                           </li>
                           
                           <li>
                              <u>Click on “I Want to Update” to select your electronic refund choice</u>. 
                           </li>
                           
                           <li>Enter requested information based on your refund choice.</li>
                           
                           <blockquote>
                              <u>OPTION 1: ACH  Transfer</u>
                              
                              <ul>
                                 
                                 <li>Enter your bank or financial institution  information. &nbsp; Use the 9-digit routing 
                                    number from your check, NOT deposit slip.
                                 </li>
                                 
                                 <li>Enter your account information, and confirm by re-typing.</li>
                                 
                                 <li>Read and accept Terms and Conditions.</li>
                                 
                                 <li>Confirm your ACH information.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           <blockquote>
                              <u>OPTION 2: <a href="prepaid-card.php" title="View Valencia Refund Prepaid Card Information">Money+Card, a Money Network® Enabled Prepaid MasterCard®</a></u>
                              
                              <ul>
                                 
                                 <li>Read and accept Terms and Conditions and <a href="/documents/admissions/refund/Valencia-College-Costs-MoneyCard-MasterCard.pdf" target="_blank" title="View MoneyCard Master Card Fee Schedule">Fee Schedule</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>
                                    
                                 </li>
                                 
                                 <li>Confirm or change you mailing address for card delivery.</li>
                                 
                              </ul>
                              
                           </blockquote>
                           
                           <p><strong>IMPORTANT: </strong>Mailing address changes are not updated automatically in Atlas. If you need to change
                              your address with Valencia, please go to  Atlas/Student Tab/Student Resources/Personal
                              Information/Update my address.
                           </p>
                           
                           
                           <li>Check for your Valencia College Refund Payment Choice Confirmation email sent to your
                              Atlas  email from the Money Network Support Team (<a href="mailto:Notification@moneynetworkedu.com">Notification@moneynetworkedu.com</a>). Make sure to check your Spam and Junk Folders.
                           </li>
                           
                           
                        </ol>
                        
                        <ul>
                           
                           <li>If  you have elected to receive your refunds to a Money+Card Prepaid Card, you will
                              be mailed a Welcome Packet materials within 7 to  10 business days to the address
                              selected during the enrollment process. This packet includes very important information
                              on how to activate your account, fee and transaction limits  schedule, and a comprehensive
                              How-To Guide booklet.
                           </li>
                           
                        </ul>
                        
                        
                        
                        <p><strong>Remember you can make  changes to your refund choice by going back to</strong> <a href="http://www.enroll.moneynetworkedu.com" target="_blank" title="Go To Money Network Enrollment">www.enroll.moneynetworkedu.com</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/refund/enrollment.pcf">©</a>
      </div>
   </body>
</html>