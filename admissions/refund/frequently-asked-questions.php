<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Frequently Asked Questions | Valencia Refund | Valencia College">
      <meta name="Keywords" content="college, school, educational, valencia, refund, frequently, asked, questions, faq">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/refund/frequently-asked-questions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/refund/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Refund</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/refund/">Refund</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>
                           <span>Your Refund.</span>  <span>Your Choice.</span>
                           
                        </h2>
                        
                        
                        <h3>Frequently Asked Questions</h3>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>GENERAL QUESTIONS</h3>
                        
                        
                        <div>
                           
                           
                           <h3>When can I expect a refund if I dropped my course(s) before the deadline?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia  releases funds approximately fifteen (15) working days after the last day
                                    of  the Drop/Add period. If a debt is owed  to the college, any refund will be applied
                                    to that debt, and any remaining  refund due will be payable to the student. Visit
                                    the <a href="../business-office/important-deadlines.html">Important  Deadline</a>s page for anticipated refund dates.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Do I receive a refund when I withdraw from  a course?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>No,  to receive a refund you must drop your course(s) on or before the advertised
                                    add/drop deadline. If you withdraw from a class you are still responsible for  the
                                    tuition and fees, and the charges will be reflected on your student  account. 
                                 </p>
                                 
                                 <p>Please  click <a href="../business-office/policies.html">here</a> to refund to learn more about the refund policy. 
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3>ENROLLMENT QUESTIONS IN NEW PROGRAM</h3>
                           
                           <h3>Why did I get an email informing me of the refund choice available at Valencia College?<br>I thought I already selected a refund preference.
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia  College has ended its partnership with Higher One (now BankMobile) as of
                                    July  31, 2016. In order to offer students a  way to conveniently receive refunds
                                    electronically, Valencia has contracted  with Blackboard to manage refund disbursements.
                                    This contract may be found by clicking <a href="/documents/admissions/refund/Valencia-College-BBPay-Contract.pdf">here</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>.
                                 </p>
                                 
                                 <p>Since  no financial information will be migrated from Higher One to Blackboard, you
                                    must re-enroll in the new system to receive your refunds electronically. 
                                 </p>
                                 
                                 <p>Please  refer to the REFUND PROGRAM TRANSITION section for additional information.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Am I required to enroll to receive my  refunds electronically?</h3>
                           
                           <div>
                              
                              <div>
                                 While Valencia College encourages all students to have their  refunds deposited electronically,
                                 you are NOT required to enroll in order to  receive funds due to you. &nbsp;If you take
                                 no action, you will receive refund  via a paper check. 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>When can I enroll to  receive my refunds electronically?</h3>
                           
                           <div>
                              
                              <div>
                                 You will receive an Important Message About Your Refund  Choices email sent to your
                                 assigned Valencia Atlas email on or before July 31,  2016. This email will provide
                                 further instructions on how to enroll.<br>
                                 Visit <a href="enrollment.html">valenciacollege.edu/refund/enrollment.cfm</a> to learn more.
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>What if I haven't  received my Important Message About Your Refund Choices Email?</h3>
                           
                           <div>
                              
                              <div>
                                 Please contact the Business Office at <a href="mailto:BusinessOffice@valenciacollege.edu">BusinessOffice@valenciacollege.edu</a> or call 407-582-1200//407-582-2387. You  may also visit the Business Office in person
                                 during regular business hours  (Monday - Thursday: 8AM to 6PM and Friday 8AM to 5PM).
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>How fast will I receive my refund? </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You will receive an email to either Atlas email address or designated email address
                                    (Money+ Card holders only) after the payment file has been released by Valencia. 
                                    Typically, this occurs on Friday by 2PM, but this cannot be guaranteed.  Rest assured,
                                    we are working diligently to get your refund to you as fast as possible.  
                                 </p>
                                 
                                 <p>Once you have received notification, funds will be available depending on your refund
                                    choice as follows.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Money+ Card: Same business day</li>
                                    
                                    <li>ACH Transfer to valid bank account: 3 - 5 business days</li>
                                    
                                    <li>Paper Check - 7 to 10 business days</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Why haven't I received my security token even after checking my Spam and Junk folders?</h3>
                           
                           <div>
                              
                              <div>
                                 <p>Please make sure you are checking your Atlas Email for the security token, and not
                                    your Webmail if you are a Valencia employee and a Valencia student. <br><br>
                                    <img src="/images/admissions/refund/Atlas-Email.png">
                                    
                                 </p>
                              </div>
                              
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3>REFUND PROGRAM TRANSITION</h3>   
                           
                           <h3>What if I was previously enrolled to  receive refunds on my Higher One (now BankMobile)
                              account?<br>What will happen to  my enrollment?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You  will continue to receive your refunds to your BankMobile account through July
                                    31, 2016. 
                                 </p>
                                 
                                 <p>You  have the option to <a href="enrollment.php">enroll</a> to receive refunds via an ACH transfer to your BankMobile  account. In addition,
                                    you may choose to  sign up for a Money+ Card, a Money Network® Enabled Prepaid MasterCard,
                                    enabled by Money Network. <a href="prepaid-card.php">(Learn more  about this option)</a> Please make sure you understand the fees  associated with your electronic refund
                                    choice before enrolling.
                                 </p>
                                 
                                 <p>If  you take no action, you will receive refunds due to you starting August 1, 2016
                                    via a paper check. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>If I have any questions regarding my  BankMobile (formerly Higher One) account what
                              should I do?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Contact  unique number located on the back of your card. You can also reach Customer
                                    Care at 866-309-7454. 
                                 </p>
                                 
                                 <p>Answers  to your questions may also be found at the <a href="https://higherone.custhelp.com/app/home">EasyHelp support site</a>.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>What if I was previously enrolled to  electronically receive refunds to a bank account
                              other than Higher One?<br>What  will happen to my enrollment?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You  will continue to receive your refunds to your bank account via ACH transfer 
                                    through July 31, 2016.
                                 </p>
                                 
                                 <p>You  will have re-enroll (<em>link to enrollment  page here</em>) to continue receiving refunds by ACH transfer. If you take no action, you will receive
                                    refunds due to you starting August 1, 2016 via a paper check.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/refund/frequently-asked-questions.pcf">©</a>
      </div>
   </body>
</html>