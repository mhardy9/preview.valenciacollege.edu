<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Prepaid Card  | Valencia College</title>
      <meta name="Description" content="Prepaid Card | Valencia Refund | Valencia College">
      <meta name="Keywords" content="college, school, educational, valencia, refund, prepaid, mastercard">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/refund/prepaid-card.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/refund/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Refund</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/refund/">Refund</a></li>
               <li>Prepaid Card </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h1>
                           <span>Your Refund.</span>  <span>Your Choice.</span>
                           
                        </h1>
                        
                        
                        
                        
                        <h3>Prepaid Card Information</h3> 
                        
                        
                        <p>You are not obligated to receive your refund via the Money+ Card, a prepaid debit
                           MasterCard®, enabled by Money Network®.  However, we encourage all student to receive
                           their funds electronically, and this may be an option you want to consider. 
                        </p>
                        
                        
                        
                        
                        <h4>Features of the Money+Card include:</h4>
                        
                        <ul>
                           
                           <li>Use it anywhere a debit MasterCard is accepted.</li>
                           
                           <li>Funds are protected by MasterCard's Identity Theft Protection and Zero Liability Services</li>
                           
                           <li>Free cash withdrawals at any Allpoint network  ATM location. Allpoint has 55,00 surcharge-free
                              ATMs world wide.
                           </li>
                           
                           <li>Free Money Network Checks that can be cashed at <a href="http://www.walmart.com/store/finder?queryparameter=&amp;adid=22222222224226137833&amp;wmlspartner=wmtlabs&amp;wl0=p&amp;wl1=o&amp;wl2=c&amp;wl3=%7bcreative%7d&amp;wl4=%7baceid%7d&amp;veh=sem">Walmart</a>,  so you can access every penny of your funds.
                           </li>
                           
                           <li>Money Network Mobile App.</li>
                           
                           <li>FDIC-insured AFTER Card is activated.</li>
                           
                        </ul>
                        
                        <p><strong>CERTAIN FEES AND TRANSACTION LIMITS APPLY FOR THE MONEY NETWORK SERVICE.  Please review
                              the <a href="/documents/admissions/refund/Valencia-College-Costs-MoneyCard-MasterCard.pdf">fee schedule</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i> carefully before enrolling. (Fees subject to change.)  </strong>
                           
                        </p>
                        
                        <p>If you choose to <a href="enrollment.php">enroll</a> for the Money+ Card, you will receive Welcome Packet materials with 7 to 10 business
                           days to the address selected during the enrollment process.  If you have not received
                           your Welcome Packet after 10 business days from the date you received your Valencia
                           College Refund Payment Choice Confirmation sent to your Atlas email, please contact
                           Money Network Customer service at 800-822-4283. 
                        </p>
                        
                        
                        <p>Once you receive the Card and Welcome Packet, you must activate the Card and set up
                           a PIN by calling Money Network Customer Service at 800-822-4283 before you can use
                           the card or Money Network Checks.  The PIN is needed for ATM transactions, PIN purchases,
                           and automated assistance and web account access. 
                        </p>
                        
                        <p>Welcome Packet includes:</p>
                        
                        <ul>
                           
                           <li>Money Network Terms and Conditions, including  Privacy Policy.</li>
                           
                           <li>A complete Fee and Transaction Limits Schedule.</li>
                           
                           <li>An instruction sheet on how to activate your  Prepaid Card and create a 4-digit PIN.</li>
                           
                           <li>3 Money Network Checks and instructions.</li>
                           
                           <li>Comprehensive How-To Guide booklet with  instructions and FAQs.</li>
                           
                        </ul>
                        
                        
                        <p>Once your card is activated, it is highly recommended that you create an online user
                           account at www.moneynetworkedu.com.  By creating account, you can:
                           
                        </p>
                        
                        <ul>
                           
                           <li>Set up alerts account daily balance</li>
                           
                           <li>View account balance and transaction activity</li>
                           
                           <li>View or request a Statement</li>
                           
                           <li>Find Allpoint ATMs and Check Cashing Locations</li>
                           
                        </ul>
                        
                        
                        
                        <p>You can also use the <a href="http://www.moneynetwork.com/mobile">MN Mobile App</a> to check your balance, see transaction history, located ATMs and Check Cashing  Locations,
                           and set up notifications. <br>
                           To avoid withdrawal and surcharge fees, you should use an  Allpoint network ATM and
                           always select "WITHDRAWAL from CHECKING".  To find the ATM most convenient for you
                           can:
                           
                        </p>
                        
                        <ul>
                           
                           <li>Visit <a href="http://www.allpointnetwork.com/">www.allpointnetwork.com</a>
                              
                           </li>
                           
                           <li>Call Allpoint Customer Service at 800-809-0308, option 2</li>
                           
                           <li>Download Allpoint ATM Locator app from <a href="https://itunes.apple.com/us/app/allpoint-global-surcharge/id307308511?mt=8" target="_blank">Apple iTunes</a>, <a href="https://play.google.com/store/apps/details?id=com.allpoint&amp;hl=en" target="_blank">Google Play</a>, or <a href="https://www.microsoft.com/en-us/store/apps/allpoint/9wzdncrdqtfj" target="_blank">Microsoft Store</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p>NOTE: The Central Florida Educator (CFE) Credit  Union ATMs on the East, Osceola,
                           West, and Winter Park Campuses are NOT part of  the Allpoint network. Therefore, you
                           will incur a fee if you use this out of network ATM to access funds on your  Money+
                           Card.
                        </p>
                        
                        <p>Money Network Checks can be used to are not account  specific, so you MUST obtain
                           a transaction number by contacting Money Network  Customer Service (800-822-4283)
                           to write on the face of the check before you  can present for payment. Detailed instructions
                           on how to use Money Network  Checks are included in the Welcome Package. You can also
                           speak to a customer service representative to locate free  check cashing locations.
                        </p>
                        
                        <p><strong>Remember you can make  changes to your refund choice by going back to</strong> <a href="http://www.moneynetworkedu.com">www.moneynetworkedu.com</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/refund/prepaid-card.pcf">©</a>
      </div>
   </body>
</html>