<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Refund  | Valencia College</title>
      <meta name="Description" content="Valencia Refund | Valencia College">
      <meta name="Keywords" content="college, school, educational, refund">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/refund/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/refund/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Refund</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li>Refund</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h1>
                           <span>Your Refund.</span>  <span>Your Choice.</span>
                           
                        </h1>
                        
                        
                        <h3>IMPORTANT ANNOUNCEMENT REGARDING REFUNDS</h3>
                        
                        <p>Effective August 2016, Valencia College has partnered with Blackboard to provide students
                           with electronic refund choices.The contract between Valencia College and Blackboard
                           may be reviewed <a href="/documents/admissions/refund/Valencia-College-BBPay-Contract.pdf">here</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>BlackboardPay </h3>
                        
                        <h4>2016-2017 Award Year</h4>
                        
                        <ul>
                           
                           <li>Number of Active Cards* - 
                              2,235
                              
                              <ul>
                                 
                                 <li>*Active cards are defined as any account that had a deposit transaction within the
                                    award period regardless of enrollment status.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Mean of Fees Charged to Cardholders - 
                              $11
                           </li>
                           
                           <li>Median of Fees Charged to Cardholders - 
                              $3
                           </li>
                           
                        </ul>
                        
                        <h4>Monetary Consideration</h4>
                        
                        <ul>
                           
                           <li>Institution Paid Blackboard - 
                              $40,685
                           </li>
                           
                           <li>Blackboard Paid Institution - 
                              $0
                           </li>
                           
                        </ul>
                        
                        <h4>Non-Monetary Consideration</h4>
                        
                        <ul>
                           
                           <li>Institution Paid Blackboard - 
                              $0
                           </li>
                           
                           <li>Blackboard Paid Institution - 
                              $0
                           </li>
                           
                        </ul>
                        
                        
                        <p><strong>ALL STUDENTS</strong> will need to select a refund preference for any funds due from Valencia, including
                           financial aid, to be deposited electronically.
                           
                           
                           Additional information will be sent to your Atlas email.
                        </p>
                        
                        <p><strong>Valencia College encourages all students to have their refunds deposited electronically,
                              but you are not required to enroll in the Money+ Card program to receive your financial
                              aid refund. If you take no action, you will receive refund via a paper check in the
                              mail.</strong></p>
                        
                        
                        <hr class="styled">
                        
                        
                        <h3>Refund Options</h3> 
                        
                        <h4>Electronic Deposit Options (Enrollment Required)</h4>
                        <strong>ACH transfer to your existing bank account</strong>
                        
                        <p>Refunds are sent by ACH transfer processed four times per business day after the payment
                           file is released by Valencia.  It will take between 3 to 5 business days for funds
                           to be available in your account.  Please note: You will be responsible for entering
                           your banking information and keeping it up to date.
                        </p>
                        
                        <strong><img src="/images/admissions/refund/Valencia_Bb_PayCard_Flat-Charli-Atlas.png" height="" width="" align="right">Money + Card a Money Network® Enabled Prepaid MasterCard</strong>
                        
                        <p>Refunds sent to Money Network are processed on a continual basis throughout the business
                           day after the payment file is released by Valencia.  Funds will be placed on your
                           prepaid debit card sometime during the business day.  Please familiarize yourself
                           with the fees associated with this Prepaid Card by clicking on the  <a href="/documents/admissions/refund/Valencia-College-Costs-MoneyCard-MasterCard.pdf" target="_blank">fee schedule</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>.<br>
                           
                           
                        </p>
                        
                        <h3>Other Deposit Option (No Action Required)</h3>
                        <strong>Paper Check</strong>
                        
                        <p>Checks are sent by first class US Mail to your home address which should arrive within
                           7 to 10 business days.    Please note: You will be responsible for keeping your home
                           address up to date in Atlas. <br>
                           <br></p>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Important Message for Higher One Account Holders</h3>
                        
                        <p>Effective May 4, 2016 Higher One ATMs will be taken out of service at all Valencia
                           campus locations. However, you will be able to access your funds surcharge-free at
                           any Allpoint network ATM location using your Valencia debit card. To find the ATM
                           most convenient for you:
                        </p>
                        
                        <ul>
                           
                           <li>Visit <a href="http://www.allpointnetwork.com">www.allpointnetwork.com</a>
                              
                           </li>
                           
                           <li> Call 800-809-0308, option 2</li>
                           
                           <li> Download Allpoint ATM Locator app from Apple iTunes, Google Play, or Microsoft Store
                              
                           </li>
                           
                        </ul>
                        
                        <p>The Central Florida Educator (CFE) Credit Union ATMs on the East, Osceola, West, and
                           Winter Park Campuses will remain operational, but they are NOT part of the Allpoint
                           network. Therefore, you will incur a fee if you use this out of network ATM to access
                           your Higher One account with your Valencia debit card. 
                        </p>
                        
                        <p>If you have any additional questions, please Higher One Customer care directly at
                           877-327-9515 or login to your account using EasyHelp
                        </p>
                        
                        
                        
                        <div>
                           <strong>Valencia Debit Card account holders:</strong> <br>
                           Please review the Program Transition section under the <a href="faq.php">Refund Frequently Asked Questions</a> page to learn more about how you might be affected. 
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/refund/index.pcf">©</a>
      </div>
   </body>
</html>