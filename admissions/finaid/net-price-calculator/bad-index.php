<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Net Price Calculator | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/net-price-calculator/bad-index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/net-price-calculator/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/net-price-calculator/">Net Price Calculator</a></li>
               <li>Net Price Calculator</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <center>
                  
                  <div id="divInstitutionBanner" style="display:none;">
                     <img id="imgInstitutionBanner" src="http://valenciacollege.edu/finaid/net-price-calculator/images/bannerplace.gif" alt="Institution banner" title="Institution banner" style="border:0px;">
                     
                  </div>
                  
                  <div>
                     <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/border_top.png" alt="Net Price Calculator" title="Net Price Calculator" style="_margin:-3px;*margin:-3px;">
                     
                  </div>
                  
                  <table class="table ">
                     
                     <tr>
                        
                        <td width="12px" style="background: url('http://valenciacollege.edu/finaid/net-price-calculator/images/border_left.png') repeat-y;"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/blank.gif" alt="" title=""></td>
                        
                        <td align="left" valign="top" style="background-color:#fffef5;padding-top:17px;padding-bottom:20px;">
                           
                           
                           <div id="divInstitutionNameWindow" style="position: absolute; display:none; width:436px;">
                              
                              <div style="position:relative; top:120px; left:54px; ">
                                 <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/instnm_popup_top.png" style="_margin-bottom:-3px;*margin-bottom:-3px;" alt="">
                                 
                                 <div style="background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/instnm_popup_middle.png'); background-repeat:repeat-y; ">
                                    
                                    <div style="padding:0px 10px 0px 35px;">
                                       
                                       <div style="font-weight:bold; font-size:13px; color:#6699cc; font-family:Arial; text-align:left;width:350px;">Welcome to Valencia College's Net Price Calculator!</div>
                                       
                                       <div style="width:370px; height:170px; overflow:auto; margin-top:12px;font-weight:normal; font-size:13px; color:#333333; font-family:Arial; text-align:left;">Welcome to Valencia College's net price calculator. Begin by reading and agreeing
                                          to the statement below. Then follow the instructions on the subsequent screens to
                                          receive an estimate of how much students similar to you paid to attend Valencia College
                                          in 2015-16.
                                       </div>
                                       
                                       <div style="margin-top:10px; margin-left:auto; margin-right:auto; width:50px; ">
                                          <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/button-ok.gif" alt="Ok" title="Ok" style="cursor:pointer;" onclick="closeInstitutionNameWindow()">
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/instnm_popup_bottom.png" alt="">
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <table class="table ">
                              
                              <tr>
                                 
                                 <td valign="top" align="left" style="padding-left:10px; padding-bottom:10px;">
                                    
                                    
                                    <div id="dv_npc_s1_t" style="display:none; margin-bottom:13px;">
                                       
                                       <p>
                                          <strong>Step <span id="s_step1">1</span>:</strong> Please provide the requested information. Your responses will be used to
                                          calculate an estimated amount that students like you paid - after grant aid and scholarships
                                          but before
                                          student loans - to attend this institution in a given year.
                                          
                                       </p>
                                       
                                    </div>
                                    
                                    
                                    <div id="dv_npc_s2_t" style="display:none; margin-bottom:13px;">
                                       <strong>Step <span id="s_step2"></span>:</strong> Please provide the following information and then click Continue.
                                       
                                    </div>
                                    
                                    
                                    <div id="dv_npc_s3_t" style="display:none; margin-bottom:13px;">
                                       <strong>Step <span id="s_step3"></span>:</strong> For the purposes of this calculator, an independent student is
                                       one who is at least 24 years old, married, and/or has legal dependents other than
                                       a spouse (e.g., children).
                                       A student who does not meet any of the above criteria is considered dependent.
                                       <br><br>
                                       Based on the information you provided in previous steps, your
                                       dependency status is estimated to be <strong>Dependent</strong>.  Please provide the following information and then click Continue.
                                       <br>
                                       
                                    </div>
                                    
                                    
                                    <div id="dv_npc_s4_t" style="display:none; margin-bottom:13px;">
                                       <strong>Step <span id="s_step4"></span>:</strong> For the purposes of this calculator, an independent student is
                                       one who is at least 24 years old, married, and/or has legal dependents other than
                                       a spouse (e.g., children).
                                       A student who does not meet any of the above criteria is considered dependent.
                                       <br><br> Based on the information you provided in previous steps, your
                                       dependency status is estimated to be <strong>Independent</strong>.  Please provide the following information and then click Continue.
                                       
                                    </div>
                                    
                                    
                                    <div id="dv_npc_s5_t" style="display:none; margin-bottom:13px;">
                                       <span id="s_step5" style="display:none"></span>Review the information you have provided. You can click Modify to return to Step 1
                                       and edit this information,
                                       or if you are happy with the current selections, click Continue to receive your <strong>estimated</strong> net price.
                                       
                                    </div>
                                    
                                    
                                    <div id="dv_npc_s6_t" style="display:none; margin-bottom:13px;">
                                       <span id="s_step6" style="display:none;"></span>
                                       
                                       <div id="dv_npc_s6_academic" style="">Based on the information you have provided, the following calculations represent the
                                          average net price of attendance that students similar to you paid in the given year:
                                       </div>
                                       
                                       <div id="dv_npc_s6_program" style="display:none;">Based on the information you have provided, the following calculations represent the
                                          average net price of attendance that students similar to you paid in the given year.
                                          This information applies only to the <span style="font-weight:bold; color:#cc6600;">##LARGESTPROGRAM##</span>&nbsp;program at the institution, which typically takes an average of <span style="font-weight:bold; color:#cc6600;">##NUMBEROFMONTHS##</span>&nbsp;months for a full-time student to complete. Prices may vary depending on the program
                                          of interest and its expected duration.
                                       </div>
                                       
                                    </div>
                                    
                                    <div style="">
                                       
                                       <table class="table ">
                                          
                                          <tr>
                                             
                                             <td align="left" valign="top" width="10px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_top_left.gif" title="" alt=""></td>
                                             
                                             <td align="left" valign="top" style="background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_top_center.gif');"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/blank.gif" alt=""></td>
                                             
                                             <td align="left" valign="top" width="10px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_top_right.gif" title="" alt=""></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td align="left" valign="top" style="background:url('http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_middle_left.gif') repeat-y;"></td>
                                             
                                             <td align="left" valign="top" style="padding:6px 10px 10px 8px; background-color:#dfecf1;">
                                                
                                                
                                                <div id="dv_npc_s0" style="line-height:17px;">
                                                   <strong>Please read.</strong>
                                                   This calculator is intended to provide <i>estimated</i> net price information (defined as estimated cost of attendance — including tuition
                                                   and required fees, books and supplies, room and board (meals), and other related expenses
                                                   — minus estimated grant and scholarship aid) to current and prospective students and
                                                   their families based on what similar students paid in a previous year.
                                                   <br><br>
                                                   By clicking below, I acknowledge that the estimate provided using this calculator
                                                   does not represent
                                                   a final determination, or actual award, of financial assistance, or a final net price;
                                                   it is an
                                                   estimate based on cost of attendance and financial aid provided to students in a previous
                                                   year.
                                                   Cost of attendance and financial aid availability change year to year. The estimates
                                                   shall
                                                   not be binding on the Secretary of Education, the institution of higher education,
                                                   or the State.
                                                   <br><br>
                                                   Students must complete the Free Application for Federal Student Aid (FAFSA) in order
                                                   to be eligible for,
                                                   and receive, an actual financial aid award that includes Federal grant, loan, or work-study
                                                   assistance.
                                                   For more information on applying for Federal student aid, go to <a href="http://www.fafsa.ed.gov/" target="_blank">http://www.fafsa.ed.gov/</a>
                                                   
                                                   <div style="font-size:11px; margin-top:25px; margin-bottom:10px;line-height:14px;">
                                                      <strong>Note:</strong> Any information that you provide on this site is confidential. The Net Price Calculator
                                                      does not store your responses or ask for personal identifying information of any kind.
                                                   </div>
                                                   
                                                   <div style="text-align:center; margin-top:30px;">
                                                      <a href="JavaScript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/button-iagree.gif" alt="I Agree" title="I Agree" style="border-width:0px;"></a>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <div id="dv_npc_s1" style="display:none;">
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Financial aid:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">Do you plan to apply for financial aid?</span><br>
                                                            <span id="s_fa_0"><input type="radio" name="rb_financialaid" value="0" title="Yes, I plan to apply for financial aid.">Yes</span>&nbsp;&nbsp;
                                                            <span id="s_fa_1"><input type="radio" name="rb_financialaid" value="1" title="No, I do not plan to apply for financial aid.">No</span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="middle" width="140px"><span class="title1">Age:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">How old are you?</span> <input id="txt_age" type="text" value="" size="6" maxlength="4" title="How old are you?">
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr id="tr_ls">
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Living arrangement:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">Where do you plan to live while attending this institution?</span><br>
                                                            <span id="s_ls_0"><input type="radio" name="rb_livingstatus" value="##rb_livingstatus_0##" title="On-campus (in a residence hall, dormitory, or on-campus apartment)">On-campus (in a residence hall, dormitory, or on-campus apartment)<br></span>
                                                            <span id="s_ls_1"><input type="radio" name="rb_livingstatus" value="0" title="Living on my own or with a roommate">Living on my own or with a roommate<br></span>
                                                            <span id="s_ls_2"><input type="radio" name="rb_livingstatus" value="1" title="Living with my parents or other family members">Living with my parents or other family members<br></span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr id="tr_rs">
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Residency:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span id="s_rs_0"><input type="radio" name="rb_residencystatus" value="##rb_residencystatus_0##" title="Eligible for in-district tuition">Eligible for in-district tuition<br></span>
                                                            <span id="s_rs_1"><input type="radio" name="rb_residencystatus" value="0" title="Eligible for in-state tuition">Eligible for in-state tuition<br></span>
                                                            <span id="s_rs_2"><input type="radio" name="rb_residencystatus" value="1" title="Eligible for out-of-state tuition">Eligible for out-of-state tuition<br></span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="font-weight:bold;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-bottom:0px; padding-top:10px;">
                                                            <a href="javascript:GoPrevious()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_previous.gif" border="0" alt="Previous" title="Previous"></a>
                                                            &nbsp;&nbsp;
                                                            <a href="javascript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_continue.gif" border="0" alt="Continue" title="Continue"></a>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <div id="dv_npc_s2" style="display:none;">
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Marital Status:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">Are you (the student) married?</span><br>
                                                            <span style="font-size:10px;">(Answer "yes" if you are separated but not divorced.)</span><br>
                                                            <span><input type="radio" name="rb_maritalstatus" value="1" title="Yes, I am married or separated but not divorced.">Yes<br>
                                                               <input type="radio" name="rb_maritalstatus" value="0" title="No, I am not married.">No</span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Children:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">Are you (the student) the primary source of financial support for any children?</span><br>
                                                            <span>
                                                               <input type="radio" name="rb_numberofchildren" value="1" title="Yes">Yes<br>
                                                               <input type="radio" name="rb_numberofchildren" value="0" title="No">No<br>
                                                               </span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="font-weight:bold;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-bottom:0px; padding-top:10px;">
                                                            <a href="javascript:GoPrevious()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_previous.gif" border="0" alt="Previous" title="Previous"></a>
                                                            &nbsp;&nbsp;
                                                            <a href="javascript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_continue.gif" border="0" alt="Continue" title="Continue"></a>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <div id="dv_npc_s3" style="display:none;">
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="padding-bottom:0px;"><span class="title1">Number in Family:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-right:6px;padding-bottom:0px;">
                                                            <span class="title2">How many people are in your family's household?</span><br>
                                                            <span style="font-size:10px;line-height:14px;">(Count yourself, your parent(s), and your parents' other dependent children.)</span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="middle" width="140px" style="font-weight:bold;padding-bottom:0px;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="middle" style="padding-top:3px;padding-left:6px;padding-bottom:15px;">
                                                            <input type="radio" name="rb_numinfamily_dep" id="rb_numinfamily_dep2" value="Two|2"><label for="rb_numinfamily_dep2">Two</label><br>
                                                            <input type="radio" name="rb_numinfamily_dep" id="rb_numinfamily_dep3" value="Three|3"><label for="rb_numinfamily_dep3">Three</label><br>
                                                            <input type="radio" name="rb_numinfamily_dep" id="rb_numinfamily_dep4" value="Four|4"><label for="rb_numinfamily_dep4">Four</label><br>
                                                            <input type="radio" name="rb_numinfamily_dep" id="rb_numinfamily_dep5" value="Five|5"><label for="rb_numinfamily_dep5">Five</label><br>
                                                            <input type="radio" name="rb_numinfamily_dep" id="rb_numinfamily_dep6" value="Six or more|6"><label for="rb_numinfamily_dep6">Six or more</label>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="padding-bottom:0px;"><span class="title1">Number in College:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-right:6px;padding-bottom:0px;">
                                                            <span class="title2">Of the number in your family above, how many will be in college next year?</span><br>
                                                            <span style="font-size:10px;line-height:14px;">(Count yourself and your siblings; do not count your parents.)</span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="middle" width="140px" style="font-weight:bold;padding-bottom:0px;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="middle" style="padding-top:3px;padding-left:6px;padding-bottom:15px;">
                                                            <input type="radio" name="rb_numincollege_dep" id="rb_numincollege_dep1" value="One child|1"><label for="rb_numincollege_dep1">One child</label><br>
                                                            <input type="radio" name="rb_numincollege_dep" id="rb_numincollege_dep2" value="Two children|2"><label for="rb_numincollege_dep2">Two children</label><br>
                                                            <input type="radio" name="rb_numincollege_dep" id="rb_numincollege_dep3" value="Three or more children|3"><label for="rb_numincollege_dep3">Three or more children</label><br>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Household Income:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">What is your annual household income after taxes?</span><br>
                                                            
                                                            <ul style="font-size:10px;margin:0px 0px 0px 20px;padding:5px;line-height:14px;">
                                                               
                                                               <li>Include income earned by yourself and your parent(s).</li>
                                                               
                                                               <li>Include income from work, child support, and other sources.</li>
                                                               
                                                               <li>If your parent is single, separated, or divorced, include the income for the parent
                                                                  with whom you live.
                                                               </li>
                                                               
                                                               <li>If the parent with whom you live is remarried, include both your parent's income and
                                                                  his/her spouse's income.
                                                               </li>
                                                               
                                                            </ul>
                                                            <input type="radio" name="rb_householdincome_dep" value="0" title="Less than $30,000">Less than $30,000<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="1" title="Between $30,000 and $39,999">Between $30,000 - $39,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="2" title="Between $40,000 and $49,999">Between $40,000 - $49,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="3" title="Between $50,000 and $59,999">Between $50,000 - $59,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="4" title="Between $60,000 and $69,999">Between $60,000 - $69,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="5" title="Between $70,000 and $79,999">Between $70,000 - $79,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="6" title="Between $80,000 and $89,999">Between $80,000 - $89,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="7" title="Between $90,000 and $99,999">Between $90,000 - $99,999<br>
                                                            <input type="radio" name="rb_householdincome_dep" value="8" title="Between Above $99,999">Above $99,999<br>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="font-weight:bold;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-bottom:0px; padding-top:10px;">
                                                            <a href="javascript:GoPrevious()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_previous.gif" border="0" alt="Previous" title="Previous"></a>
                                                            &nbsp;&nbsp;
                                                            <a href="javascript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_continue.gif" border="0" alt="Continue" title="Continue"></a>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <div id="dv_npc_s4" style="display:none;">
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="padding-bottom:0px;"><span class="title1">Number in Family:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-right:6px;padding-left:6px;padding-bottom:0px;">
                                                            <span class="title2">How many people are in your family's household?</span><br>
                                                            <span id="spanNumInFamilyHint" style="display:none;font-size:10px;line-height:14px;">Count yourself and your spouse (if applicable). </span>
                                                            <span id="spanNumInFamilyHintWithChildren" style="display:none;font-size:10px;line-height:14px;">Count yourself, your spouse (if applicable), and any dependent children.</span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="middle" width="140px" style="font-weight:bold;padding-bottom:0px;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="middle" style="padding-top:3px;padding-left:6px;padding-bottom:15px;">
                                                            
                                                            <div id="divFirstOptionForNumInFamilyWithChildren" style="display:none;">
                                                               <input type="radio" name="rb_indnuminfamily" value="One|1" title="One">One<br>
                                                               
                                                            </div>
                                                            <input type="radio" name="rb_indnuminfamily" value="Two|2" title="Two">Two<br>
                                                            
                                                            <div id="divNumInFamilyWithChildren" style="display:none;">
                                                               <input type="radio" name="rb_indnuminfamily" value="Three|3" title="Three">Three<br>
                                                               <input type="radio" name="rb_indnuminfamily" value="Four|4" title="Four">Four<br>
                                                               <input type="radio" name="rb_indnuminfamily" value="Five|5" title="Five">Five<br>
                                                               <input type="radio" name="rb_indnuminfamily" value="Six or more|6" title="Six or more">Six or more<br>
                                                               
                                                            </div>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="padding-bottom:0px;"><span class="title1">Number in College:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-right:6px;padding-left:6px;padding-bottom:0px;">
                                                            <span class="title2">Of the number in your family above, how many will be in college next year?</span><br>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="middle" width="140px" style="font-weight:bold;padding-bottom:0px;">&nbsp;</td>
                                                         
                                                         <td colspan="2" align="left" valign="middle" style="padding-top:3px;padding-left:6px;padding-right:6px;padding-bottom:15px;">
                                                            <input type="radio" name="rb_indnumincollege" value="One|1" title="One">One<br>
                                                            <span id="spanIndNumInCollegeTwoOrMore" style="display:none;"><input type="radio" name="rb_indnumincollege" value="Two or more|2" title="Two or more">Two or more<br></span>
                                                            <span id="spanIndNumInCollegeTwo" style="display:none;"><input type="radio" name="rb_indnumincollege" value="Two|2" title="Two">Two<br></span>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px"><span class="title1">Household Income:</span></td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;">
                                                            <span class="title2">What is your annual household income after taxes?</span><br>
                                                            <span style="font-size:10px;line-height:14px;">(Include income from work, child support, and other sources; if you are married, include
                                                               your spouse's income.)</span><br><br>
                                                            <input type="radio" name="rb_householdincome_ind" value="0" title="Less than $30,000">Less than $30,000<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="1" title="Between $30,000 and $39,999">Between $30,000 - $39,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="2" title="Between $40,000 and $49,999">Between $40,000 - $49,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="3" title="Between $50,000 and $59,999">Between $50,000 - $59,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="4" title="Between $60,000 and $69,999">Between $60,000 - $69,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="5" title="Between $70,000 and $79,999">Between $70,000 - $79,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="6" title="Between $80,000 and $89,999">Between $80,000 - $89,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="7" title="Between $90,000 and $99,999">Between $90,000 - $99,999<br>
                                                            <input type="radio" name="rb_householdincome_ind" value="8" title="Above $99,999">Above $99,999<br>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="140px" style="font-weight:bold;">&nbsp;</td>
                                                         
                                                         <td align="left" valign="top" style="padding-left:6px;padding-bottom:0px; padding-top:10px;">
                                                            <a href="javascript:GoPrevious()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_previous.gif" border="0" alt="Previous" title="Previous"></a>
                                                            &nbsp;&nbsp;
                                                            <a href="javascript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_continue.gif" border="0" alt="Continue" title="Continue"></a>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                                
                                                
                                                <div id="dv_npc_s5" style="display:none;">
                                                   
                                                   <div id="dv_summary" class="summarytable" style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"></div>
                                                   
                                                   <div style="text-align:center;padding-bottom:0px; padding-top:20px;">
                                                      <a href="javascript:ClearVars();GoTo('1');"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_modify.gif" border="0" alt="Modify" title="Modify"></a>
                                                      &nbsp;&nbsp;
                                                      <a href="javascript:GoNext()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_continue.gif" border="0" alt="Continue" title="Continue"></a>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                
                                                
                                                
                                                <div id="dv_npc_s6" style="display:none; margin-left:50px; margin-top:10px;">
                                                   
                                                   <table class="table ">
                                                      
                                                      <tr>
                                                         
                                                         <td><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/notepad_top.jpg" alt=""></td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td>
                                                            
                                                            <div style="background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/notepad_middle.jpg'); background-repeat:repeat-y;width:387px;">
                                                               
                                                               <div style="padding:0px 10px 10px 25px; font-size:13px;">
                                                                  
                                                                  <table class="table formtable">
                                                                     
                                                                     <tr>
                                                                        
                                                                        <td colspan="2" align="left" valign="top" style="padding-bottom:3px;">
                                                                           &nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight:bold;color:#cc6600; font-size:14px;">Academic Year: 2015-16</span>
                                                                           
                                                                           <div><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/divider_academicyear.gif" alt=""></div>
                                                                           
                                                                        </td>
                                                                        
                                                                     </tr>
                                                                     
                                                                     <tr>
                                                                        
                                                                        <td align="left" valign="middle" width="220px" style="padding-top:7px;padding-left:20px; padding-bottom:3px;">Estimated tuition and fees</td>
                                                                        
                                                                        <td align="left" valign="top" style="width:95px;padding-top:7px;padding-left:2px; padding-bottom:3px;"><span id="s_etf" style="">$00,000</span></td>
                                                                        
                                                                     </tr>
                                                                     
                                                                     <tr>
                                                                        
                                                                        <td align="left" valign="middle" width="220px" style="padding:3px;padding-left:5px;">
                                                                           <span style="font-weight:bold; font-size:13px; font-family:Verdana;">+</span> Estimated room and board charges
                                                                           
                                                                           <div style="font-size:10px;font-weight:normal;color:#666666;padding-left:17px;">(Includes rooming accommodations and meals)</div>
                                                                           
                                                                        </td>
                                                                        
                                                                        <td align="left" valign="top" style="padding:3px;padding-left:2px;"><span id="s_erb" style="">$00,000</span></td>
                                                                        
                                                                     </tr>
                                                                     
                                                                     <tr>
                                                                        
                                                                        <td align="left" valign="middle" width="220px" style="padding:3px;padding-left:5px;">
                                                                           <span style="font-weight:bold; font-size:13px; font-family:Verdana;">+</span> Estimated cost of books and &nbsp; &nbsp;&nbsp;supplies
                                                                        </td>
                                                                        
                                                                        <td align="left" valign="top" style="padding:3px;padding-left:2px;"><span id="s_ebs" style="">$00,000</span></td>
                                                                        
                                                                     </tr>
                                                                     
                                                                     <tr>
                                                                        
                                                                        <td align="left" valign="middle" width="220px" style="padding:3px;padding-left:5px;">
                                                                           <span style="font-weight:bold; font-size:13px; font-family:Verdana;">+</span> Estimated other expenses<br>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:10px;font-weight:normal;color:#666666;">(Personal expenses, transportation, etc.)</span>
                                                                           
                                                                        </td>
                                                                        
                                                                        <td align="left" valign="top" style="padding:3px;padding-left:2px;"><span id="s_eo" style="">$00,000</span></td>
                                                                        
                                                                     </tr>
                                                                     
                                                                  </table>
                                                                  
                                                                  
                                                                  <div style="margin-top:10px; margin-bottom:8px; _margin-top:-2px; _margin-bottom:2px;">
                                                                     <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/divider_black.gif" alt="">
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                                  
                                                                  
                                                                  <div style="width:329px; height:97px; background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/output_bg-blue.jpg'); background-repeat:no-repeat; ">
                                                                     
                                                                     <table class="table formtable">
                                                                        
                                                                        <tr>
                                                                           
                                                                           <td align="left" valign="middle" width="228px" style="padding-bottom:7px;padding-left:19px;">Estimated total cost of attendance:</td>
                                                                           
                                                                           <td align="left" valign="top" style="padding-bottom:7px;padding-left:2px;"><span id="s_etpoa">$00,000</span></td>
                                                                           
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                           
                                                                           <td align="left" valign="middle" width="228px" style="padding-top:2px; padding-left:8px; padding-bottom:0px;">
                                                                              <span style="font-weight:bold; font-size:14px; font-family:Verdana;">-</span>
                                                                              <strong>Estimated total grant aid:</strong>
                                                                              
                                                                              <div style="font-size:10px;font-weight:normal;color:#666666; margin-left:11px; margin-top:3px; line-height:12px;">(Includes both merit and need based grant and scholarship aid from Federal, State,
                                                                                 or Local Governments, or the Institution)
                                                                              </div>
                                                                              
                                                                           </td>
                                                                           
                                                                           <td align="left" valign="top" style="padding-top:2px;padding-left:2px; padding-bottom:0px;"><span id="s_etga" style="font-weight:bold;">$00,000</span></td>
                                                                           
                                                                        </tr>
                                                                        
                                                                     </table>
                                                                     
                                                                  </div>
                                                                  
                                                                  
                                                                  <div style="margin-top:10px; margin-bottom:8px; _margin-top:-2px; _margin-bottom:2px;">
                                                                     <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/divider_black.gif" alt="">
                                                                     
                                                                  </div>
                                                                  
                                                                  
                                                                  <div style="width:329px; height:50px; background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/output_bg-orange.jpg'); background-repeat:no-repeat; color:#ffffff; font-weight:bold; ">
                                                                     
                                                                     <table class="table formtable">
                                                                        
                                                                        
                                                                        <tr>
                                                                           
                                                                           <td align=" left" valign="middle" width="228px" style="padding-top:9px;padding-left:19px;font-size:13px;color:#ffffff;">Estimated Net Price After Grants and Scholarships:</td>
                                                                           
                                                                           <td align="left" valign="middle" style="padding-top:9px;padding-left:2px;font-size:13px;color:#ffffff;"><span id="s_enp">$00,000</span></td>
                                                                           
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                           
                                                                           <td colspan="2" align="left" valign="top" width="285px">
                                                                              <span id="s_step6_body"></span>
                                                                              
                                                                           </td>
                                                                           
                                                                        </tr>
                                                                        
                                                                     </table>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div id="tr_requiredliveoncampus" style="display:none; margin-right:20px;font-size:11px; line-height:12px; color:#cc6600; margin-top:10px;">
                                                                     This institution requires that full-time, first-time students live on-campus or in
                                                                     institutionally controlled housing.
                                                                     
                                                                  </div>
                                                                  
                                                                  <div style="margin-right:20px;font-size:11px; line-height:12px; margin-top:10px;">
                                                                     Grants and scholarships do not have to be repaid. Some students also qualify for student
                                                                     loans to assist in paying this net price; however, student loans do have to be repaid.
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                      <tr>
                                                         
                                                         <td><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/notepad_bottom.jpg" alt=""></td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                                
                                                
                                                
                                                <div id="dv_npc_s6_r" class="disclaimer" style="display:none;">
                                                   
                                                   <table class="table formtable">
                                                      
                                                      <tr>
                                                         
                                                         <td colspan="2" align="left" valign="top" style="">
                                                            
                                                            <table class="table ">
                                                               
                                                               <tr>
                                                                  
                                                                  <td align="left" valign="top" width="140px" style="font-weight:bold;">&nbsp;</td>
                                                                  
                                                                  <td align="left" valign="top" style="padding-left:6px;padding-bottom:0px; padding-top:10px;">
                                                                     <a href="javascript:GoPrevious()"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_previous.gif" border="0" alt="Previous" title="Previous"></a>
                                                                     &nbsp;&nbsp;
                                                                     <a href="javascript:StartOver();"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/icon_startover.gif" border="0" alt="Start Over" title="Start Over"></a>
                                                                     
                                                                  </td>
                                                                  
                                                               </tr>
                                                               
                                                            </table>
                                                            
                                                         </td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                   
                                                   
                                                   <table class="table ">
                                                      
                                                      <tr>
                                                         
                                                         <td align="left" valign="top" width="5px">&nbsp;</td>
                                                         
                                                         <td align="left" valign="top" style="padding:1px;">
                                                            <strong>Please Note:</strong> The estimates above apply to <strong>full-time, first-time degree/certificate-seeking undergraduate students</strong> only.&nbsp;
                                                            
                                                            <br><br>
                                                            These estimates do not represent a final determination, or actual award, of financial
                                                            assistance or a final net
                                                            price; they are only estimates based on cost of attendance and financial aid provided
                                                            to students in 2015-16.
                                                            Cost of attendance and financial aid availability change year to year. These estimates
                                                            shall not be binding on
                                                            the Secretary of Education, the institution of higher education, or the State.<br><br>
                                                            
                                                            Not all students receive financial aid. In 2015-16, 76% of our full-time students
                                                            enrolling
                                                            for college for the first time received grant/scholarship aid. Students may also be
                                                            eligible
                                                            for student loans and work-study. Students must complete the Free Application for
                                                            Federal Student
                                                            Aid (FAFSA) in order to determine their eligibility for Federal financial aid that
                                                            includes
                                                            Federal grant, loan, or work-study assistance. For more information on applying for
                                                            Federal student aid, go to <a href="http://www.fafsa.ed.gov/" target="_blank">
                                                               http://www.fafsa.ed.gov/
                                                               </a>.&nbsp; 
                                                            <br><br>
                                                            
                                                            <br>
                                                            
                                                         </td>
                                                         
                                                         <td align="left" valign="top" width="5px">&nbsp;</td>
                                                         
                                                      </tr>
                                                      
                                                   </table>
                                                   
                                                </div>
                                                
                                             </td>
                                             
                                             <td align="left" valign="top" style="background:url('http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_middle_right.gif') repeat-y;"></td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td align="left" valign="top" width="10px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_bottom_left.gif" title="" alt=""></td>
                                             
                                             <td align="left" valign="top" style="background-image:url('http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_bottom_center.gif');"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/blank.gif" alt=""></td>
                                             
                                             <td align="left" valign="top" width="10px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/npcb_bottom_right.gif" title="" alt=""></td>
                                             
                                          </tr>
                                          
                                       </table>
                                       
                                    </div>
                                    
                                 </td>
                                 
                                 <td valign="top" align="right" width="200px">
                                    <img src="http://valenciacollege.edu/finaid/net-price-calculator/images/bg-r.jpg" alt="" title="">
                                    
                                    <div style="margin:35px 0px 0px 10px; text-align:left;">
                                       
                                       <noscript>
                                          <img id="imgJavaScriptNote" src="http://valenciacollege.edu/finaid/net-price-calculator/images/javascript_note.gif" alt="You must have JavaScript enabled in your browser to use this site." title="You must have JavaScript enabled in your browser to use this site.">
                                          
                                       </noscript>
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </table>
                           
                        </td>
                        
                        <td width="12px" style="background: url('http://valenciacollege.edu/finaid/net-price-calculator/images/border_right.png') repeat-y;"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/blank.gif" alt="" title=""></td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td width="12px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/border_bottom_left.png" alt="" title=""></td>
                        
                        <td style="background:url('http://valenciacollege.edu/finaid/net-price-calculator/images/border_bottom_center.png') repeat-x;"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/blank.gif" alt="" title=""></td>
                        
                        <td width="12px"><img src="http://valenciacollege.edu/finaid/net-price-calculator/images/border_bottom_right.png" alt="" title=""></td>
                        
                     </tr>
                     
                  </table>
                  
               </center>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/net-price-calculator/bad-index.pcf">©</a>
      </div>
   </body>
</html>