<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Scholarship Bulletin Board  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/scholarship-bulletin-board.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Scholarship Bulletin Board </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Scholarship Bulletin Board</h2>
                        
                        <h3>Quick Links</h3>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="#CurrentOutside">Private Scholarships (Non-Valencia Foundation Donors) </a><strong>If you are a private/outside scholarship donor and would like to have your scholarship
                                 offer shared with Valencia College students please email your information to <a href="mailto:sambridge@valenciacollege.edu">Susan Ambridge. </a></strong></li>
                           
                           <li><a href="#FoundationList">Valencia Foundation Scholarship List</a></li>
                           
                           <li><a href="#Alumni">Valencia Alumni Association Scholarship and Awards Programs</a></li>
                           
                           <li><a href="#ScholarshipSearch">Free Scholarship Search Engines</a></li>
                           
                        </ul>
                        
                        <h3>Valencia Foundation Scholarship Applications</h3>
                        
                        <p>The 2017-2018 Valencia Foundation Scholarship application for the current SPRING TERM
                           and the 2018-2019 Valencia Foundation Scholarship application for FALL TERM are now
                           open. Students who are currently attending and will also attend in the fall term should
                           complete BOTH applications!
                        </p>
                        
                        <p>Apply today:</p>
                        
                        <p><a class="button btn-default" href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a></p>
                        
                        <h3>Helpful Hints on Applying for a Valencia Foundation Scholarship</h3>
                        
                        <p><strong>Students must only use their Valencia Student ID number as their username and their
                              Atlas email address on the application.</strong></p>
                        
                        <p>Most scholarships offered through the Valencia Foundation require a student to demonstrate
                           need by completing the <a href="http://www.fafsa.ed.gov/">Free Application for Federal Student Aid</a> (FAFSA). Don’t let the fact that you may not qualify for financial aid stop you from
                           applying for a FAFSA or the chance to be matched to a Valencia Foundation scholarship
                           donor. We use the FAFSA to verify financial need and not all scholarship decisions
                           are based on the FAFSA.
                        </p>
                        
                        <p>Recommendations and a high GPA are very helpful, but not all scholarships require
                           them. Please complete a 2017-2018 Valencia Foundation Scholarship Application for
                           this spring and a 2018-2019 for next fall so we can see if you are eligible for any
                           of the hundreds of scholarships still available.
                        </p>
                        
                        <p>For questions on completing your Valencia Foundation scholarship application, please
                           contact Susan Ambridge <a href="mailto:sambridge@valenciacollege.edu">sambridge@valenciacollege.edu</a> or Elvin Cruz-Vargas <a href="mailto:ecruz52@valenciacollege.edu">ecruz52@valenciacollege.edu</a></p>
                        
                        <p><a href="#NeedHelp">Need Help Applying For a Valencia Foundation Scholarship? CLICK HERE! </a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Need Help Applying For a Valencia Foundation Scholarship?</h3>
                        
                        <p><strong>For questions about the Valencia Foundation Scholarship Application please email your
                              questions and VID number to: (this email is NOT for private scholarship information)</strong>: <a href="mailto:foundation_finaid@valenciacollege.edu%20">foundation_finaid@valenciacollege.edu </a></p>
                        
                        <p><strong>To learn more about Valencia Foundation Scholarships please visit: </strong><a href="http://www.valencia.org/scholarships">http://www.valencia.org/scholarships </a></p>
                        
                        <p><strong>How to Complete a Foundation Scholarship Application - it is a handy checklist on
                              how to create or log into your Valencia Foundation Scholarship Application account:
                              </strong><a href="/admissions/finaid/documents/StepstoCompletetheValenciaFoundationScholarship_1617.pdf">How to Complete a Foundation Scholarship Application</a></p>
                        
                        <p><strong>If you are or will be a new student at Valencia College please see the New Student
                              Checklist at: </strong><a href="/admissions/finaid/documents/EnrollmentServicesChecklist.pdf">Enrollment Services New Student Checklist</a></p>
                        
                        <p><strong>Questions for drafting or refining your scholarship essay: </strong><a href="/admissions/finaid/documents/Draftingascholarshipessay.pdf">Drafting an Exceptional Scholarship Essay</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>What is a First Generation College Student?</h3>
                        
                        <p>Students who are the first in their families to attend an institution of higher education,
                           including vocational or technical school.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>Need Help Writing a Scholarship Thank You Letter ?</h3>
                        
                        <p>Please feel free to view the following <a href="/admissions/finaid/documents/ThankYouNoteMemo.pdf">Quick Tip Guide</a> to writing an exceptional thank you letter to our donors. Please note that we do
                           accept thank you greeting cards in addition to your original letter. Thank You Letters
                           should include the following: What your program of studies are at Valencia, what your
                           future educational goals are after Valencia and what your future goals are after your
                           education is complete.
                        </p>
                        
                        <p>If you have any additional questions, please feel free to contact Valencia Foundation
                           at <a href="mailto:foundation_finaid@valenciacollege.edu">foundation_finaid@valenciacollege.edu</a>&nbsp;
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>Valencia Foundation Student Feature Opportunity</h3>
                        
                        <p>The Valencia Foundation, in partnership with our generous donors, offer and award
                           scholarship funding throughout the academic year. If you are a 2016-17 or 2017-18&nbsp;Valencia
                           Foundation scholarship recipient, we would love to hear from you! Please email your
                           story to <a href="mailto:foundation_finaid@valenciacollege.edu">foundation_finaid@valenciacollege.edu</a> for a chance to be featured on our Blog, Facebook and Twitter sites!
                        </p>
                        
                        <p>Please feel free to share the following in your email submission:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>What has been your proudest achievement so far?</li>
                           
                           <li>What do you want to get out of your college experience?</li>
                           
                           <li>How did you become interested in your major?</li>
                           
                           <li>What have you learned about yourself as you've been a student at Valencia?</li>
                           
                           <li>What are your educational/academic goals? What are your future career plans?</li>
                           
                           <li>How do you plan to use your studies to achieve your future career plans?</li>
                           
                           <li>What do you envision yourself doing in 10 years?</li>
                           
                           <li>What will you do with your degree in the long run?</li>
                           
                           <li>Why will this scholarship help you in your career goals?</li>
                           
                           <li>What is your fondest volunteer experience?</li>
                           
                           <li>What scholarship did you receive?</li>
                           
                           <li>What would you like to say to the donor who awarded you their scholarship?</li>
                           
                           <li>Photographs from volunteer, community service or classroom settings.</li>
                           
                        </ul>
                        
                        <p>We look forward to sharing your inspiring stories!</p>
                        
                        <hr class="styled_2">
                        
                        <h3 id="CurrentOutside">Current Private Scholarships (Non-Valencia Foundation Donors)</h3>
                        
                        <p><strong>What is a Private Scholarship?</strong> A private scholarship is scholarship that is not offered through Valencia College,
                           Valencia Foundation or Valencia Alumni. Private donors send scholarship offers from
                           their companies and businesses for any students who meet their criteria to apply for.
                           Private scholarship offers are added regularly so check back often!
                        </p>
                        
                        <p><strong>PRIVATE SCHOLARSHIP DONORS:</strong> If you are a private/outside scholarship donor and would like to have your scholarship
                           offer shared with Valencia College students please email your information to <a href="mailto:sambridge@valenciacollege.edu">Susan Ambridge. </a></p>
                        
                        <p><strong>STUDENTS</strong>: Private scholarships are not affiliated with Valencia College or Valencia Foundation.
                           These are other private resources that you may be eligible for. Students should never
                           pay to apply for a scholarship. Avoid scholarship guarantees. Never provide any personal
                           information to apply for a scholarship (bank account information, credit card numbers,
                           etc.). Students may visit <a href="https://www.consumer.ftc.gov/articles/0082-scholarship-and-financial-aid-scams">the FTC's Scholarship and Financial Aid Scams page</a> to learn how to avoid scholarship scams and other free
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li><a href="https://www.idsecurityonline.com/scholarship-program/" target="_blank">$1,000 ID Security Online Scholarship</a> - Deadline 01/15/18
                           </li>
                           
                           <li><a href="https://www.homesecuritylist.com/smart-home-scholarship/" target="_blank"> $1,000 Meldon Law College Scholarship</a> Deadline 01/20/2018
                           </li>
                           
                           <li><a href="https://bettercreditblog.org/scholarship/" target="_blank">$1,750 Positive Habit Scholarship</a> - Deadline 02/15/18
                           </li>
                           
                           <li><a href="http://bestlaptopsworld.com/scholarship/" target="_blank">$1,000 Innovation Scholarship</a> - Deadline 02/20/18
                           </li>
                           
                           <li><a href="https://www.oxagile.com/scholarship/" target="_blank">$2,000 Oxagile Annual Virtual Reality Scholarship</a> - Deadline 02/22/18
                           </li>
                           
                           <li><a href="https://www.elearners.com/scholarships/military-scholarships/" target="_blank">$1,000 Military Scholarship Essay Contest</a> Deadline 02/28/18
                           </li>
                           
                           <li><a href="https://www.musicinstrumentscenter.com/2018-scholarship/" target="_blank">$</a><a href="https://jitterymonks.com/scholarship/" target="_blank">1,500 Jitterymonks Scholarship</a> - Deadline 03/01/18
                           </li>
                           
                           <li><a href="https://www.chicagoexcelclasses.com/scholarship/" target="_blank">$1,000 Continued Learning Scholarship</a> - Deadline 03/01/18
                           </li>
                           
                           <li><a href="https://techreviewpro.com/techreviewpro-scholarship-program/">$1,000 TechReviewPro Scholarship Program</a> Deadline 09/30/2017
                           </li>
                           
                           <li><a href="http://accident.usattorneys.com/scholarship-program/">$1,500 USAttorneys.com Accident Law Scholarship</a> Deadline 03/15/2018
                           </li>
                           
                           <li><a href="https://getlegal.nyc/scholarship/" target="_blank"> $1,000 Rybak Firm Scholarship</a> Deadline 03/15/2018
                           </li>
                           
                           <li><a href="https://attorneyguss.com/annual-stewart-j-guss-college-student-scholarship/" target="_blank">$1,000 Stewart J Guss College Scholarship</a> - Deadline 03/15/2018
                           </li>
                           
                           <li><a href="http://suvchicagolimo.com/scholarship/" target="_blank">$1,500 SUV Chicage Limo Scholarship</a> - Deadline 03/18/2018
                           </li>
                           
                           <li><a href="http://www.bikejar.com/scholarship/" target="_blank">$1,000 Bikejar Scholarship</a> - Deadline 03/30/18
                           </li>
                           
                           <li><a href="http://bautistaleroy.com/bautista-leroy-entrepreneur-scholarship/" target="_blank">$2,500 Bautista LeRoy Entrepreneur Scholarship</a> - Deadline 03/30/18
                           </li>
                           
                           <li><a href="http://epfilms.tv/epfilms-scholarship-program/" target="_blank">$1,000 EpiFilms Scholarship Program</a> Deadline 03/31/18
                           </li>
                           
                           <li><a href="http://www.mommaps.com/scholarship/" target="_blank">$5,000 Best Mattress Guide Scholarship</a> - Deadline 03/31/18
                           </li>
                           
                           <li><a href="http://www.levelwinner.com/scholarship/" target="_blank">$2,000 Level Up Scholarship</a> - Deadline 04/01/18
                           </li>
                           
                           <li><a href="https://www.chicagocomputerclasses.com/scholarship/" target="_blank">$2,000 Real World Experience Scholarship</a> - Deadline 04/18/18
                           </li>
                           
                           <li><a href="https://bestcarseathub.com/secure-car-seat-scholarship/" target="_blank">Up to $2,500 Child Safety in Cars Scholarship</a> - Deadline 04/25/18
                           </li>
                           
                           <li><a href="http://besttrampolinereview.net/scholarships/" target="_blank">$3,100 Creative Essay Writing Scholarship</a> - Deadline 04/30/18
                           </li>
                           
                           <li><a href="https://thepeckfirm.com/peck-law-firm-scholarship/" target="_blank">$2,000 Peck Law Firm Scholarship</a> - Deadline 05/31/18
                           </li>
                           
                           <li><a href="https://clufflaw.com/phoenix-car-accident-lawyer/#scholarship-application" target="_blank">$750 Cluff Law Car Accident Scholarship</a> - Deadline 05/31/18
                           </li>
                           
                           <li><a href="https://www.colwell-law.org/albany-divorce-lawyer/#scholarship-application" target="_blank">$2,000 Colwell Law Group Single Parent Scholarship</a> - Deadline 05/31/18
                           </li>
                           
                           <li><a href="https://americantechpros.com/newjersey/scholarship/" target="_blank">$1,000 IT Support NJ Scholarship</a> - Deadline 05/31/18
                           </li>
                           
                           <li><a href="http://www.scam-detector.com/scholarship/not-selected" target="_blank">$1,000 Scam Detector Scholarship</a> - Deadline 06/30/18
                           </li>
                           
                           <li><a href="https://www.mitchellcrunk.com/commerce-ga-adoption-attorney/#scholarship-application" target="_blank">$500 Mitchell &amp; Crunk Adoption Scholarship</a> - Deadline 06/30/18
                           </li>
                           
                           <li><a href="https://www.slumbersearch.com/scholarship" target="_blank">$1,000 Slumber Search Scholarship</a> - Deadline 06/30/18
                           </li>
                           
                           <li><a href="https://sorin.tv/scholarship/" target="_blank">$1,000 Travel by Dart TV Show Scholarship</a> - Deadline 06/30/18
                           </li>
                           
                           <li><a href="http://smartmail.io/email-marketing-scholarship/" target="_blank">$2,000 SmartMail Scholarship</a> - Deadline 06/30/18
                           </li>
                           
                           <li><a href="https://glofx.com/college-scholarship/" target="_blank">$500 GloFX College Scholarship</a> - Deadline 07/31/18
                           </li>
                           
                           <li><a href="http://www.iobd2.org/scholarship/" target="_blank">$3,000 IOBD2 Essay Contest Scholarship</a> - Deadline 07/31/18
                           </li>
                           
                           <li><a href="https://www.creditsesame.com/scholarship/" target="_blank">$1,000 Credit Sesame Essay Scholarship</a> - Deadline 07/31/18
                           </li>
                           
                           <li><a href="http://sdbotox.com/scholarship/" target="_blank">$2,000 SDBotox Scholarship Essay Contest</a> - Deadline 08/15/18
                           </li>
                           
                           <li><a href="http://bestcarwaxguide.net/scholarship/" target="_blank">$3,000 BCG Essay Contest Scholarship</a> - Deadline 08/31/18
                           </li>
                           
                           <li><a href="http://www.myflyfit.com/scholarship/" target="_blank">$2,500 Myflyfit Essay Writing Scholarship</a> - Deadline 08/31/18
                           </li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3 id="FoundationList">Valencia Foundation Scholarship List</h3>
                        
                        <p>Valencia Foundation offers students hundreds of scholarship opportunities every year.
                           All it takes is ONE scholarship application per year to be eligible for consideration.
                           After submitting your Valencia Foundation Scholarship Application it will be forwarded
                           to ALL Valencia Foundation scholarship donors to which you qualify for! Here is a
                           small sample of the hundreds of Valencia Foundation scholarship opportunities available
                           each year. Should you be a match for any of these your application will be forwarded
                           automatically to the donor for consideration. If you are selected by the donor you
                           will be notified through your Atlas email with more information. Donors make selections
                           through the entire aid year.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Featured Valencia Foundation Scholarship: <a href="#Featured">Commercial Real Estate Women Orlando Scholarship (CREW Orlando) (0516) </a></li>
                           
                           <li><a href="#HispanicBC">Hispanic Business Council Scholarship</a></li>
                           
                           <li><a href="#WaltersSecondChance">Walters Family Foundation/Second Chance Scholarship (1149) </a></li>
                           
                           <li><a href="#f465">Orlando Chapter IAAP/Sylvia Scott CPS Memorial Scholarship</a></li>
                           
                           <li><a href="#F470">Mercury Marine Scholarship</a></li>
                           
                           <li><a href="#F623">Central Florida Amateur Softball Association Scholarship</a></li>
                           
                           <li><a href="#F711">Universal Orlando Scholarship</a></li>
                           
                           <li><a href="#F765">M. Overstreet Hospitality Scholarship</a></li>
                           
                           <li><a href="#bloomandgrow">Bloom N Grow Scholarship</a></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3 id="Alumni">Valencia Alumni Association Scholarships and Award Programs</h3>
                        
                        <p>The 2016-17 Valencia General Scholarship Application (sponsored by the Alumni Association)
                           is now open! Apply today at:
                        </p>
                        
                        <p><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466</a></p>
                        
                        <p>Valencia Foundation administers scholarships and award programs sponsored by the Alumni
                           Association that benefit students attending Valencia College. These scholarships are
                           for CURRENT Valencia College students only.
                        </p>
                        
                        <p>To learn more about the various Alumni Association Scholarships that are available
                           please click <a href="/admissions/alumni/scholarships.html">HERE!</a></p>
                        
                        <p>If you have questions regarding Alumni Association Scholarships for students, please
                           contact Elvin Cruz-Vargas, Scholarships &amp; Special Projects Coordinator at <a href="mailto:Ecruz52@valenciacollege.edu">Ecruz52@valenciacollege.edu</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Valencia Foundation Scholarships</h3>
                        
                        <h4 id="Featured">Commercial Real Estate Women Orlando Scholarship (CREW Orlando) (0516)</h4>
                        
                        <p>The CREW Network Foundation scholarship program supports future female leaders as
                           they pursue university-level education that will lead to careers in commercial real
                           estate. With the help of our generous donors, Scholarship Endowment Founders and scholarship
                           program partners, CBRE and PGIM Real Estate, we were able to award fifteen (15) $5,000
                           academic scholarships to young women in 2016. To date, CREW Network and its Foundation
                           have awarded 91 scholarships totaling $685,000.
                        </p>
                        
                        <p><strong>All scholars receive:</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Up to $5,000 USD (tuition and books)</li>
                           
                           <li>Paid internship opportunity (summer of 2018)</li>
                           
                           <li>Free CREW Network student at-large membership (18 months)</li>
                           
                           <li>Free registration to a CREW Network Convention &amp; Marketplace</li>
                           
                        </ul>
                        
                        <p><strong>Applicants must meet these requirements</strong>:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Completed FAFSA</li>
                           
                           <li>Must be a female</li>
                           
                           <li>Must have a minimum 3.0 GPA (4.0 scale)</li>
                           
                           <li>Must be a citizen of the U.S. or Canada</li>
                           
                           <li>Must be a full-time junior, senior or graduate student (2017-18 academic year) at
                              an accredited college or university
                           </li>
                           
                           <li>Intent is to pursue a career in commercial real estate</li>
                           
                           <li>Focus of study included in our qualified fields of commercial real estate</li>
                           
                           <li>Enrolled in any Accounting, Acquisitions/Dispositions, Appraisal, Architecture, Asset
                              Management, Brokerage, CRE Business Development (100% CRE firm), Commercial Insurance,
                              Commercial Lending, Construction Management/General, Contracting, Consulting, Corporate
                              Real Estate, Cost Segregation, Economic Development, Education, Engineering, Environmental,
                              CRE Executive, Facility Management, Finance, CRE Human Resources, Interior Design/Space
                              Planning, Investment Management, Investor Relations, Land Use Planning and Zoning,
                              Land Surveying, Law, Market Research, Program Management/Project Management, Property
                              Management, Public Sector, Quasi-Governmental Transportation and Port, Authorities,
                              Real Estate Development, Relocation Services, Corporate, Risk Management, or Title/Escrow
                              program of study
                           </li>
                           
                        </ul>
                        
                        <p><strong>To apply please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="HispanicBC">Hispanic Business Council Scholarship (F1167)</h4>
                        
                        <p>Hispanic Business Council awards students who are Hispanic Osceola County residents
                           who are enrolled in a Business Related Program at Valencia College.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Are you Hispanic?</li>
                           
                           <li>Are you enrolled in a Business Related Program at Valencia?</li>
                           
                           <li>Are you a resident of Osceola County?</li>
                           
                           <li>Did you file a current FAFSA ? <a href="http://www.fafsa.ed.gov/">FAFSA</a> (Free Application for Federal Student Aid)
                           </li>
                           
                        </ul>
                        
                        <p>If you match all of the above, then you could be a match for the 2015-16 Hispanic
                           Business Council Scholarship!
                        </p>
                        
                        <p>The scholarship will pay toward tuition, fees, and books.</p>
                        
                        <p><strong>To apply please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a>.</strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="WaltersSecondChance">Walters Family Foundation/Second Chance Scholarship (1149)</h4>
                        
                        <p>Walters Family Foundation/Second Chance is a scholarship to help single mothers (preferred)
                           assisting with the cost of tuition and books at Valencia College.
                        </p>
                        
                        <p><strong>To apply please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="f465">Orlando Chapter IAAP/Sylvia Scott CPS Memorial Scholarship (F465)</h4>
                        
                        <p>The Orlando Chapter IAAP/Sylvia Scott CPS Memorial Scholarship is currently seeking
                           a student enrolled in office administration for a $500 award.&nbsp; <em>Applicants must meet these requirements:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Submit a complete <a href="http://www.fafsa.ed.gov">FAFSA</a> (Free Application for Federal Student Aid).
                           </li>
                           
                           <li>Demonstrate Financial Need.</li>
                           
                           <li>Have a Minimum GPA of 3.0 on a 4.0 scale.</li>
                           
                           <li>Be part-time or full-time student.</li>
                           
                        </ul>
                        
                        <p>The scholarship will pay toward tuition.</p>
                        
                        <p><strong>To apply, please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application.</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="F470">Mercury Marine Scholarship (F470)</h4>
                        
                        <p>The Mercury Marine Scholarship is seeking an applicant who is the son, daughter, current
                           spouse of a Mercury Marine employee or of a former employee of Mercury Marine who
                           had contributed to the scholarship fund.&nbsp; <em>Applicants must also meet these additional requirements:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Submit a complete <a href="http://www.fafsa.ed.gov">FAFSA</a> (Free Application for Federal Student Aid).
                           </li>
                           
                           <li>Demonstrate Financial Need.</li>
                           
                           <li>Maintain a 2.5 GPA on a 4.0 scale.</li>
                           
                        </ul>
                        
                        <p>The scholarship will pay toward tuition and books.</p>
                        
                        <p>To apply, please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a><em><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">.</a></em></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="F623">Central Florida Amateur Softball Association Scholarship (F623)</h4>
                        
                        <p>The Central Florida Amateur Softball Association Scholarship is currently seeking
                           a student who has completed community service with the Metro Orlando ASA.&nbsp; <em>Applicants must also meet these additional requirements:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Submit a complete <a href="http://www.fafsa.ed.gov">FAFSA</a> (Free Application for Federal Student Aid).
                           </li>
                           
                           <li>Have a Minimum GPA of 2.5 on a 4.0 scale.</li>
                           
                           <li>Must be a Full-Time Student.</li>
                           
                           <li>Must be resident of Orange, Osceola or Seminole County.</li>
                           
                        </ul>
                        
                        <p>The scholarship will pay toward tuition.</p>
                        
                        <p><strong>To apply, please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application.</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="F711">Universal Orlando Scholarship (F711)</h4>
                        
                        <p>The Universal Orlando Scholarship is currently seeking recent graduating high school
                           seniors from Dr. Phillips, Olympia, West Orange, Jones, or Evans high school. Applicants
                           must also meet these additional requirements:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Have a Minimum GPA of 2.0.</li>
                           
                           <li>Should a student apply, be accepted to, and attend the UCF Rosen School of Hospitality
                              in Orlando the student will then be eligible for up to an addtional two years of scholarship
                              awards.
                           </li>
                           
                        </ul>
                        
                        <p>The scholarship will pay toward tuition, fees, books and materials.</p>
                        
                        <p><strong>To apply please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application.</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="F765">M. Overstreet Hospitality Scholarship (F765)</h4>
                        
                        <p>The M. Overstreet Hospitality Scholarship is currently seeking a student enrolled
                           in one of Valencia's Hospitality &amp; Tourism Institute programs.&nbsp; <em>Applicants must also meet these additional requirements:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Submit a complete <a href="http://www.fafsa.ed.gov">FAFSA</a> (Free Application for Federal Student Aid).
                           </li>
                           
                           <li>Have a Minimum GPA of 2.5 on a 4.0 scale.</li>
                           
                           <li>Demonstrated involvement in hospitality industry through work and/or industry activities.The
                              scholarship will pay toward tuition.
                           </li>
                           
                        </ul>
                        
                        <p><strong>To apply, please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application.</a></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h4 id="bloomandgrow">Bloom N Grow Scholarship (F1143)</h4>
                        
                        <p>The Bloom N Grow Scholarship, an affiliate of Florida Federal of Garden Clubs, Inc.,
                           was organized by a group of West Orange County women sharing a common interest in
                           gardening and horticulture. The purpose of this scholarship is to support educational
                           opportunities for students who have an interest in gardening and horticulture. <em>All applicants must meet these requirements:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>Must be Enrolled Full-Time or Part-Time.</li>
                           
                           <li>Preference for graduates of West Orange High School.</li>
                           
                           <li>Must be enrolled in Horticulture Science, Landscape or Horticulture Technology</li>
                           
                        </ul>
                        
                        <p>The scholarship will pay toward tuition, books and fees.</p>
                        
                        <p><strong>To apply please complete the <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">Valencia Foundation Scholarship Application</a><em><a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">.</a></em></strong></p>
                        
                        <hr class="styled_2">
                        
                        <h3 id="ScholarshipSearch">FREE Scholarship Search Engines and Resources</h3>
                        
                        <p>Start your Search Now With FREE Scholarship Search Sites. Good Luck!</p>
                        
                        <p><strong>STUDENTS:</strong> Private scholarships are not affiliated with Valencia College or Valencia Foundation.
                           These are other private resources that you may be eligible for. Students should never
                           pay to apply for a scholarship. Avoid scholarship guarantees. &nbsp;Never provide any personal
                           information to apply for a scholarship (bank account information, credit card numbers,
                           etc.). Students may visit <a href="https://www.consumer.ftc.gov/articles/0082-scholarship-and-financial-aid-scams">https://www.consumer.ftc.gov/articles/0082-scholarship-and-financial-aid-scams</a> to learn how to avoid scholarship scams and other free scholarship information.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://www.salliemae.com/plan-for-college/scholarships/scholarship-search/?dtd_cell=SMPLSHDCOTDOWBOTOTHOTHRR010005">Sallie Mae Scholarship Search</a></li>
                           
                           <li></li>
                           
                           <li><a href="http://www.collegescholarships.com/">CollegeScholarships.com</a></li>
                           
                           <li><a href="http://www.fastweb.com">The FastWeb Scholarship Search Engine</a> is updated daily.
                              
                              <ul class="list_style_1 nomargin">
                                 
                                 <li>View the <a href="http://www.fastweb.com/nfs/fastweb/static/Student_Bulletin_HS_SeptOct10_Final.pdf">Fastweb Student Bulletin</a></li>
                                 
                                 <li><a href="http://www.finaid.org/scholarships/WinningaScholarship.pdf" target="_blank">Winning a Scholarship Quick Reference Guide</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li><a href="https://www.chegg.com/scholarships">Chegg Scholarship Search Engine</a></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                           <li></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/scholarship-bulletin-board.pcf">©</a>
      </div>
   </body>
</html>