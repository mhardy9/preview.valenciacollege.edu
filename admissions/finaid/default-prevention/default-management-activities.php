<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Default Prevention Activities  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/default-management-activities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Default Prevention Activities </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Default Prevention Activities</h2>
                        
                        
                        <h3>Organization of Debt Prevention Activities</h3>
                        
                        <p>This document is organized using Valencia College's "Early Stages of Enrollment" approach,
                           and describes the default prevention and debt management efforts of the Financial
                           Aid Services Office at each stage of the borrower's loan: 
                        </p>
                        
                        <ol>
                           
                           <li><a href="#NSE">New Student Experience (NSE) and Student Life Skills (SLS) Courses</a></li>
                           
                           <li><a href="#transfer-orientation">Transfer Orientation</a></li>
                           
                           <li><a href="#entrance-counseling">Entrance Counseling</a></li>
                           
                           <li><a href="#financial-literacy">Financial Literacy for Borrowers</a></li>
                           
                           <li><a href="#communication">Communication Across Campus</a></li>
                           
                           <li><a href="#graduation-or-withdrawal">Upon Graduation or Withdrawal</a></li>
                           
                           <li><a href="#entering-repayment">Upon Entering Repayment</a></li>
                           
                           <li><a href="#during-repayment">During Repayment</a></li>
                           
                           <li><a href="#exit-counseling">Exit Counseling</a></li>
                           
                           <li><a href="#NIS">Nursing Information Sessions</a></li>
                           
                        </ol>
                        
                        
                        <h3 id="NSE">New Student Experience (NSE) and Student Life Skills (SLS) Courses</h3>
                        
                        <p>Incoming first-year students will be given an overview of the financial aid process,
                           student loan responsibilities, and basic money management skills, through New Student
                           Experience (NSE) and Student Life Skills (SLS) courses. 
                        </p>
                        
                        
                        <h3 id="transfer-orientation">Transfer Orientation</h3>
                        
                        <p>A financial aid representative disseminate materials regarding the different types
                           of student loan repayment plans, Managing Your Credit booklet, Managing Your Credit
                           booklet, etc. Transfer students receive a personalized letter stating how much they
                           have borrowed since the beginning of their educational career. 
                        </p>
                        
                        
                        <h3 id="entrance-counseling">Entrance Counseling</h3>
                        
                        <p>The current process for meeting the federal entrance counseling requirement is achieved
                           by referring students to the following website: <a href="http://www.studentloans.gov">www.studentloans.gov</a> where the student checks off that they have read and understand repayment obligations.
                           We believe that students do not sufficiently comprehend their repayment obligations
                           from simply going online and checking a box agreeing to repay the loan. And the cohort
                           default rate increase over the last three years proves this.
                        </p>
                        
                        
                        <h3 id="financial-literacy">Financial Literacy for Borrowers</h3>
                        
                        <p>Valencia College Financial Learning Ambassadors Peer Program promote financial literacy
                           and responsible money management amongst their student peers by conducting skill shops,
                           presenting in New Student Experience (NSE) (first year experience classes), collaborating
                           with Wellness Ambassadors and other faculty members and Student Life offices at campus
                           events. Many of the Ambassadors' presentations are with students enrolled in Valencia's
                           Student Success courses. These are classes teaching strategies for success in life
                           and college and providing the appropriate platform for introducing the topic of financial
                           responsibility. In addition, Ambassadors' are partnering with faculty members who
                           teach developmental math courses to promote financial literacy. Ambassadors also ensure
                           they reach students outside of the Student Success courses by hosting information
                           booths and distributing financial literacy material during campus events. Through
                           these Ambassadors, students are empowered to make educated fiscal choices and take
                           action to improve current and future financial security. 
                        </p>
                        
                        
                        <h3 id="communication">Communication Across Campus</h3>
                        
                        <p>On October 2, 2014, we implemented our Default Management Committee to get the commitment
                           from key individuals’ college wide to acknowledge and support default management and
                           prevention. Since then, the name of the committee was changed to Default Prevention
                           Advisory Committee. The reason for the name change, was to “prevent” the student from
                           going into debt, not “manage” their debt. The Financial Aid Services Office along
                           with the Financial Learning Ambassadors reached out to faculty and was able to get
                           them to add USA Funds Life Skills workshops to NSE classes. We were also able to target
                           graduating students and provide them with information and resources to help them regarding
                           their student loans, and target at risk cohorts and provide face to face counseling
                           and in classroom orientations. 
                        </p>
                        
                        
                        <h3 id="graduation-or-withdrawal">Upon Graduation or Withdrawal</h3>
                        
                        <p><em>Graduation: </em>Valencia College Graduation Office sends a report to the Financial Aid Services Office
                           of students who have applied for graduation. The Financial Aid Services Office contacts
                           these students to notify them of the exit counseling requirement. These students have
                           the option to complete exit counseling online as well as in-person at Grad Finale.
                        </p>
                        
                        <p> <em>Withdrawal:</em>Many borrowers who default on their loans are borrowers who withdrew from the college
                           prior to completing their academic programs. Early identification and timely intervention
                           improves student retention and reduces the number of defaulted loans. In addition
                           to fulfilling the regulatory requirement to provide exit counseling to students, Valencia
                           College is implementing the review of students who show up on the Return to Title
                           IV (R2T4) reports who have student loans. These students will be contacted via phone
                           to inform them of the effects of withdrawing and be provided with information regarding
                           repayment options. <br> When contacting withdrawing students the following information will be provided,
                           
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>An estimate of required monthly payments on the borrower's loan balance,</li>
                           
                           <li>Loan servicer contact information,</li>
                           
                           <li>Contact information for delinquency and default prevention assistance on campus,</li>
                           
                           <li>Introduction to NSLDS for Students,</li>
                           
                           <li>
                              <em>Repaying Your Student Loans</em> publication.
                           </li>
                           
                        </ul>
                        
                        
                        <h3 id="entering-repayment">Upon Entering Repayment</h3>
                        
                        <p> Valencia College is currently using the USA Funds Borrower Connect software to communicate
                           with borrowers as they enter and make their way through repayment. Letters and emails
                           are sent monthly. Sedrick Brinson is the primary user of the software and is the students'
                           contact throughout repayment. 
                        </p>
                        
                        
                        <h3 id="during-repayment">During repayment</h3>
                        
                        <p> Throughout repayment, Valencia College uses USA Funds Borrower Connect to send delinquency,
                           deferment, grace, default and forbearance letters and emails to borrowers. This tool
                           has been used since September 2013. This communication generally generates email conversation
                           with the borrowers, and Sedrick Brinson is the primary owner of the activities managing
                           this communication. Borrower detail reports are monitored. 
                        </p>
                        
                        
                        <h3 id="exit-counseling">Exit Counseling Sessions</h3>
                        
                        <p>On November 1-3, 2016, the Financial Aid Services Office held it's First Annual Exit
                           Counseling Sessions. These sessions were held on the East, Osceola, and West campus
                           with the help of the Financial Learning Ambassadors. Students received instructions
                           on how to complete Exit Counseling and were able to ask questions regarding their
                           account. Student were also entered a drawing to receive a $500 scholarship to go towards
                           their loan repayment. These sessions will continue on a semester basis to ensure that
                           our students are continuously receving accurate information about Exit Counseling.
                           
                        </p>
                        
                        
                        <h3 id="NIS">Nursing Information Sessions</h3>
                        
                        <p>In Spring 2016, the Financial Aid Services Office collaborated with the Nursing Department
                           to present to the incoming Traditional and Advanced nursing students. The presentation
                           outlined the different types of scholarships and financial aid available. Nursing
                           students also receive a personalized letter stating how much they have borrowed since
                           the beginning of their educational career.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/default-management-activities.pcf">©</a>
      </div>
   </body>
</html>