<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Repayment Plans  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/repayment-plans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Repayment Plans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Repayment Plans</h2>
                        
                        
                        <h3>What Repayment Plans are available to me?</h3>
                        
                        <p> When it comes time to start repaying your student loan(s), you can select a repayment
                           plan that's right for your financial situation. Generally, you'll have from 10 to
                           25 years to repay your loan, depending on which repayment plan you choose. 
                        </p>
                        
                        
                        <h3>Standard Repayment</h3>
                        
                        <p> With the standard plan, you'll pay a fixed amount each month until your loans are
                           paid in full. Your monthly payments will be at least $50, and you'll have up to 10
                           years to repay your loans.
                        </p>
                        
                        <p> Your monthly payment under the standard plan may be higher than it would be under
                           the other plans because your loans will be repaid in the shortest time. For that reason,
                           having a 10-year limit on repayment, you may pay the least interest.
                        </p>
                        
                        <p> To calculate your estimated loan payments, go to <a href="https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action">https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action.</a> 
                        </p>
                        
                        
                        <h3>Extended Repayment</h3>
                        
                        <p>Under the extended plan, you’ll pay a fixed annual or graduated repayment amount over
                           a period not to exceed 25 years. If you’re a FFEL borrower, you must have more than
                           $30,000 in outstanding FFEL Program loans. If you’re a Direct Loan borrower, you must
                           have more than $30,000 in outstanding Direct Loans.
                        </p>
                        
                        <p> This means, for example, that if you have $35,000 in outstanding FFEL Program loans
                           and $10,000 in outstanding Direct Loans, you can choose the extended repayment plan
                           for your FFEL Program loans, but not for your Direct Loans. Your fixed monthly payment
                           is lower than it would be under the Standard Plan, but you’ll ultimately pay more
                           for your loan because of the interest that accumulates during the longer repayment
                           period.
                        </p>
                        
                        <p> This is a good plan if you will need to make smaller monthly payments. Because the
                           repayment period will be 25 years, your monthly payments will be less than with the
                           standard plan. However, you may pay more in interest because you’re taking longer
                           to repay the loans. Remember that the longer your loans are in repayment, the more
                           interest you will pay.
                        </p>
                        
                        <p> To calculate your estimated loan payments, go to <a href="https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action">https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action.</a> 
                        </p>
                        
                        
                        <h3>Graduated Repayment</h3>
                        
                        <p>With this plan, your payments start out low and increase every two years. The length
                           of your repayment period will be up to ten years. If you expect your income to increase
                           steadily over time, this plan may be right for you.
                        </p>
                        
                        <p> Your monthly payment will never be less than the amount of interest that accrues
                           between payments. Although your monthly payment will gradually increase, no single
                           payment under this plan will be more than three times greater than any other payment.
                        </p>
                        
                        <p> To calculate your estimated loan payments, go to <a href="https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action">https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action.</a> 
                        </p>
                        
                        
                        <h3>Income Based Repayment (IBR) - Effective July 1, 2009</h3>
                        
                        <p>Income Based Repayment is a new repayment plan for the major types of federal loans
                           made to students. Under IBR, the required monthly payment is capped at an amount that
                           is intended to be affordable based on income and family size.
                        </p>
                        
                        <p> You are eligible for IBR if the monthly repayment amount under IBR will be less than
                           the monthly amount calculated under a 10-year standard repayment plan. If you repay
                           under the IBR plan for 25 years and meet other requirements you may have any remaining
                           balance of your loan(s) cancelled.
                        </p>
                        
                        <p> Additionally, if you work in public service and have reduced loan payments through
                           IBR, the remaining balance after ten years in a public service job could be cancelled.
                           For more important information about IBR go to <a href="http://studentaid.ed.gov/PORTALSWebApp/students/english/IBRPlan.jsp">IBR Plan Information</a>. 
                        </p>
                        
                        
                        <h3>Income Contingent Repayment</h3>
                        
                        <p> <strong>(Not available for Parent PLUS Loans)</strong> 
                        </p>
                        
                        <p> This plan gives you the flexibility to meet your Direct Loan obligations without
                           causing undue financial hardship. Each year, your monthly payments will be calculated
                           on the basis of your adjusted gross income (AGI, plus your spouse's income if you're
                           married), family size, and the total amount of your Direct Loans. Under the ICR plan
                           you will pay each month the lesser of:
                        </p>
                        
                        <ol>
                           
                           <li>the amount you would pay if you repaid your loan in 12 years multiplied by an income
                              percentage factor that varies with your annual income, or
                           </li>
                           
                           <li>20% of your monthly discretionary income*.</li>
                           
                        </ol>
                        
                        <p>If your payments are not large enough to cover the interest that has accumulated on
                           your loans, the unpaid amount will be capitalized once each year. However, capitalization
                           will not exceed 10 percent of the original amount you owed when you entered repayment.
                           Interest will continue to accumulate but will no longer be capitalized.
                        </p>
                        
                        <p>The maximum repayment period is 25 years. If you haven't fully repaid your loans after
                           25 years (time spent in deferment or forbearance does not count) under this plan,
                           the unpaid portion will be discharged. You may, however, have to pay taxes on the
                           amount that is discharged.
                        </p>
                        
                        
                        <h3>Income-Based Repayment</h3>
                        
                        <p>Under this plan the required monthly payment will be based on your income during any
                           period when you have a partial financial hardship. Your monthly payment may be adjusted
                           annually. The maximum repayment period under this plan may exceed 10 years. If you
                           meet certain requirements over a specified period of time, you may qualify for cancellation
                           of any outstanding balance of your loans.
                        </p>
                        
                        
                        <h3>Pay As You Earn Repayment</h3>
                        
                        <p>This plan usually has the lowest monthly payment of the repayment plans that are based
                           on your income. Your payment amount may increase or decrease each year based on your
                           income and family size. To qualify for pay as you earn, you must have a partial financial
                           hardship. You have a partial financial hardship if the monthly amount you would be
                           required to pay on your eligible federal student loans under a 10-year standard repayment
                           plan is higher than the monthly amount under pay as you earn. Once you’ve qualified
                           for pay as you earn, you may continue to make payments under the plan even if you
                           no longer have a partial financial hardship. For this purpose, your eligible student
                           loans include Direct Loans as well as certain types of Federal Family Education Loan
                           (FFEL) Program loans. Although your FFELP loans cannot be repaid under pay as you
                           earn, the following types are counted in determining whether you have a partial financial
                           hardship:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Subsidized and Unsubsidized Federal Stafford Loans</li>
                           
                           <li>Federal PLUS Loans made to graduate or professional students</li>
                           
                           <li>Federal Consolidation Loans that did not repay any PLUS loans for parents</li>
                           
                        </ul>
                        
                        <p>You also must be a new borrower as of Oct. 1, 2007, and must have received a disbursement
                           of a Direct Loan on or after Oct. 1, 2011. You are a new borrower if you had no outstanding
                           balance on a Direct Loan or FFE loan as of Oct. 1, 2007, or had no outstanding balance
                           on a Direct Loan or FFE loan when you received a new loan on or after Oct. 1, 2007.
                           
                        </p>
                        
                        
                        <h3>Revised Pay As You Earn Repayment</h3>
                        
                        <p>Generally, 10 percent of your discretionary income. You have 20 years to repay if
                           all loans you’re repaying under the plan were received for undergraduate study. You
                           have 25 years if any loans you’re repaying under the plan were received for graduate
                           or professional study.
                        </p>
                        
                        
                        <h3>Public Service Loan Forgiveness</h3>
                        
                        <p>The Public Service Loan Forgiveness (PSLF) Program forgives the remaining balance
                           on your Direct Loans after you have made 120 qualifying monthly payments under a qualifying
                           repayment plan while working full-time for a qualifying employer. Qualifying employment
                           for the PSLF Program is not about the specific job that you do for your employer.
                           Rather, it is about who your employer is. Employment with the following types of organizations
                           qualifies for PSLF:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Government organizations at any level (federal, state, local, or tribal)</li>
                           
                           <li>Not-for-profit organizations that are tax-exempt under Section 501(c)(3) of the Internal
                              Revenue Code
                           </li>
                           
                           <li>Other types of not-for-profit organizations that provide certain types of qualifying
                              public services
                           </li>
                           
                        </ul>
                        
                        <p>Serving in a full-time AmeriCorps or Peace Corps position also counts as qualifying
                           employment for the PSLF Program. 
                        </p>
                        
                        <p>The following types of employers do not qualify for PSLF:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Labor unions</li>
                           
                           <li>Partisan political organizations</li>
                           
                           <li>For-profit organizations</li>
                           
                           <li>Non-profit organizations that are not tax-exempt under Section 501(c)(3) of the Internal
                              Revenue Code and that do not provide a qualifying service
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/repayment-plans.pcf">©</a>
      </div>
   </body>
</html>