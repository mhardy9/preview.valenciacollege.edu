<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Loan Servicers  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/loan-servicers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Loan Servicers </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Loan Servicers</h2>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> Loan Servicer </th>
                                 
                                 <th scope="col"> Phone Number </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.aspireresourcesinc.com/" target="_blank">Aspire Resources Inc.</a> 
                                 </td>
                                 
                                 <td> 1-855-475-3335 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="https://www.mycornerstoneloan.org/" target="_blank">CornerStone</a> 
                                 </td>
                                 
                                 <td> 1-800-663-1662 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.edfinancial.com/DL" target="_blank">ESA/Edfinancial</a> 
                                 </td>
                                 
                                 <td> 1-855-337-6884 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.myfedloan.org/" target="_blank">FedLoan    Servicing (PHEAA)</a> 
                                 </td>
                                 
                                 <td> 1-800-699-2908 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.gsmr.org/" target="_blank">Granite State — GSMR</a> 
                                 </td>
                                 
                                 <td> 1-888-556-0022 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="https://www.mygreatlakes.org/" target="_blank">Great    Lakes Educational Loan Services, Inc.</a> 
                                 </td>
                                 
                                 <td> 1-800-236-4300 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.mohela.com/" target="_blank">MOHELA</a> 
                                 </td>
                                 
                                 <td> 1-888-866-4352 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.nelnet.com/" target="_blank">Nelnet</a> 
                                 </td>
                                 
                                 <td> 1-888-486-4722 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.osla.org/" target="_blank">OSLA Servicing</a> 
                                 </td>
                                 
                                 <td> 1-866-264-9762 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="https://www.salliemae.com/FederalLoans" target="_blank">Sallie Mae</a> 
                                 </td>
                                 
                                 <td> 1-800-722-1300 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <a href="http://www.vsacfederalloans.org/" target="_blank">VSAC Federal Loans</a> 
                                 </td>
                                 
                                 <td> 1-888-932-5626 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/loan-servicers.pcf">©</a>
      </div>
   </body>
</html>