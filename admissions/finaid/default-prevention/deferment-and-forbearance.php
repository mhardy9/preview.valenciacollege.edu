<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Deferment and Forbearance  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/deferment-and-forbearance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Deferment and Forbearance </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Deferment and Forbearance</h2>
                        
                        <p>If you want additional information on loan default, visit the Department's Debt Resolution
                           website at <a href="https://myeddebt.ed.gov/">https://myeddebt.ed.gov/</a>.
                        </p>
                        
                        
                        <h3>Deferment</h3>
                        
                        <p>If you are having temporary problems repaying your federal student loans, contact
                           your loan servicer to see if you are eligible for deferment. A deferment allows you
                           to temporarily stop making payments on your federal student loans. If you have Direct
                           Subsidized Loans, you are not charged interest on those loans during deferment. You
                           are never charged a fee for applying for a deferment on your federal student loans.
                           Note: interest will continue to be charged during deferment on your Direct or FFEL
                           Unsubsidized and PLUS Loans. If you do not pay this interest during the deferment,
                           it will be capitalized at the end of the deferment.
                        </p>
                        
                        <p>You may qualify for a deferment if you are:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>enrolled at least half time at an eligible postsecondary school;</li>
                           
                           <li>in a full-time course of study in a graduate fellowship program;</li>
                           
                           <li>in an approved full-time rehabilitation program for individuals with disabilities;</li>
                           
                           <li>unemployed or unable to find full-time employment (for a maximum of three years);</li>
                           
                           <li>experiencing an economic hardship (including Peace Corps service) as defined by federal
                              regulations (for a maximum of three years);
                           </li>
                           
                           <li>serving on active duty during a war or other military operation or national emergency
                              and, if you were serving on or after Oct. 1, 2007, for an additional 180-day period
                              following the demobilization date for your qualifying service;
                           </li>
                           
                           <li>performing qualifying National Guard duty during a war or other military operation
                              or national emergency and, if you were serving on or after Oct. 1, 2007, for an additional
                              180-day period following the demobilization date for your qualifying service;
                           </li>
                           
                           <li>a member of the National Guard or other reserve component of the U.S. armed forces
                              (current or retired) and you are called or ordered to active duty while you are enrolled
                              (or within six months of having been enrolled) at least half time at an eligible school.
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Forbearance</h3>
                        
                        <p>If you are having temporary problems repaying your federal student loans and are not
                           eligible for a deferment, contact your loan servicer to see if you are eligible for
                           forbearance. Forbearance is another method of temporarily postponing or reducing loan
                           payments. You are never changed a fee for applying for forbearance on your federal
                           student loans.
                        </p>
                        
                        <p>You may be granted forbearance if you meet one of the following requirements:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>You are unable to make your scheduled loan payments for reasons including, but not
                              limited to, financial hardship and illness.
                           </li>
                           
                           <li>You are serving in a medical or dental internship or residency program and you meet
                              specific requirements
                           </li>
                           
                           <li>The total amount you owe each month for all of the Title IV student loans you received
                              is 20% or more of your total monthly gross income (for a maximum of three years)
                           </li>
                           
                           <li>You are serving in an approved AmeriCorps position.</li>
                           
                           <li>You are performing a teaching service that would qualify for loan forgiveness under
                              the requirements of the Teacher Loan Forgiveness Program
                           </li>
                           
                           <li>You qualify for partial repayment of your loans under the Student Loan Repayment Program,
                              as administered by the Department of Defense
                           </li>
                           
                           <li>You are called to active duty in the U.S. armed forces.</li>
                           
                        </ul>
                        
                        <p>Note: Interest will continue to be charged during forbearance on all types of loans.
                           If you do not pay this interest, it will be capitalized at the end of the forbearance.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/deferment-and-forbearance.pcf">©</a>
      </div>
   </body>
</html>