<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Default Prevention | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/index_backup.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Default Prevention</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Default Prevention</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <img src="featured-2.jpg">
                        
                        
                        <div>
                           
                           
                           <h3>Introduction and Background</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Although not required by the Department of Education to have a default prevention
                                    plan in place, the Financial Aid Services Office at Valencia College makes significant
                                    efforts in the areas of default prevention and debt management. This document, a part
                                    of Valencia College's Financial Aid Policies and Procedures, describes these efforts
                                    and presents information about the institution's loan program that helps guide these
                                    efforts. The Financial Aid Services Office is responsible for implementation of this
                                    plan.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Valencia College Student Loan Borrower Population</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>To  understand the default prevention and debt management efforts for loan  borrowers,
                                    it is helpful to see Valencia College's borrowing population in the  context of the
                                    institution. Valencia College serves well over 60,000 students  annually and processes
                                    over 10,000 loan requests per academic year. In the 2015-2016 academic year, Valencia
                                    College had 59,813  enrolled students, 9,710  of which received student loans and
                                    the average loan was $5,043.42. For the 2015-2016 academic year,  Valencia College
                                    awarded over $83 million dollars in grant and scholarship aid. 
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Valencia College's Default Rates</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    The chart below illustrates Valencia's historical rates compared to  all two year
                                    institutions as well as the state of Florida and the nation's  colleges and universities
                                    overall. It is  important to note that the population of all two year public colleges
                                    nationwide includes many schools quite different in size and character to Valencia,
                                    and therefore the comparison may not be as useful as comparing our rates to  more
                                    similar institutions, which will follow.
                                    
                                 </p>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>Valencia College’s 3-year Cohort Default Rate</strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Cohort Year</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Numerator</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Denominator</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>3-year CDR</strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2006</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>362</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>3,026</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>11.9%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2007</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>399</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>2,543</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>15.6%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2008</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>381</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>2,383</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>15.9%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2009</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>504</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>2,672</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>18.8%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2010</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>644</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>3,670</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>17.5%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2011</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>1,088</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>5,346</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>20.3%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2012</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>1,347</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>7,905</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>17.0%</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>2013</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>1,258</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>8,479</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>14.8%</p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Measuring the Effectiveness of Valencia College's Plan</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Each year, Valencia College will evaluate its financial aid packaging policies, taking
                                    into account institutional as well as student needs. The focus of this review will
                                    be to encourage conservative borrowing by students and to promote scholarships, other
                                    gift aid, and federal work/study in lieu of loans, and will tie into the institutional
                                    effectiveness plan. The Financial Aid Services Office will examine on an ongoing basis
                                    this default prevention plan to assess its effectiveness. As part of this assessment,
                                    the Financial Aid team will work closely with its regional USA Funds Account Executive
                                    to evaluate plans used by other institutions, review useful web sites, and attend
                                    default prevention conferences and workshops to ensure that Valencia College delivers
                                    the most effective programs and services to its students.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/index_backup.pcf">©</a>
      </div>
   </body>
</html>