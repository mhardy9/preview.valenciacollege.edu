<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Default Prevention  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Default Prevention</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Default Prevention</h2>
                        
                        <h3>Introduction and Background</h3>
                        
                        <p>
                           Although not required by the Department of Education to have a default prevention
                           plan in place, the Financial Aid Services Office at Valencia College makes significant
                           efforts in the areas of default prevention and debt management. This document, a part
                           of Valencia College's Financial Aid Policies and Procedures, describes these efforts
                           and presents information about the institution's loan program that helps guide these
                           efforts. The Financial Aid Services Office is responsible for implementation of this
                           plan.
                           
                        </p>
                        
                        <h3>Valencia College Student Loan Borrower Population</h3>
                        
                        <p>To understand the default prevention and debt management efforts for loan borrowers,
                           it is helpful to see Valencia College's borrowing population in the context of the
                           institution. Valencia College serves well over 60,000 students annually and processes
                           over 10,000 loan requests per academic year. In the 2015-2016 academic year, Valencia
                           College had 59,813 enrolled students, 9,710 of which received student loans and the
                           average loan was $5,043.42. For the 2015-2016 academic year, Valencia College awarded
                           over $83 million dollars in grant and scholarship aid.
                           
                        </p>
                        
                        <h3>Valencia College's Default Rates</h3>
                        
                        <p>
                           The chart below illustrates Valencia's historical rates compared to all two year institutions
                           as well as the state of Florida and the nation's colleges and universities overall.
                           It is important to note that the population of all two year public colleges nationwide
                           includes many schools quite different in size and character to Valencia, and therefore
                           the comparison may not be as useful as comparing our rates to more similar institutions,
                           which will follow.
                           
                        </p>
                        
                        <table class="table table">
                           
                           <caption>Valencia College’s 3-year Cohort Default Rate</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Cohort Year</th>
                                 
                                 <th scope="col">Numerator</th>
                                 
                                 <th scope="col">Denominator</th>
                                 
                                 <th scope="col">3-year CDR</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2006</td>
                                 
                                 <td>362</td>
                                 
                                 <td>3,026</td>
                                 
                                 <td>11.9%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2007</td>
                                 
                                 <td>399</td>
                                 
                                 <td>2,543</td>
                                 
                                 <td>15.6%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2008</td>
                                 
                                 <td>381</td>
                                 
                                 <td>2,383</td>
                                 
                                 <td>15.9%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2009</td>
                                 
                                 <td>504</td>
                                 
                                 <td>2,672</td>
                                 
                                 <td>18.8%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2010</td>
                                 
                                 <td>644</td>
                                 
                                 <td>3,670</td>
                                 
                                 <td>17.5%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2011</td>
                                 
                                 <td>1,088</td>
                                 
                                 <td>5,346</td>
                                 
                                 <td>20.3%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2012</td>
                                 
                                 <td>1,347</td>
                                 
                                 <td>7,905</td>
                                 
                                 <td>17.0%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2013</td>
                                 
                                 <td>1,258</td>
                                 
                                 <td>8,479</td>
                                 
                                 <td>14.8%</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3>Measuring the Effectiveness of Valencia College's Plan</h3>
                        
                        <p>
                           Each year, Valencia College will evaluate its financial aid packaging policies, taking
                           into account institutional as well as student needs. The focus of this review will
                           be to encourage conservative borrowing by students and to promote scholarships, other
                           gift aid, and federal work/study in lieu of loans, and will tie into the institutional
                           effectiveness plan. The Financial Aid Services Office will examine on an ongoing basis
                           this default prevention plan to assess its effectiveness. As part of this assessment,
                           the Financial Aid team will work closely with its regional USA Funds Account Executive
                           to evaluate plans used by other institutions, review useful web sites, and attend
                           default prevention conferences and workshops to ensure that Valencia College delivers
                           the most effective programs and services to its students.
                           
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/index.pcf">©</a>
      </div>
   </body>
</html>