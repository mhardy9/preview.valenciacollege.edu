<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Cancellation and Consolidation  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/cancellation-and-consolidation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Cancellation and Consolidation </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Cancellation and Consolidation</h2>
                        
                        <h3>Conditions for cancelling all or part of your loan:</h3>
                        
                        
                        <h3>Teacher Service</h3>
                        
                        <p>If you are a new borrower* and are a full-time teacher in a low-income elementary
                           or secondary school for 5 consecutive years, you may be able to have as much as $17,500
                           of your subsidized or unsubsidized loans cancelled. This provision is not available
                           for borrowers of PLUS Loans. For more information, <a href="http://studentaid.ed.gov/PORTALSWebApp/students/english/cancelstaff.jsp?tab=repaying" target="_blank">see  Student Aid on the Web</a> or contact your loan servicer.
                        </p>
                        
                        <p> * You are considered a new borrower if you did not have an outstanding balance on
                           an FFEL or Direct Loan on Oct. 1, 1998, or on the date you obtained an FFEL or Direct
                           Loan after Oct. 1, 1998.
                        </p>
                        
                        <h3>Public Service</h3>
                        
                        <p> If you are employed in certain public service jobs and have made 120 payments on
                           your Direct Loans (after Oct. 1, 2007), the remaining balance that you owe may be
                           forgiven. Only payments made under certain repayment plans may be counted toward the
                           required 120 payments. You must not be in default on the loans that are forgiven.
                           
                        </p>
                        
                        
                        <h3>School-Related Discharges</h3>
                        
                        <p>In certain cases, you may be able to have all or a part of your loan cancelled because:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Your school closed before you completed your program.</li>
                           
                           <li>Your school forged your signature on your promissory note or falsely certified that
                              you were eligible to get the loan.
                           </li>
                           
                           <li>Your loan was falsely certified because of identity theft (additional requirements
                              apply).
                           </li>
                           
                           <li>You withdrew from school but the school didn't pay a refund that it owed under its
                              written policy or our regulations. Check with the school to see how refund policies
                              apply to federal aid at the school.
                           </li>
                           
                        </ul>
                        
                        <p>In general, you must repay your loan even if you don't graduate, can't find work in
                           your field of study, or are dissatisfied with the education program.
                        </p>
                        
                        
                        <h3>Disability, Bankruptcy. or Death</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Your loan may be discharged if you are determined to be totally and permanently disabled
                              and you meet certain requirements during a 3-year conditional discharge period. To
                              apply for this discharge, you must provide a physician's statement that you became
                              totally and permanently disabled after the loan was made. See your copy of the Borrower's
                              Rights and Responsibilities Statement for more information on the procedures and conditions
                              for this discharge.
                           </li>
                           
                           <li>Your loan may be cancelled if it is discharged in bankruptcy. This is not an automatic
                              process — you must prove to the bankruptcy court that repaying the loan would cause
                              undue hardship.
                           </li>
                           
                           <li>For a student who dies, the loan will be cancelled if a family member or other representative
                              provides acceptable documentation to the student's servicer.
                           </li>
                           
                        </ul>
                        
                        <p>Contact your loan servicer for more information or to get a cancellation form. You
                           can also find more information in your copy of the Borrower's Rights and Responsibilities
                           Statement.
                        </p>
                        
                        
                        <h3>Consolidation</h3>
                        
                        <p>There may be advantages to consolidating (combining) your federal student loans into
                           one loan, starting with the convenience of making a single monthly payment. Consolidation
                           generally extends the repayment period, resulting in a lower monthly payment. This
                           may make it easier for you to repay your loans. However, you will pay more interest
                           if you extend your repayment period through consolidation since you will be making
                           payments for a longer period of time. <a href="http://studentaid.ed.gov/repay-loans/consolidation" target="_blank">Read  more about consolidation</a>.
                        </p>         
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/cancellation-and-consolidation.pcf">©</a>
      </div>
   </body>
</html>