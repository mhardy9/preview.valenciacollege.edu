<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Aid &amp; Money Management Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/default-prevention/money-mgmt-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/default-prevention/">Default Prevention</a></li>
               <li>Financial Aid &amp; Money Management Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Financial Aid &amp; Money Management Resources</h2>
                        
                        
                        <p>These websites may help you navigate the important financial challenges you may face:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="https://www.fafsa.ed.gov" target="_blank">The Free Application for Federal Student Aid (FAFSA)</a> - The FAFSA is used to apply for all federal financial aid, including federal student’s
                              loans, federal grants and work-study programs. You must reapply each academic year,
                              beginning January 1.
                           </li>
                           
                           <li>
                              <a href="https://atlas.valenciacollege.edu/">ATLAS</a> - Once you have completed the FAFSA, you can check the status of outstanding and
                              received paperwork in ATLAS by clicking the Financial Aid tab, then click the Student
                              Requirements tab.
                           </li>
                           
                           <li>
                              <a href="../scholarship-bulletin-board.html">Valencia College Scholarship Bulletin Board</a> - The Valencia College Foundation offers many scholarships to students each semester.
                              The application for all scholarships is completed online and is available throughout
                              the year. Most scholarships offered through the Valencia Foundation require a student
                              to demonstrate need by completing the <a href="http://www.fafsa.ed.gov/">Free Application for Federal Student Aid</a> (FAFSA). Valencia encourages all students to complete the FAFSA early each year and
                              has a priority deadline of July 18th for the upcoming academic year.
                           </li>
                           
                           <li>
                              <a href="https://www.fastweb.com" target="_blank">FastWeb</a> - One of the most comprehensive scholarship search engines.
                           </li>
                           
                           <li>
                              <a href="http://www.floridastudentfinancialaid.org" target="_blank">The Florida Office of Student Financial Assistance (OSFA)</a> provides information about state funded financial aid, scholarships, and loan forgiveness
                              for nursing students.
                           </li>
                           
                           <li>
                              <a href="https://www.osfaffelp.org/workshops/" target="_blank">NyFF Online Workshops</a> - Free online workshops about managing your credit, budget, career planning, financial
                              aid, scholarships, and more.
                           </li>
                           
                           <li>
                              <a href="https://www.nslds.ed.gov" target="_blank">The National Student Loan Data System</a> is the U.S. Department of Education’s central database for student aid. Use this
                              website to find the name of your loan servicer and contact information, as well as
                              additional information about your loans and/or grants.
                           </li>
                           
                           <li>
                              <a href="https://studentaid.ed.gov/sa/repay-loans" target="_blank">Federal Student Aid Loan Repayments</a> - The U.S. Department of Education also offers information to help you manage repayment
                              of your federal student loans, including how to avoid and get out of default.
                           </li>
                           
                           <li>
                              <a href="https://www.mappingyourfuture.org/planyourcareer/careership/index.cfm" target="_blank">Mapping Your Future CareerShip</a> - Provides free information about career planning, college, financial aid and money
                              management.
                           </li>
                           
                           
                           <li>
                              <a href="https://www.bettermoneyhabits.com" target="_blank">Better Money Habits</a> - The Khan Academy partners with Bank of America to provide a collection of free
                              videos and self-paced tools to make understanding money and finances a little easier.
                           </li>
                           
                           <li>
                              <a href="http://www.mymoney.gov" target="_blank">MyMoney.gov</a> - A federal website full of important information about maximizing your financial
                              decisions such as balancing your checkbook, buying a home, or investing in your 401(k).
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/default-prevention/money-mgmt-resources.pcf">©</a>
      </div>
   </body>
</html>