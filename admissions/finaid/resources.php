<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Resources</h2>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://net4.valenciacollege.edu/promos/internal/financialaidvideos.cfm" target="_blank">View short video answers to financial aid questions</a></li>
                           
                           <li><a href="https://www.finaid.ucsb.edu/FAFSASimplification/index.html" target="_blank">7 Easy Steps to Completing the Free Application for Federal Student Aid</a></li>
                           
                        </ul>
                        
                        <h3>Financial Aid Services &amp; College</h3>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://www.nefe.org/" target="_blank">nefe.org </a>: National Endowment for Financial Education
                           </li>
                           
                           <li><a href="http://federalstudentaid.ed.gov/opportunity/index.html" target="_blank">Opportunity.Gov</a> for those receiving unemployment compensation
                           </li>
                           
                           <li><a href="https://studentaid.ed.gov/students/publications/student_guide/index.html" target="_blank">Funding Your Education Beyond High School - Financial Aid Guide </a></li>
                           
                           <li><a href="https://www.fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a></li>
                           
                           <li><a href="https://www.youtube.com/playlist?list=PL23B9A23CD8DD82DD" target="_blank">FAFSA Made Easy Videos</a></li>
                           
                           <li><a href="https://studentaid.ed.gov/sa/repay-loans" target="_blank">Your Federal Student Loans: Learn the Basics and manage your Debt </a></li>
                           
                           <li><a href="http://www.floridastudentfinancialaid.org/" target="_blank">Florida's Office of Student Financial Aid - Bright Futures &amp; More</a></li>
                           
                           <li><a href="http://www.knowhow2go.org/" target="_blank">KnowHow2GO.org</a> (to College) - For Middle School through High School
                           </li>
                           
                           <li><a href="http://www.finaid.org/" target="_blank">Finaid.org - The SmartStudent Guide to Financial Aid </a></li>
                           
                           <li><a href="http://www.fastweb.com/" target="_blank">FastWeb - Scholarships, Financial Aid &amp; College Information </a></li>
                           
                           <li><a href="https://www.cmu.edu/sfs/docs/student-aid.pdf" target="_blank">Student Aid Reference Sheet for 2017-18</a></li>
                           
                           <li><a href="https://fafsa.ed.gov/FAFSA/app/f4cForm" target="_blank">FAFSA4Caster</a> - For High School Juniors and Seniors
                           </li>
                           
                           <li><a href="http://www.edupass.org/finaid/" target="_blank">Financial Aid Information for International Students</a></li>
                           
                        </ul>
                        
                        <h3>Financial Literacy Education</h3>
                        
                        <p>Through your school, you now have access to USA Funds Life Skills, a free online learning
                           program that offers you advice for managing your time and money wisely while on campus
                           and after graduation. This <a href="/admissions/finaid/ValenciaCollege.docx">Student Guide</a> provides basic information about the tool and outlines the steps necessary to access
                           and complete the lesson(s) that your school has assigned to you.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://www.360taxes.org/" target="_blank">360 Degrees of Taxes</a> (answers to all your tax filing questions)
                           </li>
                           
                           <li><a href="https://www.fdic.gov/consumers/consumer/moneysmart/mscbi/mscbi.html" target="_blank">FDIC Money Smart Web Based Instruction </a></li>
                           
                           <li><a href="https://www.aie.org/" target="_blank">Adventures in Education</a></li>
                           
                           <li><a href="https://www.smartaboutmoney.org/" target="_blank">Smart About Money with Economic Survival Guide by National Endowment for Financial
                                 Education </a></li>
                           
                           <li><a href="https://www.pbs.org/your-life-your-money/" target="_blank">PBS SIte for Young Adults: Your Life Your Money</a></li>
                           
                           <li><a href="https://mappingyourfuture.org/Money/" target="_blank">Mapping your Future Money Management </a></li>
                           
                           <li><a href="http://www.floridastudentfinancialaid.org/FFELP/Student_2nd_Level/student_page_2.html" target="_blank">Florida Department of Education - Default Prevention</a></li>
                           
                           <li><a href="/admissions/finaid/default-prevention/index.html" target="_blank">Valencia Default Prevention </a></li>
                           
                           <li><a href="http://www.collegegold.com/index.phtml" target="_blank">College Gold - Guide to Paying for College </a></li>
                           
                           <li><a href="https://www.360financialliteracy.org/Topics/Taxes" target="_blank">360 Degrees of Financial Literacy, Your Money. Your Life </a></li>
                           
                           <li><a href="http://www.studentloanborrowerassistance.org/" target="_blank">Student Loan Borrower Assistance</a> - A Project of the Nat'l Consumer law Center
                           </li>
                           
                           <li><a href="https://www.feedthepig.org/" target="_blank">www.feedthepig.org</a> - Tips on Saving Money from the AICPA
                           </li>
                           
                           <li><a href="https://www.ecmc.org/fab/showPage.action?id=1100" target="_blank">Financial Awareness Basics (FAB)</a></li>
                           
                           <li><a href="https://www.ftc.gov/gettingcredit/" target="_blank">About Credit Cards - Federal Trade Commission </a></li>
                           
                           <li><a href="http://www.mymoney.gov/" target="_blank">My Money - US Financial Literacy and Education Commission</a></li>
                           
                           <li><a href="http://www.handsonbanking.com/" target="_blank">Hands On Banking - Wells Fargo </a></li>
                           
                           <li><a href="http://www.choosetosave.org/calculators/" target="_blank">Financial Web Calculators to help you Save </a></li>
                           
                           <li><a href="https://ticas.org/project-student-debt" target="_blank">Project on Student Debt</a></li>
                           
                           <li><a href="https://creditcardinsider.com/credit-cards/student/" target="_blank">Creditcardinsider.com/credit-cards/student/ </a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/resources.pcf">©</a>
      </div>
   </body>
</html>