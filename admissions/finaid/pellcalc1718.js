// JavaScript Document
// use full year amount divided by 2 for term amounts.  Example full time full year pell divide by 2 is full time one term amount.
function doFullTime1718(efc){
	var pell = 0;
	if(efc == 0){
		pell = 2960;
	}
	if((efc > 0)&&(efc < 101)){pell = 2935;}
	if((efc > 100)&&(efc < 201)){pell = 2885;}
	if((efc > 200)&&(efc < 301)){pell = 2835;}
	if((efc > 300)&&(efc < 401)){pell = 2785;}
	if((efc > 400)&&(efc < 501)){pell = 2735;}
	if((efc > 500)&&(efc < 601)){pell = 2685;}
	if((efc > 600)&&(efc < 701)){pell = 2635;}
	if((efc > 700)&&(efc < 801)){pell = 2585;}
	if((efc > 800)&&(efc < 901)){pell = 2535;}
	if((efc > 900)&&(efc < 1001)){pell = 2485;}
	if((efc > 1000)&&(efc < 1101)){pell = 2435;}
	if((efc > 1100)&&(efc < 1201)){pell = 2385;}
	if((efc > 1200)&&(efc < 1301)){pell = 2335;}
	if((efc > 1300)&&(efc < 1401)){pell = 2285;}
	if((efc > 1400)&&(efc < 1501)){pell = 2235;}
	if((efc > 1500)&&(efc < 1601)){pell = 2185;}
	if((efc > 1600)&&(efc < 1701)){pell = 2135;}
	if((efc > 1700)&&(efc < 1801)){pell = 2085;}
	if((efc > 1800)&&(efc < 1901)){pell = 2035;}
	if((efc > 1900)&&(efc < 2001)){pell = 1985;}
	if((efc > 2000)&&(efc < 2101)){pell = 1935;}
	if((efc > 2100)&&(efc < 2201)){pell = 1885;}
	if((efc > 2200)&&(efc < 2301)){pell = 1835;}
	if((efc > 2300)&&(efc < 2401)){pell = 1785;}
	if((efc > 2400)&&(efc < 2501)){pell = 1735;}
	if((efc > 2500)&&(efc < 2601)){pell = 1685;}
	if((efc > 2600)&&(efc < 2701)){pell = 1635;}
	if((efc > 2700)&&(efc < 2801)){pell = 1585;}
	if((efc > 2800)&&(efc < 2901)){pell = 1535;}
	if((efc > 2900)&&(efc < 3001)){pell = 1485;}
	if((efc > 3000)&&(efc < 3101)){pell = 1435;}
	if((efc > 3100)&&(efc < 3201)){pell = 1385;}
	if((efc > 3200)&&(efc < 3301)){pell = 1335;}
	if((efc > 3300)&&(efc < 3401)){pell = 1285;}
	if((efc > 3400)&&(efc < 3501)){pell = 1235;}
	if((efc > 3500)&&(efc < 3601)){pell = 1185;}
	if((efc > 3600)&&(efc < 3701)){pell = 1135;}
	if((efc > 3700)&&(efc < 3801)){pell = 1085;}
	if((efc > 3800)&&(efc < 3901)){pell = 1035;}
	if((efc > 3900)&&(efc < 4001)){pell = 985;}
	if((efc > 4000)&&(efc < 4101)){pell = 935;}
	if((efc > 4100)&&(efc < 4201)){pell = 885;}
	if((efc > 4200)&&(efc < 4301)){pell = 835;}
	if((efc > 4300)&&(efc < 4401)){pell = 785;}
	if((efc > 4400)&&(efc < 4501)){pell = 735;}
	if((efc > 4500)&&(efc < 4601)){pell = 685;}
	if((efc > 4600)&&(efc < 4701)){pell = 635;}
	if((efc > 4700)&&(efc < 4801)){pell = 585;}
	if((efc > 4800)&&(efc < 4901)){pell = 535;}
	if((efc > 4900)&&(efc < 5001)){pell = 485;}
	if((efc > 5000)&&(efc < 5101)){pell = 435;}
	if((efc > 5100)&&(efc < 5201)){pell = 385;}
	if((efc > 5200)&&(efc < 5301)){pell = 335;}
	if((efc > 5300)&&(efc < 5329)){pell = 303;}
	if(efc > 5328){pell = 0;}
	return pell;
}

function doThreeTime1718(efc){
	var pell = 0;
	if(efc == 0){
		pell = 2220;
	}
	if((efc > 0)&&(efc < 101)){pell = 2202;}
	if((efc > 100)&&(efc < 201)){pell = 2164;}
	if((efc > 200)&&(efc < 301)){pell = 2127;}
	if((efc > 300)&&(efc < 401)){pell = 2089;}
	if((efc > 400)&&(efc < 501)){pell = 2052;}
	if((efc > 500)&&(efc < 601)){pell = 2014;}
	if((efc > 600)&&(efc < 701)){pell = 1977;}
	if((efc > 700)&&(efc < 801)){pell = 1939;}
	if((efc > 800)&&(efc < 901)){pell = 1902;}
	if((efc > 900)&&(efc < 1001)){pell = 1864;}
	if((efc > 1000)&&(efc < 1101)){pell = 1827;}
	if((efc > 1100)&&(efc < 1201)){pell = 1789;}
	if((efc > 1200)&&(efc < 1301)){pell = 1752;}
	if((efc > 1300)&&(efc < 1401)){pell = 1714;}
	if((efc > 1400)&&(efc < 1501)){pell = 1677;}
	if((efc > 1500)&&(efc < 1601)){pell = 1639;}
	if((efc > 1600)&&(efc < 1701)){pell = 1602;}
	if((efc > 1700)&&(efc < 1801)){pell = 1564;}
	if((efc > 1800)&&(efc < 1901)){pell = 1527;}
	if((efc > 1900)&&(efc < 2001)){pell = 1489;}
	if((efc > 2000)&&(efc < 2101)){pell = 1452;}
	if((efc > 2100)&&(efc < 2201)){pell = 1414;}
	if((efc > 2200)&&(efc < 2301)){pell = 1377;}
	if((efc > 2300)&&(efc < 2401)){pell = 1339;}
	if((efc > 2400)&&(efc < 2501)){pell = 1302;}
	if((efc > 2500)&&(efc < 2601)){pell = 1264;}
	if((efc > 2600)&&(efc < 2701)){pell = 1227;}
	if((efc > 2700)&&(efc < 2801)){pell = 1189;}
	if((efc > 2800)&&(efc < 2901)){pell = 1152;}
	if((efc > 2900)&&(efc < 3001)){pell = 1114;}
	if((efc > 3000)&&(efc < 3101)){pell = 1077;}
	if((efc > 3100)&&(efc < 3201)){pell = 1039;}
	if((efc > 3200)&&(efc < 3301)){pell = 1002;}
	if((efc > 3300)&&(efc < 3401)){pell = 964;}
	if((efc > 3400)&&(efc < 3501)){pell = 927;}
	if((efc > 3500)&&(efc < 3601)){pell = 889;}
	if((efc > 3600)&&(efc < 3701)){pell = 852;}
	if((efc > 3700)&&(efc < 3801)){pell = 814;}
	if((efc > 3800)&&(efc < 3901)){pell = 777;}
	if((efc > 3900)&&(efc < 4001)){pell = 739;}
	if((efc > 4000)&&(efc < 4101)){pell = 702;}
	if((efc > 4100)&&(efc < 4201)){pell = 664;}
	if((efc > 4200)&&(efc < 4301)){pell = 627;}
	if((efc > 4300)&&(efc < 4401)){pell = 589;}
	if((efc > 4400)&&(efc < 4501)){pell = 552;}
	if((efc > 4500)&&(efc < 4601)){pell = 514;}
	if((efc > 4600)&&(efc < 4701)){pell = 477;}
	if((efc > 4700)&&(efc < 4801)){pell = 439;}
	if((efc > 4800)&&(efc < 4901)){pell = 402;}
	if((efc > 4900)&&(efc < 5001)){pell = 364;}
	if((efc > 5000)&&(efc < 5101)){pell = 327;}
	if(efc > 5100){pell = 0;}
	return pell;
}

function doHalfTime1718(efc){
	var pell = 0;
	if(efc == 0){
		pell = 1480;
	}
	if((efc > 0)&&(efc < 101)){pell = 1468;}
	if((efc > 100)&&(efc < 201)){pell = 1443;}
	if((efc > 200)&&(efc < 301)){pell = 1418;}
	if((efc > 300)&&(efc < 401)){pell = 1393;}
	if((efc > 400)&&(efc < 501)){pell = 1368;}
	if((efc > 500)&&(efc < 601)){pell = 1343;}
	if((efc > 600)&&(efc < 701)){pell = 1318;}
	if((efc > 700)&&(efc < 801)){pell = 1293;}
	if((efc > 800)&&(efc < 901)){pell = 1268;}
	if((efc > 900)&&(efc < 1001)){pell = 1243;}
	if((efc > 1000)&&(efc < 1101)){pell = 1218;}
	if((efc > 1100)&&(efc < 1201)){pell = 1193;}
	if((efc > 1200)&&(efc < 1301)){pell = 1168;}
	if((efc > 1300)&&(efc < 1401)){pell = 1143;}
	if((efc > 1400)&&(efc < 1501)){pell = 1118;}
	if((efc > 1500)&&(efc < 1601)){pell = 1093;}
	if((efc > 1600)&&(efc < 1701)){pell = 1068;}
	if((efc > 1700)&&(efc < 1801)){pell = 1043;}
	if((efc > 1800)&&(efc < 1901)){pell = 1018;}
	if((efc > 1900)&&(efc < 2001)){pell = 993;}
	if((efc > 2000)&&(efc < 2101)){pell = 968;}
	if((efc > 2100)&&(efc < 2201)){pell = 943;}
	if((efc > 2200)&&(efc < 2301)){pell = 918;}
	if((efc > 2300)&&(efc < 2401)){pell = 893;}
	if((efc > 2400)&&(efc < 2501)){pell = 868;}
	if((efc > 2500)&&(efc < 2601)){pell = 843;}
	if((efc > 2600)&&(efc < 2701)){pell = 818;}
	if((efc > 2700)&&(efc < 2801)){pell = 793;}
	if((efc > 2800)&&(efc < 2901)){pell = 768;}
	if((efc > 2900)&&(efc < 3001)){pell = 743;}
	if((efc > 3000)&&(efc < 3101)){pell = 718;}
	if((efc > 3100)&&(efc < 3201)){pell = 693;}
	if((efc > 3200)&&(efc < 3301)){pell = 668;}
	if((efc > 3300)&&(efc < 3401)){pell = 643;}
	if((efc > 3400)&&(efc < 3501)){pell = 618;}
	if((efc > 3500)&&(efc < 3601)){pell = 593;}
	if((efc > 3600)&&(efc < 3701)){pell = 568;}
	if((efc > 3700)&&(efc < 3801)){pell = 543;}
	if((efc > 3800)&&(efc < 3901)){pell = 518;}
	if((efc > 3900)&&(efc < 4001)){pell = 493;}
	if((efc > 4000)&&(efc < 4101)){pell = 468;}
	if((efc > 4100)&&(efc < 4201)){pell = 443;}
	if((efc > 4200)&&(efc < 4301)){pell = 418;}
	if((efc > 4300)&&(efc < 4401)){pell = 393;}
	if((efc > 4400)&&(efc < 4501)){pell = 368;}
	if((efc > 4500)&&(efc < 4601)){pell = 343;}
	if((efc > 4600)&&(efc < 4701)){pell = 318;}
	if(efc > 4700){pell = 0;}
	return pell;
}

function doQuartTime1718(efc){
	var pell = 0;
	if(efc == 0){
		pell = 740;
	}
	if((efc > 0)&&(efc < 101)){pell = 734;}
	if((efc > 100)&&(efc < 201)){pell = 722;}
	if((efc > 200)&&(efc < 301)){pell = 709;}
	if((efc > 300)&&(efc < 401)){pell = 697;}
	if((efc > 400)&&(efc < 501)){pell = 684;}
	if((efc > 500)&&(efc < 601)){pell = 672;}
	if((efc > 600)&&(efc < 701)){pell = 659;}
	if((efc > 700)&&(efc < 801)){pell = 647;}
	if((efc > 800)&&(efc < 901)){pell = 634;}
	if((efc > 900)&&(efc < 1001)){pell = 622;}
	if((efc > 1000)&&(efc < 1101)){pell = 609;}
	if((efc > 1100)&&(efc < 1201)){pell = 597;}
	if((efc > 1200)&&(efc < 1301)){pell = 584;}
	if((efc > 1300)&&(efc < 1401)){pell = 572;}
	if((efc > 1400)&&(efc < 1501)){pell = 559;}
	if((efc > 1500)&&(efc < 1601)){pell = 547;}
	if((efc > 1600)&&(efc < 1701)){pell = 534;}
	if((efc > 1700)&&(efc < 1801)){pell = 522;}
	if((efc > 1800)&&(efc < 1901)){pell = 509;}
	if((efc > 1900)&&(efc < 2001)){pell = 497;}
	if((efc > 2000)&&(efc < 2101)){pell = 484;}
	if((efc > 2100)&&(efc < 2201)){pell = 472;}
	if((efc > 2200)&&(efc < 2301)){pell = 459;}
	if((efc > 2300)&&(efc < 2401)){pell = 447;}
	if((efc > 2400)&&(efc < 2501)){pell = 434}
	if((efc > 2500)&&(efc < 2601)){pell = 422;}
	if((efc > 2600)&&(efc < 2701)){pell = 409;}
	if((efc > 2700)&&(efc < 2801)){pell = 397;}
	if((efc > 2800)&&(efc < 2901)){pell = 384;}
	if((efc > 2900)&&(efc < 3001)){pell = 372;}
	if((efc > 3000)&&(efc < 3101)){pell = 359;}
	if((efc > 3100)&&(efc < 3201)){pell = 347;}
	if((efc > 3200)&&(efc < 3301)){pell = 334;}
	if((efc > 3300)&&(efc < 3401)){pell = 322;}
	if((efc > 3400)&&(efc < 3501)){pell = 309;}
	if((efc > 3500)&&(efc < 3601)){pell = 298;}
	if(efc > 3600){pell = 0;}
	return pell;
}

function doTheMath1718(){
	var fullTime = "";
	var threeTime = "";
	var halfTime = "";
	var fourTime = "";
	if (isNaN(document.pell1718.efcAmount1718.value) || document.pell1718.efcAmount1718.value == ""){
		alert('You must enter a number in the EFC field');
		return;
	}
	var EFC = eval(document.pell1718.efcAmount1718.value);
	if (isNaN(document.pell1718.crHours1718.value) || document.pell1718.crHours1718.value == ""){
		alert('You must enter a number in the Credits field');
		return;
	}
	var CREDITS = eval(document.pell1718.crHours1718.value);
	if(CREDITS >= 12){
		holder = doFullTime1718(EFC);
	} else if((CREDITS < 12)&&(CREDITS >= 9)){
				holder = doThreeTime1718(EFC);
			} else if((CREDITS < 9)&&(CREDITS >=6)){
						holder = doHalfTime1718(EFC);
					} else if((CREDITS < 6)&&(CREDITS > 0)){
								holder = doQuartTime1718(EFC);
							} else holder = 0;
	document.pell1718.pellAward1718.value = holder;
}



