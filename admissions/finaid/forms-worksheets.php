<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Forms &amp; Worksheets  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/forms-worksheets.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Forms &amp; Worksheets </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Forms &amp; Worksheets</h2>
                        
                        <p>Welcome to the Valencia College Forms &amp; Worksheets page. Follow all instructions carefully
                           or your financial aid may be delayed. 
                        </p>
                        
                        
                        <h3>2017-2018 Forms and Worksheets</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>DO NOT</strong> complete forms 1 - 12 unless they are requested by our office.
                           </li>
                           
                           <li>Make sure you submit the correct form, or your aid will be delayed.</li>
                           
                           <li>Online (eSign) Forms require first time users to create an account. <em>(Students and parents must each create their own seperate accounts.)</em>
                              
                           </li>
                           
                        </ul>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Description </th>
                                 
                                 <th>Online (eSign) </th>
                                 
                                 <th>Print </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1. Dependent Verification Form*</td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=5b476b37-657d-4e2a-aaf6-01dc46d4e7f7" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Dependent_Verification_Worksheet.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2. Independent Verification Form** </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=e13bf67d-cd15-44bd-aa4f-46ef4f638a1d" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Independent_Verification_Worksheet.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3. Student's Identity and Statement of Educational Purpose Worksheet</td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/1718_SOEDP_Form.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>4. Parent Low Income Statement Form* </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=d5497a4f-199a-4369-acd6-d2e8fe8eaa18" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718ParentLowIncomeStatementForm.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>5. Student Low Income Statement Form** </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=a66f3111-43ab-4e15-b12e-1ceddd0e5537" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718StudentLowIncomeStatementForm.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 6. 2017-2018 FAFSA Signature Page</td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/1718_FAFSA_Signature_Page_Form.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>7. Parent Asset Form </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=9afcfa4f-d2be-4034-9906-d49c4877e548" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Asset_Form_000.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>8. Student Asset Form</td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=225acf3a-fa97-4cc8-922e-335533c3fc88" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Asset_Form_000.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>9. Declaration of Parental Non-Support Form*</td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=c0622bd3-f7a4-40f1-b4e9-403867478134" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Declaration_of_Parental_Non_Support_Form_FA_COM_000.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>10. Social Security Number Form</td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/1718_Social_Security_Number_Form.pdf" target="_blank">PDF </a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>11. Bachelor's Degree Confirmation Form</td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=aec02dff-85e2-4f76-a7b4-38aac41c40a5" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Bachelors_Degree_Confirmation_Form.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>12. Selective Service Registration Status Form</td>
                                 
                                 <td> <a href="documents/1718_Selective_Service_Form_000.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 
                                    <em>Only complete this form if you are requesting changes to your existing 2017-2018 Federal
                                       Direct Stafford Student Loans or to reinstate a loan that was previously canceled.
                                       </em>
                                    <br>A. Federal Direct Loan Change Request
                                    
                                 </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=2069606d-2ff5-4a17-b739-4d9291ad5f83" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 
                                    <em>This form must be completed and eSigned by the parent. </em>
                                    <br>B. Parent Loan (Plus) Certification Request* 
                                    
                                 </td>
                                 
                                 <td> <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=0dbe92b2-6452-4293-a246-4512794f6c34" target="_blank">Online</a> 
                                 </td>
                                 
                                 <td> <a href="documents/1718_Federal_Direct_Parent_Loan_PLUS_Form.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> C. Satisfactory Academic Progress (SAP) Appeal Form – All Terms </td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/1718SatisfactoryAcademicProgressAppeal.docx" target="_blank">Word</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> D. Are you making SAP? Use This Worksheet as a Guide</td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/Satisfactory_Academic_Progress_Worksheet_001.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> E. Private Loan Self-Certification Form </td>
                                 
                                 <td></td>
                                 
                                 <td> <a href="documents/1718_Private_Education_Loan.pdf" target="_blank">PDF</a> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> F. Request an IRS  Transcript Online </td>
                                 
                                 <td> 
                                    <p><a href="http://www.irs.gov/individuals/article/0,,id=232168,00.html" target="_blank">IRS Webpage</a> 
                                    </p> 
                                 </td>
                                 
                                 <td></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> G. Request an IRS Transcript by Phone</td>
                                 
                                 <td colspan="2"> 
                                    <p>1-800-908-9946</p> 
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p>* Dependent/Parent Forms – Are for students who were required to file the FAFSA using
                           their parent(s) financial information. Parent(s) signature is required. 
                        </p>
                        
                        <p>**Independent/Student Forms - Are for students who were required to file the FAFSA
                           using their and spouse (if married) financial information. Student signature is required.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/forms-worksheets.pcf">©</a>
      </div>
   </body>
</html>