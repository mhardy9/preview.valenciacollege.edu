<ul>
	<li><a href="/admissions/finaid/">Financial Aid Services</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<div class="menu-wrapper c3">
			<div class="row">
				<div class="col-md-6">
					<a href="/admissions/finaid/getting-started"><h3>Getting Started</h3></a>
					<ul>
						<li><a href="/admissions/finaid/getting-started/obtaining-forms.php">Obtaining Forms</a></li>
						<li><a href="/admissions/finaid/getting-started/atlas-checking.php">Atlas &amp; Checking Financial Aid</a></li>
						<li><a href="/admissions/finaid/getting-started/dates-eligibility.php"> Dates &amp; Eligibility</a></li>
						<li><a href="/admissions/finaid/getting-started/training-certificates.php">Eligible Vocational<br />Training Certificate</a></li>
						<li><a href="/admissions/finaid/getting-started/post-application.php">Post-Application Completion</a></li>
						<li><a href="/admissions/finaid/getting-started/aid-disbursement.php">Using Financial Aid</a></li>
						<li><a href="/admissions/finaid/getting-started/credit-hours.php">Credit Hrs. Needed</a></li>
						<li><a href="/admissions/finaid/getting-started/attending-and-withdrawing.php">Attending Classes</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<a href="/admissions/finaid/programs/"><h3>Programs Available</h3></a>
					<ul>
						<li><a href="/admissions/finaid/programs/grants.php">Grants</a></li>
						<li><a href="/admissions/finaid/programs/scholarships.php">Scholarships</a></li>
						<li><a href="/admissions/finaid/programs/loans.php">Loans</a></li>
						<li><a href="/admissions/finaid/programs/work-study.php">Work Study Programs</a></li>
						<li><a href="/admissions/finaid/federal-direct-loans.php">Apply for a Student or Parent Loan</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<a href="/admissions/finaid/financial_literacy.php"><h3>Financial Literacy</h3></a>
					<ul>
						<li><a href="/admissions/finaid/Events.php">Events</a></li>
						<li><a href="/admissions/finaid/graduation-ready.php">Grad Ready</a></li>
						<li><a href="/admissions/finaid/satisfactory-progress.php">Satisfactory Academic Progress</a></li>
						<li><a href="/admissions/finaid/rights.php">Rights &amp; Responsibilities</a></li>
						<li><a href="/admissions/finaid/Resources.php">Resources</a></li>
						<li><a href="/admissions/finaid/scholarship-bulletin-board.php">Scholarship Bulletin Board</a></li>
						<li><a href="/admissions/finaid/forms-worksheets.php">Forms &amp; Worksheets</a></li>
						<li><a href="/admissions/finaid/attendance-cost.php">Cost of Attendance</a></li>
						<li><a href="/admissions/finaid/Questions.php">FAQs</a></li>
						<li><a href="/admissions/finaid/contact.php">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<a href="/admissions/finaid/default-prevention/"><h3>Default Prevention</h3></a>
					<ul>
						<li><a href="/admissions/finaid/default-prevention/white-house.php">White House Student Loan Debt Challenge</a></li>
						<li><a href="/admissions/finaid/default-prevention/default-management-activities.php">Default Management Activities</a></li>
						<li><a href="/admissions/finaid/default-prevention/repayment-plans.php">Repayment Plans</a></li>
						<li><a href="/admissions/finaid/default-prevention/deferment-and-forebearance.php">Deferment and Forebearnace</a></li>
						<li><a href="/admissions/finaid/default-prevention/cancellation-and-consolidation.php">Cancellation and Consolidation</a></li>
						<li><a href="/admissions/finaid/default-prevention/loan-servicers.php">Loan Servicers</a></li>
						<li><a href="/admissions/finaid/default-prevention/money-mgmt-resources.php">Financial Aid and Money Management Resources</a></li>
					</ul>
				</div>
			</div>
		</div>
	</li>
</ul>