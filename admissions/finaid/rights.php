<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Rights and Responsibilities  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/rights.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Rights and Responsibilities </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Rights and Responsibilities of the Financial Aid Recipient</h2>
                        
                        <p>As a student financial aid consumer you have the right to:</p>
                        
                        <ul class="list_style_1">
                           
                           <li> Information about financial aid application procedures, cost of attendance, aid available,
                              and renewal requirements as well as about Valencia’s academic programs and policies.
                           </li>
                           
                           <li>Confidential protection of your financial aid records. The contents of your financial
                              aid file are maintained in accordance with the Family Educational Rights and Privacy
                              Act.
                           </li>
                           
                           <li>Request a review of decisions made by the Valencia College financial aid services
                              staff. A letter discussing your situation in detail and the decision you wish to have
                              reviewed should be submitted in writing to the Director of Financial Aid Services,
                              Valencia College, P.O. Box 3028, Orlando, FL 32802.
                           </li>
                           
                        </ul>
                        
                        <p>You have the responsibility to:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Complete applications correctly and on time.</li>
                           
                           <li>Read and understand all materials sent to you from Financial Aid Services and other
                              financial aid agencies by e–mail and/or letter. Keep copies of all forms and materials
                              submitted.
                           </li>
                           
                           <li>Know and comply with the rules governing your aid programs.</li>
                           
                           <li>Comply with the provisions of any promissory note and all other agreements you sign.</li>
                           
                           <li>Register for the number of credits required and maintain Satisfactory Academic Progress.</li>
                           
                           <li>Go to the Answer Center on your campus for  personal assistance if you have questions
                              or do not understand the information provided to you.
                           </li>
                           
                           <li>Budget and manage your financial aid funds.</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/rights.pcf">©</a>
      </div>
   </body>
</html>