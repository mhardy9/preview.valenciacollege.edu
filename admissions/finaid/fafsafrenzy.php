<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>FAFSA Frenzy | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/fafsafrenzy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>FAFSA Frenzy</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-9">
                        					
                        <h2>FAFSA Frenzy</h2>
                        					
                        <p><strong>Get expert help to complete your Free Application for Federal Student Aid so you can
                              get money for college!</strong></p>
                        
                        					
                        <table class="table table">
                           						
                           <tbody>
                              							
                              <tr>
                                 								
                                 <th>East Campus </th>
                                 								
                                 <th>West Campus </th>
                                 								
                                 <th>Osceola Campus </th>
                                 								
                                 <th>Poinciana Campus </th>
                                 							
                              </tr>
                              							
                              <tr>
                                 								
                                 <td>
                                    									
                                    <p>October 16, 2017<br> 9 a.m. to 6 p.m.
                                    </p>
                                    									
                                    <p> Atlas Lab, Building 5, Room 213 </p>
                                    									
                                    <ul class="list_style_1">
                                       										
                                       <li><a href="http://maps.google.com/maps?q=701+N+Econlockhatchee+Trail,+Orlando,+FL+32825&amp;iwloc=A&amp;hl=en" target="_blank">Driving Directions</a></li>
                                       										
                                       <li><a href="../map/east.html" target="_blank">Campus Map</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_east.ics">Save to Outlook</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_east.ics">Save to iCal </a></li>
                                       									
                                    </ul>
                                    
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p>October 16, 2017<br> 9 a.m. to 6 p.m.
                                    </p>
                                    									
                                    <p>Atlas Lab, Student Services Building (SSB), Room 172 </p>
                                    									
                                    <ul class="list_style_1">
                                       										
                                       <li><a href="http://maps.google.com/maps?q=1800+South+Kirkman+Road,+Orlando,+FL+32811&amp;iwloc=A&amp;hl=en" target="_blank">Driving Directions</a></li>
                                       										
                                       <li><a href="../map/west.html" target="_blank">Campus Map</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_west.ics">Save to Outlook</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_west.ics">Save to iCal </a></li>
                                       									
                                    </ul>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p>October 13, 2017<br> 9 a.m. to 6 p.m.
                                    </p>
                                    									
                                    <p>Learning Center, Building 3, Room 100 </p>
                                    									
                                    <ul class="list_style_1">
                                       										
                                       <li><a href="http://maps.google.com/maps?q=1800+Denn+John+Lane,+Kissimmee,+FL+34744&amp;iwloc=A&amp;hl=en" target="_blank">Driving Directions </a></li>
                                       										
                                       <li><a href="../map/osceola.html" target="_blank">Campus Map</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_osceola.ics">Save to Outlook</a></li>
                                       										
                                       <li><a href="http://events.valenciacollege.edu/event/fafsa_osceola.ics">Save to iCal </a></li>
                                       									
                                    </ul>
                                    								
                                 </td>
                                 								
                                 <td>
                                    									
                                    <p>November 8, 2017<br> 9 a.m. to 6 p.m.
                                    </p>
                                    									
                                    <p>Atlas Lab, Building 1, Room 102 </p>
                                    									
                                    <ul class="list_style_1">
                                       										
                                       <li><a href="https://goo.gl/maps/kBMpzS5iyXN2" target="_blank">Driving Directions </a></li>
                                       										
                                       <li><a href="../map/poinciana.html" target="_blank">Campus Map</a></li>
                                       									
                                    </ul>
                                    								
                                 </td>
                                 							
                              </tr>
                              						
                           </tbody>
                           					
                        </table>
                        
                        					
                        <p><a href="documents/2017-FAFSA-Frenzy-Postcard-BACK.PDF" target="_blank">Download the FAFSA Checklist</a></p>
                        					
                        <ul class="list_style_1">
                           						
                           <li><strong><a href="https://fafsa.ed.gov/fotw1819/pdf/fafsaws18c.pdf" target="_blank">FAFSA ON THE WEB WORKSHEET ENGLISH</a></strong></li>
                           						
                           <li><a href="https://fafsa.ed.gov/es_ES/fotw1819/pdf/PdfFafsa18-19.pdf" target="_blank"><strong>FAFSA ON THE WEB WORKSHEET SPANISH</strong></a></li>
                           					
                        </ul>
                        
                        					
                        <h3>What to do prior to arriving:</h3>
                        					
                        <ul>
                           						
                           <li><strong>If you already have an FSA ID the process will go faster. </strong></li>
                           					
                        </ul>
                        					
                        <h4>What you need to bring with you:</h4>
                        					
                        <ol>
                           						
                           <li>2016 Income Information <em>(taxable and untaxed income) </em></li>
                           						
                           <li>Social Security Card</li>
                           						
                           <li>Driver's License</li>
                           						
                           <li>Alien Registration Card <em>(eligible non-citizens)</em></li>
                           						
                           <li>Dependent students: Your parent(s) information to support lines 1 - 3</li>
                           						
                           <li>FSA ID. This ID replaced the PIN in May 2015</li>
                           					
                        </ol>
                        					
                        <blockquote>
                           						
                           <p><a href="http://fsaid.ed.gov/npas/" target="_blank">Help with completing the FAFSA and determining if you are a dependent.</a></p>
                           					
                        </blockquote>
                        					
                        <h4>Why Attend?</h4>
                        					
                        <ul>
                           						
                           <li>You must submit a FAFSA every year to receive financial aid.</li>
                           						
                           <li>Applying early every year can increase your financial aid award.</li>
                           						
                           <li>Valencia's financial aid experts will assist you with the process</li>
                           						
                           <li>Help is available for first-time applicants and those updating their information <em>(renewal).</em></li>
                           					
                        </ul>
                        					
                        <h4>Financial Aid Resources:</h4>
                        					
                        <ul>
                           						
                           <li><a href="https://www.youtube.com/watch?v=LK0bbu0y5AM&amp;index=1&amp;list=PL23B9A23CD8DD82DD" target="_blank">How To Fill Out the FAFSA </a></li>
                           						
                           <li><a href="http://studentaid.ed.gov/PORTALSWebApp/students/english/index.jsp" target="_blank">Student Aid on the Web </a></li>
                           						
                           <li><a href="http://www.floridastudentfinancialaid.org/ssfad/home/uamain.htm" target="_blank">Florida Student Financial Aid</a></li>
                           						
                           <li><a href="http://net4.valenciacollege.edu/promos/internal/financialaidvideos.cfm" target="_blank">Financial Aid TV - Got Questions? </a></li>
                           						
                           <li><a href="/admissions/finaid/" target="_blank">Valencia's Financial Aid Web </a></li>
                           						
                           <li><a href="/students/veterans-affairs/" target="_blank">Valencia's Office of Veteran's Affairs </a></li>
                           					
                        </ul>
                        				
                     </div>
                     				
                     <aside class="col-md-3">
                        					
                        <div>
                           						
                           <p>Valencia College Gainful Employment Programs are Financial Aid Eligible.</p>
                           						
                           <p><a href="/academics/programs/as-degree/gainful-employment/" target="_blank"> Gainful Employment Programs </a></p>
                           					
                        </div>
                        				
                     </aside>
                     			
                  </div>
                  		
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/fafsafrenzy.pcf">©</a>
      </div>
   </body>
</html>