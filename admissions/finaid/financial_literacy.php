<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Literacy  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/financial_literacy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Financial Literacy </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Financial Literacy</h2>
                        
                        
                        <h3>Mission</h3>
                        <img alt="" class="img-responsive pull-right" src="images/FlirtLogo.png">
                        
                        <p>To promote financial literacy among the student body through peer to peer learning.</p>
                        
                        
                        <h3>Vision</h3>
                        
                        <p>Empower Valencia students to improve their personal financial awareness; fostering
                           a life-long road to financial success. Commissioning Valencia College Cohort Default
                           Rate to continuously display a frequency of community, integrity, access, deep stewardship,
                           professional development, civic responsibility, and excellence.
                        </p>
                        
                        <p>Valencia College Financial Learning Ambassadors promote financial literacy and responsible
                           money management amongst their student peers by:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Workshops</li>
                           
                           <li>Classroom presentations</li>
                           
                           <li>Financial Aid initiatives </li>
                           
                           <li>Student Development events</li>
                           
                           <li>On campus partnership </li>
                           
                           <li>Community Outreach </li>
                           
                        </ul>
                        
                        
                        <h3>History</h3>
                        
                        <p> Valencia's Financial Learning Ambassador Program received a grant from USA Funds
                           in 2009 for 3 years, which helped the program develop and grow into what it is today.
                           Funding for this program was used to expand to all campuses and provide training from
                           USA Funds Life Skills. We expanded our current educational activities by conducting
                           April's Financial Literacy Week college wide. During these activities the Financial
                           Learning Ambassadors distributed information related to budgeting, managing finances
                           and planning for a healthy financial future, as well as engaged students through hosted
                           speakers. Valencia hosted a national acclaimed author Jackie Cummings Koski; "Money
                           Letters 2 My Daughter", winner of the 2013 Book of the Year by Adult money management
                           and awarded by Institute for Financial Literacy. Jackie engaged with students at Osceola,
                           East and West campus discussing various financial literacy topics. 
                        </p>
                        
                        
                        <blockquote>
                           I was honored to be the speaker for your 2014 Financial Literacy Week, and see first-hand
                           the dedication and enthusiasm you and your students have for the program. The financial
                           education of students is crucial in a time where student loan debt is the highest
                           it's been in history. You are absolutely doing your part and your program is a wonderful
                           example for other higher learning institutions.
                           <br>
                           <strong>— Jackie Cumming Koski</strong>
                           
                        </blockquote>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/financial_literacy.pcf">©</a>
      </div>
   </body>
</html>