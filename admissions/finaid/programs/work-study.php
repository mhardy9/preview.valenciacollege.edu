<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Work Study Program | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/work-study.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/programs/">Programs</a></li>
               <li>Work Study Program </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Work Study Program</h2>
                        
                        
                        <h3>Federal Work Study</h3>
                        
                        <p>This is a federal work program for students with financial need. You may be employed
                           on one of our campuses, or as a reading tutor in a local public school. As a work
                           study employee, you may work up to 20 hours per week and earn at least minimum wage,
                           up to the amount of your financial need. You may be offered work study as part of
                           your overall award; however, if it was not included, you may inquire about your eligibility
                           by visiting a campus Answer Center. 
                        </p>
                        
                        
                        <p><a href="https://valencia.studentemployment.ngwebsolutions.com/" target="_blank">View Valencia's Work Study Jobs and Website </a></p>
                        
                        
                        <h3>How will I receive a job assignment?</h3>
                        
                        <p>Work study awards are part of your overall financial aid package. Instructions to
                           apply for Work Study Jobs will be available on your Award Notice on Atlas. If selected
                           for a position, your supervisor will have you meet with a Financial Aid Services Specialist
                           to complete the remaining assignment documents. You will need to bring your Social
                           Security Card and proof of citizenship or eligibility to work in the U.S. with you
                           to your meeting so you can complete your payroll paperwork.
                        </p>
                        
                        
                        <h3>How will I receive payment for my Federal Work Study Award?</h3>
                        
                        <p>Federal Work Study is an opportunity to work and build work experience. You will be
                           paid by Valencia’s Payroll Department every two weeks based on the time sheet you
                           and your supervisor turn in to Financial Aid. You will receive a packet of information
                           about this process when your assignment documents are completed.
                        </p>
                        
                        
                        <h3>Institutional Work Study</h3>
                        
                        <p>Various campus departments and laboratories have funding to hire students as part–time
                           employees. You should inquire through the campus departments about any available openings.
                        </p>
                        
                        
                        <h3>Atlas Access Lab Assistants</h3>
                        
                        <p>Computer lab assistants who work in the Atlas Access Labs. For more information, contact
                           the Manager of the Atlas Access Lab.
                        </p>
                        
                        
                        <h3>Peer Advisors and Welcome Team</h3>
                        
                        <p>Student Services hires enrolled students as Peer Advisors and Welcome Team members.
                           For more information, contact your campus Student Development Coordinator.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/work-study.pcf">©</a>
      </div>
   </body>
</html>