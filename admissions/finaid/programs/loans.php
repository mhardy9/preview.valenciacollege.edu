<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Loans | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/loans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/programs/">Programs</a></li>
               <li>Loans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Loans</h2>
                        
                        <h3>Tuition Installment Plan (TIPS)</h3>
                        
                        <p>The college offers an option for students to defer a portion of their tuition and
                           fees past the payment deadline date. The payment option is managed by a private–for–profit
                           company called FACTS Tuition Management, Inc. The program is available to all students
                           registering for the current term. All registration fees assessed can be included in
                           your agreement with FACTS. No interest is charged, but there is an enrollment fee
                           each term. The payment schedule, payment amount, and enrollment fee is determined
                           when you sign up for this plan. Details or more information on this plan can be found
                           at the <a href="/STUDENTS/offices-services/business-office/">Business Office website</a>.
                        </p>
                        
                        
                        <h3>Federal Direct Stafford Student Loans</h3>
                        
                        <p>Valencia students are eligible to participate in the Federal Direct Loan Program.
                           There are three programs: the Subsidized Federal Stafford Loan, the Unsubsidized Federal
                           Stafford Loan, and the Parent Loan for Undergraduate Dependent Students (PLUS). Applicants
                           must first apply for grants by completing their FAFSA. Federal Direct Education Loans
                           are established and supported by the federal government and the Department of Education
                           serves as the lender. <strong>ALL FEDERAL STAFFORD EDUCATION LOANS MUST BE REPAID</strong>. To receive any loan, you must first apply for all types of federal financial aid
                           by completing the FAFSA. You must also enroll in at least 6 credit hours required
                           for your program each term. Please note: Valencia reserves the right to refuse to
                           certify additional student loans if there is any indication you may be unwilling to
                           repay your loan, if you have high existing loan balances or if you have ever defaulted
                           on a prior student loan that you did not repay in full. If any of these conditions
                           may apply to you, you should meet with a Valencia Financial Aid Services Specialist
                           for an in–person advising session about your loan status. 
                        </p>
                        
                        
                        <h4>Is there a time limit on how long I can receive loans?</h4>
                        
                        <p>If you are a first-time borrower on or after July 1, 2013, there is a limit on the
                           maximum period of time (measured in academic years) that you can receive Direct Subsidized
                           Loans. This time limit does not apply to Direct Unsubsidized Loans or Direct PLUS
                           Loans. If this limit applies to you, you may not receive Direct Subsidized Loans for
                           more than 150 percent of the published length of your program. This is called your
                           “maximum eligibility period.” Your maximum eligibility period is based on the published
                           length of your current program. You can usually find the published length of any program
                           of study in your school’s catalog.
                        </p>
                        
                        <p>For example, if you are enrolled in a four-year bachelor’s degree program, the maximum
                           period for which you can receive Direct Subsidized Loans is six years (150 percent
                           of 4 years = 6 years). If you are enrolled in a two-year<strong><em> associate degree </em></strong>program, the maximum period for which you can receive Direct Subsidized Loans is three
                           years (150 percent of 2 years = 3 years).
                        </p>
                        
                        <p>Because your maximum eligibility period is based on the length of your current program
                           of study, your maximum eligibility period can change if you change to a program that
                           has a different length. Also, if you receive Direct Subsidized Loans for one program
                           and then change to another program, the Direct Subsidized Loans you received for the
                           earlier program will generally count toward your new maximum eligibility period.
                        </p>
                        
                        <p>Certain types of enrollment may cause you to become responsible for the interest that
                           accrues on your Direct Subsidized Loans when the U.S. Department of Education usually
                           would have paid the interest. These enrollment patterns are described below. 
                        </p>
                        
                        <p><em>I become responsible for paying the interest that accrues on my Direct Subsidized
                              Loans, when:</em></p>
                        
                        <ul class="list_style_1">
                           
                           <li>I am no longer eligible for Direct Subsidized Loans and I stay enrolled in my current
                              program
                           </li>
                           
                           <li>I am no longer eligible for Direct Subsidized Loans, did not graduate from my prior
                              program, and am enrolled in an undergraduate program that is the same length or shorter
                              than my prior program
                           </li>
                           
                           <li>I transferred into the shorter program and lost eligibility for Direct Subsidized
                              Loans because I have received Direct Subsidized Loans for a period that equals or
                              exceeds my new, lower maximum eligibility period, which is based on the length of
                              the new program
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Subsidized Federal Direct Education Loan</h3>
                        
                        <p>This loan requires unmet financial need. The federal government will pay the interest
                           while you are enrolled in school, and you must repay the loan plus interest beginning
                           6 months after you leave school or drop to less than half–time status. You must be
                           enrolled in at least 6 credits per term. Students with less than 30 credits toward
                           their current program at the time of application may borrow up to $3,500 per year
                           or the amount of unmet financial need, whichever is less. Students with 30 credits
                           or more toward their current program at the time of application may borrow up to $4,500
                           per year or the amount of unmet financial need, whichever is less. 
                        </p>
                        
                        
                        <h3>Unsubsidized  Federal Direct Education Loan</h3>
                        
                        <p>This loan does NOT require financial need, however you must first establish eligibility
                           for need–based financial aid by completing your FAFSA. Interest will accrue on this
                           loan while you are in school. You must repay the loan, accrued interest and current
                           interest beginning 6 months after you leave school or drop to less than half–time
                           status. You must be enrolled in a minimum of 6 credits per term. Dependent students
                           with less than 30 credits toward their current program at the time of application
                           may borrow up to $5,500 per year in combined subsidized and unsubsidized loans, not
                           to exceed the cost of education less other financial aid. Students with 30 or more
                           credits toward their current program at the time of application may borrow up to $6,500
                           per year in combined subsidized and unsubsidized loans, not to exceed the cost of
                           education less other financial aid. Independent students and students whose parents
                           are denied a Parent PLUS Loan, may  borrow an additional $4,000, not to exceed the
                           cost of education less other financial aid. 
                        </p>
                        
                        
                        <h3>Parent Loan for Undergraduate Dependent Students (PLUS)</h3>
                        
                        <p>Parents of dependent students may borrow on behalf of their children. Income is not
                           a factor; however, if a borrower has adverse credit history, but qualifies for a PLUS
                           Loan through the process for reconsideration due to extenuating circumstances or by
                           obtaining an endorser for the loan, the completion of PLUS loan counseling is required
                           before disbursement of funds can take place. Students must first apply for all types
                           of financial aid by completing their FAFSA. Loan amounts must not exceed the cost
                           of education less other financial aid. Parent loans must be repaid over a 10–year
                           period with interest beginning 60 days after disbursement. The student will be allowed
                           to charge tuition and books against any authorized PLUS Loan funds.
                        </p>
                        
                        
                        <h3>Alternative Loans</h3>
                        
                        <p>There are a number of private loans which are available to credit-worthy students
                           and/or their co-borrowers. Students should first determine their eligibility under
                           the FFEL programs before seeking additional funding under this option. To apply for
                           a private loan you must contact the lender of your choice. Information on the eligibility
                           criteria and loan terms are available on each lender's website.
                        </p>
                        
                        
                        <h3>How do I apply for a Federal Student or Parent Loan?</h3>
                        
                        <p>You must first establish your eligibility for a Federal Pell Grant by completing all
                           application steps listed earlier in this publication. A separate Loan Request and
                           Promissory note is required. Instructions on applying for Federal Stafford Loan are
                           available <a href="../federal-direct-loans.html">here</a>.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/loans.pcf">©</a>
      </div>
   </body>
</html>