<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Grants | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/grants.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/programs/">Programs</a></li>
               <li>Grants </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Grants</h2>
                        
                        <p>Grants may be awarded if you show financial need based on the Free Application for
                           Federal Student Aid (FAFSA). The grants listed here are administered by Valencia based
                           on the guidelines set for each program by the provider.
                        </p>
                        
                        <h3>Federal Pell Grant</h3>
                        
                        <p>This is a need–based federal grant program for students who are seeking their first
                           undergraduate degree and whose Estimated Family Contribution (EFC) ranges from 0 to
                           5,234. The annual award amount is determined by your EFC and enrollment status. The
                           maximum scheduled award is $5,920.
                        </p>
                        
                        <p>Students who establish eligibility will be funded. All documents needed to establish
                           eligibility must be received prior to your last day of enrollment, or June 30 of the
                           award year, whichever comes first. Initial awards are estimated based on the results
                           of the FAFSA and are based on a full time enrollment calculation. Estimates will change
                           if any criteria used to determine your eligibility, such as major, degree status,
                           satisfactory progress, and income or family information changes. <strong>You may receive coverage only for courses that are required for your program of study
                              and a maximum of 30 credit hours of required college–preparatory coursework.</strong> Actual payment will not be determined until after the 100% refund deadline and class
                           attendance has been confirmed. Students registered for Flex Start classes will not
                           receive pell grant payments until their last class has begun. Flex Start classes added
                           to schedule after pell payment is made will not be considered for financial aid payment.
                           The amount actually paid to you from the Federal Pell Grant will be prorated if you
                           enroll for less than 12 credit hours in a given term or if you take courses outside
                           your program of study. You must begin attending all of your classes on the first day.
                           If you are withdrawn for non–attendance prior to the actual payment of Pell Grant
                           funds, your eligibility could be cancelled.
                        </p>
                        
                        <h3>Federal Supplemental Educational Opportunity Grant (FSEOG)</h3>
                        
                        <p>This is a need–based federal grant awarded to undergraduates seeking a first undergraduate
                           degree in an eligible program who have exceptional financial need. The award amount
                           at Valencia varies by enrollment and allocation. Priority is given to students with
                           an Estimated Family Contribution (EFC) of "0" who apply for financial aid early. Limited
                           funding is available in this program and funding is awarded on a first come–first
                           served basis. Recipients are chosen based on EFC and application date, and funds are
                           usually not available to late applicants. Initial awards are estimated based on the
                           results of the FAFSA and assume a full time enrollment. Estimates may change if any
                           criteria used to determine your eligibility, such as major, degree status, satisfactory
                           progress, and income or family contribution changes. Actual payment will not be determined
                           until after the add/drop period has ended and classes have begun. The amount paid
                           to you will be the amount awarded, each student needs to be in 6 compliant credits
                           to be eligible for these funds. You must attend all of your classes on the first day
                           of each term. If you are withdrawn for non–attendance prior to the actual payment
                           of FSEOG funds, payment for the withdrawn class(es) will not be made.
                        </p>
                        
                        <h3>Florida Student Assistance Grant (FSAG)</h3>
                        
                        <p>FSAG is a need–based program for students who meet Florida residency requirements.
                           To qualify, you must be seeking your first undergraduate degree, be enrolled in an
                           AA or AS degree program, must also qualify for a Federal Pell Grant and must enroll
                           for a minimum of 6 credit hours. The award amount at Valencia varies up to a maximum
                           of $2608 per year. Priority is given to qualified renewal students and full–time students
                           with high unmet financial need that apply for financial aid early. Limited funding
                           is available in this program and funding is awarded on a first come–first served basis.
                           Recipients are chosen based on EFC and application date, and funds are usually not
                           available to late applicants. Initial awards are estimated based on the results of
                           the FAFSA. Estimates will change if any criteria used to determine your eligibility,
                           such as residency, major, degree status, satisfactory progress, income or family information
                           changes. Actual payment will not be determined until after the add/drop period has
                           ended and classes have begun. The amount actually paid to you from the FSAG will be
                           prorated if you enroll for less than 12 credit hours in a given term. You must attend
                           all of your classes on the first day of each term. If you are withdrawn for non–attendance
                           prior to the actual payment of FSAG funds, payment for the withdrawn class(es) will
                           not be made. FSAG is available for fall and spring terms only. For additional details
                           and regulations, visit <a href="http://www.floridastudentfinancialaid.org" target="_blank">floridastudentfinancialaid.org</a>.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/grants.pcf">©</a>
      </div>
   </body>
</html>