<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Scholarships | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/programs/">Programs</a></li>
               <li>Scholarships </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Scholarships</h2>
                        
                        <p>Scholarships are awarded primarily based on financial need; however, some scholarship
                           programs may not require need. Other factors are also considered, which may include
                           academic talent, performing talent, participation in certain activities or special
                           interests. Scholarship requirements and application procedures vary depending upon
                           the criteria set by the scholarship provider. Many scholarships require a separate
                           application that must be obtained directly from the scholarship provider. <a href="/foundation/alumni/scholarships.php">The Alumni Association</a> offers more information about individual scholarships and downloadable application
                           forms.
                        </p>
                        
                        
                        <h3>Valencia Honors Program</h3>
                        
                        <p>The James M. and Dayle L. Seneff Honors College awards a limited number of full tuition,
                           non-transferable scholarships to admission candidates who demonstrate academic promise
                           and who commit to one of the four available curriculum tracks: Interdisciplinary Studies
                           track, Leadership track, Undergraduate Research track, or the Jeffersonian general
                           education track. All students who apply for the James M. and Dayle L. Seneff Honors
                           College will be considered for scholarship awards. Awards are renewable for up to
                           four terms.
                        </p>
                        
                        
                        <h4>Initial Eligibility: </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Students must apply and be admitted into the James M. and Dayle L. Seneff Honors College.</li>
                           
                           <li>Students must complete a <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> so that the results are received prior to the start of the term of admission.
                           </li>
                           
                           <li>Scholarship recipients must be degree seeking students at Valencia College.</li>
                           
                           <li>Scholarship recipients must register for at least nine credits.</li>
                           
                           <li>Scholarship recipients must be registered for at least one honors class.</li>
                           
                        </ul>
                        
                        
                        <h4>Renewal Eligibility: </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Scholarships are renewed on a term-by-term basis.</li>
                           
                           <li>Students must make good progress towards graduation in a curricular track as determined
                              by the Honors Director.
                           </li>
                           
                           <li>Students must complete a <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> so that the results are received prior to the start of the term of admission.
                           </li>
                           
                           <li>Scholarship recipients must be degree-seeking students at Valencia College.</li>
                           
                           <li>Scholarship recipients must register for at least nine credits each term.</li>
                           
                           <li>Scholarship students must complete at least 75% of attempted credits to be renewed
                              in subsequent terms.
                           </li>
                           
                        </ul>
                        
                        <p>All students receiving any form of financial aid must meet the standards of progress
                           as outlined by the federal government. See <a href="../satisfactory-progress.php"><em>valenciacollege.edu/finaid/satisfactory-progress.cfm</em></a> for Satisfactory Academic Progress criteria.
                        </p>
                        
                        
                        <h3>Valencia Advantage Scholarship</h3>
                        
                        <p>Valencia Advantage Scholarship recipients
                           are selected by Financial Aid Services based on the following criteria:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Submit a <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a>. Processed results must show the applicant to have unmet financial need.
                           </li>
                           
                           <li>Seek an AA or AS Degree Program.</li>
                           
                           <li>Selection priority shall be for students who qualify for limited or no other source
                              of grant or scholarship funding.
                           </li>
                           
                           <li>Scholarship amounts may vary based on the student’s demonstrated financial need.</li>
                           
                           <li>To receive funding, the recipient must meet Satisfactory Academic Progress Requirements
                              for financial aid recipients as described in the Valencia Catalog and enroll in at
                              least 6 credit hours each term.
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Performing Arts Scholarships</h3>
                        
                        <p>Scholarships will be available to students engaged in performing 
                           arts activities including, but not limited to, art, drama, and music. 
                           The department dean or program coordinator will forward selection 
                           recommendations to the Director of Financial Aid Services for final 
                           approval and awarding. Recipients must complete a <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> prior to receiving funding.
                        </p>
                        
                        
                        <h3>Bridges to Success</h3>
                        
                        <p>This scholarship is awarded to students who participate in the Bridges to Success
                           Program. The Bridges to Success Program is available to first generation in college
                           and socio-economically disadvantaged high school graduates from Orange or Osceola
                           County high schools. Special consideration is given to students who have participated
                           in one of the following Valencia College’s pre–collegiate programs: 
                        </p>
                        
                        <p> Community Partnerships,  Upward Bound, McKnight Achievers, POPS, Parramore Kids Zone
                           (PKZ), or AVID. The scholarship will pay for in–state tuition and fees, plus $150
                           (summer) and $300 (Fall, Spring) per semester for required books and supplies after
                           other financial aid is considered.
                        </p>
                        
                        <h4>Initial Eligibility:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Apply and be accepted into the Bridges to Success program.</li>
                           
                           <li>Complete a <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> so that results are processed by the Bridges to Success program application deadline.
                              Must be completed for Summer, and Fall aid years.
                           </li>
                           
                           <li>Meet standards of satisfactory academic progress for financial aid recipients.</li>
                           
                           <li>Seek an AA or AS Degree Program. </li>
                           
                           <li>Enroll in at least 7-8 credit hours during the first summer term (Term B) after high
                              school.
                           </li>
                           
                        </ul>
                        
                        <h4>Renewal Eligibility:</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Scholarship is renewed if student remains in compliance with the BTS program guidelines.
                              
                           </li>
                           
                           <li>Active participation in Bridges to Success activities and the recommendation of the
                              Program Director will be required.
                           </li>
                           
                           <li>Completed/Processed <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> by the financial aid deadline.
                           </li>
                           
                           <li>Enroll in at least 9 credit hours each term.</li>
                           
                           <li>Maintain a 2.75 GPA. </li>
                           
                           <li>Complete at least 75% of attempted courses. </li>
                           
                        </ul>            
                        
                        
                        <h3>Valencia Grant</h3>
                        
                        <p>This grant is awarded by the Director of Financial Aid Services to provide direct
                           educational expenses for students with unusual financial circumstances based on the
                           <a href="https://fafsa.ed.gov/" target="_blank">Free Application for Federal Student Aid (FAFSA)</a> and other documentation on file.
                        </p>
                        
                        
                        
                        <h3>Alumni Two Plus Two Scholarship - INCOMING</h3>
                        
                        <p>This scholarship is funded by the Alumni Associations of Valencia College and University
                           of Central Florida. This award is made each year to a single high school graduate
                           selected from nominations made by each high school principal in Orange and Osceola
                           counties. The recipient may receive up to $4,000 ($1,000 each year for 4 years.) in
                           scholarship funds during the pursuit of an Associate’s degree at Valencia followed
                           by a Bachelor’s degree at University of Central Florida.
                        </p>
                        
                        
                        <h3>Alumni Two Plus Two Scholarship - GRADUATING</h3>
                        
                        <p>This scholarship is funded by the Alumni Associations of Valencia College and University
                           of Central Florida. This award is made each year to Valencia College students transferring
                           to UCF. The recipients may receive up to $4,000 in scholarship funds during the pursuit
                           of an Associate’s degree at Valencia followed by a Bachelor’s degree at University
                           of Central Florida. A $2,000 check is given to the student in the spring, plus $1,000
                           goes into the student's account at the University of Central Florida each year for
                           two years.
                        </p>
                        
                        
                        <h3>Valencia Student Government Association (SGA) and Leadership Scholarships</h3>
                        
                        <p>Campus leaders who participate in Student Government or serve on the Valencia Welcome
                           Team may be considered for a scholarship. Visit the Student Government Office on your
                           campus. 
                        </p>
                        
                        
                        <h3>Valencia Foundation Scholarships</h3>
                        
                        <p>The Valencia Foundation offers privately funded scholarships to help students achieve
                           their dreams of attaining a college education. Many students are working to build
                           better lives for themselves and their families, and the foundation’s donors are committed
                           to help make a difference for our students. While many donors prefer to award scholarships
                           through their own application processes, some have asked Valencia’s scholarship committee
                           to screen and select the recipients. You may apply for scholarships that the scholarship
                           committee selects using a single foundation scholarship application. The foundation
                           scholarship application can be found <a href="https://valencia.scholarships.ngwebsolutions.com/CMXAdmin/Cmx_Content.aspx?cpId=466">HERE</a>.
                        </p>
                        
                        <p>Using this one form, you may also be selected as a candidate for scholarships that
                           are chosen by our donors. Further information on specific scholarships may be found
                           at <a href="http://www.valencia.org/">valencia.org</a>.
                        </p>
                        
                        <p>The scholarships awarded through the foundation are highly competitive. In fact, hundreds
                           of applications are received each year but we encourage every student to take advantage
                           of the opportunity to compete. The scholarship committee will give priority to criteria
                           specific to each scholarship. Merit and financial need are very often priorities,
                           but this is not always the case. Other criteria may include, but are not limited to,
                           a major or course of study, residency in a certain county, graduation from a particular
                           high school, or employment with a specific company. Our  foundation application does
                           not have a set submission deadline as we accept them throughout the academic year.
                        </p>
                        
                        <p>New scholarships are donated throughout the year and may also be available. For more
                           information on the foundation and its scholarships, please contact the foundation
                           at (407) 582–3154. You may also e–mail your questions to <a href="mailto:foundation@valenciacollege.edu">foundation@valenciacollege.edu</a>. 
                        </p>
                        
                        
                        <h3>Florida Bright Futures Scholarship Program</h3>
                        
                        <p>As of 2009-2010 - If you withdraw or drop a class anytime after the add drop period
                           you must repay the Bright Futures funding received for that class. 
                        </p>
                        
                        <p> This is a state scholarship awarded to Florida high school graduates who demonstrate
                           high academic achievement. Awards are issued by the State of Florida directly to the
                           eligible student and the amounts are based on statutory limits set by the state legislature
                           and can be viewed at <a href="http://www.floridastudentfinancialaid.org/" target="_blank">floridastudentfinancialaid.org</a>. The program has three levels. You may receive only one of these: 
                        </p>
                        
                        <ol>
                           
                           <li>
                              <strong>Florida Academic Scholars Award</strong> requires 3.0 cumulative GPA to renew; or 
                           </li>
                           
                           <li>
                              <strong>Florida Medallion Scholarship</strong> requires 2.75 cumulative GPA to renew; or
                           </li>
                           
                           <li>
                              <strong>Florida Gold Seal Vocational Scholars Award</strong> requires 2.75 cumulative GPA to renew.
                           </li>
                           
                        </ol>
                        
                        <p> Renewal requirements varies depending on the number of credit hours funded. Each
                           award level has different academic criteria for eligibility. 
                        </p>
                        
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td></td>
                                 
                                 <th scope="col">Florida Academic Scholars (FAS) Award </th>
                                 
                                 <th scope="col">Florida Medallion Scholars (FMS) Award</th>
                                 
                                 <th scope="col">Florida Gold Seal Vocational Scholars (GSV) Award</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Minimum Cumulative GPA (unrounded and unweighted)</th>
                                 
                                 <td>3.0</td>
                                 
                                 <td>2.75</td>
                                 
                                 <td>2.75</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Minimum Hours required per Term IF Funded <em>Full-Time</em> (12+ Hours) 
                                 </th>
                                 
                                 <td colspan="3">12 semester hours (earned)* </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Minimum Hours required per Term IF Funded <em>Three-Quarter-Time</em> (9-11 Hours) 
                                 </th>
                                 
                                 <td colspan="3">9 semester hours (earned)* </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Minimum Hours required per Term IF Funded <em>Half-Time</em> (6-8 Hours) 
                                 </th>
                                 
                                 <td colspan="3">6 semester hours (earned)* </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p>* Or equivalent in quarter or clock hours.</p>
                        
                        <p><strong><a href="http://www.floridastudentfinancialaid.org/SSFAD/PDF/BFHandbookChapter3.pdf" target="_blank">http://www.floridastudentfinancialaid.org/SSFAD/PDF/BFHandbookChapter3.pdf</a> </strong>(see page 3) 
                        </p>
                        
                        <p>The top–ranked scholar from each county will receive an additional award of up to
                           $1,500. Applications and eligibility criteria are available from your high school’s
                           guidance office or from the Florida Department of Education web site: <a href="http://www.floridastudentfinancialaid.org/" target="_blank">floridastudentfinancialaid.org</a>. Valencia receives electronic information about eligible Valencia scholars from the
                           Florida Department of Education. You must designate Valencia as the school you plan
                           to attend and must enroll for at least 6 hours of college–level coursework each term.
                           Initial award amounts are estimated for purposes of Valencia’s financial aid award
                           planning.
                        </p>
                        
                        <p>Actual payment depends upon the program regulations and the amount of tuition and
                           fees assessed for the term. The scholarships will cover the designated amount of in–state
                           college–level tuition and fees for up to 45 credit hours of coursework each year.
                           Your financial aid award will reflect an estimate of the maximum amount that would
                           be covered if you used all 45 credit hours in the year. Payment will be based on your
                           actual tuition charges. Bright Futures awards are available for fall and spring terms
                           only. For additional details about the Bright Futures program, the set amounts, and
                           program regulations, visit <a href="http://www.floridastudentfinancialaid.org/" target="_blank">floridastudentfinancialaid.org</a>.
                        </p> 
                        <p><a href="http://www.floridastudentfinancialaid.org/SSFAD/home/uamain.htm" target="_blank">Bright Futures Frequently Asked Questions</a> 
                        </p>
                        
                        
                        <h3>Other Florida Scholarships offered by the Florida Department of Education</h3>
                        
                        <p>The Florida Department of Education offers a number of scholarships for Florida residents.
                           Please consult the Florida Department of Education web site: <a href="http://www.floridastudentfinancialaid.org/" target="_blank">floridastudentfinancialaid.org</a>.
                        </p>
                        
                        
                        <h3>Outside Private Scholarships</h3>
                        
                        <p>Many clubs and organizations offer scholarships to college students. Students can
                           obtain information about these awards from high school guidance offices, libraries,
                           or from the Internet. For a list of <strong>free </strong>Internet scholarship search services visit our <a href="../scholarship-bulletin-board.php#ScholarshipSearch%23ScholarshipSearch">Scholarship Bulletin Board.</a></p>
                        
                        <p>If you or your family are members of a social, civic, ethnic, religious, professional
                           or service group, you should contact them. They often have educational assistance
                           programs that are not advertised. Also, employers often have educational assistance
                           plans for employees and/or dependents.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/scholarships.pcf">©</a>
      </div>
   </body>
</html>