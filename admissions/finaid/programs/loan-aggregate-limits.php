<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Loan Aggregate Limits  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/loan-aggregate-limits.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/programs/">Programs</a></li>
               <li>Loan Aggregate Limits </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Loan Aggregate Limits</h2>
                        
                        
                        <h3>How much can I borrow?</h3>
                        
                        
                        <table class="table table">
                           
                           <caption>Maximum annual and lifetime aggregate limits chart - Subsidized and Unsubsidized Direct
                              and FFEL Stafford Loans
                           </caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> College Year </th>
                                 
                                 <th scope="col"> Dependent Students </th>
                                 
                                 <th scope="col"> Independent Students and Dependent Students whose Parents are denied PLUS Loans </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Freshman - Annual Limits </th>
                                 
                                 <td> $5500 - No more than $3500 can be in Subsidized Loans </td>
                                 
                                 <td> $9500 - No more than $3500 can be in Subsidized Loans </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Sophomore - Annual Limits </th>
                                 
                                 <td> $6500 - No more than $4500 can be in Subsidized Loans </td>
                                 
                                 <td> $10,500 - No more than $4500 can be in Subsidized Loans </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Junior/Senior - Annual Limits </th>
                                 
                                 <td> $7500 - No more than $5500 can be in Subsidized Loans </td>
                                 
                                 <td> $12,500 - No more than $5500 can be in Subsidized Loans </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Maximum Lifetime Limits </th>
                                 
                                 <td> $31,000 - No more than $23,000 can be in Subsidized Loans </td>
                                 
                                 <td> $57,500 - No more than $23,000 can be in Subsidized Loans </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <ul class="list_style_1">
                           
                           <li>You must have earned 30 college level credit hours, which count toward your current
                              degree program, to be eligible for Sophomore level loans. If you are earning an Associates
                              Degree you can not be considered anything higher than a sophomore at Valencia. 
                           </li>
                           
                           <li>If you have been accepted into a Bachelors Degree at Valencia you will be eligible
                              for Junior limit loans once you have completed 60 college level credit hours toward
                              your Bachelors Degree. 
                           </li>
                           
                           <li>The amounts in the chart above are the maximum amounts that you may borrow for an
                              academic year. You might receive less than the maximum if you receive other financial
                              aid that's used to cover a portion of your cost of attendance. 
                           </li>
                           
                           <li>Borrow responsibly as the aggregate limits are for your entire undergraduate career
                              up to a bachelor's degree. 
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/loan-aggregate-limits.pcf">©</a>
      </div>
   </body>
</html>