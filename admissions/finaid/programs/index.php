<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Aid Programs | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/programs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Financial Aid Programs Available</h2>
                        
                        <p>Valencia College offers a variety of programs geared towards Financial Aid assistance.
                           Please review the links to the left for more informative and important information.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><strong><a href="grants.php">Grants</a></strong></li>
                           
                           <li><strong><a href="scholarships.php">Scholarships</a></strong></li>
                           
                           <li><strong><a href="loans.php">Loans</a></strong></li>
                           
                           <li><strong><a href="work-study.php">Work Study Programs</a></strong></li>
                           
                        </ul>
                        
                        
                        <table class="table table">
                           
                           <caption>Grants</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> Fund Type </th>
                                 
                                 <th scope="col"> Eligibility </th>
                                 
                                 <th scope="col"> Enrollment Requirements </th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <th scope="row"> Pell Grant </th>
                                 
                                 <td> A need-based federal program for students seeking their first undergraduate degree.
                                    Students must have an Expected Family Contribution (EFC) number that falls in the
                                    range of Pell eligibility. The EFC is determined by the <a href="https://fafsa.ed.gov/" target="_blank">FAFSA</a>. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li> The enrollment requirement varies depending on the EFC number. </li>
                                       
                                       <li>Pell is awarded on a <a href="../index.php#PELLCALC">graduated scale</a> based on EFC and enrollment.
                                       </li>
                                       
                                       <li>Only pays for classes in current degree program. </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Supplemental Educational Opportunity Grant (SEOG) </th>
                                 
                                 <td> A need-based federal grant for students seeking their first undergraduate degree
                                    in an eligible program who have exceptional financial need. Priority is given to students
                                    with a 0 (zero) EFC who apply for the <a href="https://fafsa.ed.gov/" target="_blank">FAFSA</a> early. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime) in current degree program.
                                          
                                       </li>
                                       
                                       <li>Award amount varies depending on federal allocation for aid year. </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Florida Student Assistance Grant (FSAG) </th>
                                 
                                 <td> A need-based state program for students who meet Florida residency requirements and
                                    seeking their first undergraduate degree. Students must also qualify for the Pell
                                    Grant. Priority is given to renewal students and full-time students with high unmet
                                    financial need and who applied early for the <a href="https://fafsa.ed.gov/" target="_blank">FAFSA</a>. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime) in current degree program.
                                          
                                       </li>
                                       
                                       <li>Award amount adjusts based on enrollment.</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <table class="table table">
                           
                           <caption>Loans</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> Fund Type </th>
                                 
                                 <th scope="col"> Eligibility </th>
                                 
                                 <th scope="col"> Enrollment Requirements </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Subsidized Stafford Loan </th>
                                 
                                 <td>
                                    
                                    <p>A loan provided by the federal government on which interest does not accrue while
                                       a student is enrolled at least halftime. The student must demonstrate unmet financial
                                       need as determined by subtracting the student’s EFC from the cost of attendance.
                                    </p>
                                    
                                    <p>*If a student who is considered a New Borrower (defined as a student borrower who
                                       currently has no outstanding loan balances) has attempted credits totaling 150% or
                                       more of their program length, any new loans disbursed on or after July 1, 2013 for
                                       these students will be an unsubsidized loan only. Once a new borrower reaches the
                                       150% limitation, eligibility for the interest subsidy ends for all Direct Loans that
                                       are disbursed on or after July 1, 2013.
                                    </p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime) in current degree program.
                                          
                                       </li>
                                       
                                       <li>The award amount may vary depending on the student’s unmet need and cost of attendance
                                          as determined by their enrollment. 
                                       </li>
                                       
                                       <li>The loan amount is also determined by the student’s earned college credit hours. </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Unsubsidized Stafford Loan </th>
                                 
                                 <td> A loan provided by the federal government. Interest does accrue while the student
                                    is enrolled. The student is not required to have unmet need to be offered this loan
                                    and any EFC can qualify. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime) in current degree program.
                                          
                                       </li>
                                       
                                       <li>The award amount may vary depending on the student’s cost of attendance as determined
                                          by their enrollment.
                                       </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Parent PLUS Loan </th>
                                 
                                 <td> Parent-initiated loan on behalf of the student. The parent applies for the federal
                                    PLUS loan and approval is based on credit history. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime). </li>
                                       
                                       <li>The award amount is based on the student’s cost of attendance as well as any other
                                          aid the student is receiving. 
                                       </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <table class="table table">
                           
                           <caption>Scholarships</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> Fund Type </th>
                                 
                                 <th scope="col"> Eligibility </th>
                                 
                                 <th scope="col"> Enrollment Requirements </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Bright Futures </th>
                                 
                                 <td> A merit-based scholarship through the state of Florida. A student is determined to
                                    be eligible based on high school GPA and must meet specific state requirements for
                                    awarding and renewal each year. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime). </li>
                                       
                                       <li>The amount adjusts based on the number of enrolled hours. The scholarship pays at
                                          a specific per credit-hour rate for each fund type.
                                       </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Valencia Honors Scholarship </th>
                                 
                                 <td> Merit-based scholarship for students admitted into the James M. and Dayle L. Seneff
                                    Honors College. Students must meet specific scholarship criteria. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 9 credit hours (three-quarter time).</li>
                                       
                                       <li>Must be registered for at least one honors class. </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Valencia Advantage Scholarship </th>
                                 
                                 <td> A need-based scholarship for students who demonstrate unmet financial need and who
                                    qualify for limited or no other source of grant or scholarship funding. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 6 credit hours (halftime). </li>
                                       
                                       <li>Award amounts vary depending on demonstrated financial need.</li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Bridges to Success Scholarship </th>
                                 
                                 <td> Scholarship is awarded to students admitted into the Bridges to Success program which
                                    focuses on first generation college students and socio-economically disadvantaged
                                    high school graduates from Orange or Osceola County high schools. Students must meet
                                    specific scholarship criteria. 
                                 </td>
                                 
                                 <td>
                                    
                                    <ul>
                                       
                                       <li>Must be enrolled in a minimum of 7-8 credit hours (halftime) in their initial Summer
                                          term after high school. 
                                       </li>
                                       
                                       <li>Must be enrolled in a minimum of 9 credit hours (three-quarter time) in all subsequent
                                          terms in the scholarship program unless given approval by an Academic Advisor to enroll
                                          in less than 9 credit hours. 
                                       </li>
                                       
                                    </ul>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Valencia Foundation Scholarships </th>
                                 
                                 <td> Foundation scholarships may be need or merit-based depending on scholarship criteria.
                                    
                                 </td>
                                 
                                 <td> 
                                    
                                    <ul>
                                       
                                       <li>Please review your scholarship award letter for minimum enrollment requirements.</li>
                                       
                                    </ul> 
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/programs/index.pcf">©</a>
      </div>
   </body>
</html>