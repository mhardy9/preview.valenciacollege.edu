<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Aid Services  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/federal-direct-loans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Financial Aid Services </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               <h2>Applying for Federal Direct Stafford Student Loans</h2>
               
               <h3>Quick Links</h3>
               
               <table class="table table">
                  
                  <tbody>
                     
                     <tr>
                        
                        <td>
                           
                           <p><a href="#PLUSLoan">Applying for a Parent Loan</a></p>
                           
                        </td>
                        
                        <td>
                           
                           <p><a href="#Helpfullinks">Direct Loan Helpful Links </a></p>
                           
                        </td>
                        
                        <td>
                           
                           <p><a href="#Privateloans">Applying for a Private Loan</a></p>
                           
                        </td>
                        
                        <td><a href="/admissions/finaid/forms-worksheets.html" target="_blank">Loan Reinstatment Form </a></td>
                        
                        <td>
                           
                           <p><a href="/admissions/about/general-counsel/policy/documents/Volume3E/3E-06-Student-Loan-Code-of-Conduct.pdf" target="_blank">College's Loan Code of Conduct </a></p>
                           
                        </td>
                        
                        <td><a href="https://studentaid.ed.gov/sa/repay-loans/understand/plans" target="_blank">Direct Loan Repayment Estimator </a></td>
                        
                     </tr>
                     
                  </tbody>
                  
               </table>
               
               <h3>Important Information About Direct Student Loans</h3>
               
               <p>(You must be enrolled in 6 credit hours which count toward your degree at Valencia)</p>
               
               <p>The interest rate for new subsidized and unsubsidized loans first disbursed on or
                  after July 1, 2017 and before July 1, 2018, is a fixed 5.31%, with the exception of
                  the subsidized and unsubsidized undergraduate loans noted in the table below. The
                  interest for PLUS loans first disbursed on or after July 1, 2017 and before July 1,
                  2018, is a fixed 7.00%
               </p>
               
               <table class="table table">
                  
                  <tbody>
                     
                     <tr>
                        
                        <th scope="col">Date of First Disbursement</th>
                        
                        <th scope="col">Interest Rate for Subsidized and Unsubsidized Undergraduate Loan</th>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2008 - 06/30/2009</td>
                        
                        <td>6.00%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2009 - 06/30/2010</td>
                        
                        <td>5.60%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2010 - 06/30/2011</td>
                        
                        <td>4.50%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2011 - 06/30/2013</td>
                        
                        <td>3.40%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2013 - 06/30/2014</td>
                        
                        <td>3.86%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2014 - 06/30/2015</td>
                        
                        <td>4.66%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2015 - 06/30/2016</td>
                        
                        <td>4.29%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>07/01/2016 - 06/30/2017</td>
                        
                        <td>3.76%</td>
                        
                     </tr>
                     
                     <tr>
                        
                        <td>
                           
                           <div>07/01/2017 – 06/302018</div>
                           
                        </td>
                        
                        <td>4.45%</td>
                        
                     </tr>
                     
                  </tbody>
                  
               </table>
               
               <h4>Is there a time limit on how long I can receive loans?</h4>
               
               <p>If you are a first-time borrower on or after July 1, 2013, there is a limit on the
                  maximum period of time (measured in academic years) that you can receive Direct Subsidized
                  Loans. This time limit does not apply to Direct Unsubsidized Loans or Direct PLUS
                  Loans. If this limit applies to you, you may not receive Direct Subsidized Loans for
                  more than 150 percent of the published length of your program. This is called your
                  “maximum eligibility period.” Your maximum eligibility period is based on the published
                  length of your current program. You can usually find the published length of any program
                  of study in your school’s catalog.
               </p>
               
               <p>For example, if you are enrolled in a four-year bachelor’s degree program, the maximum
                  period for which you can receive Direct Subsidized Loans is six years (150 percent
                  of 4 years = 6 years). If you are enrolled in a two-year<strong><em> associate degree </em></strong>program, the maximum period for which you can receive Direct Subsidized Loans is three
                  years (150 percent of 2 years = 3 years).
               </p>
               
               <p>Because your maximum eligibility period is based on the length of your current program
                  of study, your maximum eligibility period can change if you change to a program that
                  has a different length. Also, if you receive Direct Subsidized Loans for one program
                  and then change to another program, the Direct Subsidized Loans you received for the
                  earlier program will generally count toward your new maximum eligibility period.
               </p>
               
               <p>Certain types of enrollment may cause you to become responsible for the interest that
                  accrues on your Direct Subsidized Loans when the U.S. Department of Education usually
                  would have paid the interest. These enrollment patterns are described below.
               </p>
               
               <p><em>I become responsible for paying the interest that accrues on my Direct Subsidized
                     Loans, when:</em></p>
               
               <ul class="list_style_1">
                  
                  <li>I am no longer eligible for Direct Subsidized Loans and I stay enrolled in my current
                     program
                  </li>
                  
                  <li>I am no longer eligible for Direct Subsidized Loans, did not graduate from my prior
                     program, and am enrolled in an undergraduate program that is the same length or shorter
                     than my prior program
                  </li>
                  
                  <li>I transferred into the shorter program and lost eligibility for Direct Subsidized
                     Loans because I have received Direct Subsidized Loans for a period that equals or
                     exceeds my new, lower maximum eligibility period, which is based on the length of
                     the new program
                  </li>
                  
               </ul>
               
               <h3>Federal Direct <em>Subsidized</em> Stafford Loan
               </h3>
               
               <p>A Federal Direct Subsidized Loan is available to help meet financial need after other
                  resources are subtracted or to the annual maximum loan limit, whichever is lower.
                  It is the U.S. Department of Education's major form of self-help aid and is available
                  through the William D. Ford Federal Direct Loan Program. <a href="/admissions/finaid/programs/loan-aggregate-limits.html">Loan Limits</a></p>
               
               <p><em>Note:</em> New law has eliminated the interest subsidy provided during the six-month grace period
                  for subsidized loans for which the first disbursement is made on or after July 1,
                  2012, and before July 1, 2014. If you receive a subsidized loan during this timeframe,
                  you will be responsible for the interest that accrues while your loan is in the grace
                  period. This provision does not eliminate the interest subsidy while the borrower
                  is in school or during eligible periods of deferment.
               </p>
               
               <h3>Federal Direct <em>Unsubsidized</em> Stafford Loans
               </h3>
               
               <p>A Federal Direct Unsubsidized Loan is not based on your financial need. It is available
                  to you through the William D. Ford Federal Direct Loan Program, if your estimated
                  Cost of Attendance (COA) is greater than your financial aid and you have not reached
                  your annual maximum loan limit through the Federal Direct Subsidized Loan. The interest
                  rate for Federal Direct Unsubsidized loans disbursed on or after July 1, 2017 and
                  before July 1, 2018 is fixed at 5.31%. You are charged interest on this loan from
                  the time the loan is disbursed until it is paid in full. If the interest is allowed
                  to accumulate, the interest will be added to the principal amount of the loan and
                  increase the amount to be repaid. <a href="/admissions/finaid/programs/loan-aggregate-limits.html">Loan Limits </a></p>
               
               <h4>Fees</h4>
               
               <p>By law, the total origination fee for loans made on or after October 1, 2017 is 1.069%
                  for Direct Subsidized and Unsubsidized Stafford Loans. For more information on interest
                  rates go to the <a href="https://studentaid.ed.gov/sa/types/loans/interest-rates" target="_blank">Federal Student Aid</a> site.
               </p>
               
               <h4>Steps to Apply for a Federal Student Loan at Valencia</h4>
               
               <ol>
                  
                  <li><strong>Apply for a Federal Direct Stafford (Subsidized and/or Unsubsidized) Loan</strong>
                     
                     <p>To apply for a Federal Direct Stafford (Subsidized or Unsubsidized) Loan, you must
                        <a href="http://www.fafsa.ed.gov" target="_blank">complete the FAFSA</a> using our School Code of 006750. To be eligible, you must be enrolled for at least
                        six credit hours which count toward your current degree and be maintaining <a href="/admissions/finaid/satisfactory-progress.html">Valencia’s Standards of Satisfactory Academic Progress</a>. If you are eligible for a Federal Direct Stafford (Subsidized or Unsubsidized) Loan,
                        it will be included on your financial aid services award notification.
                     </p>
                     
                  </li>
                  
                  <li><strong>Accept, Reduce or Decline a Federal Direct Stafford Loan</strong>
                     
                     <p>From 2013-2014, you will have the option to Accept, Decline or Reduce your Federal
                        Direct Stafford Loan on your award notification you receive through <a href="https://atlas.valenciacollege.edu/">ATLAS</a>&nbsp;email.
                     </p>
                     
                  </li>
                  
                  <li><strong>E-sign Your Federal Direct Stafford Loan Master Promissory Note</strong>
                     
                     <p>If you are a first time borrower of the Federal Direct Stafford Loan, you will be
                        required to complete a Master Promissory Note (MPN) before the loan funds will be
                        disbursed. In most cases, you will only be required to complete one MPN during your
                        college career at Valencia, provided you are borrowing a Direct Stafford Loan. You
                        complete your Federal Direct Stafford Loan MPN by signing into the <a href="https://studentloans.gov/myDirectLoan/index.action" target="_blank">StudentLoans.gov</a> website with you Federal PIN. You can also complete your Loan Entrance Counseling
                        while you are on the site.
                     </p>
                     
                  </li>
                  
                  <li><strong>Loan Entrance Counseling </strong>
                     
                     <p>Before the Office of Student Aid can disburse your loan funds you are required to
                        complete Loan Entrance Counseling by signing into the <a href="https://studentloans.gov/myDirectLoan/index.action" target="_blank">StudentLoans.gov</a> website with your Federal PIN. This counseling session provides you with information
                        that will help you understand your rights and responsibilities as a loan borrower,
                        as well as other tools to assist you with managing your loans. In most cases, you
                        will only be required to complete Loan Entrance Counseling once during your college
                        career at Valencia.
                     </p>
                     
                  </li>
                  
                  <li><strong>Receiving Federal Direct Stafford Loan Funds</strong>
                     
                     <p>It is important to know that loan funds will automatically credit to your student
                        account, if the Direct Loan Processor has a valid MPN on file for you and you have
                        completed Loan Entrance Counseling, on the disbursement date noted on your <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas Account</a>. Your loan funds cannot be disbursed until you begin 6 credit hours during the semester
                        and must be paid in two equal disbursements for each payment period (e.g. ½ of the
                        loan in Fall and ½ in Spring).
                     </p>
                     
                  </li>
                  
                  <li><strong>Loan Exit Counseling </strong>
                     
                     <p>Student borrowers are required to complete an Exit Counseling session when they drop
                        below 1/2 time or cease to enroll at the college. You can complete your Exit Counseling
                        at <a href="http://www.nslds.ed.gov/nslds_SA/" target="_blank">https://www.nslds.ed.gov</a>.
                     </p>
                     
                  </li>
                  
               </ol>
               
               <h3>Applying for a Parent Loan for Undergraduate Students (PLUS) Federal Direct Loans</h3>
               
               <p>(The parent must apply for a PLUS Loan.)</p>
               
               <p>Interest Rate and Fee Information for the PLUS: The interest rate is fixed at 7.00%
                  with a 4.264% origination fee.
               </p>
               
               <p>For more information on interest rates go to the <a href="https://studentaid.ed.gov/sa/types/loans/interest-rates" target="_blank">Federal Student Aid</a> site.
               </p>
               
               <ol>
                  
                  <li><strong>Direct Parent Loans for Undergraduate Students (PLUS)</strong>
                     
                     <p>Visit Federal Student Aid for Information on a <a href="https://studentaid.ed.gov/sa/types/loans/plus" target="_blank">PLUS Loan</a></p>
                     
                  </li>
                  
                  <li><strong>Complete Financial Aid Services File</strong>
                     
                     <p>Your son or daughter must have a completed financial aid services file, including
                        the <a href="http://www.fafsa.ed.gov/" target="_blank">FAFSA</a>, at Valencia College using our School Code of 006750.
                     </p>
                     
                  </li>
                  
                  <li><strong>You must complete a Parent Loan Request Form for the correct year. </strong>
                     
                     <p>This <a href="/admissions/finaid/forms-worksheets.html" target="_blank">Parent Loan Request Form</a> will ask for the requested amount and allow you to certify who to send any excess
                        refunds to (the student or parent).
                     </p>
                     
                  </li>
                  
                  <li><strong>E-sign your Federal Direct Master Promissory Note (MPN)</strong>
                     
                     <p>You are required to complete a PLUS MPN before loan funds can be disbursed. By signing
                        the MPN you authorize the Department of Education to perform a credit check to determine
                        your eligibility for the PLUS loan. You can complete the MPN by signing into the <a href="https://studentloans.gov/myDirectLoan/index.action" target="_blank">StudentLoans.gov</a> website with your Federal PIN.
                     </p>
                     
                  </li>
                  
                  <li><strong>Receiving Federal Direct Parent Loan funds </strong>
                     
                     <p>It is important to know PLUS loan funds will automatically credit the students account
                        if the Direct Loan Processor has a valid MPN on file and your son or daughter has
                        begun attendance in at least 6 credit hours. Your loan funds must be paid in two equal
                        disbursements for each payment period (e.g. 1/2 the loan in the Fall and 1/2 in the
                        Spring).
                     </p>
                     
                  </li>
                  
               </ol>
               
               <h3>Applying for a Private Education Student Loan</h3>
               
               <ol>
                  
                  <li><strong>Review Federal Aid First </strong>
                     
                     <p>U.S. Department of Education <a href="http://federalstudentaid.ed.gov/federalaidfirst/index.html" target="_blank">Federal Aid First</a> explains the difference between Federal and Private loans.
                     </p>
                     
                  </li>
                  
                  <li><strong>We Encourage you to have a Completed Financial Aid Services File</strong>
                     
                     <p>We encourage you to have a completed financial aid services file, including the <a href="http://www.fafsa.ed.gov/" target="_blank">FAFSA</a>, at Valencia College using our School Code of 006750. This will provide us with information
                        to review your file for State, Federal, and Institutional funds before you borrow
                        a Private Loan.
                     </p>
                     
                  </li>
                  
                  <li><strong>Apply </strong>
                     
                     <p>Choose a lender or Bank to apply for a Private Education Loan. Valencia does not offer
                        a preferred lender list for private loans. Students can check with their bank or research
                        private loans on the Web. We strongly encourage students to borrow federal loans first
                        as they offer better benefits and interest rates.
                     </p>
                     
                  </li>
                  
                  <li><strong>School Certification</strong>
                     
                     <p>Our office will certify the loan once we receive a certification request from your
                        lending institution and you are enrolled in classes for the requested payment period.
                        Private loan limits are set by federal regulations and cannot exceed the cost of attendance
                        minus any financial aid received. We are not responsible for the timeliness of private
                        loans and students should address all customer service issues with their lender.
                     </p>
                     
                  </li>
                  
               </ol>
               
               <h3>Direct Loan Helpful Links</h3>
               
               <ul class="list_style_1">
                  
                  <li><a href="https://studentloans.gov/myDirectLoan/index.action" target="_blank">StudentLoans.gov </a></li>
                  
                  <li><a href="http://www.nslds.ed.gov/nslds_SA/" target="_blank">Direct Loan Service Center</a></li>
                  
                  <li><a href="https://studentloans.gov/myDirectLoan/whatYouNeed.action?page=mpn" target="_blank">Direct Loan Master Promissory Note (MPN)</a></li>
                  
                  <li><a href="https://studentloans.gov" target="_blank">Direct Loan Entrance Counseling</a></li>
                  
                  <li><a href="http://www.nslds.ed.gov/nslds_SA/" target="_blank">Direct Loan Exit Counseling</a></li>
                  
                  <li><a href="https://www.nslds.ed.gov/npas/index.htm" target="_blank">Direct Loan Deferment Forms</a></li>
                  
                  <li><a href="https://studentaid.ed.gov/sa/repay-loans/understand/plans" target="_blank">Loan Repayment Information </a></li>
                  
                  <li><a href="https://studentloans.gov/myDirectLoan/mobile/repayment/repaymentEstimator.action" target="_blank">Loan Repayment and Budget Calculators </a></li>
                  
                  <li><a href="https://studentaid.ed.gov/sa/repay-loans/default" target="_blank"> Help with Defaulted Student Loans</a></li>
                  
                  <li><a href="http://www.nslds.ed.gov/nslds_SA/" target="_blank">Locate your Loans on the National Student Loan Data System (NSLDS)</a></li>
                  
                  <li><a href="/admissions/finaid/programs/loan-aggregate-limits.html" target="_blank">How much Can I Borrow?</a></li>
                  
                  <li><a href="https://studentaid.ed.gov/sa/types/loans/plus" target="_blank">Direct Parent Loan for Undergraduate Students (PLUS) </a></li>
                  
               </ul>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/federal-direct-loans.pcf">©</a>
      </div>
   </body>
</html>