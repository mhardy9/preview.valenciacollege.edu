<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Aid Services  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li>Financial Aid Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Applying for Financial Aid?</h2>
                        
                        <p>Applying for financial aid is a multi–step process. Our web pages provide information
                           you will need to know to successfully apply for financial aid and complete the process.
                        </p>
                        
                        <h3>Important Links:</h3>
                        
                        <ul>
                           
                           <li><a href="http://www.finaid.ucsb.edu/FAFSASimplification/" target="_blank">FAFSA Video Tutorial</a></li>
                           
                           <li><a href="/admissions/finaid/fafsafrenzy.php" target="_blank">FAFSA Frenzy</a></li>
                           
                           <li><a href="/admissions/finaid/getting-started/aid-disbursement.php" target="_blank">Learn about Using Financial Aid for your Classes, Books &amp; Supplies and Receiving a
                                 Refund </a></li>
                           
                           <li><a href="/admissions/finaid/getting-started/attending-and-withdrawing.php" target="_blank">Must Read Before you Withdraw from a Class while Receiving Financial Aid </a></li>
                           
                           <li><a href="#PellCalculator1718">2017 -2018 Pell Grant Calculator</a></li>
                           
                           <li><a href="/admissions/finaid/net-price-calculator/" target="_blank">Net Price Calculator</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/why-valencia/consumer-information/" target="_blank">Consumer Information</a></li>
                           
                        </ul>
                        
                        <p>At Valencia we provide financial aid services to you in the following ways:</p>
                        
                        <p><strong>Financial Aid Services Videos: </strong>Click on the <a href="http://net4.valenciacollege.edu/promos/internal/financialaidvideos.cfm" target="_blank">Financial Aid TV</a> icon to get short video answers to general questions about the financial aid application
                           process, eligibility requirements, and information on federal programs.
                        </p>
                        
                        <p><strong>Online:</strong> Information about Valencia’s financial aid programs, as well as copies of all required
                           forms can be found on this website. Simply use the navigation bar on the left side
                           of this page.
                        </p>
                        
                        <p><strong>Atlas Online: </strong>All admitted students are assigned an Atlas account accessible at <a href="https://atlas.valenciacollege.edu">atlas.valenciacollege.edu</a>. Access to your confidential financial aid status can be obtained from your Valencia
                           Atlas account. Log into Atlas using your PIN. Click on the Student Tab, then click
                           on Financial Aid found under STUDENT RESOURCES, then click on My Financial Aid.
                        </p>
                        
                        <p><strong>In Person:</strong> Financial aid advising and discussions about individual circumstances are provided
                           through in–person confidential visits. Visit the Answer Center on your campus. The
                           Answer Center is open Monday through Thursday from 8AM until 6PM and on Friday from
                           9AM until 5PM.
                        </p>
                        
                        <p>To find your <strong>Expected Family Contribution (EFC)</strong>, please visit the official <strong><a href="https://fafsa.ed.gov/FAFSA/app/fafsa" target="_blank">FAFSA site</a></strong> and check under the appropriate academic year.
                        </p>
                        
                        <h3><a id="PellCalculator1718" name="PellCalculator1718"></a>2017-2018 Pell Calculator
                        </h3>
                        
                        <form id="pell1718" name="pell1718">
                           
                           <div class="row">
                              
                              <div class="col-md-10"><label for="efcAmount1718">Enter your <em>Expected Family Contributions (EFC)</em>, <br> which is located on your Student Aid Report (SAR):</label></div>
                              
                              <div class="col-md-2"><input id="efcAmount1718" name="efcAmount1718" type="text"></div>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-10"><label for="crHours1718">Enter the actual number of <em>Credit Hours</em> for the <em>Current Semester</em>: </label></div>
                              
                              <div class="col-md-2"><input id="crHours1718" maxlength="2" name="crHours1718" type="text"></div>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-12 text-center"><input class="button-outline" type="button" value="Calculate" onclick="doTheMath1718();"> <input class="button-outline" type="reset" value="Reset"></div>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-10"><label for="pellAward1718">Your Pell Grant Award Amount <em>Estimate</em> for the Current Semester is: </label></div>
                              
                              <div class="col-md-2"><input id="pellAward1718" name="pellAward1718" readonly type="text"></div>
                              
                           </div>
                           
                        </form>
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <p>Valencia College Gainful Employment Programs are Financial Aid Eligible.</p>
                           
                           <p><a href="/admissions/asdegrees/gainful-employment/index.php" target="_blank"> Gainful Employment Programs </a></p>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/index.pcf">©</a>
      </div>
      		<script src="pellcalc1718.js"></script>
      	</body>
</html>