<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Literacy Events  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Financial Literacy Events </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Financial Literacy Events</h2>
                        
                        <h3>Spring 2017</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Event Title</th>
                                 
                                 <th scope="col">Location</th>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Time</th>
                                 
                                 <th scope="col">Room</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Be a Smartie with Your Tax Refund </th>
                                 
                                 <td> Osceola </td>
                                 
                                 <td> 1/26/2017 </td>
                                 
                                 <td> 1:30-2:30pm </td>
                                 
                                 <td> Auditorium, Building 1 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Love Don't Cost a Thing </th>
                                 
                                 <td> Osceola </td>
                                 
                                 <td> 2/28/2017 </td>
                                 
                                 <td> 1:30-2:30pm </td>
                                 
                                 <td> Auditorium, Building 1 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Pot of Gold Budgeting </th>
                                 
                                 <td> Osceola </td>
                                 
                                 <td> 3/09/2017 </td>
                                 
                                 <td> 11:00am-12:00pm </td>
                                 
                                 <td> Building 1, Room 148 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Building Your Money Tree </th>
                                 
                                 <td> Osceola </td>
                                 
                                 <td> 4/04/2017 </td>
                                 
                                 <td> 1:30-2:30pm </td>
                                 
                                 <td> Auditorium, Building 1 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>West Campus</h3>
                        
                        <h3>Spring 2017</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Event Title</th>
                                 
                                 <th scope="col">Location</th>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Time</th>
                                 
                                 <th scope="col">Room</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Burning Through Your Refund </th>
                                 
                                 <td> West </td>
                                 
                                 <td> 1/31/2017 </td>
                                 
                                 <td> 2:00-3:00pm </td>
                                 
                                 <td> 11-106 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Fun, Frugal, and Free on Valentine’s Day </th>
                                 
                                 <td> West </td>
                                 
                                 <td> 2/14/2017 </td>
                                 
                                 <td> 2:00-3:00pm </td>
                                 
                                 <td> HSB-105 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Adventures are the Best Way to Learn </th>
                                 
                                 <td> West </td>
                                 
                                 <td> 3/7/2017 </td>
                                 
                                 <td> 2:00-3:00pm </td>
                                 
                                 <td> 11-106 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Financial Literacy Awareness </th>
                                 
                                 <td> West </td>
                                 
                                 <td> 4/18/2017 </td>
                                 
                                 <td> 2:00-3:00pm </td>
                                 
                                 <td> 11-106 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>East Campus</h3>
                        
                        <h3>Spring 2017</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Event Title</th>
                                 
                                 <th scope="col">Location</th>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Time</th>
                                 
                                 <th scope="col">Room</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Financial Record: A Clean Slate </th>
                                 
                                 <td> East </td>
                                 
                                 <td> 1/18/2017 </td>
                                 
                                 <td> 12:30-2:00pm </td>
                                 
                                 <td> 3-113 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> I Love Money </th>
                                 
                                 <td> East </td>
                                 
                                 <td> 2/18/2017 </td>
                                 
                                 <td> 12:30-2:30pm </td>
                                 
                                 <td> 8-101 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> March Into Good Credit </th>
                                 
                                 <td> East </td>
                                 
                                 <td> 03/01/2017 </td>
                                 
                                 <td> 1:00-2:00pm </td>
                                 
                                 <td> 3-113 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Money Never Goes Out of Style </th>
                                 
                                 <td> East </td>
                                 
                                 <td> 4/14/2017 </td>
                                 
                                 <td> 1:00-2:30pm </td>
                                 
                                 <td> Building 3 Atrium </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>Winter Park Campus</h3>
                        
                        <h3>Spring 2017</h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Event Title</th>
                                 
                                 <th scope="col">Location</th>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Time</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> More Bang for our Buck </th>
                                 
                                 <td> Winter Park </td>
                                 
                                 <td> 01/30/2017 </td>
                                 
                                 <td> 11:30am - 12:30pm </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Credit, Scholarships, Loans, Banking &amp; Investing </th>
                                 
                                 <td> Winter Park </td>
                                 
                                 <td> 03/07/2017 </td>
                                 
                                 <td> 11:30am - 12:30pm </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>Lake Nona Campus</h3>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/events.pcf">©</a>
      </div>
   </body>
</html>