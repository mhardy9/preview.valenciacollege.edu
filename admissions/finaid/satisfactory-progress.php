<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Satisfactory Academic Progress  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/satisfactory-progress.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Satisfactory Academic Progress </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Satisfactory Academic Progress (SAP) </h2>
                        
                        <h3>How to Keep Your Financial Aid</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Complete 67% of all classes attempted, and </li>
                           
                           <li>Maintain a Valencia Grade Point Average (GPA) of 2.0 or higher, and </li>
                           
                           <li>Maintain an overall Grade Point Average (GPA) of 2.0 or higher, and </li>
                           
                           <li>Complete your degree within the 150% timeframe (i.e. an associate degree of 60 credit
                              hours must be completed within 90 credit hours)
                           </li>
                           
                           <li>Become familiar with Valencia’s <a href="#SAPPolicy">Satisfactory Academic Progress Policy</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Are you meeting SAP standards? Use this <a href="documents/SatisfactoryAcademicProgressWorksheet.pdf" target="_blank">worksheet</a> as a guide. 
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col"> What Happens  </th>
                                 
                                 <th scope="col"> Your Financial Aid Status  </th>
                                 
                                 <th scope="col"> What you need to do  </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>If your overall GPA drops below a 2.0 or your Valencia GPA drops below a 2.0. </td>
                                 
                                 <td>Suspended - You are not eligible for financial aid.</td>
                                 
                                 <td>Raise your GPA to a 2.0 or higher or you can appeal if you have mitigating circumstances.
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>If you do not have an overall completion rate of 67% or more.</td>
                                 
                                 <td>Suspended - You are not eligible for financial aid.</td>
                                 
                                 <td>Raise your completion rate to 67% or higher to become eligible for financial aid or
                                    you can appeal if you have mitigating circumstances. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>If you attempt over 150% of your program of study (major).</td>
                                 
                                 <td>Suspended - You are not eligible for financial aid.</td>
                                 
                                 <td>You can appeal if you have mitigating circumstances. If your appeal is approved you
                                    will need to successfully complete 100% of all classes and required to maintain an
                                    overall GPA of 2.0 or higher. 
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h4>Completion Rate Worksheet: </h4>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td></td>
                                 
                                 <th scope="col"> Enter your hours </th>
                                 
                                 <th scope="col"> Sample </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <strong>Total Credit Hours Completed </strong> 
                                 </td>
                                 
                                 <td> ____________________ </td>
                                 
                                 <td> <strong>25</strong> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <strong>divided by </strong> 
                                 </td>
                                 
                                 <td> / </td>
                                 
                                 <td> / </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <strong>Total Credit Hours Attempted </strong> 
                                 </td>
                                 
                                 <td> ____________________ </td>
                                 
                                 <td> <strong>35</strong> 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <strong>Equals</strong> 
                                 </td>
                                 
                                 <td> = </td>
                                 
                                 <td> = </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> <strong>Completion Rate </strong> 
                                 </td>
                                 
                                 <td> ____________________ </td>
                                 
                                 <td> <strong>71.42%</strong> 
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>GPA Calculator – <a href="/students/advising-counseling/gpa-calculators.html" target="_blank">Raise My GPA</a></p>
                        
                        
                        <h3 id="SAPPolicy">Standards of Satisfactory Academic Progress for Recipients of Financial Aid</h3>
                        
                        <p>Standards of Satisfactory Academic Progress (SAP) for Financial Aid Eligibility Federal
                           regulations (34 CFR 668.34) require a student to move toward the completion of a degree
                           or certificate within an eligible program when receiving financial aid. Specific requirements
                           for academic progress for financial aid recipients are applied differently than college
                           Academic Standards, Warning, Probation, and Suspension. Federal regulations state
                           that Satisfactory Academic Progress Standards must include a review of all periods
                           of enrollment, regardless of whether or not aid was received. The student must meet
                           all the minimum standards in order to receive financial aid. 
                        </p>
                        
                        
                        <h4>I. Evaluation of Financial Aid Eligibility</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Standards of Satisfactory Academic Progress (SAP) are applied at the end of every
                              semester to determine eligibility for the following academic semester. SAP standards
                              are calculated using the cumulative GPA and cumulative completion rate. Students must
                              maintain an overall GPA of 2.0 or higher, a Valencia GPA of 2.0 or higher, and a completion
                              rate of 67% or more.
                           </li>
                           
                           <li>Students will be placed on financial aid suspension if they have not met the standards
                              of satisfactory academic progress. Students will be ineligible for aid during the
                              suspension. Students will need to raise their GPA and/or completion rate to meet the
                              minimum SAP requirements to regain financial aid eligibility or appeal their financial
                              aid suspension, if applicable. (see V. Appeal Process)
                           </li>
                           
                           <li>The evaluation period will be based on attendance in all prior semester(s) and will
                              include all classes attempted whether federal aid was received or not. The initial
                              evaluation at Valencia will be based on all previous institutions attended once the
                              transcripts are received, evaluated, and posted to the student’s record. SAP will
                              be reviewed after each semester. The student’s cumulative GPA and completion ratio
                              must meet the minimum standards or the student will be placed on suspension.
                           </li>
                           
                           <li>Credits evaluated will include credits attempted at Valencia, transfer credits accepted
                              by Valencia, and courses funded through consortium agreement. 
                           </li>
                           
                           <li>Students on probation and following a Financial Aid Eligibility Progress Plan will
                              be evaluated according to the terms of the progress plan.
                           </li>
                           
                           <li>Students who do not meet the Standards of Satisfactory Academic Progress will be notified
                              by Atlas email and their status will be available on Atlas under the financial aid
                              Requirements section. 
                           </li>
                           
                           <li>Students may follow the appeal process or the reinstatement procedures as outlined
                              in V and VI. Students will not have eligibility for any further federal aid until
                              they have met Standards of Satisfactory Academic Progress or have been granted an
                              appeal approval.  
                           </li>
                           
                        </ul>
                        
                        
                        <h4>II. Eligibility </h4>
                        
                        <p>A. Students must meet the following criteria: </p>
                        
                        <ol>
                           
                           <li>Students must complete with a passing grade 67% of all credits attempted</li>
                           
                           <li>Students must maintain a Valencia GPA requirement of 2.0</li>
                           
                           <li> Students must maintain a cumulative GPA requirement of a 2.0</li>
                           
                           <li> Students must complete their program of study within the 150% timeframe of their
                              degree or eligible certificate program. For example, if a program is 60 credit hours
                              then the student must complete all required coursework within 90 hours.  This includes
                              repeated grades and college preparatory coursework.
                           </li>
                           
                        </ol>
                        
                        <p><strong>Note: </strong>Grades of F, I, W, WN, WP, WF, WW, AR, and courses not yet graded are considered attempted
                           but not meeting progress standards for the purposes of financial aid. 
                        </p>
                        
                        
                        <h4>III. Maximum Timeframe Eligibility </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Students who have attempted more than 150% of the credits required for their program
                              of study are not considered to be making Satisfactory Academic Progress and therefore,
                              are ineligible for financial aid funds. 
                           </li>
                           
                           <li>Students seeking second degrees and students with double majors are monitored like
                              any other students under this policy. If or when the student exceeds the maximum time
                              frame allowed for their respective programs, students may appeal if they have mitigating
                              circumstances. All transfer hours accepted by Valencia will be included when determining
                              maximum timeframe eligibility. 
                           </li>
                           
                           <li>A student with a Bachelor’s degree or higher will be considered to have exhausted
                              maximum timeframe eligibility. 
                           </li>
                           
                           <li>A student may appeal as outlined in V. Reinstatement procedures as outlined in VI
                              are not applicable to maximum timeframe eligibility. 
                           </li>
                           
                           <li>Students are limited to one timeframe appeal and will be required to successfully
                              complete 100% of all future coursework if approved.  A degree audit will be required
                              for all timeframe appeals.
                           </li>
                           
                           <li>Students who have earned a Bachelor’s degree may not earn an Associates of Arts Degree.
                              All credits earned apply to the 150% timeframe for the degree program in which the
                              student is currently enrolled.
                           </li>
                           
                        </ul>
                        
                        
                        <h4>IV. Repeated, Audited, Consortium, Remedial Courses, Enrollment</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Financial aid eligibility will include repeated courses which were not successfully
                              completed. Students who are on their third attempt of a class must pay full cost of
                              tuition according to Florida statute unless they have a successful third attempt appeal.
                              
                           </li>
                           
                           <li>Financial aid eligibility will include repeating a course one time if the student
                              earned prior credit for the course with a grade of a “D”.  All classes taken must
                              be counted as attempted classes regardless of grade forgiveness.
                           </li>
                           
                           <li>Courses funded through a consortium agreement are included in determining academic
                              progress. 
                           </li>
                           
                           <li>All attempted remedial credits will be included when evaluating SAP. (A maximum of
                              30 remedial credit hours, excluding ESL courses may be funded.) 
                           </li>
                           
                           <li>Enrollment in any part of term will be considered in the respective fall, spring,
                              or summer term to be evaluated for SAP.
                           </li>
                           
                           <li> Students transferring in Z Grades only (i.e., AP, CLEP, Military, Experiential) in
                              their first term at Valencia will be evaluated as SAP OK if their earned hours are
                              67% or greater than the attempted credits. Students will not be required to appeal
                              for financial aid when entering Valencia College under these conditions. 
                           </li>
                           
                        </ul>
                        
                        
                        <h4>V. Appeal Process </h4>
                        
                        <p>A student who has lost financial aid eligibility due to extenuating circumstances
                           may appeal. 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Each student appealing for financial aid <strong>must meet</strong> with an Academic Advisor, Counselor, Transfer Program Advisor, or Career Program
                              Advisor. Bring with you a print out of your My Education Plan (MEP) so that your remaining
                              credits can be verified.
                           </li>
                           
                           <li>Extenuating circumstances that may be considered include: personal illness or accident,
                              serious illness or death within immediate family, or other circumstances beyond the
                              reasonable control of the student.
                           </li>
                           
                           <li>All appeals must be in writing to the Office of Student Financial Assistance and include
                              appropriate documentation. 
                           </li>
                           
                           <li>Examples of documentation could include an obituary notice, divorce decree, or a letter
                              from a physician, attorney, social services agency, parole officer, employer, etc.
                           </li>
                           
                           <li>The condition or situation must be resolved which will allow the student the ability
                              to complete course work successfully or an appeal will not be granted. 
                           </li>
                           
                           <li>The outcome of an appeal may include a denial or probationary period with a <strong>Financial Aid</strong> <strong>Eligibility Progress Plan (FAEPP) to assure students meet the Standards of Satisfactory
                                 Academic Process</strong>. 
                           </li>
                           
                           <li>Students will be notified by Atlas email of the results of the appeal, and of any
                              restrictions or conditions pertaining to their appeal. 
                           </li>
                           
                        </ul>
                        
                        <p>Students, whose appeal is denied, may submit a written request for a review of the
                           appeal to the College Financial Aid Services Appeals Committee. The Committee’s decision
                           is final. If the Committee’s decision is to uphold the denial, the student may not
                           submit any subsequent requests for funding consideration. In order to regain eligibility
                           for financial aid, the student would need to meet Satisfactory Academic Progress Standards.
                           Students who do not meet the terms of financial aid probation may permanently lose
                           eligibility for federal student aid at Valencia College. 
                        </p>
                        
                        
                        <h4>VI. Reinstatement of Financial Aid Eligibility </h4>
                        
                        <ul class="list_style_1">
                           
                           <li>A student who has lost financial aid eligibility may be reinstated after the student
                              has taken classes to meet the minimum requirements of an overall and Valencia GPA
                              of 2.0 and a cumulative completion rate of 67% of all credit hours being evaluated
                              or was approved on appeal.
                           </li>
                           
                           <li>All classes including those taken at other institutions will be taken into consideration
                              for reinstatement purposes. 
                           </li>
                           
                           <li>A Student must be able to complete their degree or certificate within the 150% timeframe.</li>
                           
                           <li>It is the student’s responsibility to notify the Office of Student Financial Assistance
                              when this condition has been met. 
                           </li>
                           
                           <li>Students who exhaust the 150% timeframe and have used their one appeal cannot be reinstated
                              financial aid at Valencia.
                           </li>
                           
                        </ul>
                        
                        
                        <h3>67% Completion Chart for Successful Progression toward a Degree or Eligible Certificate
                           by Term.
                        </h3>
                        
                        <p>The chart below demonstrates 67% for attempted hours in a semester.  The left hand
                           column is the attempted hours and the corresponding number in the right column is
                           the number of hours you must successfully complete with a 2.0 GPA to maintain Satisfactory
                           Academic Progress. For example, if you register for 14 credit hours, you must complete
                           10 credit hours with a 2.0 GPA or higher to meet the satisfactory academic progress
                           requirements for the semester.  Both the 2.0 GPA and completion rate of 67% applies
                           to both the semester and cumulative credit hour on your transcript.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Credit Hours Attempted</th>
                                 
                                 <th scope="col">Earned Hours Needed (Passed with GPA of 2.0 or greater)</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 19 </td>
                                 
                                 <td> 13 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 18 </td>
                                 
                                 <td> 13 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 17 </td>
                                 
                                 <td> 12 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 16 </td>
                                 
                                 <td> 11 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 15 </td>
                                 
                                 <td> 11 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 14 </td>
                                 
                                 <td> 10 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 13 </td>
                                 
                                 <td> 9 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 12 </td>
                                 
                                 <td> 8 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 11 </td>
                                 
                                 <td> 8 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 10 </td>
                                 
                                 <td> 7 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 9 </td>
                                 
                                 <td> 7 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 8 </td>
                                 
                                 <td> 6 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 7 </td>
                                 
                                 <td> 5 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 6 </td>
                                 
                                 <td> 5 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 5 </td>
                                 
                                 <td> 4 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 4 </td>
                                 
                                 <td> 3 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 3 </td>
                                 
                                 <td> 3 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 2 </td>
                                 
                                 <td> 2 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> 1 </td>
                                 
                                 <td> 1 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/satisfactory-progress.pcf">©</a>
      </div>
   </body>
</html>