<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Using Financial Aid | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/aid-disbursement.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Using Financial Aid </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Valencia's Financial Aid Services Disbursement Process</h2>
                        
                        <p><strong>Quick Links:</strong></p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="#Classes">Paying for Classes</a></li>
                           
                           <li><a href="#Books">Charging Books &amp; Supplies</a></li>
                           
                           <li><a href="#Refund">Valencia’s Refund Process</a></li>
                           
                           <li><a href="#DisbursementSchedule">Disbursement Schedule</a></li>
                           
                           <li><a href="#DisbursementDates">Disbursement Dates</a></li>
                           
                        </ul>
                        
                        <p><strong><a href="/admissions/finaid/getting-started/payment-auth-instructions.php">Federal Student Aid Payment Authorization Instructions</a> </strong></p>
                        
                        <p>You must have a completed payment authorization on file to charge books and supplies.
                           The Bookstore will have information about the amount of financial aid you have available
                           for books and will allow you to charge your bookstore purchase against the expected
                           financial aid. The dates you can charge books will be on the <a href="/students/locations-store/index.php" target="_blank">Bookstore's web page. </a></p>
                        
                        <h3><a id="Classes" name="Classes"></a>Paying for Classes with Financial Aid
                        </h3>
                        
                        <p>Once you have been awarded financial aid and meet all requirements, your awards will
                           be authorized. Financial aid programs are calculated on courses that are part of your
                           program. All students are awarded based on full-time enrollment and the amount of
                           funds authorized will be based on actual enrollment.
                        </p>
                        
                        <p><strong>Example:</strong></p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>&nbsp;</th>
                                 
                                 <th scope="col">Award Offer based on fulltime (12+ hrs.)</th>
                                 
                                 <th scope="col">Enrolls half-time (6-8 hrs.)</th>
                                 
                                 <th scope="col">Authorized Award (your eligibility)</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td scope="row">Pell Grant</td>
                                 
                                 <td>$2960 per term</td>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <td>$1480 per term</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td scope="row">SEOG</td>
                                 
                                 <td>$100 to $1000</td>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <td>$50 to $500</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td scope="row">FSAG</td>
                                 
                                 <td>$2608 per year</td>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <td>$1304 per year</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>Note: Funds will only disburse for courses required for your degree or certificate.
                           Make sure to check your <a href="https://atlas.valenciacollege.edu/">Atlas</a> email for Financial Aid Non-Compliant Course Notification and take appropriate action.
                        </p>
                        
                        <p>If your authorized financial aid funds cover your entire tuition bill your classes
                           will be automatically protected. If you have a balance after course eligibility is
                           determined and financial aid is applied you will need to pay the difference or enroll
                           in the <a href="/students/business-office/tuition-installment-plan/">Tuition Installment Program</a>, otherwise your classes may be dropped on the fee payment deadline.
                        </p>
                        
                        <p>If you do not plan to attend Valencia it is your responsibilty to drop your classes
                           by the end of the drop/refund period or you will be responsible for the tuition and
                           fees.
                        </p>
                        
                        <p>You can see whether your funds have authorized by checking your billing statement
                           in <a href="https://atlas.valenciacollege.edu/">Atlas</a>. Go to the Students Tab, choose Financial Aid under STUDENT RESOURCES on the left
                           side of the screen. Then select the <strong>Term Balance Less Anticipated Financial Aid</strong> link.
                        </p>
                        
                        <h3><a id="Books" name="Books"></a>Charging Books and Supplies with Financial Aid
                        </h3>
                        
                        <p>If your aid is more than your tuition and fees and your financial aid funds have been
                           authorized you may charge books and supplies in the bookstore during the published
                           dates listed on the Valencia College Bookstore web page. Usually this begins 2-3 weeks
                           prior to the term and is available through the end of the drop/refund period. Visit
                           the <a href="/students/locations-store/">bookstore web page</a> for published dates for the term. If you charge books and then are not eligible for
                           the funding you will be responsible for any charges.
                        </p>
                        
                        <h3><a id="Refund" name="Refund"></a>Receiving a Financial Aid Services Refund
                        </h3>
                        
                        <p>At Valencia we start processing refunds the <strong> week of classes after the “no show” reporting period </strong>listed on the online calendar <a href="/academics/calendar/" target="_blank">Important Dates &amp; Deadlines</a>. Review the chart below to determine when you should expect your refund. All Valencia
                           tuition, fee, and book charges will be paid before any refund will be issued.
                        </p>
                        
                        <p>Once the disbursement process begins, refunds are issued on most Fridays and disbursed
                           based on your selected <a href="/admissions/refund/">refund preference</a>.
                        </p>
                        
                        <p>Disbursements will begin on the following dates for the Fall, Spring, and Summer Term.
                           <strong>Please note if you have courses in later parts-of terms your funds will be disbursed
                              according to the schedule below. </strong>You should budget accordingly based on your enrollment and expected disbursement date.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Term</th>
                                 
                                 <th>Disbursement begins for the term</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Fall 2017</td>
                                 
                                 <td>September 26, 2017</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Spring 2018</td>
                                 
                                 <td>January 30, 2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Summer 2018</td>
                                 
                                 <td>June 05, 2018 - (Pell Grants July 10, 2018)</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p><strong>The bank account you elected to receive funds should be credited with any refund due
                              within 3-5 business days after your disbursement date.</strong></p>
                        
                        <h3><a id="DisbursementSchedule" name="DisbursementSchedule"></a>Disbursement Schedule
                        </h3>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Financial Aid Program</th>
                                 
                                 <th>Disbursement Information</th>
                                 
                                 <th>Notes</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Pell Grants</p>
                                    
                                    <p>Florida Student Assistance Grant (FSAG)</p>
                                    
                                    <p>Supplemental Educational Opportunity Grant (SEOG)</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Disbursement begins after your last class for the term begins. <strong>If you have courses in later parts-of-terms </strong> your Pell grant, FSAG, and SEOG will disburse after the no show reporting period
                                       for that part of term.
                                    </p>
                                    
                                    <p>Funds will only disburse for courses required for your degree or certificate.</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>If you fail to start class and receive funding from these programs you will be responsible
                                       to pay back the funds.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Loans</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Federal Direct loans will be disbursed once a student has started attendance in at
                                       least one compliant course and have met the no show reporting period for that term
                                       and are enrolled in a total of at least six compliant credit hours towards an eligible
                                       degree program.
                                    </p>
                                    
                                    <p>Students must be meeting Satisfactory Academic Progress.</p>
                                    
                                    <p>First Year, first time undergraduate borrowers are required to wait 30 days before
                                       receiving their first loan fund disbursement.
                                    </p>
                                    
                                    <p>If a student selects a <strong>one term</strong> loan, the loan will be split into two disbursements.
                                    </p>
                                    
                                    <p>If a student selects a <strong>two term</strong> loan, the loan will be split equally between the two terms.
                                    </p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Loan funds must be repaid once you graduate or drop below 6 credit hours. Depending
                                       on your situation, repayment may begin immediately or within 6 months.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Bright Futures, Bridges</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Disbursement begins after your last class for the term begins.</p>
                                    
                                    <p><strong>If you have courses in later parts-of-terms</strong> your Bright Futures and/or Bridges funds will disburse after the no show reporting
                                       period for that part of term.
                                    </p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>If you fail to start class and receive funding from Bright Futures and/or Bridges
                                       funds you will be responsible to pay back the funds.
                                    </p>
                                    
                                    <p>If you withdraw from a course paid with Bright Futures and/or Bridges funds, you will
                                       be responsible to pay back the Bright Futures funding or you will lose eligibility.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Valencia Funds: Advantage, Scholars Program, etc.</p>
                                    
                                 </td>
                                 
                                 <td colspan="2">
                                    
                                    <p>Institutional funds will disburse to your account beginning with the first disbursement
                                       for the term.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Scholarships</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Will be disbursed according to the scholarship requirements and once the funding has
                                       been received from the donor organization.
                                    </p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>The requirements should have been listed on your scholarship application or award
                                       letter.&nbsp; You can contact the Scholarship donor for details.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    <p>Federal Work Study</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Federal Work Study funds will be paid for time worked according to the payroll schedule.</p>
                                    
                                 </td>
                                 
                                 <td>
                                    
                                    <p>Student must be working a FWS position, during the term, to be eligible for these
                                       funds
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h3><a id="DisbursementDates" name="DisbursementDates"></a>Disbursement and CAPP Compliance Dates for the 2017-2018 Academic Year:
                        </h3>
                        
                        <table class="table table">
                           <caption>Fall 2017 Parts of Term</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <th scope="col">Full term (8/28/17-12/10/17) <br> H1 (8/28/17 - 10/19/17 <br> TWJ (8/28/17 - 11/5/17)
                                 </th>
                                 
                                 <th scope="col">LSC (9/25/17 - 11/19/17)</th>
                                 
                                 <th scope="col">TWK (10/2/17 - 12/15/17)</th>
                                 
                                 <th scope="col">H2 (10/20/17 - 12/17/17)</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Class Start Date</th>
                                 
                                 <td>8/28/2017</td>
                                 
                                 <td>9/25/2017</td>
                                 
                                 <td>10/2/2017</td>
                                 
                                 <td>10/20/2017</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Compliant Hours Lock <br> (Pell Census/freeze)
                                 </th>
                                 
                                 <td>9/20/2017</td>
                                 
                                 <td>10/16/2017</td>
                                 
                                 <td>10/23/2017</td>
                                 
                                 <td>11/13/2017</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">FA Applied to Acct.</th>
                                 
                                 <td>9/21/2017</td>
                                 
                                 <td>10/17/2017</td>
                                 
                                 <td>10/24/2017</td>
                                 
                                 <td>11/14/2017</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Refund Issued to Student</th>
                                 
                                 <td>9/26/2017</td>
                                 
                                 <td>10/20/2017</td>
                                 
                                 <td>10/27/2017</td>
                                 
                                 <td>11/17/2017</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           <caption>Spring 2018 Parts of Term</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <th scope="col">Full term (1/8/18 - 4/22/18) <br> H1 (1/8/18 - 2/28/18) <br> TWJ (1/8/18 - 3/25/18)
                                 </th>
                                 
                                 <th scope="col">LSC (1/22/18 - 3/25/18)</th>
                                 
                                 <th scope="col">TWK (2/12/18 - 4/29/18)</th>
                                 
                                 <th scope="col">H2 (3/1/18 - 4/29/18)</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Class Start Date</th>
                                 
                                 <td>1/8/2018</td>
                                 
                                 <td>1/22/2018</td>
                                 
                                 <td>2/12/2018</td>
                                 
                                 <td>3/1/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Compliant Hours Lock <br> (Pell Census/freeze)
                                 </th>
                                 
                                 <td>1/29/2018</td>
                                 
                                 <td>2/12/2018</td>
                                 
                                 <td>3/5/2018</td>
                                 
                                 <td>4/2/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">FA Applied to Acct.</th>
                                 
                                 <td>1/30/2018</td>
                                 
                                 <td>2/13/2018</td>
                                 
                                 <td>3/6/2018</td>
                                 
                                 <td>4/3/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Refund Issued to Student</th>
                                 
                                 <td>2/2/2018</td>
                                 
                                 <td>2/16/2018</td>
                                 
                                 <td>3/9/2018</td>
                                 
                                 <td>4/6/2018</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           <caption>Summer 2018 Parts of Term</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <th scope="col">Full term (5/7/18 - 7/31/18) <br> H1 (5/7/18 - 6/18/18) <br> TWJ (5/7/18 - 7/1/18)
                                 </th>
                                 
                                 <th scope="col">LSC</th>
                                 
                                 <th scope="col">TWK (6/4/18 - 7/59/18)</th>
                                 
                                 <th scope="col">H2 (6/19/18 - 7/31/18)</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Class Start Date</th>
                                 
                                 <td>5/7/2018</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>6/4/2018</td>
                                 
                                 <td>6/20/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Compliant Hours Lock <br> (Pell Census/freeze)
                                 </th>
                                 
                                 <td>6/4/2018</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>6/25/2018</td>
                                 
                                 <td>7/16/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">FA Applied to Acct.</th>
                                 
                                 <td>6/5/2018</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>6/26/2018</td>
                                 
                                 <td>7/17/2018</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Refund Issued to Student</th>
                                 
                                 <td>6/8/2018</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>6/29/2018</td>
                                 
                                 <td>7/20/2018</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <ul class="list_style_1">
                           
                           <li><em>All dates are subject to change!</em></li>
                           
                           <li>Each student's federal aid will be disbursed at the date listed for the latest part
                              of term they are enrolled.
                           </li>
                           
                           <li>If a student registers for a later part of term after this date has passed, the class
                              will not be covered by financial aid.
                           </li>
                           
                           <li>Federal Direct loans will be disbursed once a student has started attendance in at
                              least one compliant course and they are enrolled in at least 6 program compliant hours.
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/aid-disbursement.pcf">©</a>
      </div>
   </body>
</html>