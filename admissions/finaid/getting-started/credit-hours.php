<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Credit Hours Needed | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/credit-hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Credit Hours Needed </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>How many hours must I take?</h2>
                        
                        <p>Most financial aid programs allow you to enroll as a part-time student as long as
                           you take at least 6 credit hours. For some programs, such as Federal Pell Grant, Federal
                           SEOG, and FSAG, the dollar amounts of your award will be reduced based on the number
                           of credit hours you are enrolled. For example, if you enroll in 6 hours, you will
                           be “half-time” so your award amount will be half of the amount for a full-time student.
                           Read the information in this guide about your awarded programs. Check your Award Notification
                           for messages about the programs you have and their specific requirements.
                        </p>
                        
                        <p>For those programs that are prorated based on enrollment, the following schedule determines
                           your enrollment status. <strong>(Only courses required for the student’s degree or certificate will be included in
                              financial aid enrollment status)</strong>:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <strong>12 credit hours or more</strong> - full-time status
                           </li>
                           
                           <li>
                              <strong>9 - 11 credit hours</strong> - 3⁄4 time status&amp;gt;
                           </li>
                           
                           <li>
                              <strong>6 - 8 credit hours</strong> - 1⁄2 time status
                           </li>
                           
                           <li>
                              <strong>1 - 5 credit hours</strong> - less than 1⁄2 time status
                           </li>
                           
                        </ul>
                        
                        <p>Your enrollment in classes as well as class attendance will be confirmed before aid
                           is paid to your account. If you have withdrawn from any classes, missed the first
                           day, or have not attended regularly, your aid eligibility will be recalculated. If
                           you have withdrawn from all classes or have current enrollment of less than the minimum
                           required hours, you will not be eligible for aid and your scheduled award will be
                           cancelled. Should this occur, you will remain financially responsible for any charges
                           on your account. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/credit-hours.pcf">©</a>
      </div>
   </body>
</html>