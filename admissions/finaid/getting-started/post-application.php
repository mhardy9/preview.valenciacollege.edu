<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Post-Application | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/post-application.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Post-Application </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Post-Application Completion</h2>
                        
                        <h3>What happens after I complete my financial aid applications?</h3>
                        
                        <ol>
                           
                           <li>
                              <strong>Your FAFSA will be processed.</strong> Federal Student Aid Programs will transmit an electronic record to Valencia if you
                              chose Valencia’s school code number, 006750, when you completed your FAFSA. You will
                              also receive a copy of this record, by mail or by e–mail if you provide an e–mail
                              address. You should review the information for accuracy and make corrections, if necessary.
                              It will also indicate if you have been selected for verification and if other information
                              will be needed. Please read the report carefully, and visit a Valencia Answer Center
                              if you have any questions or do not understand the information. You can expect your
                              results to arrive 2–3 weeks from the time of your electronic application.
                           </li>
                           
                           <li>
                              <strong>Additional requirements are established.</strong> If any documents are needed at the time your FAFSA record is received, you will receive
                              an Atlas e–mail notification (in your Atlas account), or a letter if you are not yet
                              admitted to Valencia. Please be sure we have your correct address. (You may update
                              your address and view the missing information requirements on your Atlas
                              account.)
                           </li>
                           
                           <li>
                              <strong>Completed files are reviewed.</strong> The federal government requires that some files be selected for a review called verification.
                              If your file was selected for verification, you cannot be awarded financial aid until
                              your original application has been completely checked for accuracy. Corrections will
                              be made, if required, and your application will be reprocessed. Late applications
                              will be reviewed as workload permits after the term has begun. Please take special
                              care to be accurate with your verification questions and to be sure each form is signed.
                              Inaccurate or incomplete verification documents will create a need for further documentation
                              of your resources and will delay the receipt of financial aid.
                           </li>
                           
                           <li>
                              <strong>Eligibility is determined.</strong> If you are eligible for financial aid, a Financial Aid Services Award Notification
                              will be sent to you outlining the awards you may receive. You may also view the awards
                              in your Atlas account. Be sure to read any special messages about the type of aid
                              you are receiving and ask questions if you are not sure about any of the information.
                              If you are not eligible for any federal or state grants, you will receive an Atlas
                              e–mail notification. 
                           </li>
                           
                        </ol>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/post-application.pcf">©</a>
      </div>
   </body>
</html>