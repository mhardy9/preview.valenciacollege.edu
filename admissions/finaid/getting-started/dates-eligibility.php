<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dates &amp; Eligibility | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/dates-eligibility.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Dates &amp; Eligibility </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Dates &amp; Eligibility</h2>
                        
                        
                        <h3>When should I apply for financial aid? What are the deadlines?</h3>
                        
                        <p>Apply for aid by completing a <a href="http://www.fafsa.ed.gov/" target="_blank">FAFSA</a> each academic year, beginning in January prior to the start of Fall Semester. <strong>Apply as early as possible</strong>. Many financial aid programs have limited funding that will go only to early applicants.
                           As the deadlines approach each term, waiting times to receive personal assistance
                           becomes longer, so an earlier application will be far easier for you. <strong>For best results, apply each year prior to March 15th.</strong></p>
                        
                        
                        <h3>Priority File Completion Deadline Dates:</h3>
                        
                        <p>To have your financial aid processed before your fees are due, you must complete <strong>all steps</strong> in the application process no later than:
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Term (Semester)</th>
                                 
                                 <th>Priority Date</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Summer 2017 (May - August)</td>
                                 
                                 <td>March 24, 2017 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Fall 2017 (August - December)</td>
                                 
                                 <td>July 21, 2017 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Spring 2018 (January - May)</td>
                                 
                                 <td>November 17, 2017</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Summer 2018 (May - August)</td>
                                 
                                 <td>March 23, 2018</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <h3>What if I didn’t meet the above deadlines?</h3>
                        
                        <p>You are still strongly encouraged to apply for financial aid. Your application will
                           be accepted and processed; however, it may not be processed in time for you to use
                           financial aid to pay for tuition or books. If you are late, you will need to pay for
                           your own tuition and books; then, if you qualify for financial aid, a reimbursement
                           check will be issued to you later in the term. You can seek help with your tuition
                           payment through the Tuition Installment Plan (TIPS). Details of this plan can be found
                           on the <a href="/students/business-office/">Business Office website</a>.
                        </p>
                        
                        <h3>Will I be eligible?</h3>
                        
                        <p>Most financial aid programs require financial need and are based on family income.
                           However, in addition to need, you must meet the following criteria:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Be accepted as a degree seeking student in an AA degree or AS career program, or be
                              accepted as a certificate–seeking student in a financial aid eligible vocational training
                              program.
                           </li>
                           
                           <li>Be making satisfactory academic progress (see <a href="../satisfactory-progress.php">Standards of Satisfactory Academic Progress for Financial Aid Recipients</a>).
                           </li>
                           
                           <li>Be a U.S. Citizen, national or eligible non-citizen while receiving Title IV funds.</li>
                           
                           <li>Must be enrolled at least half time (6 credit hours). Some scholarships and grants
                              require three quarters time (9 credit hours) or full time (12 credit hours) enrollment.
                              Courses must be required for the degree program.
                           </li>
                           
                           <li>Not be in default on a prior student loan.</li>
                           
                           <li>Not have been convicted for sale or possession of illegal drugs while receiving Title
                              IV funds.
                           </li>
                           
                           <li>Not owe a repayment or overpayment of a federal grant.</li>
                           
                           <li>Not have an existing financial obligation to Valencia.</li>
                           
                           <li>Be registered for Selective Service, if required.</li>
                           
                           <li>As a first time in college student, you must have a high school diploma or GED certificate
                              or meet Federal Ability to Benefit (ATB) test score guidelines. For students who do
                              not have a standard high school diploma or GED, Valencia has identified the Computerized
                              Placement Tests (CPT) as the only acceptable test for ATB purposes. To receive financial
                              aid, the minimum CPT scores are: Reading 55, Sentence Skills 60, and Arithmetic 34.
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/dates-eligibility.pcf">©</a>
      </div>
   </body>
</html>