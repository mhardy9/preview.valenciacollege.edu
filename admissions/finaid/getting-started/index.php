<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Getting Started  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Getting Started</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        
                        					
                        <h2>Getting Started</h2>
                        
                        					
                        <p>Valencia FAFSA Code: <strong>006750</strong></p>
                        
                        					
                        <ol>
                           						
                           <li>
                              <p><strong>Apply for degree.</strong> Choose the degree you will seek. To receive financial aid, you must seek an Associates
                                 of Arts (A.A.), an Associates of Science (A.S.), or an Associates of Applied Science
                                 (A.A.S) degree or one of the Technical Certificate (T.C.) programs that are approved
                                 for financial aid. If you are undecided, you can get help from a campus Career Center
                                 or on–line through your Atlas Account using the LifeMap Tools.
                              </p>
                           </li>
                           						
                           <li>
                              							
                              <p><strong>Request transcripts.</strong> Federal regulations require all students applying for financial aid to provide the
                                 Admissions Office with documentation certifying your high school graduation, GED or
                                 home school credential.
                              </p>
                              							
                              <p><strong>Transfer students:</strong> Be sure to add Valencia’s <strong>Title IV code <span>006750</span></strong> to your FAFSA.
                              </p>
                              							
                              <p>If you are a college transfer student seeking a degree or technical certificate, you
                                 must submit <strong>all</strong> offical final academic transcripts from each college or university attended. These
                                 must be evaluated for transfer credit before you will be considered for financial
                                 aid.
                              </p>
                              							
                              <p>If you are currently attending another college or university, have the official academic
                                 transcript sent at the end of the term once all grades for classes are listed. Transfer
                                 credits must be included in the calculation for Satisfactory Academic Progress. There
                                 are three satisfactory progress requirements:
                              </p>
                              							
                              <ol>
                                 								
                                 <li>You must maintain at least a 2.0 cumulative grade point average, and</li>
                                 								
                                 <li>You must successfully complete at least 67% of the courses you attempt at all colleges,
                                    and
                                 </li>
                                 								
                                 <li>You may attempt no more than 150% of the total credit hours in your degree or certificate
                                    program.
                                 </li>
                                 							
                              </ol>
                              							<br>
                              							
                              <p>If you attended another college, the courses you took at that college will be considered
                                 in the evaluation of your progress. Your financial aid eligibility cannot be determined
                                 until we have received all of the required transcripts. Please allow adequate time
                                 (at least 30 days) for the evaluation.
                              </p>
                              						
                           </li>
                           						
                           <li>
                              							
                              <p><strong>Complete the Free Application for Federal Student Aid (FAFSA) online at <a href="https://fafsa.ed.gov" target="_blank">fafsa.ed.gov</a>.</strong> Enter Valencia’s School Code (<strong>006750</strong>) as a school who will receive the results of your FAFSA and allow at least 2 weeks
                                 for your form to be processed. Review the <a href="https://www.finaid.sa.ucsb.edu/Media/FAFSASimplification/index.html" target="_blank">FAFSA Tutorial</a> provided by the University of California Santa Barbara (UCSB).
                              </p>
                              							
                              <p>You may choose any of these three methods to file a Free Application for Federal Student
                                 Aid (FAFSA):
                              </p>
                              							
                              <ul>
                                 								
                                 <li>Apply online at <a href="https://www.fafsa.ed.gov/" target="_blank">www.FAFSA.ed.gov</a> (Recommended) or
                                 </li>
                                 								
                                 <li>
                                    									<a href="https://www.fafsa.ed.gov/options.htm" target="_blank">Complete</a> a PDF FAFSA (Note: PDF FAFSAs must be mailed for processing) or
                                 </li>
                                 								
                                 <li>Request a paper FAFSA by calling the Federal Student Aid Information Center at 1-800-4-FED-AID
                                    (1-800-433-3243) or 1-319-337-5665. If you are hearing impaired, please contact the
                                    TTY line at 1-800-730-8913.
                                 </li>
                                 								
                                 <li><a href="https://fafsa.ed.gov/help.htm" target="_blank">The top  Frequently Asked Questions about the FAFSA </a></li>
                                 							
                              </ul>
                              						
                           </li>
                           						
                           <li>
                              <p><strong>Follow up on additional information requests.</strong> Steps 1–3 only get you started. After that, additional forms and documents may be
                                 needed to verify your eligibility, to complete your academic record, or to request
                                 student loans and scholarships. 
                              </p>
                           </li>
                           					
                        </ol>
                        
                        
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/index.pcf">©</a>
      </div>
   </body>
</html>