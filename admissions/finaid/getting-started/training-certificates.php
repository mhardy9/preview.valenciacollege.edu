<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Eligible Vocational Training Certificates | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/training-certificates.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Eligible Vocational Training Certificates </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Eligible Vocational Training Certificates</h2>
                        
                        <ul class="list_style_1">
                           
                           <li>Accounting Applications</li>
                           
                           <li>Baking and Pastry Arts</li>
                           
                           <li>Business Management </li>
                           
                           <li>Computer Information Technology Analyst</li>
                           
                           <li>Computer Programming Analyst</li>
                           
                           <li>Computor Programming w/Specializations (33 Credit Hours)</li>
                           
                           <li>Criminal Justice Technolgy Specialist (24 Credit Hours) </li>
                           
                           <li>Culinary Arts Technical Certificate</li>
                           
                           <li>Digital Media - Webcast Technology </li>
                           
                           <li>Digital Video Editing and Post Production</li>
                           
                           <li>Drafting (Architectural, Mechanical, and Surveying)</li>
                           
                           <li>Film Production Fundamentals </li>
                           
                           <li>Graphic Design Production</li>
                           
                           <li>Graphics - Interactive Media Production</li>
                           
                           <li>Hospitality - Event Planning Management </li>
                           
                           <li>Hospitality - Food and Beverage Management</li>
                           
                           <li>Hospitality - Rooms Division Management</li>
                           
                           <li>Human Resources Management</li>
                           
                           <li>Law Enforcement Officer Academy </li>
                           
                           <li>Microsoft Systems Engineer (MCSE)</li>
                           
                           <li>Paramedic Technology</li>
                           
                           <li>Customer Service Management</li>
                           
                           <li>Landscape / Horticulture Technician </li>
                           
                           <li>Property and Casualty Insurance Management</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/training-certificates.pcf">©</a>
      </div>
   </body>
</html>