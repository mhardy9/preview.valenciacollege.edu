<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Attending &amp; Withdrawing From Classes | Financial Aid Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/getting-started/attending-and-withdrawing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li><a href="/admissions/finaid/getting-started/">Getting Started</a></li>
               <li>Attending &amp; Withdrawing From Classes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Attending &amp; Withdrawing From Classes</h2>
                        
                        <p>Before withdrawing or stopping attendance in classes, the student should be aware
                           of the proper procedure for withdrawing from classes and the consequences of withdrawing
                           or stopping attendance. Official withdraw is the responsibility of the student. Questions
                           on Return of Title IV Funds may be addressed to Financial Aid Services. Questions
                           on withdrawal should be addressed with the Answer Center or an Advisor.
                        </p>
                        
                        
                        <h3>Return of Title IV Financial Aid Funds Policy</h3>
                        
                        <p><strong>Students receiving financial aid who withdraw or stop attending will, in most cases,
                              be required to return a portion of financial aid received. The Higher Education Act,
                              as reauthorized and signed into law on October 7, 1998, established the return of
                              Title IV Funds Policy.</strong></p>
                        
                        <p>This revised Valencia policy reflects new regulations published 10/29/2010 that became
                           law 07/01/2011. The concept behind the policy is that the college and the student
                           are allowed to retain only the amount of Title IV (federal) aid that is earned. If
                           a student withdraws or stops attending classes, whether any credits have been earned
                           for the term or not, a portion of the aid received is considered to be unearned and
                           must be returned to the Title IV programs from which it was received. For Title IV
                           purposes, the withdrawal date is the last date of attendance as determined by attendance
                           records.
                        </p>
                        
                        <p>If a student attends through 60 percent of the term, all Title IV aid is considered
                           earned. However, withdrawing will affect a student’s satisfactory academic progress
                           and eligibility for additional financial aid.
                        </p>
                        
                        
                        <h3>Definitions</h3>
                        
                        <p><strong>Return to Title IV (R2T4) calculation – </strong>a required calculation to determine the amount of aid earned by the student when the
                           student does not attend all days scheduled to complete within a payment period or
                           term. (Student is considered to be a withdrawal, whether any credits were completed
                           or not).
                        </p>
                        
                        <p><strong>Overaward </strong>[not the same as a Return to Title IV calculation] – a required recalculation of Pell
                           Grant and other types of aid types due to student dropping or not attending credits
                           required for the status awarded (full-time, three-quarter time, half-time, less than
                           half-time); required at any point when information is received that changes the student’s
                           enrollment status. Reduction in aid will always be required for students whose status
                           changes due to dropping classes and classes not attended.
                        </p>
                        
                        
                        <h3>Clarification of New Regulations</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>A student who attends and completes at least one course that spans the entire term
                              will have earned the aid for that term (after adjustments for dropped classes or classes
                              not attended).
                           </li>
                           
                           <li>
                              School must be able to demonstrate that student actually attended each class, including
                              any class with a failing grade. Attendance must be “academic attendance” or “attendance
                              at an academically-related activity.” Documentation of Attendance must be made by
                              the school. A student’s self-certification of attendance is NOT acceptable unless
                              supported by school’s documentation. Examples of attendance include:
                              
                              <ul>
                                 
                                 <li>Physical class attendance where there is direct interaction between instructor and
                                    student
                                 </li>
                                 
                                 <li>Submission of an academic assignment</li>
                                 
                                 <li>Study group assigned by the school</li>
                                 
                                 <li>Examination, interactive tutorial, or computer-assisted instruction</li>
                                 
                                 <li>Participation in an online discussion about academic matters</li>
                                 
                                 <li>Initiation of contact with instructor to ask question about academic subject</li>
                                 
                                 <li><strong>Logging in to an on-line class does NOT count as attendance.</strong></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>A student who withdraws from a part-of-term class within the term must still be attending
                              another class or is considered to be a withdrawal, even if registered for future classes
                              starting in the term. The student must – at the time of withdrawal from a part-of
                              term class, if they are not attending another class – provide a written statement
                              to the college indicating their intent to attend a future class within that term,
                              or the student is a withdrawal; a Return to Title IV calculation must be completed.
                              (If student doesn’t actually attend that future class, a Return to Title IV calculation
                              is still required; withdraw date/last date of attendance dates back to originally
                              confirmed date).
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Questions to Ask</h3>
                        
                        <ol>
                           
                           <li>Did the student stop attending a class that he/she was scheduled to attend?
                              
                              <ul>
                                 
                                 <li>If yes, go to question 2</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>At the time the student stopped attending this course was he/she continuing to attend
                              other courses?
                              
                              <ul>
                                 
                                 <li>If yes, the student is not a withdrawal</li>
                                 
                                 <li>If no, go to question 3</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>At the time of withdrawal, did the student provide written confirmation of anticipated
                              attendance in a later starting, registered course within the term?
                              
                              <ul>
                                 
                                 <li>If no, student is considered a withdrawal, and a Return to Title IV calculation must
                                    be completed
                                 </li>
                                 
                                 <li>If yes, no Return to Title IV calculation is required unless the student doesn’t attend
                                    or quits the future part of term class
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                        <p><strong>Remember:</strong> Recalculation of aid for enrollment-status changes due to dropped or never attended
                           classes is required before any Return to Title IV calculation is completed. 
                        </p>
                        
                        
                        <h3>The Return to Title IV Process</h3>
                        
                        
                        <p><strong>Step 1) </strong>The first step is a series of formulas to determine the amount of aid which must be
                           returned. Following the determination of the last date of attendance, the school must
                           calculate the number of days attended and the total number of days the student was
                           scheduled to complete within the term; weekends count and any periods of no classes
                           which are five days in length or greater are excluded. Days attended are then divided
                           by the days in the term the student was scheduled to complete to calculate percentage
                           completed. The percentage is multiplied by total aid for which the student is eligible
                           to determine the amount of aid earned (% completed x total aid = earned aid). <strong>Total aid – earned aid = unearned aid (aid to be returned).</strong></p>
                        
                        <p><strong>Step 2) </strong>The next step is for the school to determine total institutional charges and multiply
                           that figure by the percentage of unearned aid (100% - % completed = % unearned). It
                           makes no difference which type of resource actually paid the school bill; the law
                           assumes that Title IV aid goes first to pay the institutional charges. <strong>Institutional charges x % unearned = amount returned by school.</strong></p>
                        
                        <p>The school must then return the amount of unearned aid, up to the maximum received,
                           to each of the Title IV programs in the following order:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Unsubsidized Direct Stafford Loan</li>
                           
                           <li>Subsidized Direct Stafford Loan</li>
                           
                           <li>Federal Perkins Loan (Valencia does not participate)</li>
                           
                           <li>Direct PLUS Loan</li>
                           
                           <li>Federal Pell Grant</li>
                           
                           <li>Federal Supplemental Education Opportunity Grant (SEOG)</li>
                           
                        </ul>
                        
                        <p><strong>Step 3) </strong>The school then calculates the amount for which the student is responsible by subtracting
                           the amount returned by the school from the total amount which is unearned. That remaining
                           amount is the student’s share and is allocated in the same order as above. <strong>Total amount unearned – amount returned by school = $ amount the student is required
                              to return to Title IV funds. </strong></p>
                        
                        
                        <h3>Important Information</h3>
                        
                        <p>Once the school determines the dollar amounts owed the student will be notified of
                           what he or she owes. Funds that must be returned by the student to the loan programs
                           can be paid in accordance with normal loan repayment terms. For grant dollars that
                           must be paid, the amount due from a student is limited to the amount by which the
                           original grant overpayment amount due from the student exceeds half of the total Title
                           IV grants funds received by the student. A student has 45 days to make repayment and
                           does not have to repay a grant overpayment of $50 or less. Unpaid balances will be
                           reported to NSLDS, the National Student Loan Data System, and turned over to the U.S.
                           Department of Education for collection. <strong>Until overpayments are repaid or satisfactory repayment arrangements have been made,
                              students will be ineligible for further Title IV aid at any institution.</strong> 
                        </p>
                        
                        <p>This policy is separate from the institutional refund policy. Unpaid balances due
                           to Valencia that result from amounts returned to Title IV programs and other sources
                           of aid will be charged back to the student. <strong>The student is also responsible for uncollected tuition to Valencia.</strong></p>
                        
                        <p>If a student does not begin attendance in all classes or ceases attendance during
                           the 100% refund period, aid may have to be reduced to reflect appropriate enrollment
                           status prior to recalculating Return of Title IV Funds. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/getting-started/attending-and-withdrawing.pcf">©</a>
      </div>
   </body>
</html>