<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Cost of Attendance  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/attendance-cost.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Cost of Attendance </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <h2>Cost of Attendance</h2>
                        
                        <p>Cost of Attendance will be determined based on enrolled hours at the time of disbursement.
                           Original awards will be based on full time attendance and adjustments will be made
                           after fee payment and finalized at time of disbursement.
                        </p>
                        
                        <p>The following charts show the average* cost of attendance for a student attending
                           both the Fall and Spring Terms of the <strong>2017-2018</strong> school year (9 month budget for Fall and Spring). The charts differ depending on
                           housing status and state residency: 
                        </p>
                        
                        
                        <table class="table table">
                           
                           <caption>Florida Resident, Living Independently</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td></td>
                                 
                                 <th scope="col"> Full Time (12 credits) </th>
                                 
                                 <th scope="col"> 3/4 Time (9 credits) </th>
                                 
                                 <th scope="col"> Half-Time (6 credits) </th>
                                 
                                 <th scope="col"> Less Than Half-Time (3 credits) </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (lower Division) </th>
                                 
                                 <td>$2,473</td>
                                 
                                 <td>$1,855</td>
                                 
                                 <td>$1,237</td>
                                 
                                 <td>$618</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (upper division) </th>
                                 
                                 <td>$2,693</td>
                                 
                                 <td>$2,019</td>
                                 
                                 <td>$1,346</td>
                                 
                                 <td>$673</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Books</th>
                                 
                                 <td>$1,266</td>
                                 
                                 <td>$950</td>
                                 
                                 <td>$633</td>
                                 
                                 <td>$317</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Room &amp; Board</th>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Transportation</th>
                                 
                                 <td>$2,854</td>
                                 
                                 <td>$2,140</td>
                                 
                                 <td>$1,427</td>
                                 
                                 <td>$714</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Miscellaneous</th>
                                 
                                 <td>$943</td>
                                 
                                 <td>$707</td>
                                 
                                 <td>$472</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Loan</th>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Lower Division </th>
                                 
                                 <td>$18,359</td>
                                 
                                 <td>$16,475</td>
                                 
                                 <td>$14,592</td>
                                 
                                 <td>$1,649</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Upper Division </th>
                                 
                                 <td>$18,579</td>
                                 
                                 <td>$16,639</td>
                                 
                                 <td>$14,701</td>
                                 
                                 <td>$1,704</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           
                           <caption>Florida Resident, Living At Home </caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td></td>
                                 
                                 <th scope="col"> Full Time (12 credits) </th>
                                 
                                 <th scope="col"> 3/4 Time (9 credits) </th>
                                 
                                 <th scope="col"> Half-Time (6 credits) </th>
                                 
                                 <th scope="col"> Less Than Half-Time (3 credits) </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (lower Division) </th>
                                 
                                 <td>$2,473</td>
                                 
                                 <td>$1,855</td>
                                 
                                 <td>$1,237</td>
                                 
                                 <td>$618</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (upper division) </th>
                                 
                                 <td>$2,693</td>
                                 
                                 <td>$2,019</td>
                                 
                                 <td>$1,346</td>
                                 
                                 <td>$673</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Books</th>
                                 
                                 <td>$1,266</td>
                                 
                                 <td>$950</td>
                                 
                                 <td>$633</td>
                                 
                                 <td>$317</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Room &amp; Board</th>
                                 
                                 <td>$5,382</td>
                                 
                                 <td>$5,382</td>
                                 
                                 <td>$5,382</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Transportation</th>
                                 
                                 <td>$2,854</td>
                                 
                                 <td>$2,140</td>
                                 
                                 <td>$1,427</td>
                                 
                                 <td>$714</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Miscellaneous</th>
                                 
                                 <td>$943</td>
                                 
                                 <td>$707</td>
                                 
                                 <td>$472</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Loan</th>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Lower Division </th>
                                 
                                 <td>$12,977</td>
                                 
                                 <td>$11,093</td>
                                 
                                 <td>$9,210</td>
                                 
                                 <td>$1,649</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Upper Division </th>
                                 
                                 <td>$13,197</td>
                                 
                                 <td>$11,257</td>
                                 
                                 <td>$9,319</td>
                                 
                                 <td>$1,704</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <table class="table table">
                           
                           <caption>Non-Florida Resident**</caption>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td></td>
                                 
                                 <th scope="col"> Full Time (12 credits) </th>
                                 
                                 <th scope="col"> 3/4 Time (9 credits) </th>
                                 
                                 <th scope="col"> Half-Time (6 credits) </th>
                                 
                                 <th scope="col"> Less Than Half-Time (3 credits) </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (lower Division) </th>
                                 
                                 <td>$9,383</td>
                                 
                                 <td>$7,037</td>
                                 
                                 <td>$4,692</td>
                                 
                                 <td>$2,346</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Tuition &amp; Fees (upper division) </th>
                                 
                                 <td>$10,262</td>
                                 
                                 <td>$7,697</td>
                                 
                                 <td>$5,131</td>
                                 
                                 <td>$2,566</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Books</th>
                                 
                                 <td>$1,266</td>
                                 
                                 <td>$950</td>
                                 
                                 <td>$633</td>
                                 
                                 <td>$317</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Room &amp; Board</th>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>$10,764</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Transportation</th>
                                 
                                 <td>$2,854</td>
                                 
                                 <td>$2,140</td>
                                 
                                 <td>$1,427</td>
                                 
                                 <td>$714</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Miscellaneous</th>
                                 
                                 <td>$943</td>
                                 
                                 <td>$707</td>
                                 
                                 <td>$472</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Loan</th>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>$59</td>
                                 
                                 <td>-</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Lower Division </th>
                                 
                                 <td>$25,269</td>
                                 
                                 <td>$21,657</td>
                                 
                                 <td>$18,047</td>
                                 
                                 <td>$3,377</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row"> Total 9 month Budget for Upper Division </th>
                                 
                                 <td>$26,148</td>
                                 
                                 <td>$22,317</td>
                                 
                                 <td>$18,486</td>
                                 
                                 <td>$3,597</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p>* It is very important to note that the average expenses are estimated based on local
                           cost of living information for use in calculating financial aid eligibility. Individual
                           expenses will vary based on your choice of lifestyle and living arrangements. 
                        </p>
                        
                        <p>** Non-Florida Residents include out-of-state students and international students.
                           Review <a href="../admissions-records/florida-residency/index.html" target="_blank">Valencia's Information for Residency Classification</a>.
                        </p>
                        
                        
                        <h3>What's it going to Cost to attend Valencia?</h3>
                        
                        <p>College cost, which is sometimes referred to as the Cost of Attendance (COA), includes
                           both direct and indirect costs. Students are advised to review their budget and plan
                           ahead.&nbsp; Financial aid is available to assist with your cost of attendance.&nbsp; 
                        </p>
                        
                        <p>To assist with budgeting for college we encourage students to complete My Financial
                           Planner in Atlas under the LifeMap tab or go to <a href="http://www.financialliteracy101.org/" target="_blank">http://www.financialliteracy101.org/</a> and put in Access Code <strong>VCCLM</strong>.&nbsp;
                        </p>
                        
                        <p>To read more about Paying for College visit <a href="http://www.floridashines.org" target="_blank">Floridashines.org</a>. 
                        </p>
                        
                        
                        <h3>Direct Costs at Valencia</h3>
                        
                        <p>These are costs that the family is billed for by the college—such as tuition and fees.
                           Tuition is set based on a semester credit hour charge. For example, a full-time student
                           enrolled in 4 classes (3 credit hours per class) will be charged for 12 credit hours
                           and any associated course fees. <a href="../business-office/tuition-fees.html" target="_blank">Valencia's Tuition and Fee Schedule</a>. 
                        </p>
                        
                        
                        <h3>Indirect Costs while attending Valencia</h3>
                        
                        <p>These are the costs that don't show up on the college bill. They include books, supplies,
                           and travel, as well as personal expenses such as laundry, telephone, and pizza. Since
                           you will be living off-campus, room and board costs will also be indirect costs. You
                           can control indirect costs to some degree, by making smart spending choices. 
                        </p>
                        
                        
                        <h3>Basic Cost Components Valencia uses to determine your Cost of Attendance (COA) for
                           Federal Student Aid Purposes
                        </h3>
                        
                        
                        <h4>Tuition and Fees – Direct Cost </h4>
                        
                        <p>These are the charges for your education. They may vary based on academic program
                           and number of credit hours attending each semester. The tuition and fee charges will
                           appear on the bill. &nbsp;At Valencia, your COA will represent the average tuition and
                           fees for a student.
                        </p>
                        
                        
                        <h4>Books and Supplies – Indirect Cost</h4>
                        
                        <p>This expense covers your course materials. At Valencia, your Books and Supplies component
                           will represent the average cost for a student. You can charge books and supplies against
                           your financial aid awards at Valencia prior to the start of the semester. 
                        </p>
                        
                        
                        <h4>Room and Board – Indirect Cost</h4>
                        
                        <p>Valencia does not have on-campus housing so it is necessary for students to make an
                           estimate of these expenses.
                        </p>
                        
                        
                        <h4>Transportation – Indirect Cost</h4>
                        
                        <p>Transportation is added to your COA and represents a student’s average cost for transportation.&nbsp;</p>
                        
                        
                        <h4>Miscellaneous – Indirect Cost</h4>
                        
                        <p>The costs for things like laundry and telephone fall under personal expenses. Keep
                           careful track of these as they can quickly build up. 
                        </p>
                        
                        <p>Information was provided by the College Board &amp; Valencia's Financial Aid Services
                           Office.
                        </p>
                        
                        
                        <h4>Disclaimer</h4>
                        
                        <p>The Financial Aid Services Office has taken care to insure the accuracy and timeliness
                           of the information contained in this publication. However, contents are subject to
                           change without notice because of changing federal, state or institutional policies.
                           All financial aid awards are contingent upon availability of funds and the student’s
                           final eligibility determination. 
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/attendance-cost.pcf">©</a>
      </div>
   </body>
</html>