<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/finaid/questions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/finaid/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Financial Aid Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/finaid/">Financial Aid Services</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        
                        <ol>
                           
                           <li><a href="#parentsinfo">The FAFSA asks for my parents information, but I am on my own. What do I do?</a></li>
                           
                           <li><a href="#lostincome">The FAFSA is based on last year's income, but I lost my job since then. Now I have
                                 no income. Do I have any options for having this considered?</a></li>
                           
                           <li><a href="#appeal">How can I appeal my suspension?</a></li>
                           
                           <li><a href="#repayment">I heard that if I withdraw from my classes I might need to repay my financial aid.
                                 Is this true?</a></li>
                           
                           <li><a href="#bookstore">Can I charge books to my grant award in the bookstore?</a></li>
                           
                           <li><a href="#disbursement">How will I receive my disbursement funds?</a></li>
                           
                           <li><a href="#withdrawal">What if I can't attend my classes or need to withdraw?</a></li>
                           
                        </ol>              
                        
                        
                        <h3 id="parentsinfo">The FAFSA asks for my parents' information, but I am on my own. What do I do?</h3>
                        
                        <p>If you are over age 24, married, a Veteran of the armed forces, an orphan or ward
                           of the court, or if you have dependents of your own that you support, you will not
                           need to report your parent's information. If you ordinarily would need to report parent
                           information, but are unable to do so because of an unusual situation, you should meet
                           with a Financial Aid Services Specialist to discuss your situation and inquire about
                           your options. You may be asked to document your situation in writing.
                        </p> 
                        
                        
                        <h3 id="lostincome">The FAFSA is based on last year's income, but I lost my job since then. Now I have
                           no income. Do I have any options for having this considered?
                        </h3>
                        
                        <p>If you or your parents have experienced a significant decrease in income, or have
                           paid unusually large medical expenses this year, you should meet with a Financial
                           Aid Services Specialist to discuss your situation and inquire about your options.
                           You may be asked to provide detailed documentation of your situation in writing in
                           order to receive special consideration.
                        </p>
                        
                        
                        <h3 id="appeal">How can I appeal my suspension?</h3>
                        
                        <p>Once you are suspended from financial aid, you cannot receive it any further unless
                           you can prove that the reasons you couldn't meet the requirement were unusual, unavoidable,
                           and beyond your control. To prove this, you may file an appeal with the Financial
                           Aid Services Appeals Committee. Approval of your appeal is not guaranteed. It is up
                           to you to convince the committee with your written appeal that your situation was
                           truly mitigating. The committee will also look favorably on any proof you can provide
                           that you now have your problems under control so they won't happen again. Please note
                           that only written appeals are accepted and there is a deadline each term.
                        </p>
                        
                        
                        <h3 id="repayment">I heard that if I withdraw from my classes I might need to repay my financial aid.
                           Is this true?
                        </h3>
                        
                        <p>Yes. The federal financial aid programs require you to "earn" your aid by attending
                           classes. If you receive federal financial aid and withdraw from all of your classes,
                           your aid "earned" will be prorated based on your class attendance. The rules are a
                           bit complicated, so we encourage you to meet with a Financial Aid Services Specialist
                           if you receive federal aid and need to withdraw.
                        </p>
                        
                        
                        <h3 id="bookstore">Can I charge books to my grant award in the bookstore?</h3>
                        
                        <p>If your financial aid program will cover book expenses, you will be able to charge
                           your books during the posted bookstore charging times. Check your Award Letter and
                           your Atlas Financial Aid Information for messages about limitations on your financial
                           aid. We encourage you to make sure you have your final class schedule before buying
                           your books. Be sure to save your receipt and don't write in your books until classes
                           have actually begun.
                        </p>
                        
                        
                        <h3 id="disbursement">How will I receive my disbursement funds?</h3>
                        
                        <p>All Financial Aid disbursements will be processed in accordance with your refund preference
                           - <a href="../refund/index.html">Valencia Refund</a>. You can visit the <a href="../refund/index.html">Valencia Refund</a> site to learn more about your options.
                        </p>
                        
                        
                        <h3 id="withdrawal">What if I can't attend my classes or need to withdraw?</h3>
                        
                        <p>You can make changes to your schedule at any time during the add-drop period without
                           any problems. After the add-drop period, the course will count as an "attempt" and
                           will remain permanently on your academic record. Remember, you must attend your classes
                           to receive financial aid payment.
                        </p>
                        <strong>
                           <p>Do not simply stop going to class without submitting an official withdrawal! You could
                              be required to pay back the entire amount of your financial aid!
                           </p></strong>
                        
                        <p>If you receive federal aid and you withdraw from all of your classes prior to the
                           mid-term withdrawal deadline, you may be required to repay a portion of your financial
                           aid. We urge you to complete your classes if at all possible to avoid complications
                           with your future financial aid.
                        </p>
                        
                        <p>If you cannot attend a class, you should notify your professor as soon as possible
                           and if you need to withdraw, use your Atlas account to enter the withdrawal. <strong>Do not simply stop going to class!</strong> You must complete at least 67% of your attempted courses to remain eligible for financial
                           aid. Please be sure you are familiar with the rules for "Satisfactory Academic Progress
                           for Financial Aid Recipients" and meet with a Financial Aid Services Specialist if
                           you have any questions.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/finaid/questions.pcf">©</a>
      </div>
   </body>
</html>