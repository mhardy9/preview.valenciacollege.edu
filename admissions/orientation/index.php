<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Orientation  | Valencia College</title>
      <meta name="Description" content="New Student Orientation at Valencia college">
      <meta name="Keywords" content="orientation, new student, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/orientation/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/orientation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>New Student Orientation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li>Orientation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Welcome to Valencia College!</h2>
                        
                        <div>
                           
                           <p>Congratulations, you're going to college!</p>
                           
                           <p>Before registering for classes, all degree-seeking students (freshmen and transfer)
                              are required to attend an Orientation session. Please read through all of the information
                              before signing up for Orientation.
                           </p>
                           
                           <p>Save Your Seat for Orientation! Sessions fill up fast, so be sure to sign up and attend
                              early!
                           </p>
                           
                           <p>Your Orientation will provide you with the information and resources to help you Start
                              Right at Valencia. Your Orientation is broken down into two parts
                           </p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <h3>Orientation Part One</h3>
                              
                              <p>This part is completely <strong>online</strong> &amp; will cover the following topics:
                              </p>
                              
                              <ul>
                                 
                                 <li>Florida Residency for Tuition Purposes</li>
                                 
                                 <li>Assessment/Reviews</li>
                                 
                                 <li>Transcripts</li>
                                 
                                 <li>Your Success Plan</li>
                                 
                                 <li>Your Pathway to Valencia</li>
                                 
                                 <li>Financial Aid and Scholarships</li>
                                 
                                 <li>Preparation for your on-campus Orientation session</li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <h3>Orientation Part Two</h3>
                              
                              <p>Part two is <strong>on Campus</strong> and will cover the following topics:
                              </p>
                              
                              <ul>
                                 
                                 <li>Welcome to Campus</li>
                                 
                                 <li>Assistance with Admissions and Financial Aid</li>
                                 
                                 <li>Academic advising for first term</li>
                                 
                                 <li>Registration for first term</li>
                                 
                                 <li>Campus Tour</li>
                                 
                                 <li>Overview of Learning Support and Campus Safety</li>
                                 
                                 <li>Meet with new students (and make new friends!)</li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-4">
                                 
                                 <h3>Orientation Resources</h3>
                                 
                                 <ul>
                                    
                                    <li><a href="/locations/">Campus locations/maps</a></li>
                                    
                                    <li><a href="/academics/calendar/">Academic Calendar</a></li>
                                    
                                    <li><a href="http://catalog.valenciacollege.edu">Catalog</a></li>
                                    
                                    <li><a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas Login</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-4">
                                 
                                 <h3>Online Orientation</h3>
                                 
                                 <p>This part must be completed online and can be done anywhere you have internet access
                                    and at your convenience. If you prefer to use a computer on campus, you may do so
                                    at any of our campus <a href="/STUDENTS/offices-services/student-services/atlas-access-labs.html">Atlas Access Labs</a>.
                                 </p>
                                 
                                 <ul class="list_order">
                                    
                                    <li><span>1</span>
                                       
                                       <h4>Sign Up for an Atlas Account</h4>
                                       
                                       <ul class="list-unstyled">
                                          
                                          <li>Go to <a href="https://atlas.valenciacollege.edu">Atlas</a></li>
                                          
                                          <li>Click on the "Sign Up for an Account" link on the login page</li>
                                          
                                          <li>You will be asked to Verify your personal information</li>
                                          
                                          <li>You can then set up your Password &amp; Secret Question</li>
                                          
                                          <li>It may take 5-10 minutes before you can log into your account</li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li><span>2</span>
                                       
                                       <h4>Log Into Atlas</h4>
                                       
                                       <p>Once you log into <a href="https://atlas.valenciacollege.edu">Atlas</a>, click on New Student Orientation to begin.
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>3</span>
                                       
                                       <h4>Take your Online Orientation</h4>
                                       
                                       <p>Follow instructions to begin Part One of Orientation online.</p>
                                       
                                    </li>
                                    
                                    <li><span>4</span>
                                       
                                       <h4>Schedule next Step</h4>
                                       
                                       <p>Once you complete the online portion of Orientation, you will then schedule your on
                                          campus Orientation session.
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-8">
                                 
                                 <h3>On Campus Orientation</h3>
                                 
                                 <p>Be prepared! Here’s what you should bring on the day of your on campus Orientation
                                    session
                                 </p>
                                 
                                 <ul class="list_order">
                                    
                                    <li><span>1</span>
                                       
                                       <h4>Valid Photo Identification</h4>
                                       
                                       <p>A government Issued photo ID (such as a Driver’s License, Passport, State ID)</p>
                                       
                                    </li>
                                    
                                    <li><span>2</span>
                                       
                                       <h4>Print your On-Campus Orientation Confirmation E-mail</h4>
                                       
                                       <p>This e-mail shows the date, time and campus of your on-campus Orientation session.</p>
                                       
                                    </li>
                                    
                                    <li><span>3</span>
                                       
                                       <h4>Your Atlas Login Information</h4>
                                       
                                       <p>Make sure you have your username and password since you will need this information
                                          to register for classes at Orientation.
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>4</span>
                                       
                                       <h4>Transcripts</h4>
                                       
                                       <p>Students transferring in with college credits (including Dual Enrollment credit completed
                                          at another institution) should bring official college transcripts (in the original,
                                          sealed envelope from the sending institution) if not previously submitted. Students
                                          with AP or IB coursework should have official score reports sent directly to Valencia
                                          and can bring in unofficial score reports to review with advisors at Orientation.
                                          If not previously submitted, official (in the original, sealed envelope from the institution)
                                          high school transcripts including high school graduation date can be submitted to
                                          Admissions staff at Orientation or to the Answer Center upon arrival at the campus.
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>5</span>
                                       
                                       <h4>Paper/Notepad</h4>
                                       
                                       <p>In case you would like to take additional notes during the Orientation session, you
                                          are welcome to bring a small notebook or notepad. Valencia will provide the rest of
                                          your Orientation materials.
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>6</span>
                                       
                                       <h4>Jacket or Sweater</h4>
                                       
                                       <p>Optional: in case you get cold in the Orientation room!</p>
                                       
                                    </li>
                                    
                                    <li><span>7</span>
                                       
                                       <h4>Comfortable Shoes</h4>
                                       
                                       <p>Optional: for campus tours</p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Most importantly, BRING YOURSELF!</p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/orientation/index.pcf">©</a>
      </div>
   </body>
</html>