<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Orientation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/orientation/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/orientation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>New Student Orientation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/orientation/">Orientation</a></li>
               <li>New Student Orientation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3></h3>
                                 
                                 <p></p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3></h3>
                                 
                                 <p></p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3></h3>
                                 
                                 <p></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Do I have to attend Orientation? </h3>
                                 
                                 <p>Yes, all degree-seeking students are required to attend Orientation.</p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Can I do an online Orientation?</h3>
                                 
                                 <p> Yes, all students are required to complete a two-part Orientation, with an online
                                    and an on-campus section.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Can I register for classes before Orientation?</h3>
                                 
                                 <p> No, all degree-seeking students have a registration hold that will be removed upon
                                    successful completion of Orientation.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>What do I need to bring to Orientation? </h3>
                                 
                                 <p>A government issued photo ID such as a driver's license or passport is required for
                                    sign-in. See Orientation Checklist page for more information.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>What will I do in Orientation? </h3>
                                 
                                 <p>During On-Campus Orientation, you will be provided valuable information about Valencia
                                    and preparing to start right in school. You will be introduced to college resources,
                                    culture, and polices. You will learn about your major or program of study and will
                                    register for your first classes. Additionally, you will have the opportunity to meet
                                    one-on-one with an advisor and meet other students.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Can I bring a friend?</h3>
                                 
                                 <p> No, guests are typically not permitted in Orientation, but please feel free to refer
                                    to the parent page for more information on resources and upcoming events for parents
                                    and guests.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Why is Orientation so long?</h3>
                                 
                                 <p> During Orientation you will be introduced to college resources, culture, and polices.
                                    You will learn about your major or program of study and will register for your first
                                    classes. Additionally, you will have the opportunity to meet one-on-one with an advisor.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Will I be able to register for classes at Orientation? </h3>
                                 
                                 <p>Yes, Orientation holds will be removed so you will be able to register the same day.</p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>How long is Orientation on campus? </h3>
                                 
                                 <p>Orientation will last three hours.</p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>Can I come in late or leave early if I have to work, have a doctor’s appointment,
                                    etc.? 
                                 </h3>
                                 
                                 <p>No, if you happen to arrive late or leave before the completion of Orientation, the
                                    Orientation hold will remain and it will be necessary to register for another session
                                    through your Atlas account. It is important to arrive on time for check in, and stay
                                    in attendance for the entire Orientation.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>I cannot make my scheduled Orientation, can I reschedule? </h3>
                                 
                                 <p>Yes, however 48 hours is preferred because the college prepares paperwork to advise
                                    students at on-campus orientation. To change sessions, please login in to your Atlas
                                    account.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="col-md-4">
                              
                              <div class="box_style_2">
                                 
                                 <h3>My financial aid, residency, or transcripts are not up to date/on file, how can I
                                    take care of this? 
                                 </h3>
                                 
                                 <p>We have representatives on campus to discuss these concerns during our hours of operation.
                                    You can stop by any campus Answer Center for assistance.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/orientation/faqs.pcf">©</a>
      </div>
   </body>
</html>