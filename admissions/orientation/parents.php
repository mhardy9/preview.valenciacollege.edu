<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Orientation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/orientation/parents.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/orientation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>New Student Orientation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/orientation/">Orientation</a></li>
               <li>New Student Orientation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Orientation Information for Parents</h2>
                        
                        <p>Separate Parent and Family&nbsp;Orientations will be offered throughout the year - please
                           see below for dates
                        </p>
                        
                        <p>Parent and Family&nbsp;Orientation booklets will be available at all student orientations.
                           You may also download the current <a href="/admissions/orientation/documents/parent-family-materials.pdf" target="_blank">Parents &amp; Family Booklet</a></p>
                        
                        <div class="row">
                           
                           <div class="col-md-5">
                              
                              <h3>Parent and Family Orientation&nbsp;Sessions</h3>
                              
                              <p>Since Orientation is for new students only, we welcome parents, family and friends
                                 to your own&nbsp;orientation session because you play such an important role in your student's
                                 success! No RSVP is required. Topics include educational support resources, residency,
                                 financial aid, campus safety and much more! Talk to college administrators and faculty
                                 and have your questions answered! Choose the campus location and date that fits your
                                 schedule. The same information will be covered at all sessions.
                              </p>
                              
                              <h4 dir="auto">EAST</h4>
                              
                              <p dir="auto">TBA</p>
                              
                              <h4 dir="auto">OSCEOLA</h4>
                              
                              <p dir="auto">TBA</p>
                              
                              <h4 dir="auto">WEST</h4>
                              
                              <p dir="auto">TBA</p>
                              
                              <h4 dir="auto">LAKE NONA</h4>
                              
                              <p dir="auto">Parents are welcome to attend other campus locations for parent orientation sessions.
                                 You do not need to attend the same date and location as your student.
                              </p>
                              
                              <h4 dir="auto">POINCIANA</h4>
                              
                              <p dir="auto">Parents are welcome to attend other campus locations for parent orientation sessions.
                                 You do not need to attend the same date and location as your student.
                              </p>
                              
                              <h4 dir="auto">WINTER PARK</h4>
                              
                              <p dir="auto">Parents are welcome to attend other campus locations for parent orientation sessions.
                                 You do not need to attend the same date and location as your student.
                              </p>
                              
                           </div>
                           
                           <div class="col-md-7">
                              
                              <div>
                                 
                                 <h3>Conversations to have with your Student before Orientation</h3>
                                 
                                 <ul class="list_order">
                                    
                                    <li><span>1</span>
                                       
                                       <h4>Discuss financial aid and funding for college</h4>
                                       
                                       <p>Remind your student to check Atlas email frequently to know the most current status
                                          in the financial aid process.
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>2</span>
                                       
                                       <h4>Discuss your student's obligations to the rest of the family.</h4>
                                       
                                       <p>Are you expecting them to pick up siblings from school? Will you need to take over
                                          some child care responsibilities? Help with elderly family members?
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>3</span>
                                       
                                       <h4>Discuss your student's work schedule and the number of classes they want to take.</h4>
                                       
                                       <p>Remember, for every one hour in class, students need to spend two hours outside of
                                          class studying. Is the schedule realistic for the demands to be met at work and school?
                                          What can you do to make sure your student has the time to study?
                                       </p>
                                       
                                    </li>
                                    
                                    <li><span>4</span>
                                       
                                       <h4>Discuss proof of residency</h4>
                                       
                                       <p>The school does not know if your student is a Florida resident without proper paperwork
                                          (usually provided by parents). Be sure you check Valencia's <a href="/admissions/">Admissions page</a> for more information.
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Champion your student</h3>
                                 
                                 <ul class="list_order">
                                    
                                    <li><span>1</span>
                                       
                                       <h4>Empower your student to become independent.</h4>
                                       
                                    </li>
                                    
                                    <li><span>2</span>
                                       
                                       <h4>Help your student set aside 2 hours for every 1 hour in class - to study, read and
                                          do homework.
                                       </h4>
                                       
                                    </li>
                                    
                                    <li><span>3</span>
                                       
                                       <h4>Help your student pay attention to deadlines = they're important</h4>
                                       
                                    </li>
                                    
                                    <li><span>4</span>
                                       
                                       <h4>Provide support and encouragement when assignments get touch, often during the middle
                                          of the semester and exam time.
                                       </h4>
                                       
                                    </li>
                                    
                                    <li><span>5</span>
                                       
                                       <h4>College changes people. Your student will encounter new ideas and skills - and grow
                                          as a result. Ask your student about what he or she is learning.
                                       </h4>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Preview your student's Online Orientation here</h3>
                        
                        <p>Simply click on the section and navigate to the menu bar that appears on the left.
                           This version does not contain the assessments which students are required to complete
                           prior to On-Campus Orientation. Students must complete their version via their Atlas
                           account.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/admissions/orientation/stories/section-1/story_html5.html" target="_blank">Section 1</a> Residency, Assessment, Meta-Majors
                           </li>
                           
                           <li><a href="/admissions/orientation/stories/section-2/story_html5.html" target="_blank">Section 2</a> Financial Aid, AA and AS Degrees, Direct Connect
                           </li>
                           
                           <li><a href="/admissions/orientation/stories/section-3/story_html5.html" target="_blank">Section 3</a> Valencia's Campuses, Developmental Education
                           </li>
                           
                           <li><a href="/admissions/orientation/stories/section-4/story_html5.html" target="_blank">Section 4</a> NSE Course (SLS 1122), Scheduling, Next Steps, Review
                           </li>
                           
                        </ul>
                        
                        <h3>Campus Contacts</h3>
                        
                        <ul class="list_staff">
                           
                           <li>
                              
                              <h4>East Campus</h4>
                              
                              <p><a href="mailto:nsoeast@valenciacollege.edu">nsoeast@valenciacollege.edu</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <h4>West Campus</h4>
                              
                              <p><a href="mailto:nsowest@valenciacollege.edu">nsowest@valenciacollege.edu</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <h4>Winter Park Campus</h4>
                              
                              <p><a href="mailto:nsowinterpark@valenciacollege.edu">nsowinterpark@valenciacollege.edu</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <h4>Osceola Campus</h4>
                              
                              <p><a href="mailto:nsoosceola@valenciacollege.edu">nsoosceola@valenciacollege.edu</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <h4>Lake Nona Campus</h4>
                              
                              <p><a href="mailto:nsolakenona@valenciacollege.edu">nsolakenona@valenciacollege.edu</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <h4>Poinciana Campus</h4>
                              
                              <p><a href="mailto:nsopoinciana@valenciacollege.edu">nsopoinciana@valenciacollege.edu</a></p>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/orientation/parents.pcf">©</a>
      </div>
   </body>
</html>