<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Enrollment Verification  | Valencia College</title>
      <meta name="Description" content="Enrollment Verification, Admissions and Records">
      <meta name="Keywords" content="enrollment verification, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/enrollment-verification.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Enrollment Verification </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Enrollment Verification</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Through EnrollmentVerify, providers of student-based services can obtain immediate,
                              affordable online
                              enrollment verifications. EnrollmentVerify is designed to fulfill the verification
                              requirements of
                              companies that offer products or services requiring proof of a student's enrollment
                              status.
                              EnrollmentVerify users include companies such as credit issuers, insurance companies,
                              travel companies,
                              and others. All Enrollment Verifications at Valencia College will be through the National
                              Student
                              Clearinghouse only. 
                           </p>
                           
                           
                           <h3>Frequently Asked Questions about Enrollment Verification</h3>
                           
                           <h4>I'm a student. How do I request Enrollment Verification (proof of enrollment)?</h4>
                           
                           <p> The Admissions &amp; Records Office can now provide current students with the ability
                              to securely access
                              and print free enrollment verifications 24 hours a day, 7 days a week. Please note:
                              Current term
                              Enrollment Verifications will not be available until approximately 5 business days
                              <strong>after</strong>
                              the Valencia College Drop/Refund Deadline. The Drop/Refund Deadlines can be found
                              in the online <a href="http://valenciacollege.edu/calendar/">Important Dates &amp; Deadlines</a> calendar. Valencia
                              College submits data to the National Student Clearinghouse once a month. View our
                              brief <a href="#enrollment-verfication-tutorial">tutorial</a> to learn how to obtain your enrollment verification
                              through your Atlas account. 
                           </p>
                           
                           <h4>I'm not a student but need to verify a student's enrollment. How do I request Enrollment
                              Verification
                              for a student?
                           </h4>
                           Valencia College has authorized the National Student Clearinghouse to provide enrollment
                           verifications.
                           Businesses and employers may go to the <a href="http://www.studentclearinghouse.org/" target="_blank">National
                              Student Clearinghouse</a> website or contact the National Student Clearinghouse at:
                           
                           <blockquote>
                              <strong>Phone:</strong> (703) 742-4200<br> <strong>Fax:</strong> (703) 742-4239 <br> <strong>E-mail:</strong>
                              <a href="mailto:service@studentclearinghouse.org">service@studentclearinghouse.org</a> <br>
                              <strong>Mail:</strong> National Student Clearinghouse / 13454 Sunrise Valley Drive, Suite 300 / Herndon,
                              VA 20171
                              
                           </blockquote>
                           
                           <h4>How long does it take to process the Enrollment Verification request?</h4>
                           
                           <p>Enrollment Verification is available 24 hours a day 7 days a week. The Enrollment
                              Verification
                              certificate is available immediately after submitting the request.
                           </p>
                           
                           <h4>Does Valencia College charge a fee for Enrollment Verifications?</h4>
                           
                           <p>Valencia College does not charge a fee for Enrollment Verification. </p>
                           
                           <p>&nbsp;</p>
                           
                           <a id="enrollment-verfication-tutorial"></a>
                           
                           <h3>Enrollment Verification Tutorial</h3>
                           
                           <ol>
                              
                              <li>Log into your Atlas account</li>
                              
                              <li>Click on the Students tab</li>
                              
                              <li>Select <strong>Admissions &amp; Records </strong> from the <strong>Student Forms </strong> box on the
                                 left side of the page
                                 
                              </li>
                              
                              <li>Select <strong>Enrollment Verification</strong> from the list of options
                              </li>
                              
                              <li>You will be automatically directed to the National Student Clearinghouse secure site</li>
                              
                              <li>Click on <strong>Obtain an enrollment certificate</strong> (you may choose to receive enrollment
                                 verification for the current term only or for all enrollment terms).&nbsp; Note that current
                                 term enrollment
                                 verification is not available until approximately five (5) business days after the
                                 end of the add/drop
                                 period.&nbsp; Valencia College submits enrollment data to the Clearinghouse once a month.
                                 
                              </li>
                              
                              <li>After obtaining your enrollment certificate be sure to click on Log Out in order to
                                 leave the secure
                                 National Student Clearinghouse website.&nbsp; You will be returned to your Atlas session.
                                 
                              </li>
                              
                           </ol>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/enrollment-verification.pcf">©</a>
      </div>
   </body>
</html>