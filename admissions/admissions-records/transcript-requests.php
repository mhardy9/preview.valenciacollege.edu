<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transcript Requests  | Valencia College</title>
      <meta name="Description" content="Transcript Requests, Admissions and Records">
      <meta name="Keywords" content="official transcript, transcript requests, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/transcript-requests.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Transcript Requests </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Transcript Requests</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Official transcript requests are made through your <a href="https://atlas.valenciacollege.edu/">Atlas</a>
                              account. There is a $3.00 fee for each official transcript requested.
                           </p>
                           
                           <p>Valencia College cannot process requests submitted via e-mail, fax or over the telephone.&nbsp;
                              Both Federal
                              and State laws on student privacy require that we have your original signature on
                              the request.
                           </p>
                           
                           <p>We cannot, under any circumstances, fax or e-mail official transcripts.&nbsp; Additionally,
                              we cannot provide
                              an official transcript if there is a hold on your student account. Please be sure
                              to include the <strong>exact
                                 mailing address</strong> of where the official transcript is to be mailed with <strong>your
                                 signature. </strong>Official transcripts are only mailed. We do not provide the option for in-person
                              pick-up.
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Active Students</h4>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You are considered an active student if you <strong>have</strong> registered for at least one course
                                    within the last two years.
                                 </p>
                                 
                                 <p>You must submit your Official Transcript Request through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> Account. For assistance with your Atlas account
                                    please visit an <a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Atlas
                                       Access Lab</a>.
                                 </p>
                                 
                                 <p>Instructions:</p>
                                 
                                 <ul>
                                    
                                    <li> Log in to your Atlas account</li>
                                    
                                    <li>Click on the <strong>Students</strong> tab
                                    </li>
                                    
                                    <li>Click on the <strong>Transcript Request</strong> link under the Admissions &amp; Records section
                                       of the Student Forms channel
                                       
                                    </li>
                                    
                                    <li>Follow the instructions for choosing an address, then click <strong>Continue</strong>
                                       
                                    </li>
                                    
                                    <li>Select the <strong>Student Transcript</strong> Transcript Type and the <strong>Credit</strong>
                                       Course Level
                                       
                                    </li>
                                    
                                    <li>Review the mailing address to make sure it is correct and then click <strong>Continue</strong>
                                       
                                    </li>
                                    
                                    <li>Fill in required information and click <strong>Continue</strong>
                                       
                                    </li>
                                    
                                    <li>Review information and click on <strong>Submit Request</strong>
                                       
                                    </li>
                                    
                                    <li>Enter Payment information ($3.00 per transcript)<br>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Inactive Students</h4>
                           
                           <p>You are considered an inactive student if you <strong>have not</strong> registered for any courses in the
                              last two years.
                           </p>
                           
                           <p>Inactive students need to set up an <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account to request their transcript. For
                              assistance with your Atlas account please visit an <a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Atlas Access Lab</a>.
                           </p>
                           
                           <p>Instructions:</p>
                           
                           <ul>
                              
                              <li>Log in to your Atlas account</li>
                              
                              <li>Click on the <strong>Students</strong> tab
                              </li>
                              
                              <li>Click on the <strong>Transcript Request</strong> link under the Admissions &amp; Records section of
                                 the Student Forms channel
                                 
                              </li>
                              
                              <li>Follow instructions on page for choosing an address, then click <strong>Continue</strong>
                                 
                              </li>
                              
                              <li>Select the <strong>Student Transcript </strong>Transcript Type and the <strong>Credit</strong> Course
                                 Level
                                 
                              </li>
                              
                              <li>Review the mailing address to make sure it is correct and then click <strong>Continue</strong>
                                 
                              </li>
                              
                              <li>Review information and click <strong>Submit Request</strong>
                                 
                              </li>
                              
                              <li>Enter payment information ($3.00 per transcript)</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           <h3>Payment Options</h3>
                           
                           <p>If you do not have a debit or credit card, you may print and complete the <a href="/documents/admissions/admissions-records/valencia-college-transcript-request-form.pdf" target="_blank">Transcript Request Form</a>. 
                           </p>
                           
                           <p><em><strong>Note: </strong>Right-click to download and save file to your desktop. "Save link as" <br> Do
                                 not open PDF in a web browser. </em></p>
                           
                           <p>Incomplete forms and forms that do not include payment will not be processed. The
                              form must be submitted
                              to the <a href="http://valenciacollege.edu/businessoffice/">Business Office</a> with your payment (cash,
                              check, or money order).&nbsp; Please make checks or money orders payable to <em>Valencia College</em>.&nbsp; The
                              $3.00 per transcript fee must be paid before your request will be processed. 
                           </p>
                           
                           <p>This form can be submitted in person to the Business Office on any campus or mailed
                              to:
                           </p>
                           
                           <blockquote>
                              
                              <p>Valencia College<br> Attn: Business Office<br> Mail Code: 3-11 <br> 701 North Econlockhatchee Trail<br>
                                 Orlando, FL 32825
                              </p>
                              
                           </blockquote>
                           
                           <p><em>Transcript requests are typically processed within two (2) business days. </em></p>
                           
                           <p>&nbsp;</p>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/transcript-requests.pcf">©</a>
      </div>
   </body>
</html>