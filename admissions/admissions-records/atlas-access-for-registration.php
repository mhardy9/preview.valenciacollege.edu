<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Atlas Access for Registration  | Valencia College</title>
      <meta name="Description" content="Atlas Access for Registration, Admissions and Records">
      <meta name="Keywords" content="atlas access, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/atlas-access-for-registration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Atlas Access for Registration </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Atlas Access for Registration</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p> If you do not have access to a computer off campus, you may register for classes
                              on campus at any of the
                              following locations: 
                           </p>
                           
                           
                           <h3><a href="http://valenciacollege.edu/labs/east.cfm">East Campus Lab Hours</a></h3>
                           
                           <ul>
                              
                              <li>Atlas Access Lab - Building 5, Room 213</li>
                              
                              <li>Library Computer Lab - Building 4, Room 201</li>
                              
                              <li>Student Computer Center - Building 4, Rooms 122 &amp; 123</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3><a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Lake Nona Campus Lab
                                 Hours</a></h3>
                           
                           <ul>
                              
                              <li>Atlas Access Lab - Building 1, Room 147</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3><a href="http://valenciacollege.edu/labs/osceola.cfm">Osceola Campus Lab Hours</a></h3>
                           
                           <ul>
                              
                              <li>Atlas Access Lab - Building 2, Room 105</li>
                              
                              <li>Library - Building 4, Room 202</li>
                              
                              <li>Learning Center - Building 3, Room 100</li>
                              
                              <li>Computer Stations - Building 1, Atrium</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3><a href="http://valenciacollege.edu/labs/west.cfm">West Campus Lab Hours</a></h3>
                           
                           <ul>
                              
                              <li>Atlas Access Lab - Student Services Building (SSB), Room 172</li>
                              
                              <li>Computer Access Lab - Building 6, Room 101\</li>
                              
                              <li>Library - Building 6, Room 201</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h3><a href="http://valenciacollege.edu/labs/winterpark.cfm">Winter Park Campus Lab Hours</a></h3>
                           
                           <ul>
                              
                              <li>Atlas Access Lab - Room 217</li>
                              
                              <li>Library - Room 140</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/atlas-access-for-registration.pcf">©</a>
      </div>
   </body>
</html>