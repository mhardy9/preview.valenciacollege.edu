<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Wait List  | Valencia College</title>
      <meta name="Description" content="Wait List, Admissions and Records">
      <meta name="Keywords" content="wait list, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/registration-wait-list.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Wait List </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Wait List</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p> Some course sections provide wait lists which will start when all of the seats for
                              the class are taken.
                              Any student attempting to register will be told that no seats are available but they
                              can choose to be
                              placed on a wait list ('wait listed') for that course. Courses that offer the wait
                              list feature can be
                              identified by the registration 'error' message that is received when someone attempts
                              to register for
                              them:
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <strong>CLOSED-Waitlist 003</strong> indicates that this course is full and 3 students are already on
                                 the wait list. You can add your name to the waitlist if you wish.
                                 
                              </li>
                              
                              <li>
                                 <strong>OPEN-Waitlist 002</strong> indicates that this course has open seats but they belong to
                                 individuals who are on the wait list. You can add your name to the wait list for future
                                 open seats if
                                 you wish.
                                 
                              </li>
                              
                              <li>
                                 <strong>CLOSED-Waitlist Full</strong> indicates that this course is full and so is the waitlist. You
                                 cannot add your name to the wait list.
                                 
                              </li>
                              
                              <li>
                                 <strong>OPEN-Waitlist Full</strong> indicates that some students on the waitlist have been offered
                                 seats so spots will become available on the waitlist when they are processed. You
                                 cannot add your name
                                 to the wait list but you should check back later for an update.
                                 
                              </li>
                              
                           </ul>
                           
                           <p>To add yourself to a wait list, choose 'Waitlisted' from the drop down menu and click
                              <strong>Submit
                                 Changes</strong> after you receive one of the errors above. 
                           </p>
                           
                           <p>If you have chosen to be wait listed and you receive an e-mail via your <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account notifying you that a seat has
                              become available, you will have 24 hours to respond to the e-mail or the seat will
                              be offered to the next
                              wait listed student. <strong>It is therefore very important that you check your Atlas e-mail account
                                 frequently. </strong>Please note that if you needed to get an override in order to register for a
                              course, you will NOT be able to add yourself to a wait list for that course in Atlas.
                              You will need to go
                              to an <a href="http://valenciacollege.edu/answer-center/">Answer Center</a> to be added to the wait list
                              for that course.
                           </p>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/registration-wait-list.pcf">©</a>
      </div>
   </body>
</html>