<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Admissions &amp; Records | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/registration-details-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Admissions &amp; Records</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Admissions &amp; Records</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Registration Details</h2>
                        
                        <p>
                           Students register for class(es) online via their <a href="https://atlas.valenciacollege.edu/">Atlas</a> account.  If you do not have access to a computer at home or work, you may register
                           for classes on campus using any of our <a href="../../student-services/atlas-access-labs.html">Atlas Access Labs</a>.  Atlas registration provides current 
                           class listings and a <strong>Registration Planner</strong> (<a href="../../support/howto/index.html">click here</a> to view a 90 second video on the benefits of using the <strong>Registration Planner</strong>) to help you create a class schedule with the courses you need. 
                        </p>
                        
                        <p>Students with certain holds will not be able to register for classes until the holds
                           
                           have been cleared. The <strong>Registration Planner</strong> will tell you if have a hold that will prevent you from registering for classes or
                           you may log in to  your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account, click on the <strong>Courses</strong> tab, select <strong>Registration</strong> inside the <strong>Registration</strong> channel, select <strong>Registration Status</strong>,   and then select the  term you want to register for -if you have a hold on your
                           account and you are not sure how to resolve it, 
                           please visit any <a href="../../student-services/atlas-access-labs.html">Answer Center</a> for assistance. 
                        </p>
                        
                        <h3>For Atlas Registration:</h3>
                        
                        <p>To register via the web log in to your&nbsp;<a href="https://atlas.valenciacollege.edu/">Atlas</a>&nbsp;account. If you have not completed the initial account set-up, select '<strong>Sign up for an account</strong>' and follow the online instructions.
                        </p>
                        
                        <p>After logging in to your Atlas Account:</p>
                        
                        <ul>
                           
                           <li>Click on the&nbsp;<strong>Courses</strong>&nbsp;tab
                           </li>
                           
                           <li>Click on&nbsp;<strong>Registration </strong>inside the <strong>Registration</strong> channel 
                           </li>
                           
                           <li>Click on <strong>Register for Classes</strong> 
                           </li>
                           
                           <li>Select the 
                              
                              
                              registration
                              
                              term and click <strong>submit</strong>
                              
                           </li>
                           
                           <li>Click on <strong>Registration Planner</strong> (<a href="../../support/howto/index.html">click here</a> to view a 90 second video on the benefits of using the <strong>Registration Planner</strong>) to select your classes and create a class schedule
                           </li>
                           
                           <li>Send your class schedule to your <strong>Registration Cart***</strong>
                              
                           </li>
                           
                           <li>In your <strong>Registration Cart</strong>, click on <strong>Register</strong> (note, if this is your first registration attempt for the semester, you will be prompted
                              to update your <strong>Valencia Alerts</strong> and submit your <strong>Student Enrollment Agreement</strong> before you can complete registration - you will need to return to your <strong>Registration Cart</strong> after submitting the <strong>Student Enrollment Agreement</strong> to complete registration).
                           </li>
                           
                           <li>Pay for your classes by the Fee Payment Deadline indicated in the online <a href="../../calendar/index.html">Important Dates &amp; Deadline</a> Calendar. 
                           </li>
                           
                        </ul>
                        
                        <p><em>***Make sure to&nbsp;<strong>Check Your Registration Status</strong>&nbsp;(Step 2) to view your registration day and time (Registration Time Ticket) and other
                              information which may impact your registration.</em></p>
                        
                        <p>Confirm that you have been successfully registered into all of your classes by clicking
                           on your&nbsp;<strong>Student Detail Schedule</strong>&nbsp;(you will need to print your&nbsp;<strong>Student Detail Schedule</strong>&nbsp;in order to purchase books for your classes).
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        
                        <h3>TRANSCRIPTS</h3>
                        
                        
                        
                        <p><a href="../officialtranscripts.html">Request your Official Valencia Transcripts</a></p>
                        
                        <p><a href="../admission-details/transcripts.html">Send us your HS, GED or College Transcripts</a></p>
                        
                        
                        
                        
                        
                        <h3>APPLICATION PRIORITY DEADLINES</h3>
                        
                        
                        
                        
                        <div data-id="application_priority_deadline_bachelors_degree">
                           
                           
                           <div>
                              Dec<br>
                              <span>08</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/application_priority_deadline_bachelors_degree" target="_blank">Application Priority Deadline - Bachelor's Degree</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT US</h3>
                           
                           
                           <p>Visit us on campus at the <a href="../../answer-center/index.html">Answer Center</a>.
                           </p>
                           
                           <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                           
                           <p>407-582-1507</p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/registration-details-1.pcf">©</a>
      </div>
   </body>
</html>