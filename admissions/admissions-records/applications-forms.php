<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Admissions &amp; Records | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/applications-forms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Admissions &amp; Records</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Admissions &amp; Records</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Applications &amp; Forms</h2>
                        
                        
                        <p>Completed forms may be submitted to  any campus <a href="../answer-center/index.html">Answer Center</a> or mailed  to:
                        </p>
                        
                        <blockquote>
                           
                           <p>Valencia College<br>
                              P.O. Box 3028<br>
                              Orlando, FL 32802-3028
                              
                           </p>
                           
                        </blockquote>
                        
                        
                        
                        <h3>Applications</h3>         
                        
                        <ul>
                           
                           <li>
                              
                              <p><a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Online Application for Admission</a> 
                              </p>
                              
                           </li>
                           
                           <li>
                              <p><a href="../dual/index.html">Dual Enrollment Admission</a></p>
                           </li>
                           
                           <li><a href="registration-details/transientstudents.html">Transient Admissions </a></li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Records &amp; Transcripts</h3>
                        
                        <p><span>The following forms can be found in Atlas:</span></p>
                        
                        <ul>
                           
                           <li>Change of Major* Form</li>
                           
                           <li> Change of Personal Information Form</li>
                           
                           <li>Late Drop With Refund / Late Withdrawal Without Refund Form</li>
                           
                        </ul>                
                        
                        <p>Log into your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account, click on Students Tab-&amp;gt; Student Forms-&amp;gt;Admissions and Records.
                        </p>
                        
                        <p><strong>*Includes:</strong> Direct Connect Classification and Change of Major 
                        </p>
                        
                        <p>Additional forms/links are below</p>
                        
                        <ul>
                           
                           <li><a href="degree_verification.html">Degree Verification</a></li>
                           
                           <li>
                              <a href="enrollment_verification.html">Enrollment Verification</a> 
                           </li>
                           
                           <li><a href="officialtranscripts.html">How to Submit an Official Transcript Request</a></li>
                           
                           <li><a href="documents/StudentConsenttoReleaseEducationRecords.pdf">Student Consent to Release Education Records Form</a></li>
                           
                           <li><a href="documents/StudentConsenttoWithholdEducationRecords.pdf">Student Consent to Withhold Education Records Form</a></li>
                           
                        </ul>
                        
                        
                        <h3>International Student Forms</h3>
                        
                        <p>Please visit the&nbsp;<a href="../international/index.html">International Student Services</a>&nbsp;website for more information about being an International Student at Valencia College.
                           
                        </p>
                        
                        
                        <h3>O<span>ther</span>                
                        </h3>
                        
                        <ul>
                           
                           <li>
                              
                              <p><a href="http://www.naces.org/members.htm" target="_blank">Foreign Credential Evaluation Agencies</a> <br>
                                 <strong>Note: The University of Central Florida currently accepts foreign credential evaluations
                                    from Josef Silny &amp; Associates, Inc. and World Education Services, Inc. only.</strong> 
                              </p>
                              
                           </li>
                           
                           <li>                  <a href="documents/HomeSchoolAffadavit8-2015.pdf">Home School Affidavit</a>
                              
                           </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <h3><strong> Award of Credit </strong></h3>
                           
                        </blockquote>                
                        <ul>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li><a href="documents/AwardofCreditSummary.pdf">Award of Credit Summary</a></li>
                                 
                              </ul>
                              
                              <ul>
                                 
                                 <li><a href="documents/AwardofCreditChart.pdf">Award of Credit Chart</a></li>
                                 
                                 <li><a href="documents/RegionallyAccreditedInstitutionsAwardofCredit.pdf">Regionally Accredited Institutions</a></li>
                                 
                                 <li><a href="../graduation/documents/Non-RegionallyAccreditedInstitution.pdf">Non-Regionally Accredited Institution Transfer Credit Request</a></li>
                                 
                                 <li><a href="documents/ExperientialLearningForm.pdf">Experiential Learning</a></li>
                                 
                                 <li><a href="documents/AwardofCreditfromInternationalInstitutions.pdf">Award of Credit International Institutions</a></li>
                                 
                                 <li><a href="documents/AlternativeMethodsofEarningCreditLocalAgreementsandStateArticulation.pdf">Alternative Methods of Earning Credit</a></li>
                                 
                                 <li>
                                    <a href="documents/AwardofCreditStateLocalArticulationAgreementsIndustryCertification.pdf">Award of Credit Local Articulation</a>                  
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        
                        <h3>TRANSCRIPTS</h3>
                        
                        
                        
                        <p><a href="officialtranscripts.html">Request your Official Valencia Transcripts</a></p>
                        
                        <p><a href="admission-details/transcripts.html">Send us your HS, GED or College Transcripts</a></p>
                        
                        
                        
                        
                        
                        <h3>APPLICATION PRIORITY DEADLINES</h3>
                        
                        
                        
                        
                        <div data-id="application_priority_deadline_bachelors_degree">
                           
                           
                           <div>
                              Dec<br>
                              <span>08</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/application_priority_deadline_bachelors_degree" target="_blank">Application Priority Deadline - Bachelor's Degree</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT US</h3>
                           
                           
                           <p>Visit us on campus at the <a href="../answer-center/index.html">Answer Center</a>.
                           </p>
                           
                           <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                           
                           <p>407-582-1507</p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/applications-forms.pcf">©</a>
      </div>
   </body>
</html>