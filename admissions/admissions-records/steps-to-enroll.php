<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Steps to Enroll  | Valencia College</title>
      <meta name="Description" content="Steps to Enroll, Admissions and Records">
      <meta name="Keywords" content="enroll, steps to enroll, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/steps-to-enroll.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Steps to Enroll </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Steps to Enroll</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div id="All">
                              
                              <ol>
                                 
                                 <li>Apply For Admission and Financial Aid</li>
                                 
                                 <li>Create Your Atlas Account</li>
                                 
                                 <li>Complete Online New Student Orientation and attend On-Campus Orientation</li>
                                 
                                 <li>Register for Classes &amp; Pay Tuition and Fees</li>
                                 
                                 <li>Get Your Student Identification Card and Parking Decal</li>
                                 
                                 <li>Buy Your Books &amp; Go To Class</li>
                                 
                              </ol>
                              
                           </div>
                           
                           <div id="1">
                              
                              <p>&nbsp;</p>
                              
                              <h3>Step 1: Apply For Admission and Financial Aid</h3>
                              
                              <p><strong>Admissions</strong></p>
                              
                              <p> Apply via the online <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Application for
                                    Admission</a>. 
                              </p>
                              
                              <ul>
                                 
                                 <li> Please be sure to have your driver's licenses, voter's and vehicle registration information
                                    ready.
                                    As you will need to complete the section related to Florida Residency.*
                                    
                                 </li>
                                 
                              </ul>
                              
                              <p>*Please note: If you're under the age of 24, these documents will need to be your
                                 parents' documents
                                 unless you meet an independent exception. 
                              </p>
                              
                              <p><strong>Financial Aid Services</strong></p>
                              
                              <p>It is suggested that you complete your financial aid application prior to the Financial
                                 Aid Priority
                                 Deadline (deadlines can be found in our online <a href="http://valenciacollege.edu/calendar/">Important
                                    Dates &amp; Deadlines</a> calendar). You can still apply for financial aid after the deadline, but you
                                 are not guaranteed to receive aid in time to pay for classes. Please see the <a href="http://valenciacollege.edu/finaid/">Financial Aid Services</a> web site for more information
                                 about financial aid. 
                              </p>
                              
                              <ul>
                                 
                                 <li>Complete the <a href="http://www.fafsa.ed.gov/" target="_blank">Free Application for Federal Student
                                       Aid (FAFSA)</a> online. Allow at least 6 weeks for your FAFSA to be processed. Valencia's school
                                    code
                                    is <strong>006750</strong>.
                                    
                                 </li>
                                 
                                 <li> To receive financial aid, you must be pursuing an Associate in Arts (A.A.) degree,
                                    an Associate in
                                    Science (A.S.) degree, a Bachelor of Science (B.S.) degree, or an eligible <a href="http://valenciacollege.edu/finaid/gettingstarted/training_cert.cfm">Vocational Training
                                       Certificate</a> program approved for financial aid.
                                    
                                 </li>
                                 
                                 <li>Submit all official transcripts (high school and/or college) to Valencia College.</li>
                                 
                              </ul>
                              
                              <p><strong>First Time in College Students</strong>: Request your official final high school transcript
                                 showing your date of graduation from high school or proof of your GED. You will be
                                 classified as a
                                 non-degree-seeking student until your official final high school transcript has been
                                 received. <br> <br>
                                 <strong>Transfer Students</strong>: Request official transcripts from all colleges that you ever
                                 attended. It is suggested that you also have your official final high school transcript
                                 or proof of your
                                 GED sent to Valencia. College transcripts must be received <strong>and</strong> evaluated before aid
                                 will be awarded.
                              </p>
                              
                              <div id="2">
                                 
                                 <p>&nbsp;</p>
                                 
                                 <h3>Step 2: Create Your Atlas Account</h3>
                                 
                                 <p><strong>Atlas</strong></p>
                                 
                                 <p>Atlas is Valencia's online learning community that connects faculty, students, and
                                    staff to the
                                    resources they need to succeed at Valencia. Students can obtain very important information
                                    regarding
                                    registration, financial aid, and course planning in their Atlas account. Students
                                    will also have an
                                    Atlas email account which is Valencia's official means of communication with students.
                                    
                                 </p>
                                 
                                 <p>Click here for <a href="https://atlas.valenciacollege.edu" target="_blank">Atlas</a>. Once at the
                                    Atlas log-in page, click on the "Sign up for an account" link and follow the prompts
                                    to create your
                                    account. Your Atlas account can be created <strong>3 - 5 business days after your application has been
                                       submitted</strong>. The only information you need in order to create your Atlas account is your
                                    name, Valencia Identification Number (VID) or Social Security Number (SSN), and your
                                    date of birth. Be
                                    sure to save your Atlas username and password in a safe place. Atlas will be very
                                    important for the <a href="http://valenciacollege.edu/orientation/">New Student Orientation</a> and registration steps
                                    that follow. <br> <br> If you need assistance with your Atlas account please visit one of our <a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Atlas Access Labs</a>.
                                    
                                 </p>
                                 
                              </div>
                              
                              <p>&nbsp; </p>
                              
                           </div>
                           
                           <h3>Step 3: Complete Online New Student Orientation and attend On-Campus Orientation </h3>
                           
                           <p>Orientation is set up in two (2) parts, online and on campus. During the online orientation,
                              you will
                              receive insider information on how to start on the right path here at Valencia and
                              will complete several
                              intake reviews that tell us more about you. After successfully completing the online
                              orientation and those
                              reviews, you can sign up for your on-campus orientation. During your on-campus orientation
                              you will have
                              the opportunity to meet other new students, plan out your class schedule with an academic
                              advisor, and
                              register for the term.
                           </p>
                           
                           <p><strong>Part 1: </strong>Online Orientation AND Part 2: On-campus Orientation must BOTH be completed for
                              degree-seeking students in order to register for classes and start your college experience
                              at Valencia!
                              
                           </p>
                           
                           <p><strong>Begin Part 1:</strong> Online Orientation by logging into your <strong><a href="https://atlas.valenciacollege.edu/">Atlas</a></strong> account; on the <strong>My Atlas</strong>
                              tab, inside the <strong>New Student Orientation</strong> channel, click on the <strong>Part 1: Online
                                 Orientation</strong> link. <strong><u>AFTER</u></strong> completing Part 1: Online Orientation (and PERT
                              if required), wait 24 hours then return to the <strong>New Student Orientation</strong> channel and click
                              <strong>Sign Up </strong>to review Part 2: On-campus Orientation dates and times.
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h3>Step 4: Register for Classes &amp; Pay Tuition and Fees</h3>
                           
                           <p><strong>Register</strong></p>
                           
                           <p>You register for classes online during Part 2: On-Campus Orientation using your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account. 
                           </p>
                           
                           <p>Check your Florida Residency status in <a href="https://atlas.valenciacollege.edu/">Atlas</a> by clicking
                              on the <strong>Registration</strong> tab, selecting <strong>Transcripts, Grades &amp; Holds</strong>, and
                              then selecting <strong>Student Information</strong>. All new and readmit students (you are a readmit
                              student if you have not attended classes at Valencia in two or more years) are classified
                              as non-Florida
                              Residents at the time of admission/re-admission to the College. To be considered for
                              Florida Residency and
                              thus be eligible for in-state tuition &amp; fees, be sure to complete the Statement of
                              <a href="residency-florida.html">Florida Residency for Tuition Purposes</a> form. 
                           </p>
                           
                           <p>Due to state and federal regulations, Valencia College requires all students registered
                              in an online
                              course to reside in Florida and to provide a mailing address in the state of Florida.
                              If you do not have a
                              mailing address in Florida, you will be deleted from any online course(s) in which
                              you register. You do
                              not need to be a Florida Resident for Tuition Purposes to register for online courses
                              but the active
                              mailing address on your Valencia College student record must be in the state of Florida.
                           </p>
                           
                           <p><strong>Pay Tuition </strong></p>
                           
                           <p>After registering for classes, you may also pay for your classes through your Atlas
                              account. If you wish
                              to pay in person, visit the <a href="http://valenciacollege.edu/businessoffice/">Business Office</a>.
                              Credit card (American Express, Discover, MasterCard, and Visa) and cash, check, or
                              money order payments
                              are accepted.
                           </p>
                           
                           <ul>
                              
                              <li>A <a href="http://valenciacollege.edu/businessoffice/tuition-installment-plan/">Tuition Installment
                                    Plan (TIP)</a> is available for students who need assistance with managing their education expenses.&nbsp;
                                 &nbsp;
                                 
                              </li>
                              
                           </ul>
                           
                           <p><strong>IMPORTANT</strong>: &nbsp;If you have applied for financial aid and are awaiting word on your
                              eligibility, you will need to pay for classes yourself by the Fee Payment Deadline
                              (see the <a href="http://valenciacollege.edu/calendar/">Important Dates &amp; Deadlines</a> calendar for Fee Payment
                              Deadlines) or your classes will be dropped for non-payment. If you are currently receiving
                              financial aid,
                              check with the Answer Center to ensure that all paperwork has been received and that
                              you will not be
                              dropped from your classes.
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <a href="http://valenciacollege.edu/businessoffice/tuition-fees.cfm">Tuition &amp; Fee Schedule</a>
                                 
                              </li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           <div id="5">
                              
                              <h3>Step 5: Get Your Student Identification Card and Parking Decal</h3>
                              
                              <p>You must show a paid receipt of classes and a photo I.D. in order to receive your
                                 student
                                 identification card. To obtain your student I.D., go to Security.
                              </p>
                              
                              <p>Parking decals are free and required for all students. The parking decal can be requested
                                 via your
                                 Atlas account. &nbsp;
                              </p>
                              
                              <p>&nbsp;</p>
                              
                           </div>
                           
                           <h3>Step 6: Buy Your Books &amp; Go To Class</h3>
                           
                           <p><strong>Purchase Your Books</strong></p>
                           
                           <p>Print your <strong>Student Detail Schedule</strong> from your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account and take it to the <a href="http://valenciacollege.edu/locations-store/">Bookstore</a> or purchase your books <a href="http://valenciacollege.edu/locations-store/faq.cfm">online</a>. 
                           </p>
                           
                           <p><strong>Go to Class(es)</strong></p>
                           
                           <p>See the online <a href="http://valenciacollege.edu/calendar/">Important Dates &amp; Deadlines</a>
                              calendar for the dates that classes begin. 
                           </p>
                           
                           <p>Your <strong>Student Detail Schedule</strong> from your <a href="https://atlas.valenciacollege.edu/">Atlas</a>
                              account includes the day(s), time, and location of your class(es). 
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <strong>Note:</strong> Students are required to attend the first day that each class meets or they may
                                 be withdrawn as a "no show" and will still be responsible for the cost of the class(es).
                                 
                              </li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/steps-to-enroll.pcf">©</a>
      </div>
   </body>
</html>