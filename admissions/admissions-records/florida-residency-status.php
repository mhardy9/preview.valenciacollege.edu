<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Residency Status  | Valencia College</title>
      <meta name="Description" content="Residency Status, Admissions and Records">
      <meta name="Keywords" content="residency status, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/florida-residency-status.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Residency Status </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>
                              Residency Status
                              
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>
                              Submitting your Florida Residency Form
                              
                           </h4>
                           
                           <p>
                              Florida Residency forms can be filled out and submitted electronically by clicking
                              the links below.
                              Supporting documentation for the forms can be attached to the electronic form prior
                              to submitting the
                              form. The preferred document types for uploading the supporting documentation (if
                              applicable) is pdf and
                              jpg.
                              
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=741af59b-382d-4171-87a7-3dcfb2ab6840" target="_blank">Florida Residency Form</a>
                                 
                              </li>
                              
                           </ul>
                           
                           <p>
                              &nbsp;
                              
                           </p>
                           
                           <h4>
                              Florida Residency for Tuition Purposes Policy and Links
                              
                           </h4>
                           
                           <p>
                              The Florida Residency for Tuition Purposes policy is based upon state statute, rules
                              of the two higher
                              education governing boards in Florida, and statewide guidelines developed by college
                              and university
                              administrators in conjunction with the Statewide Residency Committee and the Florida
                              Department of
                              Education.
                              
                           </p>
                           
                           <ul>
                              
                              <li><a href="http://www.flsenate.gov/Laws/Statutes/2014/Chapter1009/All" target="_blank">Florida Statue
                                    1009.21</a></li>
                              
                              <li><a href="https://www.flrules.org/gateway/ruleNo.asp?id=6A-10.044" target="_blank">State Board of
                                    Education Rule 6A-10.044</a></li>
                              
                              <li><a href="http://www.flbog.org/about/regulations/regulations.php" target="_blank">Click here for the
                                    full text of the Board of Governors Residency Regulation 7.005</a></li>
                              
                              <li><a href="https://www.flrules.org/gateway/RuleNo.asp?ID=6A-20.003" target="_blank">Click here for the
                                    full text of State Board of Education Rule 6A-20.003 </a></li>
                              
                           </ul>
                           
                           <p>
                              &nbsp;
                              
                           </p>
                           
                           <h4>
                              Check your Residency Status
                              
                           </h4>
                           
                           <p>
                              If you already have applied to Valencia, login to your <a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.P_DispLoginNon" target="_blank">admissions
                                 application</a> to view your residency status.
                              
                           </p>
                           
                           <p>
                              <strong>NOTE:</strong> If the documents you provided to prove residency cannot be verified, your residency
                              status may not change.
                              
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="residency-status.html" class="button-download hvr-sweep-to-right">SUBMIT RESIDENCY</a>
                     
                     <hr class="styled">
                     
                     <h3>
                        RESIDENCY PRIORITY DEADLINES
                        
                     </h3>
                     http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                     
                     
                     <p>
                        &nbsp;
                        
                     </p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/florida-residency-status.pcf">©</a>
      </div>
   </body>
</html>