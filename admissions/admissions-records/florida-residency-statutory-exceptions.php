<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Statutory Exceptions  | Valencia College</title>
      <meta name="Description" content="Statutory Exceptions, Admissions and Records">
      <meta name="Keywords" content="statutory exceptions, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/florida-residency-statutory-exceptions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Statutory Exceptions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Statutory Exceptions</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Appeals</h4>
                           
                           <p>In accordance with Florida Statutes, Valencia College has established a residency
                              appeals committee to
                              consider student appeals of residency determinations. In cases where the applicant
                              expresses a desire to
                              appeal the residency classification, the matter will be referred to the appeals committee.
                           </p>
                           
                           <p>The residency appeal officer or committee will convey to the applicant the final residency
                              determination
                              and the reasons for the determination within 20 business days. The final residency
                              determination will be
                              provided to the student in writing.
                           </p>
                           
                           <p>Students who wish to appeal the classification of residency for tuition purposes should
                              submit a written
                              request citing the basis for the appeal. Copies of all documentation supporting the
                              appeal for residency
                              reclassification should be included with the request. The request and all supporting
                              documentation can be
                              submitted at any of the Valencia College Answer Center locations. 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Waivers</h4>
                           
                           <div id="tab-1">
                              
                              <p>Effective July 1, 2014 [applicable for Fall 2014 and thereafter], House Bill 851 and
                                 House Bill 7015
                                 [amended section (s.) 1009.26, Florida Statutes (F.S.), fee waivers] have been authorized
                                 to provide an
                                 out of state fee waiver based on the criteria below.
                              </p>
                              
                              <h4>House Bill 851 – Out of state waiver</h4>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/admissions-records/florida-residency/documents/JA-Memo-HB851-Out-of-state-Fee-Waiver-HS-Students-070314.pdf" target="_blank">House Bill 851 FAQ </a>
                                    
                                 </li>
                                 
                                 <li><a href="http://www.flsenate.gov/Session/Bill/2014/0851/BillText/er/PDF" target="_blank">House Bill
                                       851</a></li>
                                 
                              </ul>
                              
                              <p> Attended a Florida high school for the last three consecutive years immediately before
                                 graduation from
                                 a Florida high school Submitted an official high school transcript providing evidence
                                 of high school
                                 attendance as listed above Submitted an admission application to a Florida College
                                 System or State
                                 University System institution within 24 months of high school graduation 
                              </p>
                              
                              <h4>House Bill 7015 – Congressman C.W. Bill Young Veteran Tuition Waiver Program</h4>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/admissions-records/florida-residency/documents/JA-Memo-HB7015-Out-of-state-Fee-Waiver-Veterans-070114.pdf" target="_blank">House Bill 7015 FAQ</a>
                                    
                                 </li>
                                 
                                 <li><a href="http://www.flsenate.gov/Session/Bill/2014/7015/BillText/er/PDF" target="_blank">House Bill
                                       7015</a></li>
                                 
                              </ul>
                              
                              <p> Honorably discharged veteran of the U.S. Armed Forces, U.S. Reserve Forces or the
                                 National Guard who
                                 physically resides in the state while enrolled in a Florida College System institution
                                 is eligible to
                                 receive the fee waiver. 
                              </p>
                              
                              <p> Please note: Receipt of this out-of-state tuition waiver does NOT constitute classification
                                 as a
                                 Florida resident for tuition purposes. Students who receive this waiver remain classified
                                 as non-Florida
                                 residents for tuition purposes and are NOT eligible for state financial aid, including
                                 Bright Futures.
                                 Residency for tuition purposes must be determined before this waiver is applied (if
                                 all criteria above
                                 is met).
                              </p>
                              
                           </div>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h4>U.S. Citizens</h4>
                           
                           <p> If one of the statutory exceptions below describes you, you qualify as a resident
                              for tuition purposes
                              without having to prove physical residency in Florida for the last 12 months. It does
                              not matter if you
                              are dependent or independent if you are claiming a statutory exception. Please complete
                              a Residency
                              Affidavit and required documents as outlined below before the first day of classes.
                              
                           </p>
                           
                           
                           <table class="table table table-striped cart-list add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col">Statutory Exceptions</th>
                                    
                                    <th scope="col">Residency Affidavit</th>
                                    
                                    <th scope="col">Required Documents</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> I am a qualified beneficiary under the terms of the Florida Pre-paid program.</td>
                                    
                                    <td>Yes</td>
                                    
                                    <td> Pre-paid Benefit Card</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> Another Florida public college or university has declared me as a resident for tuition
                                       purposes
                                       within the last 12 months.
                                       
                                    </td>
                                    
                                    <td>Yes</td>
                                    
                                    <td> Transcript indicating you were coded as RES and attendance at that institution within
                                       last 12
                                       months.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> I am married to a person who has maintained legal residence in Florida for at least
                                       12 months. I
                                       have established legal residence and intend to make Florida my permanent home.
                                       
                                    </td>
                                    
                                    <td> Yes (completed by spouse)</td>
                                    
                                    <td> Marriage certificate (two documents from spouse, one document from student* (does
                                       not have to be 12
                                       months).
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> I am a full-time employee of a Florida public school, community college or institution
                                       of higher
                                       education, or I am the employee's spouse or dependent child.
                                       
                                    </td>
                                    
                                    <td> Yes (completed by student)</td>
                                    
                                    <td> Letter from the school verifying employment of the employee.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> I am a full-time employee of a state agency or political subdivision of the state
                                       whose student
                                       fees are paid by the state agency or political subdivision, for the purpose of job-related
                                       law
                                       enforcement or corrections training.
                                       
                                    </td>
                                    
                                    <td>Yes</td>
                                    
                                    <td> Letter from corrections agency verifying employment and that fees are to be paid
                                       for the employee.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td> I am a member of the armed services of the United States or National Guard stationed
                                       in Florida on
                                       active duty pursuant to military orders, or whose home of record is Florida, or I
                                       am a member's spouse
                                       or dependent child.
                                       
                                    </td>
                                    
                                    <td>Yes</td>
                                    
                                    <td> Active duty orders, Form #DD2058, showing home of record and document that proves
                                       relationship to
                                       student.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="3">
                                       <strong>Other: </strong>There are other, more obscure exceptions that may pertain to
                                       you. For a complete list, refer to Florida Statute 1009.21
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                           
                           <h4><strong>Non-U.S. Citizens</strong></h4>
                           
                           
                           <p><strong>Exceptions for non-U.S. Citizens</strong></p>
                           
                           <p>A non-U.S. citizen may be eligible to establish residency for tuition purposes if
                              evidence is presented
                              verifying that he or she is legally present in the United States, has met the residency
                              requirements of
                              Section <a href="http://www.flsenate.gov/Laws/Statutes/2014/Chapter1009/All" target="_blank">1009.21,
                                 F.S.</a>, and the person is one of the following:
                           </p>
                           
                           
                           <ul>
                              
                              <li>A foreign national in a nonimmigrant visa classification that grants the person the
                                 legal ability to
                                 establish and maintain a bona fide domicile in the United States according to the
                                 United States
                                 Citizenship and Immigration Services (USCIS).
                                 
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>A permanent resident alien, parolee, asylee, Cuban-Haitian entrant, or other legal
                                 alien granted an
                                 indefinite stay in the United States. The student, and parent if the student is a
                                 dependent, must
                                 present evidence of legal presence in the United States.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p>&nbsp;</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="residency-status.html" class="button-download hvr-sweep-to-right">SUBMIT RESIDENCY</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>RESIDENCY PRIORITY DEADLINES</h3>
                     http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                     
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/florida-residency-statutory-exceptions.pcf">©</a>
      </div>
   </body>
</html>