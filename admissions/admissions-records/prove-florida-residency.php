<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Prove Florida Residency  | Valencia College</title>
      <meta name="Description" content="Prove Florida Residency, Admissions and Records">
      <meta name="Keywords" content="prove florida residency, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/prove-florida-residency.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Prove Florida Residency </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Prove Florida Residency</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>How New/Returning Students Prove Florida Residency</h3>
                           
                           
                           <h4>New Students</h4>
                           
                           
                           <p>If you are a Florida resident, you can easily prove residency when you submit your
                              Residency form online
                              to Valencia College by providing valid ID numbers for two of the following documents,
                              issued at least 12
                              consecutive months before the first day of your classes:
                           </p>
                           
                           
                           <p><strong>First Tier</strong></p>
                           
                           
                           <p>At least one of the two documents submitted must be from this list:</p>
                           
                           <ul>
                              
                              <li>A Florida voter's registration card</li>
                              
                              <li>A Florida driver's license or Florida identification card</li>
                              
                              <li>A Florida vehicle registration</li>
                              
                              <li>Proof of a permanent home in Florida which is occupied as a primary residence by the
                                 individual or by
                                 the individual's parent if the individual is a dependent child.
                                 
                              </li>
                              
                              <li>Proof of a homestead exemption in Florida.</li>
                              
                              <li>Transcripts from a Florida high school for multiple years (if Florida high school
                                 diploma or GED was
                                 earned within last 12 months)
                                 
                              </li>
                              
                              <li>Proof of permanent full-time employment in Florida for at least 30 hours per week
                                 for the most recent
                                 and consecutive 12-month period
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>Second Tier</strong></p>
                           
                           
                           <p>Documents from this list may be used in conjunction with documents from First Tier
                              and <strong>only
                                 if</strong> there is a valid document from the First Tier category.
                           </p>
                           
                           <ul>
                              
                              <li>A Declaration of Domicile in Florida (12 months from the date the document was sworn
                                 and subscribed as
                                 noted by the Clerk of Court)
                                 
                              </li>
                              
                              <li>A Florida professional or occupational license</li>
                              
                              <li>A Florida corporation as evidenced by Department of State filings</li>
                              
                              <li>Proof of membership in Florida-based charitable or professional organization that
                                 requires that the
                                 member reside in Florida as a condition of membership
                                 
                              </li>
                              
                              <li>Benefit histories from Florida agencies or public assistance programs</li>
                              
                              <li>Any other documentation that supports the student's request for resident status (may
                                 include utility
                                 bills in the name of the claimant and proof of 12 consecutive months of payments;
                                 a lease agreement in
                                 the name of the claimant and proof of 12 consecutive months of payments; or an official
                                 state, federal,
                                 or court document evidencing legal ties to Florida for the 12 consecutive months prior
                                 to enrollment.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>Unacceptable Documents</strong></p>
                           
                           
                           <p>The following items are not acceptable forms of documentation to establish that one
                              has been living in
                              Florida for 12 months:
                           </p>
                           
                           <ul>
                              
                              <li>Birth certificate</li>
                              
                              <li>Social Security card</li>
                              
                              <li>Passport</li>
                              
                              <li>Shopping club/rental cards</li>
                              
                              <li>Tax return</li>
                              
                              <li>Bank statements</li>
                              
                              <li>Cell phone bills</li>
                              
                              <li>Hunting/fishing permit</li>
                              
                              <li>Concealed weapons permit</li>
                              
                              <li>Credit card bills</li>
                              
                              <li>Vessel, Boat, Dune buggy or other recreational vehicles</li>
                              
                              <li>Medical bills or doctor's notes</li>
                              
                           </ul>
                           
                           
                           <p>&nbsp;</p>
                           
                           
                           <h4>Returning Students </h4>
                           
                           <p>An individual who was initially classified as a non-resident for tuition purposes
                              may become eligible for
                              reclassification as a resident for tuition purposes. Reclassification requires an
                              individual, or the
                              parent or guardian of a dependent person, to present clear and convincing documentation
                              that supports
                              permanent legal residence in Florida for at least 12 consecutive months rather than
                              temporary residence
                              for the purpose of pursuing an education. A parent is either one of the parents of
                              the student, any
                              guardian of the student, or any person in a parental relationship to the student.
                           </p>
                           
                           <p>A total of three documents will be required when submitting your Residency Reclassification
                              Form.&nbsp; One of
                              the three must come from Tier 1 while the remaining two may come from either Tier
                              1 or Tier 2.&nbsp; Please
                              refer to the form itself, or the New Students tab above, to review the list of acceptable
                              documents. 
                           </p>
                           
                           <p>Please note: Students who depend on out-of-state parents for support are presumed
                              to be legal residents
                              of the same state as their parents. 
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="residency-status.html" class="button-download hvr-sweep-to-right">SUBMIT RESIDENCY</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>RESIDENCY PRIORITY DEADLINES</h3>
                     http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                     
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/prove-florida-residency.pcf">©</a>
      </div>
   </body>
</html>