<ul>
<li><a href="/admissions/admissions-records/index.php">Admissions &amp; Records</a></li>
	
	 <li class="submenu"><a class="show-submenu" href="#">Admissions <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		<li><a href="/admissions/admissions-records/admission-details.php">Admissions Details</a></li>
                      <li><a href="/admissions/admissions-records/vaccinations.php">Vaccinations</a></li>
                        <li><a href="/admissions/admissions-records/readmission.php">Readmission</a></li>
						 <li><a href="/admissions/admissions-records/change-program-major.php">Change of Program/Major</a></li>
						<li><a href="/admissions/admissions-records/paying-for-classes.php">Paying for Classes</a></li>
</ul>
	</li>

	<li class="submenu"><a class="show-submenu" href="#">Applications &amp; Forms <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		 <li><a href="/admissions/admissions-records/applications-forms.php">Applications &amp; Forms</a></li>
		<li><a href="/admissions/admissions-records/degree-verification.php">Degree Verification</a></li>
		<li><a href="/admissions/admissions-records/enrollment-verification.php">Enrollment Verification</a></li>
  <li><a href="/admissions/admissions-records/transcripts.php">Transcripts</a></li>
<li><a href="/admissions/admissions-records/transcript-requests.php">Transcripts Request</a></li>
</ul>
	</li>
	
	
	
		 <li class="submenu"><a class="show-submenu" href="#">Registration <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/admissions/admissions-records/registration-details.php">Registration Details</a></li>
                    <li><a href="/admissions/admissions-records/atlas-access-for-registration.php"> Atlas Access for Registration</a></li>
                    <li><a href="/admissions/admissions-records/registration-in-online-classes.php">Online Classes</a></li>
                    <li><a href="/admissions/admissions-records/wait-list.php">Wait List</a></li>
                    <li><a href="/admissions/admissions-records/assisted-registration.php">Assisted Registration</a></li>
                    <li><a href="/admissions/admissions-records/transient-students-registration.php">Transient Students Registration</a></li>
                    <li><a href="/admissions/admissions-records/dual-enrollment-registration.php">Dual Enrollment Registration</a></li>
                    <li><a href="/admissions/admissions-records/senior-citizens-registration.php">Senior Citizens Registration</a></li>
                    <li><a href="/admissions/admissions-records/students-with-disabilities-registration.php">Students with Disabilities Registration</a></li>
                    <li><a href="/admissions/admissions-records/class-cancellation.php">Class Cancellation</a></li>
                    <li><a href="/admissions/admissions-records/continuing-education.php">Continuing Education</a></li>
                   
</ul>
	</li>
	
	
	  <li class="submenu"><a class="show-submenu" href="#">Florida Residency <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/admissions/admissions-records/florida-residency.php">Florida Residency</a></li>
<li><a href="/admissions/admissions-records/florida-residency-status.php">Residency Status</a></li>
<li><a href="/admissions/admissions-records/florida-residency-statutory-exceptions.php">Statutory Exceptions</a></li>
<li><a href="/admissions/admissions-records/florida-residency-frequently-asked-questions.php">Frequently Asked Questions</a></li>
<li><a href="/admissions/admissions-records/prove-florida-residency.php">Prove Florida Residency</a></li>
</ul>
	</li>

<li><a href="/admissions/admissions-records/steps-to-enroll.php">Steps to Enroll</a></li>

</ul>