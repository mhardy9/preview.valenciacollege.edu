<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Admission Details | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/admission-details.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Admission Details</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Admission Details</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Admission Details</h2>
                        
                        <p>Thank you for visiting the Admissions website of Valencia  
                           College! We are glad you are here and we hope that you find the 
                           information on this website to be helpful as you transition to one 
                           of the premiere learning-centered institutions in the United States.
                        </p>
                        
                        <p>You will be admitted to Valencia College as a degree-seeking student if you have 
                           a standard high school diploma or a state-issued General Educational 
                           Development (GED) diploma. To be admitted as a degree-seeking student you 
                           must have an admissions application and official academic transcript(s) 
                           on file in the Admissions and Records Office. If you do not have a high school diploma
                           or GED, you may be admitted as a Provisional non-degree seeking student. For a complete
                           list of requirements, visit the <a href="http://catalog.valenciacollege.edu/admissions/Admissions_Requirements_Chart_in_Catalog_201617-REV-093016.pdf">Summary of Admissions Requirements Chart</a> in our catalog. 
                        </p>
                        
                        <h3>Admissions Deadlines</h3>
                        
                        <p><strong>            </strong>Remember, it takes time to process your admissions application. 
                           The earlier you <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">apply</a>, the better! Please visit the online <a href="../../calendar/index.html">Important Dates &amp; Deadlines</a> calendar  for admissions Application Priority 
                           Deadlines. 
                        </p>
                        
                        <h3>Application Fees</h3>
                        
                        <p>There is a non-refundable  application fee of $35.00 for Associate's degree level
                           applicants and for <a href="http://preview.valenciacollege.edu/future-students/degree-options/bachelors/">Bachelor's</a> degree level applicants (you must have already <strong>completed</strong> an Associate's degree in order to apply for admission to a Bachelor's degree program
                           at Valencia). Dual Enrollment students cannot apply online and should visit the <a href="../../dual/index.html">Dual Enrollment</a> website for application instructions.
                        </p>
                        
                        <p>Previous Valencia students who have not taken classes here in the past two (2) years
                           will need to <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">reapply</a> for admission and pay the non-refundable $35.00 readmission fee. 
                        </p>
                        
                        <h3>Application Timeline</h3>
                        
                        <p>PLEASE NOTE: Your admissions application will not be processed 
                           without the  non-refundable admissions application fee. Admissions applications are
                           normally processed within three to five (3-5) business days of receipt. However, between
                           November 1 to January 15, July 1 to August 31, and April 1 to May 15, it may take
                           additional time for your admissions application to be processed due to the large number
                           of applications received.<br>
                           
                        </p>
                        
                        You will initially be admitted as a non-degree seeking student 
                        until your official final academic transcript(s) are received. 
                        Order your official final high school and/or college transcript(s) 
                        and have them sent directly to: 
                        
                        
                        <div><strong>Valencia College</strong></div>
                        
                        <div><strong>Admissions and Records Office</strong><br>
                           <strong>PO Box 3028</strong><br>
                           <strong>Orlando, Florida 32802-3028</strong><br>
                           
                        </div>
                        
                        Financial aid applicants must have <strong>all</strong> official 
                        final academic transcripts received <strong>and</strong> evaluated before their 
                        financial aid will be disbursed. Please allow adequate 
                        time for this process to occur. The evaluation of transfer credit 
                        takes at least 20 business days (from the date the official transcript is received)
                        to complete. Official academic 
                        transcripts are not evaluated for non-degree seeking students 
                        (personal interest, technical certificate, transient, teacher recertification, or
                        job 
                        improvement). You must be in a degree seeking program in order 
                        for your official college/university academic transcript(s) to be evaluated.
                        
                     </div>
                     	  
                  </div>
                  			
               </div>
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/admission-details.pcf">©</a>
      </div>
   </body>
</html>