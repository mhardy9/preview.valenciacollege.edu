<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Admissions &amp; Records | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/readmission.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Admissions &amp; Records</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Admissions &amp; Records</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        <h2>Readmission </h2>
                        
                        <p>Students who have not attended classes in two or more years must reapply for admission
                           to Valencia College. There is a $35.00 Readmit Fee. Readmit students who were initially
                           classified as Florida Residents for Tuition Purposes must <strong>reprove</strong> Florida Residency by 
                           
                           
                           completing
                           
                           the residency portion in the 
                           
                           
                           admission
                           
                           application or by completing a new <a href="../florida-residency/status.html"><em>Statement of Florida Residency for Tuition Purposes</em></a> form. Please see the <a href="../../calendar/index.html">Important Dates &amp; Deadlines</a> calendar for the Application Priority Deadlines for each term. 
                        </p>
                        <br>
                        
                        
                        
                        
                        
                        <h2>Readmission after Academic Suspension/Dismissal</h2>
                        
                        <p>
                           Students currently on academic suspension or dismissal must submit 
                           a written request for readmission to the Dean of Students on their 
                           campus.  Please 
                           see the <a href="../../calendar/index.html">Important Dates &amp; Deadlines</a> calendar  for the Suspension Readmission Request Deadlines 
                           for each term. 
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        
                        <h3>TRANSCRIPTS</h3>
                        
                        
                        
                        <p><a href="../officialtranscripts.html">Request your Official Valencia Transcripts</a></p>
                        
                        <p><a href="transcripts.html">Send us your HS, GED or College Transcripts</a></p>
                        
                        
                        
                        
                        
                        <h3>APPLICATION PRIORITY DEADLINES</h3>
                        
                        
                        
                        
                        <div data-id="application_priority_deadline_bachelors_degree">
                           
                           
                           <div>
                              Dec<br>
                              <span>08</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/application_priority_deadline_bachelors_degree" target="_blank">Application Priority Deadline - Bachelor's Degree</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT US</h3>
                           
                           
                           <p>Visit us on campus at the <a href="../../answer-center/index.html">Answer Center</a>.
                           </p>
                           
                           <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                           
                           <p>407-582-1507</p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/readmission.pcf">©</a>
      </div>
   </body>
</html>