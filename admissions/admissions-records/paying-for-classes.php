<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Paying for Classes  | Valencia College</title>
      <meta name="Description" content="Paying for Classes, Admissions and Records">
      <meta name="Keywords" content="paying for classes, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/paying-for-classes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Paying for Classes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container">
                  		
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           
                           <h3>Paying for Classes</h3>
                           
                           
                           <div class="wrapper_indent">
                              
                              <p> For information on paying for classes, please visit the <a href="http://valenciacollege.edu/businessoffice/">Business Office</a> website.
                              </p>
                              
                              <p> If you have applied for <a href="http://valenciacollege.edu/finaid/">financial aid</a> and are awaiting
                                 word on your eligibility, you will need to pay for your classes yourself by the Fee
                                 Payment Deadline (see
                                 the <a href="http://valenciacollege.edu/calendar/">Important Dates &amp; Deadlines</a> calendar for Fee
                                 Payment Deadlines) or your classes will be dropped for non-payment. If you are currently
                                 receiving
                                 financial aid, check with the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a> to
                                 ensure that all papework has been received and that you will not be dropped from your
                                 class(es). 
                              </p>
                              
                              <p>A <a href="http://valenciacollege.edu/businessoffice/tuition-installment-plan/">Tuition Installment
                                    Plan</a> is available for students who need assistance with managing their education expenses.
                                 
                              </p>
                              
                              <p>All new and readmit students (you are a readmit student if you have not attended classes
                                 at Valencia in
                                 two or more years) are classified as non-Florida Residents at the time of admission/re-admission
                                 to the
                                 College. To be considered for Florida Residency and thus be eligible for in-state
                                 <a href="http://valenciacollege.edu/businessoffice/tuition-fees.cfm">Tuition &amp; Fees</a> be sure to
                                 complete the <a href="residency-florida-prove.html">Statement of Florida Residency for Tuition
                                    Purposes</a> form.
                              </p>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                        
                        
                        <hr class="styled">
                        
                        
                        <h3>TRANSCRIPTS</h3>
                        
                        <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                        
                        <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                        
                        
                        <hr class="styled">
                        
                        
                        <h3>APPLICATION PRIORITY DEADLINES</h3>
                        
                        <p>
                           
                           http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                           
                           
                        </p>
                        
                        <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                              Deadlines</a>.
                        </p>
                        
                        
                        <hr class="styled">
                        
                        
                        <h3>CONTACT US</h3>
                        
                        <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                        </p>
                        
                        <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                        
                        <p>407-582-1507</p>
                        
                        
                        <p>&nbsp;</p>
                        
                     </aside>
                     
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/paying-for-classes.pcf">©</a>
      </div>
   </body>
</html>