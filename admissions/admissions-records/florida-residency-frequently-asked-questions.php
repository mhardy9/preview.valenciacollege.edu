<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Residency Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Residency Frequently Asked Questions, Admissions and Records">
      <meta name="Keywords" content="residency status, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/florida-residency-frequently-asked-questions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Residency Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="main-title">
                  
                  <h2>Residency Frequently Asked Questions</h2>
                  
               </div>
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>What is a "Florida resident for tuition purposes?"</h4>
                        
                        <p>Under Florida law, being a legal Florida resident does not automatically qualify you
                           for in-state
                           tuition.
                        </p>
                        
                        <p>A "Florida resident for tuition purposes" is a person who has, or a dependent person
                           whose parent,
                           guardian, or spouse has established and maintained legal residence in Florida for
                           at least twelve
                           consecutive months prior to the first day of the term. A parent is either one of the
                           parents of the student,
                           any guardian of the student, or any person in a parental relationship to the student.
                        </p>
                        
                        <p><strong>Please Note: </strong>Students, who depend on out-of-state parents for support, are presumed to be
                           legal residents of the same state as their parents.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>Which residency affidavit should I submit?</h4>
                           
                        </div>
                        
                        <p>All claimants for Florida residency for tuition purposes must submit a residency affidavit.
                           New and
                           readmission applicants should complete the Residency Affidavit at the time of application
                           for
                           admission/readmission.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What is a Dependent/Independent student?</h4>
                           
                        </div>
                        
                        <p><a href="residency-florida.html">See the description in the Depending vs. Independent page. </a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>If my family owns a house/condominium in Florida, can I be considered a resident for
                              tuition
                              purposes?
                           </h4>
                           
                        </div>
                        
                        <p>Home ownership does not automatically qualify you for Florida residency for tuition
                           purposes. Residence in
                           Florida must be as a bona fide domicile rather than for the purpose of maintaining
                           a residence incident to
                           enrollment at an institution of higher education. Owning property in Florida, while
                           residing in another
                           state, will not of its own merit meet residency requirements.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What if I've lived in or attended a school in Florida in the past?</h4>
                           
                        </div>
                        
                        <p>To be considered a "Florida Resident for Tuition Purposes" you must prove through
                           official and/or legal
                           documents that you or the claimant have established bona fide domicile in the state
                           of Florida for at least
                           12 months preceding the first day of classes of the term for which Florida residency
                           is sought. You do not
                           receive credit for any time that you spent in the state in prior years.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>If I live near the Florida border, can I qualify for Florida residency?</h4>
                           
                        </div>
                        
                        <p>No. Living near the state of Florida does not grant the student residency for tuition
                           purposes. However,
                           active duty military personnel and their dependents stationed within 50 miles of the
                           Florida border may
                           claim residency based on the official military orders. Official documentation is subject
                           to evaluation and
                           verification.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>Why do I have to prove that I'm a Florida resident if I graduated from a Florida High
                              School?
                           </h4>
                           
                        </div>
                        
                        <p>Just graduating from a Florida high school is not sufficient proof of Florida residency
                           for tuition
                           purposes. As examples, you might live in another state and be attending a Florida
                           school as a boarding
                           student in a private school; or you may have moved from another state during your
                           senior year and have less
                           than the requisite 12 months required for residency for tuition purposes. You must
                           still submit the
                           appropriate documentation to prove your bona fide domicile in the state of Florida
                           for at least 12 months
                           preceding the first day of classes of the term for which Florida residency is sought.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What if I just renewed my Driver's License, Voter Registration, and/or Vehicle Registration?</h4>
                           
                        </div>
                        
                        <p> If your documentation was recently renewed in accordance with state policy/regulation,
                           you may submit the
                           original date of issue on the residency affidavit. However, you may be asked to provide
                           additional
                           documentation to verify the dates you report. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>Do I meet the qualifications for the waiver?</h4>
                           
                        </div>
                        
                        <p>Yes, if you meet these qualifications: Attended a Florida high school for the last
                           three consecutive years
                           immediately before graduation from a Florida high school Submitted an official high
                           school transcript
                           providing evidence of high school attendance as listed above Submitted an admission
                           application to a Florida
                           College System or State University System institution within 24 months of high school
                           graduation. 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>Can I be considered a FL resident even though my immigration status is Deferred Action
                              for Childhood
                              Arrivals (DACA).
                           </h4>
                           
                        </div>
                        
                        <p>No, not at this time.&nbsp; DACA students are not eligible to receive in-state fees according
                           to the Florida
                           Department of Education, Division of Colleges. <a href="residency-statutory-exceptions.html">Please feel
                              free to look at the exceptions section of our website to see if you qualify for a
                              waiver.</a></p>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What are the deadlines for residency documentation?</h4>
                           
                        </div>
                        
                        <p></p>
                        
                        <ul>
                           
                           <li>Documentation must be submitted on or before the first day of the term for which you
                              are enrolling.
                              Check the <a href="http://valenciacollege.edu/calendar/">Academic Calendar</a> to verify the first day of
                              classes.
                              
                           </li>
                           
                           <li>Documentation must prove that you have lived in Florida for at least 12 consecutive
                              months before the
                              first day of classes.
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What happens if my documents do not meet the requirements?</h4>
                           
                        </div>
                        
                        <p> If your documents do not verify as you expected, your status will be changed to nonresident
                           and you will
                           have to apply for reclassification. You will have until the first day of classes to
                           submit any additional
                           documents to support your claim of residency. However, if you do not provide sufficient
                           documentation, you
                           will be assessed out-of-state fees and later, you can request a reclassification of
                           residency. However, the
                           burden of proof for reclassification is stricter<strong>, requiring three verified documents as opposed to
                              the two that are required in the initial application. </strong></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <div>
                           
                           <h4>What are acceptable documents for proof of relationship if my parents and I have different
                              last
                              names?
                           </h4>
                           
                        </div>
                        
                        <ul>
                           
                           <li> Bith Certificate (s)</li>
                           
                           <li>Prior year's tax documents</li>
                           
                           <li>Marriage Certificate (if name differs from birth certificate)</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/florida-residency-frequently-asked-questions.pcf">©</a>
      </div>
   </body>
</html>