<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Admissions &amp; Records | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/transient-students-registration.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Admissions &amp; Records</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Admissions &amp; Records</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Transient Student Registration</h2>
                        
                        <p>
                           Transient students are currently enrolled college students who register for courses
                           at another institution on a temporary basis (for example: a university student attending
                           a summer semester at a state college). In order to take courses at another institution,
                           you must receive approval from your home institution. 
                        </p>
                        
                        <p><strong>Students whose home institution is in Florida must complete a <a href="https://www.floridashines.org" target="_blank">Transient Student Admission Application</a> on https://www.floridashines.org/.</strong></p>
                        
                        <ul>
                           
                           <li>Beginning December 1, 2012, the floridashines.org Transient Student Admission Application
                              will also serve as an application for admission to Valencia College. Until then, Transient
                              students whose home institution is in Florida must also complete Valencia's online
                              Application for Admission if they have not previously done so. 
                           </li>
                           
                           <li>Students whose home institution is in another state or who are attending a Florida
                              private institution that does not utilize floridashines.org should contact their home
                              institution for permission to attend Valencia. 
                           </li>
                           
                        </ul>            
                        
                        <p>Once you have been admitted to Valencia you will need to create your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account. You will register yourself for your classes via your Atlas account. Only
                           <strong>after</strong> all approvals have been received should a transient student register for classes.
                           Transient approval is required for each term the student wishes to take classes at
                           Valencia. 
                        </p>
                        
                        <p><strong>Note:</strong> Effective with Spring 2013 registration, there will be a $5.00 Transient Fee applied
                           to each course you take at Valencia College. 
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        
                        <h3>TRANSCRIPTS</h3>
                        
                        
                        
                        <p><a href="../officialtranscripts.html">Request your Official Valencia Transcripts</a></p>
                        
                        <p><a href="../admission-details/transcripts.html">Send us your HS, GED or College Transcripts</a></p>
                        
                        
                        
                        
                        
                        <h3>APPLICATION PRIORITY DEADLINES</h3>
                        
                        
                        
                        
                        <div data-id="application_priority_deadline_bachelors_degree">
                           
                           
                           <div>
                              Dec<br>
                              <span>08</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/application_priority_deadline_bachelors_degree" target="_blank">Application Priority Deadline - Bachelor's Degree</a><br>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <div>
                           
                           <h3>CONTACT US</h3>
                           
                           
                           <p>Visit us on campus at the <a href="../../answer-center/index.html">Answer Center</a>.
                           </p>
                           
                           <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                           
                           <p>407-582-1507</p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/transient-students-registration.pcf">©</a>
      </div>
   </body>
</html>