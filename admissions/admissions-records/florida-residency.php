<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Florida Residency  | Valencia College</title>
      <meta name="Description" content="Florida Residency, Admissions and Records">
      <meta name="Keywords" content="florida residency, admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/florida-residency.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Florida Residency </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        
                        <div class="row">
                           
                           <div class="col-md-5 col-sm-5">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="residency-statutory-exceptions.html" class="button-download hvr-sweep-to-right">Were you
                                       denied?</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="residency-florida-prove.html" class="button-download hvr-sweep-to-right">What do I need to
                                       bring?</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Florida Residency</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4><strong>Important Reminder </strong></h4>
                           
                           <p>Students who are eligible for Florida residency for tuition purposes pay a much lower
                              tuition rate. <a href="http://valenciacollege.edu/businessoffice/tuition-fees.cfm">See the current rates.</a></p>
                           
                           <p>For students who fail to submit documentation and still register for classes, tuition
                              will be charged at
                              the out-of-state rate. 
                           </p>
                           
                           <p>If your request for Florida Residency and the necessary documentation is not received
                              prior to the Proof
                              of Florida Residency Deadline as listed in the <a href="http://valenciacollege.edu/calendar/">Important
                                 Dates and Deadlines</a> calendar, your residency <strong>will be processed for the next available
                                 term. </strong></p>
                           
                           <h3>Dependent vs. Independent Students</h3>
                           
                           <p>The determination of dependent or independent student status is important because
                              it is the basis for
                              whether the student has to submit his/her own documentation for residency (as an independent)
                              or his/her
                              parent's or guardian's documentation of residency (as a dependent). Parent means one
                              or both of the
                              parents of a student, any guardian of a student or any person in a parental relationship
                              to a student.
                           </p>
                           
                           <div>
                              
                              <h4>Independent Students</h4>
                              
                              <div id="tab-1">
                                 
                                 <p> A student who meets any of the following criteria shall be classified as an independent
                                    student for
                                    the determination of residency for tuition purposes:
                                 </p>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>You are 24 years of age or older prior to the Proof of Florida Residency Deadline
                                          listed in the
                                          <a href="http://valenciacollege.edu/calendar/">Academic Calendar</a>.
                                          
                                       </li>
                                       
                                       <li>You are married. (Copy of marriage certificate required.)</li>
                                       
                                       <li>You have a child who lives with you and receives their support from you. (Copy of
                                          most recent
                                          federal tax return listing the child as your dependent required.)
                                          
                                       </li>
                                       
                                       <li>You have other dependents who live with you and receive their support from you. (Copy
                                          of most
                                          recent federal tax return listing the person(s) as your dependent required.)
                                          
                                       </li>
                                       
                                       <li>You are a veteran of the U.S. Armed Forces. (Copies of military documents required.)</li>
                                       
                                       <li>Your parents are deceased and you are, or were until age 18, a ward of the court.
                                          (Copies of
                                          court documents required.)
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <p>A student who does not meet any of the criteria outlined above may be classified as
                                    an independent
                                    student only if he or she submits documentation that he or she provides fifty (50)
                                    percent or more of
                                    the <a href="http://valenciacollege.edu/finaid/attendancecost.cfm">cost of attendance</a> for
                                    independent, in-state students as defined by the Office of Financial Aid (exclusive
                                    of federal, state,
                                    and institutional aid or scholarships).
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h4>Dependent</h4>
                           
                           <p>All other students who do not meet the definition of an independent student shall
                              be classified as
                              dependent students for the determination of residency for tuition purposes. 
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="residency-status.html" class="button-download hvr-sweep-to-right">SUBMIT RESIDENCY</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>RESIDENCY PRIORITY DEADLINES</h3>
                     http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                     
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/florida-residency.pcf">©</a>
      </div>
   </body>
</html>