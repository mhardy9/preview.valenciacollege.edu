<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Registration Details  | Valencia College</title>
      <meta name="Description" content="Registration Details, Admissions and Records">
      <meta name="Keywords" content="admissions, records, transcript, enrollment, verification, residency, registration, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/admissions/admissions-records/registration-details.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/admissions/admissions-records/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/admissions/">Admissions</a></li>
               <li><a href="/admissions/admissions-records/">Admissions Records</a></li>
               <li>Registration Details </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Registration Details</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p> Students register for class(es) online via their <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account. If you do not
                              have access to a computer at home or work, you may register for classes on campus
                              using any of our <a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Atlas Access Labs</a>. Atlas
                              registration provides current class listings and a <strong>Registration Planner</strong> (<a href="http://valenciacollege.edu/support/howto/">click here</a> to view a 90 second video on the
                              benefits of using the <strong>Registration Planner</strong>) to help you create a class schedule with the
                              courses you need. 
                           </p>
                           
                           <p>Students with certain holds will not be able to register for classes until the holds
                              have been cleared.
                              The <strong>Registration Planner</strong> will tell you if have a hold that will prevent you from
                              registering for classes or you may log in to your <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account, click on the
                              <strong>Courses</strong> tab, select <strong>Registration</strong> inside the
                              <strong>Registration</strong> channel, select <strong>Registration Status</strong>, and then select the
                              term you want to register for -if you have a hold on your account and you are not
                              sure how to resolve it,
                              please visit any <a href="http://valenciacollege.edu/student-services/atlas-access-labs.cfm">Answer
                                 Center</a> for assistance. 
                           </p>
                           
                           <h4>For Atlas Registration:</h4>
                           
                           <p>To register via the web log in to your&nbsp;<a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a>&nbsp;account. If you have not completed
                              the initial account set-up, select '<strong>Sign up for an account</strong>' and follow the online
                              instructions.
                           </p>
                           
                           <p>After logging in to your Atlas Account:</p>
                           
                           <ul>
                              
                              <li>Click on the&nbsp;<strong>Courses</strong>&nbsp;tab
                              </li>
                              
                              <li>Click on&nbsp;<strong>Registration </strong>inside the <strong>Registration</strong> channel
                              </li>
                              
                              <li>Click on <strong>Register for Classes</strong>
                                 
                              </li>
                              
                              <li>Select the registration term and click <strong>submit</strong>
                                 
                              </li>
                              
                              <li>Click on <strong>Registration Planner</strong> to select your classes and create a class schedule (<a href="http://valenciacollege.edu/support/howto/">click here</a> to view a 90 second video on the
                                 benefits of using the <strong>Registration Planner</strong>)
                                 
                              </li>
                              
                              <li>Send your class schedule to your <strong>Registration Cart***</strong>
                                 
                              </li>
                              
                              <li>In your <strong>Registration Cart</strong>, click on <strong>Register</strong> (note, if this is your
                                 first registration attempt for the semester, you will be prompted to update your <strong>Valencia
                                    Alerts</strong> and submit your <strong>Student Enrollment Agreement</strong> before you can complete
                                 registration - you will need to return to your <strong>Registration Cart</strong> after submitting the
                                 <strong>Student Enrollment Agreement</strong> to complete registration).
                                 
                              </li>
                              
                              <li>Pay for your classes by the Fee Payment Deadline indicated in the online <a href="http://valenciacollege.edu/calendar/">Important Dates &amp; Deadline</a> Calendar.
                                 
                              </li>
                              
                           </ul>
                           
                           <p><em>***Make sure to&nbsp;<strong>Check Your Registration Status</strong>&nbsp;(Step 2) to view your registration
                                 day and time (Registration Time Ticket) and other information which may impact your
                                 registration.</em></p>
                           
                           <p>Confirm that you have been successfully registered into all of your classes by clicking
                              on your&nbsp;<strong>Student
                                 Detail Schedule</strong>&nbsp;(you will need to print your&nbsp;<strong>Student Detail Schedule</strong>&nbsp;in order to
                              purchase books for your classes).
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Registration in Online Classes</h4>
                           
                           <p>Due to state and federal regulations, Valencia College requires all students registered
                              in an online
                              course to reside in Florida and to provide a mailing address in the state of Florida.
                              If you do not have a
                              mailing address in Florida, you will be deleted from any online course(s) in which
                              you register. You do
                              not need to be a Florida Resident for Tuition Purposes to register for online courses
                              but the active
                              mailing address in your Valencia student record must be in the state of Florida. 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Assisted Registration</h4>
                           
                           <p> Assisted registration is a limited option for being registered on campus by a Student
                              Services
                              Specialist in the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>. Assisted
                              registration is only available for senior citizens, <a href="http://valenciacollege.edu/internship/">Internship
                                 &amp; Workforce Services</a> students, students who wish to audit a course, and students who received a
                              registration override and need to be added to a wait list. All other students must
                              register for courses
                              via their <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account.<br>
                              
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Dual Enrollment Registration</h4>
                           
                           <p> The <a href="http://valenciacollege.edu/dual/">Dual Enrollment</a> program is for high school students
                              who have been approved to take courses at Valencia College that will count toward
                              high school completion.
                              Dual Enrollment students may register for classes online via their <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account. If you have problems
                              registering for your dual enrollment classes please contact the Dual Enrollment Office
                              at (407) 582-1600.
                              
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Transient Student Registration</h4>
                           
                           <p> Transient students are currently enrolled college students who register for courses
                              at another
                              institution on a temporary basis (for example: a university student attending a summer
                              semester at a state
                              college). In order to take courses at another institution, you must receive approval
                              from your home
                              institution. 
                           </p>
                           
                           <p><strong>Students whose home institution is in Florida must complete a <a href="https://www.floridashines.org" target="_blank">Transient Student Admission Application</a> on
                                 https://www.floridashines.org/.</strong></p>
                           
                           <ul>
                              
                              <li>Beginning December 1, 2012, the floridashines.org Transient Student Admission Application
                                 will also
                                 serve as an application for admission to Valencia College. Until then, Transient students
                                 whose home
                                 institution is in Florida must also complete Valencia's online Application for Admission
                                 if they have
                                 not previously done so.
                                 
                              </li>
                              
                              <li>Students whose home institution is in another state or who are attending a Florida
                                 private institution
                                 that does not utilize floridashines.org should contact their home institution for
                                 permission to attend
                                 Valencia.
                                 
                              </li>
                              
                           </ul>
                           
                           <p>Once you have been admitted to Valencia you will need to create your <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> account. You will register yourself
                              for your classes via your Atlas account. Only <strong>after</strong> all approvals have been received
                              should a transient student register for classes. Transient approval is required for
                              each term the student
                              wishes to take classes at Valencia. 
                           </p>
                           
                           <p><strong>Note: </strong>Effective with Spring 2013 registration, there will be a $5.00 Transient Fee
                              applied to each course you take at Valencia College. 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Senior Citizen Registration</h4>
                           Registration fees for students 60 years of age or older may be waived under the following
                           conditions:
                           
                           <ul>
                              
                              <li>You must be classified as a Florida Resident. To be considered for Florida Residency
                                 please complete
                                 the <a href="http://valenciacollege.edu/admissions-records/florida-residency/">Statement of Florida
                                    Residency for Tuition Purposes</a> form.
                                 
                              </li>
                              
                              <li>You must register for classes on a space-available basis on the day senior citizen
                                 registration
                                 begins. Check the <a href="http://valenciacollege.edu/calendar/">important dates calendar</a> to get the
                                 date.
                                 
                              </li>
                              
                              <li>You must obtain a Senior Citizen Waiver Form from the <a href="http://valenciacollege.edu/businessoffice/">Business Office</a> or the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>, complete the form, and present it to
                                 the Business Office after registering for your classes.
                                 
                              </li>
                              
                              <li>You must decide if you want to receive a grade in the course or if you want to audit
                                 the course (no
                                 grade).
                                 
                              </li>
                              
                           </ul>
                           
                           <p>PLEASE NOTE: If you register early in order to assure a space, you will be charged
                              full course fees with
                              no rights to a waiver or refund (you will receive a refund if you drop the class by
                              the published
                              Drop/Refund Deadline or if the class is canceled by the College). If you received
                              an override to register
                              into a class that is full you will be charged for the class. 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Students with Disabilities Registration</h4>
                           
                           <p> Students are strongly encouraged to request accommodations from the Office for Students
                              with
                              Disabilities at least four weeks prior to the first day of classes. The college cannot
                              guarantee that
                              accommodations will be available on the first day of classes for those students who
                              choose to request
                              services later than four weeks prior to the first day of classes. However, students
                              have the right to
                              self-identify and request accommodations at any time during their enrollment at Valencia
                              College. For
                              information on programs, special equipment, and other services available for students
                              with documented
                              disabilities, please contact the <a href="http://valenciacollege.edu/osd">Office for Students with
                                 Disabilities</a> on the campus you would like to attend. All Valencia campuses are accessible for
                              students with physical limitations. 
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Class Cancellation</h4>
                           
                           <p> Classes may be canceled during registration due to low enrollment or the ability
                              to find a qualified
                              instructor. If a course is canceled, every effort will be made to notify the students
                              involved and to help
                              them find a suitable substitute class. Notifications will be sent to students' Atlas
                              e-mail account.
                              <strong>Important: Check your Atlas e-mail account frequently.</strong></p>
                           
                           <p>&nbsp;</p>
                           
                           <h4>Continuing Education</h4>
                           
                           <p>Valencia College offers Continuing Education for professional development, career
                              advancement, languages,
                              certifications and certification preparation.&nbsp;
                           </p>
                           
                           <p>For more information about continuing education courses, workshops, ESOL courses and
                              seminars in a
                              variety of subject areas, please visit <a href="http://valenciacollege.edu/continuing-education/">Valencia
                                 College Continuing Education</a>&nbsp;or call (407) 582-6688 or (407) 582-6755.
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button-download hvr-sweep-to-right">APPLY NOW</a>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>TRANSCRIPTS</h3>
                     
                     <p><a href="transcript-requests.html">Request your Official Valencia Transcripts</a></p>
                     
                     <p><a href="index.html">Send us your HS, GED or College Transcripts</a></p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>APPLICATION PRIORITY DEADLINES</h3>
                     
                     <p>
                        
                        http://events.valenciacollege.edu/widget/view?schools=valencia&amp;days=365&amp;num=50&amp;types=16521&amp;format=rss
                        
                        
                     </p>
                     
                     <p>Refer to the college calendar for <a href="http://valenciacollege.edu/calendar/">Important Dates &amp;
                           Deadlines</a>.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <h3>CONTACT US</h3>
                     
                     <p>Visit us on campus at the <a href="http://valenciacollege.edu/answer-center/">Answer Center</a>.
                     </p>
                     
                     <p><a href="mailto:enrollment@valenciacollege.edu">enrollment@valenciacollege.edu</a></p>
                     
                     <p>407-582-1507</p>
                     
                     
                     <p>&nbsp;</p>
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/admissions/admissions-records/registration-details.pcf">©</a>
      </div>
   </body>
</html>