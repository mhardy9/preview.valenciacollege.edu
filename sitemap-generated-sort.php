<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Sitemap Generated | Valencia College</title>
      <meta name="Description" content="Generated sitemap from sitemap.xml">
      <meta name="Keywords" content="sitemap, site, map, menu"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/sitemap-generated.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- Collegewide Navigation ================================================== --><div class="container-fluid header-college"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu-collegewide.inc"); ?>
         <!-- Site Navigation ================================================== -->
         <nav class="container navbar navbar-expand-lg navbar-inverse">
            <div class="collapse navbar-collapse flex-row navbar-val"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/_menu.inc"); ?></div>
         </nav>
      </div>
      <div class="page-header bg-interior">
         <div id="intro-txt">
            <h1>Sitemap Generated</h1>
            <p class="page-menu"></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>Sitemap Generated</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid">
            	
            		
            <div class="container margin-30">
               		<?php include ($_SERVER["DOCUMENT_ROOT"]."/sitemap-small-sort-test.html"); 
		?>
               			
            </div>
            	
            <hr class="styled_2">
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/sitemap-generated.pcf">&copy; 2018  Valencia College.</a>
      </div>
   </body>
</html>