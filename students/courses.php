<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Students | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Students</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a href="courses.html"></a>
                        
                        
                        
                        <h2> Courses Offered </h2>
                        
                        <p>Find the scheduled listings for all courses currently offered on 
                           Valencia's campuses, on Valencia television and Valencia Online 
                           for the session.
                        </p>
                        
                        <ul>
                           
                           <li>
                              
                              <p><strong><a href="http://net4.valenciacollege.edu/promos/internal/schedule-main.cfm">Credit Class Schedule Search</a></strong> 
                              </p>
                              
                              <ul>
                                 
                                 <li><a href="http://net5.valenciacollege.edu/schedule/default.cfm?request=flex">Flex Start Class Schedule Search</a></li>
                                 
                                 <li><a href="http://preview.valenciacollege.edu/future-students/schedule-options/weekend-courses/">Weekend College Class Schedule Search</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="../public-safety/criminal-justice-institute/index.html">Criminal Justice Institute</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="../epi/index.html"> Educator Preparation Institute (EPI) Alternative Certification Program</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="academics/programs/index.html">Degree &amp; Career Programs</a> 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="http://www.valenciaenterprises.org/">Continuing Education, Training and Employee Development</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="../west/health/cehealth/index.html">Continuing Education for Health Professions</a></p>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/courses.pcf">©</a>
      </div>
   </body>
</html>