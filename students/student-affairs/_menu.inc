<ul>
<li><a href="index.php">Student Affairs</a></li>
<li class="submenu">
<a class="show-submenu" href="javascript:void(0);">Resources <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
<li><a href="assessmentPlan.php">Assessment Plan</a></li>
<li><a href="organizationChart.php">Organization Chart</a></li>
<li><a href="SALTMembers.php">Student Affairs Leadership Team (SALT)</a></li>
<li><a href="SALTMinutes.php">SALT Meetings</a></li>
</ul>
</li>
<li><a href="http://net4.valenciacollege.edu/forms/studentaffairs/contact.cfm" target="_blank">Contact Us</a></li>	
</ul>

