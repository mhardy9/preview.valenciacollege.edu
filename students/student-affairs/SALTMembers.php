<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>SALT Members | Student Affairs | Students | Valencia College</title>
      <meta name="Description" content="SALT Members | Student Affairs | Students">
      <meta name="Keywords" content="college, school, educational, salt, members, student, affairs">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-affairs/SALTMembers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Affairs</h1>
            <p>SALT Members</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-affairs/">Student Affairs</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>&nbsp;</h2>
                        
                        <h3>It is the mission of Student Affairs to encourage and engage students to fully participate
                           in their educational progress through empowerment, planning and partnerships that
                           result in successful completion of their career and educational goals.&nbsp; In addition,
                           it is the role of Student Affairs to strengthen organizational collaboration among
                           College departments to support student enrollment, retention and success.
                        </h3>
                        
                        <h3><span><a href="/students/student-affairs/documents/SALTMissionStatement.pdf">SALT Purpose Statements</a></span></h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">STUDENT AFFAIRS LEADERSHIP TEAM</td>
                                 
                              </tr>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <th valign="top"><strong></strong>Managers<span>&nbsp;</span><em>and assistants / 407-582-xxxx</em></th>
                                 
                                 <th valign="top">Direct Reports to VP</th>
                                 
                                 <th valign="top">MC</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Dr. Kim Sepich</strong></td>
                                 
                                 <td valign="top"><strong>Vice President for Student Affairs</strong></td>
                                 
                                 <td valign="top"><strong>DO-334</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Suzanne Eng (3401)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Amy Kleeman (1238)</strong></td>
                                 
                                 <td valign="top"><strong>Assistant V. P. College Transition Programs</strong></td>
                                 
                                 <td valign="top"><strong>4-25</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Hanna Bauman (1965)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Ben Lion (1388)</strong></td>
                                 
                                 <td valign="top"><strong>Dean of Students, West Campus</strong></td>
                                 
                                 <td valign="top"><strong>4-10</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Kathryn Snitzer (1344)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Daniel Barkowitz (1458)</strong></td>
                                 
                                 <td valign="top"><strong>Assistant V. P. Financial Aid &amp; Veterans Affairs</strong></td>
                                 
                                 <td valign="top"><strong>4-17</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Paula Kerr (1457)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Jill Szentmiklosi (4142)</strong></td>
                                 
                                 <td valign="top"><strong>Dean of Students, Osceola &amp; Lake Nona Campuses</strong></td>
                                 
                                 <td valign="top"><strong>6-2</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Eileen Lowe (4141)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Joe Sarrubbo (2586)</strong></td>
                                 
                                 <td valign="top"><strong>Dean of Students, East &amp; Winter Park Campuses</strong></td>
                                 
                                 <td valign="top"><strong>3-15</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Jackie Perron (2377)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Linda Herlocker (1511)</strong></td>
                                 
                                 <td valign="top"><strong>Assistant Vice President-Admissions &amp; Records</strong></td>
                                 
                                 <td valign="top"><strong>4-8</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Penny Waters (1506)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Sonya Joseph, (7734)</strong></td>
                                 
                                 <td valign="top"><strong>Assistant Vice President, Student Affairs</strong></td>
                                 
                                 <td valign="top"><strong>7-1</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Maritza Goodman(7997)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">Additional SALT members</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Brian Barilone (5781)</strong></td>
                                 
                                 <td valign="top"><strong>Director Enrollment Services</strong></td>
                                 
                                 <td valign="top"><strong>4-33</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Jorge Valentin (1969)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Cheryl Robinson (1373)</strong></td>
                                 
                                 <td valign="top"><strong>Director, Honors Program</strong></td>
                                 
                                 <td valign="top"><strong>4-16</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Daniel Chip Turner (5674)</strong></td>
                                 
                                 <td valign="top"><strong>Director Title III Project</strong></td>
                                 
                                 <td valign="top"><strong>4-11</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Elibeth Guerrero (1099)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Deborah Larew (2236)</strong></td>
                                 
                                 <td valign="top"><strong>Director Students with Disabilities</strong></td>
                                 
                                 <td valign="top"><strong>3-31</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Leah Anton (2934)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Donna MacDonald (5602)</strong></td>
                                 
                                 <td valign="top"><strong>Director, Student Financial Operations</strong></td>
                                 
                                 <td valign="top"><strong>4-17</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Ed Holmes (1127)</strong></td>
                                 
                                 <td valign="top"><strong>Campus Director Advising Title III West Campus</strong></td>
                                 
                                 <td valign="top"><strong>4-10</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Edwin Sanchez (1554)</strong></td>
                                 
                                 <td valign="top"><strong>Director Records &amp; Graduation</strong></td>
                                 
                                 <td valign="top"><strong>4-43</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Daisy Alzate (1283)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Erica Reese (1644)</strong></td>
                                 
                                 <td valign="top"><strong>Director of Standardized Testing</strong></td>
                                 
                                 <td valign="top"><strong>4-29</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Cheryl Kelley (1622)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Evelyn Lora-Santos (2022)</strong></td>
                                 
                                 <td valign="top"><strong>Campus Director Advising East Campus</strong></td>
                                 
                                 <td valign="top"><strong>3-15</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>John Britt (4964)</strong></td>
                                 
                                 <td valign="top"><strong>Campus Director Advising OC</strong></td>
                                 
                                 <td valign="top"><strong>6-2</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Julie Corderman (6868)</strong></td>
                                 
                                 <td valign="top"><strong>Director Student Services WPC</strong></td>
                                 
                                 <td valign="top"><strong>5-1</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Katrina Manley (1052)</strong></td>
                                 
                                 <td valign="top"><strong>Director Accounting, Bursar</strong></td>
                                 
                                 <td valign="top"><strong>DO-330</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Kelly Astro (3120)</strong></td>
                                 
                                 <td valign="top"><strong>Director Valencia Promise</strong></td>
                                 
                                 <td valign="top"><strong>4-46</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Lateshia Martin (1652)</strong></td>
                                 
                                 <td valign="top"><strong>Manager Student Services WC</strong></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Latishua Lewis (1992)</strong></td>
                                 
                                 <td valign="top"><strong>Director Dual Enrollment</strong></td>
                                 
                                 <td valign="top"><strong>4-48</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Kari Benson (1808)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Lauren Zanders (2207)</strong></td>
                                 
                                 <td valign="top"><strong>Manager Student Services EC</strong></td>
                                 
                                 <td valign="top"><strong>3-15</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Lisa Eli (6649)</strong></td>
                                 
                                 <td valign="top"><strong>Assistant V. P. Global and Cont Ed</strong></td>
                                 
                                 <td valign="top"><strong>4-47</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Lisa Stilke (1512)</strong></td>
                                 
                                 <td valign="top"><strong>Director Atlas Information System</strong></td>
                                 
                                 <td valign="top"><strong>4-43</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Mary McGowan (6069)</strong></td>
                                 
                                 <td valign="top"><strong>Director Student Services PNC</strong></td>
                                 
                                 <td valign="top"><strong>6-1</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Mindy Mozena (7780)</strong></td>
                                 
                                 <td valign="top"><strong>Director Student Services LNC</strong></td>
                                 
                                 <td valign="top"><strong>7-1</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Niurka Ferrer (1037)</strong></td>
                                 
                                 <td valign="top"><strong>Director Transition Planning</strong></td>
                                 
                                 <td valign="top"><strong>4-25</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Aira Pierce (1318)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Nicholle Trapp (4873)</strong></td>
                                 
                                 <td valign="top"><strong>Manager Student Services OSC</strong></td>
                                 
                                 <td valign="top"><strong>6-2</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Nick Estee (1556)</strong></td>
                                 
                                 <td valign="top"><strong>Interim Director Admissions &amp; Registration</strong></td>
                                 
                                 <td valign="top"><strong>4-8</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><em>Natalie Eck (1372)</em></td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                                 <td valign="top">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Reyna Rangel (1876)</strong></td>
                                 
                                 <td valign="top"><strong>Director Admission/Records Systems</strong></td>
                                 
                                 <td valign="top"><strong>4-43</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Scott Bianconi (5512)</strong></td>
                                 
                                 <td valign="top"><strong>Interim Director of Financial Aid Systems</strong></td>
                                 
                                 <td valign="top"><strong>4-17</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Tamika Martin (1501)</strong></td>
                                 
                                 <td valign="top"><strong>Director Student Financial Aid Services</strong></td>
                                 
                                 <td valign="top"><strong>4-17</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><strong>Tanisha Carter (5521)</strong></td>
                                 
                                 <td valign="top"><strong>Director Bridges to Success</strong></td>
                                 
                                 <td valign="top"><strong>4-39</strong></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <div data-old-tag="table">&nbsp;</div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-affairs/SALTMembers.pcf">©</a>
      </div>
   </body>
</html>