<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>SALT Meetings | Student Affairs | Students | Valencia College</title>
      <meta name="Description" content="SALT Minutes | Student Affairs | Students">
      <meta name="Keywords" content="college, school, educational, student, affairs, salt, meeting, minutes">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-affairs/saltminutes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Affairs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-affairs/">Student Affairs</a></li>
               <li>SALT Meetings</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>SALT Meeting Minutes</h2>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">SALT Meeting Minutes 2016-2017</td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td class="icon_for_pdf" height="25"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTRetreatNotes7-11-16JCR.pdf">July 2016 (Retreat)</a></td>
                                 
                                 <td>November 2016</td>
                                 
                                 <td>March 2017</td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td class="icon_for_pdf" height="23"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT8-12-16MinutesJCR.pdf">August 2016</a></td>
                                 
                                 <td class="icon_for_pdf"><a href="http://valenciacollege.edu/studentaffairs/documents/SALTMeetingminutesDecember152016.docx.pdf">December 2016</a></td>
                                 
                                 <td>April 2017</td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td height="24"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT9-8-16MinutesLM_000.pdf">September 2016</a></td>
                                 
                                 <td>January 2017</td>
                                 
                                 <td>May 2017</td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td height="24"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT10-13-16Minutesdraft-Sonyaupdates.pdf">October 2016</a></td>
                                 
                                 <td>February 2017</td>
                                 
                                 <td>June 2017</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">SALT Meeting Minutes 2015-2016</td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT7-27-15MeetingNotesfinal.pdf">July 27, 2015</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT11-23-15NotesJCR.pdf">November 23, 2015</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT3-17-16MinutesJCR.pdf">March 17, 2016</a></td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td>
                                    
                                    <p align="left"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes8-24-15finalJCR.pdf">August 24, 2015</a></p>
                                    
                                 </td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT12-14-15NotesJCR.pdf">December 14, 2015</a></td>
                                 
                                 <td class="icon_for_pdf"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT4-8-16MinutesJCR.pdf">April 8 , 2016</a></td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes9-21-15JCR.pdf">September 21, 2015</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT1-14-16MinutesLM.pdf">January 14, 2016</a></td>
                                 
                                 <td>
                                    
                                    <div align="center">
                                       
                                       <p class="icon_for_pdf" align="left">May 17, 2015 (cancelled)</p>
                                       
                                    </div>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr valign="middle">
                                 
                                 <td class="icon_for_pdf"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes10-26-15JCR.pdf">October 26, 2015</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT2-22-16MinutesLM.pdf">February 22, 2016</a></td>
                                 
                                 <td>June 13, 2016 (Postponed)</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>&nbsp;</p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr valign="top" bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">SALT Meeting Minutes 2014-2015</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT07-28-2014minutes.pdf">July 28, 2014</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT11-17-14MinutesFinal.pdf">November 17, 2014</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT3-30-15finalNotes.pdf">March 30, 2015</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT08-18-2014Minutesfinal.pdf">August 18, 2014</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT12-19-14Minutesfinal.pdf">December 19, 2014</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT4-27-15Notes.pdf">April 27, 2015</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT9-29-2014Minutesfinal.pdf">September 29, 2014</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT2-16-15MeetingNotes-final.pdf">January 26, 2015</a></td>
                                 
                                 <td>
                                    
                                    <p align="left"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTRetreat6-17-15NotesLY.pdf">June 17 , 2015 (Retreat)</a></p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT10-27-14MinutesFinal.pdf">October 27, 2014</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT2-16-15MeetingNotes.pdf">February 16, 2015</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALT6-22-15NotesJCR.pdf">June 22, 2015</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p align="left">&nbsp;</p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr bgcolor="#CCCCCC">
                                 
                                 <td class="tableHead_1" colspan="3">SALT Meeting Minutes 2013-2014</td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes7-15-13.pdf">July 15, 2013</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes11-18-13jcr.pdf">November 18, 2013</a></td>
                                 
                                 <td width="33%"><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes3-24-14final.pdf">March 24, 2014</a></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes8-19-13final.pdf">August 19, 2013</a></td>
                                 
                                 <td>December 16, 2013 (Retreat)</td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes4-28-14.pdf">April 28, 2014</a></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes9-30-13jr.pdf">September 30, 2013</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes1-27-14final.pdf">January 27, 2014</a></td>
                                 
                                 <td>May 21, 2014 (Retreat)</td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes10-28-13jcr.pdf">October 28, 2013</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes2-17-14final.pdf">February 17, 2014</a></td>
                                 
                                 <td><a class="icon_for_pdf" href="http://valenciacollege.edu/studentaffairs/documents/SALTMinutes6-23-14final.pdf">June 23, 2014</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <div data-old-tag="table">&nbsp;</div>
                        
                        <div data-old-tag="table">&nbsp;</div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-affairs/saltminutes.pcf">©</a>
      </div>
   </body>
</html>