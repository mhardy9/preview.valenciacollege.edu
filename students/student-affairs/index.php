<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Affairs | Students | Valencia College</title>
      <meta name="Description" content="Student Affairs | Students">
      <meta name="Keywords" content="college, school, educational, student, affairs">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-affairs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Affairs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Student Affairs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Welcome to the Student Affairs web site! </h2>
                        
                        <h2><img alt="Dr. Kim Sepich, Vice President for Student Affairs" height="386" src="images/kim-sepich.png" width="299"></h2>
                        
                        <p>Our Student Affairs division has focused since the mid-1990’s on the design and implementation
                           of systems that support students in the achievement of their career and educational
                           goals. This work has been accomplished through the development of three major systems:
                           LifeMap, Atlas, and Learning-Centered Student Services.
                        </p>
                        
                        <p><strong>LifeMap</strong> is our developmental advising model that promotes social and academic integration,
                           education and career planning, and acquisition of study and life skills. Organized
                           into a five stage model of student progression, the focus is on student planning and
                           goal setting, creating a normative expectation that student have life, career and
                           academic goals, and setting up a system of engagement to establish and document those
                           goals for each student. 
                        </p>
                        
                        
                        <p>We went live in 2002 with our <strong>Atlas</strong> portal that was designed to support our learning-centered focus and create "connection
                           and direction" for students, faculty and staff. In Atlas students can get information
                           about their educational plans, academic progress, services to support their goals,
                           and receive just in time prompts through a series of automated communications (e-mails)
                           that lets them know where they are in various educational processes (e.g. application
                           to enrollment, degree audits, graduation processing). We have Atlas labs on each campus
                           in which students can get help in using Atlas.
                        </p>
                        
                        
                        
                        
                        <div data-old-tag="table">
                           <div data-old-tag="tbody">
                              <div data-old-tag="tr">
                                 <div data-old-tag="td">
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">Important Links</div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                
                                                <p><a href="../student-services/index.php">Learning Centered Student Services</a></p>
                                                
                                                <p><strong>LifeMap Resources</strong></p>
                                                
                                                <ul>
                                                   
                                                   <li><a href="/about/lifemap/pbs/index.php">LifeMap on PBS</a></li>
                                                   
                                                   <li><a href="/about/lifemap/index.php">LifeMap</a></li>
                                                   
                                                </ul>                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        
                        <p>Finally, in 2003, we reorganized our <strong>service delivery model</strong> with a focus on student learning the end-to-end processes they need to complete their
                           educational goals at Valencia. Students begin their on-campus interactions at the
                           Answer Center where staff cross-trained in admissions to enrollment processes, advising,
                           financial aid, student records, and graduation can help them learn the educational
                           processes they need to complete at that point of their degree programs. The staff
                           help the student understand the complete process they are engaging rather than just
                           answer their first question. Our goal is to have each student leave the Answer Center
                           with an understanding of the process and knowing what their "next step" is. Other
                           student affairs staff continues to engage students through these processes based on
                           their area of expertise (advising and counseling, assessment, career counseling, financial
                           aid, student development, international students, students with disabilities, honors).
                           
                        </p>
                        
                        
                        <p>We’ve provided links on this page to information that might help you better understand
                           our focus and organization. Please email me if I can provide additional information
                           to help you.
                        </p>
                        
                        
                        <p>Dr. Kim Sepich </p>
                        
                        <p>Vice President for Student Affairs</p>
                        
                        <p><a href="mailto:ksepich@valenciacollege.edu">ksepich@valenciacollege.edu</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-affairs/index.pcf">©</a>
      </div>
   </body>
</html>