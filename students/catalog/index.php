<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Catalog | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/catalog/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/catalog/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Valencia Catalog</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Catalog</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>Current Catalog: <a href="http://catalog.valenciacollege.edu/">2017-2018</a>
                           
                        </h2>
                        
                        
                        
                        <p>Published at the beginning of the academic year, the catalog contains the necessary
                           information for you to plan your degree at Valencia and your career.
                        </p>
                        
                        <p>Refer to the most current <a href="http://net5.valenciacollege.edu/schedule/">credit class schedule search</a> for information on available classes.
                        </p>
                        
                        <p>Catalog Archive</p>
                        
                        <div>
                           
                           <ul>
                              
                              
                              <li><a href="http://catalog.valenciacollege.edu/catalogarchives/2016-17/">2016-2017</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/catalogarchives/2015-16/">2015-2016</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/catalogarchives/2014-15/">2014-2015</a></li>
                              
                              <li><a href="http://catalog.valenciacollege.edu/catalogarchives/2013-14/">2013-2014</a></li>
                              
                              <li><a href="archives/VC2012-13Catalog.pdf">2012-2013</a></li>
                              
                              <li><a href="archives/VCC2011-12Catalog.pdf">2011-2012</a></li>
                              
                              <li><a href="archives/VCC2010-11Catalog.pdf">2010-2011</a></li>
                              
                              <li><a href="archives/VCC2009-10Catalog.pdf">2009-2010</a></li>
                              
                              <li><a href="archives/VCC2008-09Catalog.pdf">2008-2009</a></li>
                              
                              <li><a href="archives/VCC2007-08Catalog.pdf">2007-2008</a></li>
                              
                              <li><a href="archives/VCC2006-07Catalog.pdf">2006-2007</a></li>
                              
                              <li><a href="archives/VCC2005-06Catalog.pdf">2005-2006</a></li>
                              
                              <li><a href="archives/VCC2004-05Catalog.pdf">2004-2005</a></li> 
                              
                              <li><a href="archives/VCC2003-04Catalog.pdf">2003-2004</a></li>
                              
                              <li><a href="archives/VCC2002-03Catalog.pdf">2002-2003</a></li>
                              
                              <li><a href="archives/VCC2001-02Catalog.pdf">2001-2002</a></li>
                              
                              <li><a href="archives/VCC2000-01Catalog.pdf">2000-2001</a></li>
                              
                              <li><a href="archives/VCC1999-00Catalog.pdf">1999-2000</a></li>
                              
                              
                           </ul>  
                           
                           <ul>
                              
                              <li><a href="archives/VCC1998-99Catalog.pdf">1998-1999</a></li>
                              
                              <li><a href="archives/VCC1997-98Catalog.pdf">1997-1998</a></li>
                              
                              <li><a href="archives/VCC1996-98Catalog.pdf">1996-1998</a></li>
                              
                              <li><a href="archives/VCC1994-96Catalog.pdf">1994-1996</a></li>
                              
                              <li><a href="archives/VCC1992-94Catalog.pdf">1992-1994</a></li>
                              
                              <li><a href="archives/VCC1990-92Catalog.pdf">1990-1992</a></li>
                              
                              <li><a href="archives/VCC1989-90Catalog.pdf">1989-1990</a></li>
                              
                              <li><a href="archives/VCC1988-90Catalog.pdf">1988-1990</a></li> 
                              
                              <li><a href="archives/VCC1987-88Catalog.pdf">1987-1988</a></li>
                              
                              <li><a href="archives/VCC1985-87Catalog.pdf">1985-1987</a></li>
                              
                              <li><a href="archives/VCC1984-85Catalog.pdf">1984-1985</a></li>
                              
                              <li><a href="archives/VCC1983-84Catalog.pdf">1983-1984</a></li>
                              
                              <li><a href="archives/VCC1982-83Catalog.pdf">1982-1983</a></li>
                              
                              <li><a href="archives/VCC1981-82Catalog.pdf">1981-1982</a></li> 
                              
                           </ul>
                           
                           <ul>
                              
                              <li><a href="archives/VCC1980-81Catalog.pdf">1980-1981</a></li>
                              
                              <li><a href="archives/VCC1979-80Catalog.pdf">1979-1980</a></li>
                              
                              <li><a href="archives/VCC1978-79Catalog.pdf">1978-1979</a></li>
                              
                              <li><a href="archives/VCC1977-78Catalog.pdf">1977-1978</a></li>
                              
                              <li><a href="archives/VCC1976-77Catalog.pdf">1976-1977</a></li>
                              
                              <li><a href="archives/VCC1975-76Catalog.pdf">1975-1976</a></li> 
                              
                              <li><a href="archives/VCC1974-75Catalog.pdf">1974-1975</a></li>
                              
                              <li><a href="archives/VCC1973-74Catalog.pdf">1973-1974</a></li>
                              
                              <li><a href="archives/VCC1972-73Catalog.pdf">1972-1973</a></li>
                              
                              <li><a href="archives/VCC1971-72Catalog.pdf">1971-1972</a></li>
                              
                              <li><a href="archives/VCC1970-71Catalog.pdf">1970-1971</a></li>
                              
                              <li><a href="archives/VCC1969-70Catalog.pdf">1969-1970</a></li>
                              
                              <li><a href="archives/VCC1968-69Catalog.pdf">1968-1969</a></li>
                              
                              <li><a href="archives/VCC1967-68Catalog.pdf">1967-1968</a></li>
                              
                           </ul>
                           
                        </div>
                        <br><br>
                        
                        
                        <div>    
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    
                                    <div data-old-tag="td">In order to view PDF files, you will need Adobe Reader which is available 
                                       for free from the <a href="http://get.adobe.com/reader/" target="_blank">Adobe</a> 
                                       web site. 
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>  
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/catalog/index.pcf">©</a>
      </div>
   </body>
</html>