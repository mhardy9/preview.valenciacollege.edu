<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Community Partnerships | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/community-partnerships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Community Partnerships</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        
                        <h2>Community Partnerships</h2>
                        
                        
                        <h3>Become a Community Partner</h3>
                        
                        
                        <p>The Bridges to Success program has provided a wide range of support services for minority
                           and socioeconomically disadvantaged students who need assistance with the college
                           process. An important
                           element in a student's success at Valencia is the connection with people and services
                           that can assist them
                           when needed.
                        </p>
                        
                        <p>If you are interested in becoming an official community partner with Bridges to Success.
                           <br>
                           			  Please complete the form below and fax to 407-582-1304.
                        </p>
                        
                        
                        <div><a href="/students/bridges-to-success/documents/bridges-partnership-agreement-form.pdf">Partnership Agreement Form</a></div>
                        
                        <div>
                           
                           <p><strong>Agencies on the list below cannot be used as a community service location.</strong><br> Approved locations can be found on the following pages:
                              			  
                              <ul>
                                 			  
                                 <li><a href="/students/bridges-to-success/community-service-locations.php">Community Service Locations</a></li>
                                 			  
                                 <li><a href="/students/bridges-to-success/orange-county-public-schools-community-service-locations.php">Orange County Public Schools Community Service Locations</a></li>
                                 			  
                                 <li><a href="/students/bridges-to-success/community-service-locations.php">Osceola County Community Service Locations</a></li>
                                 			  
                              </ul>
                           </p>
                           
                        </div>
                        
                        <div>PDF Version of all Community Service Locations <a href="/students/bridges-to-success/documents/bridges-community-partnership-list-2017-18.pdf">Community Partners List</a></div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/community-partnerships.pcf">©</a>
      </div>
   </body>
</html>