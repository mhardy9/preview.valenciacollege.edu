<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Orange County Public Schools Community Service Locations | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/orange-county-public-schools-community-service-locations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Orange County Public Schools Community Service Locations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		  
                        
                        <h2>Orange County Public Schools Community Service Locations</h2>
                        
                        <p>Below is a list of volunteer locations, names of the representative for each location,
                           and the contact
                           information for that representative.
                        </p>
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Acceleration Academy</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2274 Semoran Blvd, Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-992-0917
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Acceleration Academy West</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2751 Lake Stanley Rd., Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-521-2358
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Access Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1100 Lee Road, Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 321-319-0640
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Addictions Receiving Facility</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 823 W. Central Blvd., Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-836-8729
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Adolescent Sub. Abuse Program</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5600 Clarcona Ocoee Road, Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 407-521-2495
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Aloma Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2949 Scarlet Road, Winter Park, FL 32792<br>
                                    <strong>Telephone:</strong> 407-672-3100
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Apopka Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 425 N. Park Avenue, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-884-2208
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Avalon Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 13914 Mailer Blvd., Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-207-7839
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Bay Meadows Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9150 S. Apopka Vineland Road, Orlando, FL 32836<br>
                                    <strong>Telephone:</strong> 407-876-7500
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Bonneville Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 14700 Sussex Drive, Orlando, FL 32826<br>
                                    <strong>Telephone:</strong> 407-249-6290
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Bridgewater Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5660 Tiny Road, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-905-3710
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Brookshire Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 400 Greene Drive, Winter Park, FL 32729<br>
                                    <strong>Telephone:</strong> 407-623-1400
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Camelot Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 14501 Waterford Chase Pkwy, Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-207-3875
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Carver Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4500 W. Columbia Street, Orlando, FL 32811<br>
                                    <strong>Telephone:</strong> 407-296-5110
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Castle Creek Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1245 N. Avalon Park Blvd., Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1735
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Central FL. Leadership Acad. Charther</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 427 N. Primrose Drive, Orlando, FL 32803<br>
                                    <strong>Telephone:</strong> 407-480-2352
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Chain of Lakes Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8720 Conroy Windermere Road, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-909-5400
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Cheney Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2000 N. Forsyth Road, Orlando, FL 32807<br>
                                    <strong>Telephone:</strong> 407-672-3120
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Chickasaw Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6900 Autumnvale Drive, Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-249-6300
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Citrus Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 87 N. Clarke Road, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-445-5475
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Clarcona Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3607 Damon Road, Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-844-2220
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Clay Springs Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 555 N. Wekiwa Springs Road, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-884-2275
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Columbia Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 18501 Cypress Lake Glen Blvd., Orlando, FL 32820<br>
                                    <strong>Telephone:</strong> 407-568-2921
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Conway Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4100 Lake Margaret Drive, Orlando, FL 32812<br>
                                    <strong>Telephone:</strong> 407-249-6310
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Conway Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4600 Anderson Road, Orlando, FL 32812<br>
                                    <strong>Telephone:</strong> 407-249-6420
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Corner Lake Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1700 Chuluota Road, Orlando, FL 32820<br>
                                    <strong>Telephone:</strong> 407-568-0510
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Cypress Creek High School </h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1101 Bear Crossing Drive, Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-852-3415
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Cypress Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9601 11th Avenue, Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-858-3100
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Cypress Springs Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 10401 Cypress Springs PKWY, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-249-6950
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Deerwood Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1356 S. Econlockhatchee Trail, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-249-6320
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Dillard Street Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 311 N. Dillard Street, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-877-5000
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Discovery Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 601 Woodbury Road, Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-384-1555
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Dommerich Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1900 Choctaw Trail, Maitland, FL 34787<br>
                                    <strong>Telephone:</strong> 407-623-1407
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Dover Shores Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1200 Gaston Foster Road, Orlando, FL 32812<br>
                                    <strong>Telephone:</strong> 407-249-6330
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Dr. Phillips Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6909 Dr. Phillips Blvd., Orlando, FL 32819<br>
                                    <strong>Telephone:</strong> 407-354-2600
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Dream Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 500 N.Park Ave, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-884-2227
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Drop Back In</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 228 N. Semoran Blvd., Orlando, FL 32807<br>
                                    <strong>Telephone:</strong> 407-658-9555
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Durrance Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8101 Benrus Street, Orlando, FL 32827<br>
                                    <strong>Telephone:</strong> 407-858-3110
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Eagles Nest Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5353 Metrowest Blvd., Orlando, FL 32811<br>
                                    <strong>Telephone:</strong> 407-521-2795
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>East Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3971 N. Tanner Road, Orlando, FL 31826<br>
                                    <strong>Telephone:</strong> 407-658-6825
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>East Orlando Education Center</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1900 N. Alafaya Trail Ste. 100, Orlando, FL 32826<br>
                                    <strong>Telephone:</strong> 407-241-4700
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>East River High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 654 Columbia School Road, Orlando, FL 32833<br>
                                    <strong>Telephone:</strong> 407-956-8550
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Eccleston Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1500 Aaron Avenue, Orlando, FL 32811<br>
                                    <strong>Telephone:</strong> 407-296-6400
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Edgewater High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3100 Edgewater Drive, Orlando, FL 32804<br>
                                    <strong>Telephone:</strong> 407-835-4900
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Endeavor Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 13501 Balcombe Road, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-251-2560
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Engelwood Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 900 Engel Drive, Orlando, FL 32807<br>
                                    <strong>Telephone:</strong> 407-249-6340
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Evans High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4949 Silver Star Road, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-522-3400
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>FL. Mall Education Center</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 730 Sand Lake Road Suite 176, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-858-6114
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Fern Creek Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1121 N. Fern Creek Avenue, Orlando, FL 32803<br>
                                    <strong>Telephone:</strong> 407-897-6410
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Forsyth Woods Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6651 Curtis Street, Orlando, FL 32807<br>
                                    <strong>Telephone:</strong> 407-207-7495
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Frangus Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 380 Killington Way, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-296-6469
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Freedom High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2500 Taft Vineland Road, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-816-5600
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Freedom Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2850 Taft Vineland Road, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-6130
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Glenridge Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2900 Upper Park Road, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-6130
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Gotha Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9155 Gotha Road, Windermere, FL 34786<br>
                                    <strong>Telephone:</strong> 407-251-2360
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Grand Ave. Prim. Learning Center</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 800 W. Grand Street, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1750
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hiawassee Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6800 Hennepin Blvd., Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-296-6410
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hidden Oaks Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9051 Suburban Drive, Orlando, FL 32829<br>
                                    <strong>Telephone:</strong> 407-296-6350
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hillcrest Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1010 E. Concord Street, Orlando, FL 32803<br>
                                    <strong>Telephone:</strong> 407-245-1770
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Howard Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 800 E. Robinson Street, Orlando, FL 32801<br>
                                    <strong>Telephone:</strong> 407-245-1780
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hungerford Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 230 S. College Avenue, Eatonville, FL 32751<br>
                                    <strong>Telephone:</strong> 407-623-1430
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hunters Creek Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4650 Town Center Blvd., Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-4610
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Hunters Creek Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 13400 Town Loop Blvd., Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-4620
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Innovations Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2768 N. Hiawassee Road, Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-429-7901
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ivey Lane Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 209 Silverton Street, Orlando, FL 32811<br>
                                    <strong>Telephone:</strong> 407-296-6420
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Jackson Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6000 Stonewall Jackson Road, Orlando, FL 32807<br>
                                    <strong>Telephone:</strong> 407-249-6430
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>John Young Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12550 Marsfield Avenue, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-3120
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Jones High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 801 S. Rio Grande Avenue, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-835-2300
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Kaley Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1600 E. Kaley Avenue, Orlando, FL 32806<br>
                                    <strong>Telephone:</strong> 407-897-6420
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Keens Crossing Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5240 Keens Pheasant Dr., Windermere, FL 34786<br>
                                    <strong>Telephone:</strong> 407-654-1351
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Killarney Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2401 Wellington Blvd., Winter Park, FL 32789<br>
                                    <strong>Telephone:</strong> 407-623-1438
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Como Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 901 S. Bumby Avenue, Orlando, FL 32806<br>
                                    <strong>Telephone:</strong> 407-897-6430
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Gem Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4801 Bloodhound Street, Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-532-7900
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake George Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4101 Gatlin Avenue, Orlando, FL 32812<br>
                                    <strong>Telephone:</strong> 407-737-1430
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Nona High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12500 Narcoosee Road, Orlando, FL 32832<br>
                                    <strong>Telephone:</strong> 407-956-8300
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Nona Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 13700 Narcoossee Road, Orlando, FL 32832<br>
                                    <strong>Telephone:</strong> 407-858-5522
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Silver Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2401 N. Rio Grande Avenue, Orlando, FL 32804<br>
                                    <strong>Telephone:</strong> 407-245-1850
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Sybelia Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 600 Sandspur Road, Maitland, FL 32751<br>
                                    <strong>Telephone:</strong> 407-623-1445
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Weston Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5300 Milan Drive, Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 407-296-6430
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lake Whitney Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1351 Windermere Road, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-877-8888
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lakemont Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 901 N. Lakemont Avenue, Winter Park, FL 32792<br>
                                    <strong>Telephone:</strong> 407-623-1453
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lakeview Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1200 W. Bay Street, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-877-5010
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lakeville Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2015 Lakeville Road, Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-814-6110
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lancaster Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6700 Sheryl Ann Drive, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-858-3130
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lawton Chiles Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11001 Bloomfield Drive, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-737-1470
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lee Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1201 Maury Road, Orlando, FL 32804<br>
                                    <strong>Telephone:</strong> 407-245-1800
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Legacy High Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1550 E. Crown Point Road, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-656-4673
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Legacy Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11398 Lk Underhill Road, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-658-5330
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Liberty Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3405 S. Chickasaw Trail, Orlando, FL 32829<br>
                                    <strong>Telephone:</strong> 407-249-6440
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Life Skills Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4504 S. Orange Blossom Trail, Orlando, FL 32839<br>
                                    <strong>Telephone:</strong> 407-816-3566
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Little River Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 100 Caswell Drive, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-249-6360
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lockhart Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 7500 Edgewater Drive, Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 407-296-6440
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lockhart Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3411 Doctor Love Blvd., Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 407-296-5120
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Lovell Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 815 Roger Williams Road, Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-884-2235
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Maitland Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1901 Choctaw Trail, Maitland, FL 32751<br>
                                    <strong>Telephone:</strong> 407-623-1462
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Maxey Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1100 E. Maple Street, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-877-5020
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Mccoy Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5225 S. Semoran Blvd., Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-249-6370
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Meadow Woods Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 500 Rhode Island Woods Cir., Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-858-3140
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Meadow Woods Middle School/Avid Program</h4>
                                 
                                 <p>
                                    <strong>Address: </strong> 1800 Rhode Island Woods Circle, Orlando, FL 32824<br>
                                    <strong>Telephone: </strong> 407-850-5180
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Meadowbrook Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6000 North Lane, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-296-5130
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Memorial Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2220 W. 29th, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1810
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Metrowest Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1801 Lake Vilma Drive, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-296-6450
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Millennia Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5301 Cypress Creek Blvd., Orlando, FL 32811<br>
                                    <strong>Telephone:</strong> 407-355-5730
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Mollie Ray Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2000 Beecher Street, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-296-6460
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Moss Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9301 N. Shore Golf Club Blvd., Orlando, FL 32832<br>
                                    <strong>Telephone:</strong> 407-249-4747
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Nap Ford Community Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 648 W. Livingston Street, Orlando, FL 32801<br>
                                    <strong>Telephone:</strong> 407-245-8711
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>New Choices Academy</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6125 N. OBT, Orlando, FL 32810<br>
                                    <strong>Telephone:</strong> 407-317-3636
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>NorthLake Park Comm. Elem.</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9055 Northlake Pwky, Orlando, FL 32827<br>
                                    <strong>Telephone:</strong> 407-852-3500
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Northstar High Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8287 Curry Ford Road, Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-273-1188
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Oak Hill Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11 S. Hiawassee Road, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-296-6470
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>OakRidge High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 700 W. Oak Ridge Road, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-852-3200
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Oakland Avenue Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 456 E. Oakland Avenue, Oakland, FL 34760<br>
                                    <strong>Telephone:</strong> 407-877-2039
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Oakshire Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 14501 Oakshire Blvd., Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-251-2500
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ocoee Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 400S. Lakewood Avenue, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-877-5027
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ocoee High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1925 Ocoee Crown Point Pkwy, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-905-3000
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ocoee Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 300 Bulford Avenue, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-877-5035
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Odyssey Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 9290 Lee Vista Blvd., Orlando, FL 32829<br>
                                    <strong>Telephone:</strong> 407-207-3850
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Olive Branch Academy Chater</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2525 W. Church Street, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-296-6568
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Olympia High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4301 S. Apopka Vineland Road, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-905-6400
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Orange Center Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 621 S. Texas Avenue, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-296-6480
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Orlo Vista Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3 N. Hastings Street, Orlando, FL 32835<br>
                                    <strong>Telephone:</strong> 407-296-6490
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Palm Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8000 Pine Oak Drive, Orlando, FL 32819<br>
                                    <strong>Telephone:</strong> 407-354-2610
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Palmetto Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2015 Duskin Avenue, Orlando, FL 32839<br>
                                    <strong>Telephone:</strong> 407-858-3150
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Pershing Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1800 Pershing Avenue, Orlando, FL 32806<br>
                                    <strong>Telephone:</strong> 407-858-3160
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Piedmount Lake Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2601 Lakeville Road, Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-884-8865
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Pinar Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3701 Anthony Lane, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-249-6380
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Pine Castle Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 905 Waltham Avenue, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-858-3170
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>PineHills Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1006 Ferndell Road, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-296-6500
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>PineLoch Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3101 Woods Avenue, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1825
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>PineWood Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3005 N. Apopka Vineland Road, Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-532-7930
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Pinecrest Prep. High Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8503 Daetwyler Drive, Orlando, FL 32827<br>
                                    <strong>Telephone:</strong> 407-856-8359
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Princeton Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1500 Falcon Drive, Orlando, FL 32803<br>
                                    <strong>Telephone:</strong> 407-245-1840
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Richmond Heights Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2500 Bruton Blvd., Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1870
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ridgewood Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3401 Pioneer Road, Orlando, FL 32803<br>
                                    <strong>Telephone:</strong> 407-296-6510
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Rio Grande Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2210 S. Rio Grande Avenue, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-649-9122
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Riverdale Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11301 Lokanotosa Trail, Orlando, FL 32817<br>
                                    <strong>Telephone:</strong> 407-737-1400
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Riverside Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3125 Pembrook Drive, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1880
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Robinswood Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6305 Balboa Drive, Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-296-5140
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Rock Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 408 N. Tampa Avenue, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-245-1880
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Rock Springs Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 800 N. Wells Street, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-884-2242
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Rolling Hills Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4903 Donavan Street, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-296-6530
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Rosemont Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4650 Point Look Out Road, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-522-6050
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Sadler Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4000 W. Oak Ridge Road, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-354-2620
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Sand Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 8301 Buena Vista Woods Blvd., Orlando, FL 32836<br>
                                    <strong>Telephone:</strong> 407-903-7400
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Sheeler High Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 875 S. Semoran Blvd., Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-886-1825
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Shenandoah Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4827 S.Conway Road, Orlando, FL 32812<br>
                                    <strong>Telephone:</strong> 407-858-3180
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Shingle Creek Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5620 Harcourt Avenue, Orlando, FL 32839<br>
                                    <strong>Telephone:</strong> 407-354-560
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>South Creek Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3801 E. Wetherbee Road, Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-251-2413
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Southwest Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6450 Dr. Phillips Blvd., Orlando, FL 32819<br>
                                    <strong>Telephone:</strong> 407-370-7200
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Southwood Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12600 Bisted Drive, Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-858-2230
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Spring Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 115 Spring Lake Circle, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-877-5047
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Stone Lakes Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 15200 Stoneybrook Blvd, Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-207-7793
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Sunrise Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 101 Lone Palm Road, Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-384-1585
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Sunset Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12050 Overstreet Road, Windermere, FL 34786<br>
                                    <strong>Telephone:</strong> 407-905-3724
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Tangelo Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5115 Anzio Street, Orlando, FL 32819<br>
                                    <strong>Telephone:</strong> 407-354-2630
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Thornebrooke Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 601 Thornebrooke Drive, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-909-1301
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Three Points Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4001 S. Goldenrod Road, Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-207-3800
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Tildenville Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1221 Brick Road, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-877-5054
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Timber Creek High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1001 Avalon Park Blvd., Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 321-235-7800
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Timber Lakes Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2149 Crown Hill Blvd., Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-249-6177
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>UCP Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3305 S. Orange Avenue, Orlando, FL 32806<br>
                                    <strong>Telephone:</strong> 407-852-3333
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>UCP Charter (EAST)</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12702 Science Drive, Orlando, FL 32826<br>
                                    <strong>Telephone:</strong> 407-281-0441
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>UCP Charter (PINEHILLS)</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5800 Golf Club Pkwy, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-299-5553
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Union Park Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1600 N. Dean Road, Orlando, FL 32825<br>
                                    <strong>Telephone:</strong> 407-249-6390
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Union Park Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1844 WestFall Drive, Orlando, FL 32817<br>
                                    <strong>Telephone:</strong> 407-249-6309
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Universal Education Center</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1000 Universal Studio Plz, Orlando, FL 32819<br>
                                    <strong>Telephone:</strong> 407-224-6634
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Universal High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2450 Cougar Way, Orlando, FL 32817<br>
                                    <strong>Telephone:</strong> 407-482-8700
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Ventura Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 4400 Woodgate Blvd., Orlando, FL 32822<br>
                                    <strong>Telephone:</strong> 407-249-6400
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Vista Lakes Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 6050 Lake Champlain Drive, Orlando, FL 32829<br>
                                    <strong>Telephone:</strong> 407-207-4991
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Walker Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 150 Amidon Lane, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-858-3210
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Washington Shores Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 944 W. Lake Mann Drive, Orlando, FL 32805<br>
                                    <strong>Telephone:</strong> 407-296-6540
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>WaterFord Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 12950 Lake Underhill Road, Orlando, FL 32828<br>
                                    <strong>Telephone:</strong> 407-249-6410
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Waterbridge Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11100 Galvin Drive, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-3190
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wekiva High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2501 N. Hiawassee Road, Apopka, FL 32703<br>
                                    <strong>Telephone:</strong> 407-297-4900
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>West Creek Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 5026 Tacon Drive, Orlando, FL 32837<br>
                                    <strong>Telephone:</strong> 407-858-5920
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>West Oaks Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 905 Dorscher Road, Orlando, FL 32818<br>
                                    <strong>Telephone:</strong> 407-532-3875
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>West Orange High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1625 Beulah Road, Winter Garden, FL 32787<br>
                                    <strong>Telephone:</strong> 407-905-2400
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Westbrooke Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 500 Tomyn Blvd, Ocoee, FL 34761<br>
                                    <strong>Telephone:</strong> 407-656-6228
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Westrigde Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3800 W. Oak Ridge Road, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-354-2640
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wetherbee Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 701 E. Wetherbee Road, Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-850-5130
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wheatley Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2 W. 18th Street, Orlando, FL 32703<br>
                                    <strong>Telephone:</strong> 407-884-2250
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Whisphering Oak Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 15300 Stoneybrook West Pkwy, Winter Garden, FL 34787<br>
                                    <strong>Telephone:</strong> 407-465-7773
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Windermere Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 11125 Park Avenue, Windermere, FL 34786<br>
                                    <strong>Telephone:</strong> 407-876-7520
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>WineGard Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 7055 Winegard Road, Orlando, FL 32809<br>
                                    <strong>Telephone:</strong> 407-858-3200
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Winter Park High 9th Gd Center</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 528 Huntington Avenue, Winter Park, FL 32789<br>
                                    <strong>Telephone:</strong> 407-623-1476
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Winter Park High School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2100 Summerfield Road, Winter Park, FL 32792<br>
                                    <strong>Telephone:</strong> 407-622-3200
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wolf Lake Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1771 Ponkan Road, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-464-3342
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wolf Lake Middle School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 1725 W. Pokan Road, Apopka, FL 32712<br>
                                    <strong>Telephone:</strong> 407-464-3317
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Workforce Adv Academy Charter</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 2113 E. South Street, Orlando, FL 32808<br>
                                    <strong>Telephone:</strong> 407-898-7228
                                 </p>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Wyndham Lakes Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 14360 Wyndham Lakes Blvd., Orlando, FL 32824<br>
                                    <strong>Telephone:</strong> 407-251-2347
                                 </p>
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Zellwood Elem. School</h4>
                                 
                                 <p>
                                    <strong>Address:</strong> 3551 N. Washington Street, Zellwood, FL 32798<br>
                                    <strong>Telephone:</strong> 407-884-2258
                                 </p>
                              </div>
                              
                           </div>
                           
                        </div>
                        	  
                     </div>
                     		
                  </div>
                  		
               </div>
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/orange-county-public-schools-community-service-locations.pcf">©</a>
      </div>
   </body>
</html>