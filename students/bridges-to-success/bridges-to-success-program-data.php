<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Bridges to Success | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/bridges-to-success-program-data.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Bridges to Success</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Bridges to Success Program Data</h2>  
                        
                        <p>
                           
                           
                           
                           
                           <div class="tableauPlaceholder" id="viz1481897621532" style="position: relative">
                              <noscript><a href=""><img alt=" " src="https://public.tableau.com/static/images/Br/BridgesStudentCharacteristics/Summary/1_rss.png" style="border: none"></a></noscript><object class="tableauViz" style="display:none;">
                                 <param name="host_url" value="https%3A%2F%2Fpublic.tableau.com%2F"> 
                                 <param name="site_root" value="">
                                 <param name="name" value="BridgesStudentCharacteristics/Summary">
                                 <param name="tabs" value="yes">
                                 <param name="toolbar" value="yes">
                                 <param name="static_image" value="https://public.tableau.com/static/images/Br/BridgesStudentCharacteristics/Summary/1.png"> 
                                 <param name="animate_transition" value="yes">
                                 <param name="display_static_image" value="yes">
                                 <param name="display_spinner" value="yes">
                                 <param name="display_overlay" value="yes">
                                 <param name="display_count" value="yes"></object></div>                <script type="text/javascript">                    var divElement = document.getElementById('viz1481897621532');                    var vizElement = divElement.getElementsByTagName('object')[0];                    vizElement.style.width='980px';vizElement.style.height='1500px';                    var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);                </script></p>
                     </div>
                     	
                     
                     
                     
                     
                     
                     				
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/bridges-to-success-program-data.pcf">©</a>
      </div>
   </body>
</html>