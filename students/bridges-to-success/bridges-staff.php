<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Bridges Staff | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/bridges-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Bridges Staff</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Bridges Staff</h2>
                        
                        <p></p>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-dr-tanisha-d-carter.jpg" alt="Dr. Tanisha D. Carter" class="img-circle">
                              
                              <h4>Dr. Tanisha D. Carter</h4>
                              	
                              <p><strong>Director</strong><br>
                                 			<strong>Office Location:</strong> SSB-173-B<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-7533">407-582-7533</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:carter27@valenciacollege.edu">carter27@valenciacollege.edu</a></p> 
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-joy-benjamin-fieulleteau.jpg" alt="Joy Benjamin-Fieulleteau" class="img-circle">
                              
                              <h4>Joy Benjamin-Fieulleteau</h4>
                              	
                              <p><strong>Assistant Director</strong><br>
                                 			<strong>Office Location:</strong> SSB-173-C<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-1495">407-582-1495</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:benjaminfieullet@valenciacollege.edu">benjaminfieullet@valenciacollege.edu</a></p> 
                              		
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-tracie-godfrey.jpg" alt="Tracie Godfrey" class="img-circle">
                              
                              <h4>Tracie Godfrey</h4>
                              	
                              <p><strong>Academic Advisor</strong><br>
                                 			<strong>Office Location:</strong> 3-104-C<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-4803">407-582-4803</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:tgodfrey3@valenciacollege.edu">tgodfrey3@valenciacollege.edu</a></p> 
                              	
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-alexia-mitchell.jpg" alt="Alexia Mitchell" class="img-circle">
                              
                              <h4>Alexia Mitchell</h4>
                              	
                              <p><strong>Staff Assistant</strong><br>
                                 			<strong>Office Location:</strong> 5-210<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-2730">407-582-2730</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:amitchell19@valenciacollege.edu">amitchell19@valenciacollege.edu</a></p> 
                              		
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-umu-graham.jpg" alt="Umu Graham" class="img-circle">
                              
                              <h4>Umu Graham</h4>
                              	
                              <p><strong>Academic Advisor</strong><br>
                                 			<strong>Office Location:</strong><br>
                                 			<i class="far fa-phone"></i> 	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:ugraham@valenciacollege.edu">ugraham@valenciacollege.edu</a></p> 
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-reginald-drummond.jpg" alt="Reginald Drummond" class="img-circle">
                              
                              <h4>Reginald Drummond</h4>
                              	
                              <p><strong>Academic Advisor</strong><br>
                                 			<strong>Office Location:</strong> SSB-173-E<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-1190">407-582-1190</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:rdrummond1@valenciacollege.edu">rdrummond1@valenciacollege.edu</a></p> 
                              		
                           </div>
                           
                        </div>
                        	
                        	
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-paula-quero-rendon.jpg" alt="Paula Quero Rendon" class="img-circle">
                              
                              <h4>Paula Quero Rendon</h4>
                              	
                              <p><strong>Staff Assistant</strong><br>
                                 			<strong>Office Location:</strong> <br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-1395">407-582-1395</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:pquero@valenciacollege.edu">pquero@valenciacollege.edu</a></p> 
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-jamesia-scott.jpg" alt="Jamesia Scott" class="img-circle">
                              
                              <h4>Jamesia Scott</h4>
                              	
                              <p><strong>Academic Advisor</strong><br>
                                 			<strong>Office Location:</strong> 5-210<br>
                                 			<i class="far fa-phone"></i> <a href="tel:407-582-2260">407-582-2260</a>	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:jscott52@valenciacollege.edu">jscott52@valenciacollege.edu</a></p> 
                              		
                           </div>
                           
                        </div>
                        	
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              <img src="/students/bridges-to-success/images/staff-photo-heather-montes.jpg" alt="Heather Montes" class="img-circle">
                              
                              <h4>Heather Montes</h4>
                              	
                              <p><strong>Academic Advisor</strong><br>
                                 			<strong>Office Location:</strong> 2-103-A<br>
                                 			<i class="far fa-phone"></i> 	<br>		  
                                 			<i class="far fa-envelope"></i> <a href="mailto:hmontes@valenciacollege.edu">hmontes@valenciacollege.edu</a></p> 
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              
                              <h4></h4>
                              	
                              <p><strong></strong><br>
                                 			<strong></strong><br>
                                 			<br>		  
                                 			
                              </p> 
                              		
                           </div>
                           
                        </div>
                        	
                        	
                        	
                        	
                     </div>
                     
                     	
                     
                  </div>
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/bridges-staff.pcf">©</a>
      </div>
   </body>
</html>