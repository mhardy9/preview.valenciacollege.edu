<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Community Service Frequently Asked Questions | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/community-service-frequently-asked-questions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Community Service Frequently Asked Questions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        	
                        <div class="container margin-60">
                           <div class="main-title">
                              						
                              <h2>Community Service Frequently Asked Questions</h2>
                              						
                              <p>Bridges to Success Community Service and Workshop FAQs</p>
                              					
                           </div>
                           						
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										 
                                    <h3>Can I change my location in the middle of the year?</h3>
                                    
                                    <ul>
                                       <li type="a"> No. The location you indicate on the community service contract is for an entire
                                          academic year (fall and
                                          spring only).
                                       </li>
                                       
                                       
                                       <li type="a"> Tip: Make sure you have already contacted and signed up with your location before
                                          you submit your contract. If you submit your contract before contacting your location,
                                          and you find out later that you
                                          are unable to volunteer at the site, it will create issues with you trying to change
                                          locations with the
                                          Bridges office.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Do we have to do community service and workshops during summer term?</h3> 
                                    <ul>
                                       	
                                       <li type="a"> No. Unless you have been mandated by an advisor.</li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Can I pick any locations on my own?</h3> 
                                    <ul>
                                       
                                       <li type="a"> Yes and No. You are allowed to choose your own location as long as it is from the
                                          approved community
                                          service list available on the Bridges website.
                                          
                                          <li type="a"> Tip: Pick a location associated with your academic or career goals. For example,
                                             if you want to go to
                                             medical school then pick a hospital or medical center to volunteer.
                                          </li>
                                          Spend time looking through the list and do research about the location (i.e. Read
                                          over their website, call
                                          their offices and ask questions, etc.). Do not pick a location randomly.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Can I roll over hours and/or workshops from last semester into this semester?</h3> 
                                    										
                                    <ul>
                                       											
                                       <li type="a"> No. All hours and workshops must be completed in their respective semester.</li>
                                       
                                       <li type="a"> Tip: Do no attempt to complete all hours for the year in a few weeks. This will not
                                          be accepted and you
                                          will only burden yourself more.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Is the Transfer Plan Workshop listed on the Bridges website considered a workshop?</h3> 
                                    <ul>
                                       
                                       <li type="a"> No. The Transfer Plan Workshop (Soon to be named Transition Planning Seminar) is
                                          a required
                                          workshop for upcoming graduates. It is not counted as a workshop.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Can I do my Community Service hours with Valencia Volunteers?</h3> 
                                    <ul>
                                       
                                       <li type="a"> No. Valencia Volunteers often offers volunteer opportunities for campus events. These
                                          events are
                                          considered services to the college and we highly recommend and encourage students
                                          to participate.
                                          However, since they are not considered service in the community they cannot be used
                                          towards
                                          community service hours.
                                       </li>
                                       
                                       <li type="a"> Tip: Use the community service list from the Bridges website.</li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Do I have to choose only workshops from the Skillshop Handbook?</h3> 
                                    <ul>
                                       
                                       <li type="a"> No. Valencia College offers many workshops not listed in the Skillshop Handbook.
                                          As long as the
                                          workshop you attend is associated with the college and teaches you something fundamental
                                          you can
                                          attend. Valencia College often sends these special workshops via e-mail and/or on
                                          the Valencia College
                                          website.
                                       </li>
                                       i. If you are interested in attending a workshop offered at another college (i.e.
                                       Rollins College,
                                       UCF, etc.), please inquire first with your Bridges advisor. 
                                       
                                       <li type="a"> Tip: Be sure to pick Skillshops and workshops that are meaningful to you. Do not
                                          wait till the last
                                          minute to do random workshops. Be thoughtful with the workshops you choose. Ask yourself,
                                          “Does
                                          this pertain to my academic/career goals”? “Will this workshop help in reaching a
                                          short term/long term
                                          goal?”
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Does my Bridges Presentation count as one of my required workshops? (New Students
                                       Only).
                                    </h3> 
                                    <ul>
                                       
                                       <li type="a"> No.</li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>I forgot my Bridges workshop form, but they handed out flyers at the Skillshop. Can
                                       I just turn that in as proof that I attended?
                                    </h3> 
                                    <ul>
                                       
                                       <li type="a"> No. You can attach them to your workshop form if you’d like, but you still should
                                          have the Bridges
                                          workshop form completed and signed by the presenter.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>I forgot to have my Bridges workshop form signed. Can the Bridges department contact
                                       the presenter and ask to confirm my attendance on their sign-in sheet?
                                    </h3> 
                                    <ul>
                                       
                                       <li type="a"> No. It is very important that you remember to have the workshop form with you because
                                          advisors will
                                          not be able to do this time consuming research for you.
                                       </li>
                                       
                                       <li type="a"> Tip: Print 4 copies of the workshop form in the beginning of the semester and keep
                                          them in your main
                                          school folder. That way you should always have it with you no matter what.
                                       </li>
                                       
                                       <li type="a"> Tip: Do not try and “re-create” the form on paper while you are sitting in the presentation.
                                          Also, do not
                                          have the presenter sign a piece of paper as an attempt to show attendance. This will
                                          not be accepted.
                                          Be prepared, plan ahead and be professional.
                                       </li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>I went to a Skillshop, but it was cancelled. Can I still receive credit?</h3> 
                                    <ul>
                                       
                                       <li type="a"> No. We are unable to give credit for Skillshops that you did not attend.</li>
                                       
                                       <li type="a"> Tip: Plan ahead and do not wait for the last minute to attend your workshops.</li>
                                    </ul>
                                    									
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    										
                                    <h3>Where do I find out when Skillshops/Valencia workshops are offered?</h3>
                                    										
                                    <p>
                                       <ul>
                                          <li type="a">Student Development releases a booklet every semester. They are available in the Student
                                             											Development office, advising offices, West Campus Bridges office, and online.
                                          </li>
                                          											
                                          <li type="a"> All Bridges Presentations are available on the Bridges website under “Calendar”.</li>
                                          											
                                          <li type="a"> UCF Direct Connect workshops are available on the Valencia Website.</li>
                                          
                                          <li type="a"> Tip: Plan ahead because some dates are subject to change, so make sure you have back-up
                                             Skillshops
                                             already planned in the event a Skillshop is cancelled. Also, arrive to the Skillshop
                                             early in the event the
                                             	time might have changed. Coming early gives you enough time to adapt to the situation
                                          </li>
                                       </ul>
                                    </p>
                                    									
                                 </div>
                              </div>
                           </div>
                           					
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/community-service-frequently-asked-questions.pcf">©</a>
      </div>
   </body>
</html>