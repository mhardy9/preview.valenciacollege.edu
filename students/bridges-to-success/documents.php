<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Documents  | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/documents.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Documents </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Documents</h2>
                        
                        <div>
                           
                           <h3>Meeting Notes</h3>
                           
                        </div>
                        
                        <div>
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>September 2016</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="/students/bridges-to-success/documents/osceola-meeting-septemeber-2016.pdf">Osceola Campus</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/west-meeting-september-2016.pdf">West Campus</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/east-meeting-september-2016.pdf">East Campus</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>December 2016</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="/students/bridges-to-success/documents/osceola-meeting-december-2016.pdf">Osceola Campus</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/east-meeting-december-2016.pdf">East Campus</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>January 2017</h4>
                                 
                                 <ul>
                                    
                                    <li><a href="/students/bridges-to-success/documents/east-meeting-january-2017.pdf">East Campus</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/west-meeting-january-2017-1617-cohort.pdf">West Campus (16-17 Cohort)</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/west-meeting-january-2017-1516-cohort.pdf">West Campus (15-16 Cohort)</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div>
                           
                           <h3>Community College Conference on Learning Assessment</h3>
                           
                        </div>
                        
                        <div>
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul>
                                    
                                    <li><a href="/students/bridges-to-success/documents/bridges-to-success-student-handbook.pdf">BTS Student Handbook</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/bridges-data-spring-2016.pdf">2016 Spring IR Data</a></li>
                                    
                                    <li><a href="/academics/academic-affairs/institutional-effectiveness-planning/institutional-research/reporting/strategic-indicators/bridges-student-characteristics.php">2016 Fall IR Data</a></li>
                                    
                                    <li><a href="/students/bridges-to-success/documents/bridges-dbot-report-2016.pdf">2016 BTS DBOT Report</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/documents.pcf">©</a>
      </div>
   </body>
</html>