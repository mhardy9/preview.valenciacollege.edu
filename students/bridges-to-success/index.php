<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Bridges to Success | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Bridges To Success</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="main-title">
                           <td class="main-title">
                              	<img src="/students/bridges-to-success/images/bridges-to-success-logo.png" alt="Bridges to Success Logo"><br><br>
                              	
                              <h2>Bridges to Success</h2>
                              
                              <p><span>The Bridges to Success Department provides a wide range of support services to students
                                    who need assistance in getting through college.</span></p>
                              
                           </td>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-8 col-sm-8">
                              	
                              <h3>About Us</h3>
                              	
                              <p>The Office of Bridges to Success (BTS) is responsible for working with diverse populations
                                 to assist with the post-secondary transition from application to enrollment and graduation;
                                 assist the college in creating an educational environment that supports the success
                                 of these students.
                              </p>
                              	
                              <p>In addition, BTS is committed to helping all students become successful in achieving
                                 their educational and career goals. We know that the<em>Road to Success</em> can be challenging as students balance their academic course work, responsibilities
                                 and personal life.
                              </p>
                              	
                              <p>An important element in a student’s success at Valencia is the connection with people
                                 and services that can assist them when they need it. BTS works within the college
                                 to provide services and programs of interest and benefit to our diverse student population.
                              </p>
                              	
                              <p>BTS is committed to recruiting students who may present with factors which could prove
                                 challenging for his/her transition to and completion of college. Risk factors considered
                                 include: First generation college student, Qualify for free or reduced lunch, English
                                 as a second language, Single parent home, Foster child, Unemployment of parent/guardian,
                                 Known disability, Undocumented Florida residency status (non-FL resident), etc. Other
                                 factors that are considered include participation in pre-collegiate programs such
                                 as AVID, Elevate, and McKnight Achievers; community service and extra-curricular activities;
                                 as well as personal characteristics such as perseverance, communication skills, self-motivation
                                 and commitment to educational success.
                              </p>
                              	
                              <p>BTS, as a part of its action plan for minority recruitment and retention maintains
                                 vital connections with colleges and universities throughout the country, and with
                                 churches and other civic and ethnic groups in the community that support students
                                 in their educational endeavors by offering a needs-based scholarship to those who
                                 would benefit from participation in the Bridges to Success program.
                              </p><br>
                              
                              <br>
                              
                              <h3>Bridges to Success Staff</h3>
                              
                              <ul class="list_staff">
                                 <li>
                                    <figure><img src="/students/bridges-to-success/images/staff-photo-dr-tanisha-d-carter.jpg" alt="Dr. Tanisha D. Carter" class="img-circle"></figure>
                                    <h4>Dr. Tanisha D. Carter</h4>
                                    <p>Director</p>
                                 </li>
                                 <li>
                                    <figure><img src="/students/bridges-to-success/images/staff-photo-joy-benjamin-fieulleteau.jpg" alt="Joy Benjamin-Fieulleteau" class="img-circle"></figure>
                                    <h4>Joy Benjamin-Fieulleteau</h4>
                                    <p>Assistant Director</p>
                                 </li>
                              </ul>
                              	
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <div class="box_style_4">
                                 
                                 <h4>BTS Scholarship</h4>
                                 
                                 <p>To be considered for the Bridges scholarship you must meet the following criteria:</p>
                                 
                                 <ul class="list_order">
                                    <li><span>1</span>Student should apply to Valencia 3 to 4 weeks prior to BTS application
                                    </li>
                                    <li><span>2</span>Student must be a resident of Orange or Osceola County
                                    </li>
                                    <li><span>3</span>Student must be a graduating senior (must graduate with a standard high school
                                       diploma, or GED, and be eligible to receive financial aid)
                                    </li>
                                    <li><span>4</span>Student must complete the Free Application for Federal Student Aid (FAFSA)
                                    </li>
                                    <li><span>5</span>Student must take the Postsecondary Education Readiness Test (PERT) or have ACT/SAT
                                    </li>
                                    <li><span>6</span>Scores that indicate college readiness
                                    </li>
                                    <li><span>7</span>Submit all documents to the corresponding Valencia departments as instructed
                                       on the application check list (Note: Incomplete packets will not be considered)
                                    </li>
                                 </ul>
                                 
                              </div>
                           </div>
                        </div>
                        
                        <hr class="more_margin">
                        
                        
                        
                        	
                        <section class="grid">
                           <div class="container margin-60">
                              <div class="main-title">
                                 						
                                 <h2>Bridges to Success YouTube Videos</h2>
                                 						
                                 <p></p>
                                 					
                              </div>
                              <div class="row">
                                 						
                                 <ul class="magnific-gallery">
                                    <li>
                                       <figure><img src="/students/bridges-to-success/images/bridges-to-success-gallery1-thumb.png" alt="New Student Orientation video preview"><figcaption>
                                             <div class="caption-content"><a href="https://www.youtube.com/watch?v=7cT3vOt5oHs" class="video_pop" title="New Student Orientation"><i class="far fa-film"></i><p>
                                                      										
                                                      									 New Student Orientation 
                                                   </p></a></div>
                                          </figcaption>
                                       </figure>
                                    </li>
                                    <li>
                                       <figure><img src="/students/bridges-to-success/images/bridges-to-success-gallery3-thumb.png" alt="Summer Graduation Bash 2017"><figcaption>
                                             <div class="caption-content"><a href="https://www.youtube.com/watch?v=gEskq-uiY0M" class="video_pop" title="Summer Graduation Bash 2017 "><i class="far fa-film"></i><p>
                                                      										
                                                      									 Summer Graduation Bash 2017
                                                      
                                                   </p></a></div>
                                          </figcaption>
                                       </figure>
                                    </li>
                                    <li>
                                       <figure><img src="/students/bridges-to-success/images/bridges-to-success-gallery4-thumb.png" alt="Women of Distinction Luncheon 2017"><figcaption>
                                             <div class="caption-content"><a href="https://www.youtube.com/watch?v=PgGHdMpm8ZQ" class="video_pop" title="Women of Distinction Luncheon 2017"><i class="far fa-film"></i><p>
                                                      										
                                                      									 Women of Distinction Luncheon 2017 
                                                   </p></a></div>
                                          </figcaption>
                                       </figure>
                                    </li>
                                 </ul>
                                 					
                              </div>
                           </div>
                        </section>
                        		
                        <hr>
                        
                        	  
                        	
                        		
                        		  
                        	
                        <h3>Campus Locations &amp; Hours</h3>
                        
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>East Campus</h4>
                                 
                                 <p><strong>Building:</strong> 5-210<br>
                                    <strong>Hours:</strong> Monday - Friday: 8:00 AM - 5:00 PM
                                 </p>
                                 			  
                                 			  
                                 <h4>West Campus</h4>
                                 
                                 <p><strong>Building:</strong> SSB-173<br>
                                    <strong>Hours:</strong> Monday - Friday: 8:00 AM - 6:00 PM
                                 </p>
                                 			  
                                 			  
                                 			  
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Osceola Campus</h4>
                                 
                                 <p>
                                    <strong>Building:</strong> 2-103C<br>
                                    
                                    <strong>Hours:</strong> Monday,Thursday, Friday: 8:00 AM - 5:30 PM<br>
                                    Tuesday, Wednesday: 8:00 AM - 6:00 PM
                                 </p>
                                 				
                                 <h4>Contact Us at:</h4>
                                 				
                                 <p><i class="far fa-phone"></i> <a href="tel:407-582-1395">407-582-1395</a></p>
                                 
                              </div>
                              	
                           </div>
                           		
                        </div>
                        	
                        	
                        <hr>
                        
                        <h3>Resources</h3>
                        
                        <p><a href="https://www.facebook.com/ValenciaBridgesToSuccess/" target="_blank">Facebook</a><br>
                           		<a href="https://twitter.com/@_BTSUpdates" target="_blank">Twitter</a><br>
                           		<a href="https://www.instagram.com/btsupdates/" target="_blank">Instagram</a><br>
                           	<a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A%2F%2Fgcp.valenciacollege.edu%2FCPIP%2F%3Fsys%3DBridgesToSuccess" target="_blank">2018 Summer Program Application</a><br>
                           	<a href="/students/bridges-to-success/documents/bridges-to-success-faq.pdf" target="_blank">Bridges to Success Application FAQ</a><br>
                           	<a href="/students/bridges-to-success/documents/bridges-handbook-2017-18.pdf" target="_blank">Student Handbook</a></p>
                        	
                        	
                     </div>
                     	  
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/index.pcf">©</a>
      </div>
   </body>
</html>