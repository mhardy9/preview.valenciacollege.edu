<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola County Community Service Locations | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/school-district-of-osceola-county-community-service-locations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Osceola County Community Service Locations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        	
                        <h2>Osceola County Community Service Locations</h2>
                        
                        <p>Below is a list of volunteer locations, names of the representative for each location,
                           and the contact
                           information for that representative.
                        </p>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boggy Creek Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 810 Florida Pkwy, Kissimmee, FL 34743<br>
                                 
                                 <strong>Telephone:</strong> 407-344-5060<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Canoe Creek Charter School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 3600 Canoe Creek Road, Saint Cloud, FL 34772<br>
                                 
                                 <strong>Telephone:</strong> 407-891-7320<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Celebration High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1809 Celebration Blvd., Celebration, FL 34747<br>
                                 
                                 <strong>Telephone:</strong> 321-939-6600<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Central Avenue Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 500 W. Columbia Avenue, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-343-7330<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Chestnut Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 4300 Chestnut Street, Kissimmee, FL 34759<br>
                                 
                                 <strong>Telephone:</strong> 407-807-4862<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Cypress Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2251 Lakeside Drive, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-344-5000<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Deerwood Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 3701 Marigold Avenue, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-870-2400<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Denn John Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2001 Denn John Lane, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3560<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Discovery Intermediate School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 5350 San Miguel Road, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-343-7300<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>East Lake Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 4001 Boggy Creek Road, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-943-8450<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Flora Ridge Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2900 Dyer Blvd., Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-933-3999<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Gateway High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 93 Panther Paws Trail, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3600<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Harmony High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 3601 Arthur J Gallagher Blvd., Harmony, FL 34771<br>
                                 
                                 <strong>Telephone:</strong> 407-933-9900<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Hickory Tree Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2355 Old Hickory Tree Rd, St. Cloud, FL 34772<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3120<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Highlands Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 800 W. Donegan Avenue, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3620<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Horizon Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2020 Ham Brown Road, Kissimmee, FL 34746<br>
                                 
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Kissimmee Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 3700 West Donegan, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3640<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Kissimmee Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2410 Dyer Blvd., Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-870-0857<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Kora Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 5000 Koa Street, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-518-1161<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Lakeview Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2900 5th Street, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3220<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Liberty High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 4250 Pleasant Hill Road, Kissimmee, FL 34746<br>
                                 
                                 <strong>Telephone:</strong> 407-933-3910<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Michigan Avenue Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2015 S. Michigan Avenue, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3140<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Mill Creek Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1700 Mill Slough Road, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3660<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Narcoossee Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2690 N. Narcoossee Road, St. Cloud, FL 34771<br>
                                 
                                 <strong>Telephone:</strong> 407-892-6858<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Narcoossee Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2700 N. Narcoossee Road, St. Cloud, FL 34771<br>
                                 
                                 <strong>Telephone:</strong> 407-891-6600<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Neptune Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1200 Betsy Ross Lane, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-892-8387<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Neptune Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2727 Neptune Road, Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3500<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 420 S. Thacker Avenue, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-518-5400<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>PATHS@TECO</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 501 Simpson Road, Kissimmee, FL 34774<br>
                                 
                                 <strong>Telephone:</strong> 407-518-5407<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Parkway Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 857 Florida Parkway, Kissimmee, FL 34743<br>
                                 
                                 <strong>Telephone:</strong> 407-344-7000<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Partin Settlement Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2434 Remington Blvd., Kissimmee, FL 34744<br>
                                 
                                 <strong>Telephone:</strong> 407-518-2000<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Pleasant Hill Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1253 Plesant Hill Road, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3700<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Poinciana Academy of Fine Arts</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 4201 Rhododendron Avenue, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-343-4500<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Poinciana High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2300 S. Poinciana Boulevard, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-870-4860<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Reedy Creek Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 5100 Eagles Trail, Kissimmee, FL 34758<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3580<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>St. Cloud Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2701 Budinger Avenue, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3160<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>St. Cloud High School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 2000 Bulldog Lane, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3100<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>St. Cloud Middle School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1975 S. Michigan Avenue, St. Cloud, FL 34769<br>
                                 
                                 <strong>Telephone:</strong> 407-891-3200<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Sunrise Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 1925 Ham Brown Road, Kissimmee, FL 34746<br>
                                 
                                 <strong>Telephone:</strong> 407-870-4866<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Thacker Ave. Elem. Inter. Studies</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 301 Thacker Avenue, Kissimmee, FL 34741<br>
                                 
                                 <strong>Telephone:</strong> 407-935-3540<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Ventrua Elem. School</h4>
                              
                              <p>
                                 
                                 <strong>Address:</strong> 275 Waters Edge Drive, Kissimmee, FL 34743 <br>
                                 
                                 <strong>Telephone:</strong> 407-344-5040<br>
                                 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     		
                  </div>
                  		
               </div>
               
               	
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/school-district-of-osceola-county-community-service-locations.pcf">©</a>
      </div>
   </body>
</html>