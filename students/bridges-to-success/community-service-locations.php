<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Community Service Locations  | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/community-service-locations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Community Service Locations </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Community Service Locations</h2>
                        
                        <p>Below is a list of volunteer locations, names of the representative for each location,
                           and the contact information for that representative.
                        </p>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>A Gift for Teaching</h4>
                              
                              <p><strong>Representative:</strong> Morgan<br> <strong>Phone:</strong> 407-318-3123 ext. 3126<br> <strong>Fax:</strong> 407-318-3126<br> <strong>Email:</strong> cathyb@agiftfortearching.org<br> <strong>Address:</strong> 6501 Magic Way Building 400c, Orlando, Florida 32809
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>A Gift for Teaching of Osceola County</h4>
                              
                              <p><strong>Representative:</strong> Heather Decarlo<br> <strong>Phone:</strong> 407-348-2370<br> <strong>Fax:</strong><br> <strong>Email:</strong> ejniouik@osceola.k12.fl.us<br> <strong>Address:</strong> 2310 New Beginnings Rd. Ste. 120, Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>AVID</h4>
                              
                              <p><strong>Representative:</strong> Amanda Lopez<br> <strong>Phone:</strong> 407-870-4600 extension: 65268<br> <strong>Fax:</strong> 407-870-5815<br> <strong>Email:</strong> lopeaman@osceola.k12.fl.us<br> <strong>Address:</strong> 817 Bill Beck Blvd, ,Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Advantage Behavioral and Educational Support Services</h4>
                              
                              <p><strong>Representative:</strong> Mariana Martinez<br> <strong>Phone:</strong> 407-891-8717<br> <strong>Fax:</strong> 407-891-8717<br> <strong>Email:</strong> advantagebess@yahoo.com<br> <strong>Address:</strong> 103 E. 13TH St., Saint Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Africana Student Union - Osceola Campus</h4>
                              
                              <p><strong>Representative:</strong> Wilnise Charles<br> <strong>Phone:</strong> 321-240-5302<br> <strong>Fax:</strong><br> <strong>Email:</strong> wcharles13@mail.valenciacollege.edu<br> <strong>Address:</strong> 1800 Denn John Ln., Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>After-School All-Stars</h4>
                              
                              <p><strong>Representative:</strong> Amalia Herrera<br> <strong>Phone:</strong> 407-246-2043<br> <strong>Fax:</strong> 407-246-2015<br> <strong>Email:</strong> Amalia.Herrera@cityoforlando.net<br> <strong>Address:</strong> 400 S. Orange Avenue, Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Agape Word Church</h4>
                              
                              <p><strong>Representative:</strong> Rosa Long<br> <strong>Phone:</strong> 407-814-0250<br> <strong>Fax:</strong> 407-814-9434<br> <strong>Email:</strong> rosalong@agapewordchurch.org<br> <strong>Address:</strong> 6837 Lakeville Rd., Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>American Nation Builders College Preparatory Academy</h4>
                              
                              <p><strong>Representative:</strong> Dr. Juan P. Chisholm<br> <strong>Phone:</strong> (407)-591-1612<br> <strong>Fax:</strong> 1-888-545-5919<br> <strong>Email:</strong> Director@AmericanNationBuilders.org<br> <strong>Address:</strong> 989 West Kennedy Blvd., Orlando, Florida 32810
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Arnold Palmer Medical Center</h4>
                              
                              <p><strong>Representative:</strong> Alexis Bentley<br> <strong>Phone:</strong> 321-841-1809<br> <strong>Fax:</strong> 321-841-8090<br> <strong>Email:</strong> alexis.bently@orlandohealth.com<br> <strong>Address:</strong> 92 West Miller St., Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Avante At St. Cloud</h4>
                              
                              <p><strong>Representative:</strong> Shannon Beapot<br> <strong>Phone:</strong> 407-892-5121<br> <strong>Fax:</strong> 407-892-3322<br> <strong>Email:</strong> kstouc@avantegruop.com<br> <strong>Address:</strong> 1301 Kansas Ave., St. Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Azalea Manor Assisted Living Facility</h4>
                              
                              <p><strong>Representative:</strong> Proserfino Patacsil<br> <strong>Phone:</strong> 407-282-0556<br> <strong>Fax:</strong> 407-282-2231<br> <strong>Email:</strong> pros.patacsil@yahoo.com<br> <strong>Address:</strong> 150 Willow Drive, Orlando, Florida 32807
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Barnett Park (orange county parks &amp; rec)</h4>
                              
                              <p><strong>Representative:</strong> Lamont D. Bryant-407-836-6248<br> <strong>Phone:</strong> 407-836-6251<br> <strong>Fax:</strong> (407)836-6250<br> <strong>Email:</strong> lora.turner@ocfl.net<br> <strong>Address:</strong> 4801 W. Colonial Dr., Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Bear Creek Recreation</h4>
                              
                              <p><strong>Representative:</strong> Andrew Couture<br> <strong>Phone:</strong> 407-254-9040<br> <strong>Fax:</strong><br> <strong>Email:</strong> jennifer.smith@ocfl.net<br> <strong>Address:</strong> 1600 Bear Crossing Dr., Orlando, Florida 32824
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Beta Center</h4>
                              
                              <p><strong>Representative:</strong> Xiozo Mara<br> <strong>Phone:</strong> 407-277-1942 ext. 135<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 4680 Lake Underhill Road, Orlando, Florida 32807
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Bishop Grady Villas</h4>
                              
                              <p><strong>Representative:</strong> Bishop Grady Villas<br> <strong>Phone:</strong> 321-443-9311<br> <strong>Fax:</strong> 407-892-3081<br> <strong>Email:</strong> dshamacher@bishopgradyvillas.org<br> <strong>Address:</strong> 401 Bishop Grady Ct, St. Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Club (Joe R. Lee)</h4>
                              
                              <p><strong>Representative:</strong> Austin Long<br> <strong>Phone:</strong> 407-599-3771<br> <strong>Fax:</strong><br> <strong>Email:</strong> Along@BGCCF.org<br> <strong>Address:</strong> 400 Ruffle Street, Eatonville, Florida 32751
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Club of Central Florida</h4>
                              
                              <p><strong>Representative:</strong> Latoya Anderson<br> <strong>Phone:</strong> 321-682-5910<br> <strong>Fax:</strong><br> <strong>Email:</strong> landerson@bgccf.org<br> <strong>Address:</strong> 501 Florida Parkway, Kissimmee, Florida 34743<br> <strong>Requirements:</strong> Level 2 FBI Background Check: $47 fee
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Club of Central Florida - Carter St.</h4>
                              
                              <p><strong>Representative:</strong> Marquette Carmichael<br> <strong>Phone:</strong> 407-761-3929<br> <strong>Fax:</strong><br> <strong>Email:</strong> Mcarmichael@bgccf.org<br> <strong>Address:</strong> 1002 W. Carter Street, Orlando, Florida 32805<br> <strong>Requirements:</strong></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Club of Piedmont Lakes Middle School</h4>
                              
                              <p><strong>Representative:</strong> Sherie Parker Jackson<br> <strong>Phone:</strong> 407-884-2265 ext:2264<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 2601 Lakeville Rd., Apopka, florida 32703
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Club of Universal Orlando Foundation</h4>
                              
                              <p><strong>Representative:</strong> Mrs.McKinney<br> <strong>Phone:</strong> 407-298-0680<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 5055 Raleigh St., Orlando, Florida 32811
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Clubs Walt Disney World</h4>
                              
                              <p><strong>Representative:</strong> Jose Bastias<br> <strong>Phone:</strong> 407-295-1100<br> <strong>Fax:</strong><br> <strong>Email:</strong> Jbastias@bgccf.org<br> <strong>Address:</strong> 5211 Hernandes Drive, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Boys &amp; Girls Clubs of CF East Altamonte Branch</h4>
                              
                              <p><strong>Representative:</strong> Kimberly Caldwell<br> <strong>Phone:</strong> 407-332-8668<br> <strong>Fax:</strong> 407-8668<br> <strong>Email:</strong> kcaldwell@bgccf.org<br> <strong>Address:</strong> 325 Station Street, East Altamonte, Florida 32701
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Breakthrough Theatre of Winter Park</h4>
                              
                              <p><strong>Representative:</strong> Wade Hair<br> <strong>Phone:</strong> 407-920-4034<br> <strong>Fax:</strong><br> <strong>Email:</strong> wadehair@breakthroughtheatre.com<br> <strong>Address:</strong> 419a w. Fairbanks ave, Winter Park, Florida 32789
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Catholic Charities of Central Florida</h4>
                              
                              <p><strong>Representative:</strong> Flavia Cavalconte<br> <strong>Phone:</strong> 407-658-1818<br> <strong>Fax:</strong> 407-282-2891<br> <strong>Email:</strong> flavia.cavalconte@cflcc.org<br> <strong>Address:</strong> 1819 N. Semoran Blvd., Orlando, Florida 32807
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Center for Drug Free Living</h4>
                              
                              <p><strong>Representative:</strong> Alicia Williams<br> <strong>Phone:</strong> 407-697-2948<br> <strong>Fax:</strong> 407-846-5484<br> <strong>Email:</strong> ekurpetski@ofdpfl.com<br> <strong>Address:</strong> 2540 Michigan Ave., Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Central Florida Urban League</h4>
                              
                              <p><strong>Representative:</strong> Sharita Richardson<br> <strong>Phone:</strong> 407-841-7654<br> <strong>Fax:</strong><br> <strong>Email:</strong> srichardson@cful.org<br> <strong>Address:</strong> 2804 Belco Drive, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Charity &amp; Love</h4>
                              
                              <p><strong>Representative:</strong> Barbara Ansar<br> <strong>Phone:</strong> 407-522-4473<br> <strong>Fax:</strong> 407-704-8903<br> <strong>Email:</strong> peacelovecharity@gmail.com<br> <strong>Address:</strong> 5372 Silver Star Road, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Christ Open Door Community Church</h4>
                              
                              <p><strong>Representative:</strong> Bishop F.L. Patton<br> <strong>Phone:</strong> 407-421-6239<br> <strong>Fax:</strong> 407-849-0093<br> <strong>Email:</strong> n/a<br> <strong>Address:</strong> 108 N. Parramore Ave., Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>City Of Orlando- Engelwood</h4>
                              
                              <p><strong>Representative:</strong> Taryn Ruthkovitz<br> <strong>Phone:</strong> 407-246-4453<br> <strong>Fax:</strong><br> <strong>Email:</strong> alexandra.temes@cityoforlando.net<br> <strong>Address:</strong> 6123 La Costa Dr., Orlando, Florida 32807
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>City of St. Cloud (Parks &amp; Rec)</h4>
                              
                              <p><strong>Representative:</strong> Demetrius Chappell<br> <strong>Phone:</strong> 407-892-2533<br> <strong>Fax:</strong> 407-957-8962<br> <strong>Email:</strong> dchappell@stcloud.org<br> <strong>Address:</strong> 3001 17th Street, St. Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>College Bound Project</h4>
                              
                              <p><strong>Representative:</strong> Jacqueline R. Stunter<br> <strong>Phone:</strong> (407)616-8915<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 416 Tarpon Street, Kissimmee, Florida 34774
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Colonial Lakes Health Care</h4>
                              
                              <p><strong>Representative:</strong> Ivey Davis<br> <strong>Phone:</strong> 407-877-6143<br> <strong>Fax:</strong> 407-877-6143<br> <strong>Email:</strong> n/a<br> <strong>Address:</strong> 15204 W. Colonial Dr., Winter Garden, Florida 34787
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Cornerstone Hospice</h4>
                              
                              <p><strong>Representative:</strong> Esperanza Saud<br> <strong>Phone:</strong> 407-846-1231<br> <strong>Fax:</strong> 407-846-2045<br> <strong>Email:</strong> esaud@cshospice.org<br> <strong>-Address:</strong> 3283 South John Young Parkway- Suite J, Kissimmee, Florida 34746
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Council of Aging</h4>
                              
                              <p><strong>Representative:</strong> Connie Bellanceau<br> <strong>Phone:</strong> 407-933-9525<br> <strong>Fax:</strong><br> <strong>Email:</strong> bellancc@osceola_coa.com<br> <strong>Address:</strong> 700 Generation Pointe, Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Department of Veteran Affairs</h4>
                              
                              <p><strong>Representative:</strong> Renee Frosbotter<br> <strong>Phone:</strong> 407-626-1599<br> <strong>Fax:</strong> 407-599-1352<br> <strong>Email:</strong> loraine.frosbotter@va.gov<br> <strong>Address:</strong> 5201 Raymond St., Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Devereux Family Support &amp; Visitation Center</h4>
                              
                              <p><strong>Representative:</strong> Silvanita Trevino<br> <strong>Phone:</strong> 407-999-5577<br> <strong>Fax:</strong> 407-599-5589<br> <strong>Email:</strong> strevino@devereux.org<br> <strong>Address:</strong> 118 Pasadena Pl., Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Dover Shores Community Center</h4>
                              
                              <p><strong>Representative:</strong> Dustin Wheeler<br> <strong>Phone:</strong> 407-246-4451<br> <strong>Fax:</strong><br> <strong>Email:</strong> dustin.wheeler@cityoforlando.net<br> <strong>Address:</strong> 1400 Gaston Foster Road, Orlando, Florida 32812
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Dr. P. Phillips Hospital</h4>
                              
                              <p><strong>Representative:</strong> Cheryl Cyr<br> <strong>Phone:</strong> (321)842-8868<br> <strong>Fax:</strong> 321-842-7312<br> <strong>Email:</strong> cheryl.cyr@orlandohealth.com<br> <strong>Address:</strong> 9400 Turkey Lake Rd., Orlando, Florida 32819<br> <strong> LOCATION REQUIREMENTS:</strong> <a href="http://www.orlandohealth.com/volunteer-services/dr-p-phillips-hospital-volunteer-services/dr-p-phillips-volunteer-service-areas">Available Here</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Dr. Phillips Center for the Performing Arts</h4>
                              
                              <p><strong>Representative:</strong> Isaiah Mervin<br> <strong>Phone:</strong> 407-839-0119<br> <strong>Fax:</strong><br> <strong>Email:</strong> Isaiah@drphillipscenter.org<br> <strong>Address:</strong> 155 East Anderson St., Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Dr.Surgery Center</h4>
                              
                              <p><strong>Representative:</strong> Hernan Hernandez<br> <strong>Phone:</strong> 407-933-7800<br> <strong>Fax:</strong> 407-933-0564<br> <strong>Email:</strong> hhernandez32792@gmail.com<br> <strong>Address:</strong> 901 N. Main Street, Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Dru's Place</h4>
                              
                              <p><strong>Representative:</strong> Drucilla Marshall<br> <strong>Phone:</strong> 407-294-1589<br> <strong>Fax:</strong> 407-523-0660<br> <strong>Email:</strong> drusplace@msn.com<br> <strong>Address:</strong> 8118 Bright Meadow Dr., Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>E-We Computer Lab &amp; Learing Center</h4>
                              
                              <p><strong>Representative:</strong> Alice M. Grant<br> <strong>Phone:</strong> 407-740-8923<br> <strong>Fax:</strong> 407-740-8926<br> <strong>Email:</strong> amgrant12@cfl.rr.com<br> <strong>Address:</strong> 323 Kennedy Blvd. Suite d, Eatonville, Florida 32751
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Early Learning Coalition of Orange County</h4>
                              
                              <p><strong>Representative:</strong> Jonna Gordon<br> <strong>Phone:</strong> 407-259-2464<br> <strong>Fax:</strong> 407-749-0282<br> <strong>Email:</strong> jgordon@elcoc.org<br> <strong>Address:</strong> 1940 Traylor Blvd., Orlando, Florida 32804
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Educational Partners</h4>
                              
                              <p><strong>Representative:</strong> Erika Beard<br> <strong>Phone:</strong> 407-462-5721<br> <strong>Fax:</strong> 407-363-1320<br> <strong>Email:</strong> erika_beard@yahoo.com<br> <strong>Address:</strong> 520 N. Orlando Ave Suite 19, Winter Park, Florida 32789
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Elevate Orlando</h4>
                              
                              <p><strong>Representative:</strong> TATIANA MONTANEZ<br> <strong>Phone:</strong> 386-473-9143<br> <strong>Fax:</strong><br> <strong>Email:</strong> tmontanez@elevateorlando.org<br> <strong>Address: </strong>P.O. BOX 940633, MAITLAND,FL 32794
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Faith Christian Academy</h4>
                              
                              <p><strong>Representative:</strong> Angel Feliciano<br> <strong>Phone:</strong> 407-275-8031<br> <strong>Fax:</strong> 407-281-3710<br> <strong>Email:</strong> felicianoA@fcalions.org<br> <strong>Address:</strong> 9307 Curry Ford Road, Orlando, Florida 32825
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Family and Friends United Inc.</h4>
                              
                              <p><strong>Representative:</strong> Sharon Warner<br> <strong>Phone:</strong> 321-947-5182<br> <strong>Fax:</strong> 321-947-5182<br> <strong>Email:</strong> ful@bellsouth.net<br> <strong>Address:</strong> P.O. Box 680642, Orlando, Florida 32868
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Florida Celebration Hospital</h4>
                              
                              <p><strong>Representative:</strong> Al Salanitro - "100 Hours are required for this location"<br> <strong>Phone:</strong> 407-303-4463<br> <strong>Fax:</strong> 407-303-4097<br> <strong>Email:</strong> al.salanitao@flhosp.org<br> <strong>Address:</strong> 400 Celebration Place, Celebration , Florida 34747<br> <strong> LOCATION REQUIREMENTS: </strong> <a href="https://www.floridahospital.com/giving/volunteer/requirements">Available Here</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Florida Hospital Altamonte</h4>
                              
                              <p><strong>Representative:</strong> Heather Reasoner- "100 Hours are required for this location"<br> <strong>Phone:</strong> 407-303-2663<br> <strong>Fax:</strong> 407-303-2323<br> <strong>Email:</strong> heather.reasoner@flhosp.org<br> <strong>Address:</strong> 601 East Altamonte Dr., Altamonte, Florida 32701<br> <strong> LOCATION REQUIREMENTS:</strong> <a href="https://www.floridahospital.com/giving/volunteer/requirements">Available Here</a></p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Florida Hospital Apopka</h4>
                              
                              <p><strong>Representative:</strong> Ronda Babich<br> <strong>Phone:</strong> 407-889-1057<br> <strong>Fax:</strong> 407-889-1037<br> <strong>Email:</strong> ronda.babich@flhosp.org<br> <strong>Address:</strong> 201 N. Park Avenue, Apopka, Florida 32703<br> <strong> LOCATION REQUIREMENTS:</strong> <a href="https://www.floridahospital.com/giving/volunteer/requirements">Available Here</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Florida Hospital Kissimmee</h4>
                              
                              <p><strong>Representative:</strong> Al Salanitro- "100 Hours are required for this location"<br> <strong>Phone:</strong> 407-932-6165<br> <strong>Fax:</strong> 407-932-6183<br> <strong>Email:</strong> al.salanitro@flhosp.org<br> <strong>Address:</strong> 2450 N. Orange Blossom Trail, Kissimmee, Florida 34744<br> <strong> LOCATION REQUIREMENTS:</strong> <a href="https://www.floridahospital.com/giving/volunteer/requirements">Available Here</a></p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Florida Hospital for Children</h4>
                              
                              <p><strong>Representative:</strong> Meagan Krizek-Fonseca<br> <strong>Phone:</strong> 407-303-8815<br> <strong>Fax:</strong> 407-303-9841<br> <strong>Email:</strong> meagan.krizek@flhosp.org<br> <strong>Address:</strong> 601 East Rollins Street, Orlando, Florida 32803<br> <strong>LOCATION REQUIREMENTS:</strong> <a href="https://www.floridahospital.com/giving/volunteer/requirements">Available Here</a></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Freedomland Christian Academy</h4>
                              
                              <p><strong>Representative:</strong> Yanira C. Diaz<br> <strong>Phone:</strong> 407-935-9088<br> <strong>Fax:</strong> 1-888-759-3713<br> <strong>Email:</strong> director@freedomlandacademy.com<br> <strong>Address:</strong> 1210 North Main St., Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>FrontLine OutReach</h4>
                              
                              <p><strong>Representative:</strong> Ella Lewis<br> <strong>Phone:</strong> 407-293-3000<br> <strong>Fax:</strong><br> <strong>Email:</strong> elewis3255@aol.com<br> <strong>Address:</strong> 3000 C.R. Smith St., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>FrontLine OutReach (Head Start)</h4>
                              
                              <p><strong>Representative:</strong> Teresa Williams<br> <strong>Phone:</strong> 407-293-3000<br> <strong>Fax:</strong><br> <strong>Email:</strong> Teresa.williams@ocfl.net<br> <strong>Address:</strong> 3000 C.R. Smith St., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Give Kids the World</h4>
                              
                              <p><strong>Representative:</strong> Connie Foo<br> <strong>Phone:</strong> 407-396-0770<br> <strong>Fax:</strong> 1-888-577-9406<br> <strong>Email:</strong> jennyl@gktw.org<br> <strong>Address:</strong> 210 South Bass Rd., Kissimmee, Florida 34746
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Grand Ave Primary Learning Center</h4>
                              
                              <p><strong>Representative:</strong> Vilandra Dinkins<br> <strong>Phone:</strong> 407-245-1750 ext:2221<br> <strong>Fax:</strong><br> <strong>Email:</strong> vilandra.dinkins@ocps.net<br> <strong>Address:</strong> 800 W. Grand Street, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>HBI-Project Bridge</h4>
                              
                              <p><strong>Representative:</strong> Crystal Dennis<br> <strong>Phone:</strong> 407-297-1696<br> <strong>Fax:</strong> 407-297-1699<br> <strong>Email:</strong> cdennis@hbi.org<br> <strong>Address:</strong> 2133 All Childrens Way Orlando, Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Habitat for Humanity Restore</h4>
                              
                              <p><strong>Representative:</strong> Kiana Politis<br> <strong>Phone:</strong> 407-426-7192<br> <strong>Fax:</strong><br> <strong>Email:</strong> kpolitis@habitatorlando.org<br> <strong>Address:</strong> 2105 N. Orange Blossom Trail, Orlando, Florida 32703
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Habitat for Humanity of Apopka</h4>
                              
                              <p><strong>Representative:</strong> Marie Kaiser<br> <strong>Phone:</strong> 407-880-8881 or 8852<br> <strong>Fax:</strong> 407-880-8918<br> <strong>Email:</strong> execuhivedirector@habitatapopkafl.org<br> <strong>Address:</strong> 2001 Rock Springs Rd., Apopka, Florida 32712
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Habitat for Humanity of Greater Orlando</h4>
                              
                              <p><strong>Representative:</strong> Dustin Gill<br> <strong>Phone:</strong> 407-574-2655<br> <strong>Fax:</strong> 407-648-4450<br> <strong>Email:</strong> dgill@habitat-orlando.org<br> <strong>Address:</strong> 1925 Traylor Blvd., Orlando, Florida 32804
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Habitat for Humanity of West Orange</h4>
                              
                              <p><strong>Representative:</strong> Joe Williams<br> <strong>Phone:</strong> 407-905-0406<br> <strong>Fax:</strong> M-S 8am-4pm<br> <strong>Email:</strong> Restore@westorangeHabitat.org<br> <strong>Address:</strong> 114 S. Dillard St., Winter Garden, Florida 34787
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Hal P. Marston</h4>
                              
                              <p><strong>Representative:</strong> Dina Mathews/ Pamela Randell<br> <strong>Phone:</strong> 407-836-8450<br> <strong>Fax:</strong><br> <strong>Email:</strong> dina.mathews@ocfl.net<br> <strong>Address:</strong> 3933 W.D. Judge Dr., Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Harbor House</h4>
                              
                              <p><strong>Representative:</strong> Manny Ayala<br> <strong>Phone:</strong> 407-953-2683<br> <strong>Fax:</strong><br> <strong>Email:</strong> mayala@harborhousefl.com<br> <strong>Address:</strong> 924 N. Magnolia Avenue, Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Health Center of Windermere</h4>
                              
                              <p><strong>Representative:</strong> Imbania Rojas<br> <strong>Phone:</strong> 407-420-2090<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 4875 Cason Cave Dr., Orlando, Florida 32811
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Health Central Auxiliary</h4>
                              
                              <p><strong>Representative:</strong> Paulina S. Wolfe<br> <strong>Phone:</strong> 407-296-1148<br> <strong>Fax:</strong> 407-296-1110<br> <strong>Email:</strong> paulina.wolfe@healthcentral.org<br> <strong>Address:</strong> 10000 West Colonial, Ocoee, Florida 34761
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Health Central Hospital / Orlando Health</h4>
                              
                              <p><strong>Representative:</strong> Paulina Wolfe<br> <strong>Phone:</strong> 407-296-1148<br> <strong>Fax:</strong> 407-296-1110<br> <strong>Email:</strong> paulina.wolfe@healthcentral.org<br> <strong>Address:</strong> 10000 West Colonial, Ocoee, Florida 34761
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Heart of Florida United Way</h4>
                              
                              <p><strong>Representative:</strong> Mathew Blood<br> <strong>Phone:</strong> 407-849-2372<br> <strong>Fax:</strong> 407-244-2802<br> <strong>Email:</strong> uwvolunteers.org<br> <strong>Address:</strong> 1940 Traylor Blvd., Orlando, Florida 32804
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Heavenly Hoofs Inc.</h4>
                              
                              <p><strong>Representative:</strong> Amy Lesch<br> <strong>Phone:</strong> 407-933-7433<br> <strong>Fax:</strong><br> <strong>Email:</strong><a href="mailto:amy@mccormick.us"> amy@mccormick.us</a> <br> <strong>Address:</strong>4651 Rummell Road Saint Cloud, FL 34771
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Help Now Of Osceola</h4>
                              
                              <p><strong>Representative:</strong> Shelly Gordon<br> <strong>Phone:</strong> 407-847-0128<br> <strong>Fax:</strong><br> <strong>Email:</strong> evelynhj@helpnowshelter.org<br> <strong>Address:</strong> P.O. Box 420370, Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Heritage Christian School-Osceola County</h4>
                              
                              <p><strong>Representative:</strong> Randy McGaha<br> <strong>Phone:</strong> 407-847-4087<br> <strong>Fax:</strong> 407-932-2806<br> <strong>Email:</strong> r.mcgaha@heritage.eagles.org<br> <strong>Address:</strong> 1500 East Vine St., Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Hope Community Center</h4>
                              
                              <p><strong>Representative:</strong> Lynn Devanie<br> <strong>Phone:</strong> 407-880-4673 ext. 222<br> <strong>Fax:</strong> 407-464-0854<br> <strong>Email:</strong> ldevanie@hcc-offin.org<br> <strong>Address:</strong> 1016 North Park Avenue, Apopka, Florida 32712
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Hubbard House</h4>
                              
                              <p><strong>Representative:</strong> Peggy Smith<br> <strong>Phone:</strong> 407-649-6886<br> <strong>Fax:</strong> 407-849-6447<br> <strong>Email:</strong> peggy.smith@orlandohealth.com<br> <strong>Address:</strong> 29 W. Miller St., Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Hubbs - Seaworld Research Institute</h4>
                              
                              <p><strong>Phone:</strong> 321-327-8970<br> <strong>Fax:</strong> 321-327-8973<br> <strong>Email:</strong> tmazza@hswri.org<br> <strong>Address:</strong> 3830 South Highway A1A #4-181, Melbourne Beach, FL 32951<br> <strong>Representative :</strong> Teresa Jablonski
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Imagine Kissimmee Charter Academy</h4>
                              
                              <p><strong>Representative:</strong> Sandra Dominguez<br> <strong>Phone:</strong> 407-847-1400<br> <strong>Fax:</strong> 407-847-1401<br> <strong>Email:</strong> lori.mccarley@imagineschools.com<br> <strong>Address:</strong> 2850 Bill Beck Blvd., Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>King's Way Learning Center</h4>
                              
                              <p><strong>Representative:</strong> Pastor Eric Richard<br> <strong>Phone:</strong> 407-422-5044<br> <strong>Fax:</strong><br> <strong>Email:</strong> kwlc@att.net<br> <strong>Address:</strong> 1000 22ND St., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>L. Claudia Allen Senior Center</h4>
                              
                              <p><strong>Representative:</strong> Herbert Williams<br> <strong>Phone:</strong> 407-246-4461<br> <strong>Fax:</strong> 407-521-3365<br> <strong>Email:</strong> herbert.williams@cityfororland.net<br> <strong>Address:</strong> 1840 Mable Butler, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Lila Mitchell Head-Start</h4>
                              
                              <p><strong>Representative:</strong> Wilna Francois<br> <strong>Phone:</strong> 407-254-8484<br> <strong>Fax:</strong><br> <strong>Email:</strong> Wilna.Francois@ocfl.net<br> <strong>Address:</strong> 5151 Raleigh Street, Orlando, Florida 32811
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Lucerne Pavillion</h4>
                              
                              <p><strong>Representative:</strong> Jenny Kelly<br> <strong>Phone:</strong> 321-841-1694<br> <strong>Fax:</strong><br> <strong>Email:</strong> carmen.negron@orlandohealth.com<br> <strong>Address:</strong> 818 Main Lane, Orlando, Florida 332801
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Meadow Woods Recreation Center</h4>
                              
                              <p><strong>Representative:</strong> Chantall Cooper<br> <strong>Phone:</strong> 407-256-9065<br> <strong>Fax:</strong><br> <strong>Email:</strong> orlandomagicocflgyms@ocfl.net<br> <strong>Address:</strong> 1751 Rhode Island Woods Cir., Orlando, Florida 32824
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>&nbsp;</h4>
                              
                              <p><strong>&nbsp;</strong></p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Mt. Sinai Tutorial Ministries</h4>
                              
                              <p><strong>Representative:</strong> Ingrid Anderson<br> <strong>Phone:</strong> 407-521-8063<br> <strong>Fax:</strong> 407-521-8063<br> <strong>Email:</strong> mrsineimbe@hotmail.com<br> <strong>Address:</strong> 5200 West South St., Orlando, Florida 32811
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>NEW Image Youth Center</h4>
                              
                              <p><strong>Representative:</strong> Shatovia Dukes<br> <strong>Phone:</strong> 407-496-2307<br> <strong>Fax:</strong> (407)514-9348<br> <strong>Email:</strong> shatoviadukes.niyc@gmail.com<br> <strong>Address:</strong> 214 S.Parramore Avenue, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Nemour's Childrens Hospital</h4>
                              
                              <p><strong>Representative:</strong><br> <strong>Phone:</strong><br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>New Fellowship Church of God</h4>
                              
                              <p><strong>Representative:</strong> Charles Bargaineer<br> <strong>Phone:</strong> 407-303-1221<br> <strong>Fax:</strong><br> <strong>Email:</strong> bargaineerp@aol.com<br> <strong>Address:</strong> 600 W. Canton Ave., Winter Park, Florida 32790
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>New Hope For Kids</h4>
                              
                              <p><strong>Representative:</strong> Gloria Copeze<br> <strong>Phone:</strong> 407-331-3059 ext. 15<br> <strong>Fax:</strong><br> <strong>Email:</strong> cory@newhopeforkids.org<br> <strong>Address:</strong> 900 N. Maitland Ave., Maitland, Florida 32751
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Nspire Training and Development Center</h4>
                              
                              <p><strong>Representative:</strong> Victoria Dunston<br> <strong>Phone:</strong> 407-756-7804<br> <strong>Fax:</strong> 407-210-1656<br> <strong>Email:</strong> nspiredcenter@gmail.com<br> <strong>Address:</strong> 6837 Lakeville Rd., Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>OCPS-Magnolia School</h4>
                              
                              <p><strong>Representative:</strong> Teleshia Jones<br> <strong>Phone:</strong> 407-296-6499<br> <strong>Fax:</strong> (407)521-3301<br> <strong>Email:</strong> teleshia.jones@ocps.net<br> <strong>Address:</strong> 1900 Matterhorne Dr., Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orange County Animal Services</h4>
                              
                              <p><strong>Representative:</strong> Crystal Kincaid<br> <strong>Phone:</strong> 407-254-9155<br> <strong>Fax:</strong><br> <strong>Email:</strong> crystal.kincaid. @ocfl.net<br> <strong>Address:</strong> 2769 Conroy Road, Orlando, Florida 32839
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orange County Fire Rescue Dept</h4>
                              
                              <p><strong>Representative:</strong><br> <strong>Phone:</strong><br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong></p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orange County Health Department</h4>
                              
                              <p><strong>Representative:</strong> Arthur Howell<br> <strong>Phone:</strong> 407-836-2600<br> <strong>Fax:</strong><br> <strong>Email:</strong> arthur_howell@doh.state.fl.us<br> <strong>Address:</strong> 6101 Lake Ellenor Dr., Orlando, Florida 32809
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orange County Library System</h4>
                              
                              <p><strong>Representative:</strong> Karen Wente<br> <strong>Phone:</strong> 407-835-7496<br> <strong>Fax:</strong><br> <strong>Email:</strong> wente.karen@ocls.info<br> <strong>Address:</strong> 101 East Central Blvd., Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orange County Parks and Recreation</h4>
                              
                              <p><strong>Representative:</strong><br> <strong>Phone:</strong> 407-254-9174<br> <strong>Fax:</strong> 407-523-1662<br> <strong>Email:</strong> mark.wright@ocfl.net<br> <strong>Address:</strong> 5100 Turkey Lake Road, Orlando, Florida 32703
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando After-School All Stars</h4>
                              
                              <p><strong>Representative:</strong> Nicholas Olmeda<br> <strong>Phone:</strong> 407-680-5146<br> <strong>Fax:</strong><br> <strong>Email:</strong> Nicholas.Olmeda@ocps.net<br> <strong>Address:</strong> 9290 Lee Vista Blvd., Orlando, Florida 32829
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Museum of Art</h4>
                              
                              <p><strong>Representative:</strong> Phyllis Lankiewicz<br> <strong>Phone:</strong> 407-896-4231 ext: 240<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 2416 N. Mills Ave., Orlando, Florida 32804
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Neighborhood Improvement Corporation</h4>
                              
                              <p><strong>Representative:</strong> Apryl Jennings<br> <strong>Phone:</strong> 407-648-1623<br> <strong>Fax:</strong> 407-648-1717<br> <strong>Email:</strong> Jennings@orlandoneighborhood.org<br> <strong>Address:</strong> 101 S. Terry Ave, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Police Explorers</h4>
                              
                              <p><strong>Representative:</strong> Scott Daniels<br> <strong>Phone:</strong> 407-758-4632<br> <strong>Fax:</strong><br> <strong>Email:</strong> scott.daniels@cityoforlando.net<br> <strong>Address:</strong> 100 South Hughey Avenue, Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Regional Medical Center</h4>
                              
                              <p><strong>Representative:</strong> Hilary McFadden<br> <strong>Phone:</strong> 321-841-5379<br> <strong>Fax:</strong><br> <strong>Email:</strong> hilarymcfadden@orlandohealth.com<br> <strong>Address:</strong> 1414 Kuhl Ave-MP 83, Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Repertory Theatre</h4>
                              
                              <p><strong>Representative:</strong> Laura Dubose<br> <strong>Phone:</strong> 407-896-7365<br> <strong>Fax:</strong> 407-897-3284<br> <strong>Email:</strong> ldubose@orlandorep.com<br> <strong>Address:</strong> 1001 East Princeton St., Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando Science Center</h4>
                              
                              <p><strong>Representative:</strong> Zach Lynn<br> <strong>Phone:</strong> 407-514-2223<br> <strong>Fax:</strong> 407-514-2277<br> <strong>Email:</strong> zlenn@osc.org<br> <strong>Address:</strong> 777 East Princeton St., Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlando VA Medical Center</h4>
                              
                              <p><strong>Representative:</strong> Sandra Placencia<br> <strong>Phone:</strong> 407-629-1599<br> <strong>Fax:</strong><br> <strong>Email:</strong> lorraine.frosbutter@va.gov<br> <strong>Address:</strong> 5201 Raymond St., Orlando, Florida 32803
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Orlo Vista Park</h4>
                              
                              <p><strong>Representative:</strong> Diana Roberts<br> <strong>Phone:</strong> 407-254-9053<br> <strong>Fax:</strong><br> <strong>Email:</strong> diana.roberts@ocfl.net<br> <strong>Address:</strong> 1North Powers Dr., Orlando, Florida 32835
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola County Animal Services</h4>
                              
                              <p><strong>Representative:</strong> Tami Solano<br> <strong>Phone:</strong> 407-742-8003<br> <strong>Fax:</strong> 407-891-1290<br> <strong>Email:</strong> tami.solano@osceola.org<br> <strong>Address:</strong> 3910 Old Canoe Creek Road, St.Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola County Guardian Ad Litem Program</h4>
                              
                              <p><strong>Representative:</strong> Liner Snider<br> <strong>Phone:</strong> 407-742-6656<br> <strong>Fax:</strong> 407-742-6670<br> <strong>Email:</strong> linder.snider@gal.fl.gov<br> <strong>Address:</strong> 3 Courthouse Square, Suite 100, Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola County Historical Society</h4>
                              
                              <p><strong>Representative:</strong> Kathy A. Graham<br> <strong>Phone:</strong> 407-396-8644<br> <strong>Fax:</strong> (866) 929-5070<br> <strong>Email:</strong> phillip@osceolahistory.org<br> <strong>Address:</strong> 750 N. Bass Rd., Kissimmee, Florida 34746
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola County Sheriffs Office</h4>
                              
                              <p><strong>Representative:</strong> Gina El-Bary<br> <strong>Phone:</strong> 407-729-1529<br> <strong>Fax:</strong><br> <strong>Email:</strong> Gina.Elbary@osceola.org<br> <strong>Address:</strong> 2600 E 192, Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola County Teen Court</h4>
                              
                              <p><strong>Representative:</strong> Angie Martinez<br> <strong>Phone:</strong> 407-742-2465<br> <strong>Fax:</strong><br> <strong>Email:</strong> ctadam1@ocnjcc.org<br> <strong>Address:</strong> 2 Courthouse Sq. STE. 3100, Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola Regional Hospital</h4>
                              
                              <p><strong>Representative:</strong> Elizabeth Galarza<br> <strong>Phone:</strong> 407-518-3907<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 700 West Oak Street., Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Osceola Regional Medical Center</h4>
                              
                              <p><strong>Representative:</strong> Marianne Davitt<br> <strong>Phone:</strong> 407-931-2222<br> <strong>Fax:</strong><br> <strong>Email:</strong> marianne.davitt@healthcare.com<br> <strong>Address:</strong> 700 West Oaks St., Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Parramore Kids Zone</h4>
                              
                              <p><strong>Representative:</strong> Colen Providence<br> <strong>Phone:</strong> 407-254-4759<br> <strong>Fax:</strong><br> <strong>Email:</strong> glenn.providence@cityoforlando.net<br> <strong>Address:</strong> 363 N. Parramore Ave, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Pawsitive Action Foundation</h4>
                              
                              <p><strong>Representative:</strong> Norma Jean Ross<br> <strong>Phone:</strong> 407-506- 3818<br> <strong>Fax:</strong><br> <strong>Email:</strong> pawsitiveaction@gmail.com<br> <strong>Address:</strong> 5701 Leon Tyson Road, St. Cloud, Florida 34771
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Pet Alliance of Greater Orlando</h4>
                              
                              <p><strong>Representative:</strong> Kathy Burns<br> <strong>Phone:</strong> 407-248-1748<br> <strong>Fax:</strong><br> <strong>Email:</strong> volunteer@petallianceorlando.org<br> <strong>Address:</strong> 2727 Conroy Rd., Orlando, Florida 32839
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Profound Medical Equipment</h4>
                              
                              <p><strong>Representative:</strong> Rafael Romero<br> <strong>Phone:</strong> 877-710-8181<br> <strong>Fax:</strong> 877-720-7131<br> <strong>Email:</strong> PME782@gmail.com<br> <strong>Address:</strong> 7111 Hiawassee Overlook Drive., Orlando, Florida 32835
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Rebecca's Garden of Hope</h4>
                              
                              <p><strong>Representative:</strong> Sanya Parson<br> <strong>Phone:</strong> 407-273-0101<br> <strong>Fax:</strong><br> <strong>Email:</strong> Sanya@rebeccasgardenofhope.org<br> <strong>Address:</strong> 1750 Bruton Blvd., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Rock Lake Community Center</h4>
                              
                              <p><strong>Representative:</strong> Marquis Jolly<br> <strong>Phone:</strong> 407-246-4474<br> <strong>Fax:</strong><br> <strong>Email:</strong> freddie.andrews@cityoforlando.net<br> <strong>Address:</strong> 440 N. Tampa Ave., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Rosemont Community Center</h4>
                              
                              <p><strong>Representative:</strong> Brenda Scott<br> <strong>Phone:</strong> 407-246-4475<br> <strong>Fax:</strong><br> <strong>Email:</strong> michael.paverlko@cityoforlando.net<br> <strong>Address:</strong> 4872 Rosebay Dr., Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Second Harvest Food Bank</h4>
                              
                              <p><strong>Representative:</strong> Mindy Ortiz<br> <strong>Phone:</strong> 407-295-1066<br> <strong>Fax:</strong><br> <strong>Email:</strong> mortiz@foodbankcentralflorida.org<br> <strong>Address:</strong> 411 Mercy Drive, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>St. Cloud Hospital Auxilary</h4>
                              
                              <p><strong>Representative:</strong> Ronald R. Marble<br> <strong>Phone:</strong> 407-892-4798<br> <strong>Fax:</strong><br> <strong>Email:</strong> marblestcloud@aol.com<br> <strong>Address:</strong> 921 Louisiana Avenue, St.Cloud, Florida 34769
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Straight Street Orlando</h4>
                              
                              <p><strong>Representative:</strong> Tonya Grese<br> <strong>Phone:</strong> 321-439-4040<br> <strong>Fax:</strong><br> <strong>Email:</strong> tonya@straightstorlando.com<br> <strong>Address:</strong> 12472 Lake Underhill Road, Orlando, Florida 32828
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Strengthening Our Sons Inc.</h4>
                              
                              <p><strong>Representative:</strong> Tammie Hott<br> <strong>Phone:</strong> 407-233-4450<br> <strong>Fax:</strong> 866-217-0157<br> <strong>Email:</strong> Tammie.Hott@strengtheningoursons.org<br> <strong>Address:</strong> 750 South Orange Blossom Trail #162, Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>TDR Learning Academy</h4>
                              
                              <p><strong>Representative:</strong> Jose Correa<br> <strong>Phone:</strong> 407-694- -8595<br> <strong>Fax:</strong> 407-826-1957<br> <strong>Email:</strong> tdr.learningacademy@gmail.com<br> <strong>Address:</strong> 3057 Curry Ford Rd., Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Temple of Brothers &amp; Sisters of Godwill Inc.</h4>
                              
                              <p><strong>Representative:</strong> Angela Scott<br> <strong>Phone:</strong> 407-286-2900<br> <strong>Fax:</strong> 407-286-2942<br> <strong>Email:</strong> 4Judah@gmail.com<br> <strong>Address:</strong> 4013 Clarcona Ocoee Road, Orlando, Florida 32870
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Terra Vista Rehab</h4>
                              
                              <p><strong>Representative:</strong> Nene Player<br> <strong>Phone:</strong> 407-423-1940<br> <strong>Fax:</strong><br> <strong>Email:</strong> dwilliams@terravista.bz<br> <strong>Address:</strong> 1730 Lucerne Terrace, Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>The Foundation for Orange County Public Schools</h4>
                              
                              <p><strong>Representative:</strong> Elizabeth Conrad<br> <strong>Phone:</strong> 407-317-3261<br> <strong>Fax:</strong> 407-317-3457<br> <strong>Email:</strong> elizabeth.conrad@ocps.net<br> <strong>Address:</strong> 445 W. Amelia St. Suite 901, Orlando, Florida 32801
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>The Leukemia &amp; Lymphoma Society</h4>
                              
                              <p><strong>Representative:</strong> April Carpenter<br> <strong>Phone:</strong> 407-898-0733<br> <strong>Fax:</strong> 407-896-8645<br> <strong>Email:</strong> linda.gurian@lls.org<br> <strong>Address:</strong> 341 Maitland Ave. Suite 100, Orlando, Florida 32751
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>The Potters House Academy and Daycare</h4>
                              
                              <p><strong>Representative:</strong> Michelle Pacheco<br> <strong>Phone:</strong> 407-273-8215<br> <strong>Fax:</strong> 407-736-0762<br> <strong>Email:</strong> Jpacheco@cdaorlando.org<br> <strong>Address:</strong> 7051 Pershing Ave, Orlando, Florida 32822
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Transforming Children and Families LLC</h4>
                              
                              <p><strong>Representative:</strong> Sharday Moore<br> <strong>Phone:</strong> 766-209-9626<br> <strong>Fax:</strong><br> <strong>Email:</strong> sharday.moore@gmail.com<br> <strong>Address:</strong> 1350 West Colonial Drive, Orlando, Florida 32804
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Treasure Of Knowledge Christian Academy</h4>
                              
                              <p><strong>Representative:</strong> Chad Lang<br> <strong>Phone:</strong> 407-859-8755<br> <strong>Fax:</strong> 407-858-0494<br> <strong>Email:</strong> tkcaorlando@gmail.com<br> <strong>Address:</strong> 13001 Landstar Blvd, Orlando, Florida 32824
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College (Osceola), OSD</h4>
                              
                              <p><strong>Representative:</strong> Nikkia Gumbs<br> <strong>Phone:</strong> 407-582-4094<br> <strong>Fax:</strong> (407 )582-4804<br> <strong>Email:</strong> ngumbs@valenciacollege.edu<br> <strong>Address:</strong> 1800 Denn John Lane, Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College (West Campus), Advising Office</h4>
                              
                              <p><strong>Representative: Lynn Desjarlais</strong><br> <strong>Phone:</strong> 407-582-1549<br> <strong>Fax:</strong><br> <strong>Email:</strong> ldesjarlais@valenciacollege.edu<br> <strong>Address:</strong> 1800 S. Kirkman Rd Orlando, FL 32811
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College (West Campus), Career Center</h4>
                              
                              <p><strong>Representative:</strong> Christine Moore<br> <strong>Phone:</strong> 4072995000<br> <strong>Fax:</strong><br> <strong>Email:</strong> cmoran6@valenciacollege.edu<br> <strong>Address:</strong> 1800 South Kirkman Road, Orlando, Florida 32811<br> <strong>Note:</strong> This location is currently NOT accepting volunteers at this time.
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College (West Campus), Internships &amp; Workforce Services Office</h4>
                              
                              <p><strong>Representative:</strong> Jessica Camilo<br> <strong>Phone:</strong> 407-582-1034<br> <strong>Fax:</strong> 407-582-1864<br> <strong>Email:</strong> jcamilo4@valenciacollege.edu<br> <strong>Address:</strong> 1800 S. Kirkman Rd., SSB-235 Orlando, FL 32811
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College (West Campus), Transitions</h4>
                              
                              <p><strong>Representative: </strong> La'Tasha Graham<br> <strong>Phone:</strong> 407-582-1434<br> <strong>Fax:</strong> 407-582-1038<br> <strong>Email:</strong> lgraham18@valenciacollege.edu<br> <strong>Address:</strong> 1800 S. Kirkman Rd. Orlando, FL 32811<br> <strong>Note:</strong> This location is currently NOT accepting volunteers at this time
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia College Depot (Osceola Campus)</h4>
                              
                              <p><strong>Representative:</strong> Nelson Torres<br> <strong>Phone:</strong> 321-682-4346<br> <strong>Fax:</strong><br> <strong>Email:</strong> ntorresarroyo@valenciacollege.edu<br> <strong>Address:</strong> 1800 Denn John Lane, Kissimmee, FL, 34744
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Valencia Volunteers (Osceola Campus)</h4>
                              
                              <p><strong>Representative:</strong> Tracy Vincent<br> <strong>Phone:</strong> 321-682-4960<br> <strong>Fax:</strong><br> <strong>Email: </strong><u>tvincent2@valenciacollege.edu</u> <br> <strong>Address: </strong>1800 Denn John Lane Kissimmee, Florida 34744
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Victory Prep Christian Academy</h4>
                              
                              <p><strong>Representative:</strong> Monique Benet<br> <strong>Phone:</strong> (407)293-1950<br> <strong>Fax:</strong> (407)293-1961<br> <strong>Email:</strong> ahishamoyet@vpreracademy.info<br> <strong>Address:</strong> 5324A Silver Star Road, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Volunteers for Community Impact</h4>
                              
                              <p><strong>Representative:</strong> Delilah Rivera<br> <strong>Phone:</strong> 407-298-4180<br> <strong>Fax:</strong><br> <strong>Email:</strong> drivera@vcifl.org<br> <strong>Address:</strong> 3545 Lake Breeze, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Watercrest at Lake Nona</h4>
                              
                              <p><strong>Representative:</strong> Christopher S. Shepherd<br> <strong>Phone:</strong> (239)293-1049<br> <strong>Fax:</strong><br> <strong>Email:</strong> cshepherd@watercrestslg.com<br> <strong>Address:</strong> 445 24th Street Suite 300, Vero Beach, Florida 32960
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Wells Built Museum</h4>
                              
                              <p><strong>Representative:</strong> James Deshay<br> <strong>Phone:</strong> 407-245-7535<br> <strong>Fax:</strong><br> <strong>Email:</strong> wbmuseum@hotmail.com<br> <strong>Address:</strong> 511 W. South St., Orlando, Florida 32805
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>West Oaks Academy</h4>
                              
                              <p><strong>Representative:</strong> Richard Jones<br> <strong>Phone:</strong> 407-292-8481<br> <strong>Fax:</strong><br> <strong>Email:</strong> n/a<br> <strong>Address:</strong> 8624 A.D. Mims Rd., Orlando, Florida 32818
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Winnie Palmer Hospital</h4>
                              
                              <p><strong>Representative:</strong> Alexis Bentley- "100 hours are required for this location"<br> <strong>Phone:</strong> 321-841-1809<br> <strong>Fax:</strong><br> <strong>Email:</strong> alexis.bently@orlandohealth.com<br> <strong>Address:</strong> 92 West Miller St., Orlando, Florida 32806
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>Winter Park Memorial Hospital</h4>
                              
                              <p><strong>Representative:</strong> Dotty McFarlane- "100 hours are required for this location"<br> <strong>Phone:</strong> 407-646-7090<br> <strong>Fax:</strong><br> <strong>Email:</strong> dorothy.mcfarlane@flhosp.org<br> <strong>Address:</strong> 200 Lakemont Ave., Winter Park, Florida 32792
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA (Avalon Park)</h4>
                              
                              <p><strong>Representative:</strong> Meredith Swanson<br> <strong>Phone:</strong> 407-381-2512<br> <strong>Fax:</strong><br> <strong>Email:</strong> n/a<br> <strong>Address:</strong> 12001 Avalon Lake Drive, Orlando, Florida 32828
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA Lockhart Elementary After School Program</h4>
                              
                              <p><strong>Representative:</strong> Larissa Brennan<br> <strong>Phone:</strong> 407-296-6440<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 7500 Edgewater Drive Orlando, Florida 32810
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA Osceola County</h4>
                              
                              <p><strong>Representative:</strong> Junie Lewis<br> <strong>Phone:</strong> 407-847-7413<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 2117 W. Mabbette Street Kissimmee, Florida 34741
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA READS! Rolling Hills Elementary</h4>
                              
                              <p><strong>Representative:</strong> Michael Hallbrook<br> <strong>Phone:</strong> 407-296-6530<br> <strong>Fax:</strong><br> <strong>Email:</strong> reads-rollinghills@cfymca.org<br> <strong>Address:</strong> 4903 Donovan Street, Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA Roper</h4>
                              
                              <p><strong>Representative:</strong> Lani Lanes<br> <strong>Phone:</strong> 407-656-6430 ext. 236<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 100 Windermere Road Winter Garden, Florida 34787
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA South Orlando Family Center</h4>
                              
                              <p><strong>Representative:</strong> Jessica Frometa<br> <strong>Phone:</strong> 407-855-2430<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 814 W. Oak Ridge Road Orlando, Florida 32809
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA Tangelo Park</h4>
                              
                              <p><strong>Representative:</strong> Amanda Molina<br> <strong>Phone:</strong> 407-855-2430<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 5160 Pueblo Street Orlando, Florida 32819
                              </p>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA Wayne Densch</h4>
                              
                              <p><strong>Representative:</strong> Deborah Allen<br> <strong>Phone:</strong> 407-299-4350<br> <strong>Fax:</strong><br> <strong>Email:</strong><br> <strong>Address:</strong> 870 N. Hastings Street Orlando, Florida 32808
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <h4>YMCA of Central Florida</h4>
                              
                              <p><strong>Representative:</strong> Jason Hernandez<br> <strong>Phone:</strong> 407-644-1509<br> <strong>Fax:</strong><br> <strong>Email:</strong> Jhernandez@cfyma.org<br> <strong>Address:</strong> 1201 N. Lakemont Avenue, Winter Park, Florida 32792
                              </p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  	
               </div>
               
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/community-service-locations.pcf">©</a>
      </div>
   </body>
</html>