<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Programs | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        					
                        <h2>Bridges to Success Programs</h2>
                        					
                        <h3>Bridges Summer Program</h3>
                        					
                        <p>The Bridges to Success (BTS) Summer Program provides you with an opportunity to begin
                           your college-level work in the summer prior to full-time degree seeking enrollment
                           at Valencia College in the fall. If you are selected for the program you will register
                           and attend courses from June to August.
                        </p>
                        					
                        <p>Bridges students receive individualized personal attention from Valencia's faculty
                           and staff. These individuals are dedicated to assisting students in making a successful
                           transition from high school to college and will help build leadership skills through
                           community service and extra-curricular activities.
                        </p>
                        					
                        <p>Upon successful completion of the Bridges to Success Summer Program you will become
                           a Bridges to Success Achievers Scholar in the Fall. Bridges Achievers Scholars receive
                           a renewable in-state full-tuition scholarship and book stipend that will be coordinated
                           with financial aid to cover up to the full cost of attending Valencia College.
                        </p>
                        					
                        <hr class="styled_2">
                        					
                        <h3>Bridges Achievers Program</h3>
                        					
                        <p>Students who successfully complete the Bridges to Success Summer Program are eligible
                           to transition into the Bridges Achievers Program and to receive the Bridges to Success
                           Achievers Scholarship. The Bridges Achievers Program provides continued support and
                           mentoring while building strong academic and leadership skills.
                        </p>
                        					
                        <p>Bridges Achievers students participate in 30 hours of community service each term
                           as tutors, mentors and role models in programs such as AVID, Head Start, YMCA After
                           School Programs, and as elementary school reading tutors. Attendance at a variety
                           of skills workshops is also required.
                        </p>
                        					
                        <hr class="styled_2">
                        					
                        <h3>How to Apply</h3>
                        					
                        <p>Applications for next year's Summer Program will open in September. See the <a href="/students/bridges-to-success/calendar.php">BTS Calendar</a> for details.
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/programs.pcf">©</a>
      </div>
   </body>
</html>