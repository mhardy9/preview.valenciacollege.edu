<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia College Resources | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Valencia College Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Valencia College Resources</h2>
                        
                        <h3>East Campus</h3>
                        
                        <p><strong>East Campus Testing Center</strong> <br> Bldg 4-120 <br> <a href="tel:407-582-2795">407-582-2795</a> <br> Monday - Thursday 7am - 10pm <br> Friday: 8am - 8pm <br> Saturday: 8am - 4pm
                        </p>
                        
                        <p><strong>East Campus Library</strong> <br> Bldg 4-201 <br> <a href="tel:407-582-2456">407-582-2456</a> <br> Monday - Thursday: 7am -10pm <br> Friday: 8am - 8pm <br> Saturday: am - 4pm
                        </p>
                        
                        <p><strong>East Campus Tutoring</strong> <br> Bldg 4-101 <br> <a href="tel:407-582-2840">407-582-2840</a> <br> Monday - Thursday: 7am - 10pm <br> Friday: 8am - 8pm <br> Saturday: 8am - 4pm
                        </p>
                        	
                        <hr class="styled_2">
                        
                        <h3>Osceola Campus</h3>
                        
                        <p><strong>Osceola Writing Center</strong> <br> Bldg 3, Rm 100 <br> <a href="tel:407-582-4557">407-582-4557</a> <br> Monday - Thursday: 8am - 9pm <br> Friday: 8am - 5pm <br> Saturday: 8am - 1pm
                        </p>
                        
                        <p><strong>Osceola Math Depot</strong> <br> Bldg 4, Rm 121 <br> <a href="tel:407-582-4856">407-582-4856</a> <br> Monday - Thursday: 8am to 9:30pm <br> Friday: 8am - 5pm <br> Saturday: 8am - 1pm
                        </p>
                        
                        <p><strong>Osceola Language Lab</strong> <br> Bldg 4, Rm 121 <br> <a href="tel:407-582-4146">407-582-4146</a> <br> Monday - Thursday: 8am - 9:30pm <br> Friday: 8am - 12pm <br> Saturday: 8am - 12pm
                        </p>
                        	
                        <hr class="styled_2">
                        
                        <h3>West Campus</h3>
                        
                        <p><strong>West Campus Writing Center</strong> <br> Bldg 7-240 <br> <a href="tel:407-582-5454">407-582-5454</a> <br> Monday - Thursday 8am - 8pm <br> Friday: 8am - 5pm <br> Saturday: 9am - 2pm
                        </p>
                        
                        <p><strong>West Campus Library</strong> <br> Bldg 6 <br> <a href="tel:407-582-1574">407-582-1574</a> <br> Monday - Thursday: 7:30am - 10:00pm (1st floor) <br> Monday - Thursday: 7:30am - 9:00pm (2nd floor) <br> Friday: 7:30am - 5pm <br> Saturday: 9am - 1pm <br> Sunday: 2pm - 6pm
                        </p>
                        
                        <p><br> <strong>West Campus Math Center</strong> <br> Bldg 7-240 <br><a href="tel:407-582-1633">407-582-1633</a><br> Monday - Thursday: 8am - 8pm <br> Friday: 8am - 7pm
                        </p>
                        
                     </div>
                     
                  </div>
                  	  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/resources.pcf">©</a>
      </div>
   </body>
</html>