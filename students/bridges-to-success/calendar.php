<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Calendar | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/calendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Calendar</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Calendar</h2>
                        	
                        <h3>Important Dates &amp; Deadlines</h3>
                        
                        
                        <p>Students participating in the Bridge to Success (BTS) program are required to attend
                           a number of events
                           and programs throughout the year. Students should pay special attention to the dates
                           listed below as many
                           of the programs are mandatory.
                        </p>
                        
                        
                        <p><strong>Please also note that dates/times are subject to change without notice.</strong></p>
                        
                        
                        <p>Check the website regularly to stay informed!</p>
                        
                        
                        <div class="row">
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="#skillshops_calendar">SkillShops 2016-2017</a></li>
                                 
                                 <li><a href="#fall_2016_calendar">Fall 2016 Deadlines</a></li>
                                 
                                 <li>
                                    <a href="#spring_2017_calendar">Spring 2017 Deadlines</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <ul class="list-unstyled">
                                 
                                 <li>
                                    <a href="#summer_2017_calendar">Summer 2017 Deadlines</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="#bts_application_calendar">BTS Application Days</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in" id="skillshops_calendar">
                        
                        <h3>SkillShops 2016-2017</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Date</th>
                                 
                                 <th scope="col"> Event</th>
                                 
                                 <th scope="col"> Time</th>
                                 
                                 <th scope="col"> Location</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>September 7, 2016</td>
                                 
                                 <td>Financial Empowerment</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 22, 2016</td>
                                 
                                 <td>Financial Empowerment</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 28, 2016</td>
                                 
                                 <td>Business Savvy Skills</td>
                                 
                                 <td>2:00P 3:00P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 27, 2016</td>
                                 
                                 <td>Time Management - The Clock is Ticking</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>Osceola Campus , 4-341</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 05, 2016</td>
                                 
                                 <td>Financial Empowerment</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 13, 2016</td>
                                 
                                 <td>Let's Talk Politics</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 13, 2016</td>
                                 
                                 <td>Let's Talk Politics</td>
                                 
                                 <td>4:00P-5:00P</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 13, 2016</td>
                                 
                                 <td>Business Savvy Skills</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 19, 2016</td>
                                 
                                 <td>Time Management - The Clock is Ticking</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, 3-214</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 21, 2016</td>
                                 
                                 <td>Let's Talk Politics</td>
                                 
                                 <td>1:30P-2:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 09, 2016</td>
                                 
                                 <td>Business Savvy Skills</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 28, 2016</td>
                                 
                                 <td>Questions and Answers for Young Adults</td>
                                 
                                 <td>2:00P-3:00P</td>
                                 
                                 <td>Osceola Campus, 1-219B</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 6, 2016</td>
                                 
                                 <td>Career Center</td>
                                 
                                 <td>5:30P-6:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 25, 2017</td>
                                 
                                 <td>Passion for Healing - Professions in Health</td>
                                 
                                 <td>4:00P- 5:00P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 02, 2017</td>
                                 
                                 <td>I'm Stressed, You're Stressed, We're ALL Stressed - Combating Anxiety</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 09, 2017</td>
                                 
                                 <td>Passion for Healing - Professions in Health</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 15, 2017</td>
                                 
                                 <td>Building Leadership Skills - Follower vs Leader</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 16, 2017</td>
                                 
                                 <td>Passion for Healing - Professions in Health</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 01, 2017</td>
                                 
                                 <td>I'm Stressed, You're Stressed, We're ALL Stressed - Combating Anxiety</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 08, 2017</td>
                                 
                                 <td>Building Leadership Skills - Follower vs Leader</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>Osceola Campus , 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 05, 2017</td>
                                 
                                 <td>I'm Stressed, You're Stressed, We're ALL Stressed - Combating Anxiety</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 06, 2017</td>
                                 
                                 <td>You've Hit Bottom - Now What?</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>West Campus, SSB-206G</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 12, 2017</td>
                                 
                                 <td>Building Leadership Skills - Follower vs Leader</td>
                                 
                                 <td>2:30P- 3:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 13, 2017</td>
                                 
                                 <td>You've Hit Bottom - Now What?</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>East Campus, 8-105D</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 19, 2017</td>
                                 
                                 <td>You've Hit Bottom - Now What?</td>
                                 
                                 <td>2:30P-3:30P</td>
                                 
                                 <td>Osceola Campus , 1-101</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in" id="fall_2016_calendar">
                        
                        <h3>Bridges Dates/Deadlines, Fall 2016</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Date</th>
                                 
                                 <th scope="col"> Event</th>
                                 
                                 <th scope="col"> Time</th>
                                 
                                 <th scope="col"> Location</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>August 8, 2016</td>
                                 
                                 <td>Bridges Appeal Deadline</td>
                                 
                                 <td>Offices Close 5P</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>August 12, 2016</td>
                                 
                                 <td>Financial Aid SAP Appeal Priority Deadline</td>
                                 
                                 <td>Offices Close 5P</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>August 19, 2016</td>
                                 
                                 <td>BTS Planning/Organization Follow-Up Workshop</td>
                                 
                                 <td>9:00A-10:15A</td>
                                 
                                 <td>West Campus, Building 3 Room 214</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>August 29, 2016</td>
                                 
                                 <td>Classes Begin</td>
                                 
                                 <td> </td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 10, 2016</td>
                                 
                                 <td>2017 BTS Summer Program Application Released</td>
                                 
                                 <td>Via Online</td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 5, 2016</td>
                                 
                                 <td>Labor Day (College Closed)</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 6, 2016</td>
                                 
                                 <td>Drop/Refund Deadline</td>
                                 
                                 <td> </td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 6, 2016</td>
                                 
                                 <td>Community Service Fair</td>
                                 
                                 <td>12:00P-2:00P</td>
                                 
                                 <td>West Campus, SSB Patio &amp; Breezeway</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 7, 2016</td>
                                 
                                 <td>Community Service Fair</td>
                                 
                                 <td>11:00A-2:00P</td>
                                 
                                 <td>Osceola Campus, Building 4 Courtyard</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 9, 2016</td>
                                 
                                 <td>Osceola Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 9, 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00-11:30A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 9, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:00-2:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 16, 2016</td>
                                 
                                 <td>Community Service Location Form Deadline</td>
                                 
                                 <td>by 5:00P Online</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 16, 2016</td>
                                 
                                 <td>West Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>West Campus, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 16, 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00-11:30A</td>
                                 
                                 <td>West Campus, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 16, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:30P-3:00P</td>
                                 
                                 <td>West Campus, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 16, 2016</td>
                                 
                                 <td>Valencia College Graduation Applications Due</td>
                                 
                                 <td>Offices Close 5P</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 22, 2016</td>
                                 
                                 <td>Hispanic Heritage Month Celebration</td>
                                 
                                 <td> </td>
                                 
                                 <td>East Campus</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 23, 2016</td>
                                 
                                 <td>East Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 23, 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00-11:30A</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>September 23, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 4, 2016</td>
                                 
                                 <td>Valencia College Night</td>
                                 
                                 <td>6:00P-8:00P</td>
                                 
                                 <td>Osceola Campus</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 6, 2016</td>
                                 
                                 <td>Valencia College Night</td>
                                 
                                 <td>CANCELLED</td>
                                 
                                 <td>CANCELLED</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 12, 2016</td>
                                 
                                 <td>Parent Meeting East &amp; Osceola</td>
                                 
                                 <td>6:30P-7:45P</td>
                                 
                                 <td>Osceola Campus,1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 13, 2016</td>
                                 
                                 <td>Parent Meeting West</td>
                                 
                                 <td>6:30P-7:45P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr class="danger">
                                 
                                 <td>October 14, 2016</td>
                                 
                                 <td>Community Service/Workshop Deadline #1<br>15 hrs &amp; 2 workshops Due TODAY
                                 </td>
                                 
                                 <td>Offices Close 5P</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 17, 2016</td>
                                 
                                 <td>Group Advising (Returning Students)</td>
                                 
                                 <td>9:00A-5:00P</td>
                                 
                                 <td>Osceola Campus, 1-219B</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 18, 2016</td>
                                 
                                 <td>Group Advising (Returning Students)</td>
                                 
                                 <td>9:00A-5:00P</td>
                                 
                                 <td>West Campus, SSB-110D</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 20, 2016</td>
                                 
                                 <td>Group Advising (Returning Students)</td>
                                 
                                 <td>9:00A-5:00P</td>
                                 
                                 <td>East Campus, 5-112</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 29, 2016</td>
                                 
                                 <td>Adopt A Highway Service Project</td>
                                 
                                 <td>7:30A-10:30A</td>
                                 
                                 <td>All Campuses- Will Be Notified By Your Advisor Of Location</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 <td>West Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>West, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 <td>West ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>West, 11-106</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>West Campus, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>West Campus, 3-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 <td>Valencia College Withdrawal Deadline "W" Grade</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 23-27, 2016</td>
                                 
                                 <td>Thanksgiving Holiday</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 2, 2016</td>
                                 
                                 <td>Osceola Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 2, 2016</td>
                                 
                                 <td>Osceola ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>Osceola Campus, 1-219B</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 2, 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 2, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr class="danger">
                                 
                                 <td>December 9, 2016</td>
                                 
                                 <td>Community Service/Workshop Deadline #2<br>15 hrs &amp; 2 workshops Due TODAY
                                 </td>
                                 
                                 <td>Offices Close 5P</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 9, 2016</td>
                                 
                                 <td>East Molding Men for Tomorrow</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 9, 2016</td>
                                 
                                 <td>East ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>East Campus, 8-102</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 9 , 2016</td>
                                 
                                 <td>Campus Meeting (NEW SUMMER 2016 STUDENTS)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>East Campus , 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 9, 2016</td>
                                 
                                 <td>Campus Meeting (RETURNING STUDENTS)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 18, 2016</td>
                                 
                                 <td>Valencia College Term Ends</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 12-18, 2016</td>
                                 
                                 <td>Final Exams/Semester Ends</td>
                                 
                                 <td> </td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 21, 2016-January 1, 2017</td>
                                 
                                 <td>Winter Break</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     
                     <div class="indent_title_in" id="spring_2017_calendar">
                        
                        <h3>Bridges Dates/Deadlines, Spring 2017</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Date</th>
                                 
                                 <th scope="col"> Event</th>
                                 
                                 <th scope="col"> Time</th>
                                 
                                 <th scope="col"> Location</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>January 3, 2017</td>
                                 
                                 <td>Academic Appeals Deadline</td>
                                 
                                 <td>By Noon 12:00P</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 5, 2017</td>
                                 
                                 <td>SAP Appeals Deadline</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 9, 2017</td>
                                 
                                 <td>Classes Begin</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>West Campus, 8-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>West Campus, 8-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Campus Meeting (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>West Campus, 8-111</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 13, 2017</td>
                                 
                                 <td>Campus Meeting (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 16, 2017</td>
                                 
                                 <td>MLK Holiday</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 17, 2017</td>
                                 
                                 <td>Drop/Refund Deadline</td>
                                 
                                 <td> </td>
                                 
                                 <td>Campuses E,O, &amp; W</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 20, 2017</td>
                                 
                                 <td>Valencia College Graduation Application for Spring 2017</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 20, 2017</td>
                                 
                                 <td>New Community Service Location Request Deadline</td>
                                 
                                 <td>By 5:00P</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 20, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 20, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>Osceola Campus,1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 20, 2017</td>
                                 
                                 <td>Campus Meeting (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 26, 2017</td>
                                 
                                 <td>Bridges Application Deadline</td>
                                 
                                 <td>Due ONLINE by 5:00 PM</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 28, 2017</td>
                                 
                                 <td>Adopt A Highway Service Project</td>
                                 
                                 <td>7:30A-10:30A</td>
                                 
                                 <td>All Campuses- Will Be Notified By Your Advisor Of Location</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>January 30, 2017 - February 3, 2017</td>
                                 
                                 <td>Bridges Office CLOSED</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 3, 2017</td>
                                 
                                 <td>USF College Tour</td>
                                 
                                 <td> </td>
                                 
                                 <td>Details of College Tour sent via Email</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 8-9, 2017</td>
                                 
                                 <td>Bridges Office CLOSED</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 13-17, 2017</td>
                                 
                                 <td>Bridges Office CLOSED</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 20, 2017</td>
                                 
                                 <td>Group Advising (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>8:00A-5:00P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 21, 2017</td>
                                 
                                 <td>Group Advising (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>8:00A-5:00P</td>
                                 
                                 <td>Osceola Campus, 4-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 22-25, 2017</td>
                                 
                                 <td>Black, Brown &amp; College Bound Conference</td>
                                 
                                 <td> </td>
                                 
                                 <td>Details of College Tour sent via Email</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 28, 2017</td>
                                 
                                 <td>Black History Month- Movie Day</td>
                                 
                                 <td>5PM-9PM</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 28, 2017</td>
                                 
                                 <td>Black History Month- Movie Day</td>
                                 
                                 <td>5PM-9PM</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>February 28, 2017</td>
                                 
                                 <td>Black History Month- Movie Night</td>
                                 
                                 <td>5PM-9PM</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 1, 2017</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 1, 2017</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>Osceola Campus, 1-233</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 1, 2017</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>West Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 3, 2017</td>
                                 
                                 <td>West ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 6, 2017</td>
                                 
                                 <td>Group Advising (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>8:00A-5:00P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 10, 2017</td>
                                 
                                 <td>Osceola ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 13-19, 2017</td>
                                 
                                 <td>Spring Break</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 16, 2017</td>
                                 
                                 <td>Acceptance Form Deadline (2017 Students ONLY)</td>
                                 
                                 <td>By 5:00P</td>
                                 
                                 <td>Online</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 24, 2017</td>
                                 
                                 <td>East ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 24, 2017</td>
                                 
                                 <td>Community Service/Workshops Deadline #1</td>
                                 
                                 <td>Offices Close at 5:00P</td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 24-25, 2017</td>
                                 
                                 <td>FAMU College Tour</td>
                                 
                                 <td> </td>
                                 
                                 <td>Details of College Tour sent via Email</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 31, 2017</td>
                                 
                                 <td>Withdrawal Deadline W Grade</td>
                                 
                                 <td>By 11:59P</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>March 31, 2017</td>
                                 
                                 <td>Women of Distinction Luncheon</td>
                                 
                                 <td>11:30A-1:30P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 5, 2017</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>East Campus, 3-113</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 5, 2017</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>Osceola Campus, 1-233</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 5, 201 7</td>
                                 
                                 <td>ASP Group Meeting</td>
                                 
                                 <td>10:00A-3:00P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7, 2017</td>
                                 
                                 <td>Scholarship Contract Deadline</td>
                                 
                                 <td>By 5:00P</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7, 2017</td>
                                 
                                 <td>Community Service Location Form Deadline</td>
                                 
                                 <td>By 5:00P Online</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7th, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>9:15A-10:15A</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7th, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:30A-12:00P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7th, 2017</td>
                                 
                                 <td>Campus Meeting (1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 7th, 2017</td>
                                 
                                 <td>West ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>West Campus, HSB-105</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 14, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 14, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 14, 2017</td>
                                 
                                 <td>Campus Meeting(1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 14, 2017</td>
                                 
                                 <td>Osceola ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>Osceola Campus, 1-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 21, 2017</td>
                                 
                                 <td>Community Service/Workshop Deadline #2</td>
                                 
                                 <td>Offices Close at 5:00P</td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 21, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>8:45A-9:45A</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 21, 2017</td>
                                 
                                 <td>Campus Meeting (1617 Cohort ONLY)</td>
                                 
                                 <td>10:00A-11:30A</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 21, 2017</td>
                                 
                                 <td>Campus Meeting(1415-1516 Cohorts ONLY)</td>
                                 
                                 <td>1:00P-2:30P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 21, 2017</td>
                                 
                                 <td>East ROAR Meeting</td>
                                 
                                 <td>3:00P-4:15P</td>
                                 
                                 <td>East Campus, 8-101</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 29, 2017</td>
                                 
                                 <td>Adopt A Highway Service Project</td>
                                 
                                 <td>7:30A-10:30A</td>
                                 
                                 <td>All Campuses- Will Be Notified By Your Advisor Of Location</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 24-30, 2017</td>
                                 
                                 <td>Final Exams/ Semester Ends</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>April 30, 2017</td>
                                 
                                 <td>Valencia College Term Ends</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 4, 2017</td>
                                 
                                 <td>BTS Academic Appeal Deadline</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 5, 2017</td>
                                 
                                 <td>Valencia College Commencement</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 5, 2017</td>
                                 
                                 <td>SAP Appeals Deadline</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 19, 2017</td>
                                 
                                 <td>Valencia College Graduation Application for Summer 2017</td>
                                 
                                 <td> </td>
                                 
                                 <td> </td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 29, 2017</td>
                                 
                                 <td>Memorial Day</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in" id="summer_2017_calendar">
                        
                        <h3>Bridges Dates/Deadlines, Summer 2017</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Date</th>
                                 
                                 <th scope="col"> Event</th>
                                 
                                 <th scope="col"> Time</th>
                                 
                                 <th scope="col"> Location</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>June 6, 2017</td>
                                 
                                 <td>New Student Parent Orientation</td>
                                 
                                 <td>8:30A-10:00A</td>
                                 
                                 <td>West Campus, 8-111 Special Events Center</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 6, 2017</td>
                                 
                                 <td>New Student Orientation</td>
                                 
                                 <td>1:00P-5:00P</td>
                                 
                                 <td>West Campus, 8-111 Special Events Center</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 7, 2017</td>
                                 
                                 <td>New Student Parent Orientation (Osceola Campus)</td>
                                 
                                 <td>8:30A-10:00A</td>
                                 
                                 <td>Osceola Campus, Building 1 Room 101 Auditorium</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 7, 2017</td>
                                 
                                 <td>New Student Orientation (Osceola Campus)</td>
                                 
                                 <td>1:00P-5:00P</td>
                                 
                                 <td>Osceola Campus, Building 1 Room 101 Auditorium</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 8, 2017</td>
                                 
                                 <td>New Student Parent Orientation (East Campus)</td>
                                 
                                 <td>8:30A-10:00A</td>
                                 
                                 <td>School of Public Safety, Building 1 Auditorium</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 8, 2017</td>
                                 
                                 <td>New Student Orientation (East Campus)</td>
                                 
                                 <td>1:00P-5:00P</td>
                                 
                                 <td>School of Public Safety, Building 1 Auditorium</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>May 29th, 2017 - June 2, 2017</td>
                                 
                                 <td>FAFSA 201617 Submission Deadline (2017 Cohort)</td>
                                 
                                 <td> </td>
                                 
                                 <td>Newly accepted SUMMER STUDENTS</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 16, 2017</td>
                                 
                                 <td>Financial Aid Documentation Submission Deadline (2017 - New Students)</td>
                                 
                                 <td> </td>
                                 
                                 <td>Submit Documentation to the Answer Center; You must have a VALID ID to be seen</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 20, 2017</td>
                                 
                                 <td>Summer B Starts</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>Parent Information Sheet Deadline</td>
                                 
                                 <td>Submit Online by 5:00P</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>Final High School Transcript Deadline</td>
                                 
                                 <td>Submit by 12P</td>
                                 
                                 <td>All Campuses, Answer Center</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>ROAR (Females only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>June 23, 2017</td>
                                 
                                 <td>ROAR (Females only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 4, 2017</td>
                                 
                                 <td>Independence Day</td>
                                 
                                 <td>College Closed</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus,TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 7, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>West Campus, 4-120</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>East Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>New Bridges Student Meeting</td>
                                 
                                 <td>9A-10:15</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>Molding Men For Tomorrow Meeting</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>ROAR (Females Only)</td>
                                 
                                 <td>10:30A-11:30</td>
                                 
                                 <td>Osceola Campus, TBA</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 21, 2017</td>
                                 
                                 <td>Achievers Scholarship Contract Deadline (NEW Students ONLY)</td>
                                 
                                 <td>By 5:00P Online</td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>July 22, 2017</td>
                                 
                                 <td>Adopt A Highway Service Project</td>
                                 
                                 <td>7:30A-10:30A</td>
                                 
                                 <td>All Campuses- Will Be Notified By Your Advisor Of Location</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>August 1, 2017</td>
                                 
                                 <td>Final Exams/ Summer Term Ends</td>
                                 
                                 <td> </td>
                                 
                                 <td>All Campuses</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>August 1, 2017</td>
                                 
                                 <td>BTS Recognition/Summer Splash (Required Event)</td>
                                 
                                 <td>11:00A-2:00P</td>
                                 
                                 <td>West Camus, TBA</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in" id="bts_application_calendar">
                        
                        <h3>BTS Application Days</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <table class="table table table-striped add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col"> Date</th>
                                 
                                 
                                 <th scope="col"> Time</th>
                                 
                                 <th scope="col"> Location</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>September 28, 2016</td>
                                 
                                 
                                 <td>3:30P-5:30P</td>
                                 
                                 <td>West Campus- Building 6 Room 220</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>October 14, 2016</td>
                                 
                                 
                                 <td>6:00P-8:00P</td>
                                 
                                 <td>West Campus- SSB-173 (BTS Lab)</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 2, 2016</td>
                                 
                                 
                                 <td>3:30P-5:30P</td>
                                 
                                 <td>East Campus- Building 4-122 Testing Center Lab</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 9, 2016</td>
                                 
                                 
                                 <td>3:30P-5:30P</td>
                                 
                                 <td>West Campus-Building 6 Room 220</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 11, 2016</td>
                                 
                                 
                                 <td>10:00A-1:00P</td>
                                 
                                 <td>Osceola Campus - Building 4- Room 305</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>November 30, 2016</td>
                                 
                                 
                                 <td>3:30P-5:30P</td>
                                 
                                 <td>East Campus- Building 4-122 Testing Center Lab</td>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>December 7, 2016</td>
                                 
                                 
                                 <td>3:30P-5:30P</td>
                                 
                                 <td>Osceola Campus - Building 4- Room 305</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  	  
               </div>
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/calendar.pcf">©</a>
      </div>
   </body>
</html>