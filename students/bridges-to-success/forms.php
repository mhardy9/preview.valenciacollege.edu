<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Forms | Valencia College</title>
      <meta name="Description" content="Bridges to Success">
      <meta name="Keywords" content="bridges to success, bridges, bts, valenciacolleged.edu/bts, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/bridges-to-success/forms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/bridges-to-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Bridges to Success</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/bridges-to-success/">Bridges To Success</a></li>
               <li>Forms</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Forms</h2>
                        
                        <p>BTS student forms have been moved to the Atlas portal. You will need to have your
                           Atlas login and password to access the forms. 
                        </p>
                        
                        <p><strong>Instructions on viewing forms in Atlas:</strong></p>
                        
                        <ul>
                           
                           <li><em><strong>Login to Atlas  <a href="https://ptl5-prod.valenciacollege.edu:8443/web/home-community/students" target="_blank">"Click here to Login to Atlas"</a></strong></em></li>
                           
                           <li><strong><em>Go to the Student Resources Tab </em></strong></li>
                           
                           <li><strong><em>Click on "Bridges to Success' </em></strong></li>
                           
                           <li><strong><em>The forms are listed there</em></strong></li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3>General Forms</h3>
                        	
                        <ul>
                           		
                           <li>
                              			<a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=05ae5619-54ee-4a53-9c3f-81ae4e844e50">Parent Information Form (Spanish)</a>
                              		
                           </li>
                           		
                           <li>
                              			<a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=cf25bef1-e335-4c30-b305-722590436617">Parent Information Form (English)</a>
                              		
                           </li>
                           		
                           <li>
                              			<a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=be2c18ca-3e68-401a-bc6f-2a0a08eaafce">College Night/ High School Presentation Contact Form</a>
                              		
                           </li>
                           	
                        </ul>    
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Bridges Summer Program Forms (QR Codes)</h3>
                        	
                        <p></p>
                        	
                        <ul>
                           		
                           <li>
                              			<a href="/students/bridges-to-success/documents/sample-forms.pdf">Sample Forms</a>
                              		
                           </li>
                           		
                           <li>
                              			<a href="/students/bridges-to-success/documents/assumption-of-risk.pdf">Assumption of Risk</a>
                              		
                           </li>
                           		
                           <li>
                              			<a href="/students/bridges-to-success/documents/conference-delegate.pdf">Conference Delegate</a>
                              		
                           </li>
                           		
                           <li>
                              			<a href="/students/bridges-to-success/documents/talent_release_form.pdf">Talent Release Form</a>
                              		
                           </li>
                           	
                        </ul>
                        	
                        <p></p>
                        
                     </div>
                     
                  </div>
                  	  
               </div>
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/bridges-to-success/forms.pcf">©</a>
      </div>
   </body>
</html>