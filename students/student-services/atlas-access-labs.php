<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Atlas Access Labs | Student Services | Valencia College</title>
      <meta name="Description" content="Atlas Access Labs | Student Services">
      <meta name="Keywords" content="college, school, educational, student, services, atlas, access, labs">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/atlas-access-labs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Atlas Access Labs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Atlas Access Labs</h2>
                        
                        <p>These labs are open to all registered students to conduct educational and career related
                           research and planning. Open use includes access to the internet and your Atlas account,
                           with staff on hand to assist.
                        </p>
                        
                        <h3>Get Help with Atlas</h3>
                        
                        <p>Browse the <a href="/students/support/howto/index.html" target="_blank">Student Atlas How-To</a> or call the Student Help Desk at (407) 582-5444.
                        </p>
                        
                        <h3>Hours</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <table class="table bgColor3ContentBox">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td>Monday - Thursday</td>
                                             
                                             <td>8 AM to 6 PM</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Fridays</td>
                                             
                                             <td>9 AM to 5 PM</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Summer Fridays</td>
                                             
                                             <td>9 AM to Noon</td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Locations</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <table class="table bgColor3ContentBox">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td>East</td>
                                             
                                             <td>Building 5, Room 213</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>West</td>
                                             
                                             <td>Student Services Building (SSB), Room 172</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Winter Park</td>
                                             
                                             <td>Building 1, Room 217</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Osceola</td>
                                             
                                             <td>Building 2, Room 105</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Poinciana</td>
                                             
                                             <td>Room 102</td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td>Lake Nona</td>
                                             
                                             <td>Building 1, Room 147</td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/atlas-access-labs.pcf">©</a>
      </div>
   </body>
</html>