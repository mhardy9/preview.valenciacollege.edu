<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/help.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Where to Get Help</h2>
                        
                        <p><span>The Answer Center</span> <br>
                           (2-150) <br>
                           <br>
                           The Answer Center is where most student service activities begin.
                           This is the first stop for students to submit an application, discuss
                           financial aid, inquire about orientation, and receive basic academic
                           advising. 
                        </p>
                        
                        <p>A student visiting The Center will be met by staff personnel known as a
                           Student Services Specialist. These are professional staff members who are
                           trained to assist in the areas of financial aid, admissions and academic
                           advising. 
                        </p>
                        
                        <p>In the Center, you will receive assistance with every aspect of student
                           services. If you are still in need of additional information or assistance,
                           a referral is made for you to visit with one of the Student Services Managers
                           in the Student Services Offices. 
                        </p>
                        
                        <p><a href="help.html#top">TOP</a> 
                        </p>
                        
                        <p>              <span>Student Services </span><br>
                           (<a href="offices.html">Click
                              to visit Student Services</a>) <br>
                           <br>
                           The Student Services Offices are where you will receive specialized
                           assistance with issues that were more involved or complex than
                           those handled in the Center. 
                        </p>
                        
                        <p>A student visiting Student Services may meet with a Student Services Manager.
                           These professional staff members are the functional experts in their area.
                           A referral or appointment is required to visit with Student Services. 
                        </p>
                        
                        <p>In the Home Offices, you will receive specialized assistance with issues
                           that were more involved or complex than those handled in the Center. The
                           Student Services staff are trained to both assist and educate you. Once
                           they have helped with the situation at hand, they may ask you to take a
                           minute to learn a little bit more about what related resources would be
                           the most useful for you to explore. You may also be referred to the Atlas
                           Access Lab to learn how certain online tools can be used to plan for and
                           possibly prevent similar situations in the future. 
                        </p>
                        
                        <p> <a href="help.html#top">TOP</a></p>
                        
                        <p>                <span>Atlas Access Lab </span><br>
                           (<a href="atlas.html">Click to visit Atlas Access</a>) <br>
                           <br>
                           The Atlas Access is your resource for assistance with online
                           tools and services. The staff there can assist you with registration,
                           online career and educational planning, and other student service
                           applications. 
                        </p>
                        
                        <p>A student visiting Atlas Access is assisted by an Atlas Assistant. These
                           are student leaders who are employed by the college and trained to assist
                           students in the electronic processing of forms in the areas of financial
                           aid and admissions. 
                        </p>
                        
                        <p>In the Access Lab, you can receive assistance with internet searches, working
                           with your Atlas accounts, and utilizing other online educational and career
                           planning tools. If after working with the Atlas Assistant, you still would
                           like additional assistance, you may be referred to a Student Services Specialist
                           in the Answer Center for additional advisement.    
                        </p>
                        
                        <p><a href="help.html#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/help.pcf">©</a>
      </div>
   </body>
</html>