<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/atlas.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Atlas Access Lab</h2>
                        
                        <p> The Atlas Access is your resource for assistance with online tools 
                           and services.&nbsp;The staff there can assist you with registration, 
                           online career and educational planning, and other student service 
                           applications.&nbsp;
                        </p>
                        
                        <div>
                           
                           <div>
                              <div>
                                 
                                 <div>
                                    
                                    <p><span>Educational Tools</span></p>
                                    
                                    <ul>
                                       
                                       <li>
                                          <strong><a href="http://www.fafsa.ed.gov/">FAFSA Online 
                                                </a><br>
                                             </strong>Free Application for Federal Student Aid 
                                          
                                       </li>
                                       
                                       <li>
                                          <strong><a href="http://www.nelnet.net">Nelnet </a><br>
                                             </strong>Assistance with paying for college 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><span>Career Research</span></p>
                                    
                                    <ul>
                                       
                                       <li> <strong><a href="http://florida.echoices.com/eChoices/eChoices.nsf/frmlogin?OpenForm">Florida 
                                                eChoices </a><br>
                                             </strong>The career exploration and information <br>
                                          system for the state of Florida. 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><span>Educational Research</span></p>
                                    
                                    <ul>
                                       
                                       <li> <strong><a href="http://www.floridashines.org/">Floridashines.org 
                                                </a><br>
                                             </strong>Florida Virtual Campus 
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>Important Links</div>
                                             
                                          </div>
                                          
                                          <div>
                                             <div>
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      
                                                      <p><a href="../../catalog/index.html"><strong>Know your Catalog</strong></a><a href="../../catalog/index.html"><br>
                                                            </a></p>
                                                      
                                                   </li>
                                                   
                                                   <li>
                                                      
                                                      <p><strong>Having problems with Atlas?</strong>&nbsp;&nbsp; <strong><a href="mailto:askatlas@valenciacollege.edu"><br>
                                                               CLICK HERE</a></strong></p>
                                                      
                                                   </li>
                                                   
                                                   <li><a href="../../support/howto/index.html">How To...</a></li>
                                                   
                                                </ul> 
                                             </div>
                                          </div>   
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </div>              
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/atlas.pcf">©</a>
      </div>
   </body>
</html>