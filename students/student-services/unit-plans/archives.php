<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Departmental Unit Action Plans  | Valencia College</title>
      <meta name="Description" content="Departmental Unit Action Plans">
      <meta name="Keywords" content="college, school, educational, student, services, unit, action, plans">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/unit-plans/archives.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/student-services/unit-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		Departmental Unit Action Plans
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li><a href="/students/student-services/unit-plans/">Unit Plans</a></li>
               <li>Departmental Unit Action Plans </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Student Affairs Planning and Evaluation Archives</h3>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>To comply with SACS&nbsp;<em><strong>Principles of Accreditation</strong></em>&nbsp;requirements,&nbsp;<strong>Comprehensive
                                 Standards 3.3.1</strong>, each Student Affairs department will design and implement an annual plan to
                              improve student services and success.
                           </p>
                           
                           <p><strong>3.3.1</strong>&nbsp;The institution identifies expected outcomes for its educational programs and its
                              administrative and educational support services; assesses whether it achieves these
                              outcomes; and provides
                              evidence of improvement based on analysis of those results.
                           </p>
                           
                           <p>The&nbsp;<strong>Index of Plans</strong>&nbsp;provided below includes an informational overview including
                              a&nbsp;<strong>Department Action Plan (DAP)</strong>&nbsp;number, department of origin, campus, student affairs
                              leader and a brief topical focus for each plan. By clicking on a&nbsp;<strong>Division Action Plan
                                 (DAP)</strong>&nbsp;number you will be linked to current planning and evaluation documents.
                           </p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2014-15</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-Renee-DAPAdmissionsandRecords2014-15Final.pdf">SA1415-01</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Leadership team development</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-Andy-DAPAdmissionsandRegistration2014-15.pdf">SA1415-02</a>
                                       
                                    </td>
                                    
                                    <td>Andy Oguntola</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Admissions leadership</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3-Joe-DAPAtlasLab2014-2015.pdf">SA1415-03</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Atlas coordinator development</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4-Lisa2014-2015UnitPlansAtlasInformationSystemsLuminisfinal.pdf">SA1415-04</a>
                                       
                                    </td>
                                    
                                    <td>Lisa Stilke</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Atlas upgrade</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5-Lisa2014-2015UnitPlansAtlasDegreeWorksfinal.pdf">SA1415-05</a>
                                       
                                    </td>
                                    
                                    <td>Lisa Stilke</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Degree works integration</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6-Reyna-DAP2014-2015.pdf">SA1415-06</a>
                                       
                                    </td>
                                    
                                    <td>Reyna Rangel</td>
                                    
                                    <td>Admissions/Records Systems</td>
                                    
                                    <td>Admissions wesite redesign</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7-Tanisha-BTSUnitPlan2014_2015_Final.pdf">SA1415-07</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Carter</td>
                                    
                                    <td>Bridges to Success</td>
                                    
                                    <td>AA Increase in male enrollment</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8-Evelyn-EdCareerCenter2014-2015DAP.pdf">SA1415-08</a>
                                       
                                    </td>
                                    
                                    <td>Evelyn Lora-Santos</td>
                                    
                                    <td>Career Center</td>
                                    
                                    <td>NSE online instruction</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <p><a href="/documents/students/student-services/unit-plans/9-Amy-CollegeTransitions_Unit_plan_1_final.pdf">SA1415-09</a>
                                          
                                       </p>
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transitions</td>
                                    
                                    <td>Department enhancement</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/10-Amy-CollegeTransitions_Unit_plan_2_final.pdf">SA1415-10</a>
                                       
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transitions</td>
                                    
                                    <td>Improve enrollment process</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-Joe-DAPfinal2014-15.pdf">SA1415-11</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>Dean of Student East Campus</td>
                                    
                                    <td>Group advising</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12-Jill-finalOSCDeanofStudentsTeam14-15.pdf">SA1415-12</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Dean of Students Osceola Campus</td>
                                    
                                    <td>Group advising</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13-Linda-StudentAffairsWCUnitPlan2014-2015final.pdf">SA1415-13</a>
                                       
                                    </td>
                                    
                                    <td>Linda Herlocker</td>
                                    
                                    <td>Dean of Students West Campus</td>
                                    
                                    <td>Group adivsing</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-Joe-Cheryl-DAPCPAs2014-2015.pdf">SA1415-14</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>
                                       <p>Career Program Advisors</p>
                                    </td>
                                    
                                    <td>AS program videos</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15-Joe-Cheryl-DAPDOSWinterPark2014-2015.pdf">SA1415-15</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>Dean of Students Winter Park Campus</td>
                                    
                                    <td>NSE Co-Curricular</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16-Jacquelyn-Enrollment_Services_14-15_Unit_Plan_final.pdf">SA1415-16</a>
                                       
                                    </td>
                                    
                                    <td>Jacquelyn Thompson</td>
                                    
                                    <td>
                                       <p>Enrollment Services</p>
                                    </td>
                                    
                                    <td>Quality Assurance Program</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/17-ChrisUnitPlanFinancialAid14-15.pdf">SA1415-17</a>
                                       
                                    </td>
                                    
                                    <td>Christen Christensen</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Veteran Student Plan</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-DebLarewunitplanfinal1415.pdf">SA1415-20</a>
                                       
                                    </td>
                                    
                                    <td>Deb Larew</td>
                                    
                                    <td>Office for Students with Disability</td>
                                    
                                    <td>Standard Operating Procedure</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21-Edwin-DAPRecordsGraduation2014-15final.pdf">SA1415-21</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Transcript requests</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22-Edwin-DAPRecordsGraduation2014-15final.pdf">SA1415-22</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Student Records Training</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23-Edwin-DAPRecordsGraduation2014-15final.pdf">SA1415-23</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Records Office website</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/24-Edwin-DAPRecordsGraduation2014-15final.pdf">SA1415-24</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Student Messages for Transcript Requests</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/25-Erica-Stand_Test_UnitPlan_InductionCourse_final.pdf">SA1415-25</a>
                                       
                                    </td>
                                    
                                    <td>Erica Reese</td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Revise Student Induction Blackboard Course</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26-Erica-Stand_Test_UnitPlan_trainingandpolicies_final.pdf">SA1415-26</a>
                                       
                                    </td>
                                    
                                    <td>Erica Reese</td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Staff Training Materials</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27-Tracey2014-2015DAPfinal.pdf">SA1415-27</a>
                                       
                                    </td>
                                    
                                    <td>Tracey Olsen-Oliver</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Partnerships for Co-Curricular Experiences</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28-Sonya-FinalUnitPlans20142015.pdf">SA1415-28</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Assistant Vice President Student Affairs</td>
                                    
                                    <td>Exempt students advising system</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29-PenfoldNavarroStudentAdvisingPATHWAYSUnitPlan2014-2015.pdf">SA1415-29</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>Efficient Student Advising</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/30-PenfoldNavarroLifeMapMEPToolPATHWAYSUnitPlan2014-2015-Copy.pdf">SA1415-30</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>My educational plan improvement</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/31-PenfoldNavarroAdvisingResourcesPATHWAYSUnitPlan2014-2015.pdf">SA1415-31</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>Increase advising access</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/32-PenfoldNavarroImproveSystemsPATHWAYSUnitPlan2014-2015.pdf">SA1415-32</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>Mediated withdrawal process</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/33-PenfoldNavarroIncreaseparticipationPATHWAYSUnitPlan2014-2015.pdf">SA1415-33</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>Faculty participation in advising</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td height="29">
                                       <a href="/documents/students/student-services/unit-plans/34-PenfoldNavarroImproveabilityPATHWAYSUnitPlan2014-2015.pdf">SA1415-34</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Title III Pathways</td>
                                    
                                    <td>Improve advising systems</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/35-Nikky-TransitionsPlanning2014-15_final.pdf">SA1415-35</a>
                                       
                                    </td>
                                    
                                    <td>Niurka Ferrer</td>
                                    
                                    <td>Transition Planning</td>
                                    
                                    <td>Strengthen community outreach</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/36-KiaDEDepartmentalUnitActionPlan_201415_summary.pdf">SA1415-36</a>
                                       
                                    </td>
                                    
                                    <td>Kia Heard</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Improve timely communication to all stakeholders</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2013-14</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-ReneeDAPAdmissionsandRecords-2013-14final.pdf">SA1314-01</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>
                                       <strong>Provide the tools needed (formal training) to the supervisory staff</strong>&nbsp;to address
                                       legal and ethical concerns, establish systemic approaches to best practices, and enhance
                                       opportunities
                                       for building relationships. &nbsp;
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-AndyFinalDAP1314.pdf">SA1314-02</a>
                                       
                                    </td>
                                    
                                    <td>Andy Oguntola</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>
                                       <strong>Create partnerships with other units in student services</strong>&nbsp;to deliver an improved
                                       understanding of Admissions and Registration processes.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3-EricaStand_Test_2013-14_StudentInformation.pdf">SA1314-03</a>
                                       
                                    </td>
                                    
                                    <td>Erica Reese</td>
                                    
                                    <td>Assessment Coordinators</td>
                                    
                                    <td>
                                       <strong>To improve our systems</strong>&nbsp;to protect personally identifiable information.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4-EricaStand_Test_2013-14_CustomerService.pdf">SA1314-04</a>
                                       
                                    </td>
                                    
                                    <td>Erica Reese</td>
                                    
                                    <td>Assessment/Standardized Testing Leadership Team</td>
                                    
                                    <td>
                                       <p><strong>To advance our student experience</strong>&nbsp;by improving our office work flow and
                                          communication with students.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5-EricaStand_Test_2013-14_TechnologicalImprovements.pdf">SA1314-05</a>
                                       
                                    </td>
                                    
                                    <td>Erica Reese</td>
                                    
                                    <td>Assessment/Standardized Testing Leadership Team</td>
                                    
                                    <td>
                                       
                                       <p><strong>To advance our student experience</strong>&nbsp;by improving our testing technologies.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6-JoeDAPAtlasLab2013-2014.pdf">SA1314-06</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>Atlas Access Lab</td>
                                    
                                    <td>
                                       <strong>Ensure ll Atlas Access Labs staff are knowledgeable and adequately trained:&nbsp;</strong>The
                                       Atlas Access Labs are the first point of contact on-campus for students who need to
                                       complete an
                                       education plan, check their email, or complete any other function within the Atlas
                                       portal.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7LisaS-2013-2014UnitPlansAtlasInformationSystemsDegreeWorks8-21-2014.pdf">SA1314-07</a>
                                       
                                    </td>
                                    
                                    <td>Lisa Stilke</td>
                                    
                                    <td>Atlas Information Systems (Degree Works)</td>
                                    
                                    <td>
                                       <strong>Assist with the Implementation of Degree Works:</strong>&nbsp;The Atlas Information Systems team
                                       will assist with the implementation of Degree Works as the new Student Education Planning
                                       tool.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8LisaS-2013-2014UnitPlansAtlasInformationSystemsDegreeWorks8-21-2014.pdf">SA1314-08</a>
                                       
                                    </td>
                                    
                                    <td>Lisa Stilke</td>
                                    
                                    <td>Atlas Information Systems (Luminis 5.1)</td>
                                    
                                    <td>
                                       <strong>Implement the upgrade from Luminis 4 to Luminis 5.1:</strong>&nbsp;The Atlas Information Systems
                                       team will partner with OIT and other constituents to implement the upgrade.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/9-TanishaBTS2013_2014.pdf">SA1314-09</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Bridges</td>
                                    
                                    <td>Bridges to Success</td>
                                    
                                    <td>
                                       <strong>Increase the number of African American males who are accepted into the program&nbsp;</strong>by
                                       working with the Transitions coordinators to visit the high schools, and reestablished
                                       partnerships
                                       with community organizations, and churches to spread the word to groups that serve
                                       the targeted
                                       population.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/10-JillDAPCareerCenter1314.pdf">SA1314-10</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Career Centers</td>
                                    
                                    <td>
                                       <p><strong>Create meta-major informational sheets</strong>&nbsp;to include academic course
                                          requirements/recommendations and career prospects.&nbsp;
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-CherylWEAVECPA2013-14.pdf">SA1314-11</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Program Advisors</td>
                                    
                                    <td>
                                       <strong>Create a strong Career Program Advisor team and support system:&nbsp;</strong>Get to know each
                                       other and feel comfortable reaching out to other team members for assistance as needed.
                                       This will help
                                       build depth of knowledge in the work as well as provide individual support for the
                                       Career Program
                                       Advisors.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12-AmyCollegeTransitions_13-14_Unitplan_2.pdf">SA1314-12</a>
                                       
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transition Programs</td>
                                    
                                    <td>
                                       <strong>Enhance and improve the enrollment management process</strong>&nbsp;to better serve students and
                                       facilitate a more seamless, student-friendly experience that positively impacts enrollment
                                       goals.&nbsp;
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13-AmyCollegeTransitions_13-14_Unit_plan_1.pdf">SA1314-13</a>
                                       
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transition Programs</td>
                                    
                                    <td>
                                       <strong>Enhance and improve the performance of College Transitions units</strong>&nbsp;through internal
                                       and external partnerships; training; and process analysis/redesign to better serve
                                       Valencia and the
                                       community.&nbsp;
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-LindaUnitPlansTemplate2013-2014End-of-year.pdf">SA1314-14</a>
                                       
                                    </td>
                                    
                                    <td>Linda Herlocker</td>
                                    
                                    <td>Dean of Students&nbsp;<br> West
                                       
                                    </td>
                                    
                                    <td>
                                       <strong>Student Affairs Process Flow:</strong>&nbsp;Reeingeer the process flow of students through the
                                       student affairs divisions tol increase efficiency and effectiveness.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15-JoeDAPDOSEast2013-2014.pdf">SA1314-15</a>
                                       
                                    </td>
                                    
                                    <td>Joe Sarrubbo</td>
                                    
                                    <td>Dean of Students East</td>
                                    
                                    <td>
                                       <p><strong>Evaluate current space needs on second floor of building 5:&nbsp;</strong>Make changes to
                                          better serve students on East Campus.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16-JillWEAVEOSCDeanofStudentsTeam13-14.pdf">SA1314-16</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Dean of Student Osceola</td>
                                    
                                    <td>
                                       <p><strong>Move into newly designed and renovated space, increase capacity (staff and space),
                                             and
                                             implement software systems</strong>&nbsp;to serve students in a welcoming/personal, professional, and
                                          efficient manner.&nbsp;
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/17-CherylWEAVEWP2013-14.pdf">SA1314-17</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students Winter Park</td>
                                    
                                    <td>
                                       <strong>Maintain designated populations of students on P2 through Black Board:</strong>&nbsp;Students in
                                       the probation 2 pilot will have their course load and GPAs compared to the non-pilot
                                       group
                                       college-wide. We will track academic standing at the end of each term to determine
                                       student success.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/18Cherylfinal2013-14.pdf">SA1314-18</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Student Winter Park</td>
                                    
                                    <td>
                                       <strong>Expand online chat to make it easier for students to find and increase usage:&nbsp;</strong>We
                                       will track chat usage for the time slots offered and move sessions to high demand
                                       time or offer
                                       service during regular student service hours.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/19-KiaDepartmentalUnitActionPlan_201314_revsummary.pdf">SA1314-19</a>
                                       
                                    </td>
                                    
                                    <td>Kiawania Heard</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>
                                       <strong>Refresh the Dual Enrollment program processes</strong>&nbsp;and procedures to further clarify and
                                       increase the efficiency, quality and effectiveness of communication with internal
                                       and external
                                       audiences or key stakeholders.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-finalEnrollment_Services_13-14_Unit_Plan.pdf">SA1314-20</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Monica Negron/</p>
                                       
                                       <p>LaToya Ward</p>
                                       
                                    </td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                    <td>
                                       <p><strong>Improve student/caller experience received via telephone</strong>&nbsp;Aspect and VortalSoft
                                          for prospect and current students calling into Enrollment Services.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21ChrisUnitPlan2013-2014FinancialAidDefault.pdf">SA1314-21</a>
                                       
                                    </td>
                                    
                                    <td>Chris Christensen</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>
                                       <strong>Create a comprehensive default management plan for the college.&nbsp;</strong>&nbsp;Reduce student
                                       default and increase financial literacy of our student population.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22ChrisUnitPlan2013-2014Veterans.pdf">SA1314-22</a>
                                       
                                    </td>
                                    
                                    <td>Chris Christensen</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>
                                       <strong>Contribute to a College wide Veterans Services model.</strong>To consolidate services,
                                       funding, and activities for Veteran students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23-EdwinDAPRecordsGraduation2013-14FinalReportJune2014.pdf">SA1314-23</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>
                                       <p><strong>Continue the work of improving the course articulation practices and procedures</strong>&nbsp;by
                                          creating better descriptors for students, staff and the college transcript.&nbsp;&nbsp;
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/24-DebfinalUnitPlans2013-2014_OSD_SJ.pdf">SA1314-24</a>
                                       
                                    </td>
                                    
                                    <td>Deb Larew</td>
                                    
                                    <td>Office for Students with Disabilities</td>
                                    
                                    <td>
                                       <strong>Increase retention and graduation of students registered with OSD:</strong>&nbsp;Building upon
                                       feedback from staff and faculty survey, the OSD strives to increase retention and
                                       graduation of
                                       students registered with the OSD through enhanced advising and targeted communication
                                       strategies.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/25-PenfoldNavarroPATHWAYSUnitPlan2013-2014.pdf">SA1314-25</a>
                                       
                                    </td>
                                    
                                    <td>Cathy Penfold Navarro</td>
                                    
                                    <td>Pathways Title III</td>
                                    
                                    <td>
                                       
                                       <p><strong>Increase efficiency and capability of student advising:</strong></p>
                                       
                                       <p>(target group: pre-nursing and business transfer students on West Campus)</p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26-Sonya2013-14finalunitplansNSE1314.pdf">SA1314-26</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>
                                       
                                       <table class="table ">
                                          
                                          <tbody>
                                             
                                             <tr>
                                                
                                                <td width="977" valign="top">
                                                   <p><strong>Lead the Quality Enhancement Plan Co-Curricular
                                                         Taskforce</strong>&nbsp;in piloting the Co-Curricular Certificate Program at the Lake Nona Campus.
                                                      
                                                   </p>
                                                </td>
                                                
                                             </tr>
                                             
                                          </tbody>
                                          
                                       </table>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27-Sonya-21032014unitplansDW.pdf">SA1314-27</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>
                                       <strong>Develop and education plan using DegreeWorks software</strong>: Develop an plan that is
                                       useful to students and allows students to develop an accurate degree plan for any
                                       Valencia degree.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28-Tracey2013-2014DAP.pdf">SA1314-28</a>
                                       
                                    </td>
                                    
                                    <td>Tracey Olsen-Oliver</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>
                                       
                                       <table class="table ">
                                          
                                          <tbody>
                                             
                                             <tr>
                                                
                                                <td width="977" valign="top">
                                                   <p><strong>Utilize Student Development’s four Core
                                                         Components:&nbsp;</strong>Leadership, Education, Community Involvement, and Personal Engagement.
                                                      Devise, develop, enhance and implement programs/activities/services that provide content
                                                      and
                                                      value to the educational experience and that support the QEP’s co-curricular engagement
                                                      focus.
                                                   </p>
                                                </td>
                                                
                                             </tr>
                                             
                                          </tbody>
                                          
                                       </table>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29-ElishaTSIC_13-14_Unit_Plan.pdf">SA1314-29</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzelez</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>
                                       <strong>Provide a set of sequential College Planning experiences for Orange County Take Stock
                                          in
                                          Children students</strong>&nbsp;to provide transition support beginning in the senior year of high school
                                       and lasting into their freshman&nbsp; year of college.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/30-NikkyUnitPlansTransitionsPlanning2013-2014COMPLETE.pdf">SA1314-30</a>
                                       
                                    </td>
                                    
                                    <td>Niurka Ferrer</td>
                                    
                                    <td>Transition Planning</td>
                                    
                                    <td>
                                       <strong>Develop a comprehensive communication plan</strong>&nbsp;targeting 9th – 12th grade high school
                                       students that considers the student experience and needs as a whole as they transition
                                       from high
                                       school to college.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2012-13</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-Renee2012-13UnitPlan-1.pdf">SA1213-01</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Bachelors Program Processes:&nbsp;Continue to tweak policies, procedures and protocols
                                       associated with
                                       upper level certificates. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-Renee2012-13UnitPlan-2.pdf">SA1213-02</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Support and implement new processes:&nbsp;Distance Learning/Financial Aid Transient Students;
                                       Degree
                                       Works, CAPP and Catalog, revision of MEP and Banner 9. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3-Renee2012-13UnitPlan-3.pdf">SA1213-03</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Admissions and Records Procedure Manual:&nbsp;Produce Department Procedures Manual. (final)</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4-SonjaBoles-2012-2013WEAVEDAP-SONJA.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/4-Cheryl-DOS-WPC11-12final.pdf">-04</a>
                                       
                                    </td>
                                    
                                    <td>Sonja Boles</td>
                                    
                                    <td>Admissions and Registration</td>
                                    
                                    <td>Admissions Processing:&nbsp;Perform a business process analysis of specific Admissions
                                       and Records
                                       documented procedures and practices. (not complete)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5-EricaStandardized_Testing_12-13_Unit_Plan.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/5-Cheryl-CPA2011-12.pdf">-05</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Cynthia Cerrato/</p>
                                       
                                       <p>Erica Reese</p>
                                       
                                    </td>
                                    
                                    <td>Assessment/Standardized Testing</td>
                                    
                                    <td>Entry Test Preparation:&nbsp;Lead in the development of a mandatory pre-placement testing
                                       preparation
                                       system to support student success. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6-Erica-Standardized_Testing_12-13_Unit_Plan__2.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/6-Chanda-UnitPlans2011-2012.pdf">-06</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Cynthia Cerrato/</p>
                                       
                                       <p>Erica Reese</p>
                                       
                                    </td>
                                    
                                    <td>Assessment/Standardized Testing</td>
                                    
                                    <td>Partnership Building:&nbsp;Strengthen our collaborations with other campus-based units,
                                       both within
                                       Student Affairs and other areas of the college. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7-Joe-DAPAtlasLab2012-2013.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/7-Donwing_AVP_1112.pdf">-07</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Linda Vance/</p>
                                       
                                       <p>Joe Sarrubbo</p>
                                       
                                    </td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Atlas Lab Expenses:&nbsp;Financial tracking of lab expenses throughout the year to focus
                                       on staying
                                       within our budget. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8-SonyaforLizcompletedunitlplan12013.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/8-Tanisha-WEAVEForm_Bridges_1112.pdf">-08</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Liz Gangemi/</p>
                                       
                                       <p>Lisa Stilke</p>
                                       
                                    </td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Call Center Phone System:&nbsp;Assist with implementation of the new Aspect Phone System
                                       for Enrollment
                                       Services and Student Helpdesk. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/9-SonyaforLizcompletedunitplans22013.pdf">SA1213-09</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>New MEP:&nbsp;Assist with the implementation of Degree Works as our new Student Educational
                                       Planning
                                       tool. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/10-Tanisha-Bridges_12-13_Unit_Plan.pdf">SA1213-10</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Carter</td>
                                    
                                    <td>Bridges to Success</td>
                                    
                                    <td>Student Recruitment:&nbsp;Increase number of African American males served by Bridges to
                                       Success program.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-JillWEAVECareerCenters12-13.pdf">SA1213-11</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Career Centers - Collegewide</td>
                                    
                                    <td>Career Preparation:&nbsp;Promote resume and cover letter writing preparation for meaningful
                                       advisement to
                                       support LifeMap stage Graduation Transition. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12-CherylCPAs2012-13.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/12-LindaV.-ECStudentServices.pdf">-12</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Program Advisors</td>
                                    
                                    <td>Expiring Catalog Process:&nbsp;Continue partnership with Graduation and Records to refine
                                       the system for
                                       expiring catalog years and moving students to a new catalog year. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13-GraceTransitionsPlanning_12_13_Unit_Plan.pdf">SA1213-13</a>
                                       
                                    </td>
                                    
                                    <td>Grace Acevedo</td>
                                    
                                    <td>College Transitions</td>
                                    
                                    <td>Partnership Building:&nbsp;Develop and maintain external and internal partnerships (Bridges,
                                       Career
                                       Pathways, Take Stock, HSF, SACAC, Girl Scouts, Orange County Adult Learners, etc.)
                                       to promote Valencia
                                       and increase number of prospect students, which will lead to an increase in enrollment.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-AmyCollegeTransitions_Unitplan2012-13.pdf">SA1213-14</a>
                                       
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transitions</td>
                                    
                                    <td>Enhance performance of College Transitions:&nbsp;Enhance and improve the performance of
                                       College
                                       Transitions units through internal and external partnerships; training; and process
                                       analysis/redesign
                                       to better serve Valencia and the community. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15AmyCollegeTransitions_Unitplan2012-13_1.pdf">SA1213-15</a>
                                       
                                    </td>
                                    
                                    <td>Amy Kleeman</td>
                                    
                                    <td>College Transitions</td>
                                    
                                    <td>Enhance and improve Enrollment Management:&nbsp;Enhance and improve the enrollment management
                                       process to
                                       better serve students and facilitate a more seamless, student-friendly experience
                                       that positively
                                       impacts enrollment goals. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16-Joe-DAPDOSEast2012-2013.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/16-EdwinRecordsandGraduation2011-12.pdf">-16</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Linda Vance/</p>
                                       
                                       <p>Joe Sarrubbo</p>
                                       
                                    </td>
                                    
                                    <td>Dean of Students EC</td>
                                    
                                    <td>Campus Partnerships:&nbsp;Focus on building a stronger relationship with our SLS team and
                                       the newly
                                       created Academic Success Department. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/17-JillWEAVEOSCDeanofStudentsTeam12-13.pdf">SA1213-17</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Dean of Students OSC</td>
                                    
                                    <td>Campus Visibility:&nbsp;Promote department services through a variety of methods aimed
                                       to increase
                                       awareness of our services to campus faculty and staff outside of Student Services.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/18-Linda-Ty-Weave2012-13Form-Partnership.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/18-EdwinGrad-Records2011-12DAP.pdf">-18</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Ty Johnson/</p>
                                       
                                       <p>Linda Herlocker</p>
                                       
                                    </td>
                                    
                                    <td>Dean of Students WC</td>
                                    
                                    <td>Campus Partnerships:&nbsp;Establish academic department and program liaisons through assignment
                                       of
                                       student services staff for contacts and reporting. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/19-CherylWEAVEWP2012-13.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/19-EdwinGrad-Records2011-12DAP.pdf">-19</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students WPC</td>
                                    
                                    <td>Academic Probation Program:&nbsp;Increase number and percentage of students on academic
                                       probation
                                       maintaining current academic standing or returning to good standing. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-KiaDual_Enrollment_2012-13_Unit_Plan.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/20-SonjaBolesDAP2011-2012.pdf">-20</a>
                                       
                                    </td>
                                    
                                    <td>Kiawania Heard</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Dual Enrollment Processes:&nbsp;Refresh the Dual Enrollment program processes and procedures
                                       to further
                                       clarify and increase the efficiency, quality and effectiveness of communication with
                                       internal and
                                       external audiences. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21-Derek-LaToya-EnrollmentServices_12-13_unit_plan.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/21-SonjaBolesDAP2011-2012.pdf">-21</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Derek Harris, LaToya Ward,</p>
                                       
                                       <p>Amy Kleeman</p>
                                       
                                    </td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                    <td>Call Center Systems:&nbsp;Improve experience through the implementation of Aspect and VortalSoft
                                       for
                                       prospect and current students calling into Valencia. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22-Chris-UnitPlan2012-2013-FWS.pdf">SA1213-22</a>
                                       
                                    </td>
                                    
                                    <td>Chris Christensen</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Work Study Plan:&nbsp;Develop a comprehensive FWS plan to serve the best interests of FWS
                                       students and
                                       their supervisors. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23-Chris-UnitPlan2012-2013.pdf">SA1213-23</a>
                                       
                                    </td>
                                    
                                    <td>Chris Christensen</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Default Aversion/Financial Literacy:&nbsp;Educate students for increased financial awareness
                                       and
                                       preparedness. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/24-Ty-Linda-2013-14DAP.pdf">SA1213-24</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson / Linda Herlocker</td>
                                    
                                    <td>Dean of Students WC</td>
                                    
                                    <td>Campus Partnerships:&nbsp;Engage the department in support of the West Campus Plan (12-13)
                                       by developing
                                       and reinforcing campus partnerships with academic departments and other support areas.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <p><a href="/documents/students/student-services/unit-plans/25EdwinDAPRecordsGraduation2012-13RevisedMay2013.pdf">SA1213-25</a>
                                          
                                       </p>
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Course Articulation:&nbsp;Continue the work of improving the course articulation practices
                                       and procedures
                                       by creating better descriptors for students, staff and the college transcript. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26EdwinDAPRecordsGraduation2012-13RevisedMay2013-Copy.pdf">SA1213-26</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Graduation Finale:&nbsp;Develop, implement and assess the new Graduation Information Sessions
                                       (aka GRAD
                                       FINALE). (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27EdwinDAPRecordsGraduation2012-13RevisedMay2013-Copy.pdf">SA1213-27</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Paperless Records:&nbsp;Remove physical records from Lektriever at West Campus by January
                                       2013. Continue
                                       implementation of plan to become a paperless campus through use of BDMS. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28EdwinDAPRecordsGraduation2012-13RevisedMay2013-Copy.pdf">SA1213-28</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Academic Suspension Process:&nbsp;Revamp the Academic Suspension process to improve communication
                                       and
                                       deletion of student schedules that have been placed on academic suspension. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29-Linda-Ty-DAP-InternationalStudentServices2012-2013.pdf">SA1313-29</a>
                                       
                                    </td>
                                    
                                    <td>Tyron Johnson</td>
                                    
                                    <td>International Student Services College-wide</td>
                                    
                                    <td>International Staff Training:&nbsp;Continue formal training for PDSO's (Principal Designated
                                       School
                                       Officials) and DSO's (Designated School Officials) working with international student
                                       admissions,
                                       enrollment and compliance to improve quality and performance through SEVIS and SEVP.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/30-Deb1213Unitplans_review_OSD.pdf">SA1213-30</a>
                                       
                                    </td>
                                    
                                    <td>Debbie Larew</td>
                                    
                                    <td>Office for Students with Disabilities</td>
                                    
                                    <td>Academic Success:&nbsp;Improve the success of students with disabilities through increased
                                       outreach and
                                       training to Academic Affairs and students and families of students with disabilities.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/31-Sonya-UnitPlan2012-2013.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/31-BradDAP2011-122.pdf">-31</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>Veterans Institute:&nbsp;Offer a Veterans Institute for Valencia faculty and staff. (final)</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td height="90">
                                       <a href="/documents/students/student-services/unit-plans/32-TraceyOO-2012-2013DAP.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/32-BradDAP2011-122.pdf">-32</a>
                                       
                                    </td>
                                    
                                    <td>Tracey Olsen-Oliver</td>
                                    
                                    <td>Student Development Collegewide</td>
                                    
                                    <td>Student Leadership Program Development:&nbsp;Develop a Student Leadership Program model
                                       that will
                                       encompass program learning outcomes, program components, assessment plan and co-curricular
                                       transcript.
                                       (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/33-ElishaTake_Stock_12-13_Unit_Plan.pdf">SA1213</a><a href="/documents/students/student-services/unit-plans/33-Ty-DAP2011-12StudentServices.pdf">-33</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>New College Student Program:&nbsp;Provide a set of sequential college planning experiences
                                       for Valencia
                                       Volunteers students and mentors to provide transition support beginning in the junior
                                       year of high
                                       school and lasting into the freshman year of college.(final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/34-CherylWEAVEWP2012-13.pdf">SA1213-34</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students WPC</td>
                                    
                                    <td>Partner with Acadecic Affairs to conduct LifeMap and Student Services training to
                                       campus
                                       staff:&nbsp;Increase undestanding of the LifeMap system. (final)
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2011-12</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-CynthiaDAP2011-2012PlacementSystems.pdf">SA1112-01</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Assessment</td>
                                    
                                    <td>Collaborate with academic deans, faculty and other stakeholders to develop a plan
                                       to transition to
                                       new EAP and mathematics placement systems.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-Cynthia-DAP2011-2012StaffDevelopment.pdf">SA1112-02</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Assessment</td>
                                    
                                    <td>Implement a comprehensive professional development plan for all Assessment staff.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3-Deb-2011-2012Unitplans_result.pdf">SA1112-03</a>
                                       
                                    </td>
                                    
                                    <td>Debbie Larew</td>
                                    
                                    <td>Office for Students with Disabilities</td>
                                    
                                    <td>Conduct a systematic assessment of the processes of the Office for Students with Disabilities
                                       (OSD).
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4-Cheryl-DOS-WPC11-12final.pdf">SA1112-04</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students, Winter Park Campus</td>
                                    
                                    <td>Partner with developmental education faculty to promote educational planning.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5-Cheryl-CPA2011-12.pdf">SA1112-05</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Program Advisors</td>
                                    
                                    <td>Partner with Graduation and Records to develop a streamlined system for expiring catalog
                                       years and
                                       moving students to a new catalog year.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6-Chanda-UnitPlans2011-2012.pdf">SA1112-06</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Complete a collegewide Student Leader Program Review that will assist with the creation
                                       of learning
                                       outcomes and program components.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7-Donwing_AVP_1112.pdf">SA1112-07</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>AVP College Transitions</td>
                                    
                                    <td>Strengthen ongoing area partnerships and relationships with school districts, non-profit
                                       agencies,
                                       cities and colleges.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8-Tanisha-WEAVEForm_Bridges_1112.pdf">SA1112-08</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Carter</td>
                                    
                                    <td>Bridges to Success</td>
                                    
                                    <td>Increase number of students served by the Birdges to Success program.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/9-HeardDual_1112.pdf">SA1112-09</a>
                                       
                                    </td>
                                    
                                    <td>Kiawania Heard</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Refresh the Dual Enrollment website to imporove efficiency and effectiveness of communication
                                       with
                                       internal and external audiences.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/10ElishaTSIC2012WEAVEForm_TSIC.pdf">SA1112-10</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez-Bonnewitz</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>Provide a set of sequential College Planning experiences for Take Stock in Children
                                       students and
                                       mentors to provide transition support.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-LindaV.-Atlas.pdf">SA1112-11</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Promote LifeMap by increasing visibility/presensce/purpose of ATLAS labs on each campus.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12-LindaV.-ECStudentServices.pdf">SA1112-12</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>East Campus Student Services</td>
                                    
                                    <td>Establish a strong connection with our academic colleagues, collaborate more closely
                                       with our campus
                                       leadership team, and build pathways between departments on campus.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13-S.Joseph-plans2011-2012.pdf">SA1112-13</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>Provide immediate resources for Valencia students who are sturggling academically
                                       and/or personally.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-S.Joseph-plans22011-2012.pdf">SA1112-14</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>Review the current educational planning tool (MEP) and determine if it is meeting
                                       the needs of
                                       students. If not, then secure a solution.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15-Cheryl-DOSWP2011-12.pdf">SA1112-15</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students Winter Park Campus</td>
                                    
                                    <td>Partner with Academic Affairs to conduct LifeMap training to campus staff.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16-EdwinRecordsandGraduation2011-12.pdf">SA1112-16</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Improve the course articulation practices and procedures by creating better descriptors
                                       for
                                       students, staff and the college transcript.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/17-EdwinRecordsandGraduation2011-12.pdf">SA1112-17</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Research vendors that can enhance the quality, timeliness and efficiency of printing
                                       and mailing the
                                       student diplomas.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/18-EdwinGrad-Records2011-12DAP.pdf">SA1112-18</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>
                                       <p>Continue implementation of 2010 "Banner Xtender Solutions Electronic Records" to become
                                          a
                                          paperless campus.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/19-EdwinGrad-Records2011-12DAP.pdf">SA1112-19</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Research the automation of Change of Major for Students through Self Service and consider
                                       the
                                       implementation depending on outcomes from research.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-SonjaBolesDAP2011-2012.pdf">SA1112-20</a>
                                       
                                    </td>
                                    
                                    <td>Sonja Boles-Melvin</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>
                                       <p>Increase the efficiency of the International Admissions process.</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21-SonjaBolesDAP2011-2012.pdf">SA1112-21</a>
                                       
                                    </td>
                                    
                                    <td>Sonja Boles-Melvin</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Establish benchmarks from Admissions and Records staff productivity report.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22-SonjaBolesDAP2011-2012.pdf">SA1112-22</a>
                                       
                                    </td>
                                    
                                    <td>Sonja Boles-Melvin</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Research feasibility of automating the Dual Enrollment process.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23-Renee-AdmissionsandRecords2011-12.pdf">SA1112-23</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Revamp Curriculum Advising and Program Planning Instruction (CAPP).</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/24-Jill-OSCStudentServices11-12.pdf">SA1112-24</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students Services - Osceola Campus</td>
                                    
                                    <td>Implement serveral strategies to improve effectiveness and capability.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/25-Jill-CareerCenters11-12.pdf">SA1112-25</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Career Centers - Collegewide</td>
                                    
                                    <td>Increase awareness of STEM majors and connection to related career fields.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26-Chanda-UnitPlans2011-2012.pdf">SA1112-26</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development - CAS Program Review</td>
                                    
                                    <td>Conduct a Student Development Program Review using the Council for the Advancement
                                       of Standards
                                       (CAS) in Higher Education.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27-LizDAPFormGoal12011-2012.pdf">SA1112-27</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Fully integrate the Banner Event Management for the use with scheduling and tracking
                                       Orientation
                                       Sessions.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28-LizDAPFormGoal22011-2012yearend.pdf">SA1112-28</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Research all available options for the replacement or improvement of "My Education
                                       Plan" tool in
                                       Atlas.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29-Jessica-ManageIncreasedCallVolume.pdf">SA1112-29</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Enrollment Services / Communication</td>
                                    
                                    <td>Improve experience for prospective and current students calling Valencia during peak
                                       registration.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/30-Jessica-DevelopIntlEnrollmentPlan.pdf">SA1112-30</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Develop a plan to increase the number of international students enrolled at Valencia.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/31-BradDAP2011-122.pdf">SA1112-31</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Office of Student Financial Assistance</td>
                                    
                                    <td>Enhance the students experience with the Satisfactory Academic Progress (SAP) policy
                                       and process.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="90">
                                       <a href="/documents/students/student-services/unit-plans/32-BradDAP2011-122.pdf">SA1112-32</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Veteran's Services</td>
                                    
                                    <td>
                                       <p>Enhance the web site and the Satisfactory Academic Requirements and notification for
                                          Veteran
                                          students.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/33-Ty-DAP2011-12StudentServices.pdf">SA1112-33</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>Student Services-West Campus</td>
                                    
                                    <td>
                                       <p>Work collaboratively with the West Campus President, Campus Leadership Team, Faculty,
                                          staff and
                                          students to identify, develop and implement a new campus organizational plan.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/34-Ty-DAP2011-12-InternationalStudentServices.pdf">SA1112-34</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>International Student Services</td>
                                    
                                    <td>Development and identify training opportunities for staff working with international
                                       students.
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2010-11</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-AC20102011finalSonya.pdf">SA1011-01</a>
                                       
                                    </td>
                                    
                                    <td>
                                       <p>Sonya Joseph &amp; Answer Center Managers</p>
                                    </td>
                                    
                                    <td>Answer Center</td>
                                    
                                    <td>Establish, through a collaborative process, a guideline for standard practices for
                                       welcoming and
                                       working with students int he Answer Center.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-finalNSO2010-2011Sonya.pdf">SA1011-02</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Orientation</td>
                                    
                                    <td>Redesign the online New Student Orientation to increase student learning of the registration
                                       process, the use of Atlas, and the education planning process.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3-2010-2011plansfinalSonya.pdf">SA1011-03</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs Administration</td>
                                    
                                    <td>Develop Student Services support for Valencia's Collegiate Academy at Lake Nona.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4-Jill-CherylWEAVECDS2010-11final.pdf">SA1011-04</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson/Jill Szentmiklosi</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                    <td>Create an informational interviewing database with business and industry professionls
                                       who will
                                       conduct an interview with our students who plan to work in their field.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5WEAVEWP2010-11-CherylRobinson.pdf">SA1011-05</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students, Winter Park Campus</td>
                                    
                                    <td>Refine the Early Alert System (EAS) to identify students with non-success behaviors
                                       and provide an
                                       intervention to prevent withdrawal.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6-WEAVEFormOSD2010_2011-D.Larew.pdf">SA1011-06</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Jillian Szentmiklosi/</p>
                                       
                                       <p>Deboarah Larew</p>
                                       
                                    </td>
                                    
                                    <td>Office for Students with Disabilities (OSD)</td>
                                    
                                    <td>Inform faculty about OSD policies and procedures and provide resources and strategies
                                       to assist in
                                       working with students with disabilities in their classrooms.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7-BradFinancialAid-2010-11.pdf">SA1011-07</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Offer financial aid to high school graduates prior to receiving their official final
                                       high school
                                       transcripts to assist them in planning for college.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8-BradVeterans-2010-11.pdf">SA1011-08</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Veteran's Affairs</td>
                                    
                                    <td>Enhance the online Veteran's benefits certification request and email notification
                                       to the Veteran
                                       students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/9-WEAVEForm_AVP_BridgesLindaD..pdf">SA1011-09</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions Administration</td>
                                    
                                    <td>Increase the number of students served by the Bridges to Success program.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/10-WEAVEForm_AVP_CallCenterLindaD..pdf">SA1011-10</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions Administration</td>
                                    
                                    <td>Improve the experience students have when they telephone the college for information
                                       about
                                       enrollment.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-WEAVEForm_DE1_1011Jackie.pdf">SA1011-11</a>
                                       
                                    </td>
                                    
                                    <td>Jackie Cole</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Explore options for the future organization and delivery of dual enrollment as an
                                       Early College
                                       experience.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12-WEAVEForm_DE2_1011Jackie.pdf">SA1011-12</a>
                                       
                                    </td>
                                    
                                    <td>Jackie Cole</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Imporove efficiency and effectiveness in communicating with internal and external
                                       audiences.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13-WEAVEForm_CROP1011.pdf">SA1011-13</a>
                                       
                                    </td>
                                    
                                    <td>Kiawania Heard</td>
                                    
                                    <td>College Reach Out Program</td>
                                    
                                    <td>Make sure 100% of CROP high school graduates enroll in college. Increase the number
                                       of students who
                                       participate in accelerated mechanisms and who score college-ready on the CPT test.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-WEAVEForm_TSICElisha.pdf">SA1011-14</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez-Bonnewitz</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>Develop the belief that college is possible by providing mentoring and scholarship
                                       assistance from
                                       7th grade through college for students who are at risk for dropping out of high school
                                       or not
                                       attending college.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15-ATLAS2010-11UnitplansWEAVEFormL.Vance.pdf">SA1011-15</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Use reports from the ATLAS labs to better understand students' needs and to improve
                                       the level of
                                       service and efficiency they receive.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16EastDOS2010-11UnitplansL.Vance.pdf">SA1011-16</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Student Services East Campus</td>
                                    
                                    <td>Make sure staff is located in areas that support their work with students. Find appropriate
                                       workspace for all staff that will work with future growth.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/17DAP2010-2011EPRA-Cynthia.pdf">SA1011-17</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Assessment</td>
                                    
                                    <td>Ensure a smooth collaboration with Orange and Osceola Schools in the continued implementation
                                       of
                                       Expanded Postsecondary Readiness Assessment (EPRA) testing.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/18DAP2010-2011PERT-Cynthia.pdf">SA1011-18</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Assessment</td>
                                    
                                    <td>Ensure Valencia's smooth transition to the new placement test - the Postsecondary
                                       Education
                                       Readiness Test (PERT), to support student success.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/19-WEAVEGoal12010-2011FinalLiz.pdf">SA1011-19</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Lead and conduct college-wide training for "New Banner Security" for supervisors.
                                       Coordinate
                                       college-wide refresh training on Banner after new security is in effect.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-WEAVEGOAL22010-2011-finalLiz.pdf">SA1011-20</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Fully integrate new functional tech position into International Project.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21-Edwin2010-11FinalDAP.pdf">SA1011-21</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Develop a process that will assist and ensure all requirements of the degree are accounted
                                       for prior
                                       to programmatic posting of the degrees awarded.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22-Edwin2010-11FinalDAP.pdf">SA1011-22</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Continue implementation plan to become a paperless campus through BDMS and develop
                                       training
                                       workshops for college staff and new hires.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23DepartmentActionPlan2010-11AdvisingFinalReport-Ty.pdf">SA1011-23</a>
                                       
                                    </td>
                                    
                                    <td>Tyron Johnson</td>
                                    
                                    <td>Dean of Students, West Campus</td>
                                    
                                    <td>Develop a formal training program for professional development of Student Services
                                       Staff by 6/30/11.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/25-Edwin2010-11FinalDAP.pdf">SA1011-25</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Re-engineer the articulation process by creating better descriptors for the student
                                       transcript.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26-Edwin2010-11FinalDAP.pdf">SA1011-26</a>
                                       
                                    </td>
                                    
                                    <td>Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Provide refresher training for Student Affairs staff and new hires related to Records
                                       utilizing:
                                       BANNER (BDMS).
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27WEAVEForm2010-2011Revised2-Chanda.pdf">SA1011-27</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres Miller</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Establish a collegewide Student Leader Program recruitment/hiring timeline, student
                                       leader retreat
                                       learning outcomes, training outcomes and modify student leader award criteria.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28WEAVEForm2010-2011Revised-Chanda.pdf">SA1011-28</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres Miller</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Create a new Student Activities Budget system that ensures student activity dollars
                                       are
                                       appropriately administered.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29-JMoralesDAP2010-11final-TransitionServices.pdf">SA1011-29</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Improve image of marketing materials to best identify with prospective students by
                                       highlighting what
                                       Valencia has to offer.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td height="70">
                                       <a href="/documents/students/student-services/unit-plans/30-JMoralesDAP2010-11final-Enrollment.pdf">SA1011-30</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                    <td>Create a better student experience for callers who reach Enrollment Services.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td height="150">
                                       <a href="/documents/students/student-services/unit-plans/31-AdmissionsandRecords-Renee1011.pdf">SA1011-31</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>
                                       <p>Assist students in planning and implementing a program of study that will take them
                                          from entry
                                          level to completion of the bachelor's degree while remaining a vital part of the Valencia
                                          community.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/32DepartmentActionPlan2010-11-FinalReportISS-Ty.pdf">SA1011-32</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td>Identify, develop and implement training/learning opportunities to enhance staff's
                                       skills, knowledge
                                       and understanding of federal and institutional policies, procedures and technology.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/33-LisaStilke2010-2011FinalDAP.pdf">SA1011-33</a>
                                       
                                    </td>
                                    
                                    <td>Lisa Stilke</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                    <td>Increase staff knowledge and functionality in order to better serve applicants and
                                       students.
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2009-10</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/1-AdmissionandRecords2009DAP1-Final-ReneeforLisaStilke.pdf">SA0910-01</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Renee Simpson,</p>
                                       
                                       <p>Lisa Stilke</p>
                                       
                                    </td>
                                    
                                    <td>Admissions &amp; Records</td>
                                    
                                    <td>Applicant and International Student Improvements</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/2-AdmissionandRecords2009DAP1-Final-Renee.pdf">SA0910-02</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson</td>
                                    
                                    <td>Admissions &amp; Records</td>
                                    
                                    <td>Provide Admissions &amp; Records staff with tools needed to achieve peak levels of performance
                                       and
                                       to assist one another.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/3ACDAPfinished2009.pdf">SA0910-03</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Julie Corderman, Connie Parrish,</p>
                                       
                                       <p>Joe Sarrubbo,</p>
                                       
                                       <p>Steve Thiesse</p>
                                       
                                    </td>
                                    
                                    <td>Answer Center College Wide</td>
                                    
                                    <td>Create an informational website for Valencia's Answer Center college wide.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/4DAP2010final-Sonya.pdf">SA0910-04</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs</td>
                                    
                                    <td>Serve as primary Student Affairs contact and coordinator for Developmental Education
                                       Initiative and
                                       other grant-related activities.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/5SADAPAVPTransitions10910endofyear-L.Downing.pdf">SA0910-05</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions and Financial Aid</td>
                                    
                                    <td>Align Valencia's Strategic Goal. Build Pathways &amp; Transition to College with Florida's
                                       Next
                                       Generation PreK-20 Education Strategic Plan
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/6SADAPAVPTransitions20910endofyear-L.Downing.pdf">SA0910-06</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions and Financial Aid</td>
                                    
                                    <td>Design and implement a plan to increase the impact of the scholarship selection process
                                       on Valencia
                                       enrollment and on donor recognition.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/7-UnitplansATLAS0910SharewithAcademics-LindaV..pdf">SA0910-07</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>ATLAS Access</td>
                                    
                                    <td>Partner with Academic Departments on each campus to shuare what is available through
                                       ATLAS to assist
                                       students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/8-DAP0910InternationalStudentSystemsfinal-SEVIS-Liz.pdf">SA0910-08</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Research and evaluate a more consistent and automated way to track, record and report
                                       SEVIS student
                                       data.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/9SADAPBridges0910-L.Downing.pdf">SA0910-09</a>
                                       
                                    </td>
                                    
                                    <td>John Stover</td>
                                    
                                    <td>Bridges to Success</td>
                                    
                                    <td>Establish a Book Club to encourage Bridges students to read for learning and pleasure.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="left" valign="top">
                                       <a href="/documents/students/student-services/unit-plans/10-WEAVECDS2009-10-Cheryl.pdf">SA0910-10</a>
                                       
                                    </td>
                                    
                                    <td valign="top">Cheryl Robinson</td>
                                    
                                    <td valign="top">Career Development Services</td>
                                    
                                    <td>Ensure timely and accurate review of education plans sent through the My Education
                                       Plan tool for
                                       advisor review.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/11-CarDevSrvsSLSonlinesupport0910.pdf">SA0910-11</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                    <td>Create online resources for the Student Success class presentation.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/12SADAPCROP0910-L.Downing.pdf">SA0910-12</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Carter</td>
                                    
                                    <td>College Reach Out Program</td>
                                    
                                    <td>Successfully meet 100% of the outcomes projected for the 0910 Grant year.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/13SADAPDual0910-JackieCole.pdf">SA0910-13</a>
                                       
                                    </td>
                                    
                                    <td>Jacquelyn Cole</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Develop the plans, curriculum template, procedures and articulation needed to implement
                                       AS Dgree in
                                       Laser/Photonics at Wekiva High School.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/14-Unitplans0910EastDOSDevelPartnership-LindaV..pdf">SA0910-14</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Student Services - East Campus</td>
                                    
                                    <td>Continue work from 08/09 to reinforce the development of students to become more in
                                       charge of their
                                       academic information.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/15-OscDAPPlan0910-Chris.pdf">SA0910-15</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Student Services - Osceola Campus</td>
                                    
                                    <td>Develop departmental referral procedure that includes action plan and follow-up. Track
                                       the referral
                                       outcomes and student satisfaction.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/16DepartmentActionPlan2009-2010Advising-Ty.pdf">SA0910-16</a>
                                       
                                    </td>
                                    
                                    <td>Tyron Johnson</td>
                                    
                                    <td>Student Affairs - West Campus</td>
                                    
                                    <td>Professional Development of Student Services Advising Staff by developing a formal
                                       training program.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td valign="top" bgcolor="#FFFFFF">
                                       <a href="/documents/students/student-services/unit-plans/17DepartmentActionPlan2009-2010ISS-Ty.pdf">SA0910-17</a>
                                       
                                    </td>
                                    
                                    <td>Tyron Johnson</td>
                                    
                                    <td>Student Affairs - West Campus</td>
                                    
                                    <td>Professional Development of International Student Services Team through an increase
                                       of knowledge and
                                       understanding of federal and institutional policies and procedures that govern the
                                       enrollment of
                                       international students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/18-WEAVEWPCDOS2009-10-Cheryl.pdf">SA0910-18</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park Campus</td>
                                    
                                    <td>Continued revision of the Early Alert System that supports students showing early
                                       warning signs of
                                       behaviors that could lead to withdrawal.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/19-FinAidEarlyAwarding0910-Brad.pdf">SA0910-19</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Explore the possibility of awarding high school students financial aid before we receive
                                       their final
                                       high transcript.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/20-FinAidFinLiterarcy-Brad.pdf">SA0910-20</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Develop financial literacy programs and services and evaluate the participation and
                                       learning
                                       outcomes.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21NSO2010-Sonya.pdf">SA0910-21</a>
                                       
                                    </td>
                                    
                                    <td>
                                       <p>Angela Demahy, Celeste Henry, Melanie Price, Sally Witkamp</p>
                                    </td>
                                    
                                    <td>New Student Orientation</td>
                                    
                                    <td>Redesign the online New Student Orientation to increase student learning of the registration
                                       progress, the use of Atlas, and the education planning process.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/22RecordsGraduation2009DAP-EdwinSanchez-R.Simpson.pdf">SA0910-22</a>
                                       
                                    </td>
                                    
                                    <td>
                                       <p>Renee Simpson, Edwin Sanchez</p>
                                    </td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>To increase the efficiency of the graduation review and convert student paper records
                                       into an
                                       electroinic scanned image.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/23DAP_0910_Faculty_Resource_Guide1-Jill.pdf">SA0910-23</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Office for Students with Disabilities</td>
                                    
                                    <td>Provide faculty a resource guide that will inform them about Office for Students with
                                       Disabilities
                                       policies and procedures and assist in working with students with disabilities in their
                                       classrooms.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/24DAP0910ImprovinguseofBanner-Jill.pdf">SA0910-24</a>
                                       
                                    </td>
                                    
                                    <td>Jill Szentmiklosi</td>
                                    
                                    <td>Office for Students with Disabilities</td>
                                    
                                    <td>Reduce the number of Banner forms being used to capture and track disability data
                                       by utilizing
                                       Banner student disability form SGADISA.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/25FinalDAP2009-2010CPT-CLAST-CLASAdvising-Cynthia.pdf">SA0910-25</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Cynthia Cerrato, Brenda Martinez,</p>
                                       
                                       <p>Andrew Becker,</p>
                                       
                                       <p>Jonathan Hernandez</p>
                                       
                                    </td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Ensure students are provided with all needed assessment-related information pertaining
                                       to the recent
                                       changes that affect their transitions into and out of the college.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/26-FinalDAP2009-2010PartnershipswiththeSchoolDistricts-Cynthia.pdf">SA0910-26</a>
                                       
                                    </td>
                                    
                                    <td>
                                       <p>Cynthia Cerrato, Ruby Raffo</p>
                                    </td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Strenthen the current partnerships with public high schools in Orange and Osceola
                                       counties and to
                                       improve the Expanded Postsecondary Readiness Assessment (EPRA) testing process at
                                       their sites.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/27DAP_09-10-Assessment-Chanda.pdf">SA0910-27</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Design an assessment plan for the Student Development Department that will effectively
                                       assess the
                                       five areas of Student Development.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/28DAP_09-10Co-Curricular-Chanda.pdf">SA0910-28</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Design curriculum for college-wide co-curricular programs that will include goals,
                                       outcomes and
                                       assessments.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29SADAPCareerPathways0910-L.Downing.pdf">SA0910-29</a>
                                       
                                    </td>
                                    
                                    <td>Nicole Schneller</td>
                                    
                                    <td>Career Pathways (Tech Prep)</td>
                                    
                                    <td>Evaluate outcome requirements for Perkins funding with consideration for new Career
                                       Pathways model
                                       and revise the end-of-year outcomes reporting to support Perkins grant outcomes.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/30DAP09-10Viewbook-Jessica.pdf">SA0910-30</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Transition &amp; Enrollment Services</td>
                                    
                                    <td>Create a Viewbook that will be part of our communication plan to prospective students
                                       interested in
                                       learning more about Valencia.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/31DAP09-10FutureStudentsite-Jessica.pdf">SA0910-31</a>
                                       
                                    </td>
                                    
                                    <td>Jessica Morales</td>
                                    
                                    <td>Transition &amp; Enrollment Services</td>
                                    
                                    <td>Create a new Future Student tab that has better functionality and is more-user friendly.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/32SADAPTSIC0910-Elisha.pdf">SA0910-32</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez-Bonnewitz</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>Program Growth: Add at least 15 new 7th graders to Take Stock in Children.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/33-ACDAPfinished2009-Sonya.pdf">SA0910-33</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Answer Center Managers</td>
                                    
                                    <td>Create an assessment to evaluate the effectiveness of the newly implemented training
                                       manual.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/34-WEAVEFinal2009-2010-Atlas-Banner-Liz.pdf">SA0910-34</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Develop and refine the various training topics needed on Atlas and Banner for faculty,
                                       staff and
                                       students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2008-09</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/01AdmissionsandRegistration0809Results.pdf">SA0809-01</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Renee Simpson</p>
                                       
                                       <p>Lisa Stilke</p>
                                       
                                    </td>
                                    
                                    <td>Admissions and Registration</td>
                                    
                                    <td>Fully automate the admissions application process.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/2RecordsGraduation200809DAPfinal-Edwin.pdf">SA0809-02</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Renee Simpson</p>
                                       
                                       <p>Edwin Sanchez</p>
                                       
                                    </td>
                                    
                                    <td>Graduation and Records</td>
                                    
                                    <td>Fully automate the manner in which degrees and certificates are awarded.&nbsp; Begin steps
                                       to become a
                                       paperless campus through use of Banner Xtender Solutions.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/03ARSystemTechnicalSupport0809Results-Reyna.pdf">SA0809-03</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Renee Simpson</p>
                                       
                                       <p>Reyna Rangel</p>
                                       
                                    </td>
                                    
                                    <td>Admissions and Records, System Technical Support</td>
                                    
                                    <td>Research, review and revise current student support system.&nbsp; Streamline processes
                                       and procedures by
                                       fully utilizing enhancements provided through baseline Banner Programming.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/4SADAPforAVP0809CollegeTransition-Downing.pdf">SA0809-04</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>AVP College Transitions</td>
                                    
                                    <td>Build stronger connections with OCPS and ODS district and school level staff, with
                                       the Orange and
                                       Osceola Educational Foundations, and with the UCF Reach Out program to strengthen
                                       Valencia's
                                       connection to their work.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/05DAPFinalSonya.pdf">SA0809-05</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>AVP Student Affairs</td>
                                    
                                    <td>Lead Student Affairs division through the upgrade to Luminis 4.0 and Banner 8.0.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/6DAP0809Sonya1.pdf">SA0809-06</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>AVP Student Affairs</td>
                                    
                                    <td>Provide Student Affairs and college wide leadership on a self-study determining Valencia
                                       experiences
                                       that make a difference in the first-year students' retention and success rate.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/7SADAPforBridges0809.pdf">SA0809-07</a>
                                       
                                    </td>
                                    
                                    <td>John Stover</td>
                                    
                                    <td>Bridges</td>
                                    
                                    <td>Implement new mentoring program for Hispanic females at&nbsp;<br> East Campus.&nbsp; Develop and 1 credit SLS
                                       course as part of the mentoring program.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/8DAP09FinalCheryl.pdf">SA0809-08</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                    <td>Ensure timely and accurate review of education plans sent through the My Education
                                       Plan tool for
                                       advisor review.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/9annualreport200809CDSC.Robinson.pdf">SA0809-09</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                    <td>Effectively support the college's work with probation students.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="left" valign="top">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/10CPADAP2008-09-Chris.pdf">SA0809-10</a>
                                       
                                    </td>
                                    
                                    <td valign="top">Chris Klinger</td>
                                    
                                    <td valign="top">Career Program Advisors</td>
                                    
                                    <td>Review and update the program mileposts to meet new Perkins guidelines and simplify
                                       assessment.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/11SADAPforCROP0809-Downing.pdf">SA0809-11</a>
                                       
                                    </td>
                                    
                                    <td>Tanisha Carter</td>
                                    
                                    <td>CROP</td>
                                    
                                    <td>Revitalize program by focusing on achieving Life Map success indicators and developing
                                       partnerships
                                       with others to maximize use of resources.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/12SADAPforDE0809-Downing.pdf">SA0809-12</a>
                                       
                                    </td>
                                    
                                    <td>Jacquelyn Cole</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Expand the Summer Institute pilot to other campuses and evaluate feasibility of expanding
                                       it to
                                       afternoons.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/13DAPDECole.pdf">SA0809-13</a>
                                       
                                    </td>
                                    
                                    <td>Jacquelyn Cole</td>
                                    
                                    <td>Dual Enrollment</td>
                                    
                                    <td>Improve communication with high school staff.&nbsp; Develop and use reporting tools to
                                       evaluate success
                                       in meeting state standards.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/14DAPLVance1.pdf">SA0809-14</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students - East Campus</td>
                                    
                                    <td>Help students be better prepared for conversations with Valencia staff by emphasizing
                                       the importance
                                       of being responsible for their academic information.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/150809DAPLVance3.pdf">SA0809-15</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students - East Campus</td>
                                    
                                    <td>Have more team building activities within department to promote better understanding
                                       of each other
                                       and our daily challenges.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/16DAP2008-09-Chris.pdf">SA0809-16</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Dean of Students - Osceola Campus</td>
                                    
                                    <td>Develop a strong feeling of community among staff resulting in a stronger team.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td valign="top" bgcolor="#FFFFFF">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/17DAPFinalTy.pdf">SA0809-17</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>Dean of Students - West Campus</td>
                                    
                                    <td>Plan for Transition of the Unit Work Team.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/18DAPFinalCheryl.pdf">SA0809-18</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park Campus</td>
                                    
                                    <td>Support students showing early warning signs of behaviors taht could lead to course
                                       withdrawal to
                                       increase retention (joint goal with Academic Affairs).
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/19annualreport200809WPCC.Robinson.pdf">SA0809-19</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park Campus</td>
                                    
                                    <td>Build a strong Student Services team through transition to a new staffing model.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/20DAP0809FAFinancialLiteracyEOY-Brad.pdf">SA0809-20</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid Services</td>
                                    
                                    <td>Address the lack of financial literacy of our students.&nbsp; Provide programs and tools
                                       to assist
                                       students to budget and borrow responsibility.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/21DAPFARegulatoryCompliance0809-Brad.pdf">SA0809-21</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid Services</td>
                                    
                                    <td>Implement a federal quality assurance plan that will provide a quality assurance review
                                       of a major
                                       area each year.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/22DAPTy.pdf">SA0809-22</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>International Student Services</td>
                                    
                                    <td>Recertification of all campuses and ensure compliance with Student Exchange Visitor
                                       Program while
                                       growing our international students by 100.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/23dap22008-Jill.pdf">SA0809-23</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                    <td>Create and present a seminar on Universal Design in Learning for faculty.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/24dap12008-Jill.2.pdf">SA0809-24</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                    <td>Work with OSD staff to build a stronger team through improved communication and collaboration.&nbsp;
                                       Help
                                       staff better understand their roles within the department.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td valign="top" bgcolor="#FFFFFF">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/25DAP10809-Chanda.pdf">SA0809-25</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Establish a Staff Development Plan based on current literature and the institutions's
                                       Strategic Plan
                                       to allow staff to grow professionally and personally.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/26DAP20809-Chanda.2.pdf">SA0809-26</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Create optimal conditions for student learning through establishing a college wide
                                       tutoring team and
                                       assess student learning.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/27SADAPforTechPrep0809-Downing.pdf">SA0809-27</a>
                                       
                                    </td>
                                    
                                    <td>Nicole Schneller</td>
                                    
                                    <td>Tech Prep</td>
                                    
                                    <td>Convert all existing career pathways to a new state template, adjusting program and
                                       articulation as
                                       needed. (5 year plan).
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/28SADAPforTSIC0809-Downing.pdf">SA0809-28</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>Build community support for the program by marketing TSIC to community and business
                                       leaders.&nbsp; Grow
                                       program by 50 students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/290809DAPLVance2.pdf">SA0809-29</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Help students and staff have a better understanding of ATLAS and its tools.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/300809DAPLVance4.pdf">SA0809-30</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Atlas Access</td>
                                    
                                    <td>
                                       <p>Raise the visibility and availability of our ATLAS lab staff and continue to support
                                          the
                                          professional development of our student leaders.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/31ACDAP20082009Sonya2_000.pdf">SA0809-31</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Answer Center</td>
                                    
                                    <td>Create an assessment to evaluate the effectiveness of the newly implemented training
                                       manual.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/32ACDAP20082009-Sonya_000.pdf">SA0809-32</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Answer Center</td>
                                    
                                    <td>Create a system to continuously monitor the Answer Center website for information
                                       accuracy on staff
                                       and student processes so it is always up to date.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/33DAP0809TrainingonAtlasBanner2-Liz.pdf">SA0809-33</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Development and refine the various training topics needed on Atlas and Banner for
                                       faculty, staff and
                                       students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/34DAP2008-Jessica.pdf">SA0809-34</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Linda Downing</p>
                                       
                                       <p>Jessica Morales</p>
                                       
                                    </td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Clarify and enhance the Individual School Recruitment Plans for Orange and Osceola.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/35DAPFinalJessica.pdf">SA0809-35</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Linda Downing</p>
                                       
                                       <p>Jessica Morales</p>
                                       
                                    </td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Fully implement the Prospect Module in Banner.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/36DAPFinalJessica.pdf">SA0809-36</a>
                                       
                                    </td>
                                    
                                    <td>
                                       
                                       <p>Linda Downing</p>
                                       
                                       <p>Jessica Morales</p>
                                       
                                    </td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Strengthen relationships with student affairs, high school counselors, Orange and
                                       Osceola county
                                       human resource personnel, students and parents.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/37DAP08-09Goal1Cynthia1.pdf">SA0809-37</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Improve the level of support that Assessment staff provides students by deepening
                                       their
                                       understanding and integration of the LifeMap model into their work.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/38DAP08-09Goal2Cynthia2.pdf">SA0809-38</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Development the Assessment staff from both the individual and team perspective.</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2007-08</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td width="10%">DAP</td>
                                    
                                    <td width="15%">Leader</td>
                                    
                                    <td width="20%">Department</td>
                                    
                                    <td>Description</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/01DAPAdmRegFacComm0708.doc">SA00708-01</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson Lisa Stilke</td>
                                    
                                    <td>Admissions and Registration</td>
                                    
                                    <td>Expand the current Admissions &amp; Registration information exchange plan to incorporate
                                       faculty
                                       and staff in electronic notifications triggered by specific events.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/02DAPAnswerCntrInterComm0708final.docx">SA0708-02</a>
                                       
                                    </td>
                                    
                                    <td>Answer Center Coordinators - Sonya Joseph</td>
                                    
                                    <td>Answer Centers</td>
                                    
                                    <td>Develop training manual for Answer Center Specialists. Devlop survey of communication
                                       between
                                       departments.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/03DAPAtlasAccessFAFSAfinal0708.docx">SA0708-03</a>
                                       
                                    </td>
                                    
                                    <td>Atlas Access Coordinators - Linda Vance</td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Increase staff ability to assist students with the FAFSA.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/04DAPAtlasAccessTrainingfinal0708.docx">SA0708-04</a>
                                       
                                    </td>
                                    
                                    <td>Atlas Access Coordinators - Linda Vance</td>
                                    
                                    <td>Atlas Access Labs</td>
                                    
                                    <td>Increase staffs' leadership abilities.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/05DAPAtlasUpdatesStaffComm0708final.doc">SA0708-05</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Updates and Training</td>
                                    
                                    <td>Develop effective communication plan for notification of system operational issues.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/06DAPAVPCTTSIC0708.doc">SA0708-06</a>
                                       
                                    </td>
                                    
                                    <td>Elisha Gonzalez Linda Downing</td>
                                    
                                    <td>Take Stock in Children</td>
                                    
                                    <td>Implement Take Stock in Children program.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/07DAPAVPCTCROP0708.doc">SA0708-07</a>
                                       
                                    </td>
                                    
                                    <td>Gerald Jones Linda Downing</td>
                                    
                                    <td>College Reach Out Program (CROP)</td>
                                    
                                    <td>Strengthen documentation plan in order to increase funding.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/08AVPSAOnlineNSO0708final.doc">SA0708-08</a>
                                       
                                    </td>
                                    
                                    <td>Orientation Coordinators - Sonya Joseph</td>
                                    
                                    <td>New Student Orientation</td>
                                    
                                    <td>Create a fully online New Student Orientation program for college ready new students.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/09CarDevSvcsMCP0708final.doc">SA0708-09</a>
                                       
                                    </td>
                                    
                                    <td>Career Development Team - Cheryl Robinson</td>
                                    
                                    <td>My Career Planner</td>
                                    
                                    <td>Redirect the career assessment function in My Career Planner.</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/10CarDevSvcsoutcomes0708final.doc">SA0708-10</a>
                                       
                                    </td>
                                    
                                    <td>Career Development Team - Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                    <td>Develop and assess outcomes for career counseling process.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/11DOSEaststafftraining0708final.doc">SA0708-11</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students-East</td>
                                    
                                    <td>Improve staff listening/speaking skills to more effectively resolve difficult situations.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/12DOSEastprobation0708final.doc">SA0708-12</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students- East</td>
                                    
                                    <td>Increase personalized communication with probation/suspension students.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/13DOSOsconlineapp0708final.doc">SA0708-13</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Dean of Students- Osceola</td>
                                    
                                    <td>Conduct staff feedback from helping students with the on-line application in order
                                       to reduce student
                                       confusion.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/14DOSOscnondegree0708final.doc">SA0708-14</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Dean of Students- Osceola</td>
                                    
                                    <td>Develop written information for non degree seeking students who choose not to attend
                                       orientation.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/15DOSWestacademicsuccess0708final.doc">SA0708-15</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>Dean of Students, West</td>
                                    
                                    <td>Early Alert System - Implement a collegewide model of intervention and strategies
                                       to work with
                                       students in academic difficulty.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/16DOSWPenrollment0708final.doc">SA0708-16</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park</td>
                                    
                                    <td>Increase campus enrollment by 1% each term.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td bgcolor="#ffffff">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/17DOSWPCLC0708final.doc">SA0708-17</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park</td>
                                    
                                    <td>Design and implement interventions that increase student engagement in the Cooperative
                                       Learning
                                       Community.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/18DOSWPteam0708final.doc">SA0708-18</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students - Winter Park</td>
                                    
                                    <td>Build a strong Student Services team.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/19FAcompliance0708final.doc">SA0708-19</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Implement a federal quality assurance plan that will provide for review of 2 areas
                                       each year.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/20FAawards0708final.doc">SA0708-20</a>
                                       
                                    </td>
                                    
                                    <td>Brad Honious</td>
                                    
                                    <td>Financial Aid</td>
                                    
                                    <td>Reduce student confusion with the financial aid application process.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/21GradRecordsFacComm0708.doc">SA0708-21</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson Edwin Sanchez</td>
                                    
                                    <td>Records and Graduation</td>
                                    
                                    <td>Expand the current Records and Graduation Information Exchange Plan to engage all
                                       constituent groups
                                       that touch student records or any of its processes.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/22IntStuSvcsconsistency0708final.doc">SA0708-22</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>International Student Services</td>
                                    
                                    <td>Develop collegewide consistent processes and procedures for the admission of international
                                       students.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/23OSDPersonalize0708final.doc">SA0708-23</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                    <td>Implement appointment reminder calling system to personalize communication to students.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/24OSDCPT0708final.doc">SA0708-24</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                    <td>Implement personalized communication to studetns who self-identify on the CPT.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td bgcolor="#ffffff">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/25OSDSelfsufficient0708final.doc">SA0708-25</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                    <td>Implement personalized new student folders in OSD to promote student self-determination
                                       and
                                       self-advocacy.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/26SDmarketing0708final.doc">SA0708-26</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>Implement a personalized marketing plan that will relate Student Development's mission
                                       and services
                                       to students in a meaningful way.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/27StandTestingCLAST0708final.doc">SA0708-27</a>
                                       
                                    </td>
                                    
                                    <td>Cynthia Cerrato</td>
                                    
                                    <td>Standardized Testing</td>
                                    
                                    <td>Improve support systems for students taking the CLAST.</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/28TransSvcsProspectcoach0708final.doc">SA0708-28</a>
                                       
                                    </td>
                                    
                                    <td>Nechell Bonds</td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Identify opportunities to provide one-on-one coaching to prospective students through
                                       the steps to
                                       enrollment. Engage prospective students with a more personalized communication plan.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="/documents/students/student-services/unit-plans/29TransSvcsregional0708.doc">SA0708-29</a>
                                       
                                    </td>
                                    
                                    <td>Nechell Bonds</td>
                                    
                                    <td>Transition Services</td>
                                    
                                    <td>Develop Regional Recruitment Plans to outline strategies to implement the Enrollment
                                       Plan.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/30SDleadertraining0708final.doc">SA0708-30</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                    <td>
                                       <p>Develop a comprehensive Student Leader Program that includes college-wide standards,
                                          goals,
                                          curriculum, and an effective assessment process.
                                       </p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/31DAPAtlasUpdatesOnlinedataaccuracy0708final.doc">SA0708-31</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                    <td>Develop a more efficient means to identify and update obsolete data being presented
                                       in Atlas
                                       channels, SSB WebTailor and Web pages.
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2006-07</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="https://valenciacollege.edu/studentservices/unitplans/files/01DAPAdm&amp;Reg0607plan.pdf">SA0607-01</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson / Lisa Stilke</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="https://valenciacollege.edu/studentservices/unitplans/files/02DAPRecandGrad0607plan.pdf">SA0607-02</a>
                                       
                                    </td>
                                    
                                    <td>Renee Simpson / Edwin Sanchez</td>
                                    
                                    <td>Admissions and Records</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/03DAPAtlascommunication0607final.doc">SA0607-03</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/04DAPBannerandATLAStraining0607final.doc">SA0607-04</a>
                                       
                                    </td>
                                    
                                    <td>Liz Gangemi</td>
                                    
                                    <td>Atlas Information Systems</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/05DAPCareerDevMCP0607final.doc">SA0607-05</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/06DAPCareerDevWeb0607final.doc">SA0607-06</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/07DAPAVPCollegeTransitionsComandPlng0607final.doc">SA0607-07</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/08DAPAVPCollegeTransitionsTech0607final.doc">SA0607-08</a>
                                       
                                    </td>
                                    
                                    <td>Linda Downing</td>
                                    
                                    <td>College Transitions</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/09DAPDOSeastLNC0607final.doc">SA0607-09</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students, East</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/10DAPDOSeastNSO0607final.doc">SA0607-10</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Dean of Students, East</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/11DAPDOSOsceolaRegFair0607final.doc">SA0607-11</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Dean of Students, Osceola</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/12DAPDOSOsceolaStaffDev0607final.doc">SA0607-12</a>
                                       
                                    </td>
                                    
                                    <td>Chris Klinger</td>
                                    
                                    <td>Dean of Students, Osceola</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/13DAPDOSWPCInterDeptComm0607final.doc">SA0607-13</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students, Winter Park</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/14DAPDOSWPCEnrGrowth0607final.doc">SA0607-14</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Career Development Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/15DAPDOSWPCLC0607final.doc">SA0607-15</a>
                                       
                                    </td>
                                    
                                    <td>Cheryl Robinson</td>
                                    
                                    <td>Dean of Students, Winter Park</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/16DAPDOSwestearlyalert0607final.doc">SA0607-16</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>Dean of Students, West</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td bgcolor="#ffffff">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/17DAPDOSwestanswercenter0607final.doc">SA0607-17</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>Dean of Students, West</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/18DAPEnrServicesProspectComm0607final.doc">SA0607-18</a>
                                       
                                    </td>
                                    
                                    <td>Nechell Bonds</td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="https://valenciacollege.edu/studentservices/unitplans/files/19DAPEnrServicesBannerProspectModule0607plan.pdf">SA0607-19</a>
                                       
                                    </td>
                                    
                                    <td>Nechell Bonds</td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="https://valenciacollege.edu/studentservices/unitplans/files/20DAPEnrServicesStaffDev0607plan.pdf">SA0607-20</a>
                                       
                                    </td>
                                    
                                    <td>Nechell Bonds</td>
                                    
                                    <td>Enrollment Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/21DAPInternational0607final.doc">SA0607-21</a>
                                       
                                    </td>
                                    
                                    <td>Ty Johnson</td>
                                    
                                    <td>International Student Services</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/22DAPOSDAsstTech0607final.doc">SA0607-22</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/23DAPOSDFacComm0607final.doc">SA0607-23</a>
                                       
                                    </td>
                                    
                                    <td>Jillian Szentmiklosi</td>
                                    
                                    <td>Students with Disabilities</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/24DAPAVPSAAtlasContent0607final.doc">SA0607-24</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs AVP</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td bgcolor="#ffffff">
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/25DAPAVPSAGradTran0607final.doc">SA0607-25</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Student Affairs AVP</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/26DAPStuDevSGAExecutive0607final.doc">SA0607-26</a>
                                       
                                    </td>
                                    
                                    <td>Chanda Torres</td>
                                    
                                    <td>Student Development</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/27DAPAnswerCenter0607final.doc">SA0607-27</a>
                                       
                                    </td>
                                    
                                    <td>Sonya Joseph</td>
                                    
                                    <td>Answer Center</td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td>
                                       <a href="http://valenciacollege.edu/studentservices/unitplans/documents/28DAPAtlasAccess0607final.doc">SA0607-28</a>
                                       
                                    </td>
                                    
                                    <td>Linda Vance</td>
                                    
                                    <td>Atlas Access</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2005-06</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           <a href="/documents/students/student-services/unit-plans/2005-2006SLPs.pdf">2005-2006
                              SLPs</a>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Index of Plans for 2004-05</h3>
                           
                           
                           <p>Click on DAP Number to open file as a PDF Document</p>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           <a href="/documents/students/student-services/unit-plans/2004-2005SLPspart1of2.pdf">2004-2005
                              SLPs part 1 of 2</a><br> <a href="/documents/students/student-services/unit-plans/2004-2005SLPspart2of2.pdf">2004-2005
                              SLPs part 2 of 2</a>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/unit-plans/archives.pcf">©</a>
      </div>
   </body>
</html>