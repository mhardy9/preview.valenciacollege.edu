<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Departmental Unit Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/unit-plans/15-16.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/student-services/unit-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Departmental Unit Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li><a href="/students/student-services/unit-plans/">Unit Plans</a></li>
               <li>Departmental Unit Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>2015-16 Student Affairs Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <em><strong>Principles of Accreditation</strong></em> requirements, <strong>Comprehensive Standards 3.3.1</strong>, each Student Affairs department will design and implement an annual plan to improve
                           student services and success.
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its educational programs and its
                           administrative and educational support services; assesses whether it achieves these
                           outcomes; and provides evidence of improvement based on analysis of those results.
                        </p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an informational overview including a <strong>Department Action Plan (DAP)</strong> number, department of origin, campus, student affairs leader and a brief topical
                           focus for each plan. By clicking on a <strong>Division Action Plan (DAP)</strong> number you will be linked to current planning and evaluation documents.
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <font size="4">Index of Plans for 2015-16</font><br>
                                    Click on DAP Number to open file as a PDF Document
                                    
                                 </div>
                                 
                              </div> 
                              
                           </div>
                           
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h4>DAP</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Leader</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Department</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Description</h4>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/2Andy-Admissions-RegistrationDAP2015-16.pdf">SA1516-02.</a></div>
                                 
                                 <div data-old-tag="td">Andy Oguntola </div>
                                 
                                 <div data-old-tag="td">Admissions &amp; Records </div>
                                 
                                 <div data-old-tag="td">Review documentation processes for efficiency and make changes as needed. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/3Reyna-AdmissionsRecordsSystemsDAP2015-16.pdf">SA1516-03.</a></div>
                                 
                                 <div data-old-tag="td">Reyna Rangel</div>
                                 
                                 <div data-old-tag="td">Admission/Records Systems </div>
                                 
                                 <div data-old-tag="td">Incorporate state &amp; federal changes and cross train staff </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/4Ed-EvelynDirofAdvisingCWUnitPlan2015-2016.pdf">SA1516-04</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Ed Holmes &amp; Evelyn Lora-Santos </div>
                                 
                                 <div data-old-tag="td">Advising - College wide </div>
                                 
                                 <div data-old-tag="td">Development of LifeMap 2.0 advisor training curriculum </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/5JoeDAPAtlasLab2015-2016.pdf">SA1516-05</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Atlas Access Labs</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Create a training program for support staff in the Atlas Access Labs. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/6LisaS-2015-2016UnitPlansAtlasInformationSystemsDegreeWorks7-7-2016.pdf">SA1516-06</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Lisa Stilke </div>
                                 
                                 <div data-old-tag="td">Atlas Information Systems </div>
                                 
                                 <div data-old-tag="td">Degree Works: Create reports to track student usage of Degree Works </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/7LisaS.pdf">SA1516-07.</a></div>
                                 
                                 <div data-old-tag="td">Lisa Stilke </div>
                                 
                                 <div data-old-tag="td">Atlas Information Systems </div>
                                 
                                 <div data-old-tag="td">Luminis</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/8Tanisha-BTSUnitPlan2015_2016_Final.pdf">SA1516-08.</a></div>
                                 
                                 <div data-old-tag="td">Tanisha Carter </div>
                                 
                                 <div data-old-tag="td">Bridges to Success </div>
                                 
                                 <div data-old-tag="td">Implementing learning strategies that improve academic outcomes for Bridges students.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/9JohnBritt-Ed-Evelyn-CareerCenterDAP15-16-Final.pdf">SA1516-09</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Evelyn Lora-Santos </div>
                                 
                                 <div data-old-tag="td">Career Center </div>
                                 
                                 <div data-old-tag="td">Conduct research that will drive the redesign of the Career Center website</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/10AmyCollegeTransitions_2015_2016_Unit_plan_1_completed.pdf">SA1516-10</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Amy Kleeman </div>
                                 
                                 <div data-old-tag="td">College Transitions </div>
                                 
                                 <div data-old-tag="td">Enhance and improve performance of College Transitions </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/11Joe-DOSEC-Final1516.pdf">SA1516-11</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo</div>
                                 
                                 <div data-old-tag="td">Dean of Students East Campus </div>
                                 
                                 <div data-old-tag="td">Host 3 student outreach events to measure student engagement and knowledge of services</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/12Jill-WEAVEOSCDeanofStudentsTeam15-16.pdf">SA1516-12</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Jill Szentmiklosi </div>
                                 
                                 <div data-old-tag="td">Dean of Students Osceola Campus </div>
                                 
                                 <div data-old-tag="td">Facilitate the Osceola Campus implementation of LifeMap 2.0 Advising </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/13Joe-DAPDOSWinterPark2015-2016.pdf">SA1516-13</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo </div>
                                 
                                 <div data-old-tag="td">Dean of Students Winter Park Campus</div>
                                 
                                 <div data-old-tag="td">Ensure a smooth transition for staff as Winter Park student services goes through
                                    changes in 2015-16
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/14Ben-Linda-DOSWCUnitPlan2015-2016.pdf">SA1516-14</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Linda Herlocker &amp; Ben Lion </div>
                                 
                                 <div data-old-tag="td">Dean of Students West Campus </div>
                                 
                                 <div data-old-tag="td">Facilitate West Campus implementation of LifeMap 2.0 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/15-Kia-DE2015_2016.pdf">SA1516-15.</a></div>
                                 
                                 <div data-old-tag="td">Kia Heard &amp; Latishua Lewis </div>
                                 
                                 <div data-old-tag="td">Dual Enrollment </div>
                                 
                                 <div data-old-tag="td">Renew and develop ways to refresh the Dual Enrollment program processes and procedures
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/16-Jacquelyn-Enrollment_Services_15-16_Unit_Plan2.pdf">SA1516-16</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Jacquelyn Thompson </div>
                                 
                                 <div data-old-tag="td">Enrollment Services </div>
                                 
                                 <div data-old-tag="td">Increase student preparation, self-efficacy, and success while increasing service
                                    levels 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/17ChrisUnitPlanFinancialAid15-16.pdf">SA1516-17</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Chris Christensen </div>
                                 
                                 <div data-old-tag="td">Financial Aid </div>
                                 
                                 <div data-old-tag="td">Create a comprehensive plan for easier access to funding for Veteran students. Financial
                                    Aid Process redesign. Create and implement a Default Prevention Plan. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/18Deb-UnitPlans2015-2016OSD_DL_review.pdf">SA1516-18.</a></div>
                                 
                                 <div data-old-tag="td">Deb Larew </div>
                                 
                                 <div data-old-tag="td">Office for Students with Disabilities </div>
                                 
                                 <div data-old-tag="td">Strengthen OSD's internal processes and build partnerships in the community and in
                                    the college 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/19Edwin-RecordsGraduationDAP2015-16.pdf">SA1516-19</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Edwin Sanchez </div>
                                 
                                 <div data-old-tag="td">Records &amp; Graduation </div>
                                 
                                 <div data-old-tag="td">Develop online forms in Atlas to assist students </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/20Erica-Stand_Test_2015_2016.pdf">SA1516-20</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Erica Reese </div>
                                 
                                 <div data-old-tag="td">Standardized Testing </div>
                                 
                                 <div data-old-tag="td">Refine staff training materials</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/21Tracey2015-2016UnitPlan.pdf">SA1516-21</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Tracey Olsen-Oliver </div>
                                 
                                 <div data-old-tag="td">Student Development </div>
                                 
                                 <div data-old-tag="td">Develop Program Outcome Guides (POG's) </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/22SonyaUnitPlans20152016.pdf">SA1516-22</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Sonya Joseph </div>
                                 
                                 <div data-old-tag="td">Student Services Administration </div>
                                 
                                 <div data-old-tag="td">Contribute to design and implementation of the LifeMap 2.0 Advising Model </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/23CathyUnitPlan2015-2016.pdf">SA1516-23</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Cathy Penfold Navarro </div>
                                 
                                 <div data-old-tag="td">Title III Pathways</div>
                                 
                                 <div data-old-tag="td">Improve ability of college systems, faculty and staff </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/24CathyPATHWAYSUnitPlan2015-2016.pdf">SA1516-24</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Cathy Penfold Navarro </div>
                                 
                                 <div data-old-tag="td">Title III Pathways</div>
                                 
                                 <div data-old-tag="td">Increase efficiency and capability of student advising </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/25CathyPATHWAYSUnitPlan2015-2016.pdf">SA1516-25</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Cathy Penfold Navarro </div>
                                 
                                 <div data-old-tag="td">Title III Pathways</div>
                                 
                                 <div data-old-tag="td">Increase efficiency and functionality of LifeMap My Education Plan tool </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/26CathyPATHWAYSUnitPlan2015-2016.pdf">SA1516-26</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Cathy Penfold Navarro </div>
                                 
                                 <div data-old-tag="td">Title III Pathways</div>
                                 
                                 <div data-old-tag="td">Increase accessibility of advising resources for students </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/28PenfoldNavarroPATHWAYSUnitPlan2015-2016-Copy.pdf">SA1516-28.</a></div>
                                 
                                 <div data-old-tag="td">Cathy Penfold Navarro </div>
                                 
                                 <div data-old-tag="td">Title III Pathways</div>
                                 
                                 <div data-old-tag="td">Increase faculty participation in developing advising models and methods </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/29Nikky-Transitions_Planning_2015_2016.pdf">SA1516-29</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Nikky Ferrer </div>
                                 
                                 <div data-old-tag="td">Transition Planning </div>
                                 
                                 <div data-old-tag="td">Strengthen and redesign existing programs and initiatives in recruitment and student
                                    transition 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/30-Chris-FAredesignUnitPlanFinancialAid15-16.pdf">SA1516-30</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Chris Christensen </div>
                                 
                                 <div data-old-tag="td">Financial Aid </div>
                                 
                                 <div data-old-tag="td">Redesign the financial aid process to enable student to navigate it more accurately
                                    and intuitively. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="documents/31-ChrisDefaultpreventionUnitPlanFinancialAid15-16.pdf">SA1516-31</a>.
                                 </div>
                                 
                                 <div data-old-tag="td">Chris Christensen </div>
                                 
                                 <div data-old-tag="td">Financial Aid </div>
                                 
                                 <div data-old-tag="td">Create a comprehensive Default Prevention plan that includes financial literacy to
                                    reduce the financial aid default rate. 
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/unit-plans/15-16.pcf">©</a>
      </div>
   </body>
</html>