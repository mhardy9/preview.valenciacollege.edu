<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Departmental Unit Action Plans  | Valencia College</title>
      <meta name="Description" content="Departmental Unit Action Plans | Student Services">
      <meta name="Keywords" content="college, school, educational, student, services, department, unit, action, plans">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/unit-plans/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/student-services/unit-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		Departmental Unit Action Plans
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Unit Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Welcome</h2>
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>Departmental Unit Action Plans (DAPs) are presented on this web-site for the entire
                              academic community of
                              Valencia to view. The College's annual planning and evaluation cycle requires that
                              academic departments
                              identify plans related to the Strategic Learning Plan that include learning outcomes.<br><br>
                              
                              In July of 2003, the Vice President for Academic Affairs and Chief Learning Officer
                              met with the academic
                              deans and designed a new set of procedures to plan and evaluate educational effectiveness
                              of the College's
                              academic programs, with emphasis on the quality of student learning outcomes. At the
                              end of the annual
                              cycle, an evaluation form will be completed and used to document specific measures
                              of educational
                              effectiveness.<br><br>
                              
                              As we move toward new forms of assessment at the College we will no doubt see some
                              changes in how we
                              document student learning outcomes, and how we use the results of our analysis of
                              the outcomes to plan new
                              goals.
                           </p>
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>2016-17 Student Affairs Planning and Evaluation Links</h3>
                        
                        <p>To comply with SACS <em><strong>Principles of Accreditation</strong></em> requirements, <strong>Comprehensive Standards 3.3.1</strong>, each Student Affairs department will design and implement an annual plan to improve
                           student services and success.
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its educational programs and its
                           administrative and educational support services; assesses whether it achieves these
                           outcomes; and provides evidence of improvement based on analysis of those results.
                        </p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an informational overview including a <strong>Department Action Plan (DAP)</strong> number, department of origin, campus, student affairs leader and a brief topical
                           focus for each plan. By clicking on a <strong>Division Action Plan (DAP)</strong> number you will be linked to current planning and evaluation documents.
                        </p>
                        
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <caption>
                                    Click on DAP Number to open file as a PDF Document
                                    
                                 </caption>
                                 
                              </tr>
                              
                              
                              <tr style="background-color: #e6e7e9">
                                 
                                 <th width="10%">
                                    <h4>DAP</h4>
                                 </th>
                                 
                                 <th width="28%">
                                    <h4>Leader</h4>
                                 </th>
                                 
                                 <th width="20%">
                                    <h4>Department</h4>
                                 </th>
                                 
                                 <th>
                                    <h4>Description</h4>
                                 </th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/1-linda-arrgdap12016-2017.pdf">SA1617-01</a></td>
                                 
                                 <td>Linda Herlocker</td>
                                 
                                 <td>Admissions &amp; Records </td>
                                 
                                 <td>Fully implement the CRM for Recruit. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/2-linda-arrgdap22016-2017.pdf">SA1617-02</a></td>
                                 
                                 <td>Linda Herlocker </td>
                                 
                                 <td>Admissions &amp; Records </td>
                                 
                                 <td>Create and implement enhanced department-wide communicaton stragegies to enhance team
                                    building college wide. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/3-andy-admissions-registrationdap2016-17.pdf">SA1617-03</a></td>
                                 
                                 <td>Andy Oguntola </td>
                                 
                                 <td>Admissions &amp; Records </td>
                                 
                                 <td>Focus on CRM training, automating admissions letters, and creating beneficial edit
                                    reports. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/4-reyna-admissionsrecordssystemsdap2016-17.pdf">SA1617-04</a></td>
                                 
                                 <td>Reyna Rangel</td>
                                 
                                 <td>Admission/Records Systems </td>
                                 
                                 <td>Provide system support and cross training for upcoming Recruit project. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/5-joe-dapatlaslab2016-2017.pdf">SA1617-05</a></td>
                                 
                                 <td>Joe Sarrubbo </td>
                                 
                                 <td>Atlas Access Labs </td>
                                 
                                 <td>To continue with the creation and expansion of an online interactive training module
                                    for the support specialist II staff members, IWS, FWS and student leaders. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/6-lisas-2016-2017unitplansatlasinformationsystemsadvisecrm.pdf">SA1617-06</a></td>
                                 
                                 <td>Lisa Stilke </td>
                                 
                                 <td>Atlas Information Systems </td>
                                 
                                 <td>Continued implementation of the Adviser CRM. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/7-lisas-2016-2017unitplansatlasinformationsystemsdegreeworks.pdf">SA1617-07</a></td>
                                 
                                 <td>Lisa Stilke </td>
                                 
                                 <td>Atlas Information Systems </td>
                                 
                                 <td>Continued implementation of the Degree Works Plans tab.</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/8-tanisha-btsunitplan2016_2017.pdf">SA1617-08</a></td>
                                 
                                 <td>Tanisha Carter </td>
                                 
                                 <td>Bridges to Success </td>
                                 
                                 <td>Improving our academic outcomes for our students by implementing strategies. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/9-careercenterbritt-holmes-lorasantosdap16-17.pdf">SA1617-09</a></td>
                                 
                                 <td>
                                    
                                    <p>Evelyn Lora-Santos, Ed Holmes, John Britt </p>
                                    
                                 </td>
                                 
                                 <td>Career Centers College wide </td>
                                 
                                 <td>Along with OIT and Marketing, the Career Center will develop the content of the Career
                                    Center website. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/10-amy-collegetransitions16-17.pdf">SA1617-10</a></td>
                                 
                                 <td>Amy Kleeman </td>
                                 
                                 <td>College Transitions </td>
                                 
                                 <td>Enhance and improve the enrollment process to be more seamless and student-friendly.
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/11-joe-dapdoseast2016-2017.pdf">SA1617-11</a></td>
                                 
                                 <td>Joe Sarrubbo</td>
                                 
                                 <td>Dean of Students East Campus </td>
                                 
                                 <td>Implementing Who's Next (an electronic queuing system) at East Campus.</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/12-jill-weaveoscdeanofstudentsteam16-17b.pdf">SA1617-12</a></td>
                                 
                                 <td>Jill Szentmiklosi </td>
                                 
                                 <td>Dean of Students Osceola Campus </td>
                                 
                                 <td>Continue implementation of LifeMap 2.0 Advising to include CRM. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    <p><a href="/documents/students/student-services/unit-plans/13-joe-dapdoswinterpark2016-2017.pdf">SA1617</a><a href="/documents/students/student-services/unit-plans/13joe-doswinterpark2015-2016.pdf">-13</a></p>
                                 </td>
                                 
                                 <td>Joe Sarrubbo </td>
                                 
                                 <td>Dean of Students Winter Park Campus</td>
                                 
                                 <td>The Winter Park DOS team will create its own team user's manual to guide new and existing
                                    staff. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/14-benwdos2016-2017unitplan.pdf">SA1617-14</a></td>
                                 
                                 <td>Ben Lion </td>
                                 
                                 <td>Dean of Students West Campus </td>
                                 
                                 <td>Evaluate the DOS-WC space and make recommendation to enhance the space to benefit
                                    student services. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/15-latishuade2016-2017unitplan.pdf">SA1617-15</a></td>
                                 
                                 <td>Latishua Lewis </td>
                                 
                                 <td>Dual Enrollment </td>
                                 
                                 <td>Continue to develop and implement strategies to enhance Dual Enrollment processes
                                    and procedures. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/16-jacquelynenrollment_services_16-17_unit_plan.pdf">SA1617-16</a></td>
                                 
                                 <td>Jacquelyn Thompson </td>
                                 
                                 <td>Enrollment Services </td>
                                 
                                 <td>Strengthen mastery of content shared through Enrollment Service to increase student
                                    preparation.
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/17-tamika2016-2017unitplanwhitehousechallenge-1.pdf">SA1617-17</a></td>
                                 
                                 <td>Tamika Martin</td>
                                 
                                 <td>Financial Aid </td>
                                 
                                 <td>Create communications or workshops for employees to learn about the new student loan
                                    repayment options. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/18-tamika2016-2017unitplanwhitehousechallenge-2.pdf">SA1617-18</a></td>
                                 
                                 <td>Tamika Martin </td>
                                 
                                 <td>Financial Aid </td>
                                 
                                 <td>Monitor FWS awards, placement and development to benefit the Federal Work Study students.
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/19-debosd2016-2017unitplanfinal.pdf">SA1617-19</a></td>
                                 
                                 <td>Deb Larew </td>
                                 
                                 <td>Office for Students with Disabilities </td>
                                 
                                 <td>Conduct an OSD self-study to insure compliance with legal standards and best practices
                                    related to students with disabilities. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/20-edwin-recordsgraduationdap2016-17july2016.pdf">SA1617-20</a></td>
                                 
                                 <td>Edwin Sanchez </td>
                                 
                                 <td>Records &amp; Graduation </td>
                                 
                                 <td>Develop online forms in Atlas to assist students in submission, processing and identifying
                                    the status of their record changes. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/21-erica-standardizedtesting_2016-2017unitplan.pdf">SA1617-21</a></td>
                                 
                                 <td>Erica Reese </td>
                                 
                                 <td>Standardized Testing </td>
                                 
                                 <td>Continue the work and implementation of newly created Assessment Blackboard Course.
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/22-tracey-2016-2017unitplan.pdf">SA1617-22</a></td>
                                 
                                 <td>Tracey Olsen-Oliver </td>
                                 
                                 <td>Student Development </td>
                                 
                                 <td>Enhance or create/implement a variety of co-curricular experiences and opportunities.
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/23-sonya20162017lncunitplan.pdf">SA1617-23</a></td>
                                 
                                 <td>Sonya Joseph &amp; Mindy Smith </td>
                                 
                                 <td>Lake Nona Student Administration </td>
                                 
                                 <td>Strengthen relationship between Academic Affairs and Student Services at LNC. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/24-sonya20162017downtowncampusplan.pdf">SA1617-24</a></td>
                                 
                                 <td>Sonya Joseph </td>
                                 
                                 <td>Student Affairs Administration </td>
                                 
                                 <td>Create a Student Affairs plan for the Valencia/UCF Downtown Campus in collaboration
                                    with the UCF Student Affairs Team. 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/25-danielturner-2016-2017unitplan.pdf">SA1617-25</a></td>
                                 
                                 <td>Daniel Turner </td>
                                 
                                 <td>Title Pathways </td>
                                 
                                 <td>Increase accessibility of advising resources for students. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/26-nikky-unit_plan_transitions_planning_2016_2017.pdf">SA1617-26</a></td>
                                 
                                 <td>Nikky Ferrer </td>
                                 
                                 <td>Transition Planning </td>
                                 
                                 <td>Continue to strengthen and redesign exiting programs and outreach initiatives. </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><a href="/documents/students/student-services/unit-plans/27-kelly-valenciapromise-tsic2016-2017unitplan.pdf">SA1617-27</a></td>
                                 
                                 <td>Kelly Astro </td>
                                 
                                 <td>Valencia Promise </td>
                                 
                                 <td>Redesign and re-implementation of the Valencia Promise/Take Stock in Children program.
                                    
                                 </td>
                                 
                              </tr>
                              
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/unit-plans/index.pcf">©</a>
      </div>
   </body>
</html>