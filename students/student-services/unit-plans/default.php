<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Departmental Unit Action Plans | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/unit-plans/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/student-services/unit-plans/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Departmental Unit Action Plans</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li><a href="/students/student-services/unit-plans/">Unit Plans</a></li>
               <li>Departmental Unit Action Plans</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <h2>2016-17 Student Affairs Planning and Evaluation Links</h2>
                        
                        <p>To comply with SACS <em><strong>Principles of Accreditation</strong></em> requirements, <strong>Comprehensive Standards 3.3.1</strong>, each Student Affairs department will design and implement an annual plan to improve
                           student services and success.
                        </p>
                        
                        <p><strong>3.3.1</strong> The institution identifies expected outcomes for its educational programs and its
                           administrative and educational support services; assesses whether it achieves these
                           outcomes; and provides evidence of improvement based on analysis of those results.
                        </p>
                        
                        <p>The <strong>Index of Plans</strong> provided below includes an informational overview including a <strong>Department Action Plan (DAP)</strong> number, department of origin, campus, student affairs leader and a brief topical
                           focus for each plan. By clicking on a <strong>Division Action Plan (DAP)</strong> number you will be linked to current planning and evaluation documents.
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <font size="4">Index of Plans for 2016-17</font><br>
                                    Click on DAP Number to open file as a PDF Document
                                    
                                 </div>
                                 
                              </div> 
                              
                           </div>
                           
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h4>DAP</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Leader</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Department</h4>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <h4>Description</h4>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/1-Linda-ARRGDAP12016-2017.pdf">SA1617-01</a></div>
                                 
                                 <div data-old-tag="td">Linda Herlocker</div>
                                 
                                 <div data-old-tag="td">Admissions &amp; Records </div>
                                 
                                 <div data-old-tag="td">Fully implement the CRM for Recruit. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/2-Linda-ARRGDAP22016-2017.pdf">SA1617-02</a></div>
                                 
                                 <div data-old-tag="td">Linda Herlocker </div>
                                 
                                 <div data-old-tag="td">Admissions &amp; Records </div>
                                 
                                 <div data-old-tag="td">Create and implement enhanced department-wide communicaton stragegies to enhance team
                                    building college wide. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/3-Andy-Admissions-RegistrationDAP2016-17.pdf">SA1617-03</a></div>
                                 
                                 <div data-old-tag="td">Andy Oguntola </div>
                                 
                                 <div data-old-tag="td">Admissions &amp; Records </div>
                                 
                                 <div data-old-tag="td">Focus on CRM training, automating admissions letters, and creating beneficial edit
                                    reports. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/4-Reyna-AdmissionsRecordsSystemsDAP2016-17.pdf">SA1617-04</a></div>
                                 
                                 <div data-old-tag="td">Reyna Rangel</div>
                                 
                                 <div data-old-tag="td">Admission/Records Systems </div>
                                 
                                 <div data-old-tag="td">Provide system support and cross training for upcoming Recruit project. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/5-Joe-DAPAtlasLab2016-2017.pdf">SA1617-05</a></div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo </div>
                                 
                                 <div data-old-tag="td">Atlas Access Labs </div>
                                 
                                 <div data-old-tag="td">To continue with the creation and expansion of an online interactive training module
                                    for the support specialist II staff members, IWS, FWS and student leaders. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/6-LisaS-2016-2017UnitPlansAtlasInformationSystemsAdviseCRM.pdf">SA1617-06</a></div>
                                 
                                 <div data-old-tag="td">Lisa Stilke </div>
                                 
                                 <div data-old-tag="td">Atlas Information Systems </div>
                                 
                                 <div data-old-tag="td">Continued implementation of the Adviser CRM. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/7-LisaS-2016-2017UnitPlansAtlasInformationSystemsDegreeWorks.pdf">SA1617-07</a></div>
                                 
                                 <div data-old-tag="td">Lisa Stilke </div>
                                 
                                 <div data-old-tag="td">Atlas Information Systems </div>
                                 
                                 <div data-old-tag="td">Continued implementation of the Degree Works Plans tab.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/8-Tanisha-BTSUnitPlan2016_2017.pdf">SA1617-08</a></div>
                                 
                                 <div data-old-tag="td">Tanisha Carter </div>
                                 
                                 <div data-old-tag="td">Bridges to Success </div>
                                 
                                 <div data-old-tag="td">Improving our academic outcomes for our students by implementing strategies. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/9-CareerCenterBritt-Holmes-LoraSantosDAP16-17.pdf">SA1617-09</a></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Evelyn Lora-Santos, Ed Holmes, John Britt </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Career Centers College wide </div>
                                 
                                 <div data-old-tag="td">Along with OIT and Marketing, the Career Center will develop the content of the Career
                                    Center website. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/10-Amy-CollegeTransitions16-17.pdf">SA1617-10</a></div>
                                 
                                 <div data-old-tag="td">Amy Kleeman </div>
                                 
                                 <div data-old-tag="td">College Transitions </div>
                                 
                                 <div data-old-tag="td">Enhance and improve the enrollment process to be more seamless and student-friendly.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/11-Joe-DAPDOSEast2016-2017.pdf">SA1617-11</a></div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo</div>
                                 
                                 <div data-old-tag="td">Dean of Students East Campus </div>
                                 
                                 <div data-old-tag="td">Implementing Who's Next (an electronic queuing system) at East Campus.</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/12-Jill-WEAVEOSCDeanofStudentsTeam16-17b.pdf">SA1617-12</a></div>
                                 
                                 <div data-old-tag="td">Jill Szentmiklosi </div>
                                 
                                 <div data-old-tag="td">Dean of Students Osceola Campus </div>
                                 
                                 <div data-old-tag="td">Continue implementation of LifeMap 2.0 Advising to include CRM. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/13-Joe-DAPDOSWinterPark2016-2017.pdf">SA1617</a><a href="documents/13Joe-DOSWinterPark2015-2016.pdf">-13</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">Joe Sarrubbo </div>
                                 
                                 <div data-old-tag="td">Dean of Students Winter Park Campus</div>
                                 
                                 <div data-old-tag="td">The Winter Park DOS team will create its own team user's manual to guide new and existing
                                    staff. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/14-BenWDOS2016-2017UnitPlan.pdf">SA1617-14</a></div>
                                 
                                 <div data-old-tag="td">Ben Lion </div>
                                 
                                 <div data-old-tag="td">Dean of Students West Campus </div>
                                 
                                 <div data-old-tag="td">Evaluate the DOS-WC space and make recommendation to enhance the space to benefit
                                    student services. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/15-LatishuaDE2016-2017UnitPlan.pdf">SA1617-15</a></div>
                                 
                                 <div data-old-tag="td">Latishua Lewis </div>
                                 
                                 <div data-old-tag="td">Dual Enrollment </div>
                                 
                                 <div data-old-tag="td">Continue to develop and implement strategies to enhance Dual Enrollment processes
                                    and procedures. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/16-JacquelynEnrollment_Services_16-17_Unit_Plan.pdf">SA1617-16</a></div>
                                 
                                 <div data-old-tag="td">Jacquelyn Thompson </div>
                                 
                                 <div data-old-tag="td">Enrollment Services </div>
                                 
                                 <div data-old-tag="td">Strengthen mastery of content shared through Enrollment Service to increase student
                                    preparation.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/17-Tamika2016-2017UnitPlanWhiteHouseChallenge-1.pdf">SA1617-17</a></div>
                                 
                                 <div data-old-tag="td">Tamika Martin</div>
                                 
                                 <div data-old-tag="td">Financial Aid </div>
                                 
                                 <div data-old-tag="td">Create communications or workshops for employees to learn about the new student loan
                                    repayment options. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/18-Tamika2016-2017UnitPlanWhiteHouseChallenge-2.pdf">SA1617-18</a></div>
                                 
                                 <div data-old-tag="td">Tamika Martin </div>
                                 
                                 <div data-old-tag="td">Financial Aid </div>
                                 
                                 <div data-old-tag="td">Monitor FWS awards, placement and development to benefit the Federal Work Study students.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/19-DebOSD2016-2017UnitPlanfinal.pdf">SA1617-19</a></div>
                                 
                                 <div data-old-tag="td">Deb Larew </div>
                                 
                                 <div data-old-tag="td">Office for Students with Disabilities </div>
                                 
                                 <div data-old-tag="td">Conduct an OSD self-study to insure compliance with legal standards and best practices
                                    related to students with disabilities. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/20-Edwin-RecordsGraduationDAP2016-17July2016.pdf">SA1617-20</a></div>
                                 
                                 <div data-old-tag="td">Edwin Sanchez </div>
                                 
                                 <div data-old-tag="td">Records &amp; Graduation </div>
                                 
                                 <div data-old-tag="td">Develop online forms in Atlas to assist students in submission, processing and identifying
                                    the status of their record changes. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/21-Erica-StandardizedTesting_2016-2017UnitPlan.pdf">SA1617-21</a></div>
                                 
                                 <div data-old-tag="td">Erica Reese </div>
                                 
                                 <div data-old-tag="td">Standardized Testing </div>
                                 
                                 <div data-old-tag="td">Continue the work and implementation of newly created Assessment Blackboard Course.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/22-Tracey-2016-2017UnitPlan.pdf">SA1617-22</a></div>
                                 
                                 <div data-old-tag="td">Tracey Olsen-Oliver </div>
                                 
                                 <div data-old-tag="td">Student Development </div>
                                 
                                 <div data-old-tag="td">Enhance or create/implement a variety of co-curricular experiences and opportunities.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/23-Sonya20162017LNCunitplan.pdf">SA1617-23</a></div>
                                 
                                 <div data-old-tag="td">Sonya Joseph &amp; Mindy Smith </div>
                                 
                                 <div data-old-tag="td">Lake Nona Student Administration </div>
                                 
                                 <div data-old-tag="td">Strengthen relationship between Academic Affairs and Student Services at LNC. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/24-Sonya20162017downtowncampusplan.pdf">SA1617-24</a></div>
                                 
                                 <div data-old-tag="td">Sonya Joseph </div>
                                 
                                 <div data-old-tag="td">Student Affairs Administration </div>
                                 
                                 <div data-old-tag="td">Create a Student Affairs plan for the Valencia/UCF Downtown Campus in collaboration
                                    with the UCF Student Affairs Team. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/25-DanielTurner-2016-2017UnitPlan.pdf">SA1617-25</a></div>
                                 
                                 <div data-old-tag="td">Daniel Turner </div>
                                 
                                 <div data-old-tag="td">Title Pathways </div>
                                 
                                 <div data-old-tag="td">Increase accessibility of advising resources for students. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/26-Nikky-Unit_Plan_Transitions_Planning_2016_2017.pdf">SA1617-26</a></div>
                                 
                                 <div data-old-tag="td">Nikky Ferrer </div>
                                 
                                 <div data-old-tag="td">Transition Planning </div>
                                 
                                 <div data-old-tag="td">Continue to strengthen and redesign exiting programs and outreach initiatives. </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="documents/27-Kelly-ValenciaPromise-TSIC2016-2017UnitPlan.pdf">SA1617-27</a></div>
                                 
                                 <div data-old-tag="td">Kelly Astro </div>
                                 
                                 <div data-old-tag="td">Valencia Promise </div>
                                 
                                 <div data-old-tag="td">Redesign and re-implementation of the Valencia Promise/Take Stock in Children program.
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/unit-plans/default.pcf">©</a>
      </div>
   </body>
</html>