<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Student Services">
      <meta name="Keywords" content="college, school, educational, student, services">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <p>The Student Services office provides developmental advising which includes life, career,
                           and educational planning, interpretation of assessments, strategies to address academic
                           and personal challenges, programs to encourage student success skills, assitance with
                           understanding academic policies, preparation for university transfer, and financial
                           aid assistance.
                        </p>
                        
                        <hr>
                        
                        <h3>Admissions &amp; Records</h3>
                        This is where you'll find the Valencia application and the steps to enroll.<br><a title="Admissions &amp; Records" href="/admissions/admissions-records/index.php">Learn More</a><br>
                        
                        <h3>Advising &amp; Counseling</h3>
                        Academic advising staff and counselors provide developmental advising which includes
                        life, career and educational planning.<br><a title="Advising &amp; Counseling" href="/students/advising-counseling/index.php">Learn More</a><br>
                        
                        <h3>Answer Center</h3>
                        This is the first stop for students to submit an application, discuss financial aid,
                        inquire about orientation, and receive basic enrollment advising.<br> <a title="Answer Center" href="/students/answer-center/index.php">Learn More </a>
                        
                        <h3>Assessments</h3>
                        The Assessment office administers the PERT, LOEP, CLEP, TEAS, and other assessments
                        to Valencia's students and provides information regarding other College testing.<br> <a title="Assessments" href="/students/assessments/index.php">Learn More </a>
                        
                        <h3>Business Office</h3>
                        The office where questions related to student accounts are provided.<br> <a title="Business Office" href="/students/business-office/index.php">Learn More </a>
                        
                        <h3>Career Center</h3>
                        The Career Center offers a variety of career-related services to assist students in
                        exploring career options, making career decisions, and preparing for the workforce
                        (interviewing, resume, etc.).<br><a title="Career Center" href="/students/career-center/index.php">Learn More</a><br>
                        
                        <h3>Financial Aid Services</h3>
                        Applying for financial aid is a multi-step process. This site will guide you through
                        the process and provide you with information about all types of aid and scholarships.<br> <a title="Financial Aid Services" href="/admissions/finaid/index.php">Learn More </a>
                        
                        <h3>International Student Services</h3>
                        International Student Services (ISS) assists International students with the admission
                        process, academic advising, immigration questions, and other individual needs.<br> <a href="/students/international/index.html">Learn More </a>
                        
                        <h3>Internship &amp; Workforce Services</h3>
                        Internship and Workforce Services is where students and employers connect for opportunities
                        in a specific field such as employment or internship.<br> <a title="Internship &amp; Workforce Services" href="/students/internship/index.php">Learn More </a>
                        
                        <h3>Office of Students with Disabilities</h3>
                        The OSD exists to provide reasonable and appropriate accommodations for qualified
                        students with disabilities and to assist students in self-advocacy.<br> <a title="Office of Students with Disabilities" href="/students/office-for-students-with-disabilities/index.php">Learn More </a>
                        
                        <h3>Student Development</h3>
                        Student Development provides opportunities for students to develop personally, socially
                        and academically by offering co-curricular and extra-curricular activities, programs
                        and services.<br> <a title="Student Development" href="/students/student-development/index.php">Learn More </a>
                        
                        <h3>Find Solutions for Your Life</h3>
                        
                        <p>Get the complete list of upcoming Skillshops, including dates, times and locations.</p>
                        
                        <p><a title="Skillshops Brochure" href="/students/student-services/documents/valencia_skillshops.pdf" target="_blank">Fall 2017 Brochure</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/index.pcf">©</a>
      </div>
   </body>
</html>