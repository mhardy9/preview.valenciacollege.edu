<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Deans of Students  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/dean.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Deans of Students </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Deans of Students</h3>
                           
                           
                           
                        </div>
                        
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th>Osceola</th>
                                    
                                    <td><a href="mailto:jszentmiklosi@valenciacollege.edu">Dr. Jillian Szentmiklosi</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>East</th>
                                    
                                    <td><a href="mailto:jsarrubbo@valenciacollege.edu">Joe Sarrubbo</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>West</th>
                                    
                                    <td><a href="mailto:lherlocker@valenciacollege.edu">Linda Herlocker</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Winter Park</th>
                                    
                                    <td><a href="mailto:crobinson@valenciacollege.edu%20">Dr. Cheryl Robinson</a></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th>Lake Nona</th>
                                    
                                    <td><a href="mailto:mbosley@valenciacollege.edu">Dr. Mike Bosley</a></td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                           <p>&nbsp;</p>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" pe-7s-compass"></i>
                        
                        
                        <h3>Find Solutions for Your Life</h3>
                        
                        
                        <p>Get the complete list of upcoming Skillshops, including dates, times and locations.</p>
                        
                        <p><a href="/documents/students/student-services/skillshops.pdf" target="_blank">Spring 2017
                              Brochure</a></p>
                        
                        
                     </div>
                     
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/dean.pcf">©</a>
      </div>
   </body>
</html>