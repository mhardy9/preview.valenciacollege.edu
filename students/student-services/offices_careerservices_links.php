<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/offices_careerservices_links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Links</h2>
                        
                        <p><strong>Useful Links to Career Exploration and Educational Research 
                              information for students and community residents.</strong></p>
                        
                        <p>Career Research</p>
                        
                        <ul>
                           
                           <li>
                              <strong><a href="http://florida.echoices.com/eChoices/eChoices.nsf/frmlogin?OpenForm" target="_blank">Florida 
                                    eChoices</a><br>
                                 </strong> <strong> </strong>The career exploration and information 
                              system from Bridges.com for the state of Florida. 
                           </li>
                           
                        </ul>
                        
                        <p>Educational Research</p>
                        
                        <ul>
                           
                           <li>
                              <strong><a href="http://www.floridashines.org/" target="_blank">Floridashines.org</a><br>
                                 </strong>Florida Virtual College
                           </li>
                           
                        </ul>
                        
                        <p>Career Links </p>
                        
                        <ul>
                           
                           <li><a href="http://www.usnews.com/usnews/edu/eduhome.htm" target="_blank">US 
                                 News Education Online</a></li>
                           
                           <li><a href="http://www.acinet.org/acinet/" target="_blank">America's 
                                 Career Internet</a></li>
                           
                           <li><a href="http://collegeboard.org/explore/" target="_blank">College 
                                 Board Online</a></li>
                           
                           <li><a href="http://stats.bls.gov/ocohome.htm" target="_blank">Occupational 
                                 Outlook Handbook</a></li>
                           
                           <li>
                              <a href="http://www.petersons.com/" target="_blank">Peterson's 
                                 Guide to Colleges/Careers</a> 
                           </li>
                           
                        </ul>
                        
                        <p><a href="offices_careerServices_links.html#top">TOP</a></p>
                        
                        <p>Four Year Public Universities in Florida</p>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 
                                 <div>
                                    <p>Name</p>
                                 </div>
                                 
                                 <div>
                                    <p>City</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.famu.edu/" target="_blank">Florida A&amp;M 
                                       University</a></div>
                                 
                                 <div>
                                    <p>Tallahassee</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.fau.edu/" target="_blank">Florida Atlantic 
                                       University</a></div>
                                 
                                 <div>
                                    <p>Boca Raton</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.fgcu.edu/" target="_blank">Florida Gulf 
                                       Coast University</a></div>
                                 
                                 <div>
                                    <p>Fort Myers</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.fiu.edu/choice.html" target="_blank">Florida 
                                       International University</a></div>
                                 
                                 <div>
                                    <p>Miami</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.fsu.edu/" target="_blank">Florida State 
                                       University</a></div>
                                 
                                 <div>
                                    <p>Tallahassee</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.ncf.edu/" target="_blank">New College 
                                       of Florida</a></div>
                                 
                                 <div>
                                    <p>Sarasota</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.ucf.edu/" target="_blank">University 
                                       of Central Florida</a></div>
                                 
                                 <div>
                                    <p>Orlando</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.ufl.edu/" target="_blank">University 
                                       of Florida</a></div>
                                 
                                 <div>
                                    <p>Gainesville</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.unf.edu/" target="_blank">University 
                                       of North Florida</a></div>
                                 
                                 <div>
                                    <p>Jacksonville</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.usf.edu/" target="_blank">University 
                                       of South Florida</a></div>
                                 
                                 <div>
                                    <p>Tampa</p>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 <div><a href="http://www.uwf.edu/uwfMain/" target="_blank">University 
                                       of West Florida</a></div>
                                 
                                 <div>
                                    <p>Pensacola</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="offices_careerServices_links.html#top">TOP</a></p>
                        
                        <p> <span>Florida Private Colleges and Universities</span>
                           
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.barry.edu/barryhome.html" target="_blank">Barry 
                                 University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.db.erau.edu/" target="_blank">Embry-Riddle 
                                 Aeronautical University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.fit.edu/" target="_blank">Florida Institute 
                                 of Technology</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.collegenet.com/ataglanc/fl/flsouthern/aag.html" target="_blank">Florida 
                                 Southern College</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://fcn.state.fl.us/oraweb/owa/www_index.show?p_searchkey=4407&amp;pframe=1">Jacksonville 
                                 University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.lynn.edu/" target="_blank">Lynn University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.rollins.edu/" target="_blank">Rollins College</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.specdata.com/cgbbs/scbc.html" target="_blank">Smith 
                                 Chapel Bible College</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.stu.edu/" target="_blank">St. Thomas University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.stetson.edu/" target="_blank">Stetson University</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.ir.miami.edu/" target="_blank">University 
                                 of Miami</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.utampa.edu/" target="_blank">University 
                                 of Tampa</a> 
                              
                           </li>
                           
                           <li>
                              <a href="http://www.warner.edu/" target="_blank">Warner Southern 
                                 College</a> 
                              
                           </li>
                           
                           <li><a href="http://fcn.state.fl.us/icuf/" target="_blank">Independent 
                                 Colleges and Universities of Florida, Inc. (ICUF)</a></li>
                           
                        </ul>
                        
                        <p><a href="offices_careerServices_links.html#top">TOP</a></p>
                        <span>Florida Community Colleges </span> 
                        <ul>
                           
                           <li>
                              <a href="http://www.brevardcc.edu/" target="_blank">Brevard 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.broward.edu/" target="_blank">Broward Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.cfcccc.edu/" target="_blank">Central 
                                 Florida Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.chipola.edu/" target="_blank">Chipola Jr. 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.dbcccc.edu/" target="_blank">Daytona 
                                 Beach Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.edison.edu/" target="_blank">Edison Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.fccjcc.edu/" target="_blank">Florida 
                                 Community College at Jacksonville</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.fkcc.edu/" target="_blank">Florida Keys 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.gccc.edu/" target="_blank">Gulf Coast 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.hcccc.edu/" target="_blank">Hillsborough 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.ircccc.edu/" target="_blank">Indian River 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.lakecitycc.edu/" target="_blank">Lake 
                                 City Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.lscccc.edu/" target="_blank">Lake-Sumter 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.mccfl.edu/" target="_blank">Manatee Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.mdc.edu/" target="_blank">Miami-Dade Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.nfcc.edu/" target="_blank">North Florida 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.owcccc.edu/" target="_blank">Okaloosa-Walton 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.pbcccc.edu/" target="_blank">Palm Beach 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.phcc.edu/" target="_blank">Pasco-Hernando 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.pjccc.edu/" target="_blank">Pensacola 
                                 Jr. College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.polk.edu/" target="_blank">Polk Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.sjrcccc.edu/" target="_blank">St. Johns 
                                 River Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.spjccc.edu/" target="_blank">St. Petersburg 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.sfcc.edu/" target="_blank">Santa Fe Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.scc-fl.edu/" target="_blank">Seminole Community 
                                 College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.sfcccc.edu/" target="_blank">South Florida 
                                 Community College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.tcc.fl.edu" target="_blank">Tallahassee 
                                 Community College</a> 
                           </li>
                           
                           <li><a href="../../index.html" target="_blank">Valencia College</a></li>
                           
                        </ul>
                        
                        <p><a href="offices_careerServices_links.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/offices_careerservices_links.pcf">©</a>
      </div>
   </body>
</html>