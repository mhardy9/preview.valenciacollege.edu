<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transfer Readiness Information  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/transfer-readiness.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Transfer Readiness Information </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Transfer Readiness Information</h3>
                           
                           
                           
                        </div>
                        
                        
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Valencia&nbsp;<strong>Career Centers</strong>&nbsp;&nbsp;<a href="https://valenciacollege.edu/careercenter/">http://valenciacollege.edu/careercenter/</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>Unsure about your career direction? How to get there? Where am I going and more? Stop
                                    by the Career
                                    Center office on each of our campuses.
                                    
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>AA Transfer Plan/Program sheet&nbsp;&nbsp;<a href="https://valenciacollege.edu/aadegrees/premajors_list.cfm">http://valenciacollege.edu/aadegrees/premajors_list.cfm</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>Associate of Arts</strong>&nbsp;degrees are the freshman and sophomore years&nbsp; (or roughly 60
                                    credit hours) of a Bachelor’s Degree (roughly 120 credit hours).&nbsp;<strong>Your program sheet can
                                       provide you with the “recipe” for the course work needed to complete your
                                       degree.</strong>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>AS degrees/CPA Info/Program&nbsp;&nbsp;<a href="https://valenciacollege.edu/asdegrees/">http://valenciacollege.edu/asdegrees/</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>Associate of Science</strong>&nbsp;degrees offer you an opportunity to gain skill and knowledge
                                    and move into the workforce, or transfer to a college or university.&nbsp;<strong>Career Program
                                       Advisors</strong>&nbsp;are specialist to guide you as you secure your AS degree. Program sheets are
                                    available for all 34 AS degrees.&nbsp;<strong>Your program sheet is your “recipe” for degree
                                       completion</strong>.
                                    
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>Florida Virtual Campus</strong>&nbsp;<a href="http://www.floridashines.org/">http://www.floridashines.org/</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>&nbsp;Check out the valuable resources under the “College Transfer Center” tab</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>UCF Direct Connect</strong>&nbsp;<a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/">http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>How do I Direct Connect to UCF – this site will answer that question and others.</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>GPA</strong>&nbsp;Calculator&nbsp;&nbsp;<a href="https://valenciacollege.edu/advising-center/">http://valenciacollege.edu/advising-center/</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>&nbsp;Stay on top of your semester and cumulative grade point average. Use this free resource.</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>Transfer scholarship information</strong><a href="https://valenciacollege.edu/finaid/Scholarship_bulletin.cfm">http://valenciacollege.edu/finaid/Scholarship_bulletin.cfm</a>
                                 
                              </li>
                              
                              <ul>
                                 
                                 <li>Many colleges and universities off scholarships specifically for transfer students.</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>Glossary of Terms</strong>&nbsp;<a href="http://catalog.valenciacollege.edu/glossary/glossary.pdf">http://catalog.valenciacollege.edu/glossary/glossary.pdf</a>
                                 
                              </li>
                              
                           </ul>
                           
                           <p><strong>Additional tips to enhance your transfer readiness:</strong></p>
                           
                           <ul>
                              
                              <li>At Valencia , attend a Skillshop on transferring topics&nbsp;<a href="https://valenciacollege.edu/learning-support/skillshops.cfm">http://valenciacollege.edu/learning-support/skillshops.cfm</a>
                                 
                              </li>
                              
                              <li>Atlas can offer you options to view and print a degree audit – In your Atlas account,
                                 click on the
                                 Students tab, then scroll down to Path To Graduation – Run a Degree Audit under “My
                                 Academic
                                 Progress” to find out what you have left to take
                                 
                              </li>
                              
                              <li>Make your transfer easier:</li>
                              
                              <ul>
                                 
                                 <li>See what scholarships might be available. Check out the link above about transfer
                                    scholarship
                                    information.
                                    
                                 </li>
                                 
                                 <li>Speak with an Advisor at your desired university</li>
                                 
                                 <li>Shop around for the transfer institution that meets your educational goals.</li>
                                 
                                 <li>Plan ahead for transferring – Look up application deadlines, FAFSA deadlines, how
                                    to finance college
                                    and how/where you will live.
                                    
                                 </li>
                                 
                                 <li>Know what courses actually transfer to your intended institution and how they will
                                    be applied toward
                                    your degree.
                                    
                                 </li>
                                 
                                 <li>Stay focused on your current courses and your goals!</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           <p>&nbsp;</p>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" pe-7s-compass"></i>
                        
                        
                        <h3>Find Solutions for Your Life</h3>
                        
                        
                        <p>Get the complete list of upcoming Skillshops, including dates, times and locations.</p>
                        
                        <p><a href="/documents/students/student-services/skillshops.pdf" target="_blank">Spring 2017
                              Brochure</a></p>
                        
                        
                     </div>
                     
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/transfer-readiness.pcf">©</a>
      </div>
   </body>
</html>