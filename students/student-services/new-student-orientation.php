<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/new-student-orientation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h3>New Student Orientation</h3>
                        
                        <p>All new students are required to participate in an orientation session before registering
                           for their first term.&nbsp; 
                        </p>
                        
                        <p><strong>New Student Orientation for degree-seeking students includes:</strong></p>
                        
                        <ul>
                           
                           <li>Information on education planning</li>
                           
                           <li>Introduction to College resources including LifeMap tools</li>
                           
                           <li>Academic advising for first term</li>
                           
                        </ul>
                        
                        <p><strong>To Access New Student Orientation Sign Up</strong></p>
                        
                        <ol>
                           
                           <li>Submit your application and pay your $35 application fee</li>
                           
                           <li>Determine if you need assessment:<br><br>
                              
                              <ol>
                                 
                                 <li>If  you provide official test scores (SAT, ACT, CPT, PERT) that are less than 2-years
                                    old you do not need assessment and can move to step 3.
                                 </li>
                                 
                                 <li>If you are a transfer student and have taken and passed both a college-level English
                                    <u>and</u> math course with a C or better you do not need assessment and can move to step 3.&nbsp;
                                    Please note – if you only have one of these courses with a C or better you will need
                                    to take entry assessment for placement into the other course.
                                 </li>
                                 
                                 <li>If you have not attended college nor taken entry assessment – SAT, ACT, CPT, PERT
                                    – go to the Assessment Office on any campus.&nbsp; For information on study materials,
                                    hours, and locations visit:&nbsp; <a href="../assessments/index.html">valenciacollege.edu/assessments</a>.
                                 </li>
                                 
                              </ol>
                              
                           </li>
                           
                           <li>Once your application is processed (please allow 3 - 5 business days) set up your
                              Atlas account by going to <a href="https://atlas.valenciacollege.edu">atlas.valenciacollege.edu</a>.
                           </li>
                           
                           <li>After setting up your Atlas account and after entry assessment, sign up for orientation:<br><br>
                              
                              <ol>
                                 
                                 <li>Log into your Atlas account</li>
                                 
                                 <li>Click on New Student Orientation (My Atlas Tab; bottom left-hand side of page)</li>
                                 
                                 <li>Select the campus you wish for orientation</li>
                                 
                                 <li>Follow instructions to select and sign up for orientation</li>
                                 
                              </ol>
                              
                           </li>
                           
                        </ol>            
                        
                        <p>If you are transferring into Valencia from a regionally accredited institution, and
                           we have not yet received your 
                           official transcript(s), please provide us with unofficial copies prior to orientation.&nbsp;
                           You may fax or email these to the NSO office listed to the right.&nbsp; Also, if you have
                           any difficulty registering for an orientation, please contact the New Student Orientation
                           Office on your campus. 
                        </p>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           <h3>Find Solutions for Your Life</h3>
                           
                           <p>Get the complete list of upcoming Skillshops, including dates, times and locations.</p>
                           
                           
                           <p><a href="documents/valencia_skillshops.pdf" target="_blank">Fall 2017 Brochure</a></p>
                           
                        </div>
                        
                        
                        
                        
                        
                        <div role="navigation"> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/new-student-orientation.pcf">©</a>
      </div>
   </body>
</html>