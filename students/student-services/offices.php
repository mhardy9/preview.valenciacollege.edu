<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-services/offices.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-services/">Student Services</a></li>
               <li>Student Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Student Services Offices</h2>
                        
                        
                        <p>Student
                           Services offices are where you will receive specialized
                           assistance with issues that were more involved
                           or complex than those handled in the Answer Center. This
                           may be in the areas of financial aid, admissions,
                           or academic advising.
                        </p>
                        
                        
                        <p>A <strong>referral</strong> or <strong>appointment</strong> is
                           often required to meet with a staff member
                           in Student Services.
                        </p>
                        
                        
                        
                        <p><font color="#000000">Career Development Services 
                              - Building 2, Room 151</font></p>
                        
                        <blockquote>
                           
                           
                           <p>Click <a href="offices_careerServices.html">HERE 
                                 </a>to schedule an appointment or to learn more
                           </p>
                           
                        </blockquote> 
                        
                        <p><font color="#000000">Career Program Advising - Building 
                              3, 340</font></p>
                        
                        <blockquote>
                           
                           
                           <p>Click <a href="offices_careerProgram.html">HERE 
                                 </a>to schedule an appointment or to learn more
                           </p>
                           
                        </blockquote>
                        
                        <p><font color="#000000">Tutoring Services - Building 
                              3, The Learning Center</font></p>
                        
                        <blockquote>
                           
                           
                           <p>Click <a href="offices_tutoring.html">HERE</a> 
                              to schedule for tutoring or to learn more
                           </p>
                           
                        </blockquote>
                        
                        <p><font color="#000000">Office for Students with Disabilities 
                              - Building 2, 152</font></p>
                        
                        <blockquote>
                           
                           
                           <p>Click <a href="offices_osd.html">HERE 
                                 </a>to schedule an appointment or to learn more
                           </p>
                           
                        </blockquote>
                        
                        <p><a href="offices.html#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-services/offices.pcf">©</a>
      </div>
   </body>
</html>