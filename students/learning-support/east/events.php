<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Success Center, East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Academic Success Center, East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Academic Success Center, East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2><u>ASC Events</u></h2>
                        
                        <p>January 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Monday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Tuesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Wednesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Thursday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Friday</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>2<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>3</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>4</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>5<strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>6</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>9<strong><br>
                                                <span><strong>Classes Begin </strong></span></strong></p>
                                          
                                          <p><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>10<strong><br>
                                                </strong><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>11<strong><br>
                                                </strong></p>
                                          
                                          <p><strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>12<strong><br>
                                                </strong><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>13</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>16<br>
                                             <span><strong>Holiday - No School</strong></span></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>17</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>18</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>19</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>20</p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>23<br>
                                          <strong></strong> 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p>24<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p>25<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p>26<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>27 </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>30<strong><br>
                                             </strong></p>
                                    </div>
                                    
                                    <div>31<br>                      
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>February 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Monday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Tuesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Wednesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Thursday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Friday</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>1<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>2<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div> 
                                       <p>3 </p>
                                       
                                       <p><strong>Math Center &amp; General Tutoring - Closed from 3pm to 5pm </strong></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>6<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>7<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>8<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>9<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>10<br>
                                       <span><strong>Learning Day </strong></span>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>13<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>14<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>15<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>16<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>17</p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>20<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p>21<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>22<br>                      
                                    </div>
                                    
                                    <div>
                                       <p>23<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>24</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>27<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>28<br>                      
                                    </div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>March 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Monday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Tuesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Wednesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Thursday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Friday</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>1<br>
                                             <br>
                                             <br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>2<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>3</strong></p>
                                          
                                          <p><span><strong>Math Center &amp; General Tutoring - Closed from 3pm to 5pm </strong></span></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>6<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>7<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>8<strong> <br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>9<strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>10</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>13<br>
                                             <span><strong>SPRING BREAK </strong></span><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>14<br>
                                             <span><strong>SPRING BREAK </strong></span> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>15<br>
                                             <span><strong>SPRING BREAK </strong></span> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>16<br>
                                             <span><strong>SPRING BREAK </strong></span> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>17<br>
                                             <span><strong>SPRING BREAK </strong></span> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>20<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p>21<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>22<br>
                                       
                                    </div>
                                    
                                    <div>
                                       <p>23<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>24</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>27<br>
                                          
                                       </p>
                                    </div>
                                    
                                    <div>28<br>                      
                                    </div>
                                    
                                    <div>29<br>                      
                                    </div>
                                    
                                    <div>30 <br>                      
                                    </div>
                                    
                                    <div>31 </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p>April 2017 </p>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>Monday</div>
                                       </div>
                                       
                                       <div>
                                          <div>Tuesday</div>
                                       </div>
                                       
                                       <div>
                                          <div>Wednesday</div>
                                       </div>
                                       
                                       <div>
                                          <div>Thursday</div>
                                       </div>
                                       
                                       <div>
                                          <div>Friday</div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>3<br>
                                                <br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>4<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>5<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>6<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p><strong>7</strong></p>
                                             
                                             <p><span><strong>Math Center &amp; General Tutoring - Closed from 3pm to 5pm </strong></span></p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>10<strong><br>
                                                   </strong></p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>11<strong><br>
                                                   </strong></p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>12<strong><br>
                                                   </strong></p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>13<strong><br>
                                                   </strong></p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>14</div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>17<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>18<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>19<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>20<br>
                                                
                                             </p>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          <div>
                                             
                                             <p>21</p>
                                             
                                          </div>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <p>24<br>
                                             <span><strong>FINAL EXAMS </strong></span></p>
                                       </div>
                                       
                                       <div>
                                          <p>25<br>
                                             <span><strong>FINAL EXAMS </strong></span> 
                                          </p>
                                       </div>
                                       
                                       <div>26<br>
                                          <span><strong>FINAL EXAMS </strong></span>
                                          
                                       </div>
                                       
                                       <div>
                                          <p>27<br>
                                             <span><strong>FINAL EXAMS </strong></span> 
                                          </p>
                                       </div>
                                       
                                       <div>28<br>
                                          <span><strong>FINAL EXAMS </strong></span>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2540</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:00 am - 10:00 pm <br>Friday: 7:00 am - 7:00 pm<br>Saturday: 8:00 am - 4:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <a href="events.html">ASC Events &amp; News</a>
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/events.pcf">©</a>
      </div>
   </body>
</html>