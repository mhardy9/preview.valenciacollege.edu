<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/tutoring/hiring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/tutoring/">Tutoring</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/tutoring/index.html"></a>
                        
                        <p><img alt="tutoring" height="115" src="../../academic-success/tutoring/playing2_000.JPG" width="582"></p>
                        
                        <h3>Want to Be a Tutor? </h3>
                        
                        <h3>Positions Needed: </h3>
                        
                        <p><u><strong>Updated:</strong></u><strong>  5/5/2014</strong></p>
                        
                        <p><strong>None</strong></p>
                        
                        <h3>What to Do: </h3>
                        
                        <p>Candidates interested in becoming a tutor will need to pick up and fill out the Tutor
                           Application, attach their unofficial transcripts, and return both documents to the
                           ASC Welcome Desk in  4-101.
                        </p>
                        
                        <p>Completed applications will be collected and kept on file for one full year at the
                           ASC Welcome Desk. &nbsp;Once the time period has passed, the candidate must initial the
                           original application for it to remain valid longer than one year.&nbsp; Otherwise, it will
                           be shredded.
                        </p>
                        
                        <p>The application  collects basic information such as the candidate’s name, contact
                           information, area the candidate is interested in working, and space to briefly explain
                           why the candidate is interested in becoming a tutor. 
                        </p>
                        
                        <p>Area coordinators who need to hire tutors will review these applications. Here is
                           a brief description of the tutoring areas:
                        </p>
                        
                        <blockquote>
                           
                           <p><span><strong>Communications Center:</strong></span> The Communications Center focuses on assisting students with writing assignments
                              from any discipline. They help students to improve writing and communications skills.
                              For more information about the Communications Center, please click <a href="../../academic-success/writing/default.html">here</a>. 
                           </p>
                           
                           <p><span><strong>Math Support Center (MSC):</strong></span> The Math Support Center assists students with college-level math skills. MSC tutors
                              provide college-level support for all students including help with test corrections,
                              PERT review assistance, and homework. For more information about the Math Support
                              Center, please click <a href="../../academic-success/math/default.html">here</a>. 
                           </p>
                           
                           <p><span><strong>Specialized Preparatory Area (SPA):</strong></span> The SPA assists students with developmental math skills leading up tot he college-level.
                              They help students with coursework and preparing for competency exams by utilizing
                              a variety of resources such as hands-on manipulatives, carious technologies, and topic-based
                              workshops. For more information about the Specialized Preparatory Area, please click
                              <a href="../../academic-success/math/index.html">here</a>. 
                           </p>
                           
                           <p><u><strong>Foreign Language Lab</strong><strong>:</strong></u> The Foreign Language Lab assists students enrolled in foreign language  courses such
                              as Spanish, French, and German. Tutoring and workshops are  available to help students
                              grasp core concepts in order to become successful in  their foreign language courses.
                              For more information about the Foreign Language Lab, click <a href="../../academic-success/language/index.html">here</a>. 
                           </p>
                           
                           <p><span><strong>General Tutoring Center:</strong></span> General tutoring is provided for all other disciplines including Science, Business,
                              Foreign Language, and Computer Science. For more information regarding the General
                              Tutoring Center, click <a href="../../academic-success/tutoring/index.html">here</a>. 
                           </p>
                           
                        </blockquote>            
                        
                        <p>For a copy of the tutor application, click <a href="../../academic-success/tutoring/documents/RevisedTutorApplication--06-14-2013.pdf">here</a>. <strong>Make sure that you print it out, fill it out, and bring it, along with a copy of your
                              unofficial transcripts,  to the East Campus ASC Welcome Desk in  4-101. </strong></p>
                        
                        <p>For information regarding East Campus Supplemental Learning Leaders, contact <a href="mailto:jmccormick5@valenciacollege.edu">Jennifer McCormick</a>. 
                        </p>
                        
                        <p><a href="../../academic-success/tutoring/TutorHiring.html">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/tutoring/hiring.pcf">©</a>
      </div>
   </body>
</html>