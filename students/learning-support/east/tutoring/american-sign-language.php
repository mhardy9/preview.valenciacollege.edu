<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/tutoring/american-sign-language.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/tutoring/">Tutoring</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/tutoring/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>General Tutoring - American Sign Language (ASL) </h2>
                        
                        <div>
                           
                           <p> In an effort to provide more efficient service, the bulk of ASC General Tutoring
                              is walk-in. Students can meet with tutors during the times listed below - no appointment
                              is necessary! 
                           </p>
                           
                        </div>
                        
                        <p> For more Tutoring Center information, visit the ASC Welcome Desk in 4-101 or call
                           the desk at: 407-582-2540.
                        </p>
                        
                        <p>Tutoring Hours &amp; Location </p>
                        
                        <div>
                           
                           <h3>ASL tutoring is located in the <span>Room 4-101 Window Lounge. </span>
                              
                           </h3>
                           
                           <p><span><strong>Tutor availability is not guaranteed. We encourage you to call ahead if you need last
                                    minute details:</strong> 407-582-2540.</span></p>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>Monday</strong></div>
                                    
                                    <div>10:00 AM - 7:00 PM</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Tuesday</strong></div>
                                    
                                    <div>10:00 AM - 7:00 PM</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Wednesday</strong></div>
                                    
                                    <div>10:00 AM - 7:00 PM</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Thursday</strong></div>
                                    
                                    <div>10:00 AM - 7:00 PM</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Friday</strong></div>
                                    
                                    <div>CLOSED</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Saturday</strong></div>
                                    
                                    <div>CLOSED</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="../../academic-success/tutoring/americansignlanguage.html#top">TOP</a></p>
                           
                           
                        </div>
                        
                        <p>Meet Our Tutors </p>
                        
                        <p><img alt="Avery Hughes" height="207" hspace="0" src="../../academic-success/tutoring/averyhuges.jpg" vspace="0" width="155"></p>
                        
                        <p><u><strong> Avery Hughes </strong></u></p>
                        
                        <p> I enjoy long walks on the beach… and pizza. </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Kayla Geide" height="202" hspace="0" src="../../academic-success/tutoring/kaylageide.jpg" vspace="0" width="160"></p>
                        
                        <p><strong><u> Kayla Geide </u></strong></p>
                        
                        <p>After graduating high school in Key West, I moved to Orlando for Valencia and started
                           the ASL program. I fell in love with the language and the people in the program. I
                           have a standard poodle. We love going to the park and taking nature hikes together
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Sam Rosario" height="191" src="../../academic-success/tutoring/Samueledit.jpg" width="200"></p>
                        
                        <p><u><strong> Samuel Rosario </strong></u></p>
                        
                        <p> I was born in Puerto Rico and raised in Florida. I am also proud to be deaf! I am
                           currently a part of the American Sign Language program interpreting and I enjoy working
                           with all the ASL students. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        <h3><strong>Tutor Feedback:</strong></h3>
                        
                        <p>Recently had a tutoring session or have general feedback? Tell us how we're doing!
                           Please complete the Tutor Survey <a href="https://docs.google.com/spreadsheet/viewform?formkey=dE1yOVJzQmtHM2VHTjZvcHZIYV9HS2c6MQ">here</a>. 
                        </p>
                        
                        <p><a href="../../academic-success/tutoring/americansignlanguage.html#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/tutoring/index.html">
                                          Academic Success Center - Tutoring 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Downstairs Building 4</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2540</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:00 am - 10:00 pm <br>Friday: 7:00 am - 12:00 pm<br>Saturday: 8:00 am - 2:00 pm <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <p><a href="../virtuallibrary.html#ASLvirtual">American Sign Language - Virtual Library</a></p>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/tutoring/american-sign-language.pcf">©</a>
      </div>
   </body>
</html>