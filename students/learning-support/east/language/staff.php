<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Meet Our Staff </h2>
                        
                        <h3>Language Lab Coordinator</h3>
                        
                        <p> <img alt="Aysha Aslam Senior Instructional Assistant" src="../../academic-success/language/Aysha2.jpg"> 
                        </p>
                        
                        <p><strong>Aysha Aslam : Foreign Language Lab Coordinator </strong></p>
                        
                        <p>Aysha is the supervisor of both the Foreign Language Lab and the EAP Lab. </p>
                        
                        <p>Language Tutors </p>
                        
                        <h3><img alt="Kauany" height="165" hspace="0" src="../../academic-success/language/KauanyBwebsite.jpg" vspace="0" width="150"></h3>
                        
                        <h3>Kauany Brown : <span>Tutors Spanish</span>
                           
                        </h3>
                        
                        <p> Kauany Brown holds a double major in Spanish and French and a Spanish translation
                           certificate from the Univesity of Central Florida (UCF). In her spare time, she knists
                           hats and scarves that she'll barely ever wear since it's too hot.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="chantelle" height="194" hspace="0" src="../../academic-success/language/chantelle.jpg" vspace="0" width="150"></p>
                        
                        <h3>Chantelle Cade : <strong>Tutors French </strong>
                           
                        </h3>
                        
                        <p>Chantelle Cade has been studying French for two years and continues to do so at the
                           advanced 3000 level. She attends Rollins College where she is majoring in Psychology.
                           She wants to continue to study French and other languages to use in her career. However,
                           as of now, learning new languages is just for fun and her personal enrichment. In
                           her spare time she sings with the Bach Festival Choir of Winter Park, reads, writes
                           and studies. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3><img alt="Gordana" height="161" hspace="0" src="../../academic-success/language/Gordanawebsite.jpg" vspace="0" width="150"></h3>
                        
                        <h3>Gordana Katusic : <span>Tutors Spanish and French</span>
                           
                        </h3>
                        
                        <p>Professor Gordana Katusic is of Croatian origen, was born in Venezuela, and lived
                           there for 13 years. She arrived to the United States in 1963. She lived in Cincinnati,
                           Ohio and graduated from the University of Cincinnati with a Masters Degree of Spanish
                           and also with a Bachelor of Science in education for teaching and a Bachelor of Arts
                           for Spanish and French. She has taught at the University of Central Florida and is
                           currently a professor at Valencia. In addition to teaching ESL, Spanish, and French,
                           Professor Katusic owned a Language school for 13 years.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3><img alt="Danny Montilla" height="149" hspace="0" src="../../academic-success/language/dannymontillapic.png" vspace="0" width="150"></h3>
                        
                        <h3>Danny Montilla : <span>Tutors Spanish and French</span>
                           
                        </h3>
                        
                        <p>Danny Montilla has been tutoring French and Spanish at Valencia since May 2008. He
                           graduated from Valencia in 2010 with an A.A. in General Studies and holds a Bachelor
                           of Arts in Spanish and a Minor in French at UCF. Danny plans on attending Graduate
                           School for a Master of Arts in Spanish. His long term goal is to teach Spanish and
                           French at the college level (and later hopefully German and Portuguese as well).
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Paula Tutor" height="272" hspace="0" src="../../academic-success/language/PaulaN.jpg" vspace="0" width="150"></p>
                        
                        <h3>Paula Naranjo : Tutors Spanish</h3>
                        
                        <p>Paula Naranjo is currently majoring in Music. She has already obtained an AA in Music
                           Performance (Voice) from Valencia College and an AS in Travel and Tourism from MBTI
                           Business Training Institute in Puerto Rico. She really enjoys singing and spending
                           quiet Sunday afternoons watching movies with her family. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><a href="../../academic-success/language/meetourstaff.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/staff.pcf">©</a>
      </div>
   </body>
</html>