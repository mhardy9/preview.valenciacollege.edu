<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/placement-test.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>The WebCAPE Exam - Foreign Language Placement </h2>
                        
                        <p>The purpose of the WebCAPE (Computer Adaptive Placement Exam) is to determine foreign
                           language proficiency and corresponding course level at Valencia College using a combination
                           of grammar, reading, and vocabulary questions. In order to achieve the most appropriate
                           placement consistent with language level, students are expected to complete the placement
                           exam to the best of their ability, without the use of textbooks, dictionaries, or
                           any other type of assistance. <strong>The placement test is free for registered Valencia students.</strong></p>
                        
                        <p><img alt="books" height="150" src="../../academic-success/language/a.gif" width="197"></p>
                        
                        <p>Who should take the exam?</p>
                        
                        <p>Students with knowledge or previous study of French, German, or Spanish who intend
                           to continue studying the language at Valencia.&nbsp; 
                        </p>
                        
                        
                        <p>Who should <strong>NOT</strong> take the exam?
                        </p>
                        
                        <p>Students who wish to receive college credit for courses they are proficient in should
                           not take the placement exam. The WebCAPE merely places students in the appropriate
                           foreign language course and does <strong>not</strong> count towards college credit.
                        </p>
                        
                        <p>How the Exam works</p>
                        
                        <p>After starting the WebCAPE exam, the student enters a password and responds to questions
                           regarding their previous language experience in order to create a student profile.
                           Once the student profile is complete, the computer prepares the student for the test
                           by briefly explaining that the student is to respond to multiple choice questions
                           by typing and confirming the letter of the correct answer. As a student proceeds through
                           the test, the computer selects and displays items based upon his or her responses
                           to previous items. The adaptive testing algorithm has been written so that the first
                           six questions serve as “level checkers.” After the first six items, the test begins
                           to “probe” in order to fine-tune the measurement by increasing or decreasing the difficulty
                           by one level after each response.
                        </p>
                        
                        <p>The test terminates if 1) the student incorrectly answers four questions at the same
                           difficulty level, or 2) the student answers five questions at the highest difficulty
                           level possible. At the conclusion of the test, the computer displays the performance
                           level of the student.
                        </p>
                        
                        <p>Where do I take exam?</p>
                        
                        <p>Students must come to the Foreign Language Lab in Building 4, Room 104 to be enrolled
                           in the exam. A Valencia ID is required to take the exam. Your results will then be
                           sent to the Humanities &amp; Foreign Language department if you have qualified for an
                           override. 
                        </p>
                        
                        
                        <p>How can I receive credit for my foreign language proficiency? </p>
                        
                        <p>Students may take the CLEP Exam to receive up to 8 credit hours for proficiency in
                           Spanish, French, or German. The CLEP Exam is administered in the Assessment Services
                           Office in Bldg. 5, Rm. 237. 
                        </p>
                        
                        <p>Placement Scores </p>
                        
                        <p><u>SPANISH:</u></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>0-299</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary Spanish I (SPN1120) </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>300-399</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary Spanish II (SPN1121)</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>400-499</p>
                                    </div>
                                    
                                    <div>
                                       <p>Intermediate Spanish I (SPN2200)</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>500 and above</p>
                                    </div>
                                    
                                    <div>
                                       <p>Intermediate Spanish II (SPN2201)<br>
                                          Eligible for Heritage Speakers (SPN1340)
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><u>FRENCH:</u></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>0-259</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary French I (FRE1120) </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>260-336</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary French II (FRE1121)</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>337-402</div>
                                    
                                    <div>Intermediate French I (FRE2200)</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>403 and above</p>
                                    </div>
                                    
                                    <div>
                                       <p>Intermediate French II (FRE2201)</p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>&nbsp;<u>GERMAN:</u></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>0-283</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary German I (FRE1120) </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>284-389</p>
                                    </div>
                                    
                                    <div>
                                       <p>Elementary German II (FRE1121)</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p>390 and above</p>
                                    </div>
                                    
                                    <div>
                                       <p>Take the CLEP Exam (Recommended)</p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../academic-success/language/foreignlanguageplacementtest.html#top">TOP</a></p>
                        
                        <h2>&nbsp;</h2>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/placement-test.pcf">©</a>
      </div>
   </body>
</html>