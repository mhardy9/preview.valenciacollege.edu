<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/lab-faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <p>What do I need to bring with me for tutoring?</p>
                        
                        <ul>
                           
                           <li>Valencia Student ID</li>
                           
                           <li>Textbook</li>
                           
                           <li>Notes</li>
                           
                           <li>Questions for our Tutors</li>
                           
                        </ul>
                        
                        <p>How do I check in when I come to the Foreign Language Lab? </p>
                        
                        <p>Use your Valencia student ID to sign in at our computer. When you scan your ID, you
                           can select one of the following three options:
                        </p>
                        
                        <ol>
                           
                           <li>Tutoring</li>
                           
                           <li>Workshops</li>
                           
                           <li>General Lab use</li>
                           
                        </ol>
                        
                        <p>Select the reason you are visiting the lab and click "Sign In".</p>
                        
                        <p>My professor is choosing to give me extra credit for using the lab. How will they
                           know I have come? 
                        </p>
                        
                        <p>You must sign in AND sign out for your instructor to know you were here.</p>
                        
                        <p>How do I check out a textbook, dictionary, or reference book?</p>
                        
                        <p>Tell one of our lab staff or tutors that you would like to check out an item. You
                           need to present your Student ID to check out anything from the lab.
                        </p>
                        
                        <p>Can I check out a textbook and take it with me to class?</p>
                        
                        <p>Yes. You must return the textbook before it is due back to avoid paying a fine. Textbooks
                           are due back within 2 hours of their check out time.
                        </p>
                        
                        <p>How do I know when my favorite tutor will be tutoring in the Foreign Language Lab?</p>
                        
                        <p>Our tutors’ schedules are posted inside the Lab. You can also call or ask any of the
                           lab staff for this information.
                        </p>
                        
                        <p>Is one-on-one tutoring available?</p>
                        
                        <p>All of our tutoring is available on a walk-in basis. If you are the only student in
                           the lab while a tutor is there, then you will be able to receive one-on-one assistance.
                           If there are multiple students in the lab, our tutors are trained to spend equal amounts
                           of time assisting each student. A tutor will work with you on one concept, make sure
                           you understand the material, and then check on the other students. 
                        </p>
                        
                        <p>How do I sign up for a workshop?</p>
                        
                        <p>You can sign up for a workshop in person or online. You can view the <a href="../../academic-success/language/flworkshops.html">calendar of workshops</a> and sign up using our <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/language/Workshopsign-up.cfm" target="_blank">online form</a>. 
                        </p>
                        
                        <p>I don’t need tutoring but need a place to study for my course. Can I use the lab as
                           a study space?
                        </p>
                        
                        <p>Yes, the lab is a great place to study, and tutors are available in case you do have
                           questions. Please keep in mind that tutoring and workshops do occur in the lab so
                           it is not a designated quiet study area. 
                        </p>
                        
                        <p>Can I recieve extra credit for attending a workshop? </p>
                        
                        <p>Students can recieve extra credit for attending the workshops but this is at the sole
                           discretion of their instructor. 
                        </p>
                        
                        <p>Can I sign up in advance for a workshop? </p>
                        
                        <p>Yes. You can sign up online or in person. However, we welcome walk-in attendees to
                           all of our workshops.
                        </p>
                        
                        <p>Do I have to sign in when I attend a workshop? </p>
                        
                        <p>Yes. All students must sign in at the computer in the front when they come for a workshop.
                           
                        </p>
                        
                        <p>Where can I find the workshop schedule? </p>
                        
                        <p>The workshop schedule is posted online at the following address: <br>
                           <a href="../../academic-success/language/flworkshops.html">http://valenciacollege.edu/east/academicsuccess/language/flworkshops.cfm</a> 
                        </p>
                        
                        <p><a href="../../academic-success/language/labfaq.html#top">TOP</a></p>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/lab-faq.pcf">©</a>
      </div>
   </body>
</html>