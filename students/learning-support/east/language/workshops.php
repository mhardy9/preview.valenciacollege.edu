<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        <h3>Foreign Language Workshops </h3>
                        
                        <p><strong>This program is being revised. Stay tuned for exciting upcoming information about
                              our new online, self-paced workshop series! </strong>          
                        </p>
                        
                        <h3><strong>Questions? Comments? </strong></h3>
                        
                        <p>If you have any questions, please contact the Foreign Language Lab Coordinator, <a href="mailto:aaslam2@valenciacollege.edu">Aysha Aslam</a>. 
                        </p>
                        
                        <p>If you have comments or suggestions about any workshps you attended,<br>
                           please refer to our <a href="https://docs.google.com/spreadsheet/viewform?formkey=dDN3eC12aU8zRW5INDNZdDJyVVp1UkE6MQ">workshop feedback form</a>. 
                        </p>
                        
                        <h3>Schedule</h3>
                        
                        <div>
                           
                           <h4>May 2016 </h4>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Monday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Tuesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Wednesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Thursday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Friday</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>May 2</p>
                                          
                                          <p><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>3</p>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>4</p>
                                          
                                          <p><span><br>
                                                </span></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>5</p>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>6</p>
                                          
                                          <p><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>9</p>
                                          
                                          <p>1st Day of Classes </p>
                                          
                                          <p><strong><br>
                                                </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>10</p>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>11</p>
                                          
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>12</p>
                                          
                                          <p>Week 1 FRE1120 Workshop 11:15 AM - 12 PM</p>
                                          
                                          <p>Week 1 FRE1120 Workshop 2:15 PM - 3 PM</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>13<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>16</p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>17</p>
                                       
                                       <p>Week 2 FRE1120 Workshop 11:15 AM - 12 PM</p>
                                       
                                       <p>Week 2 FRE1120 Workshop 2:15 PM - 3 PM</p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>18</p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>19</p>
                                       
                                       <p>Week 2 FRE1120 Workshop 11:15 AM - 12 PM</p>
                                       
                                       <p>Week 2 FRE1120 Workshop 2:15 PM - 3 PM</p>
                                       
                                    </div>
                                    
                                    <div>20</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>23</p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>24</p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p>25</p>
                                       
                                    </div>
                                    
                                    <div>26</div>
                                    
                                    <div>27</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <p><a href="../../academic-success/language/flworkshops.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/workshops.pcf">©</a>
      </div>
   </body>
</html>