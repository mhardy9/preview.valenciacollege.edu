<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/spanish-worksheets.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Spanish : Learning Resources </h2>
                        
                        <p><strong>How do I check out these items?</strong></p>
                        
                        <p>You must bring your Valencia ID to check out any lab resources. Our lab staff will
                           let you know when the items are due back.
                        </p>
                        
                        <p>Books and Textbooks </p>
                        
                        <p>In addition to a wide variety of dictionaries, we also have the following textbooks:</p>
                        
                        <ul>
                           
                           <li>Exploraciones</li>
                           
                           <li>English Grammar for Students of Spanish</li>
                           
                           <li>501 Spanish Verbs</li>
                           
                        </ul>
                        
                        <p>Printable Worksheets</p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>SPN1120</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/Adjectives.docx">Adjectives</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/DefiniteandIndefiniteArticles.docx">Definite and Indefinite Articles </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/DefiniteArticlesWorksheet.docx">Definite Articles Worksheets</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_1.doc">Exploraciones - Chapter 1</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_2.doc">Exploraciones - Chapter 2 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_3.doc">Exploraciones - Chapter 3 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_4.doc">Exploraciones - Chapter 4 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_5.doc">Exploraciones - Chapter 5 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/GenderofNounsWorksheet.docx">Gender of Nouns</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/NounsHandout.pdf">Nouns</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/PossessiveAdjectives.docx">Possessive Adjectives</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/RegularVerbs.docx">Regular Verbs</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/StemChangingRefrence.docx">Stem Changing Verbs</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/UsefulWordsandPhrasestoHelpwithSpanishWritingTasks.pdf">Useful Words and Phrases </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>SPN1121</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_6.doc">Exploraciones - Chapter 6 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_7.doc">Exploraciones - Chapter 7 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_8.doc">Exploraciones - Chapter 8 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_9.doc">Exploraciones - Chapter 9 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_10.doc">Exploraciones - Chapter 10 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_11.doc">Exploraciones - Chapter 11 </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_12.doc">Exploraciones - Chapter 12</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_13.doc">Exploraciones - Chapter 13</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/Explo_Worksheets_Ch_14.doc">Exploraciones - Chapter 14</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/IrregularverbsinthePreterite.docx">Irregular Verbs in the Preterite</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/Preteriteverbs.docx">Preterite Verbs</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/ReflexivePronounsHandout.docx">Reflexive Pronouns</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/SPN1121WS-ReflexiveVerbs.doc">Reflexive Verbs </a></span></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <h2><a href="../../academic-success/language/documents/SPN1120FinalExamReviewPDF_000.pdf">SPN1120 Final Exam Review PDF</a></h2>
                           
                        </div>
                        
                        <p>External Links </p>
                        
                        <h3><em>Spanish</em></h3>
                        
                        <p><a href="http://www.abc.es/">ABC News </a></p>
                        
                        <p><a href="http://www.bbc.co.uk/mundo/index.shtml">http://www.bbc.co.uk/mundo/index.shtml</a> 
                        </p>
                        
                        <p><a href="http://conjuguemos.com/home/docs/php/list.php?type=verbs&amp;division=verbs&amp;language=spanish">Conjuguemos.com</a></p>
                        
                        <p> <a href="http://www.bbc.co.uk/languages/spanish/">BBC - Languages - Learn Spanish</a></p>
                        
                        <p> <a href="http://www.bbc.co.uk/languages/spanish/">BBC - Languages - Spanish</a></p>
                        
                        <p> <a href="http://news.bbc.co.uk/hi/spanish/news/">BBC Mundo Portada</a> 
                        </p>
                        
                        <p><a href="http://www.diccionarios.com/">Diccionarios.com</a></p>
                        
                        <p><a href="http://elpais.com/">El País</a></p>
                        
                        <p><a href="http://www.expansion.com/">Expansión</a></p>
                        
                        <p> <a href="http://www.italki.com/">italki - Language Exchange and Learning Community</a></p>
                        
                        <p> <a href="http://www.spaleon.com/index.php">Learn Spanish online</a> 
                        </p>
                        
                        <p><a href="http://www.studyspanish.com/">Learn Spanish</a></p>
                        
                        <p><a href="http://cwx.prenhall.com/bookbind/pubbooks/mosaicos2/">Mosaicos</a></p>
                        
                        <p><a href="http://quizlet.com/search/spanish/">Quizlet Search</a></p>
                        
                        <p><a href="http://www.myhq.com/public/v/a/vanderwerken/">myHq Spanish Websites — MRTS</a></p>
                        
                        <p><a href="http://quizlet.com/search/spanish/">Quizlet Search</a></p>
                        
                        <p> <a href="http://www.colby.edu/~bknelson/SLC/index.php">Spanish Grammar Exercises</a> 
                        </p>
                        
                        <p><a href="http://spanish.about.com/">Spanish Language learn Spanish grammar, vocabulary and culture</a> 
                        </p>
                        
                        <p><a href="http://ielanguages.com/spanish.html">Spanish Tutorials Index Basic Phrases, Vocabulary and Grammar</a></p>
                        
                        <p> <a href="http://spanish.typeit.org/">Type Spanish accents online</a> 
                        </p>
                        
                        <p><a href="http://www.verbix.com/webverbix/index.asp">Verbix -- conjugate verbs in 100+ languages</a></p>
                        
                        <p><a href="http://www.wordreference.com/conj/EsVerbs.asp">WordReference.com</a> 
                        </p>
                        
                        <p><a href="http://www.profedeele.es/">http://www.profedeele.es/</a><span><br>
                              </span></p>
                        
                        <h3><em>Portuguese</em></h3>
                        
                        <p><a href="http://expresso.sapo.pt/">Expresso</a></p>
                        
                        <p><a href="http://www.folha.uol.com.br/">Folha de São Paulo</a></p>
                        
                        <p><a href="http://expresso.sapo.pt/">Jornal O Globo</a><span><a href="http://expresso.sapo.pt/"></a></span></p>
                        
                        <h3><em>Other Links</em></h3>
                        
                        <p> <a href="https://atlas.valenciacollege.edu/">Atlas</a> 
                        </p>
                        
                        <p><a href="http://espaces.vhlcentral.com/">Espaces Supersite</a></p>
                        
                        <p><a href="http://books.quia.com/books">Quia - Quia Books</a></p>
                        
                        <p> <a href="http://typeit.org/">TypeIt</a> 
                        </p>
                        
                        <p><a href="../../../index.html">Valencia College</a></p>
                        
                        <p> <a href="http://webct6.valenciacollege.edu/webct/entryPageIns.dowebct">Blackboard 9 </a> <br>
                           <br>
                           <a href="http://www.datehookup.com/content-the-romance-language-resource-center.htm">The Romance Language Resource Center</a> <br>
                           <br>
                           <a href="http://libguides.valenciacollege.edu/content.php?pid=382602&amp;sid=3135543">Mango Lib Guides</a> 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/spanish-worksheets.pcf">©</a>
      </div>
   </body>
</html>