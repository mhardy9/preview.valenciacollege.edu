<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/spanish.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Spanish</h2>
                        
                        <p><strong>Spanish Walk In Tutoring Hours</strong></p>
                        
                        <blockquote>
                           
                           <p><strong>Monday</strong>: 8am-6pm
                           </p>
                           
                           <p><strong>Tuesday</strong>: 9am-7pm
                           </p>
                           
                           <p><strong>Wednesday</strong>: 8am-6pm
                           </p>
                           
                           <p><strong>Thursday</strong>: 9am-6pm
                           </p>
                           
                           <p><strong>Friday</strong>: 9am-3pm 
                           </p>
                           
                        </blockquote>            
                        
                        <p>No Appointment is needed. Please bring your Valencia ID. See this link for more information
                           about how to get the most out of your tutoring experience: <a href="../../academic-success/language/whattoexpect.html">What to Expect From Tutoring </a></p>
                        
                        <p>The Foreign Language Lab offers learning support resources for students enrolled in
                           the following Spanish courses:
                        </p>
                        
                        <ul>
                           
                           <li>Conversational Spanish I</li>
                           
                           <li>Conversational Spanish II </li>
                           
                           <li> Elementary Spanish I</li>
                           
                           <li>Elementary Spanish II</li>
                           
                           <li> Intermediate Spanish I</li>
                           
                           <li>Intermediate Spanish II</li>
                           
                           <li>Spanish for Heritage Speakers I</li>
                           
                           <li>Spanish for Heritage Speakers II </li>
                           
                        </ul>
                        
                        <p>Spanish Online Tutoring </p>
                        
                        <p>Based on feedback from students and faculty, Valencia has chosen to use SmarThinking
                           for online tutoring. This is a free service for Valencia students. To access SmarThinking,
                           you can log on to your Atlas Account. 
                        </p>
                        
                        <p><img alt="Atlas Access for SmarThinking" height="293" src="../../academic-success/language/atlas-smartthinkingpic.jpg" width="435"></p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/spanish.pcf">©</a>
      </div>
   </body>
</html>