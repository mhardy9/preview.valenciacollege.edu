<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/tutoring-hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a> 
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>FOREIGN LANGUAGE WALK-IN TUTORING Hours - Fall 2016 (4-104) </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>* Tutoring begins the second week of classes</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div> COURSE </div>
                                    
                                    <div> DAY </div>
                                    
                                    <div> TIMES </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>                      
                                       <p><strong>FRENCH:<br>
                                             -All Levels</strong></p>
                                       
                                    </div>
                                    
                                    <div>
                                       <div><strong>MONDAY<br>
                                             </strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>12:00pm - 6:00pm <strong><br>
                                             </strong>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>TUESDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>11:00am - 4:00pm  </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>WEDNESDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <em>12:00pm - 6:00pm</em> (<em><strong>FRE1120</strong> Tutoring Available</em>)
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>THURSDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>11:00am - 4:00pm </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>FRIDAY</strong></div>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <div><strong>SPANISH:<br>
                                                -All Levels </strong></div>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>MONDAY<br>
                                             </strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>8:00am - 6:00pm <br>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>TUESDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>9:00am - 7:00pm  </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>WEDNESDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>8:00am - 6:00pm </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>THURSDAY</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>9:00am - 6:00pm </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>FRIDAY </strong></div>
                                    </div>
                                    
                                    <div>
                                       <div>9:00am - 3:00pm </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>GERMAN:<br>
                                             -All Levels </strong></div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>Tuesday<br>
                                             </strong></div>
                                    </div>
                                    
                                    <div>6:00pm - 7:00pm </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>Online German tutoring is also available by appointment </div>                    
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>*Online &amp; Face-to-Face Latin Tutoring is available by appointment. Please e-mail Aysha
                                          Aslam at aaslam2@valenciacollege.edu to schedule an appointment. </strong></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <blockquote>
                                 
                                 <blockquote>
                                    
                                    <blockquote>&nbsp;</blockquote>
                                    
                                 </blockquote>
                                 
                              </blockquote>
                              
                           </div>
                           
                        </div>
                        
                        <blockquote>
                           
                           <blockquote>
                              
                              <blockquote></blockquote>
                              
                           </blockquote>
                           
                        </blockquote>
                        
                        <p><a href="../../academic-success/language/FLtutoringhours.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/tutoring-hours.pcf">©</a>
      </div>
   </body>
</html>