<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/latin.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Latin</h2>
                        
                        <p>Latin tutoring is available, but only through special request.</p>
                        
                        <p>Tutoring</p>
                        
                        <p>Latin Tutoring is available online and Face-to-face ONLY by appointment at this time.
                           You may e-mail Aysha Aslam at aaslam2@valenciacollege.edu for more information or
                           to schedule a tutoring appointment. 
                        </p>
                        
                        <h2><strong>Books and Textbooks </strong></h2>
                        
                        <p>You must bring your Valencia ID to check out any lab resources. Our lab staff will
                           let you know when the items are due back.
                        </p>
                        
                        <p>In addition to a wide variety of dictionaries, we also have the following textbooks:</p>
                        
                        <ul>
                           
                           <li>Wheelock's Latin</li>
                           
                           <li>English Grammar for Students of Latin </li>
                           
                        </ul>
                        
                        <p><strong>External Resources </strong> 
                        </p>
                        
                        <p><a href="http://www.wheelockslatin.com/">Official Wheelock's Latin Series Website </a></p>
                        
                        <p><a href="http://www.wheelockslatin.com/chapters/introduction/introduction.html">The Official Wheelock's Latin Series </a></p>
                        
                        <p><a href="http://web.uvic.ca/hrd/latin/wheelock/contents.htm">Wheelock Latin Exercises </a></p>
                        
                        <p><a href="http://web.uvic.ca/hrd/latin/wheelock/contents.htm">Wheelock-linked Latin Practice Exercises </a></p>
                        
                        <p><a href="http://www.wheelockslatin.com/wheelocksflashcards.htm">Latin Textbook Wheelock's Latin Study Guide </a></p>
                        
                        <p><a href="http://mythfolklore.net/">Laura Gibbs Websites </a></p>
                        
                        <p><a href="http://www.perseus.tufts.edu/hopper/">Perseus Digital Library </a></p>
                        
                        <p><a href="http://www.quia.com/pages/magistermccann2.html">Quia - Class Page </a></p>
                        
                        <p><a href="http://greekandlatin.osu.edu/programs/latin/wheelock/default.cfm">Department of Greek and Latin at Ohio State University </a> 
                        </p>
                        
                        <p><a href="http://www.thelatinlibrary.com/101/">The Latin Library </a></p>
                        
                        <p><a href="http://www.verbix.com/languages/latin.shtml">Verbix - Romance Language Conjugate Latin Verbs </a></p>
                        
                        <p><a href="http://www.vroma.org/">VROMA Home </a></p>
                        
                        <p><a href="http://ablemedia.com/ctcweb/showcase/barrettelatin.html">Electronic Resources for Latin</a></p>
                        
                        <p><a href="http://www.quasillum.com/study/index.php">Latin Study Groups<br>
                              <br>
                              Origin of Words</a> 
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/latin.pcf">©</a>
      </div>
   </body>
</html>