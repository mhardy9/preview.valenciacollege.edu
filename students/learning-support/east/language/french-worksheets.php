<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Language Lab at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/language/french-worksheets.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Language Lab at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/language/">Language</a></li>
               <li>Language Lab at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/language/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>French - Learning Resources</h2>
                        
                        <h2><strong>How do I check out these items?</strong></h2>
                        
                        <p>You must bring your Valencia ID to check out any lab resources. Our lab staff will
                           let you know when the items are due back.
                        </p>
                        
                        <p>Books and Textbooks </p>
                        
                        <p>In addition to a wide variety of dictionaries, we also have the following textbooks:</p>
                        
                        <ul>
                           
                           <li>Promenades</li>
                           
                           <li>Sur le vif (Intermediate French) </li>
                           
                           <li>English Grammar for Students of French</li>
                           
                           <li>501 French Verbs </li>
                           
                        </ul>
                        
                        <p>Printable Worksheets</p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span>FRE1120</span></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><span><a href="../../academic-success/language/documents/FRE1120-PastTenseHandout.doc">Past Tense</a></span></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1120WS-ConjugationofRegular-erVerbsPresTense.doc">Conjugation of Regular -er Verbs (Present)</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1120WS-PasseComposeavoir.doc">Passé Composé (avoir)</a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1120FormationofPasseCompose.doc">Formation of Passe Compose </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1120WorksheetERverbsPresentTense.doc">ER verbs in Present Tense</a> </span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="http://valenciacollege.edu/east/academicsuccess/language/WritingPrompts.docx">Writing Prompts </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span><span>FRE1121</span></span></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1121WS-ReflexiveVerbs.doc">Reflexive Verbs </a></span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><span><a href="../../academic-success/language/documents/FRE1121WorksheetThepassecomposewithavoir.doc">The passé composé with avoir</a></span></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>External Links</p>
                        
                        <p><a href="http://www.bbc.co.uk/languages/french/index.shtml?survey=no&amp;url=www.bbc.co.uk/languages/french/index.shtml&amp;site=languagesfrench&amp;js=yes">BBC - Languages - Learn French</a> 
                        </p>
                        
                        <p><a href="http://www.bbc.co.uk/french/">BBCAfrique.com Informations French News index</a> 
                        </p>
                        
                        <p><a href="http://fr.canoe.ca/">Canoë</a></p>
                        
                        <p><a href="http://conjuguemos.com/home/docs/php/list.php?type=verbs&amp;division=verbs&amp;language=french">conjuguemos</a></p>
                        
                        <p><a href="http://espaces.vhlcentral.com/">Espaces Supersite</a> 
                        </p>
                        
                        <p><a href="http://ielanguages.com/french.html">French Tutorials Index</a></p>
                        
                        <p><a href="http://www.lefigaro.fr/">Le Figaro</a></p>
                        
                        <p><a href="http://www.leparisien.fr/">Le Parisien </a></p>
                        
                        <p><a href="http://french.about.com/">Learn French - Online Lessons and Tips to Learn French</a> 
                        </p>
                        
                        <p><a href="http://promenades.vhlcentral.com/">Promenades</a></p>
                        
                        <p><a href="http://books.quia.com/books">Quia - Quia Books</a> 
                        </p>
                        
                        <p><a href="http://faculty.valenciacollege.edu/schater/">Samira Chater - Communications</a></p>
                        
                        <p> <a href="http://french.typeit.org/">Type French accents online</a> 
                        </p>
                        
                        <p><a href="http://www.wordreference.com/conj/FrVerbs.asp">WordReference.com</a><a href="http://www.verbix.com/languages/french.shtml"></a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/language/index.html">
                                          Academic Success Center - Foreign Language Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 104</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2841</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday: 9:00 am - 6:00 pm<br>Tuesday: 9:00 am - 7:00 pm<br>Wednesday: 9:00 am - 7:00 pm<br>Thursday: 9:00 am - 6:00 pm<br>Friday: 8:00 am - 12:00 pm<br>Saturday: CLOSED
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/language/french-worksheets.pcf">©</a>
      </div>
   </body>
</html>