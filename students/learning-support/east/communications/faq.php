<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/communications/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Writing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/communications/">Communications</a></li>
               <li>Writing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/writing/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions </h2>
                        
                        <h3>What do I need to bring to the Communications Center? </h3>
                        
                        <p> If you need to print a document, you will need to buy a copy/print card!</p>
                        
                        <p><strong>For a consultation</strong> <strong>you must bring</strong>:
                        </p>
                        
                        <ul>
                           
                           <li>Valencia student ID </li>
                           
                           <li>Your class information (i.e. ENC1101, Professor Randy Gordon) </li>
                           
                           <li>Professor's assignment instructions </li>
                           
                           <li>A <span>printed</span> draft of assignment (at any state of development)
                           </li>
                           
                        </ul>
                        
                        <h3>What do I need to do before I see a writing consultant?</h3>
                        
                        <ul>
                           
                           <li>Type your draft if it is more than two pages </li>
                           
                           <li>Proofread your paper to utilize the time spent with a consultant most efficently </li>
                           
                           <li>Be prepared to discuss specific writing issues in your paper </li>
                           
                        </ul>
                        
                        <h3>How do I check in when I come to the Communications Center? </h3>
                        
                        <p><span>You must always check in before utilizing any services in the Communications Center!</span> This includes using the computers or recording a speech in the Speech Lab. 
                        </p>
                        
                        <p>To check in you must know your Valencia ID number (VID). After you enter this information,
                           select the reason why you have come to the lab. Then, finish by choosing your correct
                           class information.
                        </p>
                        
                        <p>Make sure to check out before you leave! </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>May I use a computer in the Communications Center? </h3>
                        
                        <p><span>Yes</span>, computers are available; however, priority is given to students completing lab work.
                           In order to use a computer, you will need to know your Atlas username and password.
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Can  I make an appointment for a writing consultation?</h3>
                        
                        <p>At this time, we only offer appointments to  students registered with the Office for
                           Students with Disabilities. <span>Walk-ins</span> are always welcome, but please be aware that hours for writing consultations vary.
                           Please check our <a href="../../academic-success/writing/hours.html">hours</a> to see when walk-in consultations are being offered. 
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Can I walk in at closing and still get a consultation?</h3>
                        
                        <p><span>No.</span> The last consultations will be given 30 minutes before closing. Walk-ins will not
                           be accepted after the 30 minute mark. Please see our <a href="../../academic-success/writing/hours.html">hours</a> if you have questions. 
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Will a writing consultant edit my paper? </h3>
                        
                        <p><span>No</span>. Consultants will provde feedback on your writing and recommend additional resources
                           to assist you with the writing process. The "<a href="http://valenciacollege.edu/east/academicsuccess/writing/ConsultationAgreementDetailedVersion.docx">Consultation Agreement</a>" you read prior to each session will outline what you can expect from the consultation.
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>What are the qualifications of writing consultants? </h3>
                        
                        <p>All of our writing consultants have a <span>Bachelor's degree</span> or higher in an <span>English-related discipline</span> and are trained to assist students across the curriculum. Many of our consultants
                           are graduate students or recent graduates of UCF. 
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Will a writing consultant help me brainstorm? </h3>
                        
                        <p>Absolutely! Writing consultants can help you during every step of the writing process,
                           from brainstorming and prewriting to final checks. 
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Are consultants available when classes are not in session?</h3>
                        
                        <p><span>No</span>. Our writing consultants work during fall, spring, and summer semesters. They do
                           not work when classes are not in session (spring break, summer break in August, and
                           winter break).
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>How do I sign up for a workshop? </h3>
                        
                        <p>You can sign up for the Communications Center Workshops in the <span>two ways listed below </span>. Please note that workshops are not continuously offered. Be sure to check the calendar
                           for specific dates and times. 
                        </p>
                        
                        <ul>
                           
                           <li>Call 407-582-2795</li>
                           
                           <li>Send an email through this <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/workshops/breakingbad_workshop_signup_form.cfm" target="_blank">electronic form</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>Do I need to bring my PERT workbook for the review? </h3>
                        
                        <p><span>No.</span> The PERT review is designed to tell you what you need to study in the workbook. Although
                           you are more than welcome to go through the workbook beforehand, it isn't necessary.
                           
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        <h3>How do I reserve time in the Speech Lab?</h3>
                        
                        <p>Reservations for the Speech Lab can be made over the phone or in person. </p>
                        
                        <p><span>If you are recording a speech, be prepared to record immediately! </span>The Speech Lab is a 'record only' space. If you need aditional time preparing your
                           speech, do not make an appointment until you are ready to record. 
                        </p>
                        
                        <p>Appointments for speech recordings are <span>30 minutes only</span>. If you have a group speech, we allow for no more than one hour. 
                        </p>
                        
                        <p><a href="../../academic-success/writing/FrequentlyAskedQuestions.html">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/writing/index.html">
                                          Academic Success Center - Communications Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 120</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2795</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 2:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/communications/faq.pcf">©</a>
      </div>
   </body>
</html>