<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/communications/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Writing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/communications/">Communications</a></li>
               <li>Writing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/writing/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Meet Our Staff </h2>
                        
                        <h3>Communications Center Coordinator</h3>
                        
                        <h3><img alt="Jodi" height="213" src="../../academic-success/writing/Jodiwebsite.jpg" width="197"></h3>
                        
                        <h3><u><strong>Jodi Brock </strong></u></h3>
                        
                        <p><a href="../../../index.html">Jodi Brock</a> is a Senior Instructional Assistant at the Communications Center. She received a
                           Bachelor’s degree in Elementary Education and a Master’s degree in TESOL (Teaching
                           English to Speakers of Other Languages) from the University of Central Florida.&nbsp; When
                           not working at Valencia, she enjoys working part-time buying and selling antiques,
                           traveling, studying linguistics and philosophy, and spending time with friends and
                           family.&nbsp; 
                        </p>
                        
                        <p>Assistant Coordinators</p>
                        
                        <p><img alt="Rachel" height="200" hspace="0" src="../../academic-success/writing/Rachel.jpg" vspace="0" width="151"></p>
                        
                        <h3><u>Rachel Kolman</u></h3>
                        
                        <p> Rachel is a writing consultant and English professor at Valencia. She received her
                           BA in English from Florida State University and her MFA in Creative Writing from University
                           of Central Florida. When not working with students, Rachel writes&nbsp;online for various
                           websites and works to get her own fiction published. She also enjoys arcade games,
                           podcasts, and pouring latte art. She is always looking for a good place to get lunch.&nbsp;
                           
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Chris V Kaay" height="171" hspace="0" src="../../academic-success/writing/chrisvanderkaay.jpg" vspace="0" width="175"></p>
                        
                        <h3><u>Chris Vander Kaay</u></h3>
                        
                        <p>Chris is a graduate of UCF with a Bachelor's in English. </p>
                        
                        <p>He and his wife are screenwriters and authors, with two published books and a film
                           that premiered on the Hallmark Movie Channel.
                        </p>
                        
                        <p>When he is not working, he watches every television series that isn't about a doctor,
                           lawyer, or police officer. Everything else about him, he wishes to keep secret.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Michael Dwyer" height="185" hspace="0" src="../../academic-success/writing/screenshot2016-07-25at4.17.49pm.png" vspace="0" width="175"> 
                        </p>
                        
                        <h3><u>Michael Dwyer </u></h3>
                        
                        <p>Michael has a B.A. Degree in English Language Arts Education from the University of
                           Central Florida and holds a current Florida teaching certificate. 
                        </p>
                        
                        <p>Before coming to Valencia, Michael taught students with learning disabilities for
                           four years. 
                        </p>
                        
                        <p>When not working, Michael enjoys participating in trivia competitions at local establishments,
                           traveling, and playing board and video games.
                        </p>
                        
                        
                        <h3>Writing Consultants </h3>
                        
                        <p><img alt="Yogini" height="207" src="../../academic-success/writing/32890022.jpg" width="215"></p>
                        
                        <h3><u>Yogini Dixit-Joshi<br>
                              </u></h3>
                        
                        <p> Yogini is originally from India. She completed her masters in English Literature
                           in India and TESOL at the University of Central Florida. She also has a diploma in
                           the Japanese Language. She loves music and spending time with friends. Teaching has
                           always been Yogini's passion. She likes to help people and to interact with them.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="silva" height="283" src="../../academic-success/writing/Silvawebsite.jpg" width="205"></p>
                        
                        <h3><u>Silva Kandiah</u></h3>
                        
                        <p>Silva Kandiah has a Bachelor of Arts (a major in English and minors in history and
                           geography) and a Master of Social Science (geography), both from the National University
                           of Singapore. He retired after 27 years in Singapore for Singapore Airlines in Corporate
                           Communications, Human Resources, Aviation Security, and a range of other functions.
                           He continues to write for various media and is working on some books. He is also a
                           volunteer with SCORE- counselors to small businesses.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Al Marquez" height="245" src="../../academic-success/writing/Alwebsite.jpg" width="219"></p>
                        
                        <h3><u>Al Marquez </u></h3>
                        
                        <p>Al Marquez is a Senior Curriculum Assistant in the Communications Center. He received
                           a bachelor’s degree in English and international affairs from Florida State University.
                           When he’s not fishing, studying Aikido, or seriously causing harm to others with his
                           cooking attempts, he can be found chasing the latest whimsical nonsense that has popped
                           into his head.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Glenda" height="237" hspace="0" src="../../academic-success/writing/glenda.jpg" vspace="0" width="203"></p>
                        
                        <h3><u>Glenda Ramsey</u></h3>
                        
                        <p>Glenda was born in Kansas City, MO, and then moved to Texas. There she graduated from
                           high school, married, then spent several years moving around and bringing up five
                           children. She lived in Europe for two years as well. Glenda has an A.A. from Brevard
                           Community College and a B.A. from the University of Texas in El Paso. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        <p><img alt="Gary Listort" height="191" hspace="0" src="../../academic-success/writing/screenshot2016-07-25at4.17.19pm.png" vspace="0" width="175"></p>
                        
                        <h3><u>Gary Listort</u></h3>
                        
                        <p>Music is the second greatest gift we have: second only to the ability to love each
                           other. 
                        </p>
                        
                        <p>As well as tutoring in the Communications Center, Gary also tutors students in Music
                           Theory.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        <p><img alt="Gary Listort" height="230" hspace="0" src="../../academic-success/writing/Kathleen-Fernandez.jpg" vspace="0" width="175"></p>
                        
                        <h3>Kathleen Fernandez</h3>
                        
                        <p>              Kathleen Fernandez has a B.A. in Creative Writing from the University
                           of Central Florida and an MFA from Lindenwood University. Kathleen, along with her
                           husband and co-writer, has three published books and several produced screenplays.
                           Kathleen and her husband met years ago when they were both students at Valencia, and
                           she has been committed to her husband and Valencia ever since. When she isn't helping
                           students, she is writing, mostly about Las Vegas and/or alien abductions.
                        </p>
                        
                        <p><br>
                           
                        </p>
                        
                        <p>Office Aides</p>
                        
                        
                        <p><img alt="Josh Gwynn" height="163" hspace="0" src="../../academic-success/writing/img_6360.jpg" vspace="0" width="175"></p>
                        
                        <h3><u>Joshua Gwynn</u></h3>
                        
                        <p>Josh graduated Valencia College in the Spring of 2016 with two A.S. Degrees in Computer
                           Programming and Computer Information Technology. He is starting UCF in the fall of
                           2017 for a B.A.S. in Software Development. Joshua is active with several organizations
                           here on&nbsp; East campus. He likes to spend his free time playing card games or video
                           games.
                        </p>
                        
                        
                        <hr>
                        
                        
                        <p><img alt="Stephen Espinoza" height="240" hspace="0" src="../../academic-success/writing/screenshot2016-07-25at4.18.06pm.png" vspace="0" width="200"> 
                        </p>
                        
                        <h3><u>Steven Espinoza </u></h3>
                        
                        <p>Packers fanatic. </p>
                        
                        <p>Film lover. </p>
                        
                        <p>First Team All-Pro in the game of life.</p>            
                        
                        
                        
                        
                        
                        
                        <p><a href="../../academic-success/writing/meetourstaff.html">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/writing/index.html">
                                          Academic Success Center - Communications Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 120</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2795</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 2:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/communications/staff.pcf">©</a>
      </div>
   </body>
</html>