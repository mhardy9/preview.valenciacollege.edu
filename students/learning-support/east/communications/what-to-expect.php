<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/communications/what-to-expect.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Writing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/communications/">Communications</a></li>
               <li>Writing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/writing/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>What to Expect during a Consultation Session </h2>
                        
                        <p>What to Bring </p>
                        
                        <p>In order to be fully prepared for a writing consultation, students should bring the
                           following:
                        </p>
                        
                        <ul>
                           
                           <li>A printout of the assignment</li>
                           
                           <li>The instructions to the assignment</li>
                           
                           <li>Valencia Student ID</li>
                           
                           <li>A pen or pencil to write with</li>
                           
                           <li>An open and active mind for communicating with my tutor!</li>
                           
                        </ul>
                        
                        <p>Consultation Guidelines and Expectations </p>
                        
                        <p>The goals of the Communications Center are to help you become a <strong>better writer</strong> and an <strong>independent learner</strong>. Listed below are guidelines that will enhance your learning experience while you
                           are studying in the center. By applying these guidelines, you will accomplish academic
                           and personal skills that will serve you for a lifetime! Please take a moment to review
                           the consultation guidelines.
                        </p>
                        
                        <p>I understand that…<a name="Consultationguidelines" id="Consultationguidelines"></a></p>
                        
                        <ul>
                           
                           <li>I will have up to <span><strong>thirty (30) minutes</strong></span> with my tutor. My tutor and I will discuss the strengths and weaknesses of my writing.
                              However, we <u>may not be able to address all areas of concern</u> during the consultation. I recognize that developing my writing will happen over
                              time, not in a single 30 minute tutoring session.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <span><strong>I will need to look over my paper and revise it to the best of my ability before the
                                    writing consultation session .</strong></span> Some ways to ensure that it’s my best work are to: read aloud, check content and
                              organization, and look for grammatical errors that I commonly make. I will make a
                              concerted effort to apply the writing rules that I know. If it’s obvious that I haven’t
                              proofread my paper, I may be asked to do so before the consultation begins.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>I am coming to the Communications Center for assistance with my writing; therefore,
                              I will provide an <span><strong>assignment sheet or be prepared to discuss the assignment</strong></span> requirements. Additionally, I will be able to<u> identify my class number and professor’s full name</u>.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>With the exception of the brainstorming stage and/or drafts less than two pages, <span><strong>all papers must be typed.</strong></span>
                              
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>During the tutoring session, I need to be <span><strong>engaged</strong></span>. I can be engaged by taking notes, making modifications to my paper, highlighting
                              sections of my paper, and asking questions during the consultation.<span><strong> I am responsible for directing the discussion</strong></span>. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>The consultant may write on my paper, but ultimately<u> I am responsible for any changes</u> to it, either during the consultation or afterwards. The tutor is not responsible
                              for editing or revising the paper for me. Therefore,<span><strong> the grade I earn on a paper is my responsibility</strong></span>. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>The consultant may ask me to complete some supplemental exercises either during or
                              after the consultation to strengthen my writing skills.
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <span><strong>I must follow the Student Code of Conduct.</strong></span> I, therefore, not act disrespectfully to my tutor or other students by refusing to
                              leave a tutoring session, using a cell phone, speaking loudly, or acting inappropriately
                              in the center. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <span><strong>I need to give credit to all of my sources</strong></span>. I understand that plagiarism – the use of someone else’s words or ideas without
                              credit – is a violation of the Student Code of Conduct (see policy 6Hx28:10-16) and
                              that I may not present plagiarized work to my tutor. I also understand that <u>my tutor cannot accept responsibility for identifying plagiarism</u> in my paper.
                           </li>
                           
                        </ul>
                        
                        <p><strong><a href="../../academic-success/writing/whattoexpect.html">TOP</a></strong></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/writing/index.html">
                                          Academic Success Center - Communications Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 120</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2795</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 2:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/communications/what-to-expect.pcf">©</a>
      </div>
   </body>
</html>