<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/communications/hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Writing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/communications/">Communications</a></li>
               <li>Writing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div><img alt="Comm" height="130" src="../../academic-success/writing/commcenter.JPG" width="602"></div>            
                        
                        <p>
                           <a name="content" id="content"></a>
                           <a href="../../academic-success/writing/index.html"></a></p>
                        
                        
                        <p>Fall &amp; Spring  Hours </p>
                        
                        <p>Monday - Thursday: 9:00am - 7:00pm*<br>
                           Friday: 9:00am - 3:00pm*<br>
                           Saturday: 9:00am - 2:00pm*<br>
                           Sunday: Closed
                        </p>
                        
                        <p>*last consultation given 30 minutes before closing </p>
                        
                        <p><span>Notice: Consultants are not available the first three weeks of the semester. The Communications
                              Center is open but only for lab work and orientations. </span></p>
                        
                        <p><span>Notice: Consultants are not available the week(s) between semesters </span></p>
                        
                        <h3>Summer Hours (May 10 - July 30) </h3>
                        
                        <p>Monday - Thursday: 9:00am - 7:00pm*<br>
                           Friday: 9:00am - 12:00pm*<br>
                           Saturday: 9:00am - 2:00pm*<br>
                           Sunday: Closed
                        </p>
                        
                        <p>*last consultation given 30 minutes before closing </p>
                        
                        <h3>Summer Hours (July 31 - Aug 24) </h3>
                        
                        <p>Monday - Friday: 8:00am - 5:00pm <br>
                           Saturday: Closed <br>
                           Sunday: Closed
                        </p>
                        
                        <p><span>Notice: Consultants are not available the weeks between summer semester &amp; fall semester
                              </span></p>
                        
                        <p><a href="../../academic-success/writing/hours.html">TOP</a></p>
                        
                        <h3>PERT Workshop Hours</h3>
                        
                        <p>English and Reading PERT Workshops are available for qualified students. </p>
                        
                        <p>Monday - Thursday: 9:00am - 2:00pm*<br>
                           Friday - Sunday: Closed 
                        </p>
                        
                        <p>*During the month of August, PERT hours are 8:00am - 12:00pm </p>
                        
                        <p><a href="../../academic-success/writing/hours.html">TOP</a></p>
                        
                        <h3>Location</h3>
                        The Communications Center is located in the <strong>Academic Success Center</strong>, which is on the first floor of<strong> Building 4 (library) in room</strong> <strong> 120.</strong>
                        
                        <ul>
                           
                           <li>View a <a href="../location.html">map and directions</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p><a href="../../academic-success/writing/hours.html">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/communications/hours.pcf">©</a>
      </div>
   </body>
</html>