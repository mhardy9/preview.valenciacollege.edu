<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/communications/lab-software.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Writing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/communications/">Communications</a></li>
               <li>Writing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/writing/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Lab Software </h2>
                        
                        <p>The Communications Center has a variety of software available for practice and study.</p>
                        
                        <h3>Vocabulary Programs </h3>
                        
                        <h3>
                           <img alt="Vocab Placement" height="88" src="../../academic-success/writing/0-944210-4rrrreeee0-6.JPG" width="70"> <em>Vocabulary Basics </em>
                           
                        </h3>
                        
                        <h3>
                           <img alt="Groundwork for vocab" height="89" src="../../academic-success/writing/reduced.JPG" width="71"> <em>Groundwork for a Better Vocabulary </em>
                           
                        </h3>
                        
                        <h3>
                           <img alt="Building Vocab Skills" height="88" src="../../academic-success/writing/0-944210-12-0re_000.JPG" width="70"> <em>Building Vocabulary Skills </em>
                           
                        </h3>
                        
                        
                        <h3>
                           <img alt="Improving Vocabulary Skills" height="88" src="../../academic-success/writing/0-944210-13-9re_000.JPG" width="70"> <em>Improving Vocabulary Skills </em>
                           
                        </h3>
                        
                        
                        <h3>
                           <img alt="Advancing Vocabulary Skills" height="87" src="../../academic-success/writing/0-944210-14-7re_000.JPG" width="70"> <em>Advancing Vocabulary Skills </em>
                           
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a></p>
                        
                        
                        <h3>Reading Programs </h3>
                        
                        <h3>
                           <img alt="Groundwork" height="88" src="../../academic-success/writing/GP4phonicsCoverred.JPG" width="70"> <em>Groundwork for College Reading </em>
                           
                        </h3>
                        
                        <h3>
                           <img alt="Building Reading Skills" height="81" src="../../academic-success/writing/7ebac060ad_Lre_000.JPG" width="65"> <em>Ten Steps to Building College Reading Skills </em>
                           
                        </h3>
                        
                        
                        <h3>
                           <img alt="Improving" height="88" src="../../academic-success/writing/9781591940043.jpg" width="65"> <em>Ten Steps to Improving College Reading Skills </em> 
                        </h3>
                        
                        
                        <h3>
                           <img alt="Ten Steps to Advancing" height="81" src="../../academic-success/writing/30349d60110_Lre_000.JPG" width="65"> <em>Ten Steps to Advancing College Reading Skills </em>
                           
                        </h3>
                        
                        
                        <h3>
                           <img alt="Speed Reader" height="55" src="../../academic-success/writing/ultimate-sre.GIF" width="70"> <em>Ultimate Speed Reader </em> 
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a></p>
                        
                        
                        <h3>Grammar Programs </h3>
                        
                        <h3>
                           <img alt="Grammar Sense 2" height="88" src="../../academic-success/writing/202417049re.JPG" width="69"> <em>Grammar Sense 2 </em>
                           
                        </h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="GS3" height="87" src="../../academic-success/writing/51G_re_001.JPG" width="69"> <em>Grammar Sense 3 </em>
                           
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="focus 1" height="87" src="../../academic-success/writing/focusongram.jpg" width="64"> <em>Focus on Grammar 1 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="focus 2" height="87" src="../../academic-success/writing/focus2.jpg" width="66"> <em>Focus on Grammar 2 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="focus 3" height="87" src="../../academic-success/writing/grammar-module-textbook.jpg" width="66"> <em>Focus on Grammar 3 </em> 
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a></p>
                        
                        
                        <h3>Listening Programs</h3>
                        
                        <h3>
                           <img alt="Contemporary Topics 1" height="69" src="../../academic-success/writing/51TLv29wZre.JPG" width="70"> <em>Contemporary Topics 1 </em>
                           
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Contemporary 2" height="90" src="../../academic-success/writing/517NCFK3EQL_reduced.JPG" width="70"> <em>Contemporary Topics 2 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Contemporary Topics 3" height="92" src="../../academic-success/writing/51M37X500_AA240_re.JPG" width="70"> <em>Contemporary Topics 3 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Lect 1" height="88" src="../../academic-success/writing/lect1.jpg" width="70"><em>Lecture Ready 1 </em>
                           
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Lect 2" height="91" src="../../academic-success/writing/isbn.jpg" width="70"> <em>Lecture Ready 2 </em> 
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a></p>
                        
                        
                        <h3>Pronunciation Programs </h3>
                        
                        <h3>
                           <img alt="Well Said" height="88" src="../../academic-success/writing/9780838412374er_000.JPG" width="69"> <em>Well Said - Second Edition </em>
                           
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Well Said Intro" height="89" src="../../academic-success/writing/isbn2.jpg" width="69"> <em>Well Said Intro </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Basics" height="89" src="../../academic-success/writing/basics.jpg" width="68"> <em>Basics in Pronunciation</em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="focus on pron 1" height="89" src="../../academic-success/writing/focus1.jpg" width="71"> <em>Focus on Pronunciation 1 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3><em><img alt="focus on pron 2" height="89" src="../../academic-success/writing/md0130978779.jpg" width="70">Focus on Pronunciation 2 </em></h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="focus on pron 3" height="89" src="../../academic-success/writing/focus3.jpg" width="71"> <em>Focus on Pronunciation 3 </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Eng pron" height="89" src="../../academic-success/writing/engpron.jpg" width="70"> <em>English Pronunciation Made Simple </em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Talk it Up" height="79" src="../../academic-success/writing/talk.jpg" width="63"> <em>Talk it Up</em> 
                        </h3>
                        
                        <h3></h3>
                        
                        <h3></h3>
                        
                        <h3>
                           <img alt="Talk it Through" height="79" src="../../academic-success/writing/talkII.jpg" width="64"> <em>Talk it Through </em> 
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a></p>
                        
                        
                        <h3>Language Learning Software </h3>
                        
                        <h3>
                           <img alt="learn to speak english" height="89" src="../../academic-success/writing/learn.jpg" width="90"> <em>Learn to Speak English Essentials </em>
                           
                        </h3>
                        
                        <p><a href="../../academic-success/writing/labsoftware.html">TOP</a> 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/writing/index.html">
                                          Academic Success Center - Communications Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 120</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2795</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 2:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/communications/lab-software.pcf">©</a>
      </div>
   </body>
</html>