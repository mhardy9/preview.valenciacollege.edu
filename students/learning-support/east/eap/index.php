<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Eap</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="../../academic-success/eap/arrow.gif" width="8">  <img alt="" height="8" src="../../academic-success/eap/arrow.gif" width="8">  <img alt="" height="8" src="../../academic-success/eap/arrow.gif" width="8"> 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="../index.html">Academic Success Home</a></li>
                                                   
                                                   <li><a href="../../academic-success/math/index.html">Math Center</a></li>
                                                   
                                                   <li><a href="../../academic-success/writing/index.html">Communications Center</a></li>
                                                   
                                                   <li><a href="../../academic-success/tutoring/index.html">General Tutoring</a></li>
                                                   
                                                   <li><a href="index.html">EAP Lab</a></li>
                                                   
                                                   <li><a href="../../academic-success/language/index.html">Foreign Language Lab</a></li>
                                                   
                                                   <li><a href="../../academic-success/testing/index.html">Testing Center</a></li>
                                                   
                                                   <li><a href="../../../honors/index.html">Honors Resources</a></li>
                                                   
                                                   
                                                   <li><a href="../../academic-success/welcomedesk/index.html">Welcome Desk</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          <a name="content" id="content"></a>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <link href="../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured Image 1" border="0" height="260" src="../../academic-success/eap/e1.png" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured Image 2" border="0" height="260" src="../../academic-success/eap/e2.png" width="770">
                                                               
                                                            </div> 
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured Image 3" border="0" height="260" src="../../academic-success/eap/e3.png" width="770">
                                                               
                                                            </div> 
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>            
                                                      
                                                      <h2>Welcome to the EAP Lab!</h2>
                                                      
                                                      <p>The EAP Lab supports EAP students and faculty. The EAP Lab is a multi-purpose space
                                                         and provides the following: 
                                                      </p>
                                                      
                                                      <ol>
                                                         
                                                         <li> Bookable classroom space for EAP faculty who wish to incorporate technology in their
                                                            lessons.
                                                         </li>
                                                         
                                                         <li>Learning space for students attending workshops and/or completing lab work.</li>
                                                         
                                                      </ol>
                                                      
                                                      <p>EAP Lab Hours </p>
                                                      
                                                      <p><a href="hours.html">Click here for current EAP Lab hours</a></p>
                                                      
                                                      <div>
                                                         
                                                         
                                                         <p>For assistance with EAP coursework outside of our open hours, please visit the Communications
                                                            Center in Building 4, Room 120.
                                                         </p>
                                                         
                                                         <p><a href="https://www.youtube.com/watch?v=b6QIgyewY0g">Click here to learn more about EAP students </a></p>
                                                         
                                                      </div>            
                                                      
                                                      <div>
                                                         
                                                         <p> EAP Lab Services and Resources:</p>
                                                         
                                                         <div>
                                                            
                                                            <ul>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Desk staff during open hours </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>EAP focused tutoring facilitated by instructional assistants and tutors</div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Co-curricular workshops led by faculty and instructional assistants </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Conversation practice </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>24 student computers plus one teacher work station </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>EAP focused software (reading, writing, grammar, pronunciation, listening) </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Worksheets </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Practice quizzes </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Educational DVDs </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Student and faculty resources for checkout </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Language learning games </div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Adjunct training and resources</div>
                                                                  
                                                               </li>
                                                               
                                                               <li>
                                                                  
                                                                  <div>Class orientations </div>
                                                                  
                                                               </li>
                                                               
                                                            </ul>
                                                            
                                                            
                                                         </div>
                                                         
                                                         <p><a href="index.html">TOP</a></p>
                                                         
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="490"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="265"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../../academic-success/eap/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center - EAP 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 105</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2098</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 3:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/index.pcf">©</a>
      </div>
   </body>
</html>