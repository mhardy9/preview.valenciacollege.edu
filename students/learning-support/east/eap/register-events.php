<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/register-events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Registering for EAP Workshops &amp; Events </h2>
                        
                        <p>Attend a <span>FREE EAP Workshop</span> to boost your academic performance! Workshops cover a variety of communication, pronunciation,
                           grammar, writing, and technology skills. 
                        </p>
                        
                        <p>Check out our <a href="Final_Workshops.swf">tutorial video</a> and <a href="http://www.youtube.com/watch?v=EECP9pa9BqM&amp;feature=related">welcome video</a>. 
                        </p>
                        
                        <p>How to Sign up for a Workshop </p>
                        
                        <p>You can sign up for a workshop in one of three ways:</p>
                        
                        <ul>
                           
                           <li>Visit the <a href="index.html">The EAP Lab (4-105)</a>
                              
                           </li>
                           
                           <li>Call 407-582-2098</li>
                           
                           <li>Sign up <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">electronically</a>! 
                           </li>
                           
                        </ul>
                        
                        <p>Seats are limited, so sign-up today! </p>
                        
                        <p>Workshop Calendars &amp; Information </p>
                        
                        <p>Would you like to know more about the workshop you are attending? Check out the <a href="../../academic-success/eap/workshopdescriptions.html">workshop description list</a>. 
                        </p>
                        
                        <p>Do you have questions about the workshop series? Check out the <a href="frequentlyaskedquestions.html"> Frequently Asked Questions page</a>. 
                        </p>
                        
                        <p>If you have any additional questions, please contact the EAP Lab coordinator, <a href="mailto:aaslam2@valenciacollege.edu">Aysha Aslam</a>.<span><br>
                              </span></p>
                        
                        
                        <p>May 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>Monday</div>
                                    
                                    <div>Tuesday</div>
                                    
                                    <div>Wednesday</div>
                                    
                                    <div>Thursday</div>
                                    
                                    <div>Friday</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>1</div>
                                    
                                    <div>2</div>
                                    
                                    <div>3</div>
                                    
                                    <div>4</div>
                                    
                                    <div>5</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>8</div>
                                    
                                    <div>9</div>
                                    
                                    <div>11</div>
                                    
                                    <div>12</div>
                                    
                                    <div>13</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>15</div>
                                    
                                    <div>16</div>
                                    
                                    <div>17</div>
                                    
                                    <div>18</div>
                                    
                                    <div>19</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>22</div>
                                    
                                    <div>23<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>24<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>25<br>
                                       Pronunciation:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>26</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>29</div>
                                    
                                    <div>30<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>31<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm <br>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>June 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>Monday</div>
                                    
                                    <div>Tuesday</div>
                                    
                                    <div>Wednesday</div>
                                    
                                    <div>Thursday</div>
                                    
                                    <div>Friday</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                    <div>1<br>
                                       Ed Endings:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>2</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>5</div>
                                    
                                    <div>6<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>7<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>8<br>
                                       Stress and Linking:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>9</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>12</div>
                                    
                                    <div>13<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>14<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>15<br>
                                       PowerPoint Tutorial:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>16</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>19</div>
                                    
                                    <div>20<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>21<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>22<br>
                                       Nonverbal Communication:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>23</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>26</div>
                                    
                                    <div>27<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>28<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>29<br>
                                       Idioms:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>30</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>July 2017 </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>Monday</div>
                                    
                                    <div>Tuesday</div>
                                    
                                    <div>Wednesday</div>
                                    
                                    <div>Thursday</div>
                                    
                                    <div>Friday</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>3</div>
                                    
                                    <div>4<br>
                                       <strong><span>Holiday - No School</span></strong>
                                       
                                    </div>
                                    
                                    <div>5<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>6<br>
                                       Homophones:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>7</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>10</div>
                                    
                                    <div>11<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>12<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>13<br>
                                       Phrasal Verbs:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>14</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>17</div>
                                    
                                    <div>18<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>19<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>20<br>
                                       Composing an Essay:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>21</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>24</div>
                                    
                                    <div>25<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>26<br>
                                       <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">Conversation</a>:<br>
                                       1:00pm-2:00pm
                                    </div>
                                    
                                    <div>27<br>
                                       Minimal Pairs:<br>
                                       12:00pm-1:00pm
                                    </div>
                                    
                                    <div>28</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../academic-success/eap/registerforworkshopsandevents.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/register-events.pcf">©</a>
      </div>
   </body>
</html>