<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/useful-links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Useful Links</h2>
                        
                        <p>Grammar </p>
                        
                        <ul>
                           
                           <li><a href="http://www.dailygrammar.com/">Daily Grammar</a></li>
                           
                           <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm">English Language Center </a></li>
                           
                           <li><a href="http://www.englishgrammarsecrets.com/">English Grammar Lessons</a></li>
                           
                           <li><a href="http://chompchomp.com/">Grammar Bytes </a></li>
                           
                           <li><a href="http://englishplus.com/grammar/">Grammar Slammer </a></li>
                           
                           <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm">Guide to Grammar and Writing</a></li>
                           
                        </ul>            
                        <p>Listening &amp; Speaking </p>
                        
                        <ul>
                           
                           <li><a href="http://www.elllo.org/">Elllo - English Listening </a></li>
                           
                           <li><a href="http://www.englishlistening.com/"><u>English Listening.Com&nbsp;</u></a></li>
                           
                           <li>
                              <a href="http://www.englishmojo.com/">English Mojo</a> 
                           </li>
                           
                           <li><a href="http://esl-lab.com/">ESL Lab </a></li>
                           
                           <li><a href="http://www.eslpod.com/website/index_new.html">ESL Pod </a></li>
                           
                           <li><a href="http://learningenglish.voanews.com/">Learning English </a></li>
                           
                           <li>
                              <a href="http://www.esl-lab.com/">Randall's ESL Cyber Listening Lab</a> 
                           </li>
                           
                           <li><a href="http://www.ted.com/"><u>TED.com</u></a></li>
                           
                           <li>
                              <a href="http://www.voanews.com/">Voice of America</a> 
                           </li>
                           
                        </ul>            
                        
                        <p>Pronunciation</p>
                        
                        <ul>
                           
                           <li><a href="http://www.bbc.co.uk/worldservice/learningenglish/grammar/pron/">BBC Pronunciation Tips </a></li>
                           
                           <li><a href="http://www.forvo.com/languages/en/">Forvo</a></li>
                           
                           <li><a href="http://freerice.com/">Free Rice Vocab </a></li>
                           
                           <li>
                              <a href="http://www.rachelsenglish.com/">Rachel's English</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.soundsofenglish.org/">Sounds of English</a><a href="../../academic-success/eap/usefullinks.html">
                                 
                                 </a> 
                           </li>
                           
                        </ul>
                        
                        <p>Reading</p>
                        
                        <ul>
                           
                           <li><a href="http://www.eslfast.com/">ESL Fast </a></li>
                           
                           <li><a href="http://www.MyReadingLab.com">My Reading Lab</a></li>
                           
                           <li>
                              <a href="http://www.townsendpress.net/class/exercises"><u>Townsend Press Online Exercises</u></a><a href="../../academic-success/eap/usefullinks.html">
                                 
                                 </a>              
                           </li>
                           
                        </ul>
                        
                        <p>Vocabulary</p>
                        
                        <ul>
                           
                           <li><a href="http://dictionary.reference.com/">Dictionary.com</a></li>
                           
                           <li><a href="http://www.manythings.org/">ManyThings.org</a></li>
                           
                           <li><a href="http://wps.ablongman.com/long_licklider_vocabulary_1">Longman Vocabulary Website</a></li>
                           
                           <li>
                              <a href="http://www.merriam-webster.com">Merriam-Webster OnLine</a><a href="../../academic-success/eap/usefullinks.html">
                                 
                                 </a>              
                           </li>
                           
                        </ul>
                        
                        <p>Writing</p>
                        
                        <ul>
                           
                           <li><a href="http://citationmachine.net/">Citation Machine </a></li>
                           
                           <li><a href="http://www.MyWritingLab.com">My Writing&nbsp; Lab </a></li>
                           
                           <li><a href="http://owl.english.purdue.edu/owl/">Purdue OWL</a></li>
                           
                           <li>
                              <a href="http://www.sharedvisions.com/svuhome.cfm?sid=1">Shared Visions</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.turnitin.com/en_us/home">Turnitin.com</a><a href="../../academic-success/eap/usefullinks.html">
                                 
                                 </a>              
                           </li>
                           
                        </ul>
                        
                        <p>Combined Skills </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://a4esl.org/"><u>a4esl.org&nbsp;</u></a> 
                           </li>
                           
                           <li><a href="http://cdlponline.org/">Adult Learning Activities </a></li>
                           
                           <li><a href="http://www.bbc.co.uk/worldservice/learningenglish/">BBC Learning English</a></li>
                           
                           <li><a href="http://eslcafe.com/">Dave's ESL Cafe</a></li>
                           
                           <li><a href="http://www.youtube.com/watch?v=o32kfMYQ8K0">Dr. May's <em>Reach Out and Teach Someone </em></a></li>
                           
                           <li><a href="http://eflnet.com/">EFL Net </a></li>
                           
                           <li><a href="http://eleaston.com/english.html">E.L. Easton - English Online </a></li>
                           
                           <li><a href="http://www.englishclub.com/learn-english.htm">English Club</a></li>
                           
                           <li><a href="http://www.english-online.org.uk/course.htm">English Online </a></li>
                           
                           <li><a href="http://www.englishpage.com/">English Page</a></li>
                           
                           <li><a href="http://www.eslgold.com/">ESL Gold </a></li>
                           
                           <li> <a href="http://eslpartyland.com/">ESL PartyLand</a>
                              
                           </li>
                           
                           <li><a href="http://eslus.com/eslcenter.htm">ESL Resource Center</a></li>
                           
                           <li><a href="http://www.grammarbank.com/">Grammar Bank </a></li>
                           
                           <li><a href="http://www.infosquares.com/">Info Squares </a></li>
                           
                           <li><a href="http://www.learnenglishfeelgood.com/">Learning English Feel Good </a></li>
                           
                           <li><a href="http://www.englisch-hilfen.de/en">Learning English Online (englisch-hilfen.de) </a></li>
                           
                           <li><a href="https://owl.english.purdue.edu/owl/section/5/24/">OWL Online - ESL Resources </a></li>
                           
                        </ul>            
                        
                        
                        <p><a href="../../academic-success/eap/usefullinks.html#top">TOP</a></p>
                        
                        
                        <a href="../../academic-success/eap/usefullinks.html">
                           
                           </a>          
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center - EAP 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 105</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2098</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 3:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/useful-links.pcf">©</a>
      </div>
   </body>
</html>