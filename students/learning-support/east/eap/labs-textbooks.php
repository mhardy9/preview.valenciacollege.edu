<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/labs-textbooks.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2><strong>List of Labs &amp; Textbooks for EAP Courses</strong></h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><strong>Course</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>Textbooks</strong></div>
                                    </div>
                                    
                                    <div>
                                       <div><strong>Labs</strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0281</strong><br>
                                          Combined Skills 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><em>Pathways Foundations: Reading, Writing, and Critical Thinking <br>
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
                                             Grammar in Context 2 - 5th ed. </em></p>
                                    </div>
                                    
                                    <div>
                                       <div>Townsend Press: Groundwork for College Reading Skills with Phonics (lab only) <br>
                                          <br>
                                          MyELT Grammar in Context (lab only)
                                          <br>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0300<br>
                                             </strong> Speech 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><em>Talk it up! 1 w. CD - 2nd ed. with CD &nbsp; </em></p>
                                    </div>
                                    
                                    <div>
                                       <div>Contemporary Topics 1 (lab only)<br>
                                          <br>
                                          Lecture Ready 1 (lab only) 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0320<br>
                                             </strong> Reading 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <div><em>Ten Steps to Building College Reading Skills- 6th ed.&nbsp;<br>
                                             <br>
                                             Vocabulary Plus, Townsend Press </em></div>
                                    </div>
                                    
                                    <div>
                                       <div>Townsend Press: Building College Reading Skills (lab only) </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0340</strong><br>
                                          Writing 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><em>Longman Academic Writing 2 - 3rd ed.<br>
                                                </em></p>
                                          ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>TBA</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0360</strong>&nbsp; <br>
                                          Grammar 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><em>Focus on Grammar 3 with online access - 4th ed.<br>
                                             </em></p>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>Focus on Grammar 3 </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0400</strong>&nbsp; <br>
                                          Speech 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><em>Focus on Pronunciation 2 - 3rd ed. <br>
                                             <br>
                                             Contemporary Topics 2 w. CD- 3rd ed. </em></p>
                                       ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                       
                                    </div>
                                    
                                    <div>
                                       <div>Lecture Ready 2 (lab only) <br>
                                          <br>
                                          <a href="../../academic-success/eap/registerforworkshopsandevents.html">Workshops</a> (lab only) 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0420</strong> Reading 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <div><em>Ten Steps to Improving College Reading Skills- 6th ed.&nbsp;<br>
                                             <br>
                                             Vocabulary Plus, Townsend Press </em></div>
                                    </div>
                                    
                                    <div>
                                       <div>Townsend Press: Improving College Reading Skills (lab only) </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          <strong>EAP 0440</strong>&nbsp; <br>
                                          Writing 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><em> Longman Academic Writing 3- 4th ed. <br>
                                             </em></div>
                                    </div>
                                    
                                    <div>
                                       <div>Make-a-Paragraph Kit (lab only) </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>EAP 0460</strong> Grammar 
                                       </p>
                                    </div>
                                    
                                    <div>
                                       <p><em>Fundamentals of English Grammar Interactive - 4th ed.<br>
                                             </em></p>
                                    </div>
                                    
                                    <div>
                                       <div>Fundamentals of Grammar (access code) <br>
                                          <br>
                                          Grammar Sense 2 (lab only) 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div> <strong>EAP 1500</strong> Speech 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><em>Speech Communication Made Simple 2- 4th ed.<br>
                                                <br>
                                                Lecture Ready 3 - 2nd ed. </em></p>
                                          ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>The Real Thing Videos (lab only)<br>
                                          <br>
                                          Contemporary Topics 3 (lab only)<br>
                                          <br>
                                          Well Said (lab only)<br>
                                          <br>
                                          <a href="../../academic-success/eap/registerforworkshopsandevents.html">Workshops</a> (lab only) 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>EAP 1520</strong>&nbsp; Reading 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><em>The Effective Reader w/ My Reading Lab and E Text access- 4th ed.<br>
                                                <br>
                                                </em></p>
                                          ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>Townsend Press: Advancing College Reading Skills (lab only) </p>
                                          
                                          <p><a href="http://www.pearsonmylabandmastering.com/northamerica/myreadinglab/">My Reading Lab</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>EAP 1540</strong>&nbsp; Writing 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><em> Longman Academic Writing 4- 5th ed. </em></div>
                                    </div>
                                    
                                    <div>
                                       <div>Make-a-Paragraph Kit (lab only) </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>EAP 1560</strong> Grammar 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><em> Understanding &amp; Using English Grammar with CDs - 4th ed. </em></div>
                                    </div>
                                    
                                    <div>
                                       <div>Understanding &amp; Using Grammar (access code) <br>
                                          <br>
                                          Grammar Sense 3 (lab only) 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>EAP 1585</strong> Grammar &amp; Writing 
                                    </div>
                                    
                                    <div><em>Understanding &amp; Using English Grammar with CDs - 4th ed. </em></div>
                                    
                                    <div>Understanding &amp; Using Grammar (access code) <br>
                                       <br>
                                       Grammar Sense 3 (lab only) 
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>EAP 1586<br>
                                          </strong>Speech &amp; Reading 
                                    </div>
                                    
                                    <div>No Textbook </div>
                                    
                                    <div>The Real Thing Videos (lab only)<br>
                                       <br>
                                       Contemporary Topics 3 (lab only)<br>
                                       <br>
                                       Well Said (lab only)<br>
                                       <br>
                                       <a href="../../academic-success/eap/registerforworkshopsandevents.html">Workshops</a> (lab only)<br>
                                       <br>
                                       Townsend Press: Advancing College Reading Skills (lab only) 
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>EAP 1620</strong>&nbsp; Reading 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>Reading Across the Disciplines - 6th ed. <br>
                                             
                                          </p>
                                          ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div> Townsend Press: Advanced Reading (lab only) <br>
                                          <br>
                                          LaunchPad (Online through Blackboard)
                                          <br>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><strong>EAP 1640</strong>&nbsp; Writing 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><em>Real Writing with Readings - 7th ed.<br>
                                                <br>
                                                </em></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>LaunchPad (online through Blackboard) <br>
                                          <br>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="labsandtextbooks.html">TOP</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center - EAP 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 105</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2098</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 3:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/labs-textbooks.pcf">©</a>
      </div>
   </body>
</html>