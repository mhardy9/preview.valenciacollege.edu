<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>        
                        
                        <h2>EAP Workshop Descriptions</h2>
                        
                        <p>All workshops are 1 hour. Students are expected to come on time, participate, and
                           stay until the end of the workshop. Instructors will be emailed attendance at the
                           end of the week. It is the students' responsibility to keep track of number of workshops
                           attended. Lab staff does not keep a running record of each individual student's attendance.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Course Name </div>
                                    </div>
                                    
                                    <div>
                                       <div>Description</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Conversation Hour </strong></div>
                                    
                                    <div>Group discussions and language games/activities.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Pronunciation</strong></div>
                                    
                                    <div>Four commonly confused sounds in the English language with spelling associated with
                                       sounds.
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>-ed Endings </strong></div>
                                    
                                    <div>Pronunciation rules of regular past tense verbs with -ed endings.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Stress and Linking</strong></div>
                                    
                                    <div>Common patterns involving word stress and linking.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Homophones</strong></div>
                                    
                                    <div>Words that are pronounced alike but differ in spelling and meaning.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Prefixes and Suffixes</strong></div>
                                    
                                    <div>Commonly occurring prefixes and suffixes.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Idioms</strong></div>
                                    
                                    <div>Familiar or common expressions that mean something other than the literal meaning
                                       of the words.
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Phrasal Verbs</strong></div>
                                    
                                    <div>Expressions which combine verbs and particles to make new verbs.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>PowerPoint Tutorial</strong></div>
                                    
                                    <div>How to make PowerPoint presentations. Beginning and advanced students welcome. </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Minimal Pairs</strong></div>
                                    
                                    <div>Words that vary by only a single sound with practice of minimal pair sets.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>MLA Format</strong></div>
                                    
                                    <div>How to format essays using MLA style. </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Nonverbal Communication</strong></div>
                                    
                                    <div>Understanding nonverbal communication &amp; learn how it enhances speeches. </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Slang</strong></div>
                                    
                                    <div>Discussion of slang and presentation of common slang in American culture.</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Composing an Essay </strong></div>
                                    
                                    <div>Guidance on elements of an essay, inlcuding thesis, introduction, and body </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>How to Sign Up </p>
                        
                        <p>To sign up for a workshop, please visit the <a href="index.html">The EAP Lab (4-105)</a>, call 407-582-2098, or sign up <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">electronically</a>! Seats are limited, so sign-up today! <br>
                           <br>
                           Check out our <a href="Final_Workshops.swf">tutorial video</a> on how to sign up for workshops. <br>
                           <br>
                           Check out our <a href="http://www.youtube.com/watch?v=EECP9pa9BqM&amp;feature=related">welcome video.</a></p>
                        
                        <p><a href="../../academic-success/eap/workshopdescriptions.html">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/workshops.pcf">©</a>
      </div>
   </body>
</html>