<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2><strong>Frequently Asked Questions </strong></h2>
                        
                        <h3><strong>What population of students does the EAP Lab serve? </strong></h3>
                        
                        <p>The EAP Lab serves international and domestic English language learners enrolled in
                           our English for Academic Purposes (EAP) program. Please see <a href="https://www.youtube.com/watch?v=b6QIgyewY0g">this video</a> to learn more about our students.
                        </p>
                        
                        <h3><strong>I'm an EAP student. When can I use the EAP Lab? </strong></h3>
                        
                        <p>You may use the EAP Lab during regular open hours. Please be advised that the EAP
                           Lab can be booked by professors for exclusive use for their classes. In the event
                           that you arrive and the EAP Lab is full, we will move you to the Language Lab next
                           door. The Language Lab is used as an overflow area and, therefore, is set up with
                           the same labs and programs as found in EAP. 
                        </p>
                        
                        <h3><strong>I'm an EAP professor. When can I use the EAP Lab? </strong></h3>
                        
                        <p>EAP professors may book space for their classes using our<a href="http://eastlrc.valenciacollege.edu/eap/addrequest.cfm"> online booking calendar</a>. Please note that booking the lab is on a first come, first served basis. The staff
                           will confirm your reservation via email. If you would like to make an appointment
                           to learn about the lab, please email <a href="mailto:aaslam2@valenciacollege.edu">Aysha Aslam</a>, lab coordinator.
                        </p>
                        
                        <h3>I'm a new EAP student. How do I learn about the services offered at the EAP Lab?</h3>
                        
                        <p>To begin, it's highly likely that your professor will book a beginning-of-semester
                           orientation for your class. During orientation, the EAP Lab staff will go over general
                           lab information, as well as information specific to your class. This being said, you
                           are welcome to come to the EAP Lab during open hours and meet our staff members. Anyone
                           working at the front desk can help answer your questions. 
                        </p>                      
                        
                        <h3>How do I print in the EAP Lab? </h3>
                        
                        <p>In order to print documents in the EAP Lab, you need a print card. Print cards are
                           sold in the main ASC area directly outside the EAP Lab. Black and white documents
                           cost 10 cents per page. In order to buy a print card, you must have dollar bills on
                           hand. The machine does not accept change. You can also buy a print card at the bookstore
                           in building 5 using a debit card. Print cards can be recharged, so please don't throw
                           your card away after it has been used. 
                        </p>                      
                        
                        <h3><strong>What is the EAP workshop series? </strong></h3>
                        
                        <p>The East Campus EAP workshop series is a co-curricular component for EAP0400, EAP1500,
                           and EAP1586. The series consists of Conversation Hour and multiple pronunciation,
                           vocabulary, grammar, and technology workshops. 
                        </p>
                        
                        <h3><strong>What is the purpose of the EAP workshop series? </strong></h3>
                        
                        <p>The purpose of the EAP workshop series is to increase English communication, listening,
                           vocabulary, and grammar skills.
                        </p>
                        
                        <h3><strong>Can I get credit for attending a workshop? </strong></h3>
                        
                        <p> Credit depends on your course and professor. In general, workshops are acceptable
                           lab credit for East Campus EAP0400, EAP1500, and EAP1586 courses. For any other course,
                           you would need to ask the professor if workshop attendance could possibly count towards
                           some sort of class credit. 
                        </p>
                        
                        <h3><strong>How do I get credit for attending a workshop? </strong></h3>
                        
                        <p> If your teacher allows workshop attendance to count towards lab credit, your attendance
                           per workshop will count as one lab credit. In other words, if you attend two workshops
                           in a semester, you will be allotted two lab credits. Keep in mind that in order to
                           receive credit you must come to the workshop on time, participate in all activities,
                           and stay until the end. 
                        </p>
                        
                        <h3><strong>Where can I find the EAP workshop schedule? </strong></h3>
                        
                        <p> You can find the workshop schedule <a href="../../academic-success/eap/registerforworkshopsandevents.html">online here</a>. You can also find the schedule and signup sheets at the EAP Lab (4-105). 
                        </p>
                        
                        <h3><strong>How do I signup for a workshop? </strong></h3>
                        
                        <p> To sign up for a workshop, visit the The EAP Lab on East Campus (4-105), call 407-582-2098,
                           or sign up <a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/writing/Workshopsign-up.cfm" target="_blank">electronically</a>. Please keep in mind that seats are limited and a reservation is not guaranteed.
                           
                        </p>
                        
                        <h3><strong>What happens when a workshop is full? </strong></h3>
                        
                        <p> If a workshop is full, you may be added to the waitlist. The waitlist does not mean
                           that you have a guaranteed spot in the workshop. Instead, you will be asked to come
                           to the EAP Lab a few minutes before the workshop starts to see if there is a spot
                           available. Spots only come available if another student does not show up. Waitlist
                           spots are fulfilled in order. In other words, if you're #2 on the waitlist, then you
                           will have to wait and see if two students do not show up before you will be offered
                           a spot. If you prefer not to wait, let the lab staff know and we will not add you
                           to the waitlist.
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>How does my teacher know I've attended a workshop?</strong></h3>
                        
                        <p> Workshop attendance will be sent to your professor at the beginning of the following
                           week. Please ask the EAP Lab staff if you have questions about the transmission of
                           attendance to professors. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>Can you tell me how many workshops I have attended this semester? </strong></h3>
                        
                        <p> No - The desk staff in the EAP Lab is not responsible for tracking your overall attendance
                           for the semester. You need to keep track of your attendance yourself or ask your professor.
                           Please keep in mind that many professors require their students to keep track of attendance
                           via an attendance log. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>I can't attend a workshop. Is there another way to get lab credit? </strong></h3>
                        
                        <p> Yes - if you are in EAP0400, EAP1500, or EAP1586 there is another way to get lab
                           credit if you cannot attend workshops. This involves listening activities on the computer.
                           Please ask the desk staff for more information. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>Do you have workshop descriptions available? I would like to know what will be taught
                              before I come.</strong></h3>
                        
                        <p> Yes - Please see the <a href="../../academic-success/eap/workshopdescriptions.html">Workshop Description</a> page for this. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>I would like to come to a workshop, but I will be late. Is that ok? </strong></h3>
                        
                        <p> No - It is not acceptable to be late for workshops. If you are late, we will not
                           count this towards lab credit. In addition, depending on how busy we are, your spot
                           might be gone. We give open spots to students signed up on the Waitlist. Once again,
                           if you are late, there is a possibility that we will give away your spot.
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>I would like to come to a workshop, but I need to leave early. Is that ok? </strong></h3>
                        
                        <p> No - Lab credit is only awarded for full participation. This includes staying until
                           the end of the workshop. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        <h3><strong>I'm not in the EAP program, but I am a Valencia student and a non-native speaker of
                              English. Can I come to a workshop?</strong></h3>
                        
                        <p> Yes! Provided that we have space, you are welcome to join! Please be aware that you
                           will need to sign up in advance like every other student. 
                        </p>
                        
                        <p><a href="frequentlyaskedquestions.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center - EAP 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 105</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2098</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9:00 am - 7:00 pm<br>Friday: 9:00 am - 3:00 pm<br>Saturday: 9:00 am - 3:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/faq.pcf">©</a>
      </div>
   </body>
</html>