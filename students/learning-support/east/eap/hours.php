<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>EAP at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/eap/hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>EAP at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/eap/">Eap</a></li>
               <li>EAP at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        <p><img alt="EAP" height="84" src="../../academic-success/eap/Tutoring-EAP.jpg" width="580"></p>
                        
                        <div>
                           
                           <h3>FALL &amp; SPRING HOURS</h3>
                           
                           <p><span><strong>Monday - Thursday</strong>: </span>9:00am – 7:00pm
                           </p>
                           
                           <p><span><strong>Friday</strong></span><strong>:</strong> 9:00am - 3:00pm
                              
                              
                           </p>
                           
                           <p><strong><strong>Saturday: </strong></strong>9:00am - 3:00pm 
                           </p>
                           
                           <p><strong><strong>Sunday</strong>: </strong>Closed
                           </p>
                           
                           <p><span><span>Final Exam Week:</span> </span>9:00am - 5:00pm 
                           </p>
                           
                           
                           <h3>SUMMER HOURS</h3>
                           
                           <p dir="auto"><strong>Monday - Thursday:</strong> 9:00am â€“ 7:00pm
                           </p>
                           
                           <p dir="auto"><strong>Friday:</strong> 9:00am â€“ 12:00pm
                           </p>
                           
                           <p dir="auto"><strong>Saturday:</strong> 8:00am - 2:00pm
                           </p>
                           
                           <p><strong><strong>Sunday</strong>: </strong>Closed
                           </p>
                           
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Location</h3>
                        EAP is located in the <strong>Academic Success Center</strong>,<br> 
                        which is on the <strong>first floor of Building 4</strong>,<strong> room 105</strong>.
                        
                        <ul>
                           
                           <li>View a <a href="../location.html">map and directions</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/eap/hours.pcf">©</a>
      </div>
   </body>
</html>