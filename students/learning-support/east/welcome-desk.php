<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ASC Welcome Desk at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/welcome-desk.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ASC Welcome Desk at the Academic Success Center</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>ASC Welcome Desk at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <h2>Welcome Desk</h2>
                     				
                     <p><strong>The ASC Welcome Desk provides learning support resources for<br>
                           					students, staff, and faculty</strong></p>
                     				
                     <h3>Items for Student Check-Out: </h3>
                     
                     				
                     <ul>
                        					
                        <li>Laptops</li>
                        					
                        <li>Tablets</li>
                        					
                        <li>Graphing Calculators</li>
                        					
                        <li>Headphones</li>
                        					
                        <li>GoPro Cameras </li>
                        				
                     </ul>
                     
                     				
                     <p>The above items have a 4-hour check-out limit. Double-check your receipt to make sure
                        it's not shorter or longer. Late items will be charged a late fee.
                     </p>
                     				
                     <p>Study rooms can only be checked out for 1 hour at a time; they can, however, be re-checked-out
                        unless there is a wait list or a tutoring session.
                     </p>            
                     				
                     <p>Room reservations can now be made at the following links. Subject to availability.</p>            
                     
                     				
                     <ul>
                        					
                        <li><strong><a href="https://checkappointments.com/appts/8jzdIrbbCY">ASL Study Room</a> </strong></li>
                        					
                        <li><strong><a href="https://checkappointments.com/appts/BxkEH0BMTO">Speech Lab</a></strong></li>
                        				
                     </ul>
                     				
                     <h3>Hours &amp; Location</h3>    
                     				
                     <h4>Fall and Spring Hours </h4>
                     				
                     <ul>
                        <li><i class="far fa-clock fa-fw"></i>Monday - Thursday: 7:00 am to 10:00 pm
                        </li>
                        					
                        <li><i class="far fa-clock fa-fw"></i>Friday: 7:00 am to 8:00 pm
                        </li>
                        					
                        <li>Saturday: 8:00 am to 4:00 pm </li>
                        				
                     </ul>
                     
                     				
                     <h4>Summer Hours: </h4>
                     				
                     <ul>
                        <li><i class="far fa-clock fa-fw"></i>Monday - Thursday: 7:00 am to 10:00 pm
                        </li>
                        					
                        <li><i class="far fa-clock fa-fw"></i>Friday: 7:00 am to 12:00 pm (noon)
                        </li>
                        					
                        <li>Saturday: 8:00 am to 2:00 pm </li>
                        				
                     </ul>
                     				
                     <h3>Staff</h3>
                     				
                     <ul class="list_staff">
                        					
                        <li>
                           						
                           <figure><img alt="Nichole Merris" src="../../academic-success/welcomedesk/nichole-merris.jpg" class="img-circle"></figure>
                           						
                           <h4>Nichole Merris</h4>
                           						
                           <p>General Tutoring &amp; Welcome Desk Coordinator</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img alt="Darla Smith" src="../../academic-success/welcomedesk/darla-smith.jpg" class="img-circle"></figure>
                           						
                           <h4>Darla Smith</h4>
                           						
                           <p>Assistant Coordinator</p>
                           					
                        </li>	
                        					
                        <li>
                           						
                           <figure><img alt="Priscilla Garcia" src="/_resources/img/no-photo-female-thumb.png" class="img-circle"></figure>
                           						
                           <h4>Priscilla Garcia</h4>
                           						
                           <p>Assistant Coordinator</p>
                           					
                        </li>		
                        					
                        <li>
                           						
                           <figure><img alt="Christen Costello" src="../../academic-success/welcomedesk/christen-costello.jpg" class="img-circle"></figure>
                           						
                           <h4>Christen Costello</h4>
                           						
                           <p>Assistant Coordinator</p>
                           					
                        </li>	
                        					
                        <li>
                           						
                           <figure><img alt="Alex Higgins" src="../../academic-success/welcomedesk/alex-higgins.jpg" class="img-circle"></figure>
                           						
                           <h4>Alex Higgins</h4>
                           						
                           <p>Assistant Coordinator</p>
                           					
                        </li>	
                        					
                        <li>
                           						
                           <figure><img alt="Alana Maldonado" src="/_resources/img/no-photo-female-thumb.png" class="img-circle"></figure>
                           						
                           <h4>Alana Maldonado</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>	
                        					
                        <li>
                           						
                           <figure><img alt="Areana George" src="../../academic-success/welcomedesk/areana-george.jpg" class="img-circle"></figure>
                           						
                           <h4>Areana George</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>	
                        					
                        <li>
                           						
                           <figure><img alt="Edwin Jones" src="../../academic-success/welcomedesk/edwin-jones.jpg" class="img-circle"></figure>
                           						
                           <h4>Edwin Jones</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>				
                        					
                        <li>
                           						
                           <figure><img alt="Kisha Ratliff" src="../../academic-success/welcomedesk/kisha-ratliff.jpg" class="img-circle"></figure>
                           						
                           <h4>Kisha Ratliff</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>				
                        					
                        <li>
                           						
                           <figure><img alt="Jonathan Mevs" src="/_resources/img/no-photo-male-thumb.png" class="img-circle"></figure>
                           						
                           <h4>Jonathan Mevs</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>			
                        					
                        <li>
                           						
                           <figure><img alt="Sindy Pegus" src="/_resources/img/no-photo-female-thumb.png" class="img-circle"></figure>
                           						
                           <h4>Sindy Pegus</h4>
                           						
                           <p>Welcome Desk Staff</p>
                           					
                        </li>			
                        				
                     </ul>
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/welcome-desk.pcf">©</a>
      </div>
   </body>
</html>