<ul>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/">Learning Support <i aria-hidden="true" class="far fa-angle-down"></i></a> 
		<ul>
			<li><a href="/students/learning-support/learning-centers.php">Learning Centers</a> </li>	
			<li><a href="/students/learning-support/writing-consultations.php">Writing Consultations</a></li>    
			<li><a href="/students/learning-support/skillshops.php">Skillshops</a> </li>
			<li><a href="http://net4.valenciacollege.edu/forms/students/learning-support/contact.php" target="_blank">Contact Us</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/">East Learning Centers <i aria-hidden="true" class="far fa-angle-down"></i></a> 
		<ul>
			<li><a href="/students/learning-support/east/employment.php">Employment</a></li>
			<li><a href="/students/learning-support/east/events.php">Events</a></li>
			<li><a href="/students/learning-support/east/faq.php">FAQ</a></li>
			<li><a href="/students/learning-support/east/featured-tutor.php">Featured Tutor</a></li>
			<li><a href="/students/learning-support/east/hours.php">Hours</a></li>
			<li><a href="/students/learning-support/east/location.php">Location</a></li>
			<li><a href="/students/learning-support/east/mission.php">Mission</a></li>
			<li><a href="/students/learning-support/east/online-tutoring.php">Online Tutoring</a></li>
			<li><a href="/students/learning-support/east/resources.php">Resources</a></li>
			<li><a href="/students/learning-support/east/technology.php">Technology</a></li>
			<li><a href="/students/learning-support/east/virtual-library.php">Virtual Library</a></li>
			<li><a href="/students/learning-support/east/welcome-desk.php">Welcome Desk</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/tutoring/">General Tutoring <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/east/tutoring/Accounting-Economics.php">Accounting & Economics</a></li>
			<li><a href="/students/learning-support/east/tutoring/american sign-language.php">American Sign Language (ASL)</a>
			<li><a href="/students/learning-support/east/tutoring/biology.php">Biology</a>
			<li><a href="/students/learning-support/east/tutoring/chemistry.php">Chemistry</a>
			<li><a href="/students/learning-support/east/tutoring/computer-programming.php">Computer Programming & OST</a>
			<li><a href="/students/learning-support/east/tutoring/music-theory.php">Music Theory</a>
			<li><a href="/students/learning-support/east/tutoring/physics.php">Physics</a>
			<li><a href="/students/learning-support/east/tutoring/political-science.php">Political Science</a>		  
			<li><a href="/students/learning-support/east/tutoring/faculty-resources.php">Faculty Resources</a>
			<li><a href="/students/learning-support/east/tutoring/faq.php">FAQ</a>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/communications/">Communications <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>

			<li><a href="/students/learning-support/east/communications/what-to-expect.php">What to Expect</a></li>
			<li><a href="/students/learning-support/east/communications/writing.php">Writing</a></li>
			<li><a href="/students/learning-support/east/communications/reading.php">Reading</a></li>
			<li><a href="/students/learning-support/east/communications/speech-lab.php">Speech</a></li>
			<li><a href="/students/learning-support/east/communications/PERT-Review-Workshops.php">PERT</a></li>
			<li><a href="/students/learning-support/east/communications/worksheets.php">Videos and Worksheets</a></li>
			<li><a href="/students/learning-support/east/communications/lab-software.php">Lab Software</a></li>
			<li><a href="/students/learning-support/east/communications/faculty-resources.php">Faculty Resources</a></li>
			<li><a href="/students/learning-support/east/communications/staff.php">Meet Our Staff</a></li>
			<li><a href="/students/learning-support/east/communications/faq.php">FAQ</a></li>
			<li><a href="/students/learning-support/east/communications/feedback.php">Feedback</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/eap/">EAP Lab <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/east/eap/register-events.php">Register for Workshops &amp; Events</a></li>
			<li><a href="/students/learning-support/east/eap/labs-textbooks.php">Labs &amp; Textbooks</a></li>
			<li><a href="/students/learning-support/east/eap/faculty-resources.php">Faculty Resources</a></li>
			<li><a href="/students/learning-support/east/eap/staff.php">Meet Our Staff</a></li>
			<li><a href="/students/learning-support/east/eap/faq.php">FAQ</a></li>
			<li><a href="/students/learning-support/east/eap/useful-links.php">Useful Links</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/language/">Language Labs <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>		
			<li><a href="/students/learning-support/east/language/what-to-expect.php">What to Expect</a></li>
			<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/language/spanish.php">Spanish <i aria-hidden="true" class="far fa-angle-down"></i></a>
				<ul><li><a href="/students/learning-support/east/language/spanish-workshops.php">Workshops</a></li>
					<li><a href="/students/learning-support/east/language/spanish-worksheets.php">Printable Worksheets</a></li>
				</ul></li>
			<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/language/french.php">French <i aria-hidden="true" class="far fa-angle-down"></i></a>
				<ul>
					<li><a href="/students/learning-support/east/language/french-workshops.php">Workshops</a></li>
					<li><a href="/students/learning-support/east/language/french-worksheets.php">Printable Worksheets</a></li>
				</ul>
			</li>

			<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/language/german.php">German <i aria-hidden="true" class="far fa-angle-down"></i></a>
				<ul><li><a href="/students/learning-support/east/language/german-workshops.php">Workshops</a></li>
					<li><a href="/students/learning-support/east/language/german-worksheets.php">Printable Worksheets</a></li>
				</ul>
			</li>
			<li><a href="/students/learning-support/east/language/latin.php">Latin</a></li>
			<li><a href="/students/learning-support/east/language/placement-test.php">Foreign Language Placement Test</a></li>
			<li><a href="/students/learning-support/east/language/faculty-resources.php">Faculty Resources</a></li>
			<li><a href="/students/learning-support/east/language/staff.php">Meet Our Staff</a></li>
			<li><a href="/students/learning-support/east/language/tutors.php">Meet Our Tutors</a></li>
			<li><a href="/students/learning-support/east/language/lab-faq.php">FAQ</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/students/learning-support/east/language/feedback.php" target="_blank">Feedback</a></li>
		</ul>
	</li>
	<li><a href="/students/learning-support/east/math/">Math Center</a>
		<ul>
			<li><a href="/students/learning-support/east/math/calculators.php">Calculator Information </a></li>	
			<li><a href="/students/learning-support/east/math/class-types.php">Class Types </a></li>
			<li><a href="/students/learning-support/east/math/math-path.php">Math Path</a></li>
			<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/math/lab/">Mathematics Lab <i aria-hidden="true" class="far fa-angle-down"></i></a>		
				<ul>
					<li><a href="/students/learning-support/east/math/lab/courses.php">Courses</a></li>
					<li><a href="/students/learning-support/east/math/lab/emporium.php">Emporium</a></li>
					<li><a href="/students/learning-support/east/math/lab/assistants.php">Lab Assistants </a></li>
					<li><a href="/students/learning-support/east/math/lab/resources.php">Faculty Resources</a></li>
				</ul></li>
			<li><a href="/students/learning-support/east/math/staff.php">Meet Our Staff</a></li>
			<li><a href="/students/learning-support/east/math/stumpers/"> Richard's Stumpers</a></li>
			<li><a href="/students/learning-support/east/math/state-test.php">State Test </a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/east/testing/">Testing Center <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>

			<li><a href="/students/learning-support/east/testing/workshops.php">Testing Workshops</a></li>
			<li><a href="/students/learning-support/east/testing/proctoring.php">Need Proctoring?</a></li>
			<li><a href="/students/learning-support/east/testing/faculty-resources.php">Faculty Resources</a></li>
			<li><a href="/students/learning-support/east/testing/staff.php">Meet Our Staff</a></li>
			<li><a href="/students/learning-support/east/testing/faq.php">FAQ</a></li>
		</ul>
	</li>

</ul>