<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <link href="../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           <div> 
                              
                              <div>
                                 <img alt="Featured Image 1" border="0" height="260" src="../../academic-success/math/m1.png" width="770">
                                 
                              </div>
                              
                              
                              <div>
                                 <img alt="Featured Image 2" border="0" height="260" src="../../academic-success/math/m2.png" width="770">
                                 
                              </div> 
                              
                              
                              <div>
                                 <img alt="Featured Image 3" border="0" height="260" src="../../academic-success/math/m3.png" width="770">
                                 
                              </div> 
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <h2>
                           <a name="content" id="content"></a>
                           <a href="../../academic-success/math/index.html"></a>
                           
                        </h2>
                        
                        <h2><strong><u>Meet Our Staff</u></strong></h2>
                        
                        <p>Math Center Coordinator </p>
                        
                        <p><img alt="Richard" border="1" height="150" src="../../academic-success/math/richardW.jpg" width="150"></p>
                        
                        <p><a href="mailto:rweinsier@valenciacollege.edu">Richard Weinsier</a></p>
                        
                        <p>Richard oversees the entire Math Center here in the Academic Success Center. He loves
                           nothing more than to help out students who are in need, especially if it is related
                           to the field of mathematics. 
                        </p>
                        
                        <p><strong><u>Math Center Coordinator</u></strong><br>
                           rweinsier@valenciacollege.edu <br>
                           407-582-2775<br>
                           East Campus <br>
                           
                        </p>
                        
                        <p>Assistant Coordinators</p>
                        
                        <p><img alt="Lori Keegan" height="200" hspace="0" src="../../academic-success/math/loriann.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>LORI ANN KEEGAN </strong></u></p>
                        
                        <p>Best known for rolling 1's in D&amp;D. Except when rolling stats - then it's all 20's.</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Brandon Sabedra" height="199" hspace="0" src="../../academic-success/math/brandon.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>BRANDON A. SABEDRA </strong></u></p>
                        
                        <p>Brandon is an Instructional Assistant in the Math Center. He is currently seeking
                           his B.S. in Computer Engineering at UCF. In his free time he enjoys all things food-related
                           and playing video games.
                        </p>
                        
                        
                        <p>Math Tutors</p>
                        
                        <p><img alt="Alexis Nequinto" height="200" hspace="0" src="../../academic-success/math/alexis.jpg" vspace="0" width="150"></p>
                        
                        <p><strong><u>ALEXIS NEQUINTO</u></strong></p>
                        
                        <p>Alexis is currently at UCF and is an Environmental Engineering major. In her spare
                           time, she loves cooking, traveling and eating. Alexis has worked at Valencia for almost
                           two years and took classes here at Valencia before going to UCF.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Austin Stephen" height="200" hspace="0" src="../../academic-success/math/austin.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>AUSTIN STEPHEN</strong></u></p>
                        
                        <p>Austin is currently a student at UCF and is studying Aerospace Engineering. He has
                           an Articulated A.A. from Valencia.
                        </p>
                        
                        <p>Favorite Book: The Martian - Favorite Movie: Sunshine - Favorite Band: The Mountain
                           Goats
                        </p>
                        
                        <p>GO ORLANDO CITY!!</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Ben Keene" height="199" hspace="0" src="../../academic-success/math/ben.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>BEN KEENE</strong></u></p>
                        
                        <p>Ben enjoys tutoring Math and Physics.</p>
                        
                        <div>He is currently finishing his B.A. degree in Applied Mathematics.</div>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        <p><img alt="Catherine Plumer" height="200" hspace="0" src="../../academic-success/math/catherine.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>CATHERINE PLUMMER </strong></u></p>
                        
                        <p>Along with a B.A. degree, Catherine added a Minor in Mathematics because of her immense
                           passion for Math. Her main background is Education and she greatly enjoys working
                           with students of all ages.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Christopher Andrew" height="199" hspace="0" src="../../academic-success/math/chrisa.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>CHRISTOPHER ANDREW </strong></u></p>
                        
                        <p>Even though he was born in Florida, Christopher spent much of his childhood on a farm
                           in Bulgaria. He is currently a computer specialist majoring in Computer Science. His
                           passion for math grew as he studied his major.
                        </p>
                        
                        <p>Favorite things: Large bodies of water, computers, psychology, both cats &amp; dogs.</p>
                        
                        <p>Christopher likes to consider himself a “Quantum Buddhist.”</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Damika Sanders" height="200" hspace="0" src="../../academic-success/math/damika.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>DAMIKA SANDERS</strong></u></p>
                        
                        <p>Damika greatly enjoys space, kayaking, traveling, learning about people &amp; cultures,
                           and learning in general. Her true passion is math, go figure, and she is more excited
                           about epiphanies, such as her personal learning and her students learning experience.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Eric Ward" height="199" hspace="0" src="../../academic-success/math/eric.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>ERIC WARD</strong></u></p>
                        
                        <p>Eric is currently majoring in Mechanical Engineering at UCF. He is a senior and is
                           working on an arm exoskeleton project with Limbitless.
                        </p>
                        
                        <p>In his spare time he likes to play board games and go running. </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Ian McTigue" height="199" hspace="0" src="../../academic-success/math/ian.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>IAN McTIGUE </strong></u></p>
                        
                        <p>Ian truly loves to play chess. Come into the Math Center to get some help with your
                           studies or to get schooled in a game of chess.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Josh Garber" height="200" hspace="0" src="../../academic-success/math/josh.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>JOSH GARBER </strong></u></p>
                        
                        <p>Joshua greatly enjoys talking in general! He would love to help you with any of your
                           math-related problems. 
                        </p>
                        
                        <p>And if you would rather talk about your favorite food, philosophy or how your day
                           is going, then Joshua is your guy! 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Matthew Garcia" height="200" hspace="0" src="../../academic-success/math/matthew.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>MATTHEW GARCIA </strong></u></p>
                        
                        <p>Matthew is a student of Mechanical Engineering at UCF. He enjoys helping people understand
                           the mathematics of the world around them, especially algebraically.
                        </p>
                        
                        <p>In his free time, he loves to run and ride his motorcycle around Central Florida.</p>
                        
                        <h3><u></u></h3>
                        
                        <p><img alt="Nathan Vargas" height="200" hspace="0" src="../../academic-success/math/nate.jpg.html" vspace="0" width="150"></p>
                        
                        <p><u><strong>NATHAN VARGAS </strong></u></p>
                        
                        <p>Nathan loves to do light reading. Currently he is majoring in Lasers and Photonics.</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Nicholas Pearson" height="199" hspace="0" src="../../academic-success/math/nickp.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>NICHOLAS PEARSON </strong></u></p>
                        
                        <p>Nicholas is currently a marketing major that has been working at Valencia College
                           for six years. He enjoys working out and the great outdoors. His hobbies consist of
                           billiards and rooting for Liverpool F.C.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Nick Steele" height="200" hspace="0" src="../../academic-success/math/nicks.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>NICK STEELE </strong></u></p>
                        
                        <p>Nick “The Man of”Steele is currently a Computer Engineering major at UCF.</p>
                        
                        <p>He works here at Valencia because of his immense love for the educational environment.</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Raul Quintana" height="201" src="../../academic-success/math/RaulQuintana.jpg.html" width="201"></p>
                        
                        <p><u><strong>RAUL QUINTANA </strong></u></p>
                        
                        <p>I've been working at Valencia since the Fall of 2016. I can speak Spanish and I love
                           tutoring math and physics! 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Santiago Rodriguez" height="199" hspace="0" src="../../academic-success/math/santiago.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>SANTIAGO RODRIGUEZ </strong></u></p>
                        
                        <p>Santiago is proudly Colombian. He is currently majoring in Civil Engineering. In his
                           free time he likes to skateboard.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Sarah Jackman" height="200" hspace="0" src="../../academic-success/math/sarahj.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>SARAH JACKMAN </strong></u></p>
                        
                        <p>Sarah is a math major and loves nothing more than to torture fellow tutor, Zak Pedone!</p>
                        
                        <h3><u></u></h3>
                        
                        <p><img alt="Timothy Wingert" height="200" hspace="0" src="../../academic-success/math/tim.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>TIMOTHY WINGERT</strong></u></p>
                        
                        <p>Timothy likes to juggle, read fantasy novels, and has 3 awesome cats at his house.</p>
                        
                        <p>Ask Tim and he will gladly tell you that Math is marvelous, and it is the only subject
                           that counts.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Travis Stamper" height="199" hspace="0" src="../../academic-success/math/travis.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>TRAVIS STAMPER </strong></u></p>
                        
                        <p>Travis is currently a Computer Science major over at UCF. He absolutely loves math
                           and plans on teaching college math in the future.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Yurat Abraham" height="199" hspace="0" src="../../academic-success/math/yurat.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>YURAT ABRAHAM </strong></u></p>
                        
                        <p>Yurat graduated from Valencia College in 2009 and has worked for the College since
                           2011. Previously, he worked at the Lake Nona Campus tutoring both science &amp; math.
                           His professional interest is cancer &amp; neuron research. Outside of work, he enjoys
                           scary movies, basketball, traveling, video games, plus Marvel and DC comics.
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><img alt="Zak Mefrouche" height="200" hspace="0" src="../../academic-success/math/zakm.jpg" vspace="0" width="150"></p>
                        
                        <p><u><strong>ZAK MEFROUCHE </strong></u></p>
                        
                        <p>Zak enjoys all math topics and in his free time he thoroughly enjoys solving puzzles.</p>
                        
                        <h3>&nbsp;</h3>
                        
                        <p><a href="../../academic-success/math/contact.html">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/staff.pcf">©</a>
      </div>
   </body>
</html>