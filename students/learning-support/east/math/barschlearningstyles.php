<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/barschlearningstyles.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <form enctype="application/x-www-form-urlencoded" method="post" name="barsch" id="barsch">
                           
                           <h2><font face="Arial, Helvetica, sans-serif">
                                 
                                 Barsch Learning Style Inventory</font></h2>
                           
                           
                           <hr noshade size="2" width="90%">
                           
                           
                           <p><font face="Arial, Helvetica, sans-serif"><strong>Instructions:</strong><br>
                                 The Barsch Inventory is one of the quick assessments of your learning style. Try to
                                 answer the questions by basing your answers on your actual learning preference and
                                 not areas which you would like to have as strengths.</font></p>
                           
                           <p><font face="Arial, Helvetica, sans-serif"><strong>Scoring:</strong><br>
                                 The inventory has 24 statements which are assigned values and these values are used
                                 in the scoring process. The selections, values, and descriptions are listed below.
                                 Three learning styles will be defined once the test is complete: Visual (sight), Auditory
                                 (sound), and Tactile/Kinesthetic (small/large motor movements).</font></p>
                           
                           <center>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <p><font face="Arial, Helvetica, sans-serif"><strong>Selection</strong></font> 
                                          </p>
                                       </div>
                                       
                                       <div>
                                          <p><font face="Arial, Helvetica, sans-serif"><strong>Value</strong></font> 
                                          </p>
                                       </div>
                                       
                                       <div>
                                          <p><font face="Arial, Helvetica, sans-serif"><strong>Description</strong></font> 
                                          </p>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Often True</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">5 Points</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">This statement is often true of me.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Sometimes True</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">3 Points</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">This statement is sometimes true of me (about half the time).</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Seldom True</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">1 Point</font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">This statement is seldom true of me.</font></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <hr noshade size="2" width="90%">
                              <font face="Arial, Helvetica, sans-serif"><a name="reset" id="reset"></a> </font>
                              
                              
                           </center>
                           
                           <p><font face="Arial, Helvetica, sans-serif"><strong>Barsch Learning Style Preference Form:<br>
                                    </strong>Read each statement carefully and select from the list the best answer.</font></p>
                           
                           <center>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_1">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Follow written directions better than oral directions.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_2">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Like to write things down or take notes for visual review.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_3">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Am skillful and enjoy developing and making graphs and charts.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_4">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Can understand and follow directions on maps.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_5">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Can better understand a news article by reading about than by listening to it on the
                                             radio.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_6">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Feel the best way to remember is to picture it in your head.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_7">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Grip objects in your hands during learning periods.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb1_8">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Obtain information on an interesting subject by reading related materials.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_1">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Can remember more about a subject through listening than reading.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_2">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Require explanations of graphs, diagrams, or visual directions.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_3">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Can tell if sounds match when presented with pairs of sounds.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_4">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Do better at academic subjects by listening to tapes and lectures.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_5">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Learn to spell better by repeating the letters out loud than by writing the word on
                                             paper.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_6">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Would rather listen to a good lecture or speech rather than read about the same material
                                             in a book.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_7">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Prefer listening to the news on the radio than reading about it in the newspaper.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb2_8">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Follow oral directions better than written ones.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_1">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Bear down extremely hard when writing.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_2">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Enjoy working with tools or working on models.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_3">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Remember best by writing things down several times.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_4">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Play with coins or keys in pockets.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_5">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Chew gum, snack, or smoke during studies.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_6">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Do a lot of gesturing, am well coordinated.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_7">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Am good at working and solving jigsaw puzzles and mazes.</font></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">
                                             <select name="lb3_8">
                                                
                                                <option selected value="0"> </option>
                                                
                                                <option value="5">Often True </option>
                                                
                                                <option value="3">Sometimes True </option>
                                                
                                                <option value="1">Seldom True </option>
                                                </select>
                                             </font></div>
                                       
                                       <div><font face="Arial, Helvetica, sans-serif">Feel very comfortable touching others, hugging, handshaking, etc.</font></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </center>
                           
                           <center>
                              <font face="Arial, Helvetica, sans-serif">
                                 <input name="getResults" onclick="GetResults()" type="button" value="Get Barsch Inventory Results">
                                 <input onclick="Reset()" type="reset" value="Reset Form">
                                 </font>
                              
                              
                              <hr noshade size="2" width="90%">
                              <font face="Arial, Helvetica, sans-serif"><a name="results" id="results"></a> </font>
                              
                           </center>
                           <font color="blue" face="Arial, Helvetica, sans-serif"><strong>Scoring:</strong></font><font face="Arial, Helvetica, sans-serif"><br>
                              Your learning style scores are shown in the table below:</font>
                           
                           
                           <center>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <p><font color="blue" face="Arial, Helvetica, sans-serif"><strong>Visual</strong></font> 
                                          </p>
                                       </div>
                                       
                                       <div>
                                          <p><font color="blue" face="Arial, Helvetica, sans-serif"><strong>Auditory</strong></font> 
                                          </p>
                                       </div>
                                       
                                       <div>
                                          <p><font color="blue" face="Arial, Helvetica, sans-serif"><strong>Kinesthetic</strong></font> 
                                          </p>
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <center>
                                             <font face="Arial, Helvetica, sans-serif">
                                                <input name="txt1" readonly size="2" type="text">
                                                </font>
                                             
                                          </center>
                                       </div>
                                       
                                       <div>
                                          <center>
                                             <font face="Arial, Helvetica, sans-serif">
                                                <input name="txt2" readonly size="2" type="text">
                                                </font>
                                             
                                          </center>
                                       </div>
                                       
                                       <div>
                                          <center>
                                             <font face="Arial, Helvetica, sans-serif">
                                                <input name="txt3" readonly size="2" type="text">
                                                </font>
                                             
                                          </center>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </center>
                           
                           <p><font face="Arial, Helvetica, sans-serif">Close or tied scores may indicate your ability to learn comforatably from either learning
                                 style. Write the scores down or print this page. To find out more about your scores
                                 and how to use them, read below. </font></p>
                           
                        </form>          
                        <h2>&nbsp;</h2>
                        
                        <p><strong>Barsch Learning Styles Explanations: </strong></p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <br>
                                       Learning Style 
                                    </div>
                                    
                                    <div>
                                       <p>Clues </p>
                                    </div>
                                    
                                    <div>
                                       <p>Learning Tips </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>Visual </strong></p>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Needs to see it to know it. </li>
                                          
                                          <li>Strong sense of color. </li>
                                          
                                          <li>May have artistic ability. </li>
                                          
                                          <li>Difficulty with spoken directions. </li>
                                          
                                          <li>May be easily distracted by sounds. </li>
                                          
                                          <li>trouble following lectures. </li>
                                          
                                          <li>Misinterpretation of spoken words. </li>
                                          
                                       </ul>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Use graphics to reinforce. </li>
                                          
                                          <li>Color coding to organize notes and possessions. </li>
                                          
                                          <li>Written directions. </li>
                                          
                                          <li>Use of flow charts and diagrams for note-taking. </li>
                                          
                                          <li>Visualize spelling of words of facts to be memorized. </li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>Auditory </strong></p>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Prefers to get information by listening-needs to hear it or speak it to know it. </li>
                                          
                                          <li>Written directions more difficult to follow than spoken directions. </li>
                                          
                                          <li>Prefers listening to reading and writing. </li>
                                          
                                          <li>Inability to read body language and facial expression. </li>
                                          
                                       </ul>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Use of tapes for reading and for class lecture notes. </li>
                                          
                                          <li>Learning by interviewing or by participating in discussions. </li>
                                          
                                          <li>Works well in study groups. </li>
                                          
                                          <li>Having test questions or directions read aloud or put on tape. </li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><strong>Kinesthetic </strong></p>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Prefers hands-on learning. </li>
                                          
                                          <li>Can assemble parts without reading directions. </li>
                                          
                                          <li>Difficulty sitting still. </li>
                                          
                                          <li>Learns better when physical activity is involved. </li>
                                          
                                          <li>May be very well coordinated and have athletic ability. </li>
                                          
                                       </ul>
                                    </div>
                                    
                                    <div>
                                       <ul type="disc">
                                          
                                          <li>Experiential learning (making models,, doing lab work, and role playing). </li>
                                          
                                          <li>Frequent breaks in study periods. </li>
                                          
                                          <li>Tracing letters and words to learn spelling and to remember facts. </li>
                                          
                                          <li>Use computer to reinforce learning through sense of touch. </li>
                                          
                                          <li>Memorize or drilling while walking or exercising. </li>
                                          
                                          <li>Usually involves some kind of movement while learning, i.e. tapping pencil, shaking
                                             foot, and/or holding somet 
                                          </li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <p><strong><a href="../../academic-success/math/BarschLearningStyles.html">TOP</a></strong></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../academic-success/math/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/barschlearningstyles.pcf">©</a>
      </div>
   </body>
</html>