<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/class-types.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Types of Classes</h2>
                        
                        <p><strong>Developmental area (SPA) provides assistance for the following courses:</strong></p>
                        
                        <ul>
                           
                           <li> MAT0018C - Developmental Math I</li>
                           
                           <li>MAT0022C - Developmental Math Combined</li>
                           
                           <li>MAT0028C - Developmental Math II</li>
                           
                           <li>MAT1033C - Intermediate Algebra</li>
                           
                           <li>MAC1105 - College Algebra</li>
                           
                           <li> MGF1106 - College Mathematics </li>
                           
                        </ul>
                        
                        <p><strong>Upper level area (MSC) provides assistance for the following courses:</strong></p>
                        
                        <ul>
                           
                           <li>MAC1105 - College Algebra</li>
                           
                           <li>MAC1114 - Trigonometry</li>
                           
                           <li>MAC1140 - Precalculus Algebra</li>
                           
                           <li>MAC2233 - Business Calculus </li>
                           
                           <li>MAC2311 - Calculus I</li>
                           
                           <li>MAC2312 - Calculus II</li>
                           
                           <li>MAC2313 - Calculus III</li>
                           
                           <li>MAP2302 - Differential Equation</li>
                           
                           <li>STA1001C - Statistical Understanding</li>
                           
                           <li>STA2023 - Statistical Methods</li>
                           
                           <li>MGF1107 - Math for Liberal Art</li>
                           
                        </ul>            
                        
                        <p>Standard Classroom</p>
                        
                        <p> 2 variations - Full term and Flex start<br>
                           Much of the information comes from instructor<br>
                           Homework is done outside the classroom (many instructors use on-line homework)<br>
                           Tests are given at discretion of the instructor<br>
                           Supplemental Learning Leaders - an additional resource for some classes
                        </p>
                        
                        <p>Intensive Class (also called a Combo class)</p>
                        
                        <p> Basically this is 2 classes being done in one semester<br>
                           Much of the information comes from instructor<br>
                           Homework is done outside the classroom (many instructors use on-line homework)<br>
                           Tests are given at discretion of the instructor
                        </p>
                        
                        <p>On-Line Class</p>
                        
                        <p> All course content is delivered online<br>
                           There may be a suggested orientation with instructor at the beginning of the term<br>
                           Personal support will be given to all students on-line
                        </p>
                        
                        <p>Hybrid Class</p>
                        
                        <p> Blends online and face-to-face delivery<br>
                           Any one of the blends will be a maximum of 75% of course<br>
                           Homework may be done either on-line or from the textbook
                        </p>
                        
                        <p>LinC classes</p>
                        
                        <p> These are two classes that are linked together to integrate class material across
                           the two disciplines<br>
                           Both classes are run as standard classrooms 
                        </p>
                        
                        <p>College-Level Examination Program (CLEP) Test</p>
                        
                        <p> This is a test that you can take at a cost of approximately $100 to get credit for
                           any of the following math classes:<br>
                           College Mathematics (MGF1107)<br>
                           College Algebra (MAC1105)<br>
                           Trigonometry (MAC1114)<br>
                           College Algebra &amp; Trigonometry (MAC1147)<br>
                           Calculus with Elementary Functions (MAC2233)
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/classtypes/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/class-types.pcf">©</a>
      </div>
   </body>
</html>