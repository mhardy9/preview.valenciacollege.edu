<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_23.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #23</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p> Ann, Bess, and Cathy were having their weekly game session. The girls agreed that
                                 whoever lost must pay the other two the amount of money each had at that time; thus,
                                 the other two would have doubled their money. The game began. Ann lost first and paid
                                 Bess and Cathy an amount so that each doubled her money. Next, Bess lost and paid
                                 Ann and Cathy in the same manner. Finally, Cathy lost and paid Ann and Bess. The game
                                 session ended and each girl had 8 cents. How much money did each girl have before
                                 they started playing the game?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Supposing you were offered jobs by two different firms. Both firms were exactly the
                                 same and each would require 40-hour weeks with no overtime. Firm A agreed to give
                                 you a salary of $6,500 to begin with and a $500 raise at the end of each year for
                                 the first three years. Firm B promised $6,240 to begin with and a $.50 per hour raise
                                 each year for three years. Which job would you take and WHY?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Name one US currency bill beneath the value of $100 upon which there is no portrait
                                 of a president? Who is it and what bill is it on?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The father of twin boys, Ronnie and Tommy, is concerned about Ronnie’s weight. Ronnie
                                 is eight pounds lighter that his brother. Ronnie weighs one pound for every year of
                                 his father’s age. Tommy weighs one pound for every 10 months of his father’s age.
                                 How much do the twins weigh?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> One Saturday, Dawn’s mother went grocery shopping. When she came home from the store,
                                 Dawn found that her mother bought 5 bags of groceries. One bag had all the meats in
                                 it, one had all the dairy products, one had all the vegetables, one had the cleaning
                                 products, and the last bag had odds and ends. She guessed that the bag with the dairy
                                 products was packed last. How did she know that?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Most past and present students of mathematics are aware that 4 squared is NOT the
                                 same as 42, and most are certain that 3 to the 4th power times 6 to the 3rd power
                                 do NOT equal 3463. However, there are two digits, each to the power of another, which
                                 when multiplied together, give just such a coincidental answer. If you don’t have
                                 an hour or so to space, you might accept this hint: one digit is squared, the other
                                 is to the 5th power. What are these two digits?
                              </p>
                              
                           </li>
                           
                           <li> Larson, Janes, Murphy, and Smith are four men whose occupations are butcher, banker,
                              grocer, and policeman. What is the occupation of each man?
                           </li>
                           
                        </ol>
                        
                        <blockquote>
                           
                           <ol type="a">
                              
                              <li>Larson and Janes are neighbors and take turns driving each other to work.</li>
                              
                              <li> Janes makes more money than Murphy.</li>
                              
                              <li> Larson beats Smith regularly at bowling.</li>
                              
                              <li>The butcher always walks to work.</li>
                              
                              <li> The policeman does not live near the banker.</li>
                              
                              <li>The only time the grocer had met the policeman was when the policeman arrested the
                                 grocer for speeding.
                              </li>
                              
                              <li>The policeman makes more money than the banker or the grocer. </li>
                              
                           </ol>
                           
                           <p><span>Answers are available with Stumper <strong>Set #23.</strong></span></p>
                           
                        </blockquote>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_23.html#top">TOP</a></p>
                        
                        <p><u>Answers to Stumper Set #22</u></p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>Jill is Jean’s mother. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>13 or more checks </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The oldest is John, the youngest is Thomas. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 8 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Bill is 8 years old. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 7 - $.50, 2 - $.25, 1 - $.10, 90 - $.01 or<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; 80 - $.01, 12 - $.10, 4 - $.25, 4 - $.50 or<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; 75 - $.01, 15 - $.10, 9 - $.25, 1 - $.50 or<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; 80 - $.01, 7 - $.10, 12 - $.25, 1 - $.50 or<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; 75 - $.01, 20 - $.10, 1 - $.25, 4 - $.50 or<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; 1 - $.50, 39 - $.10, 60 - $.01 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 4,154 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Both answered all questions correctly or Joe answered twice as many questions correctly
                                 as Jim did. 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Vacuum </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> James Monroe , George Washington </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Monaco, Vatican City </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The parrot was deaf. </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_23.html#top">TOP</a></p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_23.pcf">©</a>
      </div>
   </body>
</html>