<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_21.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #21</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>What relation are you to your sister-in-law’s husband’s grandfather’s wife?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Four men named Jim Black, Bob White, John Black, and Rob Roy once competed in a checker
                                 tournament. See if you can tell from the following clues in what order they finished?
                              </p>
                              
                              <ol type="a">
                                 
                                 <li>
                                    
                                    <p>The Blacks were the famous athletic brothers who had starred in college sports.</p>
                                    
                                 </li>
                                 
                                 <li>
                                    
                                    <p>Bob White played better than usual and beat Rob Roy.</p>
                                    
                                 </li>
                                 
                                 <li>
                                    
                                    <p>The man who finished third said to the winner, "I’m very glad to have met you. I have
                                       heard what a skillful player you are."
                                    </p>
                                    
                                 </li>
                                 
                                 <li>
                                    
                                    <p>The runner-up had had infantile paralysis when he was a child. As a consequence he
                                       had never married, but had lived a quiet life with his mother.
                                    </p>
                                    
                                 </li>
                                 
                                 <li>
                                    
                                    <p>Jim Black had caused a good deal of talk when he was an usher at Rob Roy’s wedding.
                                       He drank too much champagne and proposed to the bride’s mother.
                                    </p>
                                    
                                 </li>
                                 
                              </ol>
                              
                           </li>
                           
                           <li>
                              
                              <p>If it takes 6 minutes to saw a log into three pieces, how long will it take to saw
                                 the same log into 4 pieces?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Three men all staying together in a hotel room during the convention decided to pay
                                 their bill. The room cost $25. The 3 men each gave the clerk a $10 check. The clerk
                                 said he would send the change up to their room by the bell hop. The bell hop came
                                 up with 5 one dollar bills. Each man took one of the bills and the other $2 were given
                                 to the bell hop for a tip. 
                              </p>
                              
                              <p>Since each man paid out $10 and got back $1, it means that each man spent $9. Being
                                 that all three paid the same amount, this means that the total for the three came
                                 to $27. Add to this the $2 tip the total now comes to $29. What happened to the other
                                 dollar?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>If a second of time is equal to a second of distance, how long will it take to go
                                 around the earth?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Inspector Slewfoot had narrowed the suspects in the Great Haystack Heist down to
                                 four – Skinny, Slinky, Sloppy, and Hubert. The four each made a statement. Slewfoot
                                 knew that only one of the statements was true----------WHO was the thief?<br>
                                 Skinny said, "I didn’t do it."<br>
                                 Slinky said, "Skinny is lying."<br>
                                 Sloppy said, "Slinky is lying."<br>
                                 Hubert said, "Slinky did it."
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Tom, Dick, and Harry are three friends. Their last names are Smith, Jones, and Robinson,
                                 but not necessarily respectively. They all go shopping one Saturday and Tom spends
                                 exactly twice as much as Dick and Dick spends exactly twice as much as Harry. Smith
                                 spends exactly $12.65 more than Robinson. What is each man’s full name?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>"We eat what we can, and we can what we can’t." Can you explain who could make this
                                 statement?
                              </p>
                              
                              <p><span>Answers are available with Stumper <strong>Set #22.</strong></span></p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_21.html#top">TOP</a></p>
                        
                        <p><u>Answers to Stumper Set #20</u></p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>"I will marry the ugliest woman in the tribe." </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>William is Walter’s grandfather. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>4-gallon bucket/7-gallon bucket<br>
                                 &nbsp;&nbsp;&nbsp; Fill up the 4-gallon, Pour 4-gallon into the 7-gallon, Fill up the 4-gallon, Fill
                                 up the 7-gallon from the 4-gallon leaving 1 gallon, Dump out the 7-gallon bucket,
                                 Pour the 1 gallon into the 7-gallon, Fill up the 4-gallon, Pour the 4-gallon into
                                 the 7-gallon giving you 5 gallons. 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Top card is the king of diamonds.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Second card is the 10 of spades.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Third card is the ace of hearts.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fourth card is the queen of hearts.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fifth card is the jack of clubs. 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>15, 17, and 19 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>20,349 meetings lasting 390 years. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> $18 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>In 1582 – the day after October 4th became October 15th in order to bring the calendar
                                 and the sun into correspondence again. 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 61 ½ mph </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_21.html#top">TOP</a></p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_21.pcf">©</a>
      </div>
   </body>
</html>