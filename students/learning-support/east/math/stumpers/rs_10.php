<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #10</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>An automobile is now half as old as John was when the car was new. John is now 15
                                 years old. How old is the car?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>A man was given $20 to buy $20 tickets for a football game. The man had to spend all
                                 the money and had to buy at least one ticket of each type. Adult tickets cost $4,
                                 student tickets cost $.50, and tickets for children under 6 years old cost $.25. How
                                 many of each type ticket did he come home with?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>A freight train one-mile long goes through a tunnel that is one mile long. If the
                                 train is traveling at a speed of 15 mph, how long does it take the train to pass through
                                 the tunnel?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A commuter going home from work, normally reaches his station at a certain time and
                                 is driven to his house by a chauffeur. On a certain occasion he arrived at the station
                                 an hour earlier than usual, and started to walk home. The chauffeur, leaving the house
                                 at the usual time, picked up the man and drove him home arriving 10 minutes earlier
                                 than usual. For how many minutes did the man walk?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A farmer, short on means and long on imagination, planted an apple orchard of only
                                 10 trees. Just the same, his orchard has five rows with four trees in each row. How
                                 did he do it?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>If&nbsp; ¼&nbsp; of 20 is 4 (I know it really isn’t), than what will 1/3 of 10 be?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>What whole number added to 1000 is more than if 1000 is multiplied by that number?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Which is the largest lake completely within the United States?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Friday the 13th is reputed to be a very unlucky day. On the average, over a period
                                 of 10 years, how frequently does Friday the 13th occur?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>How many times do the two hands of a clock point in the same direction between 6:00
                                 AM and 6:00 PM of a single day?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A man is put into a room with 2 doors. In this room are 2 people. One of these persons
                                 lies all the time; the other always tells the truth. The man who is put into the room
                                 does not know which of the 2 people is the liar and which one tells the truth Behind
                                 one of the doors is a way to freedom. Behind the other door is sure death. The liar
                                 and the truthful man each know what is behind the 2 doors. The man who is put in the
                                 room is allowed to ask either, but not both, of the people in the room one question
                                 to determine the way to freedom. What question should he ask to get free?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>If 6 people can eat 6 bananas in 6 minutes, how many people will it take to eat 100
                                 bananas in 100 minutes? (Note: stomach capacity is not an issue!) 
                              </p>
                              
                              <p>Answers are available with Stumper <strong>Set #11.</strong></p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_10.html#top">TOP</a></p>
                        
                        <p><u>Answers to Stumper Set #9</u></p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>White (due to deductive reasoning) </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>$96. </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>3 – 5 – 8<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 – 0 – 8&nbsp;&nbsp;&nbsp; The 8-quart is filled up from the store, the others are empty.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 – 0 – 5&nbsp;&nbsp;&nbsp; Fill the 3-quart up from the 8-quart.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 – 3 – 5&nbsp;&nbsp;&nbsp; Pour the 3 quarts into the 5-quart container. <br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 – 3 – 2&nbsp;&nbsp;&nbsp; Fill the 3-quart up from the 8-quart.<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 – 5 – 2<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 – 0 – 7<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 – 1 – 4<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 – 4 – 4 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>18 years old </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>100 silver dollars </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Hitler </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Has to be closed to Aardvosk </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>$7560. </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_10.html#top">TOP</a> 
                        </p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_10.pcf">©</a>
      </div>
   </body>
</html>