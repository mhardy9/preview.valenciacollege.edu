<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_20.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #20</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>A famous explorer was captured by savages. He was then tied to a tree while the kettle
                                 was being readied. After a while, the chief of the tribe came over to him and said,
                                 "Sir, we have a courtesy here which we practice with all visitors. We will allow you
                                 to make one statement before you are sentenced. If that statement is false, you must
                                 marry the ugliest woman in the tribe and stay with her. If the statement is true,
                                 you will be immediately made into stew." After thinking a while, the explorer made
                                 one statement and the tribe had no choice but to let him go. What did he say?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>What relation is William, who had no brothers or sisters, to Walter, if Walter’s grandfather
                                 is William’s father’s son?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A man, who has only a 7-gallon bucket and a 4-gallon bucket (and no other container),
                                 wants to measure out 5 gallons of water. How does he do this? (Note: do not measure
                                 by tilting.)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> There are 5 different cards in a pile each upside down. Using the following information
                                 can you figure out what order the cards are in ? (Suit and number are required.)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>The ace is nearer the top than either the jack of queen. The diamond is nearer the
                                 top than either the space or the club. The ten is neither a heart nor a club and is
                                 just above the ace. The jack is neither a heart nor a spade and is just below the
                                 queen. The king is neither a heart nor a spade and is higher than the ace. And one
                                 heart is just above another heart.&nbsp; From top to bottom what is the order of the cards?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Name three consecutive odd integers that equal 51?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Twenty-one bridge enthusiasts once decided that they would meet each week, 5 at a
                                 time, to play bridge. This arrangement was to continue just so long as they could
                                 meet without having the same group together on any two evenings. The question is----how
                                 many meetings could they have under the conditions laid down ? And how long would
                                 they last?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> John bought 2 pair of slacks and a sports jacket for $34. If one pair of slacks cost
                                 $2 less than the other pair, and the sports jacket costs twice the price of the more
                                 expensive slacks, how much did the sports jacket cost John?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>WHEN and WHY were 10 or 11 days completely skipped from the calendar?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A man on a train is moving at the train’s speed of 60 mph. He then walks toward the
                                 engine at a speed of 2 mph. While he is walking, an ant walks across the man’s head
                                 from front to back at a speed of ½ mph. How fast is the ant moving in the direction
                                 the train is going in relation to the earth?
                              </p>
                              
                              <p><span>Answers are available with Stumper <strong>Set #21.</strong></span></p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_20.html#top">TOP</a></p>
                        
                        <p><u>Answers to Stumper Set #19</u></p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>59 minutes </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Moonlight</p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 5 couples</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>½ hour</p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 600</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>N</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Machinist</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>None</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Mother</p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_20.html#top">TOP</a>
                           
                           
                        </p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_20.pcf">©</a>
      </div>
   </body>
</html>