<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_04.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #4</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>On an airplane, Smith, Robinson, and Jones are the pilot, co-pilot, and navigator,
                                 but not respectively. Also aboard the plane are three businessmen who have the same
                                 names: Smith, Robinson, and Jones.
                              </p>
                              
                              <p>Mr. Robinson lives in Detroit.</p>
                              
                              <p>The co-pilot lives exactly halfway between Chicago and Detroit.</p>
                              
                              <p>Mr. Jones earns exactly $20,000 per year.</p>
                              
                              <p>The co-pilot’s nearest neighbor, one of the passengers, earns exactly three times
                                 as much as the co-pilot.
                              </p>
                              
                              <p>Mr. Smith beats the pilot at billiards.</p>
                              
                              <p>The passenger, whose name is the same as the co-pilot’s, lives in Chicago.</p>
                              
                              <p>Using the above information, find out who the navigator is.</p>
                              
                              
                           </li>
                           
                           <li>
                              
                              <p> A man has a basket of apples. He gave one of his friends half of the apples plus
                                 half an apple. He gave a second friend half of his remaining apples plus half an apple.
                                 He gave a third half the apples then remaining plus half an apple. He was left with
                                 just one apple which he ate himself. He did NOT divide any apples. How many apples
                                 did he have originally?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> The Durry family is made up of 1 grandmother, 1 grandfather, 2 mothers, 2 fathers,
                                 5 children, 3 grandchildren, 1 brother, 2 sisters, 2 sons, 3 daughters, 1 mother-in-law,
                                 1 father-in-law, and 1 daughter-in-law. What are the least amount of people that could
                                 be in the Durry family?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> Suppose an aircraft carrier is moving along in the water and a man is broad jumping
                                 on the deck. Can he jump farther if he jumps in the direction the boat is moving or
                                 in the opposite direction ? (Note: the wind has no effect upon this problem.) EXPLAIN!
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>A chicken weighs 12 pounds plus a fourth (1/4) of its own weight. How many pounds
                                 does the chicken weigh?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>What common word contains one vowel six times and no other vowels? (Note: One ‘y’
                                 is used.)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> How many one-inch cubes will it take to make a cubic foot?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p>In the following multiplication problem only ten digits are used and each one is different.
                                 Each ‘x’ stands for a different digit. Can you figure out what the original problem
                                 was?
                              </p>
                              
                              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; xxx<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; xx<br>
                                 &nbsp;&nbsp; ----------<br>
                                 &nbsp;&nbsp;&nbsp;&nbsp; xxxx1
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>"Can you tell me what the temperature has been at noon for the past five days?" John
                                 asked the weatherman.
                              </p>
                              
                              <p>"I can’t recall exactly," replied the weatherman, "but I do remember that the temperature
                                 was different each day, and that the product of the temperatures was 12."
                              </p>
                              
                              <p>Assuming that the temperatures are expressed to the nearest degree, what were the
                                 five temperatures?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Find the next number in this sequence: 1, 2, 3, 5, 8, ____.</p>
                              
                              
                              <p>Answers are available with Stumper <strong>Set #5.</strong></p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_04.html#top">TOP</a></p>
                        
                        <p>Answers to Stumper Set #3</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>300 miles </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Use a magnet to separate the iron filings, dissolve the salt in water and only the
                                 sand will remain.&nbsp;&nbsp; (When the water evaporates, the salt will remain.) 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>64 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> (4 – 8) then (2 – 2 and 4 – 4) then (2 – 2 and 2 – 2) <u><strong>or</strong></u> (3 – 3) then (1 – 1 and (1 – 1) then (1 – 1) <strong><u>or</u></strong> (6 – 6) then (3 – 3) then (1 – 1). 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>4 birds </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>deny </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>10 combinations of stamps </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>121 </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p> <a href="../../../academic-success/math/stumpers/rs_04.html#top">TOP</a></p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_04.pcf">©</a>
      </div>
   </body>
</html>