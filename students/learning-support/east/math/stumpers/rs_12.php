<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/stumpers/rs_12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/math/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/stumpers/">Stumpers</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Richard's Stumpers</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/index.html">#1</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_02.html">#2</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_03.html">#3</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_04.html">#4</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_05.html">#5</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_06.html">#6</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_07.html">#7</a></div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <a href="../../../academic-success/math/stumpers/rs_08.html">#8</a> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_09.html">#9</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_10.html">#10</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_11.html">#11</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_12.html">#12</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_13.html">#13</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_14.html">#14</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_15.html">#15</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_16.html">#16</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_17.html">#17</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_18.html">#18</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_19.html">#19</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_20.html">#20</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_21.html">#21</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_22.html">#22</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_23.html">#23</a></div>
                                    </div>
                                    
                                    <div>
                                       <div><a href="../../../academic-success/math/stumpers/rs_24.html">#24</a></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Set #12</p>
                        
                        <ol>
                           
                           <li>
                              
                              <p>Nine men play on a baseball team (Brown, White, Black, Miller, Green, Gray, Wilson,
                                 Jones, and Smith). See if you can figure out the position of each man using the following
                                 information:
                              </p>
                              
                              <ul>
                                 
                                 <li>Smith and Brown each won $10 playing poker with the pitcher.</li>
                                 
                                 <li>Gray is taller than Wilson and shorter than White, but each of them weighs more than
                                    the first baseman.
                                 </li>
                                 
                                 <li>The third baseman lives in the same apartment as Jones.</li>
                                 
                                 <li>Miller and the outfielders play poker in their spare time.</li>
                                 
                                 <li>White, Miller, Brown, the right fielder, and the center fielder are bachelors. The
                                    rest are married.
                                 </li>
                                 
                                 <li>Wilson and Black: one of them plays an outfield position.</li>
                                 
                                 <li>The right fielder is shorter than the center fielder.</li>
                                 
                                 <li>The third baseman is a brother to the pitcher’s wife.</li>
                                 
                                 <li>Green is taller than the infielders and battery (pitcher and catcher), except Jones,
                                    Smith, and Black.
                                 </li>
                                 
                                 <li>The second baseman beat Jones, Brown, Gray, and the catcher at golf.</li>
                                 
                                 <li>The third baseman, the shortstop, and Gray made $150 apiece speculating in the stock
                                    market.
                                 </li>
                                 
                                 <li>The second baseman is engaged to Miller’s sister.</li>
                                 
                                 <li>Black, who hates the catcher, lives in the same house with his wife.</li>
                                 
                                 <li>Brown, Black, and the shortstop lost $200 apiece speculating in overseas commodities.</li>
                                 
                                 <li>The catcher has three daughters, the third baseman has two sons, and Green is being
                                    sued for divorce.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <blockquote>
                              
                              
                           </blockquote>
                           
                           <li>Coylis Coy never tells anyone her telephone number directly. If a new suitor should
                              ask, she gives the following response: "The sum of my telephone number’s four digits
                              is my age, 23. The first digit is 3 times the third and the second is 2 less than
                              the fourth which, if you are still interested, is one more than twice the third."
                              
                              <p>Could you call her? What is her number?</p>
                              
                           </li>
                           
                        </ol>
                        
                        <ol>
                           
                           <li>
                              
                              <p>A restaurant, famous for its hot rolls, doesn’t charge for them if a diner doesn’t
                                 eat too many. There is a charge though for each roll eaten above the magic number.
                                 A husband and wife who had a passion for the rolls together consumed 13 rolls and
                                 were charged $.60. Had one person alone had eaten that many, the charge would have
                                 been $1.60. How many rolls can one person eat without being charged?
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>SIONFRFOLPEOITR With these letters can you form the longest sentence known to man"
                                 ? (Use only the letters shown.)
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>"We need more boys in the club." said Maryanne.</p>
                              
                              <p>"Why?" asked Patty.</p>
                              
                              <p>"Well, we have 32 members now and only ¼ are boys."</p>
                              
                              <p>"That’s not good!" Patty admitted.</p>
                              
                              <p>"You’re right," said Maryanne. "We have to get some more boys to join. At least one
                                 third of the members must be boys."
                              </p>
                              
                              <p>"That would be a more balanced club." Patty agreed.</p>
                              
                              <p>How many more boys would they need if no more girls joined their club?</p>
                              
                           </li>
                           
                           <li>
                              
                              <p> A frog is at the bottom of a well 30 feet deep. How many days will it take him to
                                 get out if he climbs up three feet each day and drops back two feet each night?
                              </p>
                              
                              <p>Answers are available with Stumper <strong>Set #13.</strong></p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_12.html#top">TOP</a></p>
                        
                        <p><u>Answers to Stumper Set #11</u></p>
                        
                        <ol>
                           
                           <li>
                              
                              <p> 19 male passengers </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>$3.00 </p>
                              
                           </li>
                           
                           <li>
                              
                              <p> 5 5/11 minutes </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Majority of votes in electoral college </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>4 miles </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>11:57 AM </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>11 pieces&nbsp; (To achieve this number make sure that 3 or more of the lines do NOT intersect
                                 at the same point in your circle. By the way these numbers do have a pattern.&nbsp; So
                                 although it is difficult to draw, you can calculated the number of pieces with more
                                 cuts.) 
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>61st day </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Yes, because in order to arrive at a "desirable" statistic the statistic was probably
                                 calculated to include the 5th president who was, more than likely, the 3rd president
                                 to have died on July 4th. 
                              </p>
                              
                           </li>
                           
                        </ol>
                        
                        <p><a href="../../../academic-success/math/stumpers/rs_12.html#top">TOP</a></p>
                        
                        <p>
                           
                           <a href="../../../academic-success/math/stumpers/index.html">Set #1</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_02.html">Set #2</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_03.html">Set #3</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_04.html">Set #4</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_05.html">Set #5</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_06.html">Set #6</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_07.html">Set #7</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_08.html">Set #8</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_09.html">Set #9</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_10.html">Set #10</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_11.html">Set #11</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_12.html">Set #12</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_13.html">Set #13</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_14.html">Set #14</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_15.html">Set #15</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_16.html">Set #16</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_17.html">Set #17</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_18.html">Set #18</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_19.html">Set #19</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_20.html">Set #20</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_21.html">Set #21</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_22.html">Set #22</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_23.html">Set #23</a>&nbsp;&nbsp;&nbsp; <a href="../../../academic-success/math/stumpers/rs_24.html">Set #24</a>
                           
                           
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/stumpers/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/stumpers/rs_12.pcf">©</a>
      </div>
   </body>
</html>