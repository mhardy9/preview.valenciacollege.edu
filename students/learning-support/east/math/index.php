<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Math</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">            
                        <p>
                           <a name="content" id="content"></a>
                           <a href="../../academic-success/math/index.html"></a></p>
                        
                        
                        
                        <h2>Assisting you with your math class is what we do. Come see us!</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>The Math Center provides assistance to all Valencia students enrolled in any math
                                                      course. 
                                                   </p>
                                                   
                                                </div>
                                                
                                                <p><strong>Services include: </strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Walk-in tutoring</li>
                                                   
                                                   <li>Group Study program - 4 or more students can sign up in the Math Center (4-102) to
                                                      work with a dedicated tutor 
                                                   </li>
                                                   
                                                   <li>Homework assistance</li>
                                                   
                                                   <li>Final Exam Review</li>
                                                   
                                                   <li>Computer programs</li>
                                                   
                                                   <li>PERT information </li>
                                                   
                                                   <ul>
                                                      
                                                      <li><a href="../../academic-success/math/documents/PERTmathbooklet.pdf">PERT review booklet link (new student)</a></li>
                                                      
                                                      <li><a href="../../academic-success/math/documents/CPT-Ibooklet7-09.pdf">CPT-I review booklet link (college level)</a></li>
                                                      
                                                   </ul>
                                                   
                                                </ul>
                                                
                                                
                                                <ul>
                                                   
                                                   <li>MATH PATH Information
                                                      
                                                      
                                                   </li>
                                                   
                                                </ul>                          
                                                
                                                <p><strong>Academic Refreshers</strong>
                                                   
                                                </p>
                                                
                                                <blockquote>
                                                   
                                                   <p>Ensure you are ready for college level classes by taking the Math Academic Refreshers,
                                                      an online series of workshops that cover the fundamental skills needed for success.
                                                      
                                                   </p>
                                                   
                                                </blockquote>
                                                
                                                <ul>
                                                   
                                                   <ul>
                                                      
                                                      <li>
                                                         <a href="../../academic-success/math/documents/AccesstoMAT1033CAR.RMSpring17.pdf"> Click here for instructions on how to register for 1033C Math Academic Refresher
                                                            (With Pre/Post Tests)</a> - Spring 2017 
                                                      </li>
                                                      
                                                      <li> <a href="../../academic-success/math/documents/AccesstoMAT1033CAR.RMSpring17-NoPre.PostTesting.pdf">Click here for instructions on how to register for 1033C Math Academic Refresher (With
                                                            NO Pre/Post Tests)  </a>- Spring 2017
                                                      </li>
                                                      
                                                      <li> <a href="../../academic-success/math/documents/AccesstoMAT0028CARSpring17.pdf">Click here for instructions on how to register for 28C Math Academic Refresher (With
                                                            NO Pre/Post Tests)</a> - Spring 2017 
                                                      </li>
                                                      
                                                   </ul>
                                                   
                                                   
                                                </ul>                          
                                                
                                                <p><strong>Calculator Workshops
                                                      </strong>                              
                                                   / Group-Work  program 
                                                </p>
                                                
                                                <ul>
                                                   
                                                   <ul>
                                                      
                                                      <li>
                                                         <a href="../../academic-success/math/documents/Fall2016CalculatorWorkshops.pdf">Calculator workshops for fall term</a> 
                                                      </li>
                                                      
                                                      <li>Group-Study program
                                                         
                                                         <ul>
                                                            
                                                            <li>4 or more students working on same material</li>
                                                            
                                                            <li>Sign up at the Math Center (4-102)</li>
                                                            
                                                            <li>1 hour session with a dedicated tutor </li>
                                                            
                                                         </ul>
                                                         
                                                      </li>
                                                      
                                                   </ul>
                                                   
                                                   <li>
                                                      <a href="../../academic-success/math/BarschLearningStyles.html">Barsch Learning Style Inventory</a> (downloadable <a href="http://valenciacollege.edu/east/academicsuccess/math/documents/BarschInventory.docx">Word document</a>) 
                                                   </li>
                                                   
                                                </ul>                          
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <div>For tutoring assistance when the Math Center is closed, please utilize our online
                                                               tutoring partner, <strong>SmarThinking</strong> via your Atlas Account.
                                                            </div>
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>                  
                                    
                                    
                                    <h2>Help! I'm in the Wrong Math Class!</h2>
                                    
                                    <h3> My Class is Too <strong> EASY</strong>! 
                                    </h3>
                                    
                                    <p>Do not base your opinion on the first chapter (review work). Look at the whole book.</p>
                                    
                                    <p>Options for retaking the CPT (if you have only taken it once within the past 2 years):</p>
                                    
                                    <p><span>Before the term begins:</span> 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>See Mr. Weinsier (4-102) in the Math Center (bottom floor of building 4) </li>
                                       
                                    </ul>
                                    
                                    <p><span>After the term begins:</span> 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li> Prove to instructor (tests, attendance, class work, homework, etc.) that this course
                                          is too easy.
                                       </li>
                                       
                                       <li>A few weeks before the end of the term have instructor inform Mr. Weinsier (4-102)
                                          of your ability. <br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <h3>Class is Too <strong>HARD</strong>!
                                    </h3>
                                    
                                    <p>Get extra help at the Math Center (4-102).</p>
                                    
                                    <p>See Dr. Lee (7-142) about the possibility of changing to a lower level class.</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../academic-success/math/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/index.pcf">©</a>
      </div>
   </body>
</html>