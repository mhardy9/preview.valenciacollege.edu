<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Instructor Resources | East Mathematics Lab | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/lab/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Mathematics Lab</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/lab/">Mathematics Lab East</a></li>
               <li>Instructor Resources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Instructors' Resources</h2>
                        
                        
                        <h3>Resources Available</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Laptops</li>
                           
                           <li>Windows Tablets</li>
                           
                           <li>TI-84 Calculators </li>
                           
                           <li>Lab manuals</li>
                           
                           <li>Lab supplies - <a href="documents/Lab-Supplies.xlsx">Inventory</a>
                              
                           </li>
                           
                           <li>Projectors</li>
                           
                           <li>2 TI Navigator Carts</li>
                           
                           <li><a href="documents/Lab-Procedure.pptx">Lab Procedures</a></li>
                           
                           <li><strong><a href="http://bit.ly/eastmathlabs" target="_blank">Lab Manuals</a></strong></li>
                           
                        </ul>
                        
                        
                        <h3>Instructors' FAQ:</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <strong>Where is my lab located?</strong>
                              
                              <p>On your faculty schedule, it should state that your lab is in 7-112 A,B,C, or D. The
                                 lab spaces are color coded. To view a diagram of the lab room, please <a href="index.php">see the front page</a>.
                              </p>
                              
                           </li>
                           
                           <li>
                              <strong>Where can my students pick up their lab manuals?</strong>
                              
                              <p>Students will be provided lab manuals for MAT0018, MAT0022/0028C, MAT1033C free of
                                 charge, during their first lab meeting in 7-112.
                              </p>
                              
                           </li>
                           
                           <li>
                              <strong>Where can I pick up my copy?</strong>
                              
                              <p>Instructors may also pick up a copy of the MAT0018C, MAT0022C/0028C, and MAT1033C
                                 lab manuals in 7-112. In addition, Instructor's solution manuals will be emailed out
                                 at the beginning of the semester.
                              </p>
                              
                           </li>
                           
                           <li>
                              <strong>Who is my lab assistant?</strong>
                              
                              <p>At the beginning of the semester, you will be provided the contact information of
                                 your lab assistant, and they will be provided yours. Additionally, you may look up
                                 their email address on the <a href="assistants.php">Lab Assistants</a> page.
                              </p>
                              
                           </li>
                           
                           <li>
                              <strong>If I want to reserve laptops/tablets for my class to use during lab, how do I go about
                                 doing that?</strong>
                              
                              <p>Please use this link <a href="http://10.10.64.147/booking" target="_blank">http://10.10.64.147/booking</a> to reserve laptops or tablets. (Note: this site is only accessible from a computer
                                 on the Valencia College network.) If you have any question, you may send us an email:
                                 <a href="mailto:nsidibaba@valenciacollege.edu">Nadia Sidibaba</a> and <a href="mailto:Sstull@valenciacollege.edu">Stephen Stull</a> or call us at : 407-582-2808. 
                              </p>
                              
                              <p><a href="documents/booking-instructions.pdf" target="_blank">Booking Instructions</a></p>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/lab/resources.pcf">©</a>
      </div>
   </body>
</html>