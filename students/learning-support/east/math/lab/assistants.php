<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lab Assistants | East Mathematics Lab | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/lab/assistants.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Mathematics Lab</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/lab/">Mathematics Lab East</a></li>
               <li>Lab Assistants</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Lab Assistants</h2>
                        
                        <p><a href="http://form.jotform.us/form/50085704005142" target="_blank">(availability form) </a></p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Name</th>
                                 
                                 <th scope="col">Email</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Abraham, Yurat</td>
                                 
                                 <td><a href="mailto:yabraham@valenciacollege.edu">yabraham@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Amram, Esther</td>
                                 
                                 <td><a href="mailto:eamram@valenciacollege.edu">eamram@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Bliss, Jennifer</td>
                                 
                                 <td><a href="mailto:jbliss2@valenciacollege.edu">jbliss2@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Cook, Shannon</td>
                                 
                                 <td><a href="mailto:scook10@valenciacollege.edu">scook10@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Cummings, Rodney</td>
                                 
                                 <td><a href="mailto:rcummings@valenciacollege.edu">rcummings@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Diaz, Edwin</td>
                                 
                                 <td><a href="mailto:ediaz65@valenciacollege.edu">ediaz65@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Echevarria, Erika</td>
                                 
                                 <td><a href="mailto:eechevarria3@valenciacollege.edu">eechevarria3@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Garcia, Matthew</td>
                                 
                                 <td><a href="mailto:mgarcia101@valenciacollege.edu">mgarcia101@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Jackman, Sarah</td>
                                 
                                 <td><a href="mailto:sjackman2@valenciacollege.edu">sjackman2@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Keene, Benjamin</td>
                                 
                                 <td><a href="mailto:bkeene3@valenciacollege.edu">bkeene3@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Lester, Lonnie</td>
                                 
                                 <td><a href="mailto:llester2@valenciacollege.edu">llester2@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Lopez, Francisco</td>
                                 
                                 <td><a href="mailto:flopezcruz@mail.valenciacollege.edu">flopezcruz@mail.valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>McClelland, Mario</td>
                                 
                                 <td><a href="mailto:mmcclelland1@valenciacollege.edu">mmcclelland1@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mason, Eric</td>
                                 
                                 <td><a href="mailto:emason10@valenciacollege.edu">emason10@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Pierre, Amelia</td>
                                 
                                 <td><a href="mailto:apierre2@valenciacollege.edu">apierre2@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Plummer, Catherine</td>
                                 
                                 <td><a href="mailto:cplummer5@valenciacollege.edu">cplummer5@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Richmann, Adrian</td>
                                 
                                 <td><a href="mailto:arichmann@valenciacollege.edu">arichmann@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Ruff, Logan</td>
                                 
                                 <td><a href="mailto:lruff2@valenciacollege.edu">lruff2@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Sichani, Mahdi Sabouri</td>
                                 
                                 <td><a href="mailto:msabourisichani@valenciacollege.edu">msabourisichani@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Stamper, Travis</td>
                                 
                                 <td><a href="mailto:tstamper@valenciacollege.edu">tstamper@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Ward, Eric</td>
                                 
                                 <td><a href="mailto:eward9@valenciacollege.edu">eward9@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Whitbread, Natasha</td>
                                 
                                 <td><a href="mailto:nwhitbread@valenciacollege.edu">nwhitbread@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Williams, Marie</td>
                                 
                                 <td><a href="mailto:mwillia5@valenciacollege.edu">mwillia5@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Wingert, Timothy</td>
                                 
                                 <td><a href="mailto:twingert@valenciacollege.edu">twingert@valenciacollege.edu</a></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/lab/assistants.pcf">©</a>
      </div>
   </body>
</html>