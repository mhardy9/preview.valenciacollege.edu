<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hybrid Computer Lab (Emporium) | East Mathematics Lab | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/lab/emporium.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Mathematics Lab</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/lab/">Mathematics Lab East</a></li>
               <li>Hybrid Computer Lab (Emporium) </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Hybrid Computer Lab (Emporium)</h2>
                        
                        <p>The Hybrid Computer Lab (Emporium), located in building 1, room 377, is designed for
                           students taking developmental math hybrid courses. These students are required, as
                           part of the course, to spend at least one hour a week, working on homework and other
                           class assignments. The Hybrid Computer Lab consists of 25 laptop stations where students
                           can work. In addition, there are tutors available in the area to assist.
                           
                        </p>
                        
                        
                        <h3>Operation Hours: Fall/Spring</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Monday - Thursday: 9:00 am - 5:00 pm</li>
                           
                           <li>Friday: 9:00 am - 3:00 pm </li>
                           
                        </ul>
                        
                        
                        
                        <h3>Operation Hours: Summer</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Monday, Wednesday, Thursday: 8:30 am - 5:00 pm </li>
                           
                           <li>Tuesday: 8:30 am - 7:00pm </li>
                           
                        </ul>
                        
                        
                        <h3>General Information</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>Location: Building 1, Room 377</li>
                           
                           <li>Phone: 407-582-8926</li>
                           
                        </ul>
                        
                        
                        <h3>Hybrid Computer Lab FAQ:</h3>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <strong>Who is required to attend the Hybrid Computer Lab?</strong>
                              
                              <p>Student registered in MAT0022C and MAT1033C Hybrid courses are required to spend at
                                 least one hour a week in the emporium.
                              </p>
                              
                           </li>
                           
                           <li>
                              <strong>What do I need to bring to the Hybrid Computer Lab?</strong>
                              
                              <p>Along with their class materials, students are required to bring their Valencia College
                                 ID. The ID is used to sign students in and out. The purpose of this is to help instructors
                                 keep track on how much time students spend in the Hybrid Computer Lab.
                              </p>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/lab/emporium.pcf">©</a>
      </div>
   </body>
</html>