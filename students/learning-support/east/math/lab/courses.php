<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Courses | East Mathematics Lab | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/lab/courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Mathematics Lab</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li><a href="/students/learning-support/east/math/lab/">Mathematics Lab East</a></li>
               <li>Courses </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Courses</h2>
                        
                        
                        <h3>Developmental Math I - MAT0018C</h3>
                        
                        <p> This is the first course in a college-preparatory, two-course sequence (<a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT 0018C</a> and <a href="http://catalog.valenciacollege.edu/search/?P=MAT%200028C">MAT 0028C</a>) designed to prepare students for <a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT 1033C</a>, Intermediate Algebra. This course emphasizes the fundamental mathematical operations
                           with applications to beginning algebra. Significant time will be devoted to connections
                           between mathematics and other academic disciplines and to applications outside educational
                           settings. Minimum grade of C required for successful completion. This course does
                           not apply towards mathematics requirements in general education or towards any associate
                           degree. (Special Fee: $42.00). 
                        </p>
                        
                        
                        <h3>Developmental Math Combined - MAT0022C</h3>
                        
                        <p><strong>Prerequisite: </strong>Appropriate score on an approved assessment
                        </p>
                        
                        <p> This college-preparatory course is designed to prepare students for <a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT 1033C</a> Intermediate Algebra. Topics include sets, computations with decimals, percent, integer,
                           operations with rational and polynomial expressions, solving linear equations and
                           simplifying expressions, plan geometric figures and applications, graphic ordered
                           pairs and lines and determining the intercepts of lines. A passing score on the Basic
                           Skills Exit Test is required for a minimum final course grade of C, which is required
                           for successful completion of this course. This course does not apply towards mathematics
                           requirements in general education or towards any associate degree. (Special Fee: $42.00).
                           
                        </p>
                        
                        
                        <h3>Developmental Math II - MAT0028C</h3>
                        
                        <p><strong>Prerequisite: </strong>Minimum grade of C in MAT 0018C or appropriate score on an approved assessment
                        </p>
                        
                        <p> This college-preparatory course is designed to supplement the algebraic background
                           of students prior to taking <a href="http://catalog.valenciacollege.edu/search/?P=MAT%201033C">MAT 1033C</a> Intermediate Algebra. Topics include sets, fundamental operations with polynomials,
                           linear equations and inequalities with applications, factoring and its use in algebra,
                           introduction to graphing of linear equations, introduction to radicals, and use of
                           calculators to enhance certain concepts. A passing score on the basic Skills Exit
                           Test is required for a minimum final course grade of C, which is required for successful
                           completion of this course. This course does not apply towards mathematics requirements
                           in general education or towards any associate degree. (Special Fee: $42.00). 
                        </p>
                        
                        
                        <h3>Intermediate Algebra - MAT1033C</h3>
                        
                        <p><strong>Prerequisite: </strong>Minimum grade of C in MAT 0022C or MAT 0028C or appropriate score on an approved assessment
                        </p>
                        
                        <p> This course presents algebraic skills for <a href="http://catalog.valenciacollege.edu/search/?P=MAC%201105">MAC 1105</a>. Topics include linear equations and inequalities in two variables and their graphs,
                           systems of linear equations and inequalities, introductions to functions, factoring,
                           algebraic functions, rational equations, radical and rational exponents, complex numbers,
                           quadratic equations, scientific notation, applications of the above topics and the
                           communication of mathematics. Applications emphasizing connections with disciplines
                           and the real world will be included. This course carries general elective credit but
                           does not satisfy either Gordon Rule or general education requirements. (Special Fee:
                           $42.00). 
                        </p>
                        
                        
                        <h3>Introduction to Statistical Reasoning- STA1001C</h3>
                        
                        <p><strong>Prerequisite: </strong> Min. grade of C in <a href="http://catalog.valenciacollege.edu/search/?P=MAT%200018C">MAT 0018C</a> or higher or appropriate score on an approved assessment.
                        </p>
                        
                        <p>This course provides students with an opportunity to acquire a reasonable level of
                           statistical literacy and expand their understanding of statistical approaches to problem-solving.
                           The main objective of this course is the development of statistical reasoning techniques
                           and an introduction to the statistical analysis process. (Special Fee: $42.00).
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/lab/courses.pcf">©</a>
      </div>
   </body>
</html>