<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>East Mathematics Lab | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/lab/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Mathematics Lab</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Mathematics Lab East</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>About Us</h2>
                        
                        <p>The East Campus Mathematics Lab area is designed for students in the following classes:
                           <strong>MAT0018C, MAT0022C, MAT0028C, and MAT1033C</strong>, as a place for students to work on curriculum related material to help solidify
                           a concept learned in class. The goal of the Mathematics Lab is to establish a rapport
                           between the students and the lab instructor in order to encourage a positive learning
                           environment. A lab instructor's role is to answer students' questions, be an additional
                           resource, and help contribute to the success of the student at Valencia College.
                        </p>
                        
                        <!-- <h3>Useful Links</h3>
    <ul class="list_style_1">
        <li>
<a href="http://valenciacollege.edu/east/mathematics-lab/documents/LabRoom.png">Find my lab-room</a> </li>
        <li><a href="http://valenciacollege.edu/east/mathematics-lab/documents/atlas_schedule.png">How do I find my lab time in my ATLAS schedule?</a></li>
        <li><a href="emporium.html">Emporium</a></li>
        <li><a href="courses.html">Courses</a></li>
        <li><a href="assistants.html">Lab Assistants</a></li>
    </ul> -->
                        
                        <h3>Finding Your Lab Room</h3>
                        
                        <p>Lab spaces are color-coded. For instance, if your schedule says the lab takes place
                           in 7-112C, that means your class will be seated in the section with the orange chairs.
                           A diagram is provided; if you need assistance, please speak with one of the staff.
                        </p>
                        <img class="img-responsive" src="/students/mathematics-lab-east/images/labroomdiagram.png" alt="From the left-hand side of the lab entrace and continuing clockwise: 112A (yellow chairs), 112B (grey chairs), 112C (orange chairs), and 112D (green chairs).">
                        
                        <h3>Finding Your Lab Time in the ATLAS Schedule</h3>
                        
                        <p>On your class schedule in ATLAS, under the "Scheduled Meeting Times" for your Math
                           class, find the entry for "Scheduled Laboratory." Right next to that will be the lab
                           time and location.
                        </p>
                        
                        <div class="container margin-60">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="wrapper_indent">
                                    
                                    <h3>Operation Hours: Fall/Spring</h3>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Monday - Thursday: 8:00 am - 9:30 pm</li>
                                       
                                       <li>Friday: 8:00 am - 5:00 pm</li>
                                       
                                       <li>Saturday: 8:00 am - 11:00 am</li>
                                       
                                    </ul>
                                    
                                    <h3>Operation Hours: Summer</h3>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Monday - Thursday: 9:00 am - 8:00 pm</li>
                                       
                                       <li>Friday: Closed</li>
                                       
                                       <li>Saturday: 10:00 am - 12:00 pm</li>
                                       
                                    </ul>
                                    
                                 </div>
                              </div>
                              <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">
                                 
                                 <h3>General Information</h3>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Location: Building 7, Room 112</li>
                                    
                                    <li>Phone: 407-582-2808</li>
                                    
                                    <li>Mail Code: 3-16</li>
                                    
                                 </ul>
                                 
                                 <h3>Staff</h3>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li><a href="mailto:nsidibaba@valenciacollege.edu">Nadia Sidibaba</a>, Morning Supervisor
                                    </li>
                                    
                                    <li><a href="mailto:Sstull@valenciacollege.edu">Stephen Stull</a>, Night Supervisor
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                           </div>
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/lab/index.pcf">©</a>
      </div>
   </body>
</html>