<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/calculators.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/math/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Calculator Information</h2>
                        
                        <p> For all <em><strong>prep </strong></em>math classes, Pre-Algebra (MAT0012) or Beginning Algebra (MAT0024), students can use
                           any scientific calculator. For <em><strong>college level</strong> </em>math classes<em>, </em>Intermediate Algebra (MAT1033) and beyond, students will need a graphing calculator,
                           such as a Texas Instruments TI-83 or TI-83 Plus. Other calculators can be used, but
                           instructors discuss and instruct using the TI-83 or TI-83 Plus.
                        </p>
                        
                        <h3>User Guides </h3>
                        
                        <ul>
                           
                           <li><a href="https://education.ti.com/en/us/guidebook/details/en/ABF6D3DD944745A7A76609E97F84B1F7/83p">TI-83 Plus/TI-83 Plus Silver Edition</a></li>
                           
                           <li><a href="https://education.ti.com/en/us/guidebook/details/en/C4D11EB6D86B47D19CD768E54A967441/84p">TI-84 Plus/TI-84 Plus Silver Edition</a></li>
                           
                        </ul>
                        
                        <h3>TI-83 Plus or TI-84 Graphing Calculator Hints</h3>
                        
                        <p><em>NOTE: All highlighted text represents a calculator key!</em></p>
                        
                        <p><strong>General information:</strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Zoom</div>
                                 
                                 <div>6: ZStandard</div>
                                 
                                 <div>Two steps to make a graph go 10 units in every direction.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Left arrow</div>
                                 
                                 <div>Right arrow</div>
                                 
                                 <div>Used to move your cursor on the graphing screen.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Up arrow</div>
                                 
                                 <div>Down arrow</div>
                                 
                                 <div>Changes the equation that calculator is referring to.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>y =</div>
                                 
                                 
                                 <div>Allows you to put in an equation that is solved for 'y'.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>x,T,Ø,n</div>
                                 
                                 
                                 <div>Allows you to put the variable 'x' into an equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Trace</div>
                                 
                                 <div>Left or Right arrow changes info</div>
                                 
                                 <div>Finds mostly approximate decimal values for 'x' and 'y'.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Trace</div>
                                 
                                 <div>Pick 'x' value</div>
                                 
                                 <div>Gives matching 'y'.&nbsp; The 'x' value must be on graphing screen.&nbsp;</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Trace</div>
                                 
                                 <div>0 (zero) </div>
                                 
                                 <div>Gives you the y - intercept.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Window</div>
                                 
                                 <div>xmin&nbsp; &amp;&nbsp; xmax</div>
                                 
                                 <div>Sets lowest (left) and highest (right) value of the x-axis.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Window</div>
                                 
                                 <div>ymin&nbsp; &amp;&nbsp; ymax</div>
                                 
                                 <div>Sets lowest (bottom) and highest (top) value of the y-axis.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Graph</div>
                                 
                                 <div>Zoom</div>
                                 
                                 <div>0:Zoomfit </div>
                                 
                                 <div>Helps to find a graph that does not show on your screen.</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../academic-success/math/calculators.html#top">TOP</a></p>
                        
                        <p><strong>Parentheses</strong>: Put on all numerators and denominators that have 2 or more terms.<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Example:&nbsp;&nbsp; <img alt=" " height="41" src="../../academic-success/math/Image2.gif" width="46">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Put in calculator for graphing as:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<strong>5x + 7) / (2x – 3)</strong></p>
                        
                        
                        <p><strong>How to find an exact ‘x’ value on a graph:<br>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>You are given a ‘y’ value from your graph and asked to find the matching 'x' value.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p>y =</p>
                                 </div>
                                 
                                 <div>
                                    <p>y2 =</p>
                                 </div>
                                 
                                 <div>Enter given 'y' value into the equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>2nd</p>
                                 </div>
                                 
                                 <div>
                                    <p>Trace</p>
                                 </div>
                                 
                                 <div>
                                    <p>5: intersect</p>
                                 </div>
                                 
                                 <div>First curve?&nbsp;&nbsp;<span><strong>Enter</strong></span>&nbsp;&nbsp; Second curve?&nbsp;&nbsp;<span><strong>Enter</strong></span>&nbsp;&nbsp; Guess?&nbsp;&nbsp;<span><strong>Enter</strong></span><br>
                                    If there are 2 or more intersections, just before you guess use your left or right
                                    arrow to put cursor on the intersection that you want information on.
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Calculate</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../academic-success/math/calculators.html#top">TOP</a></p>
                        
                        <p><strong>Calculations:</strong>&nbsp;&nbsp;&nbsp;&nbsp; <span><strong>2nd&nbsp;&nbsp;&nbsp;&nbsp; Trace</strong></span></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>2: zero</div>
                                 
                                 <div>Finds the 'x' intercept.</div>
                                 
                                 <div>Left Bound?&nbsp;&nbsp;&nbsp;&nbsp; Place cursor to the left of point you are looking for on the graph.&nbsp;<strong>Enter</strong><br>
                                    Right Bound?&nbsp;&nbsp; Place cursor to the right of point you are looking for on the graph.
                                    <span><strong>Enter</strong></span><br>
                                    Guess?&nbsp;&nbsp;<span><strong>Enter</strong></span>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>3: minimum</div>
                                 
                                 <div>Finds vertex of parabola facing up.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>4: maximum</div>
                                 
                                 <div>Finds vertex of parabola facing down.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>5: intersection</div>
                                 
                                 <div>Finds intersection of 2 graphs.</div>
                                 
                                 <div>First curve?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;<span>Enter</span></strong>&nbsp;&nbsp;&nbsp;<br>
                                    Second curve?&nbsp;&nbsp;<span><strong>Enter</strong></span>&nbsp;&nbsp;&nbsp;<br>
                                    Guess?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><strong>Enter</strong></span><br>
                                    If there are 2 or more intersections, just before you guess use your left or right
                                    arrow to put cursor on the intersection that you want information on.
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../academic-success/math/calculators.html#top">TOP</a></p>
                        
                        <p><strong>How to find a line of Linear Regression:<br>
                              </strong>What you are finding is a line that best fits (goes as close as possible) to all of
                           the points that you scattered about your graph (scatterplot). 
                        </p>
                        
                        <h3>Entering Data:</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Stat</p>
                                 </div>
                                 
                                 
                                 <div>Section that will allow you to enter your scatter plot points.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>1: Edit</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 <div>Allows you to erase, change, or add point information.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>L1</p>
                                 </div>
                                 
                                 <div>
                                    <p>Clear</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 <div>Clears all L1 entries (Use arrows keys to put marker on L1). </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>L2</p>
                                 </div>
                                 
                                 <div>
                                    <p>Clear</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 <div>Clears all L2 entries (Use arrows keys to put marker on L2). </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>L1</p>
                                 </div>
                                 
                                 <div>
                                    <p>x-value</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 <div>Put marker on dotted line and enter all your x-values.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>L2</p>
                                 </div>
                                 
                                 <div>
                                    <p>y-value</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 <div>Put marker on dotted line and enter all your y-values.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Note:&nbsp;&nbsp; You can change any value by highlighting the incorrect value and typing in
                                    new value. <span>Enter</span> 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Calculating equation:</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Stat</div>
                                 
                                 
                                 <div>Section that will allow you to calculate your equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>CALC</div>
                                 
                                 <div>
                                    <p>Right arrow</p>
                                 </div>
                                 
                                 <div>Highlights the CALCulation section.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>4</p>
                                 </div>
                                 
                                 <div>4: LinReg(ax+b)</div>
                                 
                                 <div>Calculates the slope (a) and the y-intercept (b) of your linear equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Note:&nbsp;&nbsp; The calculator may also display values of r2 and r which is used by some instructors.</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Graphing equation:</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <p>y =</p>
                                 </div>
                                 
                                 <div>
                                    <p>y1 =</p>
                                 </div>
                                 
                                 <div>Will put your equation into any 'y' that you put your cursor on.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>VARS</p>
                                 </div>
                                 
                                 <div>
                                    <p>5</p>
                                 </div>
                                 
                                 <div>
                                    <p>5: Statistics</p>
                                 </div>
                                 
                                 <div>Will move you to next screen for finding your equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>EQ</p>
                                 </div>
                                 
                                 <div>
                                    <div>Right Arrow twice </div>
                                 </div>
                                 
                                 <div>This will show a screen with a list that includes 1: RegEQ</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>1: RegEQ</p>
                                 </div>
                                 
                                 <div>
                                    <p>Enter </p>
                                 </div>
                                 
                                 <div>Returns you to the&nbsp; y=&nbsp; screen showing your equation.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>GRAPH</p>
                                 </div>
                                 
                                 <div>
                                    <p>Adjust window</p>
                                 </div>
                                 
                                 <div>Your now have your equation on the screen.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Note:&nbsp;&nbsp; The following steps will include all your scatterplot points on with your
                                       graph.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>y =</p>
                                 </div>
                                 
                                 <div>
                                    <p>Up arrow</p>
                                 </div>
                                 
                                 <div>Plot1 should now be highlighted and flashing.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Enter</p>
                                 </div>
                                 
                                 
                                 <div>The Plot1 colors should reverse themselves which turns on the Plot1.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>GRAPH</div>
                                 
                                 
                                 <div>Your graph should now include boxes that represent your points.</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Note:&nbsp;&nbsp; Leaving the Plot1 when doing other work will often give you an error message.</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p> <a href="../../academic-success/math/calculators.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../academic-success/math/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/calculators.pcf">©</a>
      </div>
   </body>
</html>