<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/math/state-test.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/math/">Math</a></li>
               <li>Math Support at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <h2>State Test </h2>
                        
                        <p>Florida no longer has a State Test, but this is good review material. </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Questions</div>
                                 
                                 
                                 
                                 <div>Forms</div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>1-4 </div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q1-4A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q1-4B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q1-4C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q1-4D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q1-4E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>5-8 </div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q5-8A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q5-8B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q5-8C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q5-8D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q5-8E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>9-11 </div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q9-11A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q9-11B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q9-11C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q9-11D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q9-11E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>12-14</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q12-14A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q12-14B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q12-14C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q12-14D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q12-14E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>15-18</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q15-18A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q15-18B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q15-18C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q15-18D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q15-18E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>19-22</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q19-22A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q19-22B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q19-22C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q19-22D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q19-22E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>23-25</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q23-25A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q23-25B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q23-25C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q23-25D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q23-25E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>26-28</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q26-28A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q26-28B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q26-28C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q26-28D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q26-28E.htm">E</a></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>29-30</div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q29-30A.htm">A</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q29-30B.htm">B</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q29-30C.htm">C</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q29-30D.htm">D</a></div>
                                 
                                 <div><a href="../../../academic-success/math/statetest/Q29-30E.htm">E</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>What was on the State Test for Developmental Math II?</h2>
                        
                        <ol>
                           
                           <li> Order of Operations (No grouping/No exponents)</li>
                           
                           <li>Order of Operations with grouping and exponents</li>
                           
                           <li> Absolute value with addition and subtraction</li>
                           
                           <li> Simplify algebraic expression using distributive property</li>
                           
                           <li> Evaluate an algebraic expression</li>
                           
                           <li> Solving a linear equation</li>
                           
                           <li> Solving a linear equation with fraction coefficient(s)</li>
                           
                           <li> Solving a literal equation</li>
                           
                           <li>
                              <u>Translate</u> a word problem to an algebraic equation
                           </li>
                           
                           <li> Solve a word problem</li>
                           
                           <li>
                              <u>Translate</u> word problem to a proportion
                           </li>
                           
                           <li> Simplifies exponential expression (positive integer exponents)</li>
                           
                           <li> Simplifies exponential expression (positive and negative integer exponents)</li>
                           
                           <li> Simplifies exponential expression (positive and negative and zero integer exp.)</li>
                           
                           <li> Scientific notation (To or From)</li>
                           
                           <li> (Polynomial) – (Polynomial)</li>
                           
                           <li> (Monomial)(Binomial)</li>
                           
                           <li> (Binomial)(Binomial)</li>
                           
                           <li> Factoring a polynomial - Greatest Common Factor(s)</li>
                           
                           <li> Factoring a polynomial – Difference of Squares</li>
                           
                           <li> Factoring a polynomial – By Grouping</li>
                           
                           <li> Factoring a trinomial</li>
                           
                           <li> Simplifies a rational expression – Reduce by factoring</li>
                           
                           <li> Solving quadratic equation by factoring (a = 1)</li>
                           
                           <li> Solving quadratic equation by factoring (a ¹ 1)</li>
                           
                           <li> Simplify square root of a monomial</li>
                           
                           <li> Simplify square roots in a polynomial using distributive property</li>
                           
                           <li> Solving a linear inequality</li>
                           
                           <li> Identify intercepts of a linear equation (ax + by = c)</li>
                           
                           <li> Match linear equation to graph (ax + by = c) or (y = mx + b)</li>
                           
                        </ol>
                        
                        <p>Suggestions</p>
                        
                        <p> Work all the problems. Guess, if necessary.<br>
                           Check your work by substituting answers back into the original problem.<br>
                           Check all simplifications by redoing question on a clean sheet of scrap paper.
                        </p>
                        
                        <p> <a href="../../../academic-success/math/statetest/index.html#top">TOP</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="../../../math/liveScribe.html"><img alt="Math 24/7 Help" border="0" height="60" src="../../../academic-success/math/statetest/math-24-7_245x60.png" width="245"></a></p>
                        
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../academic-success/math/index.html">
                                          Academic Success Center - Math Lab 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 102b</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2840</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8:00 am - 9:00 pm<br>Friday: 8:00 am - 5:00 pm<br>Saturday: 8:00 am - 4:00 pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>               
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/math/state-test.pcf">©</a>
      </div>
   </body>
</html>