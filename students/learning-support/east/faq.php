<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Success Center, East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Academic Success Center, East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Academic Success Center, East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <h3> Q: How do I print something? </h3>
                        
                        <p>
                           <object border="0" classid="clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="322" width="440">
                              
                              <param name="src" value="../academic-success/PrintJob-KIOSKmov.mov">
                              
                              <param name="autoplay" value="false">
                              
                              <param name="controller" value="true">
                              
                              <param name="scale" value="aspect">
                              
                              <param name="bgcolor" value="white">
                              
                              <param name="pluginspage" value="http://www.apple.com/quicktime/download/">
                              
                              <embed autoplay="false" bgcolor="white" border="0" controller="true" height="322" pluginspage="http://www.apple.com/quicktime/download/" scale="aspect" src="http://valenciacollege.edu/east/academicsuccess/PrintJob-KIOSKmov.mov" width="440"> 
                              </object> 
                           
                        </p>
                        
                        <p><strong>A:</strong> You can get a printer card from the machine on the wall next to 4-101 (ASL/Study
                           Room) 
                        </p>
                        
                        <p>There is also a machine located outside of Foreign Language Labs (4-104 and 105).</p>
                        
                        <p>Sign into a computer using your Atlas username and password. Select the file that
                           needs printing, and hit print. 
                        </p>
                        
                        <p>Head over toward the center of the ASC computer area. There you'll find two printers
                           and two computers located next to them (with a pink slip of paper on top). 
                        </p>
                        
                        <p>On those computers, find and click on your username, insert your printer card into
                           the small box next to the computer, then hit print on the bottom of the screen. 
                        </p>
                        
                        <p>Your paper(s) will print out. BE SURE to press the white button on the small box to
                           get your printer card back. You may keep the printer card for future use.
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span>How do I scan something? 
                        </h3>
                        
                        <p>
                           <object border="0" classid="clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="355" width="427">
                              
                              <param name="src" value="../academic-success/ScanJob-KIOSKmov.mov">
                              
                              <param name="autoplay" value="true">
                              
                              <param name="controller" value="true">
                              
                              <param name="scale" value="aspect">
                              
                              <param name="bgcolor" value="white">
                              
                              <param name="pluginspage" value="http://www.apple.com/quicktime/download/">
                              
                              <embed autoplay="true" bgcolor="white" border="0" controller="true" height="355" pluginspage="http://www.apple.com/quicktime/download/" scale="aspect" src="http://valenciacollege.edu/east/academicsuccess/ScanJob-KIOSKmov.mov" width="427"> 
                              </object>
                           
                        </p>
                        
                        <p><strong>A:</strong> Using the Scanner
                        </p>
                        
                        <p>1. Log in to a computer that has a scanner attached with your Atlas username and password.</p>
                        
                        <p>2. Click on the “One-Click” icon located on the top right of the scanner toolbar on
                           the desktop. (If the toolbar does not automatically open, double click the scanner
                           icon on the desktop.)
                        </p>
                        
                        <p>3. Click on the “PDF Document” located at the bottom right corner.</p>
                        
                        <p>4. Leave the setting in color document mode.</p>
                        
                        <p>5. Uncheck the box “Save to subfolder with current date”</p>
                        
                        <p>6. Type the name of the document to be scanned in the “File Name” box.</p>
                        
                        <p>7. For the “Save In” location, click “Browse”then choose “Desktop” or “Removable Disk.”
                           This will automatically save the document to the desktop or your&nbsp; ash drive a er scanning.
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span> How do I make a copy of something? 
                        </h3>
                        
                        <p>
                           <object border="0" classid="clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="345" width="438">
                              
                              <param name="src" value="../academic-success/CopyJob-KIOSKmov.mov">
                              
                              <param name="autoplay" value="true">
                              
                              <param name="controller" value="true">
                              
                              <param name="scale" value="aspect">
                              
                              <param name="bgcolor" value="white">
                              
                              <param name="pluginspage" value="http://www.apple.com/quicktime/download/">
                              
                              <embed autoplay="true" bgcolor="white" border="0" controller="true" height="345" pluginspage="http://www.apple.com/quicktime/download/" scale="aspect" src="http://valenciacollege.edu/east/academicsuccess/CopyJob-KIOSKmov.mov" width="438"> 
                              </object>
                           
                        </p>
                        
                        <p><strong>A:</strong> 1. Purchase your Valencia Copy/Print Card at any self-service card station with a
                           $1 bill, or at the&nbsp; Campus Bookstore with any other form of payment.
                        </p>
                        
                        <p>2. Insert your copy/print card in the card reader box next to the copy machine.</p>
                        
                        <p>3. Press the “Energy Saver” button to turn the copy machine on let it warm up.</p>
                        
                        <p>4. Place paper in the copier vertically (short side at the top left corner) and close
                           the lid.
                        </p>
                        
                        <p>5. Press “Start.”</p>
                        
                        <p>6. The copy will come out at the bottom of the machine.</p>
                        
                        
                        <h3> Q: Where do I get a print card? </h3>
                        
                        <p><strong>A:</strong> You can get a printer card from the machine on the wall next to 4-101 (ASL/Study
                           Room).
                        </p>
                        
                        <p>There is also a machine located outside of Foreign Language Labs (4-104 and 105).</p>
                        
                        <p><strong>IMPORTANT NOTE -</strong> The print card machines only accept $1 bills. If you do not have a $1 bill, you may
                           purchase a printer card with any type of payment method (cash, coins, debit/credit)
                           at the college bookstore.
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span> Where can I get change? 
                        </h3>
                        
                        <p><strong>A:</strong> If you are in need of change, you can get it from the bookstore, located in Building
                           5. 
                        </p>
                        
                        <p>If the bookstore is closed, you can also get change from the cafeteria during normal
                           hours of operation. 
                        </p>
                        
                        <p>If you are trying to get change to purchase a print card, print cards are also available
                           at the bookstore.&nbsp; 
                        </p>
                        <br>
                        
                        <h3>
                           <span>Q:</span> Can I print in color? 
                        </h3>
                        
                        <p><strong>A:</strong> No. The printers in the Academic Success Center only print in black &amp; white. 
                        </p>
                        
                        <p>Printing in color is available upstairs in the Library. The cost is $0.25 per page.
                           
                        </p>
                        
                        <p>A printer card is required for either option.</p>
                        
                        
                        <h3>Q: Can I fax something here? </h3>
                        
                        <p> <strong>A:</strong> There are no faxing services available in the Academic Success Center. 
                        </p>
                        
                        <p>For faxing services (school related documents only) students may go to the Student
                           Development or Career Center located upstairs in Building 5.
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span> Where is the Testing Center? 
                        </h3>
                        
                        <p><strong>A: </strong>The SPA is tutoring for students enrolled in MAT 1033C level classes or lower. 
                        </p>
                        
                        The MSC also has desks, and a computer area located on the right side which is tutoring
                        for students enrolled in MAC 1105 level classes and higher.
                        
                        <p> <img alt="Testing Center Map" height="338" hspace="0" src="../academic-success/TestingCenter.jpg" vspace="0" width="450"></p>
                        
                        <h3>
                           <span>Q:</span> Where is the Math Center? 
                        </h3>
                        
                        <p><strong>A:</strong> The SPA is tutoring for students enrolled in MAT 1033C level classes or lower. 
                        </p>
                        
                        <p>The MSC also has desks, and a computer area located on the right side which is tutoring
                           for students enrolled in MAC 1105 level classes and higher.
                           
                        </p>
                        
                        <p><img alt="Math Center Map" height="337" hspace="0" src="../academic-success/mathcenter.png" vspace="0" width="450"></p>
                        
                        <h3>
                           <span>Q:</span> Where is the Writing Center (Communications Center)? 
                        </h3>
                        
                        <p><strong>A:</strong> The Writing Center is located in the far left corner upon entrance of the Academic
                           Success Center. It is located in room 4-120.
                        </p>
                        
                        <p><img alt="Communications Center Map" height="337" hspace="0" src="../academic-success/communicationcentermap_000.png" vspace="0" width="450"></p>
                        
                        <h3>
                           <span>Q:</span> Do you have study rooms? 
                        </h3>
                        
                        <p><strong>A: </strong>The ASC has two areas that may be checked out for use as study rooms. The ASL tutoring
                           room, located in the room 4-101 Window Lounge, may be checked out when ASL tutoring
                           is not taking place. The Speech Lab, located in Room 4-120A, may also be checked out
                           as a study area. Both locations have a checkout period of one hour. Reservations may
                           be made by following the links provided below:
                        </p>
                        
                        <ul>
                           
                           <ul>
                              
                              <li><strong><a href="https://checkappointments.com/appts/8jzdIrbbCY">ASL Study Room</a> </strong></li>
                              
                              <li><strong><a href="https://checkappointments.com/appts/BxkEH0BMTO">Speech Lab</a></strong></li>
                              
                           </ul>
                           
                        </ul>            
                        
                        
                        <p>Students are also welcome to study using the open tables, desks and computers located
                           in the ASC. Please remember to be courteous to the fellow students around the center.&nbsp;&nbsp;&nbsp;
                           
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span>Where can I check out items such as calculators or headphones
                           
                           ? 
                        </h3>
                        
                        <p><strong>A: </strong>Students are able to check out calculators and headphones at the Welcome Desk, located
                           in the center of the Academic Success Center. 
                        </p>
                        
                        <p>A Valencia ID is required to check out all equipment. Laptops and tablets are also
                           available for check out. These items have a<strong> 4 hour time limit</strong>.
                        </p>
                        
                        
                        <h3>Q: Where do I get a Valencia ID? </h3>
                        
                        <p><strong> A: </strong>Students may obtain a Valencia ID through the Security Office, which is located upstairs
                           in Building 5-220. Every student gets his or her first Valencia ID free of charge.
                        </p>
                        
                        <p>For any other questions regarding a student ID, call the Security Office. <strong>407.582.2000</strong></p>
                        
                        
                        <h3>
                           <span>Q:</span> Where do I pay my fines? 
                        </h3>
                        
                        <p><strong>A:</strong> Students may pay any fines at the Business Office located upstairs in Building 5-214.
                        </p>
                        
                        <p>For any other questions regarding fines, please call the Buisness Office. <strong>407.582.2387</strong></p>
                        
                        
                        <h3>
                           <span>Q:</span> How do I sign in to the computers? 
                        </h3>
                        
                        <p><strong>A:</strong> Use must your valid Atlas username and password to sign in to any computer located
                           in the Academic Success Center.
                        </p>
                        
                        
                        <h3>
                           <span>Q:</span> How do I become a tutor? 
                        </h3>
                        
                        <p><span>A:</span> All tutor hiring information can be found by going to the Jobs tab, by clicking <a href="../academic-success/tutoring/TutorHiring.html">here</a>, or by calling the Welcome Desk at 407-582-2540.
                        </p>
                        
                        
                        <h3>
                           <span>Q: </span>Is my class meeting in here today
                           
                           ? 
                        </h3>
                        
                        <p><strong>A: </strong>The ASC and Welcome Desk staff are not informed regarding class meetings.
                        </p>
                        
                        <p>If you know the name of your professor and where the meeting location is taking place,
                           then we can try to help get you where you need to go.
                        </p>
                        
                        <p>If you do not know any of the details, then we suggest reaching out to your professor.</p>
                        
                        
                        <h3>
                           <span>Q: </span>What are the office hours for my professor
                           
                           ? 
                        </h3>
                        
                        <p><strong>A: </strong>Reach out to your professor. If you are unable to contact your professor, refer to
                           the College Directory to get your professors information.&nbsp; <a href="../../contact/directory.html">Valencia College - Faculty &amp; Staff Directory </a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2540</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:00 am - 10:00 pm <br>Friday: 7:00 am - 7:00 pm<br>Saturday: 8:00 am - 4:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <a href="events.html">ASC Events &amp; News</a>
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/faq.pcf">©</a>
      </div>
   </body>
</html>