<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li>Academic Success East</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <link href="../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    
                                    
                                    <div> 
                                       
                                       <div>
                                          <img alt="Featured Image 1" border="0" height="260" src="../academic-success/ASC.png" width="740">
                                          
                                       </div>
                                       
                                       
                                       <div>
                                          <img alt="Featured Image 2" border="0" height="260" src="../academic-success/2.jpg" width="740">
                                          
                                       </div> 
                                       
                                       
                                       <div>
                                          <img alt="Featured Image 3" border="0" height="260" src="../academic-success/3.jpg" width="740">
                                          
                                       </div> 
                                       
                                       
                                       <div>
                                          <img alt="Featured Image 4" border="0" height="260" src="../academic-success/4.jpg" width="740">
                                          
                                       </div> 
                                       
                                       
                                       <div>
                                          <img alt="Featured Image 5" border="0" height="260" src="../academic-success/5.jpg" width="740">
                                          
                                       </div> 
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>  
                              
                              
                              
                              
                              
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       ABOUT
                                    </div>
                                    
                                    <div>
                                       
                                       <div><a href="mission.html" title="Mission Statement">MISSION STATEMENT</a></div>
                                       <br>
                                       
                                       <div><a href="hours.html" title="Hours">HOURS</a></div>
                                       <br>
                                       
                                       <div><a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/contact.cfm" target="_blank" title="Contact Us">CONTACT US</a></div>
                                       <br>
                                       
                                       <div><a href="location.html" title="Location">LOCATION</a></div>
                                       <br>          
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    RESOURCES
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="../academic-success/welcomedesk/index.html" title="">Welcome Desk</a></div>
                                    <br>
                                    
                                    <div><a href="virtuallibrary.html" title="">Virtual Library</a></div>
                                    <br>
                                    
                                    <div><a href="onlinetutoring.html" title="">Online Tutoring</a></div>
                                    <br>
                                    
                                    <div><a href="technology.html" title="">Data Request</a></div>     
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 MORE INFO
                                 
                              </div> 
                              
                              <div>
                                 
                                 <div><a href="featuredtutor.html" title="">Featured Staff</a></div>
                                 <br>
                                 
                                 <div><a href="events.html" title="">ASC Events</a></div>
                                 <br>
                                 
                                 <div><a href="faq.html" title="">FAQ</a></div>
                                 <br>
                                 
                                 <div><a href="employment.html" title="">Employment</a></div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/index.pcf">©</a>
      </div>
   </body>
</html>