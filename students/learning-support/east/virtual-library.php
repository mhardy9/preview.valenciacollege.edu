<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Success Center, East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/virtual-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Academic Success Center, East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Academic Success Center, East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>Virtual Library</strong></h2>
                        
                        <p>The Virtual Library consists of short videos that cover a variety of topics in multipe
                           subject areas. These short videos reinforce core skills and concepts covered in curriculum
                           across the disciplines. Please refer to the table below for specific topics. 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><u>Math Center </u></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><a href="../../math/liveScribe.html">Math 24/7 Videos</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><u>Communication Center </u></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>GRAMMAR</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                          <p><u><strong>Prepositional Phrases</strong></u><strong> </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RS3bKw_cyow">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=owh5U8Z94ew">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=7FXKYff6dnQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Seperating the appositive </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=X16-lzjyXX0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=icL9A-uT3jk">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PPfU6LsnPno">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=iT2BvVs8Dx4">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Items in a series</strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OKIizvs1k0Q">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=g0gmBgFgIqw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nGl1j-SV5QA">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Comma splice &amp; Run-ons </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BesMcdV1KqY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=mQfJdhyeQfU">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-vck6uK-kow">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=eoI1qa6Yf40">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Dependant &amp; Independant Structure </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Rf8kD2LXcL8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bCZ2FY5xOto">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5SKrAK7SDmE">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=iO7FSq_LGQA">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Semicolons</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=IXaAVc3PQKQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=d-3nnht-b40">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5kssP4fWs9U">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=g384KO4Y7Rs">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Verb / Noun Agreements</strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UKm98TVFTQ4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dmw0kBuuEy4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CnL1RoE06vE">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=M_P6d_Pt76k">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Past and Present Tense Consistency</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=p15741AbW9I">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=DrK2wdnu3Pg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_8ObuO5neR0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3ek7V59tpLI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AM3RydRXn_g">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Preposition Use </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=B48cfVFWxcw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jLX9UqIo_kw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_2Hmox2tnjw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=E3gvjrh5_TY">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Pronoun Antecedent Agreement </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sKVFtXF86vI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VQ2IHN4VD68">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=q4kv6t_2Mdc">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>STRUCTURAL</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Thesis Statement </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wCzuAMVmIZ8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8wxE8R_x5I0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4sx42_C10zw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=en1xVDU0xlI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-e2EthZC0aU">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Topic Sentences </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=NLzKqujmdGk">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=l2nOzN6DllY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Scxgz4V2zjQ">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lLz0XgzqFu0">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Supporting Details </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=j104XC8JF4s">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BNdeV_M5820">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vv53n9H-fvU">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XFPiYCeHeys">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Conclusion</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pvs9IpA5O2s">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2L7aeO9fBzE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nqg0wHltfg0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=eYzuw70yhSU">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vU_lahZhBwU">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nKPtdqsj4gE">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Sentence Structure </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VPyo8-Pr55Q">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=F03w-vOV-xw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jIDyXReoV2g">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hGjGlhUT48Y">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Sentence Types </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=WAAizXG1osU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=76GY_h8MwAQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Hby4NBOwf7E">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hWmKnrtlTHU">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>MLA &amp; APA FORMATS</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MLA Format </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8xAc4yZ8VSA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=24Y31UrG2q4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=24Y31UrG2q4">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MLA Citation </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uwPydVmvsk4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=27De6EnqUzg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2UhzuQC161o">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2bVXJpIQAwI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XLmmNQDbnHk">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EK0CH6ePGgI">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><u><strong>APA Format</strong></u></p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VKWKswH29kM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pdAfIqRt60c">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gEjTmHr3yRo">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>APA Citation </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CL2RrT6jFpQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KN22bZ1FWa8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qzKlb7E7ERc">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9pbUoNa5tyY">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=10eg_GB_A9E">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=fLlLIYxeGls">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><u>Foreign Language Lab </u></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>SPANISH</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                          <p><u><strong>Ser v. Estar</strong></u><strong></strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=1r59lFAw180">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Stem Changing Verbs in the Present </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=IBj7-ZnP-Dk">E changing to I</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PL7f8FOjwpg">O changing to UE</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=18zgcTNvQ5I">E changing to IE</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Double Object Pronouns </strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YVAEobhtBgI">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Irregular Verbs in the Preterite </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="http://youtu.be/XjDKPR2BWlI">Estar</a></p>
                                       
                                       <p><a href="http://youtu.be/8lRfndxbZCE">Tener</a></p>
                                       
                                       <p><a href="http://youtu.be/BiZV9ciAFtQ">Ir &amp; Ser</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>FRENCH</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Popular YouTube channel for learning French </strong></u></div>
                                    
                                    <div><a href="http://www.youtube.com/user/lsfrench/featured">Learn French with Pascal</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_O-nmoMIb0M&amp;list=UUvirLHlKtDvRBB6mHDqgdpg">Passe Compose 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Qe1HsTDVU28&amp;list=UUvirLHlKtDvRBB6mHDqgdpg">Passe Compose 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ptVBVxYvny0&amp;list=UUvirLHlKtDvRBB6mHDqgdpg">Adjectives</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=isJQd5etn14&amp;list=UUvirLHlKtDvRBB6mHDqgdpg">Ask Questions 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qRQCi-Mvto0&amp;list=UUvirLHlKtDvRBB6mHDqgdpg">Ask Questions 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>GERMAN</strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=ML3GIxLvWXE">Dative Case</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=2D3AivwwjNQ">Accusative Case</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=IRJYfu5ckx8">Nominative Case</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=Mky4dEagYdQ">Which Gender Should I Use?</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=QHjOr6kDhjM">Grammar</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=4H-NLkO90vc">Possessive Adjectives</a></p>
                                       
                                       <p><a href="http://www.youtube.com/watch?v=U05x3ZQJEts&amp;list=PLjawhIWnf2lPSdjPGz1otcLBwDbs3c1tR">Perfect Tense</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><u>EAP Lab</u></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                          <p><u><strong>Punctuation</strong></u><strong></strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YbEx4x2Os0U">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LdCOswMeXFQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=1aa--jf4CjY">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Semicolon</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=IXaAVc3PQKQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=d-3nnht-b40">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5kssP4fWs9U">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=g384KO4Y7Rs">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Commas</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pOwcovqtkGY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=keZpj7PjNEo">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=i0Po7ZQCm9g">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Run-on Sentences </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=mQfJdhyeQfU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=z6RBG4UiwD8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aFMhbxAv-Hk">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Comma Splices </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PLAU8LsRR6k">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-vck6uK-kow">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OlHe2ZWRT6U">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Fragments</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=0-JRCuwpasA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=r-Wcr4Wgf7U">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_loC2XMeqxc">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=JAuCJIzrwn4">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Thesis Statement </strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wCzuAMVmIZ8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8wxE8R_x5I0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4sx42_C10zw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=en1xVDU0xlI">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Identify Main Ideas </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=tM65ImKfj-Q">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=n5_l98pF_yw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=j104XC8JF4s">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ZE7gZvPeCiI">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Identify Supporting Details </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vv53n9H-fvU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yF9uKLG10Y4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vCPqdCS5qmU">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dF1j-kZ6x04">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Creating A Formal Speech Outline</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CZ57Jh1xdRw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YFVwCJl41uw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Dd-5C2K5RpQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Modals</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8zmNJaTDZ6Y">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Articles</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=seg9jEcWIo4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=kBrUgUpjMjU">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8CiA9BCRPBk">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Subject &amp; Verb Agreement </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yEVhUEq6P1w">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UKm98TVFTQ4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sABEf7VA6No">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Pronoun Agreement </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sKVFtXF86vI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VQ2IHN4VD68">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=q4kv6t_2Mdc">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Introduction Paragraph </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_tv2-lXHfAI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=k5jgNh4FwBU">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=I-GSUSbw1CQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Thesis Statement </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wCzuAMVmIZ8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8wxE8R_x5I0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4sx42_C10zw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=en1xVDU0xlI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-e2EthZC0aU">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Topic Sentences</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=NLzKqujmdGk">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=l2nOzN6DllY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Scxgz4V2zjQ">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lLz0XgzqFu0">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Supporting Details </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=j104XC8JF4s">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BNdeV_M5820">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vv53n9H-fvU">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XFPiYCeHeys">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Conclusion</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pvs9IpA5O2s">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2L7aeO9fBzE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nqg0wHltfg0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=eYzuw70yhSU">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vU_lahZhBwU">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nKPtdqsj4gE">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Sentence Structure </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VPyo8-Pr55Q">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=F03w-vOV-xw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jIDyXReoV2g">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hGjGlhUT48Y">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Sentence Types </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=WAAizXG1osU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=76GY_h8MwAQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Hby4NBOwf7E">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hWmKnrtlTHU">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MLA Format </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8xAc4yZ8VSA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=24Y31UrG2q4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=24Y31UrG2q4">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MLA Citation </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uwPydVmvsk4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=27De6EnqUzg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2UhzuQC161o">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2bVXJpIQAwI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XLmmNQDbnHk">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EK0CH6ePGgI">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>APA Format </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VKWKswH29kM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pdAfIqRt60c">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gEjTmHr3yRo">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>APA Citation </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CL2RrT6jFpQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KN22bZ1FWa8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qzKlb7E7ERc">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9pbUoNa5tyY">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=10eg_GB_A9E">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=fLlLIYxeGls">Video 6</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><u>General Tutoring </u></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>American Sign Language <a name="ASLvirtual" id="ASLvirtual"></a></strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p><u><strong>Vocabulary</strong></u><strong></strong></p>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="http://deafhealth.org">Medical Terms</a></p>
                                       
                                       <p><a href="http://signingsavvy.com">Signing Savvy</a></p>
                                       
                                       <p><a href="http://lifeprint.com">Life Print</a></p>
                                       
                                       <p><a href="http://aslpro.com">ASL Pro</a></p>
                                       
                                       <p><a href="http://handspeak.com">Hand Speak</a></p>
                                       
                                       <p><a href="http://spreadthesign.com">Spread The Sign</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Storytelling</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div><a href="https://www.youtube.com/user/aslizedvideos">ASLized Videos</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Classifiers</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div><a href="https://www.youtube.com/user/WinkASL">Wink ASL</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>News/Vlogs</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="http://oicmovies.com">OIC Movies</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=7ZPnOwluOrw">ASL THAT!</a></p>
                                       
                                       <p><a href="https://www.youtube.com/channel/UCK-n3cLst9SuYfxk7oNhrlw">ASL Stew</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Kids</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="http://asl-kids.com/">ASL Kids</a></p>
                                       
                                       <p><a href="http://kidcourses.com/sign-language-asl/">ASL THAT! Kids</a></p>
                                       
                                       <p><a href="http://www.signingtime.com/">Signing Time</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Interpreting Resources</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="http://healthcareinterpreting.org">Health Care Interpreting</a></p>
                                       
                                       <p><a href="http://discoverinterpreting.com">Discover Interpreting</a></p>
                                       
                                       <p><a href="http://eip.org">EIP</a></p>
                                       
                                       <p><a href="http://rid.com">RID</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Fingerspelling</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="http://one%20happy%20bee">One Happy Bee</a></p>
                                       
                                       <p><a href="http://asl.ms">ASL.ms</a></p>
                                       
                                       <p><a href="http://asl.bz">ASL.bz</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          
                                          <p><u><strong>Introduction</strong></u><strong></strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=k9cFqNYlV1A">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>ASL YouTube Pages </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/user/billvicars">Bill Vicars</a></p>
                                       
                                       <p><a href="https://www.youtube.com/channel/UCM6yFmQWbC-hkdd7NaZvpKA">ASL Lab</a></p>
                                       
                                       <p><a href="unknown:">Interpreter Helper</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Signing Naturally</strong></u><strong></strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=iJZgQn75Nu4&amp;list=PLgG7klE9xZxYoiMAAzf9Qa8YcfZANbMSf">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gDLEysb1s-k">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Ethics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5fyaoaTb-X8">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Translation</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=JdS6ZeX18DU&amp;list=PLIY1mSY9_FcFKi5nGyoE8IRYGb2h8xmEw">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Consecutive</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h2zz6Wa-DWg&amp;list=PLHh-TTfWHtnkLo_elf9Q357NkbPU5M-fk">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=TGuh07G_53E">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Simultanious Interpreting </strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Ol552QC_zhE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=P1g3geJE7LE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Sod2aw995d8">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2vfpRO2mw9k">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>BIOLOGY 1 <a name="Bio1" id="Bio1"></a></strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Protons, Neutrons &amp; Electrons </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=P-wDdFyeLpM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zEX2aGpIDBY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LdWonERbS7Y">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Electronegativity </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Rr7LhdSKMxY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8N3jq4iljLM">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Kj3o0XvhVqQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>How Elctronegativity Affects Atomic Interactions </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KOYmr5W3OfM">Video 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Avogardo's Number &amp; Moles </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AsqEkF7hcII">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wrGhyhgwODo">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Conversion Factors </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HZ9weUkSdoY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4AW4sKd3D0A">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-xPNXPCjtyI">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Deizl7Saurg">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Formation of Ions &amp; Bonds </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Qf07-8Jhhpc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=QqjcCvzWwww">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9qL1fTqsEVg">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Covalent vs. Polar Covalent Bonds </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=02Q352-Y7iU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HCBsE6CDjKc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=SBWQOc35044">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Balancing Equations </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_B735turDoM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gskm-dfKv5g">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><u><strong>Naming Chemical Compounds</strong></u></p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CkCzceecCrc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9XUsOLaz3zY">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Building Blocks of Proteins, Fats, Carbs &amp; DNA </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=fD43hO0rlgc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=1FzvgJom8Og">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=H8WJ2KENlK0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zOSy7SrhuIU">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Condensation RXNS vs. Hydrolysis RXNS</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5Q6fSk3vPoA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sRhH1aYo5UY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gfg3AtOknfI">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=7e2IkQHxszM">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Protein Structure &amp; Folding</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xNa42-n4vGE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=O5gN-IK6uKs">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yZ2aY5lxEGE">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lijQ3a8yUYQ">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Cell Oranelles &amp; Function </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LP7xAr2FDFU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ZyWYID2cTK0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aczbMlSMr8U">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hRvh26xu-SE">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Phospholipid Bilayer</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=y31DlJ6uGgE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9ILSsW3tXr8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=SkUSBkDmWGc">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>DNA Replication </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=27TxKoFU2Nw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8kK2zwjRV0M">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=FBmO_rmXxIw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_88d9GufML0">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Transcription</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=WsofH466lqk">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5MfSYnItYvg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=SMtWvDbfHLo">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Translation</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5bLEDd-PSTQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=kbB2kMkmb0w">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Nm5yzrd28rs">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Gene Regulation </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=MkUgkDLp2iE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rs6UkVaOPzo">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3S3ZOmleAj0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=z1wtrlBT6yY">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Replication of Viruses </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EqK1CYYQIug">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PEWjyx2TkM8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EICader2NWo">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Cellular Metabolism </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=efzWdP-i3Jo">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2f7YwCtHcgk">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PrZBHFVj9EY">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>BIOLOGY 2 <a name="Bio2" id="Bio2"></a></strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><u><strong>Hardy Weinberg Equilibrium</strong></u></p>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4Kbruik_LOo">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=oG7ob-MtO8c">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=T6KkEMSEyvo">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Genetic Drift </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=mjQ_yN5znyk">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wkFoqRdJpvE">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Cladistics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BIDPkknZ_Vg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=t4_Y6LYo1YI">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3DxNwzzjm_4">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Frog - Early Development &amp; Differentiation of Cells </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=JceGik3Q5A8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dXpAbezdOho">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5oXmN-iC33E">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Alternation of Generations</strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ypFzdHINLTw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=SCTNKTfa-s0">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>AP 1 &amp; 2 <a name="AP1and2" id="AP1and2"></a></strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Microbiology</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ThU9Ckp1mB8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=c1OVUIz_0k4">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Culture Media </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=EoQEgVxxDxI">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Various Staining Techniques </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=i6jXWl3EUjA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OOFJyw0EYBU">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Viral Infections </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=QUQIudmvfjM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h9lxx6x3HAM">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Rpj0emEGShQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Operon Model of Gene Expression </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3S3ZOmleAj0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sx7n90crwww">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Patheogens</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_KGUHcwUMUM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gbpPxJ3zBTE">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Lifecycle of Diseases </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qMNmOsl5_e4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Gqc481RjMz4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=W2_-aSDaXHc">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=odRyv7V8LAE">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Emerging Plant Pathogens</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zW18wCNAivQ&amp;list=PLEflB_vO0LjGvFKhiZXPrHeztJOA97USD">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=0nOO3UG17j0">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u><strong>CHEMISTRY<a name="Chemistry" id="Chemistry"></a></strong></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Measurements</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bJldpTvKeA0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pFl49nIGQF8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uZ0ILIG_l7w">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h7yhfSut6Kk">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Elements, Compounds &amp; Mixtures </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=a52uyxwIb6o">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lDNpCAFKhqY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3gLMqfN4OIk">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Stoichiometry</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=SjQG3rKSZUQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LQq203gyftA">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XnfATaoubzA">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Gases</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BxUS1K7xu30">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ehvLyvwAEYc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Ifr_gOOPRug">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Thermochemistry</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AevwelqPaaU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xmKiwEhpcd4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OdO8hBBUPtM">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Structure of the Atom </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lP57gEWcisY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sRPejoNktKE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h6LPAwAmnCQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Ionic Compounds </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yyWuGmX-C1k">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lhC42qxk5kQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CkCzceecCrc">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Covalent Bond </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Mo4Vfqt5v2A">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=20AbmhCk-RI">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qE2jH5HtB-s">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Main-Group Metals </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=GSeaaROpz3E&amp;index=1&amp;list=PLDDD71372E7069489">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sqBmRXd5FG4">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Chemistry of Non-Metals </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=1O5Dz_t3F0U">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-q1OW8vJ3wA">Video 2</a></p>
                                       
                                       <p><a href="https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=video&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=0CB0QtwIwAA&amp;url=http%3A//chemistry.about.com/video/Properties-of-Nonmetals.htm&amp;ei=MHtrVJ3zMpLPgwTRvYDgCg&amp;usg=AFQjCNG4VBG-4vYMts-4W3qxIuBwcNjNVw&amp;sig2=WgZxDzqZLsZ0UKQPcA15rA">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Acid &amp; Bases </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ANi709MYnWg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Vbh52HDorkc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HPvK5lKio8o">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Xeuyc55LqiY">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Transition-Metal Chemistry </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=G0NqI6DTvXc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4P2pxaFx_nw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-xl9A34QvVk">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xxfN6KWqZ5Q">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Structure of Solids </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hOqUo2cYEPg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EHBWE3EYmxQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bMbmQzV-Ezs">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Liquids</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BqQJPCdmIp8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bMbmQzV-Ezs">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Solutions</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9h2f1Bjr0p4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rgp5y16HFtE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nqc9ASP0tq0">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Intro to Kenetics &amp; Equilibria </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=7qOFtL3VEBc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=g5wNg_dKsYY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aT-gcunlFJg">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Ok8uJ-vhGXo">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=E6k8JPUIxOw">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Acid-Base Equilibria </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=l5fk7HPmo5g">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ANi709MYnWg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-fM-QL6IpIU">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Solubility &amp; Complex Ion Equilibria </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=GjwbIarlQmg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=mLsFG6WaXs4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=r7gTH_5XfOI">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Oxidation &amp; Redution Reactions </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lQ6FBA1HM3s">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HiSibhnF3e0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=DvYs1HILq1g">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RX6rh-eeflM">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Electrochemistry </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=IV4IUsholjg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Rt7-VrmZuds">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_fNNQfGGYr4&amp;list=PLD01D3CBE52F3F917">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Chemical Thermodynamics </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LQQNw0Prmcw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=K5k8QmESGgg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=brE24gaFzPU">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Kinetics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=__zy-oOLPug">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_HA1se_gyvs">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uTFtaslJ0LM">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Nuclear Chemistry </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=cOE40P5rHCA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KWAsz59F8gA">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=FU6y1XIADdg">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Dj4CTJy6U9M">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Intro to Organic Chemistry </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YAjpntbbQ2w">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=F4TP3y1gRbs">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2xvtuYqFvtw">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Organic Nomenclature </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=c57jttGS8qI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BulW2otK854">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=96D7RHUQvMA">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Nucleophilic Substitution Reactions </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BRBLa1xX9K8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=QAyriElN-30">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-SJMFJ4pJHs">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <u><strong>Elimination Reactions</strong></u><strong> </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bWHuBmTCC60">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=C2W1gBWA7g8">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=U9dGHwsewNk">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=J0gXdEAaSiA">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Resonance Structures</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gqwswntC3ck">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lPZ-DFEZtq0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=6XOm3Km7r30">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=nUoAWH9M2GI">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Lewis Dot Structures </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ulyopnxjAZ8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Vk3uF7_Zldo">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=b2p-BtAt1T8">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dn2mO9gJhHk">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u>COMPUTER PROGRAMMING <a name="ComputerProg" id="ComputerProg"></a></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Intro to Computer Programming Concepts </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=rSQgk04Algg&amp;list=PL96BE5469318EFC74">Playlist 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>C++ Fundamentals </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KNFv4DZ4Mp4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=S3t-5UtvDN0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-WwGMNGRHdw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rGEp2lAHbqA&amp;list=PL16462912149C15F6">Playlist 1</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Java Programming </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=NiAgQN6wEjA&amp;list=PL6F117B39113D9108">Playlist 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Advanced Java Programming </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=coTTjXOuw-Q&amp;list=PLExJ0mIfzKFxYLIBI-WL_oM36pbkee0d8">Playlist 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>JavaFX Tutorial </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=wkODtTMv14c&amp;list=PLExJ0mIfzKFxtL76g98DntdY0VgJidavo">Playlist 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Mobile Device Software Development</strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=h7LSpkJzTK4&amp;list=PLExJ0mIfzKFwbBQTIf7bwJ1i_2mnEhqHk">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Android App Programming </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4tBez7WcJSQ&amp;list=PLExJ0mIfzKFw6nMo_LTlXR88XLQQxytl7">Playlist 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3SYi1kR3AQQ&amp;list=PLExJ0mIfzKFw4A4GC76uC2Q6TEVOyEgfb">Playlist 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Software Development </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=aYUnRPQIPms&amp;list=PLExJ0mIfzKFzK5xl0NRyu-k_9MrgGv5dr">Playlist 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Arrays</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=u3FZmUVT6V4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5tPLyHCZdU0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BjVeWRNiddE">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Pointers</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=W0aE-w61Cb8">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=FuK3Erbqofc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jYbx8vGB9P8">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aOVputhO2Ow">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h-HBipu_1P0">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Trees</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qH6yxkw0u78">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=01CMAM8KWZY">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sf_9w653xdE">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=COZK7NATh4k">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Recursion</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XGxbXMP6k8k">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zzhE6U3jQYU">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4h9_c97EWCs">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Searching</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AqjVd6FVFbE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vohuRrwbTT4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=iwo5WAldDks">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=h2d9b_nEzoA">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=o3sudomTPJA">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Sorting</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BujvU0GJTtI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Ne9QNWuGJvw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=c4BRHC7kTaQ">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=TW3_7cD9L1A">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=EeQ8pwjQxTM">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aQiWF4E8flQ">Video 6 </a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=6NB0GHY11Iw">Video 7</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><strong>To use both eBrary resources provided below, you'll have to sign in. The BORROWER
                                             ID is your VID and the PASSWORD is the last 4 digits of your VID. </strong></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Java</strong></u></div>
                                    
                                    <div><a href="http://site.ebrary.com.db29.linccweb.org/lib/valencia/detail.action?docID=10131918&amp;p00=java+foundations">eBrary Resource</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Adanced Java</strong></u></div>
                                    
                                    <div><a href="http://site.ebrary.com.db29.linccweb.org/lib/valencia/detail.action?docID=10882080&amp;p00=java+7">eBrary Resource</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u>ECONOMICS<a name="EconomicsVirtual" id="EconomicsVirtual"></a></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Intro to Economics </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8JYP_wU1JTU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2YULdjmg3o0">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>U.S. Economy </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PHe0bXAIuk0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=G6iRy-AISZs">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=mXxp4WO4cTw">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MICROECONOMICS</strong></u></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Supply, Demand &amp; Market Equilibrium </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PEMkfgrifDw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=NgPqyM3I_8o">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8-yWKgZv9JY">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RP0j3Lnlazs">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=0yWsOZgsTSY">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=op70yS_7du8">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sB7XFPy_bZM">Video 7</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Elasticity</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=slP8XZ6Nq40">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-b7xlINQ-zg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4oj_lnj6pXA">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Consumer &amp; Producer Surplus </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_6kwhF6hoqQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-V-Y5klejSg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=E_Guf6HxgBQ">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=R_AUMDHqfbg">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Scarcity, Possibilites, Preferences &amp; Opportunity Cost </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qZbvMtIy_kE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-A916v81lYQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=lDvkPe_J7S8">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_7VHfuWV-Qg">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=knwrcbUZSAw">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dyb4WbACBsg">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=LjXqeHHtx40">Video 7</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=qJl3FlyT7Tw">Video 8</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pkEiHZAtoro">Video 9</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yReZ4xdg5bw">Video 10</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Production Decisions &amp; Economic Profit </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=IqvoxkBAlEw">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=V_InI4S3fZ4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=06j_zPdPWOY">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=a0nUWrnuUdo">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RUVsEovktGU">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Forms of Competition </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=_NznT4Cn8As&amp;list=PLrmp80oofTJ_bo1upkdujGcEJZLpsvA8D">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Game Theory &amp; Nash Equilibrium </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UkXI-zPcDIM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Jo5aAz9UWB0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3Y1WpytiHKE">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MACROECONOMICS</strong></u></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>GDP: Measuring National Income </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Rgr1vRjxzFg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BJZ_H4NG1nw">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ZdGnhusKnRU">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hRahNK1RsRU">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RsPJ1ZhGk-c">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Inflammation: Measuring Cost of Living </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AaR1mPrdbTc">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3Jhr15BbGa0">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5-cWoqTJAfg">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Aggregate Demand and Supply </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=oLhohwfwf_U">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=scN-1B6plos">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3nbalsyibKU">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=8W0iZk8Yxhs">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=9GVUv-BJccc">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hTWPrWmPJS0">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=em5Wqg1IVp8">Video 7</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>The Monetary System</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ntxMOKXHlfo">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_dNIDo8UFSc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hZoDN3Skp84">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Income &amp; Expenditure: Keynesian Cross and IS-LM Model </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Z9b8nnvpim0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Hfz1bwK5C4o">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=hPkh8kOldU4">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sTw0e-hwYAQ">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aSY8XPGChAU">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=AW3bPaErUWU">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pzQnc_0eZA8">Video 7</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yro2jLBfyDQ">Video 8</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Q3hoC8lDCBo">Video 9</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Foreign Exchange &amp; Trade </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=itoNb1lb5hY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UnVIEX1P2IE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HRIrMdeWtmc">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xwtgByffoUw&amp;list=PLPNKx7fghye6n6obt9O1fhWjflTY3gkXO">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XWVZXn_V7f4">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=D41EuDh3epI">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uU5NmUnQyZE&amp;list=PLUGfsFdIRpPEgPXI4Du_W2QX3YGW2QVaX">Video 7</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xx9xNJlPOJo">Video 8</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uU5NmUnQyZE">Video 9</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vxPZYWoQnyc">Video 10</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u>MUSIC THEORY <a name="MusicTheory" id="MusicTheory"></a></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=rekpZCji-d0&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=1">Chromatic Scale</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=U8e3JulxGy0&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=2">Intervals</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=2blepsIifz0&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=3">Major Scales</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=rRU9AbsOL1Q&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=4">Minor Scales</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=AmcZAgYDiyE&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=5">Keys</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=8qSpKZgSfQg&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=6">Triads</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=rakB5AKEIyA&amp;list=PLRgvqVevy6iwCe-ne3sYxuYxyfOz_GjpA&amp;index=7">Cadence</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=leIpJWeWYfA">Reading Music</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=ylk7F7a1AyI">Circle of 5ths</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=c3ZWejiKrNc">Time Signatures &amp; Rhythms</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=LsnB0Arie2I">Chord Qualities</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MUSIC THEORY 2 </strong></u></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=8Db4wlA8az0">Dominant 7th Chords</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=xfYqH2zrQxc">Inversions</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=ug95Rw9WF5w">Basic Harmony Analysis</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=gD--DG_cYJY">Non-Chordal Tones</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=X58x9FB4mtw">Use of Harmonic Minor</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=a7lyb1xq1ZA">Leading Tones</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MUSIC THEORY 3 </strong></u></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=hyhc-1XdjMg">Analysis</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=6akTZfgtLCI">Secondary Dominants</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=8Db4wlA8az0">Dominished Seventh Chord</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=kAy_c2SD_Uo">Borrowed Chords</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=gAQ4cY9FlzA">Neopolitan 6</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=RMJKRzxEMeM">Augmented 6th</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=v-eTWwFkIl4">Basic Modulation</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>MUSIC THEORY 4 </strong></u></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=ugFQ-94Fmh0">Advanced Analysis</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=8CWAgGidAAg">Intro to 20th Century Music</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=whN5PXsrP6E">Jazz + Blues</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=qaCQjAKL6wg">Non-Functional Harmony</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=lQ8RwDEabC8">Chordal Extensions</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u>PHYSICS<a name="Physics" id="Physics"></a></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Vectors</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ihNZlp7iUHE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=u4gbA9qC-UU">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zyLItU1DrnY">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Kinematics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wQupPFO0P8M">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BQkZ4dqveHI">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UefWw5k4G0U">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=oVtUZG_pcmA">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Dynamics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=MRjG4O-b6HY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Cdpoo2XM6Hg">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=i2bGTC27OJU">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Work, Energy &amp; Power </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pDK2p1QbPKQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Lm7UL0XU74U">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Z42ZRC-iwtg">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=t3Wd8Mr5xAI">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Linerar Momentum </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jyEx3lFzasA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=RKERiyi2mlk">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=XFhntPxow0U">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Rotational Motion</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YwnpIhCIC_0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=grMWAI1RdVs">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3lp7fyqNlu8">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Oscillations</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VP4Wv2vHgow">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=2lzVuyR7TOE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sotx0iweVrQ">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=WZ7dakNZAc8">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Gravitation</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yhG_ArxmwRM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=391txUI76gM">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sYaeBZIGc4I">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ySojsYELMss">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Thermodynamics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VtEqn-5XHpU">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=tQcB9BLUoVI">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=x34OTtDE5q8">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xdRtWK1_2Eo">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Magnetic Forces &amp; Fields </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=dFT7-_s0jh0">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rpS21LB3Ego">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=uj0DFDfQajw">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rQZmVE1eMrM&amp;list=PLFiglG5bh60AqXiEEqW4vwxnEGaV7-hDb">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OU2WN7QNq6k">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Optics</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_LTffEMF5qI">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=P5Y6rmPpGo4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=69NPUCIKWlg">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aDhY8jfCJa8">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=FkFBAJksvXs">Video 5</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=3XfJxHkiiGs">Video 6</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PgLSAUAowZA">Video 7</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=PgLSAUAowZA">Video 8</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=yMO7Lsjhmi4">Video 9</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ORUGwPP-mb4">Video 10</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Special Relativity </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ev9zrt__lec">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=30KfPtHec4s">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ajhFNcUTJI0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KYWM2oZgi4E">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h1><u>POLITICAL SCIENCE<a name="USgovt" id="USgovt"></a></u></h1>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>American Policital Culture </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gbddciQiYdg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=pI6ndfyQwcQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=HGsOE8Ra2nU">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>US Constitution </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=OSWl5ldEv6w">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=0NVVjIriFeE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=GiahGIKTl00">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=bO7FQsCcbD8">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Federalism</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=TbiiKfBW_s4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ie6mvIDU2y4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_96DN67nxQQ">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Congress</strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=E1CIWwu6KdQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=FhVeIOy99f4">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>The Presidency </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=MjVJpMdc1cM">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ok_VQ8I7g6I">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4YRIdPqTac8">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>The Bureaucracy </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=wt5fHUcXpQE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=7ofdNxgLqHU">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>The Judiciary </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UG0ZaAVF_i4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=I7qJBQPMqLc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=UDqc2it41-4">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Political Parties </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Fu7l_ALHpQo">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=4FHqowXS4j4">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=VH46Ogc3UN0">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=gdgkK-01wms">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Interest Groups </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=xNlfwrF-hLA">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=tUtiBMiicnQ">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=5HM40BuJYss">Video 3</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>The Media </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=Vm5Zgkwnp_g">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Civil Liberties &amp; Civil Rights </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jYOOUqBmfnY">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=aJoEG9AwhZE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ip4GpGFrjG4">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=vMbhJbDf7uQ">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=S64zRnnn4Po">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Public Policy </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=-dAflRiw88E">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KWpRDkEZMQw">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Foreign Policy </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=W-Ep3svdck4">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=zs9Rl5eJiJI">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=i596vp8aWgM">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=iB-qkupAFaI">Video 4</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=NVCDnUZqLzU">Video 5</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Intro to Political Science </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=jQK0Xbfel-M">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=Xhjl-MvnqAc">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=CoduoEs4L0Q">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=rQq755KIOeE">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Politics &amp; Political Science </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=r161cLYzuDI">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong><u>Political Ideologies &amp; Styles</u> </strong></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=YMTRW3BRJHE">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=KYOKtvMOpLA">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Nations &amp; States </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=sa-41DBGHfw">Video 1</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Political Economy </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=omhu-eHOYrQ">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=_Ys_simReoE">Video 2</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>Political Culture &amp; Public Opinion </strong></u></div>
                                    
                                    <div>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=BcU8elFl2Bg">Video 1</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=sK4_aeolWAE">Video 2</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=tVpKgMCtukk">Video 3</a></p>
                                       
                                       <p><a href="https://www.youtube.com/watch?v=ubR8rEgSZSU">Video 4</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><u><strong>International Politics </strong></u></div>
                                    
                                    <div><a href="https://www.youtube.com/watch?v=y32cFdicW1U">Video 1</a></div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="index.html">
                                          Academic Success Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2540</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7:00 am - 10:00 pm <br>Friday: 7:00 am - 7:00 pm<br>Saturday: 8:00 am - 4:00 pm 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <a href="events.html">ASC Events &amp; News</a>
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>         
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook"><i></i></a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter"><i></i></a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram"><i></i></a>
                           
                           
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube"><i></i></a>  
                           
                        </div>  
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/virtual-library.pcf">©</a>
      </div>
   </body>
</html>