<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/testing/faculty-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/testing/">Testing</a></li>
               <li>Testing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/testing/index.html"></a>
                        
                        
                        
                        
                        
                        <h2>Faculty Resources </h2>
                        
                        <h3>"Experience is the worst teacher. It always gives the test first and the instruction
                           afterward." - Anonymous 
                        </h3>
                        
                        <p>Valencia’s faculty may utilize the Testing Center for proctoring: </p>
                        
                        <ul>
                           
                           <li>Traditional paper tests</li>
                           
                           <li>Online/computer exams</li>
                           
                           <li><a href="../../academic-success/testing/documents/RevisedValenciaTestingCenterBestPractices-1.12.17.pdf">Testing Center Best Practices</a></li>
                           
                        </ul>
                        
                        <h3>Testing Center Memorandum</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Faculty Resources </strong></p>
                                    
                                    <p>Proctoring Services include: </p>
                                    
                                    <ul>
                                       
                                       <li>Traditional paper tests</li>
                                       
                                       <li>Online/computer exams</li>
                                       
                                    </ul>
                                    
                                    <p><strong>Quick Tips for testing with us!</strong> 
                                    </p>
                                    
                                    <p>Submit <em>current Testing Center Referrals</em> with each exam at least one business day prior to the exam start date and time. You
                                       can access testreferrals at our front desk or in your ATLAS account in the <em>faculty</em> tab under <em>faculty tools</em>.
                                    </p>
                                    
                                    <p>Don’t forget to <em>attach a class roster</em> with each exam referral if you’d like us to administer your exam to more than five
                                       (5) students. 
                                    </p>
                                    
                                    <p>Please place your <em>name</em>, <em>preferred return method</em> and <em>time limit</em> (if applicable) on each paper exam, table, chart, and answer sheet you provide. That’s
                                       how we know who they belong to and how to get them back to you! 
                                    </p>
                                    
                                    <p>All <em>final exams</em> should be received by us at least two (2) business days before their start date.
                                       
                                    </p>
                                    
                                    <p><strong><em>Dean approval is required for all</em></strong><strong> <em>face to face</em> <em>entire class</em> <em>final exam</em> <em>submissions</em><em>.</em></strong> Approvals can be requested through the testing center referral form in your ATLAS
                                       account. Simply check the “Dean Approval” box and select your department if sending
                                       us an exam electronically. Your dean should then email us direct with their approval
                                       at <a href="mailto:eac-testingcenter@valenciacollege.edu">eac-testingcenter@valenciacollege.edu</a>. 
                                    </p>
                                    
                                    <p>Please remember, if you have a paper exam and need more than ten (10) copies for students,
                                       we ask that you use Word Processing services to make these copies.To place a Word
                                       Processing order, please click the following link<a href="http:/valenciacollege.edu/wordprocessing/">: http://valenciacollege.edu/wordprocessing/</a> 
                                    </p>
                                    
                                    
                                    <p><strong>Important information to share with your students:</strong></p>
                                    
                                    <p>The testing center requires student identification and accepts these forms: </p>
                                    
                                    <ul>
                                       
                                       <li>
                                          <em>Current Valencia College Identification cards</em> 
                                       </li>
                                       
                                       <li>
                                          <em>Current government issued ID (student must still be able to provide his/her VID number)</em> 
                                       </li>
                                       
                                       <li>
                                          <em>Current U.C.F. ID</em>. 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p>Upon arrival at the testing center, students should be prepared to provide:</p>
                                    
                                    <ul>
                                       
                                       <li>Their professor’s name </li>
                                       
                                       <li>The course in which they are testing </li>
                                       
                                       <li>The exam information, i.e., (name, number) </li>
                                       
                                       <li>
                                          <em>Cellular phones and smart watches are to be powered off and stowed away in a student’s
                                             personal bag or a blue testing center bag provided by testing staff when inside the
                                             testing center.</em> Mobile alarms should be turned off as well. 
                                       </li>
                                       
                                       <li>When a phone rings inside the testing center the student is asked to stop testing
                                          and sign out. Our staff then prepare an academic dishonesty report and sent it to
                                          you and your dean. 
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong><em>We’re a first-come, first-serve site</em></strong>! Seating is limited and, therefore, not guaranteed. 
                                    </p>
                                    
                                    
                                    <p><em>Extended wait times</em> may be experienced, especially during peak testing periods. Therefore, students should
                                       allot themselves ample time to take and complete their exams.
                                    </p>
                                    
                                    <p><strong>Reminder: Please advise students to arrive at the Testing Center at least one hour
                                          prior to closing. Tests will <u>not</u> be administered within the hour before the Testing Center closes.</strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong><a href="../../academic-success/testing/facultyresources.html">TOP</a></strong></p>
                        
                        <h3>Room Reservations </h3>
                        
                        <p><u><strong>Computer Labs:</strong></u></p>
                        
                        <ul>
                           
                           <li>Before reserving Room 4-122, please check other available computer labs on campus
                              (i.e. 2-305, 2-305A, 2-305B, 6-226, 8-221, 8-243, or 8-244). 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>You may check the availability of the aforementioned computer labs by using Valencia's
                              Room Request Form <strong>: </strong><strong><a href="http://valenciacollege.edu/facultystaff/room-request.cfm">Room Request</a></strong> 
                           </li>
                           
                        </ul>
                        
                        <p><u><strong>Testing Computer Lab (4-122) Schedule &amp; Reservations:</strong></u></p>
                        
                        <ul>
                           
                           <li>The Testing Computer Lab is available for presentations and classroom discussions
                              that require computer use of up to 28 students. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li><strong>Please only reserve this room if computer use is the primary need for the classroom
                                 discussion. </strong></li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <strong>Keep in mind that the Testing Center is the only location on East Campus for students
                                 to complete exams. It is the responsibility of the instructor(s) </strong><strong>to help maintain the testing atmosphere by facilitating a quiet classroom session
                                 when using the room.</strong>
                              
                           </li>
                           
                        </ul>
                        
                        <p>To check availability of the Testing Center Computer Lab, click here: <a href="http://eastlrc.valenciacollege.edu/scc/showrequests.cfm">View Calendar</a></p>
                        
                        <p>To reserve the Testing Center Computer Lab, click here: <a href="http://eastlrc.valenciacollege.edu/scc/addrequest.cfm">Room Reservation</a></p>
                        
                        <p><a href="../../academic-success/testing/facultyresources.html">TOP</a></p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/testing/index.html">
                                          Academic Success Center - Testing Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 124</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2428</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7am - 9pm<br>Friday: 7am - 6pm<br>Saturday: 8am - 3pm<br>Sunday: Closed 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>       
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook">
                              <i></i>
                              </a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter">
                              <i></i>
                              </a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram">
                              <i></i>
                              </a>
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube">
                              <i></i>
                              </a>  
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/testing/faculty-resources.pcf">©</a>
      </div>
   </body>
</html>