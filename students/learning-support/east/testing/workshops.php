<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/testing/workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/testing/">Testing</a></li>
               <li>Testing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/testing/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Fall 2015 Workshops</h2>
                        
                        <p>Attend a <span>FREE Workshop</span> to boost your test taking and academic SKILLS!&nbsp; <br>
                           To sign up for a workshop, please USE THE&nbsp;<a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/testing/workshopsign-up.cfm" target="_blank"><u>electronic FORM.</u></a> Seats are limited, so sign-up today!&nbsp;
                        </p>
                        
                        <h3>January</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Monday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Tuesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Wednesday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Thursday</div>
                                    </div>
                                    
                                    <div>
                                       <div>Friday</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p><br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          <br>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>1 <br>
                                       <strong>School Closed<br>
                                          Holiday </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>2</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>3<strong></strong> 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>6<strong> <br>
                                                Classes Begin </strong></p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>7<strong></strong> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>8</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>9</p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>10<strong> </strong>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>13<strong> </strong>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>14 <br>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>15</div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>16 <br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>17 </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>20<br>
                                       <strong>School Closed<br>
                                          Holiday </strong>
                                       
                                    </div>
                                    
                                    <div>
                                       <div>21<strong><br>
                                             </strong> 
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>22<br>
                                             
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>
                                          
                                          <p>23<strong><br>
                                                </strong> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                    <div>
                                       <div>24<strong> <br>
                                             </strong>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>27<strong><br>
                                          </strong>
                                       
                                    </div>
                                    
                                    <div>28</div>
                                    
                                    <div>
                                       <p>29<strong><br>
                                             </strong></p>
                                    </div>
                                    
                                    <div>
                                       <p>30<strong><br>
                                             </strong><a href="http://net4.valenciacollege.edu/forms/east/academicsuccess/testing/workshopsign-up.cfm" target="_blank">Study Skills</a><br>
                                          1:00pm - 2:00pm<br>
                                          7-125 <strong><br>
                                             </strong></p>
                                    </div>
                                    
                                    <div>
                                       <p>31<br>
                                          
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <div>
                           
                           <p><a href="../../academic-success/testing/workshops.html">TOP</a></p>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/testing/index.html">
                                          Academic Success Center - Testing Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 124</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2428</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7am - 9pm<br>Friday: 7am - 6pm<br>Saturday: 8am - 3pm<br>Sunday: Closed 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>       
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook">
                              <i></i>
                              </a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter">
                              <i></i>
                              </a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram">
                              <i></i>
                              </a>
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube">
                              <i></i>
                              </a>  
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/testing/workshops.pcf">©</a>
      </div>
   </body>
</html>