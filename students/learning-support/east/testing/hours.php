<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/testing/hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/testing/">Testing</a></li>
               <li>Testing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/testing/index.html"></a>
                        
                        
                        <h2><img alt="testing" height="110" src="../../academic-success/testing/TestingBanner_005.jpg" width="582"></h2>
                        
                        
                        
                        <p><em>The Testing Center recommends that students arrive 15 minutes prior to the start of
                              their exam.</em></p>
                        
                        <h3>Spring and Fall Hours</h3>
                        
                        
                        <div>
                           
                           <p><strong>Monday - Thursday</strong><br>
                              7:00 AM - 9:00 PM 
                           </p> 
                           
                           <p><strong>Friday</strong><br>
                              7:00 AM - 6:00 PM
                           </p>
                           
                           <p><strong>Saturday</strong><br>
                              8:00 AM - 3:00 PM 
                           </p>
                           
                        </div>
                        
                        
                        <h3>Summer Hours (May 11 - July 18, 2015) </h3>
                        
                        <div>
                           
                           <p><strong>Monday - Thursday</strong><br>
                              7:00 AM - 9:00 PM 
                           </p>
                           
                           <p><strong>Friday</strong><br>
                              7:00 AM - 11:00 AM
                           </p>
                           
                           <p><strong>Saturday</strong><br>
                              8:00 AM - 1:00 PM 
                           </p>
                           
                        </div>
                        
                        <h3>Summer Hours (July 19 - August 4, 2015) </h3>
                        
                        <div>
                           
                           <p><strong>Monday - Thursday </strong><br>
                              7:00 AM - 9:00 PM 
                           </p>
                           
                           <p><strong>Friday</strong><br>
                              7:00 AM - 6:00 PM
                           </p>
                           
                           <p><strong>Saturday</strong><br>
                              8:00 AM - 1:00 PM 
                           </p>
                           
                           <h3>&nbsp;</h3>
                           
                        </div>
                        
                        
                        <h3>Location</h3>
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/testing/index.html">
                                          East Campus 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 4 , Rm 124</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2428</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7am - 9pm<br>Friday: 7am - 6pm<br>Saturday: 8am - 3pm<br>Sunday: Closed 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <p>Updated 1/15/2015</p>            
                        
                        
                        <ul>
                           <li>View a <a href="../location.html">map and directions</a>
                              
                           </li>
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/testing/hours.pcf">©</a>
      </div>
   </body>
</html>