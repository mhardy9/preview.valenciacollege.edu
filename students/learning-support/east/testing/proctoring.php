<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing at the Academic Success Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/testing/proctoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing at the Academic Success Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li><a href="/students/learning-support/east/testing/">Testing</a></li>
               <li>Testing at the Academic Success Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../academic-success/testing/index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Need Proctoring? </h2>
                        
                        <h3><em>"Experience is the worst teacher. It always gives the test first and the instruction
                              afterward" - unknown</em></h3>
                        
                        <p>The East campus Testing Center can proctor exams for any distance learning programs
                           from other institutions or state agencies during our regular hours. Each test proctored
                           costs $25. 
                        </p>
                        
                        <p>To have an exam proctored:</p>
                        
                        <ol>
                           
                           <li> Submit a Proctor Request</li>
                           
                           <li>Provide your university/instructor with our site information.  </li>
                           
                           <li>Come to the East campus testing center in building 4, room 124 and pick up  your fee
                              slip. 
                           </li>
                           
                           <li>Take your fee slip to the East campus business office in building 5, room 214  to
                              pay your proctor fee(s). 
                           </li>
                           
                           <li>Bring your receipt to the testing center to take your test.                    </li>
                           
                        </ol>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/testing-center/distance-learner-proctor-request.cfm" target="_blank">Proctor Request</a></p>
                        
                        
                        <p>As the student, you are responsible for all proctoring fees unless  your college/school/business
                           pays them for you. 
                        </p>
                        
                        <p><strong>All exams must be submitted directly from  faculty or testing divisions. </strong></p>
                        
                        <h3>Site Information</h3>
                        
                        <h4>Valencia College - East  Campus</h4>
                        
                        <p> <strong>Testing Center, Building 4, Room 124</strong><br>
                           701 N. Econlochatchee Trail<br>
                           Orlando, FL 32825
                        </p>
                        
                        <p>Interoffice Mail Code: 3-18</p>
                        
                        <p><strong>Primary E-mail:</strong> <a href="mailto:eac-testingcenter@valenciacollege.edu">eac-testingcenter@valenciacollege.edu</a><br>
                           <strong>Office Phone:</strong> 407.582.2428 <br>
                           <strong>Office Fax:</strong> 407.582.8916 
                        </p>
                        
                        <p>Alison M. Langevin<br>
                           Lab Supervisor - Testing<br>
                           Phone: 407.582.2484<br>
                           E-Mail: <a href="mailto:alangevin@valenciacollege.edu">alangevin@valenciacollege.edu</a>          
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3>      
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../academic-success/testing/index.html">
                                          Academic Success Center - Testing Center 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Building 4 Room 124</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2428</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7am - 9pm<br>Friday: 7am - 6pm<br>Saturday: 8am - 3pm<br>Sunday: Closed 
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>          
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>Social Media</div>       
                           <a href="https://www.facebook.com/ValenciaAcademicSuccessCenter" target="_blank" title="Find us on Facebook">
                              <i></i>
                              </a>
                           
                           <a href="https://twitter.com/valencia_asc/" target="_blank" title="Follow us on Twitter">
                              <i></i>
                              </a>
                           
                           <a href="https://www.instagram.com/valencia_asc/" target="_blank" title="Instagram">
                              <i></i>
                              </a>
                           <a href="http://www.youtube.com/channel/UC5ZON8ZhpOcDzP3XrmNtdKQ" target="_blank" title="YouTube">
                              <i></i>
                              </a>  
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/testing/proctoring.pcf">©</a>
      </div>
   </body>
</html>