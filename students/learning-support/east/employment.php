<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Success Center, East Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/east/employment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/east/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Academic Success Center, East Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/east/">Academic Success East</a></li>
               <li>Academic Success Center, East Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p>
                           <a name="content" id="content"></a>
                           <a href="index.html"></a></p>
                        
                        
                        
                        <h2><strong>Employment Opportunities in the Academic Success Center </strong></h2>
                        
                        <h3><strong>Featured Part-time Job Listings</strong></h3>
                        
                        <h3><strong>Tutoring &amp; Staff positions:</strong></h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>TITLE</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>AREA</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>LINK</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div>APPLY BY DATE </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>Foreign Languages Lab </div>
                                 </div>
                                 
                                 <div>
                                    <p><a href="https://valenciacollege.csod.com/ats/careersite/jobdetails.aspx?site=1&amp;c=valenciacollege&amp;id=2841">Apply Here</a></p>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP Lab </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3506">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>Communications Center </p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div> <a href="https://valenciacollege.csod.com/ats/careersite/jobdetails.aspx?site=1&amp;c=valenciacollege&amp;id=2843">Apply Here</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>Math Center </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3200">Apply Here</a> 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Office Aide I </div>
                                 </div>
                                 
                                 <div>
                                    <div>Welcome Desk </div>
                                 </div>
                                 
                                 <div>
                                    <div> <a href="https://valenciacollege.csod.com/ats/careersite/jobdetails.aspx?site=1&amp;c=valenciacollege&amp;id=3084">Apply Here</a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Office Aide II </div>
                                 </div>
                                 
                                 <div>
                                    <div>Communications Desk </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/jobdetails.aspx?site=1&amp;c=valenciacollege&amp;id=3085">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Accounting and Economics </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3633">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: American Sign Language </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3668">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Computer Programming </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3634">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Political Science </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3635">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Science </div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="https://valenciacollege.csod.com/ats/careersite/JobDetails.aspx?id=3576">Apply Here</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>OPEN</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Music Theory </div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>NOT CURRENTLY HIRING </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>Tutor</div>
                                 </div>
                                 
                                 <div>
                                    <div>GT: Office Systems Technology</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>NOT CURRENTLY HIRING</strong></div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <h3>How to Apply</h3>
                        
                        <p>In order to apply, find the position you're interested in listed above and select
                           the approptiate "Apply Here" link. 
                        </p>
                        
                        <p> For consideration, all documents must be uploaded at the time of application. This
                           could include: resume, cover letter, all letters of reference, or any other applicable
                           documents related to this position. Once you have completed the application process,
                           no other documents may be uploaded or altered.
                        </p>
                        
                        <p>Completed applications will be collected and kept on file for six months. Once the
                           time period has passed, the candidate must initial the original application for it
                           to remain valid longer than six months.&nbsp; Applications without initials left on file
                           for more than six months will be shredded.
                        </p>
                        
                        <p>For information regarding East Campus Supplemental Learning Leaders, contact <a href="mailto:jmccormick5@valenciacollege.edu">Jennifer Adams</a>.
                        </p>
                        
                        <p><a href="employment.html">TOP</a></p>
                        
                        <p>Descriptions of the Areas in the Academic Success Center</p>
                        
                        <p><u><strong>ASC Welcome Desk:</strong></u> the ASC Welcome Desk is a centralized reception station for the entirety of the ASC.
                           Welcome Desk staff assist students with ASC-related questions, assist staff with ASC-related
                           tasks, and check out materials to students, staff, and faculty. For more information
                           about the ASC Welcome Desk, please click&nbsp;<a href="../academic-success/welcomedesk/index.html"><u>here</u></a>.
                        </p>
                        
                        <p><strong><u>Testing Center</u></strong><u>:</u> the Testing Center provides administrative and security maintenance procedures for
                           all types of testing. The Testing Center specializes in academic, Independent Study,
                           individualized learning (also known as self-paced), as well as computer and web based
                           examinations.  For more information about the Testing Center, please click&nbsp;<a href="../academic-success/testing/index.html"><u>here</u></a>.
                        </p>
                        
                        <p><span><strong><u>Communications Center:</u></strong></span> the Communications Center focuses on assisting students with reading, writing, and
                           speech assignments from any discipline. Consultants in the Communications Center help
                           students to improve their overall communications skills during 30 minutes consultations.
                           For more information about the Communications Center, please click <a href="../academic-success/writing/default.html">here</a>.
                        </p>
                        
                        <p><span><strong><u>Math  Center:</u></strong></span> the Math  Center assists students with college-level and developmental math skills.
                           Math Center tutors support students with understanding coursework and preparing for
                           exams by utizing a variety of resources. The Math Center also provides topic-based
                           workshops to further support students' development of core math competencies.  For
                           more information about the Math Center, please click <a href="../academic-success/math/default.html">here</a>. 
                        </p>
                        
                        <p><u><strong>Foreign Language Lab</strong><strong>:</strong></u> The Foreign Language Lab assists students enrolled in foreign language courses such
                           as Spanish, French, and German. Tutoring and workshops are available to help students
                           grasp core concepts in order to become successful in their foreign language courses.
                           For more information about the Foreign Language Lab, click <a href="../academic-success/language/index.html">here</a>. 
                        </p>
                        
                        <p><u><strong>E</strong></u><u><strong>AP Lab:</strong></u>&nbsp;The EAP Lab provides a classroom and learning space for non-native speakers of English
                           as they practice English communication, pronunciation, grammar, reading, and writing
                           skills. Services in the EAP Lab also include instructor-led, interactive workshops.
                           For more information about the EAP Lab, please click&nbsp;<a href="eap/index.html"><u>here</u></a>. 
                        </p>
                        
                        <p><span><strong><u>General Tutoring Center</u>:</strong></span> General Tutoring is provided for all other disciplines including science, music,
                           American Sign Language, office systems technology, computer science, accounting, economics,
                           and government. For more information regarding the General Tutoring Center, please
                           click <a href="../academic-success/tutoring/index.html">here</a>.
                        </p>
                        
                        <p><a href="employment.html">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/east/employment.pcf">©</a>
      </div>
   </body>
</html>