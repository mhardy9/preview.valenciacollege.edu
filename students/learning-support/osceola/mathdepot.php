<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Math Department | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/mathdepot.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Centers</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Math Depot</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     				
                     <h2>Math Depot</h2>
                     				
                     <p>Are you looking for a way to improve test grades, needing some help with homework,
                        or just not understanding a concept that’s being taught in class? You’ve come to the
                        right place! The  Depot can provide alternative learning resources to help you understand
                        a concept via online learning modules, helpful tips, study sessions, and one-on-one
                        help from our staff. Assistance is also available for  MAT0018 and MAT0028 exams because
                        we  provide additional resources to help you study. 
                     </p>
                     				
                     <h3>Social Media</h3>
                     				
                     <p><a href="https://twitter.com/MathDepot" target="_blank"><span class="fab fa-twitter-square fa-fw fa-2x"></span>Follow @MathDepot</a></p>
                     				
                     <p><a href="https://www.facebook.com/osceola.mathdepot"><span class="fab fa-facebook-square fa-fw fa-2x"></span>Find Us on Facebook</a></p>
                     				
                     <p><a href="http://instagram.com/mathdepot?ref=badge"><span class="fab fa-instagram fa-fw fa-2x"></span>Find Us on Instagram</a></p>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4 col-sm-4"> 
                           						
                           <h3>Help for Courses</h3>
                           						
                           <ul>
                              							
                              <li>MAT0018C Developmental Math I </li>
                              							
                              <li>MAT0022C Developmental Math Combined </li>
                              							
                              <li>MAT0028C Developmental Math II</li>
                              							
                              <li>MAT1033C Intermediate Algebra </li>
                              							
                              <li>STA1001C Statistical Reasoning </li>
                              							
                              <li>PERT Preparation</li>
                              						
                           </ul>
                           					
                        </div>
                        
                        					
                        <div class="col-md-4 col-sm-4">
                           						
                           <h3>Resources</h3>
                           						
                           <ul>
                              							
                              <li><a href="documents/AccesstoMAT1033CARRMSpring16.pdf">MAT1033 Intermediate Algebra Readiness </a></li>
                              							
                              <li><a href="http://youtube.com/mathdepot">Math Depot YouTube Channel</a></li>
                              							
                              <li><a href="http://youtube.com/osceolatutoring">Osceola Tutoring Learning on Demand</a></li>
                              							
                              <li><a href="ArchivedStudySessions.php">Archived Study Sessions</a> 
                              </li>
                              						
                           </ul>
                           
                           					
                        </div>
                        
                        					
                        <div class="col-md-4 col-sm-4">
                           						
                           <h3>Location &amp; Hours</h3>
                           						<span class="far fa-building fa-fw"></span> Building 4 Room 121<br>
                           						<span class="far fa-envelope fa-fw"></span> Email: <a href="mailto:osceolamathdepot@valenciacollege.edu"> Math Depot</a><br>
                           						<span class="far fa-phone fa-fw"></span> Phone: 321-682-4146<br>
                           						<span class="far fa-clock fa-fw"></span> Monday - Thursday: 8:00am - 8:00pm<br>
                           						<span class="far fa-clock fa-fw"></span>Friday: 8:00am - 5:00pm<br>
                           						<span class="far fa-clock fa-fw"></span>Saturday: 8:00am - 12:00pm<br>
                           						<span class="far fa-clock fa-fw"></span> Sunday: Closed
                           					
                        </div>
                        				
                     </div>
                     				
                     <h3>Meet Our Staff</h3>
                     
                     				
                     <ul class="list_staff">
                        					
                        <li>
                           						
                           <figure><img src="images/johnie-forsythe.jpg" alt="Johnie Forsythe Picture" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:jforsythe3@valenciacollege.edu">Johnie Forsythe</a></h4>
                           						
                           <p>Math Coordinator</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/dan-derosa.jpg" alt="Dan DeRosa Picture" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:dderosa1@valenciacollege.edu">Dan DeRosa</a></h4>
                           						
                           <p>Senior Lab Supervisor</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/enaud-celestin.jpg" alt="Enaud Celestin Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:ecelestin@valenciacollege.edu">Enaud Celestin </a> 
                           </h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/khalid-lazaar.jpg" alt="Khalid Lazaar Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:klazaar@valenciacollege.edu">Khalid Lazaar</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/sergio-estrella.jpg" alt="Sergio Estrella Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:sestrella@valenciacollege.edu">Sergio Estrella</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/john-fagiolino.jpg" alt="John Fagiolino Photo" class="img-circle"></figure>
                           						
                           <h4> <a href="mailto:gfagiolino@valenciacollege.edu">John Fagiolino</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/zaheer-black.jpg" alt="Zaheer Black Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:zblack@valenciacollege.edu">Zaheer Black</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:pkinsella@valenciacollege.edu">Patrick Kinsella</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:tsheppard@valenciacollege.edu">Tamara Sheppard</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/diana-budach.jpg" alt="Diana Budach Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:dbudach@valenciacollege.edu">Diana Budach</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/faulys-ponceano.jpg" alt="Faulys Ponceano Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:fponceano@valenciacollege.edu">Faulys Ponceano</a></h4>
                           						
                           <p>Instructional Lab Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/vanina-achille.jpg" alt="Vanina Achille Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:vachille@mail.valenciacollege.edu">Vanina Achille</a></h4>
                           						
                           <p>Senior Curriculum Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/emmanuel-vargas.jpg" alt="Emmanuel Vargas Photo" class="img-circle"></figure>
                           						
                           <h4><a href="mailto:evargas24@valenciacollege.edu">Emmanuel Vargas </a></h4>
                           						
                           <p>Curriculum Assistant</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/rebecca-barnett.jpg" alt="Rebecca Barnett Photo" class="img-circle"></figure>
                           						
                           <h4>Rebecca Barnett</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Rosemary Ramirez</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/jean-delot.jpg" alt="Jean DeLot Photo" class="img-circle"></figure>
                           						
                           <h4>Jean DeLot</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="images/magali-diaz.jpg" alt="Magali Diaz Photo" class="img-circle"></figure>
                           						
                           <h4>Magali Diaz</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Jasmine Estrella</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Christy Rivera</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Angelica Lopez</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Elizabeth Sanchez</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Brian Iwai</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Alex Nunez</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Michaela Barnett</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Jonhoi Smith</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Emily Walker</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Darean Flores-Caban</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-male-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Devan Cantrall</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        					
                        <li>
                           						
                           <figure><img src="/_resources/img/no-photo-female-thumb.png" alt="No Photo Available" class="img-circle"></figure>
                           						
                           <h4>Yaneliz Perez-Rodriquez</h4>
                           						
                           <p>Math Depot Expert</p>
                           					
                        </li>
                        
                        				
                     </ul>
                     				
                     <p>If you have questions or would like more information about the Math Depot, you can
                        contact us by us by <a href="mailto:osceolamathdepot@valenciacollege.edu">email</a> to speak to any of our friendly staff.
                     </p>
                     			
                  </div>
                  		
               </div>
               			
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/mathdepot.pcf">©</a>
      </div>
   </body>
</html>