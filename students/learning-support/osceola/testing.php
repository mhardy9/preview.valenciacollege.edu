<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/testing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Testing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/testing/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Important Information for Students</h3>
                        
                        <p><span>Students must have their Valencia ID cards or government-issued  identification for
                              testing purposes. Tests  are filed by the instructor's last name, so be sure you know
                              the instructor's name  and which test you need to take. Once you receive your test,
                              you will not be allowed to leave the  Testing Center. All electronic devices  (cell
                              phone, mp3 player, PDA, tablet, laptop) will need to be turned off during the test
                              session. For tests which allow notes, the  notes must be hard copies – you may not
                              use notes stored on an electronic  device. If you are taking an online test, you will
                              be required to use the Testing Center's computers and will not be allowed to use your
                              own laptop computer. Once you hand a test in, it cannot be returned to you for any
                              reason.&nbsp;</span></p>
                        
                        <h3>Important Information for Non-Valencia Students</h3>
                        
                        <p> Proctor Students from other institutions must have a valid photo ID. Please be sure
                           to bring your proctor test payment receipt with you in order to test unless your institution
                           pays the fees for you (e.g., WGU students). All of the above rules and hours apply
                           to Proctor Students.<br>
                           <br>
                           All  Academic Dishonesty policies of Valencia College will be fully enforced. 
                        </p>
                        
                        
                        <p><strong>**Identification REQUIRED for all Tests**</strong></p>
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <a href="../../../locations/osceola/testing/index.html">
                                          Osceola Campus 
                                          </a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-4149 </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday 8:00am - 9:00pm<br>Tests will not be given out after 8 pm<br>Friday 8:00am - 5:00pm<br>Tests will not be given out after 4:00pm<br>Saturday 8am-12pm<br>Tests will not be given out after 11 am
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>General Information</h3>
                        
                        
                        
                        
                        <p><span>Note:</span> No tests will be given out during the last hour before closing. For example, if the
                           Center closes at 9:00pm, the last test is given at 8:00pm.
                        </p>
                        
                        
                        <h3>Staff</h3>
                        
                        <ul>
                           
                           <li>
                              Andrew Becker, Supervisor 
                           </li>
                           
                           <li>Dino DeJesus </li>
                           
                           <li>Beverley Miller </li>
                           
                           <li>Veletta Mundell</li>
                           
                           <li>Ana Solis </li>
                           
                           <li>Becky Botts <br>         
                              
                           </li>
                           
                        </ul>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/testing.pcf">©</a>
      </div>
   </body>
</html>