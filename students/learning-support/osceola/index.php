<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Campus Learning Centers | Learning Support | Valencia College | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Centers</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li>Osceola Campus</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        <section>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Mission Statement</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <p> Our mission is to provide an engaging and supportive environment where students can
                                          pursue a wide range of digital and traditional learning opportunities. We provide
                                          a range of academic services including academic tutoring, online tutoring, assistance
                                          with your writing assignments, PERT review sessions, and student-friendly computer
                                          workshops. Also, in our spacious computer area, you will find friendly staff that
                                          will help you with the software needed for your academic assignments. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>We would love to hear about your experience at the Learning Center! Click <a href="http://valenciacc.ut1.qualtrics.com/SE/?SID=SV_5vXk5qmjtXeAkRv">HERE</a> to take our Learning Support Survey.
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Services and Resources</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div><a href="/students/learning-support/osceola/tutoring.html">Academic Tutoring</a></div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="/students/learning-support/osceola/tutoring2.html">Tutors &amp; Subjects</a></div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="/students/learning-support/osceola/cpt.html">Assesment Exam Review </a></div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/writing-center/default.html">Writing Center</a> 
                                                </div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="/students/learning-support/osceola/OnlineTutoring.html">Online Tutoring</a></div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="/students/learning-support/osceola/engreadlab.html">ENC/REA Labs</a></div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>              
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Hours of Operation</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>Mon-Thur:</div>
                                                
                                                <div>8:00 am - 8:00 pm</div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>Fri: </div>
                                                
                                                <div>8:00 am - 5:00 pm </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>Sat: </div>
                                                
                                                <div>8:00 am - 12:00pm</div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>Sun: </div>
                                                
                                                <div>Closed</div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>                  
                                       <br>
                                       
                                       <p>Please click <a href="/students/learning-support/osceola/documents/LSCalendar1516.pdf">this link</a> for our Summer 2016 Learning Support Calendar
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Policies &amp; Procedures</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>In order to provide a better learning centered environment, we ask you to please</p>
                                       
                                       <ul>
                                          
                                          <li>take cell-phone calls outside</li>
                                          
                                          <li>finish food and drinks outside</li>
                                          
                                          
                                          <li>keep the volume of your conversation and music down</li>
                                          
                                          <li>give priority to students using computers for academic assignments</li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>               
                                    <div>
                                       <h3>Contact Information</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Information Desk: 407-582-4250<br>
                                          Manager of Learning Support Services (Marie Brady): 407-582-4105<br>
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <h3>Other Labs and Centers  </h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/library/index.html">Library</a>: 4-202
                                                </div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/math-osceola/mathdepot.html">Depot</a>: 4-121 
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/testing/index.html">Testing Center:</a> 4-248
                                                </div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/mainlab/academicSystems.html">Academic Systems</a>:1-144
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/students/student-services/help.html">Answer Center</a>: 2-105B/C
                                                </div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/mainlab/graphics.html">Graphics Lab</a>: 1-246
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/students/student-services/atlas.html">Atlas Access Lab</a>: 2-105F/G
                                                </div>
                                                
                                                <div>
                                                   <a href="../../../locations/osceola/mainlab/language.html">Language Lab</a>: 3-100
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <a href="../../../locations/assessments/index.html" target="_self">Assessment Center</a> (PERT): 4-248
                                                </div>
                                                
                                                <div>
                                                   <a href="../../../locations/student-development/default.html" target="_self">Student Development:</a> 2-150
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>              
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           
                           <p><strong>Call an Admissions Coach<br>
                                 <i>Llama a tu consejero personal</i></strong></p>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>407-582-1507</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>1800 Denn John Lane<br>Kissimmee, FL 34744
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://www.google.com/maps/ms?msid=213232051841984513042.0004ac76f30b48cef93f1&amp;msa=0&amp;ll=28.310143,-81.377149&amp;spn=0.018438,0.034053" target="_blank">
                           Directions</a>
                        <a href="../../../locations/map/osceola.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        <a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1" target="_blank" title="Virtual Tour"><img alt="osceola campus virtual tour" height="245" src="/students/learning-support/osceola/youvisit-osceola-245px.jpg" width="245"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/index.pcf">©</a>
      </div>
   </body>
</html>