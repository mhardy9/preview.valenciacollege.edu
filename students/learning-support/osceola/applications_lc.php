<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Learning Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/applications_lc.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Campus Learning Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/learningcenter/index.html"></a>
                        
                        
                        <div>
                           
                           <div dir="ltr">
                              
                              <h2>Available Software </h2>
                              
                              <p>The Learning Center houses the Osceola campus's  software library, with most software
                                 associated with courses at the  Osceola campus. Please make sure that you are familiar
                                 with the  Learning Center's policies. Click on the tab for the academic area that
                                 you are interested in to find a detailed list of software available. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><strong>ENG/EAP</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>IT/OST</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>MATH</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>HEALTH</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Tools</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Academic Vocabulary </div>
                                 
                                 <div>GDP 11 Keyboarding </div>
                                 
                                 <div>Virtual TI Calc</div>
                                 
                                 <div>Medisoft</div>
                                 
                                 <div>Audacity</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Active Vicabulary </div>
                                 
                                 <div>Skillbuilding</div>
                                 
                                 
                                 <div>Diet Analysis 8 </div>
                                 
                                 <div>Gimp</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ellis Master Pronumciation </div>
                                 
                                 <div>MS Office 2013 </div>
                                 
                                 
                                 
                                 <div>Handbrake</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Ellis Placement</p>
                                    
                                 </div>
                                 
                                 <div>Express Scribe</div>
                                 
                                 
                                 
                                 <div>iTunes</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ellis Senior Mastery </div>
                                 
                                 <div>Medisoft</div>
                                 
                                 
                                 
                                 <div>VLC</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <p>Dev C++</p>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Eclipse C </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Eclipse java </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Notepad ++ </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>VMware Workstation</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Adobe Creative Suite (Limited availability)</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Filezilla</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Google Chrome</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Mozilla Firefox </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Lockdown Browser </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>Putty</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>WinSCP</div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <li>
                           
                           
                           
                           
                           
                           
                           
                           <hr>
                           
                           
                           <p><a href="javascript:countries.cycleit('prev')">Back</a> <a href="javascript:countries.cycleit('next')">Forward</a></p>
                           
                           <p>*Foreign language software available at the <a href="../../../locations/osceola/mainlab/language.html">Language Lab</a></p>
                           
                           
                        </li>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/applications_lc.pcf">©</a>
      </div>
   </body>
</html>