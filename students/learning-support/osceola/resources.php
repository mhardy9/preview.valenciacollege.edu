<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Learning Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Campus Learning Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/learningcenter/index.html"></a>
                        
                        
                        <div dir="ltr">
                           
                           <div>
                              
                              <div dir="ltr">
                                 
                                 <h2>Learning Resources </h2>
                                 
                                 <p>The following links will direct you to academic websites that will provide additional
                                    support in the following areas: computer programs, citing sources, writing, grammar,
                                    and math.
                                 </p>
                                 
                                 <p><br>
                                    <strong>PowerPoint and Word</strong></p>
                                 
                                 <p>
                                    <a href="http://office.microsoft.com/en-us/powerpoint">Microsoft PowerPoint</a></p>
                                 
                                 <p><a href="http://office.microsoft.com/en-us/word">Microsoft Word</a></p>
                                 
                                 
                                 <p><strong>MLA Style (7th edition) and APA Style </strong></p>
                                 
                                 <p><a href="http://www.easybib.com">EasyBib (Citation Generator)</a><br>
                                    
                                 </p>
                                 
                                 
                                 <p><strong>Grammar and Writing Resources</strong></p>
                                 
                                 <p><a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Resources</a></p>
                                 
                                 <p><a href="https://owl.english.purdue.edu/owl/">Purdue Online Writing Center</a></p>
                                 
                                 
                                 <p><strong>Other Resources</strong></p>
                                 
                                 <p><a href="../../../locations/learning-support/communications/links.html">West Campus Communications Center Links</a></p>
                                 
                                 <p><a href="http://www.hippocampus.org/" target="_blank">http://www.hippocampus.org/</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           <h2>&nbsp;</h2>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/resources.pcf">©</a>
      </div>
   </body>
</html>