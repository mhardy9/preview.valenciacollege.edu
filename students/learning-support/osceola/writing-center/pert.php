<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/pert.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>&nbsp;</h2>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h2><font color="#000000">PERT Resources </font></h2>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>As far as the PERT is concerned, the Osceola Writing Center's main goal is to provide
                                          a more in-depth explanation, if necessary, for those skills discussed in the PERT
                                          <a href="documents/PERTReadingGuide.pdf">Reading</a> and <a href="documents/PertWritingrevised8-12-16.pdf">Writing</a> Review Guides and provide supplemental materials for those students who would like
                                          more practice than what our guides supply.
                                       </p>
                                       
                                       <p>If you need information about PERT eligibility, costs, or other related items, please
                                          see below for links to those topics. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>For supplmental PERT support and practice beyond the review provided in our guide,
                                          please click <a href="#PERTskills">HERE</a>.
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              <div data-old-tag="tr">
                                 
                                 <div colspan="2" data-old-tag="td">
                                    <h3><font color="#000000">PERT Related Information </font></h3>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Link</strong></div>
                                 
                                 <div data-old-tag="td"><strong>Topic(s)</strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="../../assessments/pert/taking-the-exam.php">Taking the PERT</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">Fees, Time Limits, Hours Available,  Testing Locations, Test Facts, Retests,  Review
                                    Guides 
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="../../assessments/dual-enrollment.php">PERT &amp; Dual Enrollment </a></div>
                                 
                                 <div data-old-tag="td">General Requirements, SAT/ACT Scores, Testing and Retest Requirements </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/pert/">PERT Requirements &amp; Exemptions </a></div>
                                 
                                 <div data-old-tag="td">More info about requirements and exemptions</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://valenciacollege.edu/assessments/placement-chart.php">PERT Placement </a></div>
                                 
                                 <div data-old-tag="td">Placement scores by class </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="/students/learning-support/PERT/">Valencia PERT Review </a></div>
                                 
                                 <div data-old-tag="td">Information about what each campus provides in terms of reviewing for the PERT </div>
                                 
                              </div>
                              
                           </div>
                        </div>
                        
                        <section>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>PERT Practice Tests </h3>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>The company that created the PERT also provides PERT practice tests. Each section
                                          costs $2.50. Valencia does not require- nor does it endorse- these practice tests.
                                          However, they are available for those students who would like further practice (and
                                          since they come from the the PERT creators, they should allow you to get an accurate
                                          feel for what the PERT might be like). 
                                       </p>
                                       
                                       <p>Click <a href="http://www.vantageonlinestore.com/home.php?cat=337">HERE</a> to find out more information or to purchase practice tests. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>They also provide a free, shortened practice test with 10 questions per section. You
                                          can download it <a href="documents/mccannpertpracticetest.pdf">HERE</a>. </strong></div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div colspan="2" data-old-tag="td">
                                       <h3><a name="PERTskills"></a>PERT Skills - Supplementation &amp; Explanation 
                                       </h3>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div colspan="2" data-old-tag="td">
                                       <p>We hope that these materials will aid you in your preparation for the PERT placement
                                          test. You may use these resources in a standalone fashion, or they can be used in
                                          conjunction with Valencia's <a href="documents/PERT-Reading-Guide.pdf">Reading Guide</a> and <a href="documents/PertWritingrevised8-12-16.pdf">Writing Guide</a>. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Reading Skills </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong>Writing Skills </strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Finding the Subject/Topic</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="parallelism.php">Parallelism</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Main Idea </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="verb-tenses.php">Verb Tenses </a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Supporting Details </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="wordiness.php">Wordiness</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Relationships within/between Sentences </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="subject-verb-agreement.php">Subject-Verb Agreement </a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Inference</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="comma-splices.php">Comma Splices </a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Author's Purpose </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="OsceolaWritingCenterSemicolons.php">Semicolons</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Author's Tone </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="fused-sentences.php">Fused / Run-on Sentences </a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Bias</div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><a href="sentence-fragments.php">Fragments</a></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Fact and Opinion </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Dangling / Misplaced Modifiers </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Transitional Devices </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Pronoun Reference </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Organizational Patterns </div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div>Logical Comparisons </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>Context Clues </div>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/pert.pcf">©</a>
      </div>
   </body>
</html>