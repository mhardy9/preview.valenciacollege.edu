<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/thesis.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Thesis Statements </h2>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is it? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>A thesis statement is a statement that shares the main idea of your paper or the main
                                          point you're trying to make in your paper. While it's possible for a thesis to be
                                          made up of more than one sentence, for most of your assignments at the undergraduate
                                          level, you will usually only need one sentence (and that's what most teachers tend
                                          to look for). 
                                       </p>
                                       
                                       <p>A thesis can be very basic, or it can include the specific areas that you are going
                                          to use to support your paper - it depends on what your teacher wants and the kind
                                          of assignment.
                                       </p>
                                       
                                       <p><strong>Example</strong>:
                                       </p>
                                       
                                       <div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Basic:</strong></div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">
                                                      <div> People should stop smoking because of its negative consequences. </div>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Specific: </strong></div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">People should stop smoking <em>because it harms one's personal health, it harms others' health, and it can create
                                                         financial strain</em>. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Why do I need it? </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Academic writing in English is often concise and direct. By having a thesis statement
                                             <strong>at the end of your introduction</strong>, your reader does not have to wonder what your paper is going to be about or what
                                             the point is that you're trying to support. 
                                          </p>
                                          
                                          <p>Unless you're employing a specific technique where you're trying to get the reader
                                             to deduce something from what you're writing without saying it outright (kind of rare
                                             for academic writing in college), it is preferable to spell everything out for the
                                             reader. The less the reader has to do to figure out what you're trying to say, the
                                             better, because if you provide opportunity for someone to have to figure something
                                             out, it's possible that they will figure the wrong thing out. 
                                          </p>
                                          
                                          <p>Consequently, having a <em>direct</em> thesis statements sets a good starting point for your paper, and maps out specifically
                                             what you're going to discuss. 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>How do I write one? </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>A <em>basic</em> thesis statement should have 2 parts: <strong>narrowed topic</strong> + <strong>focus<span>*</span></strong></p>
                                          
                                          <p>A <em>specific</em> thesis statement will have 3 parts: <strong>narrowed topic</strong> + <strong>focus</strong> + <strong>supporting points</strong></p>
                                          
                                          <p><strong>Definitions</strong>: 
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Narrowed Topic</strong>: the specific topic you're writing about
                                             </p>
                                             
                                             <p><strong>Focus</strong>: the aspect/area within your topic that you want to discuss
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>Typical Focus Options</strong>: cause, effect, comparison, contrast, definition, classification, problem and solution,
                                                   process, argument
                                                </p>
                                                
                                             </blockquote>
                                             
                                             <p><strong>Supporting points</strong>: the major points you will use to support your point or the specific areas within
                                                which you will discuss your point. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>When creating your thesis, the order of the parts is not necessarily important as
                                             long as your thesis is concise and makes sense. For example, your supporting points
                                             could be at the beginning of the sentence or at the end.
                                          </p>
                                          
                                          <p><strong>Example:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Narrowed Topic</strong>: English in the USA and English in India 
                                             </p>
                                             
                                             <p><strong>Focus</strong>: contrast (differences) 
                                             </p>
                                             
                                             <p><strong>Supporting points</strong>: vocabulary, intonation, purpose
                                             </p>
                                             
                                             <p><span><strong>Thesis A</strong></span>: English in the USA is different than English in India in terms of vocabulary, intonation,
                                                and purpose.
                                             </p>
                                             
                                             <p><span><strong>Thesis</strong></span> <span><strong>B</strong></span>: Vocabulary, intonation, and purpose are three differences between American English
                                                and the English used in India. 
                                             </p>
                                             
                                             <p><span><strong>Thesis C</strong></span>: Three differences between English in the US and English in India are vocabulary
                                                usage, intonation, and the purpose for learning the language. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><span><strong>*</strong></span>Among other things, the "focus" may be called a "claim type." Some may consider the
                                             focus as part of the narrowed topic, so you may not always see it separated.
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>What NOT to Do</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>Don't just give a fact.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Example</strong>: There are trees in Russia. 
                                             </p>
                                             
                                             <p>This is a basic fact, but it doesn't create a debatable claime (which is normally
                                                what you want to do with a thesis statement). 
                                             </p>
                                             
                                             <p><strong>Better</strong>: Trees in Russia are causing problems for allergy sufferers.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>Don't be too vague.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Example</strong>: The main character in this novel is motivated by love because...
                                             </p>
                                             
                                             <p>Which character? What's the name of the novel? Even if you mentioned those things
                                                earlier in your introduction, you'll want to be clear in your thesis by stating the
                                                names.
                                             </p>
                                             
                                             <p><strong>Better</strong>: In The Hunt for Christopher, Mrs. Bee's motivation comes from love because... 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>Don't "announce."</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Example</strong>: In this paper, I will discuss how people can gain muscle.
                                             </p>
                                             
                                             <p>Rather than saying that you're going to discuss something (which is obvious by the
                                                fact that you've written something), you simply want to remove the "announcement"
                                                and keep the "meat" of the thesis.
                                             </p>
                                             
                                             <p><strong>Better</strong>: People can gain muscle by...  
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources: </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="https://owl.english.purdue.edu/owl/resource/588/01/">Purdue OWL - Thesis Statements</a></p>
                                          
                                          <p><a href="http://writingcenter.unc.edu/handouts/thesis-statements/">UNC - Thesis Statements </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/thesis.pcf">©</a>
      </div>
   </body>
</html>