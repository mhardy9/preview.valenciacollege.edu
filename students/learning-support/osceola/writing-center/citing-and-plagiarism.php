<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/citing-and-plagiarism.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Citing and Plagiarism </h2>
                           
                           <p>Academic integrity is highly regarded in American universities and colleges. When
                              we use other people's ideas and other people's words, we are required to provide credit
                              to the original author. This credit is given by utilizing citations. Different <a href="style-guides.php">styles</a> require different formatting of citations, but they all require for you to cite your
                              source of information. 
                           </p>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is Plagiarism? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>When you break it down, <a href="http://www.plagiarism.org/plagiarism-101/what-is-plagiarism/">plagiarism</a> is simply using someone else's intellectual property without acknowledging where
                                          you got the information. Plagiarism can be intentional or unintentional; intentional
                                          plagiarism may be dealt with more harshly than the unintentional variety, but in either
                                          case, you may be held accountable for not giving credit appropriately.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Common Misconceptions </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>1. All I need is a works cited / references page.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>If you have a works cited / references page, but you do not have in-text citations,
                                                then you are still in danger of plagiarizing. Without the in-text citations, your
                                                reader does not know what information in your paper came from a source or what is
                                                yours. In addition, it's impossible to know whether you even consulted a source that
                                                you listed (apart from plagiarism checking software like Turnitin).
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>2. One in-text citation is enough.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>While this may be true if you only have 1 sentence that contains information from
                                                a source, many people believe that if they cite a source somewhere within a paragraph
                                                that they are covered even if other information from the source is contained in other
                                                places within the paragraph. However, you typically need to cite each sentence that
                                                contains information from a source. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>3. I only need to cite if I use a quote.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>Citations are required for quotes and paraphrases. If you get information from a source,
                                                whether you choose to use the sources exact wording (quote) or use your own wording
                                                (paraphrase), the citation follows the idea.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>4. As long as I change a quote a little, I don't have to use quotation marks.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>If you choose to paraphrase, you must make sure that you change the original enough
                                                so that it's in <em><strong>your own words</strong></em>. If you change a word or two, or simply rearrange parts of the original wording,
                                                you are still plagiarizing. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>5. I learned the information a long time ago, or before I did my research, so I don't
                                                need to cite it.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>There are certain instances where information may not need cited. This kind of information
                                                is usually called "<a href="https://integrity.mit.edu/handbook/citing-your-sources/what-common-knowledge">common knowledge</a>." Still, it doesn't matter how long ago you learned something, if you use specific
                                                information in your paper that is not common knowledge, whether you knew it previously
                                                or not, you will need to cite it. For example, let's say that someone was in 10th
                                                grade and he/she learned that 9 million people per year are diagnosed with kidney
                                                stones, this information is probably not considered common knowledge unless your audience
                                                is made up of kidney doctors. Consequently, you should try to find a source for the
                                                information again (and with the power of the internet, that should be pretty easy).
                                                
                                             </p>
                                             
                                          </blockquote>
                                          
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/citing-and-plagiarism.pcf">©</a>
      </div>
   </body>
</html>