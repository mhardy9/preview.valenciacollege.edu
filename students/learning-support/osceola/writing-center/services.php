<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Services</h2>
                           
                           <h3>A Day in the Life of a Writing Consultation</h3>
                           
                           <div>
                              <ul>
                                 <li>No two consultations are the same; they are modified based on the needs of each student.
                                    Some of the writing areas we work with are below.
                                 </li>
                                 
                                 <li>Generally a consultation will last up to 30 minutes- though some may be less, depending
                                    on the help a student requires.
                                 </li>
                                 
                                 <li>There is no need to make an appointment. All consultations are conducted on a walk-in
                                    basis.
                                 </li>
                              </ul>
                           </div>
                           
                           <br>
                           
                           <div>
                              <h2> Areas of Focus</h2>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Style &amp; Formatting</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td"> 
                                          <p>Never heard of APA, MLA, or Chicago styles? Don't know how to get your works cited
                                             page to have a hanging indent? Not sure if, when, or how to cite a source? The Writing
                                             Center is the place to go. We can help with general formatting and style specific
                                             concerns. 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Content</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td"> 
                                          <p>Consultations that focus on content will look at the information that the student
                                             provides, and whether the student has argued his/her point effectively, used relevant
                                             supporting information, and fulfilled the requirements of the assignment in terms
                                             of purpose. 
                                          </p>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Grammar </h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td"> 
                                          <p>If you're not sure what in the world a comma splice is, or the difference between
                                             using a gerund vs. a participle, or how to make subjects and verbs agree, talk to
                                             a consultant! 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Brainstorming &amp; Outlining</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td"> 
                                          <p>For students who may not know how to begin the writing process, our writing consultants
                                             are more than willing to sit down and work with students to brainstorm ideas or work
                                             with students to put their ideas into a well formed outline
                                          </p>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Structure &amp; Organization </h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       
                                       <div data-old-tag="td"> 
                                          <p>We can help students make sure that they are structuring their essay in a way that
                                             works with the kind of essay they are writing. We can also look to see if a student
                                             has organized their information appropriately. 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div colspan="2" data-old-tag="td">
                                          <h3>Speech </h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>And you thought the Writing Center only worked with...writing. We actually help students
                                             work on speeches too! Bring in a speech or speech outline for us to look over, or
                                             even come in to practice your speech!
                                          </div>
                                       </div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           <br>
                           <br>
                           <br>
                           
                           
                           
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    <div data-old-tag="tr">
                                       <div data-old-tag="td">
                                          
                                          <h3>What we don't Do</h3>
                                          
                                          <p>
                                             Because the primary purpose of a writing consultation is education, we do not provide
                                             blanket editing services or proofreading. We will look at a student's work and show
                                             him/her some of the mistakes (not all), thereby giving each student an opportunity
                                             to learn to become a better editor, which will be beneficial for the next assignment.
                                          </p>
                                          
                                          <p>In the end, teachers will receive papers based on the student's work, not on a consultant's
                                             ability to perfect a student's mistakes.
                                          </p>
                                       </div>
                                       
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                           
                           
                           
                        </section>       
                        
                        
                     </div>
                     <aside class="col-md-3"> 
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/services.pcf">©</a>
      </div>
   </body>
</html>