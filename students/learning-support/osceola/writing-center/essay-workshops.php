<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/essay-workshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-12">
                        
                        
                        
                        <section>
                           
                           <p>Located in the <strong>Learning Center</strong> (3-100) 
                           </p>
                           
                           <hr>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><font color="white">
                                          
                                          <h3>Workshop</h3>
                                          </font></div>
                                    
                                    <div data-old-tag="td"><font color="white">
                                          
                                          <h3>Times</h3>
                                          </font></div>
                                    
                                    <div data-old-tag="td"><font color="white">
                                          
                                          <h3>Materials</h3>
                                          </font></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><strong>The Write Space:</strong></h3>
                                       
                                       <h3><strong>How to Format your MLA-style Essay </strong></h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Mon. Jan 23 @ 1:30PM</p>
                                       
                                       <p>Thurs. Jan 26 @ 4:45PM </p>
                                       
                                       <p>Mon. Apr 3 @ 1:30PM </p>
                                       
                                       <p>Thurs. Apr 6 @ 4:45PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><strong>Narrative in a Nutshell:</strong></h3>
                                       
                                       <h3><strong>The Narrative Essay </strong></h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Mon. Jan 30 @ 1:30PM</p>
                                       
                                       <p>Thurs. Mar 2 @ 4:45PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><strong>Fluff and Stuff:</strong></h3>
                                       
                                       <h3><strong>Recognizing and Eliminating Wordiness </strong></h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Thurs. Feb 2 @ 4:45PM</p>
                                       
                                       <p>Mon. Feb 27 @ 1:30PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><strong>This vs That:</strong></h3>
                                       
                                       <h3><strong>The Comparison and Contrast Essay </strong></h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Mon. Feb 6 @ 1:30PM</p>
                                       
                                       <p>Thurs. Mar 9 @ 4:45PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><strong>On my List:</strong></h3>
                                       
                                       <h3><strong>What is Parallelism?</strong></h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Thurs. Feb 9 @ 4:45PM</p>
                                       
                                       <p>Mon. Mar 6 @ 1:30PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>Plays Well with Authors:</h3>
                                       
                                       <h3>The Literary Analysis  </h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Mon. Feb 13 @ 1:30PM</p>
                                       
                                       <p>Thurs. Mar 23 @ 4:45PM</p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>Their, There, They're:</h3>
                                       
                                       <h3>Words Often Confused  </h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Thurs. Feb 16 @ 4:45PM</p>
                                       
                                       <p>Mon. Mar 20 @ 1:30PM  </p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>Taking a Stand:</h3>
                                       
                                       <h3>The Argument Essay </h3>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>Mon. Feb 20 @ 1:30PM</p>
                                       
                                       <p>Thurs. Feb 23 @ 4:45PM</p>
                                       
                                       <p>Mon. Mar 27 @ 1:30PM</p>
                                       
                                       <p>Thurs. Mar 30 @ 4:45PM </p>
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/essay-workshops.pcf">©</a>
      </div>
   </body>
</html>