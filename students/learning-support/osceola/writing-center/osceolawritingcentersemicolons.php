<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/osceolawritingcentersemicolons.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Semicolons <span>;</span> 
                           </h2>
                           
                           <p>Many people are confused about when to use commas, semicolons, or colons. However,
                              after reading the information below, you will be a semicolon expert. (And semicolons
                              are probably the easiest of the three to know when to use)
                           </p>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Is it a period...or a comma? It's both! </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Semicolons are somewhere between a comma and a period when it comes to pauses. You
                                          can see that a semicolon ( <strong>;</strong> ) is actually made of both of these punctuation marks. Sometimes you need a stronger
                                          punctuation mark than a comma, and other times you might need less strong of a break
                                          in thought than a period affords. Enter the semicolon. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>When to Use Semicolons </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><strong>1. To Connect <em>Closely Related</em> Independent Clauses</strong></p>
                                       
                                       <p>Most of the time when you see a semicolon, it will be connecting 2 independent clauses.
                                          
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>                                        
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>Nobody knows what happened to the inhabitants of Roanoke</strong></p>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>;</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <p><strong>it remains a mystery until this day </strong></p>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                       <p>In order to know where to put a semicolon, you must first understand what an independent
                                          clause is:
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p>An <strong>independent clause</strong>, in its most basic form, is  a subject + a verb that can stand alone (if you put
                                             a period at the end, it's a complete sentence). 
                                          </p>
                                          
                                          <p>Make sure to remember that independent clauses can be as short as two words. It's
                                             not about the length of the sentence, but  whether an appropriate subject and verb
                                             are present ("I eat." is technically a complete sentence). 
                                          </p>
                                          
                                       </blockquote>                      
                                       
                                       <p>So, how do you know if your independent clauses are "closely related"? Well, that's
                                          a little more subjective. However, if you are writing about the same subject, you
                                          more than likely <em>could</em> use a semicolon rather than a period. Do you have to? No, but think about the connections
                                          that you're trying to make in your writing- some things are stronger together than
                                          apart.
                                       </p>
                                       
                                       <p><strong> 2. To Separate List Items that Have Other Punctuation Already</strong></p>
                                       
                                       <p>For basic lists we only need to use commas to separate the items:</p>
                                       
                                       <blockquote>
                                          
                                          <p>Ex: I play the guitar<strong>,</strong> saxaphone<strong>,</strong> and piano. 
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <p>If you have a list with items that already include commas, it could be confusing to
                                          also use commas to separate the items:
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p><span><strong>Confusing</strong></span>: 
                                          </p>
                                          
                                          <p>I sent the letter to John, the president of Costco, Tim, Best Buy's CEO, and Esther,
                                             CFO fo Dell.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>Based on this construction, one might think that there are 5 people who received the
                                                letter rather than just three- 1) John, 2) Costco's president, 3) Tim, 4) Best Buy's
                                                CEO, and 5) Esther.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>Clear</strong>: 
                                          </p>
                                          
                                          <p>I sent the letter to John, the president of Costco<strong>;</strong> Tim, Best Buy's CEO<strong>;</strong> and Esther, CFO for Dell. 
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>Now I can see that the information that comes after the commas relates to a person's
                                                position/title and not to a separate person. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3>More Resources</h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://theoatmeal.com/comics/semicolon">The Oatmeal</a> (Educational &amp; Funny) 
                                          </p>
                                          
                                          <p><a href="https://www.youtube.com/watch?v=5kssP4fWs9U">Smart English</a> (Youtube Video)
                                          </p>
                                          
                                          <p><a href="https://www.youtube.com/watch?v=IXaAVc3PQKQ">Flash Grammar</a> (Youtube Video) 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://www2.ivcc.edu/rambo/practice_semicolon_quiz.htm">Practice 1</a></p>
                                          
                                          <p><a href="https://owl.english.purdue.edu/exercises/3/5/23">Practice 2</a></p>
                                          
                                          <p><a href="http://depts.dyc.edu/learningcenter/owl/exercises/semicolons_ex1.htm">Practice 3   </a></p>
                                          
                                          <p><a href="http://www2.isu.edu/success/writing/handouts/semicolons.pdf">Practice 4</a> (PDF) 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/osceolawritingcentersemicolons.pcf">©</a>
      </div>
   </body>
</html>