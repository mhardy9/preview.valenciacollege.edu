<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/understanding-person.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Understanding 1st, 2nd, and 3rd Person </h2>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">It's as easy as a Conversation </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>To understand person, it may be helpful to use the context of a conversation. Consider
                                          this diagram as you read the explanation that follows:
                                       </p>
                                       
                                       
                                       <p><strong>1st Person</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p> First person is always going to be what you would use to personally  respond to a
                                             question; if asked as a single person, you would answer with “I”; &nbsp;if asked as a member
                                             of a group, you’d answer with “we.” Think of 1st person as the person you are closest
                                             to (yourself).
                                          </p>
                                          
                                       </blockquote>                  
                                       <p><strong>2nd Person </strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Second person is the person or people that you are talking directly <strong>TO </strong>or<strong> WITH</strong> (using "you"). If I’m in a conversation with a person, I might ask “What do <strong>YOU</strong> want to eat for breakfast?” They are the next closest person/people in terms of proximity.
                                          </p>
                                          
                                       </blockquote>                  
                                       <p><strong>3rd Person</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Third person includes the person, people, or things that you are talking <strong>ABOUT</strong>. If the person/thing in your sentence could be replaced with "he", "she", "it", or
                                             "they", you are using 3rd person. 
                                          </p>
                                          
                                          <p>Going back to our conversation example, I cannot ask a direct question to a guy  in
                                             the classroom next door; he is not <strong>participating in</strong> the conversating. I can ask someone a question <strong>ABOUT</strong> that person (What do you think HE wants for breakfast?), but not TO the person. Once
                                             I go over to the person and ask him a question, he has gone from being  3rd person
                                             to  2nd person since I would ask him a question using “you.” Third person is the furthest
                                             away in proximity.
                                          </p>
                                          
                                       </blockquote>                  
                                       <blockquote>
                                          
                                          <p>Remember, objects are 3rd person. Take a look at this sentence: </p>
                                          
                                          <blockquote>
                                             
                                             <p>The car was very ugly. </p>
                                             
                                          </blockquote>
                                          
                                          <p>In my sentence, “the car” is the thing that I’m talking ABOUT, and I could replace
                                             “the car” with “it.” Consequently, car is 3rd person – I’m not having a conversation
                                             WITH the car (otherwise it might be time to seek professional help).
                                          </p>
                                          
                                       </blockquote>                  
                                       
                                       <p><strong>When Writing your Essays</strong></p>                  
                                       <blockquote>
                                          
                                          <p>Don't forget that for most of your academic writing assignments, you will probably
                                             be told to use 3rd person. As a result, the subjects of your sentences should be those
                                             things that could be replaced with the pronouns discussed above: he, she, it, and
                                             they. 
                                          </p>
                                          
                                       </blockquote>
                                       <blockquote>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>You</strong> should not smoke because it can harm your lungs. (2nd person) 
                                             </p>
                                             
                                             <p><strong>Nobody</strong> should smoke because... (3rd person- "nobody" is a he or she)
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/understanding-person.pcf">©</a>
      </div>
   </body>
</html>