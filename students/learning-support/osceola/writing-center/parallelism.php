<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/parallelism.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Parallelism / Parallel Structure </h2>
                           
                           <p>While parallelism can be discussed in terms of paragraphs, this section will focus
                              on paralellism at a sentence level.
                           </p>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is it? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>In writing, parallelism is the use of the same kind of words, phrases, or structures
                                             within a sentence or list. 
                                          </p>
                                          
                                          <p>Think of 2 parallel lines:</p>
                                          
                                          
                                          <p>These lines are the "same" because they are both going the exact same direction. It
                                             should be the same way within your sentences, except instead of using lines, you are
                                             using words, phrases, and clauses.
                                          </p>
                                          
                                          <p>Problems happen when these items don't "line up." Instead, if you have different grammatical
                                             forms, it's like having lines that are going in different directions. In the end they
                                             will intersect, causing a "grammar accident." 
                                          </p>
                                          
                                          
                                          
                                          
                                          <p><strong>Here's an example of a sentence that has a list with <span>non-parallel</span> units:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>My professor likes<span> <strong>teaching</strong>, <strong>to watch</strong> movies, and <strong>happy</strong></span>.
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p>All of the items in the list are different grammatical types:</p>
                                                
                                                <blockquote>
                                                   
                                                   <p>teaching = gerund</p>
                                                   
                                                   <p>to watch movies = infinitive phrase</p>
                                                   
                                                   <p>happy = adjective </p>
                                                   
                                                </blockquote>
                                                
                                                <p>As a result, our sentence is like the 2nd picture above, and the parts of the sentence
                                                   collide in an ugly way rather than move our thoughts along smoothly.
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <p><strong>Here's how the sentence should look using parallelism:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>My professor likes teach<strong>ing</strong>, watch<strong>ing</strong> movies, and be<strong>ing</strong> happy.
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p>Now everything in the list starts with a gerund (the -<strong>ing</strong> words).
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>How to Fix </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>For lists:</strong></p>
                                          
                                          <p>If you are listing things, you need to make sure that every item in your list is the
                                             same grammar type. For example, if your list starts with an adjective, <em>every other list item</em> should be an adjective.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>Ex: He is <strong>kind</strong>, <strong>funny</strong>, and <strong>talkative</strong>. (all adjectives)
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>Keep in mind that sometimes we "reuse" words when we start a list:</p>
                                          
                                          <blockquote>
                                             
                                             <p>Ex: I like <strong>to play</strong> soccer, <strong>wear</strong> jeans, and <strong>write</strong> poetry.
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p>In this list, these are all infinitives (to + verb). Even though the only item in
                                                   the list that has "to" is the first item, the "to" goes with all the other items as
                                                   well. This only works if we have the "to" with the first item only. If we add a "to"
                                                   to any of the other items, the list is no longer parallel.
                                                </p>
                                                
                                                <blockquote>
                                                   
                                                   <p>Ex. I like <strong>to play</strong> soccer, <strong>wear</strong> jeans, and <strong>to write</strong> poetry. 
                                                   </p>
                                                   
                                                   <p>(now one item is not the same because we've broken the rule- either we need all items
                                                      to use "to", or only the 1st item) 
                                                   </p>
                                                   
                                                </blockquote>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             <blockquote>&nbsp;</blockquote>
                                             
                                          </blockquote>
                                          
                                          <p><strong>For comparisons: </strong></p>
                                          
                                          <p>If you are comparing 2 things, make sure that the things you are comparing are both
                                             the same kind of grammatical form.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>Not parallel</strong></span>: I prefer <span><strong>to dance</strong></span> (infinitive) rather than <span><strong>singing</strong></span> (gerund).
                                             </p>
                                             
                                             <p><strong>Parallel</strong>: I prefer <strong>to dance </strong>(infinitive) rather than <strong>to sing </strong>(infinitive).
                                             </p>
                                             
                                             <p><strong>Parallel</strong>: I prefer <strong>dancing</strong> (gerund) rather than <strong>singing </strong>(gerund). 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          
                                          <p><strong>With correlative conjunctions (either...or, not only....but also, etc):</strong></p>
                                          
                                          <p>Just like with the other examples above, we just need to make sure the 2 parts of
                                             our correlation are the same type of word/phrase/clause. For these, you'll need to
                                             pay attention to <strong>where you put the verb</strong>. 
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>If the <strong>verb</strong> comes before the <em>correlative conjunction</em>, you need only add something that fits with the verb.
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>Parallel</strong>: The Maryland Children's Orchestra <em>not only</em> <strong>has</strong> children, <em>but also</em> <strong>play</strong> songs written by children. 
                                                </p>
                                                
                                                <p><span><strong>Not parallel</strong></span>: The Maryland Children's Orchestra <strong>has</strong> <em>not only</em> children, <em>but also</em> <span><strong>play</strong></span> songs written by children. 
                                                </p>
                                                
                                             </blockquote>
                                             
                                             <p> If the verb comes after the correlative conjunction, you'll need to include a verb
                                                in the 2nd part as well. 
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>Parallel</strong><span>: The post office either</span> <strong>is</strong> on 4th street, <em>or</em> it <strong>is</strong> on 9th avenue.
                                                </p>
                                                
                                             </blockquote>
                                             
                                             <blockquote>
                                                
                                                <p><span><strong>Not parallel</strong></span>: The post office <em>either</em> <strong>is</strong> on 4th street <em>or</em> 9th avenue. 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <p><strong>With coordinating conjunctions (for, and, not, but, or, yet, so):</strong></p>
                                          
                                          <p>Make sure the items that come before and after the coordinating conjunction are both
                                             the same grammar type.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>Not parallel</strong></span>: Anita doesn't want <strong>dogs</strong> (noun) or <span><strong>to visit</strong></span> (infinitive) relatives. 
                                             </p>
                                             
                                             <p><strong>Parallel</strong>: Anita doesn't want to have dogs or (to) visit relatives. (both infinitive phrases)
                                                
                                             </p>
                                             
                                          </blockquote>
                                          
                                          
                                          <p>*The above areas are not a definitive list of times when you need to use parallel
                                             structure. For more information see below.
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://study.com/academy/lesson/parallelism-how-to-write-and-identify-parallel-sentences.html">Study.com Video</a></p>
                                          
                                          <p><a href="https://www.youtube.com/watch?v=qvDNvS2M3QA">Youtube Video</a></p>
                                          
                                          <p><a href="https://owl.english.purdue.edu/owl/resource/623/01/">Purdue OWL</a></p>
                                          
                                          <p><a href="https://webapps.towson.edu/ows/moduleparallel.htm">Towson</a></p>
                                          
                                          <p><a href="http://www.quickanddirtytips.com/education/grammar/parallel-structure-an-unparalleled-letdown">Quick &amp; Dirty Tips</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://chompchomp.com/structure01/structure01.01.htm">Practice 1 
                                                </a></p>
                                          
                                          <p><a href="http://grammar.ccc.commnet.edu/grammar/quizzes/niu/niu10.htm">Practice 2 </a></p>
                                          
                                          <p><a href="http://www.oupcanada.com/higher_education/companion/literature/9780195425154/eng_135/quiz_parallelism.html">Practice 3 </a></p>
                                          
                                          <p><a href="http://www.mhhe.com/socscience/english/langan/sentence_skills/exercises/ch17/p4exj.htm">Practice 4 </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/parallelism.pcf">©</a>
      </div>
   </body>
</html>