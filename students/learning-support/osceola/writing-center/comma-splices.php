<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Wrting Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/comma-splices.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Wrting Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li>Osceola Wrting Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Comma Splices</h2>
                           
                           <p>Comma splices are probably one of the most seen errors in student writing. However,
                              these errors are very easy to fix once you know what to look for.
                           </p>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Comma Splices </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><strong>What is it? </strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>A comma splice occurs when two <strong><em>independent</em> clauses</strong> are joined with <em>only</em> a comma.
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>                  
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <div>I drove to the park yesterday </div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>,</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>I also played soccer. </div>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                       <blockquote>&nbsp;
                                          
                                       </blockquote>                      
                                       
                                       <p><strong>Independent Clauses</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>To be able to identify comma splices, it's first necessary to understand what an independent
                                             clause is. An independent clause, in its most basic form, is  a subject + a verb that
                                             can stand alone (if you put a period at the end, it's a complete sentence). 
                                          </p>
                                          
                                          <p>Make sure to remember that independent clauses can be as short as two words. It's
                                             not about the length of the sentence, but  whether an appropriate subject and verb
                                             are present ("I eat." is technically a complete sentence). 
                                          </p>
                                          
                                       </blockquote>                      
                                       
                                       <p><strong>How to Fix</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>First, you must make sure that you are dealing with a comma splice. If you think you
                                             have a comma splice, just replace the comma with a period. If both clauses before
                                             and after the comma can be sentences by themselves, then you have a comma splice.
                                          </p>
                                          
                                          <p>If you have a comma splice, you can replace the comma with:</p>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <div>a period</div>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          <div>                          
                                             
                                             <ul>
                                                
                                                <li> a semicolon (if the 2 clauses are closely related in content)</li>
                                                
                                                <li>a comma <strong><em>with</em></strong> one of the "fanboys" (for, and, nor, but, or, yet, so)
                                                </li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                          <p>You may be able to choose from several, or even all three, of the options above to
                                             use to fix the comma splice (while there may be some cases where one is preferred
                                             over another, you will almost always have more than one option that will work). 
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>&nbsp;</blockquote>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://depts.dyc.edu/learningcenter/owl/exercises/comma_splices_ex1.htm">Practice 1</a></p>
                                          
                                          <p><a href="documents/commasplicesexercise1.pdf">Practice 2</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/comma-splices.pcf">©</a>
      </div>
   </body>
</html>