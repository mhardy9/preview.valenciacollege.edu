<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/fused-sentences.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Run-on <span>/</span> Fused Sentences 
                           </h2>
                           
                           
                           
                           
                           <p>These kind of errors tend to be less common than our comma splice errors, but they
                              can still be an issue. Take a look at the information below so that you will be able
                              to recognize and fix run-ons.
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Run-on Sentences </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>What is it? </strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>A run-on sentence, also called a "fused sentence," is when 2 sentences are joined
                                                together <em>without any punctuation</em>.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Example</strong>:
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p>Researchers suggest that the best way to learn a language is by being immersed <strong>however, it is also important to study daily. </strong></p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p>In the above example, the sentences have no punctuation connecting them, so one "runs-on"
                                                into the next.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>How to Fix</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>Fixing a run-on/fused sentence is just like fixing a <a href="comma-splices.php">comma splice</a>. However, instead of having a comma where the 2 sentences connect, you have nothing.
                                                Consequently, it may take a little more time to locate where one sentence ends and
                                                the other begins. Here are a couple suggestions for finding the spot where you need
                                                to put your punctuation:
                                             </p>
                                             
                                             <ul>
                                                
                                                <li>Read the sentence out loud and see if there's a place where you pause; we often place
                                                   a pause at the end of a sentence. 
                                                </li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <ul>
                                                
                                                <li>Try reading the sentence out loud without providing any pauses- if you find a place
                                                   where you feel like you're rushing from one complete thought into another, that may
                                                   be where you need your punctuation.
                                                </li>
                                                
                                             </ul>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <ul>
                                                
                                                <li>You can also search for the 2 main subjects and their corresponding verbs and then
                                                   find the appropriate place for your punctuation so that you have 1 main subject and
                                                   main verb in each sentence. 
                                                </li>
                                                
                                             </ul>
                                             
                                             <p>Once you have found the place where you need punctuation, choose between one of the
                                                punctuation options below:
                                             </p>
                                             
                                             <ul>
                                                
                                                <li>
                                                   
                                                   <div>a period</div>
                                                   
                                                </li>
                                                
                                             </ul>
                                             
                                             <div>
                                                
                                                <ul>
                                                   
                                                   <li> a semicolon (if the 2 clauses are closely related in content)</li>
                                                   
                                                   <li>a comma <strong><em>with</em></strong> one of the "fanboys" (for, and, nor, but, or, yet, so)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             <p>You may be able to choose from several, or even all three, of the options above to
                                                use to fix the run-on (while there may be some cases where one is preferred over another,
                                                you will almost always have more than one option that will work). 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://www.chompchomp.com/terms/fusedsentence.htm">Chompchomp</a></p>
                                          
                                          <p><a href="https://owl.english.purdue.edu/owl/resource/598/02/">Purdue OWL </a></p>
                                          
                                          <p><a href="http://writingcenter.unc.edu/handouts/fragments-and-run-ons/">UNC Writing Center </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://www.ccc.commnet.edu/grammar/quizzes/runons_quiz.htm">Practice 1</a></p>
                                          
                                          <p><a href="http://www.bristol.ac.uk/arts/exercises/grammar/grammar_tutorial/page_77.htm">Practice 2</a></p>
                                          
                                          <p><a href="http://depts.dyc.edu/learningcenter/owl/exercises/run-ons_ex1.htm">Practice 3   </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/fused-sentences.pcf">©</a>
      </div>
   </body>
</html>