<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/wordiness.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Wordiness</h2>
                           
                           
                           
                           
                           <p>In academic and business writing, being aware of how many words you use to say something
                              is especially important. Creative writing allows for much more looseness in terms
                              of wordiness because there is a different purpose- creative writing is used to submerge
                              the writer in the details of a story, often while using lenthier and more artistic
                              wording, while academic/business writing is meant to get to the point quickly and
                              directly. That being said, writing is subjective; what one person considers wordy
                              might not be wordy to another. So, in your academic writing, it's important to be
                              concise, using only as many words as necessary to get your point across.
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is it? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Wordiness is simply having overly lengthy or redundant wording in your sentences/paragraphs.</p>
                                       
                                       <p><strong>Example</strong>:
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p><span><strong>Wordy</strong></span>: The question of whether or not he had made a decision was answered in abundant clarity
                                             through one reading of his quizical complexion. 
                                          </p>
                                          
                                          <p><strong>Concise</strong>: He still had not made a decision.
                                          </p>
                                          
                                       </blockquote>                  
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>How to Fix </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Obviously, the easy answer for how to fix wordiness is to remove the extra words or
                                             redundancies; however, how do you know if something is too wordy? Here are some ways
                                             to make your writing more concise:
                                          </p>
                                          
                                          <p><strong>1. Remove redundant information (saying the same thing more than once):</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Example</strong>: People studying while on a student visa in the United States <span><strong>do not have US citizenship</strong></span>. (if they are on a visa, then it's obvious they are not citizens- which is redundant)
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>2. Shorten redundant phrases:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>We often use many phrases in English that are highly redundant. Consequently, taking
                                                out the redundant part will remove the wordiness. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Examples</strong>:
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p><span>surrounded <strong>on all sides</strong></span> --&gt; <strong>surrounded</strong> (surrounded already means something is around all sides)
                                                </p>
                                                
                                                <p><span><strong>personal</strong> opinion</span> --&gt; <strong>opinion</strong> (opinions are already personal)
                                                </p>
                                                
                                                <p><span>4 am <strong>in the morning</strong></span> --&gt; <strong>4 am</strong> (4 am only happens in the morning) 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <p><strong>3. Shorten lengthy phrases:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Examples</strong>:
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p><span>In the event that something happens</span>... --&gt; <strong>If</strong> something happens... 
                                                </p>
                                                
                                                <p><span>Due to the fact that</span>... --&gt; <strong>Because</strong>... 
                                                </p>
                                                
                                                <p><span>In this day and age</span>... --&gt; <strong>Today</strong>... 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <p><strong>4. Combine sentences when possible:</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Wordy</strong>: Teachers are responsible for providing academic content to their students. They
                                                are also responsible for making sure that students are aware of the course requirements.
                                             </p>
                                             
                                             <p><strong>Better</strong>: Teachers are responsible for providing academic content and making students aware
                                                of course requirements. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>These are just a few ways to improve the conciseness of your writing. Take a look
                                             at some other resources below for more ways to prevent wordiness. 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://www.macalester.edu/max/writing/wordiness/">Macalester College</a></p>
                                          
                                          <p><a href="http://grammar.ccc.commnet.edu/grammar/concise.htm">Guide to Grammar &amp; Writing</a></p>
                                          
                                          <p><a href="http://web.uvic.ca/~gkblank/wordiness.html">List of Wordy Clauses/Phrases</a></p>
                                          
                                          <p><a href="http://leo.stcloudstate.edu/style/wordiness.html">Literary Education Online </a></p>
                                          
                                          <p><a href="http://www2.isu.edu/success/writing/handouts/wordiness.pdf">PDF 1   </a></p>
                                          
                                          <p><a href="http://www.usj.edu/files/8714/0380/5624/eliminatingwordiness.pdf">PDF 2</a> 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="https://owl.english.purdue.edu/exercises/6/9/24">Practice 1</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/wordiness.pcf">©</a>
      </div>
   </body>
</html>