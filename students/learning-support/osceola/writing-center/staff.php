<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Staff</h2>
                           
                           <h3> Don't let writing get you down. We're here to help.</h3>
                           
                           <div>
                              <p>All of our consultants are highly qualified and ready to handle any writing question
                                 you can throw their way...OK, almost any question. They all have been or currently
                                 are teachers, and love helping students with writing!<br>
                                 
                              </p>
                           </div>
                           
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Our Team</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <p><img alt="Marie Brady" height="143" src="about5.png" title="Marie Brady" width="98"><b>Marie Brady</b></p>
                                          
                                          <p><strong>Manager of Learning Support Services</strong><br>
                                             321-682-4105<br>
                                             <a href="mailto:mbrady@valenciacollege.edu">mbrady@valenciacollege.edu</a><br>
                                             
                                          </p>
                                          
                                          <p> My B.A. degree is in Education and Biblical Studies and I have a M.S. degree from
                                             Nova Southeastern University in Computer Science. I have taught at Valencia for 24
                                             years in the OST (Office Systems Technology) Department. My new full-time role at
                                             the campus is Manager of Learning Support. I also enjoy student interaction and that
                                             is why I continue to teach part-time. Originally, I am from North Carolina, but I
                                             have lived most of my years in Florida. Apart from Valencia, I enjoy the outdoors,
                                             swimming, biking, tennis, going to Disney, and getting away to the beach. 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       <div data-old-tag="td">
                                          <p><img alt="Reem Wahed" height="143" src="photo-Reem-Wahed.jpg" width="98"><strong>Reem Wahed</strong></p>
                                          
                                          <p>
                                             <b>Instructional Lab Supervisor</b><br>
                                             321-682-4989<br>
                                             <a href="mailto:rwahed@valenciacollege.edu" rel="noreferrer">rwahed@valenciacollege.edu</a><br>
                                             
                                          </p>
                                          <p>I come to Valencia from Alexandria, Egypt. Like many Egyptians, I started studying
                                             English when I was seven years old. I eventually went to the University of Alexandria,
                                             where I graduated with a BA in law. However, my passion for languages and linguistics
                                             drew me to teaching, and in 1995, I worked as a full time teacher at Alexandria Language
                                             School and as a part time teacher teaching English as a Second Language at the Arab
                                             Academy for Science and Technology University. From 2000 to 2014, I was a program
                                             coordinator as well as a teacher and principal in both Alexandria and Cairo. In 2015,
                                             I moved to Florida and joined the Valencia family. I was first an SL Leader and now
                                             work as a writing consultant in the Writing Center.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       <div data-old-tag="td">
                                          <p><img alt="Wendy Stiles" height="143" src="about4.png" title="Wendy Stiles" width="98"><b>Wendy Stiles-Tardieu</b></p>                             
                                          
                                          <p>
                                             <b>Instructional Lab Supervisor</b><br>
                                             321-682-4185<br>
                                             <a href="mailto:swtardieu@valenciacollege.edu">wtardieu@valenciacollege.edu</a><br>
                                             
                                          </p>
                                          <p>I'm a Florida native from Orlando and I'm also a Valencia graduate. I am an English
                                             Professor here at the Osceola campus where I teach Composition 1 and 2. I have a Master's
                                             Degree in Creative Writing from the University of Central Florida where I studied
                                             literature and writing instruction. I also participated in the university's writing
                                             events and was featured as a panelist at a national writing conference. I am also
                                             a published author with credits in fiction, poetry and screenwriting. I love to help
                                             students become confident in their own abilities. My goal each day is to give students
                                             the tools needed for effective communication.
                                             
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       <div data-old-tag="td">
                                          <p><img alt="Abbie Potter Henry" height="143" src="about6.png" title="Abbie Potter Henry" width="98"><b>Abbie Potter Henry</b></p>                                           
                                          
                                          <p> <strong>Instructional Lab Supervisor</strong><br>
                                             321-682-4259<br>
                                             <a href="mailto:ahenry19@valenciacollege.edu">ahenry19@valenciacollege.edu</a><br>
                                             
                                          </p>
                                          
                                          <p>I am from Eastern Kentucky and received my education at Pikeville College, where I
                                             graduated as a non-traditional student and single mother of three in 1991. My degree
                                             is in English Literature and Composition with a minor in Religious Studies. I have
                                             lived in Florida and worked as an adjunct professor and writing consultant at Valencia
                                             College, Osceola since 2005. I am deeply committed to helping students discover the
                                             writer within and develop the editing skills necessary to succeed in college and in
                                             life. My other interests include gardening, creative writing, and spending time at
                                             the beach. 
                                          </p>
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/staff.pcf">©</a>
      </div>
   </body>
</html>