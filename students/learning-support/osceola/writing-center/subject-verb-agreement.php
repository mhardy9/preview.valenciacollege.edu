<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/subject-verb-agreement.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Subject-Verb Agreement </h2>
                           
                           
                           
                           
                           <p>Being able to locate and fix this error is very important for making sure that your
                              writing is clear and flows well because if you have many of these kinds of errors,
                              it can be distracting for your reader. This is a basic explanation of the issue. Please
                              visit the other resources linked to at the bottom for more information.
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is it? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>Subject-verb agreement (SVA) means that a subject needs to "agree" with its verb in
                                          <strong><em>number</em></strong> and <em><strong><a href="understanding-person.php">person</a></strong></em>. If you have a singular subject, your verb should be singular; if you have a plural
                                          subject, your verb should be plural.
                                       </p>
                                       
                                       <p><strong>Incorrect</strong>: The <strong>researchers</strong> at the company <span><strong>is noticing</strong></span> a large change in population.
                                       </p>
                                       
                                       <p><strong>Correct</strong>: The <strong>researchers</strong> at the company <strong>are noticing</strong> a large change in population. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Where the Errors are Made </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Generally, subject-verb agreement errors occur with<strong> present tense</strong> verb forms or with <strong>past continuous/progressive</strong> verb forms. To review verb tenses, click <a href="http://www.englishpage.com/verbpage/verbtenseintro.html">HERE</a>. 
                                          </p>
                                          
                                          <p>Subject-verb agreement errors are localized in these tenses because past tense and
                                             future<strong>*</strong> tense conjugations are the same whether a subject is singular or plural.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>For example:</strong></p>
                                             
                                             <p>He (singular) <strong>walked</strong> to the store. / They (plural)<strong> walked</strong> to the store.
                                             </p>
                                             
                                             <p>She (singular) <strong>will go</strong> to the mall. / We (plural) <strong>will go</strong> to the mall.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>*</strong>Not to be confused with present tense that can be used for future (We are going to
                                             the beach tomorrow.) 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>How to Fix </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>Present tenses</strong> and <strong>past continuous tense</strong> will need to change based on the number of the subject.
                                          </p>
                                          
                                          <p>If one of these tenses uses a <span><strong> "be" verb</strong></span> (is, are, was, were, am) as either the main verb or a helping verb, it will need
                                             to be changed based on the number of the subject: 
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>The dogs <span><strong>is</strong></span> black. --&gt; The dogs <strong>are</strong> black.
                                             </p>
                                             
                                             <p>We <span><strong>was</strong></span> eating lunch at the park. --&gt; We <strong>were</strong> eating lunch at the park.
                                             </p>
                                             
                                             <p>She <span><strong>have</strong></span> played soccer. --&gt; She <strong>has</strong> played soccer. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>For regular verbs that don't use "be" verbs, you will need to add an "<strong>-s</strong>" or "<strong>-es</strong>" to verbs that are 3rd person singular (ex. he/she/it).
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>The company <span><strong>buy</strong></span> products to sell. --&gt; The company <em>buy<strong>s</strong></em> products to sell. 
                                             </p>
                                             
                                             <p>He <span><strong>watch</strong></span> TV everyday. --&gt; He <em>watch<strong>es</strong></em> TV everyday. 
                                             </p>
                                             
                                             <blockquote>&nbsp;
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                          <p>If you can replace a subject with either "he", "she", or "it", then the verb needs
                                             to have an <strong>-s/es</strong> attached.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>The book (it) require<strong>s</strong> a lot of work. ("it require<strong>s</strong>" <strong>not</strong> "it require") 
                                             </p>
                                             
                                             <p>The teacher (he/she) at the community center walk<strong>s</strong> to work. ("she/he walk<span><strong>s</strong></span>" <strong>not</strong> "she/he walk")
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>Keep in mind that the verb follows the subject, so if you mistake the subject, your
                                             verb may be incorrect.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>Each of the languages <span><strong>are</strong></span> difficult. :(
                                             </p>
                                             
                                             <p>Each of the languages <strong>is</strong> difficult. :)
                                             </p>
                                             
                                             <p>In this example, "<strong>each</strong>" is the subject of the sentence (<span><strong>not "languages"</strong></span>). Since "each" is singular, we need "is" - not "are."
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>When you have a compound subject (more than 1 subject), your verb may need to change
                                             depending on what kind of coordinating conjunction (connecting word) you are using.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>"and" adds subjects together (<span>making verbs <em>plural</em></span>): </strong></p>
                                             
                                             <blockquote>
                                                
                                                <p>My <em>teacher</em> <strong>and</strong> my <em>friend</em> <strong>play</strong> basketball.
                                                </p>
                                                
                                                <p>My teacher <strong>and</strong> my friend play<span><strong>s</strong></span> basketball. :( 
                                                </p>
                                                
                                             </blockquote>
                                             
                                             <p><strong>"or/nor" do not add - the verb agrees with</strong> <span><em><strong>the closest subject</strong></em></span><em><strong>:</strong></em></p>
                                             
                                             <blockquote>
                                                
                                                <p>His brothers <strong>or</strong> his <strong>sister likes</strong> to eat candy.
                                                </p>
                                                
                                                <p>His brothers <strong>or</strong> his <span><strong>sister</strong></span> <strong>like</strong> to eat candy. :( 
                                                </p>
                                                
                                                <p>The whale or the <strong>dolphins are</strong> responsible for the problem. 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>For a more detailed explanation and for more examples, check out: </p>
                                          
                                          <p><a href="https://owl.english.purdue.edu/owl/resource/599/01/">Purdue OWL - SVA </a></p>
                                          
                                          <p><a href="http://www.ccc.commnet.edu/grammar/sv_agr.htm">Capital Community College - SVA</a></p>
                                          
                                          <p><a href="https://webapps.towson.edu/ows/moduleSVAGR.htm">Towson - SVA </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Some of these worksheets contain explanations with exercises; others are simply exercises.</p>
                                          
                                          <p><a href="https://www.menlo.edu/wp-content/uploads/2015/03/SUBJECT-VERB_AGREEMENT.pdf">SVA Practice 1</a></p>
                                          
                                          <p><a href="http://knowledge.thinkingstorm.com/Portals/0/Subject-Verb%20Agreement%20Worksheet.pdf">SVA Practice 2</a></p>
                                          
                                          <p><a href="http://www.pcc.edu/staff/pdf/645/SubjectVerbAgreement.pdf">SVA Practice 3</a></p>
                                          
                                          <p><a href="https://owl.english.purdue.edu/exercises/5/13/34">SVA Practice 4 </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/subject-verb-agreement.pcf">©</a>
      </div>
   </body>
</html>