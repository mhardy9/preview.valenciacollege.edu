<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/topic-sentences.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Topic Sentences </h2>
                           
                           
                           
                           
                           <p>If a thesis statement is like a road map for your reader, topic sentences are definitely
                              the signposts along the way that signal where you are in the journey. See below for
                              ways to incorporate topic sentences in your essays. 
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">What is it? </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>A topic sentence is usually the <strong>first</strong> sentence of a body paragraph. Just as a thesis statement tells your reader what your
                                          paper is about, a topic sentence tells your reader what your paragraph is about.
                                       </p>
                                       
                                       <p>Topic sentences are the main idea of your paragraph, so they should be more general
                                          than specific.
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p>"<span><strong>Gorillas eat bananas, plants, and even caterpillars</strong></span>," would be too specific- this information is more of a detail rather than a main
                                             idea.
                                          </p>
                                          
                                          <p>A better topic sentence could be "<strong>Gorillas have a varied diet</strong>," or "<strong>Gorillas eat many different types of food</strong>."
                                          </p>
                                          
                                       </blockquote>                  
                                       <p>Of course, you don't want your topic sentence to be too general: "<span><strong>There are many trees in the world</strong></span>," doesn't really tell your reader what you're going to talk about. It's too much
                                          of a general statement. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>How do I make one? </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Topic sentences should be based upon your thesis statement. If you include your supporting
                                             points/subpoints in the thesis statement, those supporting points can each be used
                                             to make up one of your topic sentences.
                                          </p>
                                          
                                          <p><span><strong>Example</strong></span><strong>:</strong> (TS=topic sentence)
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>Thesis</strong>: Higher taxes, fewer job opportunities, and credit cards are all reasons why the
                                                middle class is in decline. 
                                             </p>
                                             
                                             <p><strong>TS1</strong>: Having to pay more taxes is one of the causes for the decrease in people considered
                                                middle class.
                                             </p>
                                             
                                             <p><strong>TS2</strong>: The middle class population is also diminishing because of a lack of jobs.
                                             </p>
                                             
                                             <p><strong>TS3</strong>: Finally, the fact that many people overuse credit cards has contributed to a smaller
                                                middle class as well. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>The topic sentences above use different wording than what was written in the thesis
                                             statement. If possible, try not to make your topic sentences sound exactly like your
                                             thesis statement. Notice how the following topic sentences sound a little too similar
                                             to the thesis statement (using the thesis from before):
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>TS1</strong>: Higher taxes is one reason way the middle class is in decline. 
                                             </p>
                                             
                                             <p><strong>TS2</strong>: Fewer job opportunities is another reason why the middle class is in decline.
                                             </p>
                                             
                                             <p><strong>TS3</strong>: Credit cards are also why the middle class is in decline.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>If you already read our page about <a href="thesis.php">thesis statements</a>, you know that a specific thesis statement needs 3 things: <strong>topic</strong> + <strong>focus</strong> + <strong>supporting points</strong>. Your topic sentence should follow this same model, but you only will use <strong>one</strong> of the supporting points. 
                                          </p>
                                          
                                          <p>Therefore, each topic sentence should not only include a supporting point, bu also
                                             your topic and wording that suggests your focus (whether you're talking about causes,
                                             effects, comparisons, etc). 
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="https://owl.english.purdue.edu/engagement/2/2/57/">Purdue OWL - Topic Sentences </a></p>
                                          
                                          <p><a href="http://www.indiana.edu/~wts/pamphlets/paragraphs.shtml">Indiana U - Topic Sentences &amp; Paragraphs</a></p>
                                          
                                          <p><a href="http://writingcenter.fas.harvard.edu/pages/topic-sentences-and-signposting">Harvard College - Topic Sentences &amp; Signposting </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/topic-sentences.pcf">©</a>
      </div>
   </body>
</html>