<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/scholarship-essay-tips.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Scholarship Essays </h2>
                           
                           
                           
                           
                           <p>Today, the cost of education can  be prohibitive for many people. Besides grant funding,
                              scholarships are one of the best ways to get money to help pay for college. Unlike
                              student loans, you never have to pay back the money you receive from a scholarship.
                              Whether it's $500, or $5,000, a scholarship (or more than 1 scholarship) can help
                              you cover at least some (if not all) of the costs related to pursuing your degree.
                           </p>
                           
                           <p>Every year, scholarships go unawarded simply because not enough people apply for them.
                              Since scholarship applications are free to submit, there's really no reason not to
                              apply. All it takes is a little time and effort, including the effort of writing some
                              essays. Some people choose not to apply simply because they don't think they can write
                              a good enough essay for the application. However, it's often the case that scholarship
                              committee members are not looking for "the perfect essay." Rather, they are looking
                              to see if you are a good candidate for the specific scholarship, even if your grammar
                              isn't error free.
                           </p>
                           
                           <p>One thing is for sure:<strong> there's no way you're going to get a scholarship if you don't apply. </strong>Why not take a chance? 
                           </p>
                           
                           <p>See below for links to info about scholarships, as well as some tips for writing your
                              scholarship essays. 
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Some Scholarship Links </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.valencia.org/scholarships/">Valencia Foundation </a></p>
                                       
                                       <p><a href="../../finaid/Scholarship_bulletin.php">Scholarship Bulletin</a></p>
                                       
                                       <p>Honors Scholarships: <a href="../../honors/prospective-students/finaid.php">Prospective Students</a> | <a href="../../honors/current-students/finaid.php">Current Students</a> 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        
                        <h2>
                           <section>Scholarship Essay Tips</section>
                        </h2>
                        
                        <section></section>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Be Personal</h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>Keep in mind that the scholarship committee is trying to get to know <strong>you</strong> as a person. As a result, you want your scholarship essays to give information about
                                          yourself. If you're asked to share your career goals, you might consider sharing [briefly]
                                          about your motivation for choosing your career choice.
                                       </p>
                                       
                                    </blockquote>
                                    
                                    <blockquote>
                                       
                                       <p>They don't want to hear a long, detailed account about a family member, friend, or
                                          circumstance (unless they're asking for it); they want to hear about you and your
                                          life. Of course, those other things may make an appearance in your essay, but what
                                          you write should always focus on how it relates to you.
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Be Specific </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>The less vague you are, the better. </p>
                                       
                                    </blockquote>
                                    
                                    <blockquote>
                                       
                                       <p>Think about answering at least some of the "Who, What, When, Where, Why, How" questions.
                                          For example: If the prompt asks about your career aspirations, don't just say that
                                          you want to go into business. Are you going to work for a company in a specific field-
                                          which one? Do you want to be an entrepreneur- what are you going to sell/what service
                                          will you provide? Do you plan on making your company into a franchise? Do you ultimately
                                          want to be a VP, CEO, or accounts manager? Say so in the essay. 
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Stay Focused </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>When writing your essay, make sure to completely answer the prompt. </p>
                                       
                                    </blockquote>
                                    
                                    <blockquote>
                                       
                                       <p>Try your best not to go off topic. </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>A Five-paragraph Essay may not be Required </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>Many of the scholarships that you might apply for at Valencia often don't  allow you
                                          sufficient words to write a long essay. With typical caps of 150, 250, or 500 words,
                                          most of your "essays" are likely to be a paragraph, or maybe a few paragraphs.
                                       </p>
                                       
                                       <p>Consequently, you shouldn't feel the need to have a proper essay with introduction,
                                          body paragraphs, and conclusion. You may have an introductory sentence or two, or
                                          a concluding sentence or two, but nothing like a 5 paragraph essay. That being said,
                                          you can be creative with how you introduce information, as long as it's on topic:
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p>ex. Having a congenital heart condition, as a child I spent a lot of time in hospitals.
                                             Even though it was scary, it was the nurses' gentle care and soothing words that helped
                                             me through the difficult and painful experiences. Those caregivers--those lifegivers--are
                                             the only reason I made it through emotionally and physically. As a result, it is my
                                             desire to pursue a career as a pediatric nurse and care for children in the same way
                                             I was cared for.
                                          </p>
                                          
                                          <p>In this example, the reader gets introduced to the scholarship applicant, but in a
                                             more meaningful way than "Hi, my name is _______ and my career goal is to be a pediatric
                                             nurse."
                                          </p>
                                          
                                       </blockquote>
                                       
                                    </blockquote>
                                    
                                    <blockquote>
                                       
                                       <p>That is not to say that you won't or can't write a five-pararaph style essay for a
                                          scholarship application. Again, it would depend on the length allowed, as well as
                                          the prompt provided. If the prompt asks you tell the committee about yourself, for
                                          example, that might be an ideal time to construct an essay similar to the 5 paragraph
                                          model.
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Don't Sweat the Small Stuff </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>As mentioned earlier, scholarship committees are trying to learn about you and see
                                          whether you're a good candidate, not whether you are a grammar or writing expert.
                                       </p>
                                       
                                    </blockquote>
                                    
                                    <blockquote>
                                       
                                       <p>Make sure your essay is as good as it can be, but even if you think it's not perfect,
                                          don't let that keep you from  applying. 
                                       </p>
                                       
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Take Some Time </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>Don't feel pressured to submit your application the same day you start it. Unless
                                       the deadline is today, take some time to think over your essays. After you write your
                                       essays, leave them for a day or two and then reread them. You'll often find that doing
                                       this will give you a "fresher" perspective, and you may find something you missed
                                       before (or think of something else to say that you didn't think of before). 
                                    </blockquote>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <h3>Get a Second Pair of Eyes, or Third, or Fourth... </h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <blockquote>
                                       
                                       <p>Before you submit your scholarship essays, ask a family member, friend, teacher, coworker,
                                          or whoever, to read your essays and give you some feedback. Chances are, if they don't
                                          understand something in your essay, the scholarship committee won't understand either.
                                       </p>
                                       
                                    </blockquote>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                        </div>            
                        
                        <section> </section>
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/scholarship-essay-tips.pcf">©</a>
      </div>
   </body>
</html>