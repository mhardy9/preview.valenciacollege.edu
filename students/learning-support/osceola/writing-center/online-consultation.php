<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/online-consultation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-12"><a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Unfortunately, we are unable to offer online consultations at this time. We apologize
                           for any inconvenience.
                        </h2>
                        
                        
                        <p>If you need assistance with your paper, please  come in to the Writing Center for
                           a face-to-face consultation (no appointment  needed). If you would like to receive
                           feedback on your paper but can't come to a campus, please use the Smarthinking service.
                           Thank you!
                        </p>
                        
                        
                        <h2>Smarthinking</h2>
                        
                        <p>Just a friendly reminder to let you know about Smarthinking, the free online tutoring
                           tool for Valencia's students. Smarthinking offers tutoring in a variety of subjects:
                           math, nursing, allied health, writing, reading, science, Spanish, business, computers
                           and technology. Students have several options: Drop-In Tutoring, Scheduled Appointments,
                           Offline Questions, and Paper Submissions to the Writing Center. Math tutors are available
                           24/7, please <a href="http://www.smarthinking.com/m/pdf/uploads/Smarthinking%20-%20Tutoring%20Hours%2 0of%20Service.pdf" target="_blank">consult the tutoring schedule</a> for all other subjects. Just added this summer are apps for both iOS and Android
                           devices.
                        </p>
                        
                        
                        <p>Students can access Smarthinking through the Courses tab in Atlas, under <strong>My Courses</strong>. Students have an eight hour limit of usage per semester, an increase from the previous
                           five hour limit. A tip for you: Smarthinking is best used as a back up to on-campus
                           services and support, not a replacement. Directly above the Smarthinking link in Atlas
                           is a link to <a href="http://valenciacollege.edu/learning-support/">Learning Support</a>'s website, a great place to check for campus-based information.
                        </p>
                        
                        
                        <p>Rest assured, Smarthinking tutors are:</p>
                        
                        <blockquote>...seasoned educators — 90% have an advanced degree in their fields and they average
                           over 9 years of teaching or tutoring experience. Drawn from college faculty, graduate
                           students, high school teachers and retired educators, all tutors must complete Smarthinking's
                           online training program and are regularly evaluated for quality and consistency.
                        </blockquote>
                        
                        		  
                        
                        
                        
                     </div>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/online-consultation.pcf">©</a>
      </div>
   </body>
</html>