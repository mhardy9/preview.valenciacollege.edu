<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/academic-language.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Academic Language </h2>
                           
                           
                           
                           
                           <p>Most of your writing assignments at Valencia will probably require "academic language."
                              While the writing you use in an email or on facebook will likely be similar to how
                              you speak (informal), college writing tends to be more formal, or academic, and less
                              like we speak in day-to-day conversation. Below you will find many ways to make your
                              writing more formal. Keep in mind, not all college assignments require academic style.
                              If you are writing a personal response, reflection, or some form of creative writing
                              (like fiction), you probably will not need to follow all of the guidelines listed.
                              
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Avoid Informal, Slang, or Colloquial Language </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>It's best to limit this kind of language in your papers. Consider how you change the
                                          way you talk, and the words you use, when you are giving a business presentation or
                                          applying for a job- we start speaking more formally. This holds true in the academic
                                          world as well.
                                       </p>
                                       
                                       <div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The researchers gave the children some <strong>junk</strong> to play with.
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The researchers gave the children some <strong>items</strong> to play with.
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Informal</strong>:
                                                   </div>
                                                </div>
                                                
                                                <div data-old-tag="td">There are <strong>a lot of</strong> responses to...
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Formal</strong>:
                                                   </div>
                                                </div>
                                                
                                                <div data-old-tag="td">There are <strong>many</strong> responses to...
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>                  
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid Using 1st Person (I / We) </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Depending on the assignment, you may or may not be able to use personal experience
                                             as an example or as support. If it is permitted, then you may be able to use "I" in
                                             your paper (though it would still be best used sparingly). In general though, wherever
                                             you would normally use "I/we", you can often substitute a third person subject (people,
                                             one, students, etc) or simply state the information.
                                          </p>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td"><span><strong>I</strong></span> am going to tell you about stress. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">Stress is...</div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td"><span><strong>I</strong></span> did not find any information about the effects of weight loss. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">No information on the effects of weight loss was found. </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td"><span><strong>I</strong></span> think people should not smoke because... 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">People should not smoke because... </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid Phrasal Verbs </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Phrasal Verbs are verbs made up of 2 or more words (pick up, fill out, look forward
                                             to). We use these extensively in conversation, but when writing academically it is
                                             best to find a single word option because it is perceived as more formal.
                                          </p>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The author said that the study will be <span><strong>put off</strong></span> until next year. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The author said that the study will be <strong>postponed</strong> until next year. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">He was able to <span><strong>get rid of</strong></span> the problem.
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">He was able to <strong>remove</strong> the problem. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid Contractions </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">This answer <strong>won't</strong> be enough. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">This answer <strong>will not</strong> be enough. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The theory <strong>couldn't</strong> be supported. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The theory <strong>could not</strong> be supported. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid 2nd Person ("You") </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Most of the time it is best not to have a direct conversation with the reader of your
                                             paper. In order to get around using "you", often you can simply add a noun for the
                                             person/group of people you are referring to, or you may only need to remove the pronoun.
                                             
                                          </p>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">If <span><strong>you</strong></span> enroll on time, <span><strong>you</strong></span> will be happy. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">If <strong>a student</strong> enrolls on time, <strong>he/she</strong> will be happy. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The <strong>ones</strong> who enroll on time will be happy. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">First, <span><strong>you</strong></span> heat the solution. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">First, heat the solution. </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid Informal Negative Forms </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td"><strong>Not very much</strong> research has been done. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td"><strong>Little</strong> research has been done. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">There were<span><strong> not very many</strong></span> examples. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">There were <strong>few</strong> examples. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Avoid Using "etc.", "and so on", or "and so forth" </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>The less vague you are in your writing the better. Instead of using the above,  write
                                             out what category or group the list you are making comes from. 
                                          </p>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The people saw dogs, cats, <span><strong>etc</strong></span>. 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">The people saw dogs, cats, and <strong>other animals</strong>. 
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Informal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">Strawberries, blueberries, and so on are all necessary for health. </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   <div data-old-tag="td">
                                                      <div><strong>Formal</strong>:
                                                      </div>
                                                   </div>
                                                   
                                                   <div data-old-tag="td">Strawberries, blueberries, and other berries are all necessary for health. </div>
                                                   
                                                </div>
                                                
                                             </div>
                                          </div>
                                          
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/academic-language.pcf">©</a>
      </div>
   </body>
</html>