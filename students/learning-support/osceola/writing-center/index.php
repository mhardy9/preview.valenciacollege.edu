<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Writing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-12">
                        
                        <h2>Mission</h2>
                        
                        
                        <p>We at the Writing Center exist to help every Valencia student become a better writer.
                           It is our desire to provide support during all stages of the writing process, and
                           ultimately, we wish for every consultation to be a learning experience for students
                           so that they may succeed academically as well as professionally. 
                        </p>
                        
                        
                        <div>  
                           <a alt="Services" href="/osceola/writing-center/services.php" target="_self" title="Services">Services</a>
                           <a alt="Staff" href="/osceola/writing-center/staff.php" target="_self" title="Staff">Staff</a>
                           <a alt="Resources" href="/osceola/writing-center/resources.php" target="_self" title="Resources">Resources</a>
                           <a alt="Online Consultation" href="/osceola/writing-center/online-consultation.php" target="_blank" title="Onilne Consultation">Online Consultation</a>                          
                           <br> 
                           
                        </div>
                        
                        <div>
                           
                           <h2>Writing Center Hours</h2>
                           
                           <div>
                              
                              
                              
                              <!-- email, phone, address, location, hours -->
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 3, Rm 100</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-4250</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr"> 
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                          
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                          <br></div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                              </div>
                              
                              
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div> 
                  
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/index.pcf">©</a>
      </div>
   </body>
</html>